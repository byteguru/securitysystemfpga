

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
--use clockDevider;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;



entity decoder is
    Port ( cols  : in STD_LOGIC_VECTOR (3 downto 0);
           reset : in STD_LOGIC;
           sysclk : in STD_LOGIC;
           rows: out STD_LOGIC_VECTOR (3 downto 0);
           output : out STD_LOGIC_VECTOR (19 downto 0));
end decoder;

architecture Behavioral of decoder is

signal currentState : STD_LOGIC_VECTOR(2 downto 0);
signal devidedClock : STD_LOGIC_VECTOR(32 downto 0);
signal clock15khz : STD_LOGIC;
signal clock1khz : STD_LOGIC;
signal key : STD_LOGIC_VECTOR (4 downto 0);
signal key2 : STD_LOGIC_VECTOR (4 downto 0);
signal pin : STD_LOGIC_VECTOR (19 downto 0);
signal colValues : STD_LOGIC_VECTOR (3 downto 0);

attribute mark_debug : string;
attribute mark_debug of colValues : signal is "true";
attribute mark_debug of rows : signal is "true";
attribute mark_debug of key : signal is "true";

attribute mark_debug of reset : signal is "true";
attribute mark_debug of currentState : signal is "true";


begin
    
    
    process(clock15khz, reset)
    
    variable currentKey : std_logic_vector(4 downto 0);
    
    begin
        if(reset = '1')
            then
                currentKey := "11111";
                pin <= x"FFFFF";
                currentState <= "000";
                rows <="0000";
            
        elsif(clock15khz'event and clock15khz = '0')
        then
            if(currentState = "111")
            then
                case not cols is
                                when "0001" => currentKey := "01010";
                                when "0010" => currentKey := "01011";
                                when "0100" => currentKey := "01100";
                                when "1000" => currentKey := "01101";
                                when others => currentKey := currentKey;
                           end case;
            else
                case not cols is
                    when "0001" => currentKey := std_logic_vector(unsigned("000" & currentState(2 downto 1)) + 1 + 0);
                    when "0010" => currentKey := std_logic_vector(unsigned("000" & currentState(2 downto 1)) + 1 + 3);
                    when "0100" => currentKey := std_logic_vector(unsigned("000" & currentState(2 downto 1)) + 1 + 6);
                    when "1000" => currentKey := std_logic_vector(3 - unsigned("000" & currentState(2 downto 1)) + 1 + 12);
                    when others => currentKey := currentKey;
               end case;
            end if;
 
        end if;
        
        if(clock15khz'event and clock15khz = '1')
        then
            case currentState is
                when "000" => rows <= "1110";
                when "010" => rows <= "1101";
                when "100" => rows <= "1011";
                when "110" => rows <= "0111";
                when others => rows <= "1111";
            end case;
            currentState <= std_logic_vector(unsigned(currentState) + 1);
        end if;
        
        key <= currentKey;
        
        
        
    end process;
    
    process(clock1khz)
    
    begin
        if(clock1khz'event and clock1khz = '1')
            then
                output <= "000000000000000" & key;
                
            end if;
    end process;
    
    clockDeviderInst : entity work.clockDevider
        port map( sys_clk => sysclk,
                clk => devidedClock);
               

            
    clock15khz <= devidedClock(9);
    clock1khz <= devidedClock(23);
    
    colValues <= std_logic_vector(unsigned(cols));
    

end Behavioral;
