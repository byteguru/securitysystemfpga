// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Thu Feb 22 18:01:31 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire [31:0]INPUT_STREAM_TDATA;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire INPUT_STREAM_V_data_V_0_ack_in;
  wire INPUT_STREAM_V_data_V_0_load_A;
  wire INPUT_STREAM_V_data_V_0_load_B;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_A;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_B;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_data_V_0_sel_wr;
  wire INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ;
  wire \ap_CS_fsm[3]_i_2_n_4 ;
  wire \ap_CS_fsm_reg_n_4_[0] ;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state5;
  wire ap_CS_fsm_state6;
  wire ap_CS_fsm_state7;
  wire [7:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_NS_fsm14_out;
  wire ap_NS_fsm16_out;
  wire ap_NS_fsm17_out;
  wire ap_clk;
  wire ap_done;
  wire ap_return0;
  wire ap_rst_n;
  wire ce0;
  wire [4:0]d0;
  wire [4:0]i_1_cast2_cast_reg_320;
  wire i_1_reg_177;
  wire \i_1_reg_177_reg_n_4_[0] ;
  wire \i_1_reg_177_reg_n_4_[1] ;
  wire \i_1_reg_177_reg_n_4_[2] ;
  wire \i_1_reg_177_reg_n_4_[3] ;
  wire \i_1_reg_177_reg_n_4_[4] ;
  wire [5:0]i_2_fu_229_p2;
  wire [5:0]i_3_fu_261_p2;
  wire [5:0]i_3_reg_329;
  wire \i_reg_166_reg_n_4_[0] ;
  wire \i_reg_166_reg_n_4_[1] ;
  wire \i_reg_166_reg_n_4_[2] ;
  wire \i_reg_166_reg_n_4_[3] ;
  wire \i_reg_166_reg_n_4_[4] ;
  wire [6:0]indvarinc_fu_199_p2;
  wire [6:0]indvarinc_reg_290;
  wire \indvarinc_reg_290[6]_i_2_n_4 ;
  wire interrupt;
  wire invdar_reg_155;
  wire invdar_reg_1550;
  wire \invdar_reg_155_reg_n_4_[0] ;
  wire \invdar_reg_155_reg_n_4_[1] ;
  wire \invdar_reg_155_reg_n_4_[2] ;
  wire \invdar_reg_155_reg_n_4_[3] ;
  wire \invdar_reg_155_reg_n_4_[4] ;
  wire \invdar_reg_155_reg_n_4_[5] ;
  wire \invdar_reg_155_reg_n_4_[6] ;
  wire p_44_in;
  wire \p_s_reg_188[0]_i_1_n_4 ;
  wire \p_s_reg_188[1]_i_1_n_4 ;
  wire \p_s_reg_188[2]_i_1_n_4 ;
  wire \p_s_reg_188[3]_i_1_n_4 ;
  wire \p_s_reg_188[4]_i_1_n_4 ;
  wire \p_s_reg_188[5]_i_1_n_4 ;
  wire \p_s_reg_188[5]_i_2_n_4 ;
  wire \p_s_reg_188[6]_i_1_n_4 ;
  wire \p_s_reg_188_reg_n_4_[0] ;
  wire \p_s_reg_188_reg_n_4_[1] ;
  wire \p_s_reg_188_reg_n_4_[2] ;
  wire \p_s_reg_188_reg_n_4_[3] ;
  wire \p_s_reg_188_reg_n_4_[4] ;
  wire \p_s_reg_188_reg_n_4_[5] ;
  wire reset;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]searched;
  wire [31:0]searched_read_reg_285;
  wire \tmp_1_reg_305[0]_i_1_n_4 ;
  wire \tmp_1_reg_305[0]_i_2_n_4 ;
  wire \tmp_1_reg_305_reg_n_4_[0] ;
  wire tmp_2_fu_221_p3;
  wire tmp_6_fu_253_p3;
  wire tmp_6_reg_325;
  wire tmp_8_fu_272_p2;
  wire [6:0]tmp_reg_295;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.D(ap_NS_fsm[1:0]),
        .E(ap_NS_fsm17_out),
        .Q({ap_done,ap_CS_fsm_state3,\ap_CS_fsm_reg_n_4_[0] }),
        .SR(invdar_reg_155),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .\p_s_reg_188_reg[6] ({ap_return0,\p_s_reg_188_reg_n_4_[5] ,\p_s_reg_188_reg_n_4_[4] ,\p_s_reg_188_reg_n_4_[3] ,\p_s_reg_188_reg_n_4_[2] ,\p_s_reg_188_reg_n_4_[1] ,\p_s_reg_188_reg_n_4_[0] }),
        .\rdata_data_reg[31]_0 (searched),
        .reset(reset),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .\tmp_1_reg_305_reg[0] (\tmp_1_reg_305_reg_n_4_[0] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb Adder2_stream_int_i_U
       (.DIADI(d0),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[4] (INPUT_STREAM_V_data_V_0_payload_B[4:0]),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4:0]),
        .\ap_CS_fsm_reg[3] ({ap_CS_fsm_state4,ce0}),
        .ap_clk(ap_clk),
        .\invdar_reg_155_reg[6] ({\invdar_reg_155_reg_n_4_[6] ,\invdar_reg_155_reg_n_4_[5] ,\invdar_reg_155_reg_n_4_[4] ,\invdar_reg_155_reg_n_4_[3] ,\invdar_reg_155_reg_n_4_[2] ,\invdar_reg_155_reg_n_4_[1] ,\invdar_reg_155_reg_n_4_[0] }));
  GND GND
       (.G(\<const0> ));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_A));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \INPUT_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(INPUT_STREAM_V_data_V_0_sel_wr),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_data_V_0_ack_in),
        .O(INPUT_STREAM_V_data_V_0_load_B));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hF708)) 
    INPUT_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(tmp_2_fu_221_p3),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_data_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_data_V_0_sel_wr),
        .R(reset));
  LUT6 #(
    .INIT(64'hAAA2AAAAAA000000)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(ap_CS_fsm_state4),
        .I2(tmp_2_fu_221_p3),
        .I3(INPUT_STREAM_TVALID),
        .I4(INPUT_STREAM_V_data_V_0_ack_in),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h2F22FFFF)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_221_p3),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_V_data_V_0_ack_in),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(INPUT_STREAM_V_data_V_0_ack_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hA2AAA000)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(p_44_in),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_TREADY),
        .I4(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h08FF0808FFFFFFFF)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(tmp_2_fu_221_p3),
        .I3(INPUT_STREAM_TVALID),
        .I4(INPUT_STREAM_TREADY),
        .I5(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(INPUT_STREAM_V_last_V_0_sel_wr),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I3(INPUT_STREAM_V_last_V_0_ack_in),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(tmp_2_fu_221_p3),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hA8A820A0)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I3(p_44_in),
        .I4(INPUT_STREAM_TVALID),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_2 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(tmp_2_fu_221_p3),
        .O(p_44_in));
  LUT6 #(
    .INIT(64'h3333F333BBBBFBBB)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(INPUT_STREAM_V_last_V_0_ack_in),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I2(ap_CS_fsm_state4),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I4(tmp_2_fu_221_p3),
        .I5(INPUT_STREAM_TVALID),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(reset));
  LUT4 #(
    .INIT(16'hF888)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(\ap_CS_fsm[3]_i_2_n_4 ),
        .I2(ap_CS_fsm_state3),
        .I3(\tmp_1_reg_305_reg_n_4_[0] ),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'h5757575F5F5F575F)) 
    \ap_CS_fsm[3]_i_2 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(tmp_2_fu_221_p3),
        .I3(INPUT_STREAM_V_last_V_0_payload_A),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .I5(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\ap_CS_fsm[3]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'hFFB8FF0000000000)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(tmp_2_fu_221_p3),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I5(ap_CS_fsm_state4),
        .O(ap_NS_fsm[4]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hFF04)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(tmp_8_fu_272_p2),
        .I1(ap_CS_fsm_state7),
        .I2(tmp_6_reg_325),
        .I3(ap_CS_fsm_state5),
        .O(ap_NS_fsm[5]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hC8)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(tmp_6_reg_325),
        .I1(ap_CS_fsm_state7),
        .I2(tmp_8_fu_272_p2),
        .O(ap_NS_fsm[7]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_4_[0] ),
        .S(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ce0),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ce0),
        .Q(ap_CS_fsm_state3),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state6),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_CS_fsm_state6),
        .Q(ap_CS_fsm_state7),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(ap_done),
        .R(reset));
  FDRE \i_1_cast2_cast_reg_320_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\i_1_reg_177_reg_n_4_[0] ),
        .Q(i_1_cast2_cast_reg_320[0]),
        .R(1'b0));
  FDRE \i_1_cast2_cast_reg_320_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\i_1_reg_177_reg_n_4_[1] ),
        .Q(i_1_cast2_cast_reg_320[1]),
        .R(1'b0));
  FDRE \i_1_cast2_cast_reg_320_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\i_1_reg_177_reg_n_4_[2] ),
        .Q(i_1_cast2_cast_reg_320[2]),
        .R(1'b0));
  FDRE \i_1_cast2_cast_reg_320_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\i_1_reg_177_reg_n_4_[3] ),
        .Q(i_1_cast2_cast_reg_320[3]),
        .R(1'b0));
  FDRE \i_1_cast2_cast_reg_320_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\i_1_reg_177_reg_n_4_[4] ),
        .Q(i_1_cast2_cast_reg_320[4]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hAA8A)) 
    \i_1_reg_177[5]_i_1 
       (.I0(ap_CS_fsm_state5),
        .I1(tmp_6_reg_325),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_8_fu_272_p2),
        .O(i_1_reg_177));
  LUT3 #(
    .INIT(8'h04)) 
    \i_1_reg_177[5]_i_2 
       (.I0(tmp_8_fu_272_p2),
        .I1(ap_CS_fsm_state7),
        .I2(tmp_6_reg_325),
        .O(ap_NS_fsm1));
  FDRE \i_1_reg_177_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_3_reg_329[0]),
        .Q(\i_1_reg_177_reg_n_4_[0] ),
        .R(i_1_reg_177));
  FDRE \i_1_reg_177_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_3_reg_329[1]),
        .Q(\i_1_reg_177_reg_n_4_[1] ),
        .R(i_1_reg_177));
  FDRE \i_1_reg_177_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_3_reg_329[2]),
        .Q(\i_1_reg_177_reg_n_4_[2] ),
        .R(i_1_reg_177));
  FDRE \i_1_reg_177_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_3_reg_329[3]),
        .Q(\i_1_reg_177_reg_n_4_[3] ),
        .R(i_1_reg_177));
  FDRE \i_1_reg_177_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_3_reg_329[4]),
        .Q(\i_1_reg_177_reg_n_4_[4] ),
        .R(i_1_reg_177));
  FDRE \i_1_reg_177_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm1),
        .D(i_3_reg_329[5]),
        .Q(tmp_6_fu_253_p3),
        .R(i_1_reg_177));
  LUT1 #(
    .INIT(2'h1)) 
    \i_3_reg_329[0]_i_1 
       (.I0(\i_1_reg_177_reg_n_4_[0] ),
        .O(i_3_fu_261_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_3_reg_329[1]_i_1 
       (.I0(\i_1_reg_177_reg_n_4_[0] ),
        .I1(\i_1_reg_177_reg_n_4_[1] ),
        .O(i_3_fu_261_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_3_reg_329[2]_i_1 
       (.I0(\i_1_reg_177_reg_n_4_[0] ),
        .I1(\i_1_reg_177_reg_n_4_[1] ),
        .I2(\i_1_reg_177_reg_n_4_[2] ),
        .O(i_3_fu_261_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_3_reg_329[3]_i_1 
       (.I0(\i_1_reg_177_reg_n_4_[1] ),
        .I1(\i_1_reg_177_reg_n_4_[0] ),
        .I2(\i_1_reg_177_reg_n_4_[2] ),
        .I3(\i_1_reg_177_reg_n_4_[3] ),
        .O(i_3_fu_261_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_3_reg_329[4]_i_1 
       (.I0(\i_1_reg_177_reg_n_4_[2] ),
        .I1(\i_1_reg_177_reg_n_4_[0] ),
        .I2(\i_1_reg_177_reg_n_4_[1] ),
        .I3(\i_1_reg_177_reg_n_4_[3] ),
        .I4(\i_1_reg_177_reg_n_4_[4] ),
        .O(i_3_fu_261_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_3_reg_329[5]_i_1 
       (.I0(\i_1_reg_177_reg_n_4_[3] ),
        .I1(\i_1_reg_177_reg_n_4_[1] ),
        .I2(\i_1_reg_177_reg_n_4_[0] ),
        .I3(\i_1_reg_177_reg_n_4_[2] ),
        .I4(\i_1_reg_177_reg_n_4_[4] ),
        .I5(tmp_6_fu_253_p3),
        .O(i_3_fu_261_p2[5]));
  FDRE \i_3_reg_329_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_3_fu_261_p2[0]),
        .Q(i_3_reg_329[0]),
        .R(1'b0));
  FDRE \i_3_reg_329_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_3_fu_261_p2[1]),
        .Q(i_3_reg_329[1]),
        .R(1'b0));
  FDRE \i_3_reg_329_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_3_fu_261_p2[2]),
        .Q(i_3_reg_329[2]),
        .R(1'b0));
  FDRE \i_3_reg_329_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_3_fu_261_p2[3]),
        .Q(i_3_reg_329[3]),
        .R(1'b0));
  FDRE \i_3_reg_329_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_3_fu_261_p2[4]),
        .Q(i_3_reg_329[4]),
        .R(1'b0));
  FDRE \i_3_reg_329_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_3_fu_261_p2[5]),
        .Q(i_3_reg_329[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_166[0]_i_1 
       (.I0(\i_reg_166_reg_n_4_[0] ),
        .O(i_2_fu_229_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_reg_166[1]_i_1 
       (.I0(\i_reg_166_reg_n_4_[0] ),
        .I1(\i_reg_166_reg_n_4_[1] ),
        .O(i_2_fu_229_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_reg_166[2]_i_1 
       (.I0(\i_reg_166_reg_n_4_[0] ),
        .I1(\i_reg_166_reg_n_4_[1] ),
        .I2(\i_reg_166_reg_n_4_[2] ),
        .O(i_2_fu_229_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_reg_166[3]_i_1 
       (.I0(\i_reg_166_reg_n_4_[1] ),
        .I1(\i_reg_166_reg_n_4_[0] ),
        .I2(\i_reg_166_reg_n_4_[2] ),
        .I3(\i_reg_166_reg_n_4_[3] ),
        .O(i_2_fu_229_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_reg_166[4]_i_1 
       (.I0(\i_reg_166_reg_n_4_[2] ),
        .I1(\i_reg_166_reg_n_4_[0] ),
        .I2(\i_reg_166_reg_n_4_[1] ),
        .I3(\i_reg_166_reg_n_4_[3] ),
        .I4(\i_reg_166_reg_n_4_[4] ),
        .O(i_2_fu_229_p2[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \i_reg_166[5]_i_1 
       (.I0(\tmp_1_reg_305_reg_n_4_[0] ),
        .I1(ap_CS_fsm_state3),
        .O(ap_NS_fsm16_out));
  LUT6 #(
    .INIT(64'h0000004040400040)) 
    \i_reg_166[5]_i_2 
       (.I0(tmp_2_fu_221_p3),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I2(ap_CS_fsm_state4),
        .I3(INPUT_STREAM_V_last_V_0_payload_A),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .I5(INPUT_STREAM_V_last_V_0_payload_B),
        .O(ap_NS_fsm14_out));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_reg_166[5]_i_3 
       (.I0(\i_reg_166_reg_n_4_[3] ),
        .I1(\i_reg_166_reg_n_4_[1] ),
        .I2(\i_reg_166_reg_n_4_[0] ),
        .I3(\i_reg_166_reg_n_4_[2] ),
        .I4(\i_reg_166_reg_n_4_[4] ),
        .O(i_2_fu_229_p2[5]));
  FDRE \i_reg_166_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm14_out),
        .D(i_2_fu_229_p2[0]),
        .Q(\i_reg_166_reg_n_4_[0] ),
        .R(ap_NS_fsm16_out));
  FDRE \i_reg_166_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm14_out),
        .D(i_2_fu_229_p2[1]),
        .Q(\i_reg_166_reg_n_4_[1] ),
        .R(ap_NS_fsm16_out));
  FDRE \i_reg_166_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm14_out),
        .D(i_2_fu_229_p2[2]),
        .Q(\i_reg_166_reg_n_4_[2] ),
        .R(ap_NS_fsm16_out));
  FDRE \i_reg_166_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm14_out),
        .D(i_2_fu_229_p2[3]),
        .Q(\i_reg_166_reg_n_4_[3] ),
        .R(ap_NS_fsm16_out));
  FDRE \i_reg_166_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm14_out),
        .D(i_2_fu_229_p2[4]),
        .Q(\i_reg_166_reg_n_4_[4] ),
        .R(ap_NS_fsm16_out));
  FDRE \i_reg_166_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm14_out),
        .D(i_2_fu_229_p2[5]),
        .Q(tmp_2_fu_221_p3),
        .R(ap_NS_fsm16_out));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \indvarinc_reg_290[0]_i_1 
       (.I0(\invdar_reg_155_reg_n_4_[0] ),
        .O(indvarinc_fu_199_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \indvarinc_reg_290[1]_i_1 
       (.I0(\invdar_reg_155_reg_n_4_[0] ),
        .I1(\invdar_reg_155_reg_n_4_[1] ),
        .O(indvarinc_fu_199_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \indvarinc_reg_290[2]_i_1 
       (.I0(\invdar_reg_155_reg_n_4_[0] ),
        .I1(\invdar_reg_155_reg_n_4_[1] ),
        .I2(\invdar_reg_155_reg_n_4_[2] ),
        .O(indvarinc_fu_199_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \indvarinc_reg_290[3]_i_1 
       (.I0(\invdar_reg_155_reg_n_4_[1] ),
        .I1(\invdar_reg_155_reg_n_4_[0] ),
        .I2(\invdar_reg_155_reg_n_4_[2] ),
        .I3(\invdar_reg_155_reg_n_4_[3] ),
        .O(indvarinc_fu_199_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \indvarinc_reg_290[4]_i_1 
       (.I0(\invdar_reg_155_reg_n_4_[2] ),
        .I1(\invdar_reg_155_reg_n_4_[0] ),
        .I2(\invdar_reg_155_reg_n_4_[1] ),
        .I3(\invdar_reg_155_reg_n_4_[3] ),
        .I4(\invdar_reg_155_reg_n_4_[4] ),
        .O(indvarinc_fu_199_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \indvarinc_reg_290[5]_i_1 
       (.I0(\invdar_reg_155_reg_n_4_[3] ),
        .I1(\invdar_reg_155_reg_n_4_[1] ),
        .I2(\invdar_reg_155_reg_n_4_[0] ),
        .I3(\invdar_reg_155_reg_n_4_[2] ),
        .I4(\invdar_reg_155_reg_n_4_[4] ),
        .I5(\invdar_reg_155_reg_n_4_[5] ),
        .O(indvarinc_fu_199_p2[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \indvarinc_reg_290[6]_i_1 
       (.I0(\indvarinc_reg_290[6]_i_2_n_4 ),
        .I1(\invdar_reg_155_reg_n_4_[5] ),
        .I2(\invdar_reg_155_reg_n_4_[6] ),
        .O(indvarinc_fu_199_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \indvarinc_reg_290[6]_i_2 
       (.I0(\invdar_reg_155_reg_n_4_[4] ),
        .I1(\invdar_reg_155_reg_n_4_[2] ),
        .I2(\invdar_reg_155_reg_n_4_[0] ),
        .I3(\invdar_reg_155_reg_n_4_[1] ),
        .I4(\invdar_reg_155_reg_n_4_[3] ),
        .O(\indvarinc_reg_290[6]_i_2_n_4 ));
  FDRE \indvarinc_reg_290_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[0]),
        .Q(indvarinc_reg_290[0]),
        .R(1'b0));
  FDRE \indvarinc_reg_290_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[1]),
        .Q(indvarinc_reg_290[1]),
        .R(1'b0));
  FDRE \indvarinc_reg_290_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[2]),
        .Q(indvarinc_reg_290[2]),
        .R(1'b0));
  FDRE \indvarinc_reg_290_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[3]),
        .Q(indvarinc_reg_290[3]),
        .R(1'b0));
  FDRE \indvarinc_reg_290_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[4]),
        .Q(indvarinc_reg_290[4]),
        .R(1'b0));
  FDRE \indvarinc_reg_290_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[5]),
        .Q(indvarinc_reg_290[5]),
        .R(1'b0));
  FDRE \indvarinc_reg_290_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_199_p2[6]),
        .Q(indvarinc_reg_290[6]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input input_U
       (.CO(tmp_8_fu_272_p2),
        .DIADI(d0),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (INPUT_STREAM_V_data_V_0_payload_A[31:5]),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (INPUT_STREAM_V_data_V_0_payload_B[31:5]),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .Q({ap_CS_fsm_state6,ap_CS_fsm_state4,ap_CS_fsm_state3}),
        .ap_clk(ap_clk),
        .\i_1_reg_177_reg[5] ({tmp_6_fu_253_p3,\i_1_reg_177_reg_n_4_[4] ,\i_1_reg_177_reg_n_4_[3] ,\i_1_reg_177_reg_n_4_[2] ,\i_1_reg_177_reg_n_4_[1] ,\i_1_reg_177_reg_n_4_[0] }),
        .\i_reg_166_reg[5] ({tmp_2_fu_221_p3,\i_reg_166_reg_n_4_[4] ,\i_reg_166_reg_n_4_[3] ,\i_reg_166_reg_n_4_[2] ,\i_reg_166_reg_n_4_[1] ,\i_reg_166_reg_n_4_[0] }),
        .\searched_read_reg_285_reg[31] (searched_read_reg_285),
        .\tmp_reg_295_reg[6] (tmp_reg_295));
  LUT2 #(
    .INIT(4'h2)) 
    \invdar_reg_155[6]_i_2 
       (.I0(ap_CS_fsm_state3),
        .I1(\tmp_1_reg_305_reg_n_4_[0] ),
        .O(invdar_reg_1550));
  FDRE \invdar_reg_155_reg[0] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[0]),
        .Q(\invdar_reg_155_reg_n_4_[0] ),
        .R(invdar_reg_155));
  FDRE \invdar_reg_155_reg[1] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[1]),
        .Q(\invdar_reg_155_reg_n_4_[1] ),
        .R(invdar_reg_155));
  FDRE \invdar_reg_155_reg[2] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[2]),
        .Q(\invdar_reg_155_reg_n_4_[2] ),
        .R(invdar_reg_155));
  FDRE \invdar_reg_155_reg[3] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[3]),
        .Q(\invdar_reg_155_reg_n_4_[3] ),
        .R(invdar_reg_155));
  FDRE \invdar_reg_155_reg[4] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[4]),
        .Q(\invdar_reg_155_reg_n_4_[4] ),
        .R(invdar_reg_155));
  FDRE \invdar_reg_155_reg[5] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[5]),
        .Q(\invdar_reg_155_reg_n_4_[5] ),
        .R(invdar_reg_155));
  FDRE \invdar_reg_155_reg[6] 
       (.C(ap_clk),
        .CE(invdar_reg_1550),
        .D(indvarinc_reg_290[6]),
        .Q(\invdar_reg_155_reg_n_4_[6] ),
        .R(invdar_reg_155));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    \p_s_reg_188[0]_i_1 
       (.I0(i_1_cast2_cast_reg_320[0]),
        .I1(tmp_6_reg_325),
        .I2(tmp_8_fu_272_p2),
        .I3(ap_CS_fsm_state7),
        .O(\p_s_reg_188[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    \p_s_reg_188[1]_i_1 
       (.I0(i_1_cast2_cast_reg_320[1]),
        .I1(tmp_6_reg_325),
        .I2(tmp_8_fu_272_p2),
        .I3(ap_CS_fsm_state7),
        .O(\p_s_reg_188[1]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    \p_s_reg_188[2]_i_1 
       (.I0(i_1_cast2_cast_reg_320[2]),
        .I1(tmp_6_reg_325),
        .I2(tmp_8_fu_272_p2),
        .I3(ap_CS_fsm_state7),
        .O(\p_s_reg_188[2]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    \p_s_reg_188[3]_i_1 
       (.I0(i_1_cast2_cast_reg_320[3]),
        .I1(tmp_6_reg_325),
        .I2(tmp_8_fu_272_p2),
        .I3(ap_CS_fsm_state7),
        .O(\p_s_reg_188[3]_i_1_n_4 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \p_s_reg_188[4]_i_1 
       (.I0(i_1_cast2_cast_reg_320[4]),
        .I1(tmp_6_reg_325),
        .I2(tmp_8_fu_272_p2),
        .I3(ap_CS_fsm_state7),
        .O(\p_s_reg_188[4]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h88880888)) 
    \p_s_reg_188[5]_i_1 
       (.I0(ap_CS_fsm_state6),
        .I1(tmp_6_fu_253_p3),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_8_fu_272_p2),
        .I4(tmp_6_reg_325),
        .O(\p_s_reg_188[5]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hFF404040)) 
    \p_s_reg_188[5]_i_2 
       (.I0(tmp_6_reg_325),
        .I1(tmp_8_fu_272_p2),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_6_fu_253_p3),
        .I4(ap_CS_fsm_state6),
        .O(\p_s_reg_188[5]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'hBFBFBFBFBF000000)) 
    \p_s_reg_188[6]_i_1 
       (.I0(tmp_6_reg_325),
        .I1(tmp_8_fu_272_p2),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_6_fu_253_p3),
        .I4(ap_CS_fsm_state6),
        .I5(ap_return0),
        .O(\p_s_reg_188[6]_i_1_n_4 ));
  FDSE \p_s_reg_188_reg[0] 
       (.C(ap_clk),
        .CE(\p_s_reg_188[5]_i_2_n_4 ),
        .D(\p_s_reg_188[0]_i_1_n_4 ),
        .Q(\p_s_reg_188_reg_n_4_[0] ),
        .S(\p_s_reg_188[5]_i_1_n_4 ));
  FDSE \p_s_reg_188_reg[1] 
       (.C(ap_clk),
        .CE(\p_s_reg_188[5]_i_2_n_4 ),
        .D(\p_s_reg_188[1]_i_1_n_4 ),
        .Q(\p_s_reg_188_reg_n_4_[1] ),
        .S(\p_s_reg_188[5]_i_1_n_4 ));
  FDSE \p_s_reg_188_reg[2] 
       (.C(ap_clk),
        .CE(\p_s_reg_188[5]_i_2_n_4 ),
        .D(\p_s_reg_188[2]_i_1_n_4 ),
        .Q(\p_s_reg_188_reg_n_4_[2] ),
        .S(\p_s_reg_188[5]_i_1_n_4 ));
  FDSE \p_s_reg_188_reg[3] 
       (.C(ap_clk),
        .CE(\p_s_reg_188[5]_i_2_n_4 ),
        .D(\p_s_reg_188[3]_i_1_n_4 ),
        .Q(\p_s_reg_188_reg_n_4_[3] ),
        .S(\p_s_reg_188[5]_i_1_n_4 ));
  FDSE \p_s_reg_188_reg[4] 
       (.C(ap_clk),
        .CE(\p_s_reg_188[5]_i_2_n_4 ),
        .D(\p_s_reg_188[4]_i_1_n_4 ),
        .Q(\p_s_reg_188_reg_n_4_[4] ),
        .S(\p_s_reg_188[5]_i_1_n_4 ));
  FDSE \p_s_reg_188_reg[5] 
       (.C(ap_clk),
        .CE(\p_s_reg_188[5]_i_2_n_4 ),
        .D(1'b0),
        .Q(\p_s_reg_188_reg_n_4_[5] ),
        .S(\p_s_reg_188[5]_i_1_n_4 ));
  FDRE \p_s_reg_188_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\p_s_reg_188[6]_i_1_n_4 ),
        .Q(ap_return0),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[0]),
        .Q(searched_read_reg_285[0]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[10]),
        .Q(searched_read_reg_285[10]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[11]),
        .Q(searched_read_reg_285[11]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[12]),
        .Q(searched_read_reg_285[12]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[13]),
        .Q(searched_read_reg_285[13]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[14]),
        .Q(searched_read_reg_285[14]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[15]),
        .Q(searched_read_reg_285[15]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[16]),
        .Q(searched_read_reg_285[16]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[17]),
        .Q(searched_read_reg_285[17]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[18]),
        .Q(searched_read_reg_285[18]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[19]),
        .Q(searched_read_reg_285[19]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[1]),
        .Q(searched_read_reg_285[1]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[20]),
        .Q(searched_read_reg_285[20]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[21]),
        .Q(searched_read_reg_285[21]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[22]),
        .Q(searched_read_reg_285[22]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[23]),
        .Q(searched_read_reg_285[23]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[24] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[24]),
        .Q(searched_read_reg_285[24]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[25] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[25]),
        .Q(searched_read_reg_285[25]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[26] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[26]),
        .Q(searched_read_reg_285[26]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[27] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[27]),
        .Q(searched_read_reg_285[27]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[28] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[28]),
        .Q(searched_read_reg_285[28]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[29] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[29]),
        .Q(searched_read_reg_285[29]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[2]),
        .Q(searched_read_reg_285[2]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[30] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[30]),
        .Q(searched_read_reg_285[30]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[31] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[31]),
        .Q(searched_read_reg_285[31]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[3]),
        .Q(searched_read_reg_285[3]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[4]),
        .Q(searched_read_reg_285[4]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[5]),
        .Q(searched_read_reg_285[5]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[6]),
        .Q(searched_read_reg_285[6]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[7]),
        .Q(searched_read_reg_285[7]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[8]),
        .Q(searched_read_reg_285[8]),
        .R(1'b0));
  FDRE \searched_read_reg_285_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm17_out),
        .D(searched[9]),
        .Q(searched_read_reg_285[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h22222222222E2222)) 
    \tmp_1_reg_305[0]_i_1 
       (.I0(\tmp_1_reg_305_reg_n_4_[0] ),
        .I1(ce0),
        .I2(\invdar_reg_155_reg_n_4_[5] ),
        .I3(\invdar_reg_155_reg_n_4_[1] ),
        .I4(\invdar_reg_155_reg_n_4_[0] ),
        .I5(\tmp_1_reg_305[0]_i_2_n_4 ),
        .O(\tmp_1_reg_305[0]_i_1_n_4 ));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \tmp_1_reg_305[0]_i_2 
       (.I0(\invdar_reg_155_reg_n_4_[3] ),
        .I1(\invdar_reg_155_reg_n_4_[4] ),
        .I2(\invdar_reg_155_reg_n_4_[2] ),
        .I3(\invdar_reg_155_reg_n_4_[6] ),
        .O(\tmp_1_reg_305[0]_i_2_n_4 ));
  FDRE \tmp_1_reg_305_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_1_reg_305[0]_i_1_n_4 ),
        .Q(\tmp_1_reg_305_reg_n_4_[0] ),
        .R(1'b0));
  FDRE \tmp_6_reg_325_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(tmp_6_fu_253_p3),
        .Q(tmp_6_reg_325),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[0] ),
        .Q(tmp_reg_295[0]),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[1] ),
        .Q(tmp_reg_295[1]),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[2] ),
        .Q(tmp_reg_295[2]),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[3] ),
        .Q(tmp_reg_295[3]),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[4] ),
        .Q(tmp_reg_295[4]),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[5] ),
        .Q(tmp_reg_295[5]),
        .R(1'b0));
  FDRE \tmp_reg_295_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_155_reg_n_4_[6] ),
        .Q(tmp_reg_295[6]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb
   (DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4] ,
    INPUT_STREAM_V_data_V_0_sel,
    \ap_CS_fsm_reg[3] ,
    \invdar_reg_155_reg[6] ,
    ap_clk);
  output [4:0]DIADI;
  input [4:0]Q;
  input [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [1:0]\ap_CS_fsm_reg[3] ;
  input [6:0]\invdar_reg_155_reg[6] ;
  input ap_clk;

  wire [4:0]DIADI;
  wire [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire [4:0]Q;
  wire [1:0]\ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire [6:0]\invdar_reg_155_reg[6] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom Adder2_Adder2_strbkb_rom_U
       (.DIADI(DIADI),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[4] (\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .Q(Q),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\invdar_reg_155_reg[6] (\invdar_reg_155_reg[6] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom
   (DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4] ,
    INPUT_STREAM_V_data_V_0_sel,
    \ap_CS_fsm_reg[3] ,
    \invdar_reg_155_reg[6] ,
    ap_clk);
  output [4:0]DIADI;
  input [4:0]Q;
  input [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [1:0]\ap_CS_fsm_reg[3] ;
  input [6:0]\invdar_reg_155_reg[6] ;
  input ap_clk;

  wire [4:0]DIADI;
  wire [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire [4:0]Q;
  wire [1:0]\ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire g0_b0_n_4;
  wire g0_b1_n_4;
  wire g0_b2_n_4;
  wire g0_b3_n_4;
  wire g0_b4_n_4;
  wire g1_b1_n_4;
  wire g1_b2_n_4;
  wire g1_b4_n_4;
  wire [6:0]\invdar_reg_155_reg[6] ;
  wire [4:0]q0;
  wire \q0[0]_i_1_n_4 ;
  wire \q0[1]_i_1_n_4 ;
  wire \q0[3]_i_1_n_4 ;
  wire \q0_reg[2]_i_1_n_4 ;
  wire \q0_reg[4]_i_1_n_4 ;

  LUT6 #(
    .INIT(64'h552AA554AA9552AA)) 
    g0_b0
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g0_b0_n_4));
  LUT6 #(
    .INIT(64'h99B33666CCD99B33)) 
    g0_b1
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g0_b1_n_4));
  LUT6 #(
    .INIT(64'h1E43C8790F21E43C)) 
    g0_b2
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g0_b2_n_4));
  LUT6 #(
    .INIT(64'h1F83F07E0FC1F83F)) 
    g0_b3
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g0_b3_n_4));
  LUT6 #(
    .INIT(64'hE07C0F81F03E07C0)) 
    g0_b4
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g0_b4_n_4));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    g1_b1
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g1_b1_n_4));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    g1_b2
       (.I0(\invdar_reg_155_reg[6] [0]),
        .I1(\invdar_reg_155_reg[6] [1]),
        .I2(\invdar_reg_155_reg[6] [2]),
        .I3(\invdar_reg_155_reg[6] [3]),
        .I4(\invdar_reg_155_reg[6] [4]),
        .I5(\invdar_reg_155_reg[6] [5]),
        .O(g1_b2_n_4));
  LUT5 #(
    .INIT(32'h00000001)) 
    g1_b4
       (.I0(\invdar_reg_155_reg[6] [1]),
        .I1(\invdar_reg_155_reg[6] [2]),
        .I2(\invdar_reg_155_reg[6] [3]),
        .I3(\invdar_reg_155_reg[6] [4]),
        .I4(\invdar_reg_155_reg[6] [5]),
        .O(g1_b4_n_4));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[0]_i_1 
       (.I0(g1_b1_n_4),
        .I1(\invdar_reg_155_reg[6] [6]),
        .I2(g0_b0_n_4),
        .O(\q0[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[1]_i_1 
       (.I0(g1_b1_n_4),
        .I1(\invdar_reg_155_reg[6] [6]),
        .I2(g0_b1_n_4),
        .O(\q0[1]_i_1_n_4 ));
  LUT2 #(
    .INIT(4'h2)) 
    \q0[3]_i_1 
       (.I0(g0_b3_n_4),
        .I1(\invdar_reg_155_reg[6] [6]),
        .O(\q0[3]_i_1_n_4 ));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[0]_i_1_n_4 ),
        .Q(q0[0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[1]_i_1_n_4 ),
        .Q(q0[1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0_reg[2]_i_1_n_4 ),
        .Q(q0[2]),
        .R(1'b0));
  MUXF7 \q0_reg[2]_i_1 
       (.I0(g0_b2_n_4),
        .I1(g1_b2_n_4),
        .O(\q0_reg[2]_i_1_n_4 ),
        .S(\invdar_reg_155_reg[6] [6]));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[3]_i_1_n_4 ),
        .Q(q0[3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0_reg[4]_i_1_n_4 ),
        .Q(q0[4]),
        .R(1'b0));
  MUXF7 \q0_reg[4]_i_1 
       (.I0(g0_b4_n_4),
        .I1(g1_b4_n_4),
        .O(\q0_reg[4]_i_1_n_4 ),
        .S(\invdar_reg_155_reg[6] [6]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_20
       (.I0(q0[4]),
        .I1(Q[4]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [4]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[4]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_21
       (.I0(q0[3]),
        .I1(Q[3]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [3]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[3]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_22
       (.I0(q0[2]),
        .I1(Q[2]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [2]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[2]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_23
       (.I0(q0[1]),
        .I1(Q[1]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[1]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_24
       (.I0(q0[0]),
        .I1(Q[0]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [0]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (reset,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    \rdata_data_reg[31]_0 ,
    SR,
    D,
    E,
    interrupt,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_ARADDR,
    ap_rst_n,
    \tmp_1_reg_305_reg[0] ,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR,
    \p_s_reg_188_reg[6] );
  output reset;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [31:0]\rdata_data_reg[31]_0 ;
  output [0:0]SR;
  output [1:0]D;
  output [0:0]E;
  output interrupt;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [2:0]Q;
  input s_axi_CONTROL_BUS_RREADY;
  input s_axi_CONTROL_BUS_ARVALID;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  input ap_rst_n;
  input \tmp_1_reg_305_reg[0] ;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input [6:0]\p_s_reg_188_reg[6] ;

  wire \/FSM_onehot_wstate[1]_i_1_n_4 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_4 ;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_wstate[3]_i_1_n_4 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_4_[0] ;
  wire [2:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire int_ap_done;
  wire int_ap_done_i_1_n_4;
  wire int_ap_done_i_2_n_4;
  wire int_ap_done_i_3_n_4;
  wire int_ap_idle;
  wire int_ap_ready;
  wire [11:0]int_ap_return;
  wire int_ap_start3_out;
  wire int_ap_start_i_1_n_4;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_4;
  wire int_gie_i_1_n_4;
  wire int_gie_reg_n_4;
  wire \int_ier[0]_i_1_n_4 ;
  wire \int_ier[1]_i_1_n_4 ;
  wire \int_ier[1]_i_2_n_4 ;
  wire \int_ier_reg_n_4_[0] ;
  wire int_isr6_out;
  wire \int_isr[0]_i_1_n_4 ;
  wire \int_isr[1]_i_1_n_4 ;
  wire \int_isr_reg_n_4_[0] ;
  wire \int_searched[31]_i_3_n_4 ;
  wire interrupt;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in11_out;
  wire p_1_in;
  wire [6:0]\p_s_reg_188_reg[6] ;
  wire \rdata_data[0]_i_1_n_4 ;
  wire \rdata_data[0]_i_2_n_4 ;
  wire \rdata_data[10]_i_1_n_4 ;
  wire \rdata_data[11]_i_1_n_4 ;
  wire \rdata_data[11]_i_2_n_4 ;
  wire \rdata_data[11]_i_3_n_4 ;
  wire \rdata_data[1]_i_1_n_4 ;
  wire \rdata_data[1]_i_2_n_4 ;
  wire \rdata_data[2]_i_1_n_4 ;
  wire \rdata_data[31]_i_1_n_4 ;
  wire \rdata_data[3]_i_1_n_4 ;
  wire \rdata_data[4]_i_1_n_4 ;
  wire \rdata_data[5]_i_1_n_4 ;
  wire \rdata_data[5]_i_2_n_4 ;
  wire \rdata_data[6]_i_1_n_4 ;
  wire \rdata_data[7]_i_1_n_4 ;
  wire \rdata_data[7]_i_2_n_4 ;
  wire \rdata_data[8]_i_1_n_4 ;
  wire \rdata_data[9]_i_1_n_4 ;
  wire [31:0]\rdata_data_reg[31]_0 ;
  wire reset;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_4 ;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire \tmp_1_reg_305_reg[0] ;
  wire waddr;
  wire \waddr_reg_n_4_[0] ;
  wire \waddr_reg_n_4_[1] ;
  wire \waddr_reg_n_4_[2] ;
  wire \waddr_reg_n_4_[3] ;
  wire \waddr_reg_n_4_[4] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_4 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_4 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_4_[0] ),
        .S(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_4 ),
        .Q(out[0]),
        .R(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_4 ),
        .Q(out[1]),
        .R(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_4 ),
        .Q(out[2]),
        .R(reset));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(reset));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hF444)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(\tmp_1_reg_305_reg[0] ),
        .I1(Q[1]),
        .I2(ap_start),
        .I3(Q[0]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFFFDFFFFFFF0000)) 
    int_ap_done_i_1
       (.I0(ar_hs),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(int_ap_done_i_2_n_4),
        .I3(int_ap_done_i_3_n_4),
        .I4(Q[2]),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_4));
  LUT2 #(
    .INIT(4'h1)) 
    int_ap_done_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(int_ap_done_i_2_n_4));
  LUT2 #(
    .INIT(4'h1)) 
    int_ap_done_i_3
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(int_ap_done_i_3_n_4));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_4),
        .Q(int_ap_done),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(reset));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(int_ap_ready),
        .R(reset));
  FDRE \int_ap_return_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [0]),
        .Q(int_ap_return[0]),
        .R(reset));
  FDRE \int_ap_return_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [6]),
        .Q(int_ap_return[11]),
        .R(reset));
  FDRE \int_ap_return_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [1]),
        .Q(int_ap_return[1]),
        .R(reset));
  FDRE \int_ap_return_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [2]),
        .Q(int_ap_return[2]),
        .R(reset));
  FDRE \int_ap_return_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [3]),
        .Q(int_ap_return[3]),
        .R(reset));
  FDRE \int_ap_return_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [4]),
        .Q(int_ap_return[4]),
        .R(reset));
  FDRE \int_ap_return_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\p_s_reg_188_reg[6] [5]),
        .Q(int_ap_return[5]),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFBF8)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[2]),
        .I2(int_ap_start3_out),
        .I3(ap_start),
        .O(int_ap_start_i_1_n_4));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    int_ap_start_i_2
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\int_ier[1]_i_2_n_4 ),
        .I2(\waddr_reg_n_4_[2] ),
        .I3(\waddr_reg_n_4_[3] ),
        .O(int_ap_start3_out));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_4),
        .Q(ap_start),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFEFF0200)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(\waddr_reg_n_4_[3] ),
        .I2(\waddr_reg_n_4_[2] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_4),
        .Q(int_auto_restart),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_4_[3] ),
        .I2(\waddr_reg_n_4_[2] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(int_gie_reg_n_4),
        .O(int_gie_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_4),
        .Q(int_gie_reg_n_4),
        .R(reset));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_4_[2] ),
        .I2(\waddr_reg_n_4_[3] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(\int_ier_reg_n_4_[0] ),
        .O(\int_ier[0]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_4_[2] ),
        .I2(\waddr_reg_n_4_[3] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \int_ier[1]_i_2 
       (.I0(\waddr_reg_n_4_[4] ),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(s_axi_CONTROL_BUS_WVALID),
        .I3(out[1]),
        .I4(\waddr_reg_n_4_[0] ),
        .I5(\waddr_reg_n_4_[1] ),
        .O(\int_ier[1]_i_2_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_4 ),
        .Q(\int_ier_reg_n_4_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_4 ),
        .Q(p_0_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_isr6_out),
        .I2(\int_ier_reg_n_4_[0] ),
        .I3(Q[2]),
        .I4(\int_isr_reg_n_4_[0] ),
        .O(\int_isr[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \int_isr[0]_i_2 
       (.I0(\waddr_reg_n_4_[3] ),
        .I1(\waddr_reg_n_4_[2] ),
        .I2(\int_ier[1]_i_2_n_4 ),
        .O(int_isr6_out));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_isr6_out),
        .I2(p_0_in),
        .I3(Q[2]),
        .I4(p_1_in),
        .O(\int_isr[1]_i_1_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_4 ),
        .Q(\int_isr_reg_n_4_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_4 ),
        .Q(p_1_in),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [0]),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [10]),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [11]),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [12]),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [13]),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [14]),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [15]),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [16]),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [17]),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [18]),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [19]),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [1]),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [20]),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [21]),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [22]),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\rdata_data_reg[31]_0 [23]),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [24]),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [25]),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [26]),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [27]),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [28]),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [29]),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [2]),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [30]),
        .O(\or [30]));
  LUT4 #(
    .INIT(16'h4000)) 
    \int_searched[31]_i_1 
       (.I0(\waddr_reg_n_4_[2] ),
        .I1(\waddr_reg_n_4_[3] ),
        .I2(\waddr_reg_n_4_[4] ),
        .I3(\int_searched[31]_i_3_n_4 ),
        .O(p_0_in11_out));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\rdata_data_reg[31]_0 [31]),
        .O(\or [31]));
  LUT4 #(
    .INIT(16'h1000)) 
    \int_searched[31]_i_3 
       (.I0(\waddr_reg_n_4_[1] ),
        .I1(\waddr_reg_n_4_[0] ),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\int_searched[31]_i_3_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [3]),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [4]),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [5]),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [6]),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\rdata_data_reg[31]_0 [7]),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [8]),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\rdata_data_reg[31]_0 [9]),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [0]),
        .Q(\rdata_data_reg[31]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [10]),
        .Q(\rdata_data_reg[31]_0 [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [11]),
        .Q(\rdata_data_reg[31]_0 [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [12]),
        .Q(\rdata_data_reg[31]_0 [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [13]),
        .Q(\rdata_data_reg[31]_0 [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [14]),
        .Q(\rdata_data_reg[31]_0 [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [15]),
        .Q(\rdata_data_reg[31]_0 [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [16]),
        .Q(\rdata_data_reg[31]_0 [16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [17]),
        .Q(\rdata_data_reg[31]_0 [17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [18]),
        .Q(\rdata_data_reg[31]_0 [18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [19]),
        .Q(\rdata_data_reg[31]_0 [19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [1]),
        .Q(\rdata_data_reg[31]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [20]),
        .Q(\rdata_data_reg[31]_0 [20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [21]),
        .Q(\rdata_data_reg[31]_0 [21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [22]),
        .Q(\rdata_data_reg[31]_0 [22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [23]),
        .Q(\rdata_data_reg[31]_0 [23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [24]),
        .Q(\rdata_data_reg[31]_0 [24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [25]),
        .Q(\rdata_data_reg[31]_0 [25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [26]),
        .Q(\rdata_data_reg[31]_0 [26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [27]),
        .Q(\rdata_data_reg[31]_0 [27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [28]),
        .Q(\rdata_data_reg[31]_0 [28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [29]),
        .Q(\rdata_data_reg[31]_0 [29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [2]),
        .Q(\rdata_data_reg[31]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [30]),
        .Q(\rdata_data_reg[31]_0 [30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [31]),
        .Q(\rdata_data_reg[31]_0 [31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [3]),
        .Q(\rdata_data_reg[31]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [4]),
        .Q(\rdata_data_reg[31]_0 [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [5]),
        .Q(\rdata_data_reg[31]_0 [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [6]),
        .Q(\rdata_data_reg[31]_0 [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [7]),
        .Q(\rdata_data_reg[31]_0 [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [8]),
        .Q(\rdata_data_reg[31]_0 [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [9]),
        .Q(\rdata_data_reg[31]_0 [9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hE0)) 
    interrupt_INST_0
       (.I0(\int_isr_reg_n_4_[0] ),
        .I1(p_1_in),
        .I2(int_gie_reg_n_4),
        .O(interrupt));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hD000)) 
    \invdar_reg_155[6]_i_1 
       (.I0(Q[1]),
        .I1(\tmp_1_reg_305_reg[0] ),
        .I2(Q[0]),
        .I3(ap_start),
        .O(SR));
  LUT6 #(
    .INIT(64'h0000F0F0CCAAF0F0)) 
    \rdata_data[0]_i_1 
       (.I0(int_ap_return[0]),
        .I1(\rdata_data_reg[31]_0 [0]),
        .I2(\rdata_data[0]_i_2_n_4 ),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \rdata_data[0]_i_2 
       (.I0(\int_ier_reg_n_4_[0] ),
        .I1(\int_isr_reg_n_4_[0] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(ap_start),
        .I5(int_gie_reg_n_4),
        .O(\rdata_data[0]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'h0000004000000000)) 
    \rdata_data[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(\rdata_data_reg[31]_0 [10]),
        .O(\rdata_data[10]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \rdata_data[11]_i_1 
       (.I0(int_ap_return[11]),
        .I1(\rdata_data[11]_i_3_n_4 ),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .I3(rstate[0]),
        .I4(rstate[1]),
        .O(\rdata_data[11]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h0000004000000000)) 
    \rdata_data[11]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(\rdata_data_reg[31]_0 [11]),
        .O(\rdata_data[11]_i_2_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \rdata_data[11]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[11]_i_3_n_4 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF44400400)) 
    \rdata_data[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(int_ap_return[1]),
        .I4(\rdata_data_reg[31]_0 [1]),
        .I5(\rdata_data[1]_i_2_n_4 ),
        .O(\rdata_data[1]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h4055400540504000)) 
    \rdata_data[1]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(p_1_in),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(p_0_in),
        .I5(int_ap_done),
        .O(\rdata_data[1]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'h0000CA0F0000CA00)) 
    \rdata_data[2]_i_1 
       (.I0(int_ap_return[2]),
        .I1(\rdata_data_reg[31]_0 [2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .I5(int_ap_idle),
        .O(\rdata_data[2]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAA8AAA)) 
    \rdata_data[31]_i_1 
       (.I0(ar_hs),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .I5(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[31]_i_1_n_4 ));
  LUT3 #(
    .INIT(8'h10)) 
    \rdata_data[31]_i_2 
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .O(ar_hs));
  LUT6 #(
    .INIT(64'h0000CA0F0000CA00)) 
    \rdata_data[3]_i_1 
       (.I0(int_ap_return[3]),
        .I1(\rdata_data_reg[31]_0 [3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .I5(int_ap_ready),
        .O(\rdata_data[3]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'hF888FFFFF8880000)) 
    \rdata_data[4]_i_1 
       (.I0(\rdata_data_reg[31]_0 [4]),
        .I1(\rdata_data[5]_i_2_n_4 ),
        .I2(int_ap_return[4]),
        .I3(\rdata_data[11]_i_3_n_4 ),
        .I4(ar_hs),
        .I5(s_axi_CONTROL_BUS_RDATA[4]),
        .O(\rdata_data[4]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'hF888FFFFF8880000)) 
    \rdata_data[5]_i_1 
       (.I0(\rdata_data_reg[31]_0 [5]),
        .I1(\rdata_data[5]_i_2_n_4 ),
        .I2(int_ap_return[5]),
        .I3(\rdata_data[11]_i_3_n_4 ),
        .I4(ar_hs),
        .I5(s_axi_CONTROL_BUS_RDATA[5]),
        .O(\rdata_data[5]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00001000)) 
    \rdata_data[5]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[5]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'h0000004000000000)) 
    \rdata_data[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(\rdata_data_reg[31]_0 [6]),
        .O(\rdata_data[6]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h02020200)) 
    \rdata_data[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(rstate[0]),
        .I2(rstate[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[7]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h0000CA0F0000CA00)) 
    \rdata_data[7]_i_2 
       (.I0(int_ap_return[11]),
        .I1(\rdata_data_reg[31]_0 [7]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .I5(int_auto_restart),
        .O(\rdata_data[7]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'h0000004000000000)) 
    \rdata_data[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(\rdata_data_reg[31]_0 [8]),
        .O(\rdata_data[8]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h0000004000000000)) 
    \rdata_data[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(\rdata_data_reg[31]_0 [9]),
        .O(\rdata_data[9]_i_1_n_4 ));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[0]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(\rdata_data[7]_i_1_n_4 ));
  FDSE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[10]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .S(\rdata_data[11]_i_1_n_4 ));
  FDSE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[11]_i_2_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .S(\rdata_data[11]_i_1_n_4 ));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [12]),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [13]),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [14]),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [15]),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [16]),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [17]),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [18]),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [19]),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[1]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(\rdata_data[7]_i_1_n_4 ));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [20]),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [21]),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [22]),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [23]),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [24]),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [25]),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [26]),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [27]),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [28]),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [29]),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[2]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(\rdata_data[7]_i_1_n_4 ));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [30]),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data_reg[31]_0 [31]),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[3]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(\rdata_data[7]_i_1_n_4 ));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rdata_data[4]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(1'b0));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rdata_data[5]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(1'b0));
  FDSE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[6]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .S(\rdata_data[11]_i_1_n_4 ));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[7]_i_2_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(\rdata_data[7]_i_1_n_4 ));
  FDSE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[8]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .S(\rdata_data[11]_i_1_n_4 ));
  FDSE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[9]_i_1_n_4 ),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .S(\rdata_data[11]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h005C)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_RREADY),
        .I1(s_axi_CONTROL_BUS_ARVALID),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rstate[0]_i_1_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_4 ),
        .Q(rstate[0]),
        .R(reset));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(reset));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \searched_read_reg_285[31]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .O(E));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[4]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_4_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_4_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_4_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_4_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_4_[4] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input
   (CO,
    ap_clk,
    DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_166_reg[5] ,
    \tmp_reg_295_reg[6] ,
    \i_1_reg_177_reg[5] ,
    \searched_read_reg_285_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [0:0]CO;
  input ap_clk;
  input [4:0]DIADI;
  input [2:0]Q;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]\i_reg_166_reg[5] ;
  input [6:0]\tmp_reg_295_reg[6] ;
  input [5:0]\i_1_reg_177_reg[5] ;
  input [31:0]\searched_read_reg_285_reg[31] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [0:0]CO;
  wire [4:0]DIADI;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire ap_clk;
  wire [5:0]\i_1_reg_177_reg[5] ;
  wire [5:0]\i_reg_166_reg[5] ;
  wire [31:0]\searched_read_reg_285_reg[31] ;
  wire [6:0]\tmp_reg_295_reg[6] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram Adder2_input_ram_U
       (.CO(CO),
        .DIADI(DIADI),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .ap_clk(ap_clk),
        .\i_1_reg_177_reg[5] (\i_1_reg_177_reg[5] ),
        .\i_reg_166_reg[5] (\i_reg_166_reg[5] ),
        .\searched_read_reg_285_reg[31] (\searched_read_reg_285_reg[31] ),
        .\tmp_reg_295_reg[6] (\tmp_reg_295_reg[6] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram
   (CO,
    ap_clk,
    DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_166_reg[5] ,
    \tmp_reg_295_reg[6] ,
    \i_1_reg_177_reg[5] ,
    \searched_read_reg_285_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [0:0]CO;
  input ap_clk;
  input [4:0]DIADI;
  input [2:0]Q;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]\i_reg_166_reg[5] ;
  input [6:0]\tmp_reg_295_reg[6] ;
  input [5:0]\i_1_reg_177_reg[5] ;
  input [31:0]\searched_read_reg_285_reg[31] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [0:0]CO;
  wire [4:0]DIADI;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [6:0]address0;
  wire \ap_CS_fsm[7]_i_10_n_4 ;
  wire \ap_CS_fsm[7]_i_11_n_4 ;
  wire \ap_CS_fsm[7]_i_12_n_4 ;
  wire \ap_CS_fsm[7]_i_13_n_4 ;
  wire \ap_CS_fsm[7]_i_14_n_4 ;
  wire \ap_CS_fsm[7]_i_15_n_4 ;
  wire \ap_CS_fsm[7]_i_4_n_4 ;
  wire \ap_CS_fsm[7]_i_5_n_4 ;
  wire \ap_CS_fsm[7]_i_6_n_4 ;
  wire \ap_CS_fsm[7]_i_8_n_4 ;
  wire \ap_CS_fsm[7]_i_9_n_4 ;
  wire \ap_CS_fsm_reg[7]_i_2_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_2_n_7 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_4 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_7 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_4 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_7 ;
  wire ap_clk;
  wire [31:5]d0;
  wire [5:0]\i_1_reg_177_reg[5] ;
  wire [5:0]\i_reg_166_reg[5] ;
  wire [31:0]input_q0;
  wire ram_reg_i_1_n_4;
  wire [31:0]\searched_read_reg_285_reg[31] ;
  wire [6:0]\tmp_reg_295_reg[6] ;
  wire we0;
  wire [3:3]\NLW_ap_CS_fsm_reg[7]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_7_O_UNCONNECTED ;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_10 
       (.I0(input_q0[16]),
        .I1(\searched_read_reg_285_reg[31] [16]),
        .I2(input_q0[15]),
        .I3(\searched_read_reg_285_reg[31] [15]),
        .I4(\searched_read_reg_285_reg[31] [17]),
        .I5(input_q0[17]),
        .O(\ap_CS_fsm[7]_i_10_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_11 
       (.I0(input_q0[13]),
        .I1(\searched_read_reg_285_reg[31] [13]),
        .I2(input_q0[12]),
        .I3(\searched_read_reg_285_reg[31] [12]),
        .I4(\searched_read_reg_285_reg[31] [14]),
        .I5(input_q0[14]),
        .O(\ap_CS_fsm[7]_i_11_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_12 
       (.I0(input_q0[10]),
        .I1(\searched_read_reg_285_reg[31] [10]),
        .I2(input_q0[9]),
        .I3(\searched_read_reg_285_reg[31] [9]),
        .I4(\searched_read_reg_285_reg[31] [11]),
        .I5(input_q0[11]),
        .O(\ap_CS_fsm[7]_i_12_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_13 
       (.I0(input_q0[7]),
        .I1(\searched_read_reg_285_reg[31] [7]),
        .I2(input_q0[6]),
        .I3(\searched_read_reg_285_reg[31] [6]),
        .I4(\searched_read_reg_285_reg[31] [8]),
        .I5(input_q0[8]),
        .O(\ap_CS_fsm[7]_i_13_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_14 
       (.I0(input_q0[4]),
        .I1(\searched_read_reg_285_reg[31] [4]),
        .I2(input_q0[3]),
        .I3(\searched_read_reg_285_reg[31] [3]),
        .I4(\searched_read_reg_285_reg[31] [5]),
        .I5(input_q0[5]),
        .O(\ap_CS_fsm[7]_i_14_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_15 
       (.I0(input_q0[1]),
        .I1(\searched_read_reg_285_reg[31] [1]),
        .I2(input_q0[0]),
        .I3(\searched_read_reg_285_reg[31] [0]),
        .I4(\searched_read_reg_285_reg[31] [2]),
        .I5(input_q0[2]),
        .O(\ap_CS_fsm[7]_i_15_n_4 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ap_CS_fsm[7]_i_4 
       (.I0(input_q0[30]),
        .I1(\searched_read_reg_285_reg[31] [30]),
        .I2(\searched_read_reg_285_reg[31] [31]),
        .I3(input_q0[31]),
        .O(\ap_CS_fsm[7]_i_4_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_5 
       (.I0(input_q0[28]),
        .I1(\searched_read_reg_285_reg[31] [28]),
        .I2(input_q0[27]),
        .I3(\searched_read_reg_285_reg[31] [27]),
        .I4(\searched_read_reg_285_reg[31] [29]),
        .I5(input_q0[29]),
        .O(\ap_CS_fsm[7]_i_5_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_6 
       (.I0(input_q0[25]),
        .I1(\searched_read_reg_285_reg[31] [25]),
        .I2(input_q0[24]),
        .I3(\searched_read_reg_285_reg[31] [24]),
        .I4(\searched_read_reg_285_reg[31] [26]),
        .I5(input_q0[26]),
        .O(\ap_CS_fsm[7]_i_6_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_8 
       (.I0(input_q0[22]),
        .I1(\searched_read_reg_285_reg[31] [22]),
        .I2(input_q0[21]),
        .I3(\searched_read_reg_285_reg[31] [21]),
        .I4(\searched_read_reg_285_reg[31] [23]),
        .I5(input_q0[23]),
        .O(\ap_CS_fsm[7]_i_8_n_4 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_9 
       (.I0(input_q0[19]),
        .I1(\searched_read_reg_285_reg[31] [19]),
        .I2(input_q0[18]),
        .I3(\searched_read_reg_285_reg[31] [18]),
        .I4(\searched_read_reg_285_reg[31] [20]),
        .I5(input_q0[20]),
        .O(\ap_CS_fsm[7]_i_9_n_4 ));
  CARRY4 \ap_CS_fsm_reg[7]_i_2 
       (.CI(\ap_CS_fsm_reg[7]_i_3_n_4 ),
        .CO({\NLW_ap_CS_fsm_reg[7]_i_2_CO_UNCONNECTED [3],CO,\ap_CS_fsm_reg[7]_i_2_n_6 ,\ap_CS_fsm_reg[7]_i_2_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,\ap_CS_fsm[7]_i_4_n_4 ,\ap_CS_fsm[7]_i_5_n_4 ,\ap_CS_fsm[7]_i_6_n_4 }));
  CARRY4 \ap_CS_fsm_reg[7]_i_3 
       (.CI(\ap_CS_fsm_reg[7]_i_7_n_4 ),
        .CO({\ap_CS_fsm_reg[7]_i_3_n_4 ,\ap_CS_fsm_reg[7]_i_3_n_5 ,\ap_CS_fsm_reg[7]_i_3_n_6 ,\ap_CS_fsm_reg[7]_i_3_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_3_O_UNCONNECTED [3:0]),
        .S({\ap_CS_fsm[7]_i_8_n_4 ,\ap_CS_fsm[7]_i_9_n_4 ,\ap_CS_fsm[7]_i_10_n_4 ,\ap_CS_fsm[7]_i_11_n_4 }));
  CARRY4 \ap_CS_fsm_reg[7]_i_7 
       (.CI(1'b0),
        .CO({\ap_CS_fsm_reg[7]_i_7_n_4 ,\ap_CS_fsm_reg[7]_i_7_n_5 ,\ap_CS_fsm_reg[7]_i_7_n_6 ,\ap_CS_fsm_reg[7]_i_7_n_7 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_7_O_UNCONNECTED [3:0]),
        .S({\ap_CS_fsm[7]_i_12_n_4 ,\ap_CS_fsm[7]_i_13_n_4 ,\ap_CS_fsm[7]_i_14_n_4 ,\ap_CS_fsm[7]_i_15_n_4 }));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "2112" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "65" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({d0[15:5],DIADI}),
        .DIBDI({1'b1,1'b1,d0[31:18]}),
        .DIPADIP(d0[17:16]),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(input_q0[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],input_q0[31:18]}),
        .DOPADOP(input_q0[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ram_reg_i_1_n_4),
        .ENBWREN(ram_reg_i_1_n_4),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({we0,we0}),
        .WEBWE({1'b0,1'b0,we0,we0}));
  LUT5 #(
    .INIT(32'hFEFEFEEE)) 
    ram_reg_i_1
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I4(\i_reg_166_reg[5] [5]),
        .O(ram_reg_i_1_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_10
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[14]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_11
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[13]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_12
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[12]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_13
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[11]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_14
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[10]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_15
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[9]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_16
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[8]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_17
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[7]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_18
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[6]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_19
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[5]));
  LUT3 #(
    .INIT(8'h10)) 
    ram_reg_i_2
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(\tmp_reg_295_reg[6] [6]),
        .O(address0[6]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_25
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[31]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_26
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[30]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_27
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[29]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_28
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[28]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_29
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[27]));
  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    ram_reg_i_3
       (.I0(Q[1]),
        .I1(\tmp_reg_295_reg[6] [5]),
        .I2(\i_reg_166_reg[5] [5]),
        .I3(\i_1_reg_177_reg[5] [5]),
        .I4(Q[2]),
        .O(address0[5]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_30
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[26]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_31
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[25]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_32
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[24]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_33
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[23]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_34
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[22]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_35
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[21]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_36
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[20]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_37
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[19]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_38
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[18]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_39
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[17]));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    ram_reg_i_4
       (.I0(Q[1]),
        .I1(\tmp_reg_295_reg[6] [4]),
        .I2(\i_reg_166_reg[5] [4]),
        .I3(Q[2]),
        .I4(\i_1_reg_177_reg[5] [4]),
        .O(address0[4]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_40
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[16]));
  LUT4 #(
    .INIT(16'hBAAA)) 
    ram_reg_i_41
       (.I0(Q[0]),
        .I1(\i_reg_166_reg[5] [5]),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I3(Q[1]),
        .O(we0));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    ram_reg_i_5
       (.I0(Q[1]),
        .I1(\tmp_reg_295_reg[6] [3]),
        .I2(\i_reg_166_reg[5] [3]),
        .I3(Q[2]),
        .I4(\i_1_reg_177_reg[5] [3]),
        .O(address0[3]));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    ram_reg_i_6
       (.I0(Q[1]),
        .I1(\tmp_reg_295_reg[6] [2]),
        .I2(\i_reg_166_reg[5] [2]),
        .I3(Q[2]),
        .I4(\i_1_reg_177_reg[5] [2]),
        .O(address0[2]));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    ram_reg_i_7
       (.I0(Q[1]),
        .I1(\tmp_reg_295_reg[6] [1]),
        .I2(\i_reg_166_reg[5] [1]),
        .I3(Q[2]),
        .I4(\i_1_reg_177_reg[5] [1]),
        .O(address0[1]));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    ram_reg_i_8
       (.I0(Q[1]),
        .I1(\tmp_reg_295_reg[6] [0]),
        .I2(\i_reg_166_reg[5] [0]),
        .I3(Q[2]),
        .I4(\i_1_reg_177_reg[5] [0]),
        .O(address0[0]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_9
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[15]));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [4:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
