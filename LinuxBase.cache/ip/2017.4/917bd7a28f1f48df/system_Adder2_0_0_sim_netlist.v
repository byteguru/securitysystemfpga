// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Thu Feb 22 21:24:56 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire ARESET;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2 ;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ;
  wire \ap_CS_fsm[1]_i_2_n_2 ;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state2;
  wire [2:0]ap_NS_fsm;
  wire ap_NS_fsm10_out;
  wire ap_NS_fsm11_out;
  wire ap_clk;
  wire ap_done;
  wire ap_rst_n;
  wire [5:0]i_1_fu_115_p2;
  wire i_reg_96;
  wire \i_reg_96_reg_n_2_[0] ;
  wire \i_reg_96_reg_n_2_[1] ;
  wire \i_reg_96_reg_n_2_[2] ;
  wire \i_reg_96_reg_n_2_[3] ;
  wire \i_reg_96_reg_n_2_[4] ;
  wire interrupt;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]searched;
  wire [31:0]searched_read_reg_125;
  wire tmp_fu_107_p3;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.ARESET(ARESET),
        .D(ap_NS_fsm[1:0]),
        .E(ap_NS_fsm10_out),
        .\INPUT_STREAM_V_last_V_0_payload_B_reg[0] (\ap_CS_fsm[1]_i_2_n_2 ),
        .Q({ap_done,ap_CS_fsm_state2,\ap_CS_fsm_reg_n_2_[0] }),
        .SR(i_reg_96),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .\searched_read_reg_125_reg[0] (ap_NS_fsm11_out),
        .\searched_read_reg_125_reg[31] (searched),
        .\searched_read_reg_125_reg[31]_0 (searched_read_reg_125));
  GND GND
       (.G(\<const0> ));
  LUT6 #(
    .INIT(64'hAA80AA808A80AA80)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TVALID),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I4(ap_CS_fsm_state2),
        .I5(tmp_fu_107_p3),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h4FFF4F4F)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(tmp_fu_107_p3),
        .I1(ap_CS_fsm_state2),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I3(INPUT_STREAM_TVALID),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ),
        .R(ARESET));
  LUT5 #(
    .INIT(32'h8A80AA80)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_TREADY),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ),
        .I4(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h55D5FFFF55D555D5)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I2(ap_CS_fsm_state2),
        .I3(tmp_fu_107_p3),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_TREADY),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I2(ap_CS_fsm_state2),
        .I3(tmp_fu_107_p3),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hA088A888)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_V_last_V_0_ack_in),
        .I4(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_2 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I1(ap_CS_fsm_state2),
        .I2(tmp_fu_107_p3),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'h55D5FFFF55D555D5)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I2(ap_CS_fsm_state2),
        .I3(tmp_fu_107_p3),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_last_V_0_ack_in),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'h000047FF)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I4(tmp_fu_107_p3),
        .O(\ap_CS_fsm[1]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'hA8A8A8888888A888)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_107_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I3(INPUT_STREAM_V_last_V_0_payload_A),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .I5(INPUT_STREAM_V_last_V_0_payload_B),
        .O(ap_NS_fsm[2]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_done),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_96[0]_i_1 
       (.I0(\i_reg_96_reg_n_2_[0] ),
        .O(i_1_fu_115_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_reg_96[1]_i_1 
       (.I0(\i_reg_96_reg_n_2_[1] ),
        .I1(\i_reg_96_reg_n_2_[0] ),
        .O(i_1_fu_115_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_reg_96[2]_i_1 
       (.I0(\i_reg_96_reg_n_2_[2] ),
        .I1(\i_reg_96_reg_n_2_[1] ),
        .I2(\i_reg_96_reg_n_2_[0] ),
        .O(i_1_fu_115_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_reg_96[3]_i_1 
       (.I0(\i_reg_96_reg_n_2_[0] ),
        .I1(\i_reg_96_reg_n_2_[1] ),
        .I2(\i_reg_96_reg_n_2_[2] ),
        .I3(\i_reg_96_reg_n_2_[3] ),
        .O(i_1_fu_115_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_reg_96[4]_i_1 
       (.I0(\i_reg_96_reg_n_2_[4] ),
        .I1(\i_reg_96_reg_n_2_[0] ),
        .I2(\i_reg_96_reg_n_2_[1] ),
        .I3(\i_reg_96_reg_n_2_[2] ),
        .I4(\i_reg_96_reg_n_2_[3] ),
        .O(i_1_fu_115_p2[4]));
  LUT6 #(
    .INIT(64'h0000004040400040)) 
    \i_reg_96[5]_i_2 
       (.I0(tmp_fu_107_p3),
        .I1(ap_CS_fsm_state2),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I3(INPUT_STREAM_V_last_V_0_payload_A),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .I5(INPUT_STREAM_V_last_V_0_payload_B),
        .O(ap_NS_fsm10_out));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_reg_96[5]_i_3 
       (.I0(\i_reg_96_reg_n_2_[3] ),
        .I1(\i_reg_96_reg_n_2_[2] ),
        .I2(\i_reg_96_reg_n_2_[1] ),
        .I3(\i_reg_96_reg_n_2_[0] ),
        .I4(\i_reg_96_reg_n_2_[4] ),
        .O(i_1_fu_115_p2[5]));
  FDRE \i_reg_96_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_115_p2[0]),
        .Q(\i_reg_96_reg_n_2_[0] ),
        .R(i_reg_96));
  FDRE \i_reg_96_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_115_p2[1]),
        .Q(\i_reg_96_reg_n_2_[1] ),
        .R(i_reg_96));
  FDRE \i_reg_96_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_115_p2[2]),
        .Q(\i_reg_96_reg_n_2_[2] ),
        .R(i_reg_96));
  FDRE \i_reg_96_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_115_p2[3]),
        .Q(\i_reg_96_reg_n_2_[3] ),
        .R(i_reg_96));
  FDRE \i_reg_96_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_115_p2[4]),
        .Q(\i_reg_96_reg_n_2_[4] ),
        .R(i_reg_96));
  FDRE \i_reg_96_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_115_p2[5]),
        .Q(tmp_fu_107_p3),
        .R(i_reg_96));
  FDRE \searched_read_reg_125_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[0]),
        .Q(searched_read_reg_125[0]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[10]),
        .Q(searched_read_reg_125[10]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[11]),
        .Q(searched_read_reg_125[11]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[12]),
        .Q(searched_read_reg_125[12]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[13]),
        .Q(searched_read_reg_125[13]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[14]),
        .Q(searched_read_reg_125[14]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[15]),
        .Q(searched_read_reg_125[15]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[16]),
        .Q(searched_read_reg_125[16]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[17]),
        .Q(searched_read_reg_125[17]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[18]),
        .Q(searched_read_reg_125[18]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[19]),
        .Q(searched_read_reg_125[19]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[1]),
        .Q(searched_read_reg_125[1]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[20]),
        .Q(searched_read_reg_125[20]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[21]),
        .Q(searched_read_reg_125[21]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[22]),
        .Q(searched_read_reg_125[22]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[23]),
        .Q(searched_read_reg_125[23]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[24] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[24]),
        .Q(searched_read_reg_125[24]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[25] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[25]),
        .Q(searched_read_reg_125[25]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[26] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[26]),
        .Q(searched_read_reg_125[26]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[27] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[27]),
        .Q(searched_read_reg_125[27]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[28] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[28]),
        .Q(searched_read_reg_125[28]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[29] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[29]),
        .Q(searched_read_reg_125[29]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[2]),
        .Q(searched_read_reg_125[2]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[30] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[30]),
        .Q(searched_read_reg_125[30]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[31] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[31]),
        .Q(searched_read_reg_125[31]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[3]),
        .Q(searched_read_reg_125[3]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[4]),
        .Q(searched_read_reg_125[4]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[5]),
        .Q(searched_read_reg_125[5]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[6]),
        .Q(searched_read_reg_125[6]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[7]),
        .Q(searched_read_reg_125[7]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[8]),
        .Q(searched_read_reg_125[8]),
        .R(1'b0));
  FDRE \searched_read_reg_125_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(searched[9]),
        .Q(searched_read_reg_125[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (ARESET,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    \searched_read_reg_125_reg[31] ,
    SR,
    D,
    \searched_read_reg_125_reg[0] ,
    interrupt,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_ARADDR,
    ap_rst_n,
    E,
    \INPUT_STREAM_V_last_V_0_payload_B_reg[0] ,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR,
    \searched_read_reg_125_reg[31]_0 );
  output ARESET;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [31:0]\searched_read_reg_125_reg[31] ;
  output [0:0]SR;
  output [1:0]D;
  output [0:0]\searched_read_reg_125_reg[0] ;
  output interrupt;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [2:0]Q;
  input s_axi_CONTROL_BUS_ARVALID;
  input s_axi_CONTROL_BUS_RREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  input ap_rst_n;
  input [0:0]E;
  input \INPUT_STREAM_V_last_V_0_payload_B_reg[0] ;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input [31:0]\searched_read_reg_125_reg[31]_0 ;

  wire \/FSM_onehot_wstate[1]_i_1_n_2 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_2 ;
  wire ARESET;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_wstate[3]_i_1_n_2 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_2_[0] ;
  wire \INPUT_STREAM_V_last_V_0_payload_B_reg[0] ;
  wire [2:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire int_ap_done;
  wire int_ap_done_i_1_n_2;
  wire int_ap_idle;
  wire int_ap_ready;
  wire [31:0]int_ap_return;
  wire int_ap_start3_out;
  wire int_ap_start_i_1_n_2;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_2;
  wire int_gie_i_1_n_2;
  wire int_gie_reg_n_2;
  wire \int_ier[0]_i_1_n_2 ;
  wire \int_ier[1]_i_1_n_2 ;
  wire \int_ier[1]_i_2_n_2 ;
  wire \int_ier_reg_n_2_[0] ;
  wire int_isr6_out;
  wire \int_isr[0]_i_1_n_2 ;
  wire \int_isr[1]_i_1_n_2 ;
  wire \int_isr_reg_n_2_[0] ;
  wire \int_searched[31]_i_3_n_2 ;
  wire interrupt;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in11_out;
  wire p_1_in;
  wire [31:0]rdata_data;
  wire \rdata_data[0]_i_2_n_2 ;
  wire \rdata_data[0]_i_3_n_2 ;
  wire \rdata_data[1]_i_2_n_2 ;
  wire \rdata_data[1]_i_3_n_2 ;
  wire \rdata_data[1]_i_4_n_2 ;
  wire \rdata_data[31]_i_3_n_2 ;
  wire \rdata_data[31]_i_4_n_2 ;
  wire \rdata_data[7]_i_2_n_2 ;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_2 ;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [0:0]\searched_read_reg_125_reg[0] ;
  wire [31:0]\searched_read_reg_125_reg[31] ;
  wire [31:0]\searched_read_reg_125_reg[31]_0 ;
  wire waddr;
  wire \waddr_reg_n_2_[0] ;
  wire \waddr_reg_n_2_[1] ;
  wire \waddr_reg_n_2_[2] ;
  wire \waddr_reg_n_2_[3] ;
  wire \waddr_reg_n_2_[4] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_2 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_2 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_2_[0] ),
        .S(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_2 ),
        .Q(out[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_2 ),
        .Q(out[1]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_2 ),
        .Q(out[2]),
        .R(ARESET));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h4555)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(ap_start),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFC88)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(\INPUT_STREAM_V_last_V_0_payload_B_reg[0] ),
        .I3(Q[1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \i_reg_96[5]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(E),
        .O(SR));
  LUT6 #(
    .INIT(64'hFFFFFFEFAAAAAAAA)) 
    int_ap_done_i_1
       (.I0(Q[2]),
        .I1(\rdata_data[7]_i_2_n_2 ),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .I3(rstate[0]),
        .I4(rstate[1]),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_2),
        .Q(int_ap_done),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(ARESET));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(int_ap_ready),
        .R(ARESET));
  FDRE \int_ap_return_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [0]),
        .Q(int_ap_return[0]),
        .R(ARESET));
  FDRE \int_ap_return_reg[10] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [10]),
        .Q(int_ap_return[10]),
        .R(ARESET));
  FDRE \int_ap_return_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [11]),
        .Q(int_ap_return[11]),
        .R(ARESET));
  FDRE \int_ap_return_reg[12] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [12]),
        .Q(int_ap_return[12]),
        .R(ARESET));
  FDRE \int_ap_return_reg[13] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [13]),
        .Q(int_ap_return[13]),
        .R(ARESET));
  FDRE \int_ap_return_reg[14] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [14]),
        .Q(int_ap_return[14]),
        .R(ARESET));
  FDRE \int_ap_return_reg[15] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [15]),
        .Q(int_ap_return[15]),
        .R(ARESET));
  FDRE \int_ap_return_reg[16] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [16]),
        .Q(int_ap_return[16]),
        .R(ARESET));
  FDRE \int_ap_return_reg[17] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [17]),
        .Q(int_ap_return[17]),
        .R(ARESET));
  FDRE \int_ap_return_reg[18] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [18]),
        .Q(int_ap_return[18]),
        .R(ARESET));
  FDRE \int_ap_return_reg[19] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [19]),
        .Q(int_ap_return[19]),
        .R(ARESET));
  FDRE \int_ap_return_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [1]),
        .Q(int_ap_return[1]),
        .R(ARESET));
  FDRE \int_ap_return_reg[20] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [20]),
        .Q(int_ap_return[20]),
        .R(ARESET));
  FDRE \int_ap_return_reg[21] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [21]),
        .Q(int_ap_return[21]),
        .R(ARESET));
  FDRE \int_ap_return_reg[22] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [22]),
        .Q(int_ap_return[22]),
        .R(ARESET));
  FDRE \int_ap_return_reg[23] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [23]),
        .Q(int_ap_return[23]),
        .R(ARESET));
  FDRE \int_ap_return_reg[24] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [24]),
        .Q(int_ap_return[24]),
        .R(ARESET));
  FDRE \int_ap_return_reg[25] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [25]),
        .Q(int_ap_return[25]),
        .R(ARESET));
  FDRE \int_ap_return_reg[26] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [26]),
        .Q(int_ap_return[26]),
        .R(ARESET));
  FDRE \int_ap_return_reg[27] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [27]),
        .Q(int_ap_return[27]),
        .R(ARESET));
  FDRE \int_ap_return_reg[28] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [28]),
        .Q(int_ap_return[28]),
        .R(ARESET));
  FDRE \int_ap_return_reg[29] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [29]),
        .Q(int_ap_return[29]),
        .R(ARESET));
  FDRE \int_ap_return_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [2]),
        .Q(int_ap_return[2]),
        .R(ARESET));
  FDRE \int_ap_return_reg[30] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [30]),
        .Q(int_ap_return[30]),
        .R(ARESET));
  FDRE \int_ap_return_reg[31] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [31]),
        .Q(int_ap_return[31]),
        .R(ARESET));
  FDRE \int_ap_return_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [3]),
        .Q(int_ap_return[3]),
        .R(ARESET));
  FDRE \int_ap_return_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [4]),
        .Q(int_ap_return[4]),
        .R(ARESET));
  FDRE \int_ap_return_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [5]),
        .Q(int_ap_return[5]),
        .R(ARESET));
  FDRE \int_ap_return_reg[6] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [6]),
        .Q(int_ap_return[6]),
        .R(ARESET));
  FDRE \int_ap_return_reg[7] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [7]),
        .Q(int_ap_return[7]),
        .R(ARESET));
  FDRE \int_ap_return_reg[8] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [8]),
        .Q(int_ap_return[8]),
        .R(ARESET));
  FDRE \int_ap_return_reg[9] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_125_reg[31]_0 [9]),
        .Q(int_ap_return[9]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFBF8)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[2]),
        .I2(int_ap_start3_out),
        .I3(ap_start),
        .O(int_ap_start_i_1_n_2));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    int_ap_start_i_2
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_2_[2] ),
        .I2(\int_ier[1]_i_2_n_2 ),
        .I3(\waddr_reg_n_2_[3] ),
        .O(int_ap_start3_out));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_2),
        .Q(ap_start),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFFE0002)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(\waddr_reg_n_2_[3] ),
        .I2(\int_ier[1]_i_2_n_2 ),
        .I3(\waddr_reg_n_2_[2] ),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_2),
        .Q(int_auto_restart),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_2_[3] ),
        .I2(\waddr_reg_n_2_[2] ),
        .I3(\int_ier[1]_i_2_n_2 ),
        .I4(int_gie_reg_n_2),
        .O(int_gie_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_2),
        .Q(int_gie_reg_n_2),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_2_[3] ),
        .I2(\int_ier[1]_i_2_n_2 ),
        .I3(\waddr_reg_n_2_[2] ),
        .I4(\int_ier_reg_n_2_[0] ),
        .O(\int_ier[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_2_[3] ),
        .I2(\int_ier[1]_i_2_n_2 ),
        .I3(\waddr_reg_n_2_[2] ),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFDFFFFFF)) 
    \int_ier[1]_i_2 
       (.I0(s_axi_CONTROL_BUS_WSTRB[0]),
        .I1(\waddr_reg_n_2_[4] ),
        .I2(\waddr_reg_n_2_[1] ),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[1]),
        .I5(\waddr_reg_n_2_[0] ),
        .O(\int_ier[1]_i_2_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_2 ),
        .Q(\int_ier_reg_n_2_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_2 ),
        .Q(p_0_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_isr6_out),
        .I2(\int_ier_reg_n_2_[0] ),
        .I3(Q[2]),
        .I4(\int_isr_reg_n_2_[0] ),
        .O(\int_isr[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \int_isr[0]_i_2 
       (.I0(\waddr_reg_n_2_[3] ),
        .I1(\waddr_reg_n_2_[2] ),
        .I2(\int_ier[1]_i_2_n_2 ),
        .O(int_isr6_out));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_isr6_out),
        .I2(p_0_in),
        .I3(Q[2]),
        .I4(p_1_in),
        .O(\int_isr[1]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_2 ),
        .Q(\int_isr_reg_n_2_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_2 ),
        .Q(p_1_in),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [0]),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [10]),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [11]),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [12]),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [13]),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [14]),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [15]),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [16]),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [17]),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [18]),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [19]),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [1]),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [20]),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [21]),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [22]),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_125_reg[31] [23]),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [24]),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [25]),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [26]),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [27]),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [28]),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [29]),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [2]),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [30]),
        .O(\or [30]));
  LUT4 #(
    .INIT(16'h0800)) 
    \int_searched[31]_i_1 
       (.I0(\int_searched[31]_i_3_n_2 ),
        .I1(\waddr_reg_n_2_[3] ),
        .I2(\waddr_reg_n_2_[2] ),
        .I3(\waddr_reg_n_2_[4] ),
        .O(p_0_in11_out));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_125_reg[31] [31]),
        .O(\or [31]));
  LUT4 #(
    .INIT(16'h0040)) 
    \int_searched[31]_i_3 
       (.I0(\waddr_reg_n_2_[0] ),
        .I1(out[1]),
        .I2(s_axi_CONTROL_BUS_WVALID),
        .I3(\waddr_reg_n_2_[1] ),
        .O(\int_searched[31]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [3]),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [4]),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [5]),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [6]),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_125_reg[31] [7]),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [8]),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_125_reg[31] [9]),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [0]),
        .Q(\searched_read_reg_125_reg[31] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [10]),
        .Q(\searched_read_reg_125_reg[31] [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [11]),
        .Q(\searched_read_reg_125_reg[31] [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [12]),
        .Q(\searched_read_reg_125_reg[31] [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [13]),
        .Q(\searched_read_reg_125_reg[31] [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [14]),
        .Q(\searched_read_reg_125_reg[31] [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [15]),
        .Q(\searched_read_reg_125_reg[31] [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [16]),
        .Q(\searched_read_reg_125_reg[31] [16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [17]),
        .Q(\searched_read_reg_125_reg[31] [17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [18]),
        .Q(\searched_read_reg_125_reg[31] [18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [19]),
        .Q(\searched_read_reg_125_reg[31] [19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [1]),
        .Q(\searched_read_reg_125_reg[31] [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [20]),
        .Q(\searched_read_reg_125_reg[31] [20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [21]),
        .Q(\searched_read_reg_125_reg[31] [21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [22]),
        .Q(\searched_read_reg_125_reg[31] [22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [23]),
        .Q(\searched_read_reg_125_reg[31] [23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [24]),
        .Q(\searched_read_reg_125_reg[31] [24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [25]),
        .Q(\searched_read_reg_125_reg[31] [25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [26]),
        .Q(\searched_read_reg_125_reg[31] [26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [27]),
        .Q(\searched_read_reg_125_reg[31] [27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [28]),
        .Q(\searched_read_reg_125_reg[31] [28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [29]),
        .Q(\searched_read_reg_125_reg[31] [29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [2]),
        .Q(\searched_read_reg_125_reg[31] [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [30]),
        .Q(\searched_read_reg_125_reg[31] [30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [31]),
        .Q(\searched_read_reg_125_reg[31] [31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [3]),
        .Q(\searched_read_reg_125_reg[31] [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [4]),
        .Q(\searched_read_reg_125_reg[31] [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [5]),
        .Q(\searched_read_reg_125_reg[31] [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [6]),
        .Q(\searched_read_reg_125_reg[31] [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [7]),
        .Q(\searched_read_reg_125_reg[31] [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [8]),
        .Q(\searched_read_reg_125_reg[31] [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [9]),
        .Q(\searched_read_reg_125_reg[31] [9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    interrupt_INST_0
       (.I0(int_gie_reg_n_2),
        .I1(p_1_in),
        .I2(\int_isr_reg_n_2_[0] ),
        .O(interrupt));
  LUT5 #(
    .INIT(32'hAEAEFFAE)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[0]_i_2_n_2 ),
        .I1(int_ap_return[0]),
        .I2(\rdata_data[31]_i_3_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [0]),
        .I4(\rdata_data[31]_i_4_n_2 ),
        .O(rdata_data[0]));
  LUT6 #(
    .INIT(64'hC088C088FFFFC088)) 
    \rdata_data[0]_i_2 
       (.I0(ap_start),
        .I1(\rdata_data[1]_i_2_n_2 ),
        .I2(\int_ier_reg_n_2_[0] ),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(\rdata_data[0]_i_3_n_2 ),
        .I5(\rdata_data[1]_i_3_n_2 ),
        .O(\rdata_data[0]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rdata_data[0]_i_3 
       (.I0(\int_isr_reg_n_2_[0] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(int_gie_reg_n_2),
        .O(\rdata_data[0]_i_3_n_2 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[10]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[10]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [10]),
        .O(rdata_data[10]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[11]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[11]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [11]),
        .O(rdata_data[11]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[12]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[12]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [12]),
        .O(rdata_data[12]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[13]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[13]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [13]),
        .O(rdata_data[13]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[14]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[14]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [14]),
        .O(rdata_data[14]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[15]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[15]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [15]),
        .O(rdata_data[15]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[16]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[16]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [16]),
        .O(rdata_data[16]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[17]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[17]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [17]),
        .O(rdata_data[17]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[18]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[18]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [18]),
        .O(rdata_data[18]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[19]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[19]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [19]),
        .O(rdata_data[19]));
  LUT6 #(
    .INIT(64'hFFFFFFFF8F880000)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[1]_i_2_n_2 ),
        .I1(p_0_in),
        .I2(\rdata_data[1]_i_3_n_2 ),
        .I3(p_1_in),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\rdata_data[1]_i_4_n_2 ),
        .O(rdata_data[1]));
  LUT4 #(
    .INIT(16'h0001)) 
    \rdata_data[1]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[1]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \rdata_data[1]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[1]_i_3_n_2 ));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \rdata_data[1]_i_4 
       (.I0(\rdata_data[31]_i_4_n_2 ),
        .I1(\searched_read_reg_125_reg[31] [1]),
        .I2(int_ap_done),
        .I3(\rdata_data[7]_i_2_n_2 ),
        .I4(int_ap_return[1]),
        .I5(\rdata_data[31]_i_3_n_2 ),
        .O(\rdata_data[1]_i_4_n_2 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[20]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[20]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [20]),
        .O(rdata_data[20]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[21]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[21]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [21]),
        .O(rdata_data[21]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[22]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[22]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [22]),
        .O(rdata_data[22]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[23]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[23]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [23]),
        .O(rdata_data[23]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[24]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[24]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [24]),
        .O(rdata_data[24]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[25]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[25]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [25]),
        .O(rdata_data[25]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[26]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[26]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [26]),
        .O(rdata_data[26]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[27]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[27]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [27]),
        .O(rdata_data[27]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[28]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[28]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [28]),
        .O(rdata_data[28]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[29]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[29]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [29]),
        .O(rdata_data[29]));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \rdata_data[2]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[2]),
        .I2(int_ap_idle),
        .I3(\rdata_data[7]_i_2_n_2 ),
        .I4(\searched_read_reg_125_reg[31] [2]),
        .I5(\rdata_data[31]_i_4_n_2 ),
        .O(rdata_data[2]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[30]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[30]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [30]),
        .O(rdata_data[30]));
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[31]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(rstate[0]),
        .I2(rstate[1]),
        .O(ar_hs));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[31]_i_2 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[31]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [31]),
        .O(rdata_data[31]));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    \rdata_data[31]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[31]_i_3_n_2 ));
  LUT5 #(
    .INIT(32'hFFFDFFFF)) 
    \rdata_data[31]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[31]_i_4_n_2 ));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \rdata_data[3]_i_1 
       (.I0(\rdata_data[7]_i_2_n_2 ),
        .I1(int_ap_ready),
        .I2(int_ap_return[3]),
        .I3(\rdata_data[31]_i_3_n_2 ),
        .I4(\searched_read_reg_125_reg[31] [3]),
        .I5(\rdata_data[31]_i_4_n_2 ),
        .O(rdata_data[3]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[4]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[4]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [4]),
        .O(rdata_data[4]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[5]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[5]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [5]),
        .O(rdata_data[5]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[6]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[6]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [6]),
        .O(rdata_data[6]));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \rdata_data[7]_i_1 
       (.I0(\rdata_data[31]_i_4_n_2 ),
        .I1(\searched_read_reg_125_reg[31] [7]),
        .I2(int_ap_return[7]),
        .I3(\rdata_data[31]_i_3_n_2 ),
        .I4(int_auto_restart),
        .I5(\rdata_data[7]_i_2_n_2 ),
        .O(rdata_data[7]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \rdata_data[7]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[7]_i_2_n_2 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[8]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[8]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [8]),
        .O(rdata_data[8]));
  LUT4 #(
    .INIT(16'h4F44)) 
    \rdata_data[9]_i_1 
       (.I0(\rdata_data[31]_i_3_n_2 ),
        .I1(int_ap_return[9]),
        .I2(\rdata_data[31]_i_4_n_2 ),
        .I3(\searched_read_reg_125_reg[31] [9]),
        .O(rdata_data[9]));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[10]),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .R(1'b0));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[11]),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .R(1'b0));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[12]),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(1'b0));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[13]),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(1'b0));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[14]),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(1'b0));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[15]),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(1'b0));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[16]),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(1'b0));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[17]),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(1'b0));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[18]),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(1'b0));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[19]),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(1'b0));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[20]),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(1'b0));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[21]),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(1'b0));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[22]),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(1'b0));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[23]),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(1'b0));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[24]),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(1'b0));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[25]),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(1'b0));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[26]),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(1'b0));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[27]),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(1'b0));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[28]),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(1'b0));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[29]),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(1'b0));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[30]),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(1'b0));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[31]),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(1'b0));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(1'b0));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[4]),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(1'b0));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[5]),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(1'b0));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[6]),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .R(1'b0));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(1'b0));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[8]),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .R(1'b0));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[9]),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h003A)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(s_axi_CONTROL_BUS_RREADY),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rstate[0]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_2 ),
        .Q(rstate[0]),
        .R(ARESET));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(ARESET));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \searched_read_reg_125[31]_i_1 
       (.I0(Q[0]),
        .I1(ap_start),
        .O(\searched_read_reg_125_reg[0] ));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[4]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_2_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_2_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_2_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_2_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_2_[4] ),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [4:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
