-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Thu Feb 22 21:24:56 2018
-- Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.vhdl
-- Design      : system_Adder2_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi is
  port (
    ARESET : out STD_LOGIC;
    s_axi_CONTROL_BUS_RVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_ARREADY : out STD_LOGIC;
    \out\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \searched_read_reg_125_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \searched_read_reg_125_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    interrupt : out STD_LOGIC;
    s_axi_CONTROL_BUS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ap_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_CONTROL_BUS_ARVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_RREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CONTROL_BUS_WVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ap_rst_n : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \INPUT_STREAM_V_last_V_0_payload_B_reg[0]\ : in STD_LOGIC;
    s_axi_CONTROL_BUS_BREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \searched_read_reg_125_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi is
  signal \/FSM_onehot_wstate[1]_i_1_n_2\ : STD_LOGIC;
  signal \/FSM_onehot_wstate[2]_i_1_n_2\ : STD_LOGIC;
  signal \^areset\ : STD_LOGIC;
  signal \FSM_onehot_wstate[3]_i_1_n_2\ : STD_LOGIC;
  signal \FSM_onehot_wstate_reg_n_2_[0]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_wstate_reg_n_2_[0]\ : signal is "yes";
  signal ap_idle : STD_LOGIC;
  signal ap_start : STD_LOGIC;
  signal ar_hs : STD_LOGIC;
  signal int_ap_done : STD_LOGIC;
  signal int_ap_done_i_1_n_2 : STD_LOGIC;
  signal int_ap_idle : STD_LOGIC;
  signal int_ap_ready : STD_LOGIC;
  signal int_ap_return : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal int_ap_start3_out : STD_LOGIC;
  signal int_ap_start_i_1_n_2 : STD_LOGIC;
  signal int_auto_restart : STD_LOGIC;
  signal int_auto_restart_i_1_n_2 : STD_LOGIC;
  signal int_gie_i_1_n_2 : STD_LOGIC;
  signal int_gie_reg_n_2 : STD_LOGIC;
  signal \int_ier[0]_i_1_n_2\ : STD_LOGIC;
  signal \int_ier[1]_i_1_n_2\ : STD_LOGIC;
  signal \int_ier[1]_i_2_n_2\ : STD_LOGIC;
  signal \int_ier_reg_n_2_[0]\ : STD_LOGIC;
  signal int_isr6_out : STD_LOGIC;
  signal \int_isr[0]_i_1_n_2\ : STD_LOGIC;
  signal \int_isr[1]_i_1_n_2\ : STD_LOGIC;
  signal \int_isr_reg_n_2_[0]\ : STD_LOGIC;
  signal \int_searched[31]_i_3_n_2\ : STD_LOGIC;
  signal \or\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^out\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute RTL_KEEP of \^out\ : signal is "yes";
  signal p_0_in : STD_LOGIC;
  signal p_0_in11_out : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal rdata_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \rdata_data[0]_i_2_n_2\ : STD_LOGIC;
  signal \rdata_data[0]_i_3_n_2\ : STD_LOGIC;
  signal \rdata_data[1]_i_2_n_2\ : STD_LOGIC;
  signal \rdata_data[1]_i_3_n_2\ : STD_LOGIC;
  signal \rdata_data[1]_i_4_n_2\ : STD_LOGIC;
  signal \rdata_data[31]_i_3_n_2\ : STD_LOGIC;
  signal \rdata_data[31]_i_4_n_2\ : STD_LOGIC;
  signal \rdata_data[7]_i_2_n_2\ : STD_LOGIC;
  signal rstate : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rstate[0]_i_1_n_2\ : STD_LOGIC;
  signal \^searched_read_reg_125_reg[31]\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal waddr : STD_LOGIC;
  signal \waddr_reg_n_2_[0]\ : STD_LOGIC;
  signal \waddr_reg_n_2_[1]\ : STD_LOGIC;
  signal \waddr_reg_n_2_[2]\ : STD_LOGIC;
  signal \waddr_reg_n_2_[3]\ : STD_LOGIC;
  signal \waddr_reg_n_2_[4]\ : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[0]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_wstate_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[1]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[2]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[3]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \i_reg_96[5]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of int_ap_idle_i_1 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of int_ap_start_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of int_ap_start_i_2 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of int_gie_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \int_ier[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \int_isr[0]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \int_searched[0]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \int_searched[10]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \int_searched[11]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \int_searched[12]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \int_searched[13]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \int_searched[14]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \int_searched[15]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \int_searched[16]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \int_searched[17]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \int_searched[18]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \int_searched[19]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \int_searched[1]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \int_searched[20]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \int_searched[21]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \int_searched[22]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \int_searched[23]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \int_searched[24]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \int_searched[25]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \int_searched[26]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \int_searched[27]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \int_searched[28]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \int_searched[29]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \int_searched[2]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \int_searched[30]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \int_searched[31]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \int_searched[3]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \int_searched[4]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \int_searched[5]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \int_searched[6]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \int_searched[7]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \int_searched[8]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \int_searched[9]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of interrupt_INST_0 : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \rdata_data[0]_i_3\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \rdata_data[1]_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \rdata_data[7]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \rstate[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of s_axi_CONTROL_BUS_RVALID_INST_0 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \searched_read_reg_125[31]_i_1\ : label is "soft_lutpair4";
begin
  ARESET <= \^areset\;
  \out\(2 downto 0) <= \^out\(2 downto 0);
  \searched_read_reg_125_reg[31]\(31 downto 0) <= \^searched_read_reg_125_reg[31]\(31 downto 0);
\/FSM_onehot_wstate[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000BFF0B"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_BREADY,
      I1 => \^out\(2),
      I2 => \^out\(1),
      I3 => \^out\(0),
      I4 => s_axi_CONTROL_BUS_AWVALID,
      O => \/FSM_onehot_wstate[1]_i_1_n_2\
    );
\/FSM_onehot_wstate[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_AWVALID,
      I1 => \^out\(0),
      I2 => \^out\(1),
      I3 => s_axi_CONTROL_BUS_WVALID,
      O => \/FSM_onehot_wstate[2]_i_1_n_2\
    );
\FSM_onehot_wstate[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000F404"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_BREADY,
      I1 => \^out\(2),
      I2 => \^out\(1),
      I3 => s_axi_CONTROL_BUS_WVALID,
      I4 => \^out\(0),
      O => \FSM_onehot_wstate[3]_i_1_n_2\
    );
\FSM_onehot_wstate_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => '0',
      Q => \FSM_onehot_wstate_reg_n_2_[0]\,
      S => \^areset\
    );
\FSM_onehot_wstate_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \/FSM_onehot_wstate[1]_i_1_n_2\,
      Q => \^out\(0),
      R => \^areset\
    );
\FSM_onehot_wstate_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \/FSM_onehot_wstate[2]_i_1_n_2\,
      Q => \^out\(1),
      R => \^areset\
    );
\FSM_onehot_wstate_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \FSM_onehot_wstate[3]_i_1_n_2\,
      Q => \^out\(2),
      R => \^areset\
    );
\INPUT_STREAM_V_dest_V_0_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => \^areset\
    );
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4555"
    )
        port map (
      I0 => Q(1),
      I1 => Q(2),
      I2 => Q(0),
      I3 => ap_start,
      O => D(0)
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FC88"
    )
        port map (
      I0 => ap_start,
      I1 => Q(0),
      I2 => \INPUT_STREAM_V_last_V_0_payload_B_reg[0]\,
      I3 => Q(1),
      O => D(1)
    );
\i_reg_96[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => ap_start,
      I1 => Q(0),
      I2 => E(0),
      O => SR(0)
    );
int_ap_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFAAAAAAAA"
    )
        port map (
      I0 => Q(2),
      I1 => \rdata_data[7]_i_2_n_2\,
      I2 => s_axi_CONTROL_BUS_ARVALID,
      I3 => rstate(0),
      I4 => rstate(1),
      I5 => int_ap_done,
      O => int_ap_done_i_1_n_2
    );
int_ap_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_done_i_1_n_2,
      Q => int_ap_done,
      R => \^areset\
    );
int_ap_idle_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Q(0),
      I1 => ap_start,
      O => ap_idle
    );
int_ap_idle_reg: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => ap_idle,
      Q => int_ap_idle,
      R => \^areset\
    );
int_ap_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => Q(2),
      Q => int_ap_ready,
      R => \^areset\
    );
\int_ap_return_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(0),
      Q => int_ap_return(0),
      R => \^areset\
    );
\int_ap_return_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(10),
      Q => int_ap_return(10),
      R => \^areset\
    );
\int_ap_return_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(11),
      Q => int_ap_return(11),
      R => \^areset\
    );
\int_ap_return_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(12),
      Q => int_ap_return(12),
      R => \^areset\
    );
\int_ap_return_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(13),
      Q => int_ap_return(13),
      R => \^areset\
    );
\int_ap_return_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(14),
      Q => int_ap_return(14),
      R => \^areset\
    );
\int_ap_return_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(15),
      Q => int_ap_return(15),
      R => \^areset\
    );
\int_ap_return_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(16),
      Q => int_ap_return(16),
      R => \^areset\
    );
\int_ap_return_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(17),
      Q => int_ap_return(17),
      R => \^areset\
    );
\int_ap_return_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(18),
      Q => int_ap_return(18),
      R => \^areset\
    );
\int_ap_return_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(19),
      Q => int_ap_return(19),
      R => \^areset\
    );
\int_ap_return_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(1),
      Q => int_ap_return(1),
      R => \^areset\
    );
\int_ap_return_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(20),
      Q => int_ap_return(20),
      R => \^areset\
    );
\int_ap_return_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(21),
      Q => int_ap_return(21),
      R => \^areset\
    );
\int_ap_return_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(22),
      Q => int_ap_return(22),
      R => \^areset\
    );
\int_ap_return_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(23),
      Q => int_ap_return(23),
      R => \^areset\
    );
\int_ap_return_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(24),
      Q => int_ap_return(24),
      R => \^areset\
    );
\int_ap_return_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(25),
      Q => int_ap_return(25),
      R => \^areset\
    );
\int_ap_return_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(26),
      Q => int_ap_return(26),
      R => \^areset\
    );
\int_ap_return_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(27),
      Q => int_ap_return(27),
      R => \^areset\
    );
\int_ap_return_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(28),
      Q => int_ap_return(28),
      R => \^areset\
    );
\int_ap_return_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(29),
      Q => int_ap_return(29),
      R => \^areset\
    );
\int_ap_return_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(2),
      Q => int_ap_return(2),
      R => \^areset\
    );
\int_ap_return_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(30),
      Q => int_ap_return(30),
      R => \^areset\
    );
\int_ap_return_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(31),
      Q => int_ap_return(31),
      R => \^areset\
    );
\int_ap_return_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(3),
      Q => int_ap_return(3),
      R => \^areset\
    );
\int_ap_return_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(4),
      Q => int_ap_return(4),
      R => \^areset\
    );
\int_ap_return_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(5),
      Q => int_ap_return(5),
      R => \^areset\
    );
\int_ap_return_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(6),
      Q => int_ap_return(6),
      R => \^areset\
    );
\int_ap_return_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(7),
      Q => int_ap_return(7),
      R => \^areset\
    );
\int_ap_return_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(8),
      Q => int_ap_return(8),
      R => \^areset\
    );
\int_ap_return_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => Q(2),
      D => \searched_read_reg_125_reg[31]_0\(9),
      Q => int_ap_return(9),
      R => \^areset\
    );
int_ap_start_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBF8"
    )
        port map (
      I0 => int_auto_restart,
      I1 => Q(2),
      I2 => int_ap_start3_out,
      I3 => ap_start,
      O => int_ap_start_i_1_n_2
    );
int_ap_start_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => \waddr_reg_n_2_[2]\,
      I2 => \int_ier[1]_i_2_n_2\,
      I3 => \waddr_reg_n_2_[3]\,
      O => int_ap_start3_out
    );
int_ap_start_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_start_i_1_n_2,
      Q => ap_start,
      R => \^areset\
    );
int_auto_restart_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0002"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(7),
      I1 => \waddr_reg_n_2_[3]\,
      I2 => \int_ier[1]_i_2_n_2\,
      I3 => \waddr_reg_n_2_[2]\,
      I4 => int_auto_restart,
      O => int_auto_restart_i_1_n_2
    );
int_auto_restart_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_auto_restart_i_1_n_2,
      Q => int_auto_restart,
      R => \^areset\
    );
int_gie_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => \waddr_reg_n_2_[3]\,
      I2 => \waddr_reg_n_2_[2]\,
      I3 => \int_ier[1]_i_2_n_2\,
      I4 => int_gie_reg_n_2,
      O => int_gie_i_1_n_2
    );
int_gie_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_gie_i_1_n_2,
      Q => int_gie_reg_n_2,
      R => \^areset\
    );
\int_ier[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => \waddr_reg_n_2_[3]\,
      I2 => \int_ier[1]_i_2_n_2\,
      I3 => \waddr_reg_n_2_[2]\,
      I4 => \int_ier_reg_n_2_[0]\,
      O => \int_ier[0]_i_1_n_2\
    );
\int_ier[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(1),
      I1 => \waddr_reg_n_2_[3]\,
      I2 => \int_ier[1]_i_2_n_2\,
      I3 => \waddr_reg_n_2_[2]\,
      I4 => p_0_in,
      O => \int_ier[1]_i_1_n_2\
    );
\int_ier[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFDFFFFFF"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WSTRB(0),
      I1 => \waddr_reg_n_2_[4]\,
      I2 => \waddr_reg_n_2_[1]\,
      I3 => s_axi_CONTROL_BUS_WVALID,
      I4 => \^out\(1),
      I5 => \waddr_reg_n_2_[0]\,
      O => \int_ier[1]_i_2_n_2\
    );
\int_ier_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_ier[0]_i_1_n_2\,
      Q => \int_ier_reg_n_2_[0]\,
      R => \^areset\
    );
\int_ier_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_ier[1]_i_1_n_2\,
      Q => p_0_in,
      R => \^areset\
    );
\int_isr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F777F888"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => int_isr6_out,
      I2 => \int_ier_reg_n_2_[0]\,
      I3 => Q(2),
      I4 => \int_isr_reg_n_2_[0]\,
      O => \int_isr[0]_i_1_n_2\
    );
\int_isr[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \waddr_reg_n_2_[3]\,
      I1 => \waddr_reg_n_2_[2]\,
      I2 => \int_ier[1]_i_2_n_2\,
      O => int_isr6_out
    );
\int_isr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F777F888"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(1),
      I1 => int_isr6_out,
      I2 => p_0_in,
      I3 => Q(2),
      I4 => p_1_in,
      O => \int_isr[1]_i_1_n_2\
    );
\int_isr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_isr[0]_i_1_n_2\,
      Q => \int_isr_reg_n_2_[0]\,
      R => \^areset\
    );
\int_isr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_isr[1]_i_1_n_2\,
      Q => p_1_in,
      R => \^areset\
    );
\int_searched[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(0),
      O => \or\(0)
    );
\int_searched[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(10),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(10),
      O => \or\(10)
    );
\int_searched[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(11),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(11),
      O => \or\(11)
    );
\int_searched[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(12),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(12),
      O => \or\(12)
    );
\int_searched[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(13),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(13),
      O => \or\(13)
    );
\int_searched[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(14),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(14),
      O => \or\(14)
    );
\int_searched[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(15),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(15),
      O => \or\(15)
    );
\int_searched[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(16),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(16),
      O => \or\(16)
    );
\int_searched[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(17),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(17),
      O => \or\(17)
    );
\int_searched[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(18),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(18),
      O => \or\(18)
    );
\int_searched[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(19),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(19),
      O => \or\(19)
    );
\int_searched[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(1),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(1),
      O => \or\(1)
    );
\int_searched[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(20),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(20),
      O => \or\(20)
    );
\int_searched[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(21),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(21),
      O => \or\(21)
    );
\int_searched[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(22),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(22),
      O => \or\(22)
    );
\int_searched[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(23),
      I1 => s_axi_CONTROL_BUS_WSTRB(2),
      I2 => \^searched_read_reg_125_reg[31]\(23),
      O => \or\(23)
    );
\int_searched[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(24),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(24),
      O => \or\(24)
    );
\int_searched[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(25),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(25),
      O => \or\(25)
    );
\int_searched[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(26),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(26),
      O => \or\(26)
    );
\int_searched[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(27),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(27),
      O => \or\(27)
    );
\int_searched[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(28),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(28),
      O => \or\(28)
    );
\int_searched[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(29),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(29),
      O => \or\(29)
    );
\int_searched[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(2),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(2),
      O => \or\(2)
    );
\int_searched[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(30),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(30),
      O => \or\(30)
    );
\int_searched[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => \int_searched[31]_i_3_n_2\,
      I1 => \waddr_reg_n_2_[3]\,
      I2 => \waddr_reg_n_2_[2]\,
      I3 => \waddr_reg_n_2_[4]\,
      O => p_0_in11_out
    );
\int_searched[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(31),
      I1 => s_axi_CONTROL_BUS_WSTRB(3),
      I2 => \^searched_read_reg_125_reg[31]\(31),
      O => \or\(31)
    );
\int_searched[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \waddr_reg_n_2_[0]\,
      I1 => \^out\(1),
      I2 => s_axi_CONTROL_BUS_WVALID,
      I3 => \waddr_reg_n_2_[1]\,
      O => \int_searched[31]_i_3_n_2\
    );
\int_searched[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(3),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(3),
      O => \or\(3)
    );
\int_searched[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(4),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(4),
      O => \or\(4)
    );
\int_searched[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(5),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(5),
      O => \or\(5)
    );
\int_searched[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(6),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(6),
      O => \or\(6)
    );
\int_searched[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(7),
      I1 => s_axi_CONTROL_BUS_WSTRB(0),
      I2 => \^searched_read_reg_125_reg[31]\(7),
      O => \or\(7)
    );
\int_searched[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(8),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(8),
      O => \or\(8)
    );
\int_searched[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(9),
      I1 => s_axi_CONTROL_BUS_WSTRB(1),
      I2 => \^searched_read_reg_125_reg[31]\(9),
      O => \or\(9)
    );
\int_searched_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(0),
      Q => \^searched_read_reg_125_reg[31]\(0),
      R => '0'
    );
\int_searched_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(10),
      Q => \^searched_read_reg_125_reg[31]\(10),
      R => '0'
    );
\int_searched_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(11),
      Q => \^searched_read_reg_125_reg[31]\(11),
      R => '0'
    );
\int_searched_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(12),
      Q => \^searched_read_reg_125_reg[31]\(12),
      R => '0'
    );
\int_searched_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(13),
      Q => \^searched_read_reg_125_reg[31]\(13),
      R => '0'
    );
\int_searched_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(14),
      Q => \^searched_read_reg_125_reg[31]\(14),
      R => '0'
    );
\int_searched_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(15),
      Q => \^searched_read_reg_125_reg[31]\(15),
      R => '0'
    );
\int_searched_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(16),
      Q => \^searched_read_reg_125_reg[31]\(16),
      R => '0'
    );
\int_searched_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(17),
      Q => \^searched_read_reg_125_reg[31]\(17),
      R => '0'
    );
\int_searched_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(18),
      Q => \^searched_read_reg_125_reg[31]\(18),
      R => '0'
    );
\int_searched_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(19),
      Q => \^searched_read_reg_125_reg[31]\(19),
      R => '0'
    );
\int_searched_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(1),
      Q => \^searched_read_reg_125_reg[31]\(1),
      R => '0'
    );
\int_searched_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(20),
      Q => \^searched_read_reg_125_reg[31]\(20),
      R => '0'
    );
\int_searched_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(21),
      Q => \^searched_read_reg_125_reg[31]\(21),
      R => '0'
    );
\int_searched_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(22),
      Q => \^searched_read_reg_125_reg[31]\(22),
      R => '0'
    );
\int_searched_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(23),
      Q => \^searched_read_reg_125_reg[31]\(23),
      R => '0'
    );
\int_searched_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(24),
      Q => \^searched_read_reg_125_reg[31]\(24),
      R => '0'
    );
\int_searched_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(25),
      Q => \^searched_read_reg_125_reg[31]\(25),
      R => '0'
    );
\int_searched_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(26),
      Q => \^searched_read_reg_125_reg[31]\(26),
      R => '0'
    );
\int_searched_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(27),
      Q => \^searched_read_reg_125_reg[31]\(27),
      R => '0'
    );
\int_searched_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(28),
      Q => \^searched_read_reg_125_reg[31]\(28),
      R => '0'
    );
\int_searched_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(29),
      Q => \^searched_read_reg_125_reg[31]\(29),
      R => '0'
    );
\int_searched_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(2),
      Q => \^searched_read_reg_125_reg[31]\(2),
      R => '0'
    );
\int_searched_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(30),
      Q => \^searched_read_reg_125_reg[31]\(30),
      R => '0'
    );
\int_searched_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(31),
      Q => \^searched_read_reg_125_reg[31]\(31),
      R => '0'
    );
\int_searched_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(3),
      Q => \^searched_read_reg_125_reg[31]\(3),
      R => '0'
    );
\int_searched_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(4),
      Q => \^searched_read_reg_125_reg[31]\(4),
      R => '0'
    );
\int_searched_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(5),
      Q => \^searched_read_reg_125_reg[31]\(5),
      R => '0'
    );
\int_searched_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(6),
      Q => \^searched_read_reg_125_reg[31]\(6),
      R => '0'
    );
\int_searched_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(7),
      Q => \^searched_read_reg_125_reg[31]\(7),
      R => '0'
    );
\int_searched_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(8),
      Q => \^searched_read_reg_125_reg[31]\(8),
      R => '0'
    );
\int_searched_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in11_out,
      D => \or\(9),
      Q => \^searched_read_reg_125_reg[31]\(9),
      R => '0'
    );
interrupt_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => int_gie_reg_n_2,
      I1 => p_1_in,
      I2 => \int_isr_reg_n_2_[0]\,
      O => interrupt
    );
\rdata_data[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAEFFAE"
    )
        port map (
      I0 => \rdata_data[0]_i_2_n_2\,
      I1 => int_ap_return(0),
      I2 => \rdata_data[31]_i_3_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(0),
      I4 => \rdata_data[31]_i_4_n_2\,
      O => rdata_data(0)
    );
\rdata_data[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C088C088FFFFC088"
    )
        port map (
      I0 => ap_start,
      I1 => \rdata_data[1]_i_2_n_2\,
      I2 => \int_ier_reg_n_2_[0]\,
      I3 => s_axi_CONTROL_BUS_ARADDR(3),
      I4 => \rdata_data[0]_i_3_n_2\,
      I5 => \rdata_data[1]_i_3_n_2\,
      O => \rdata_data[0]_i_2_n_2\
    );
\rdata_data[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \int_isr_reg_n_2_[0]\,
      I1 => s_axi_CONTROL_BUS_ARADDR(3),
      I2 => int_gie_reg_n_2,
      O => \rdata_data[0]_i_3_n_2\
    );
\rdata_data[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(10),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(10),
      O => rdata_data(10)
    );
\rdata_data[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(11),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(11),
      O => rdata_data(11)
    );
\rdata_data[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(12),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(12),
      O => rdata_data(12)
    );
\rdata_data[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(13),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(13),
      O => rdata_data(13)
    );
\rdata_data[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(14),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(14),
      O => rdata_data(14)
    );
\rdata_data[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(15),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(15),
      O => rdata_data(15)
    );
\rdata_data[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(16),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(16),
      O => rdata_data(16)
    );
\rdata_data[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(17),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(17),
      O => rdata_data(17)
    );
\rdata_data[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(18),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(18),
      O => rdata_data(18)
    );
\rdata_data[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(19),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(19),
      O => rdata_data(19)
    );
\rdata_data[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF8F880000"
    )
        port map (
      I0 => \rdata_data[1]_i_2_n_2\,
      I1 => p_0_in,
      I2 => \rdata_data[1]_i_3_n_2\,
      I3 => p_1_in,
      I4 => s_axi_CONTROL_BUS_ARADDR(3),
      I5 => \rdata_data[1]_i_4_n_2\,
      O => rdata_data(1)
    );
\rdata_data[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(4),
      I1 => s_axi_CONTROL_BUS_ARADDR(1),
      I2 => s_axi_CONTROL_BUS_ARADDR(0),
      I3 => s_axi_CONTROL_BUS_ARADDR(2),
      O => \rdata_data[1]_i_2_n_2\
    );
\rdata_data[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(0),
      I1 => s_axi_CONTROL_BUS_ARADDR(1),
      I2 => s_axi_CONTROL_BUS_ARADDR(2),
      I3 => s_axi_CONTROL_BUS_ARADDR(4),
      O => \rdata_data[1]_i_3_n_2\
    );
\rdata_data[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44F444F4FFFF44F4"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_2\,
      I1 => \^searched_read_reg_125_reg[31]\(1),
      I2 => int_ap_done,
      I3 => \rdata_data[7]_i_2_n_2\,
      I4 => int_ap_return(1),
      I5 => \rdata_data[31]_i_3_n_2\,
      O => \rdata_data[1]_i_4_n_2\
    );
\rdata_data[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(20),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(20),
      O => rdata_data(20)
    );
\rdata_data[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(21),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(21),
      O => rdata_data(21)
    );
\rdata_data[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(22),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(22),
      O => rdata_data(22)
    );
\rdata_data[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(23),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(23),
      O => rdata_data(23)
    );
\rdata_data[24]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(24),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(24),
      O => rdata_data(24)
    );
\rdata_data[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(25),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(25),
      O => rdata_data(25)
    );
\rdata_data[26]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(26),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(26),
      O => rdata_data(26)
    );
\rdata_data[27]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(27),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(27),
      O => rdata_data(27)
    );
\rdata_data[28]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(28),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(28),
      O => rdata_data(28)
    );
\rdata_data[29]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(29),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(29),
      O => rdata_data(29)
    );
\rdata_data[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44F444F4FFFF44F4"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(2),
      I2 => int_ap_idle,
      I3 => \rdata_data[7]_i_2_n_2\,
      I4 => \^searched_read_reg_125_reg[31]\(2),
      I5 => \rdata_data[31]_i_4_n_2\,
      O => rdata_data(2)
    );
\rdata_data[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(30),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(30),
      O => rdata_data(30)
    );
\rdata_data[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARVALID,
      I1 => rstate(0),
      I2 => rstate(1),
      O => ar_hs
    );
\rdata_data[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(31),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(31),
      O => rdata_data(31)
    );
\rdata_data[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(3),
      I1 => s_axi_CONTROL_BUS_ARADDR(4),
      I2 => s_axi_CONTROL_BUS_ARADDR(2),
      I3 => s_axi_CONTROL_BUS_ARADDR(0),
      I4 => s_axi_CONTROL_BUS_ARADDR(1),
      O => \rdata_data[31]_i_3_n_2\
    );
\rdata_data[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFDFFFF"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(4),
      I1 => s_axi_CONTROL_BUS_ARADDR(2),
      I2 => s_axi_CONTROL_BUS_ARADDR(0),
      I3 => s_axi_CONTROL_BUS_ARADDR(1),
      I4 => s_axi_CONTROL_BUS_ARADDR(3),
      O => \rdata_data[31]_i_4_n_2\
    );
\rdata_data[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44F444F4FFFF44F4"
    )
        port map (
      I0 => \rdata_data[7]_i_2_n_2\,
      I1 => int_ap_ready,
      I2 => int_ap_return(3),
      I3 => \rdata_data[31]_i_3_n_2\,
      I4 => \^searched_read_reg_125_reg[31]\(3),
      I5 => \rdata_data[31]_i_4_n_2\,
      O => rdata_data(3)
    );
\rdata_data[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(4),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(4),
      O => rdata_data(4)
    );
\rdata_data[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(5),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(5),
      O => rdata_data(5)
    );
\rdata_data[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(6),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(6),
      O => rdata_data(6)
    );
\rdata_data[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44F444F4FFFF44F4"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_2\,
      I1 => \^searched_read_reg_125_reg[31]\(7),
      I2 => int_ap_return(7),
      I3 => \rdata_data[31]_i_3_n_2\,
      I4 => int_auto_restart,
      I5 => \rdata_data[7]_i_2_n_2\,
      O => rdata_data(7)
    );
\rdata_data[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(3),
      I1 => s_axi_CONTROL_BUS_ARADDR(2),
      I2 => s_axi_CONTROL_BUS_ARADDR(0),
      I3 => s_axi_CONTROL_BUS_ARADDR(1),
      I4 => s_axi_CONTROL_BUS_ARADDR(4),
      O => \rdata_data[7]_i_2_n_2\
    );
\rdata_data[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(8),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(8),
      O => rdata_data(8)
    );
\rdata_data[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4F44"
    )
        port map (
      I0 => \rdata_data[31]_i_3_n_2\,
      I1 => int_ap_return(9),
      I2 => \rdata_data[31]_i_4_n_2\,
      I3 => \^searched_read_reg_125_reg[31]\(9),
      O => rdata_data(9)
    );
\rdata_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(0),
      Q => s_axi_CONTROL_BUS_RDATA(0),
      R => '0'
    );
\rdata_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(10),
      Q => s_axi_CONTROL_BUS_RDATA(10),
      R => '0'
    );
\rdata_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(11),
      Q => s_axi_CONTROL_BUS_RDATA(11),
      R => '0'
    );
\rdata_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(12),
      Q => s_axi_CONTROL_BUS_RDATA(12),
      R => '0'
    );
\rdata_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(13),
      Q => s_axi_CONTROL_BUS_RDATA(13),
      R => '0'
    );
\rdata_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(14),
      Q => s_axi_CONTROL_BUS_RDATA(14),
      R => '0'
    );
\rdata_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(15),
      Q => s_axi_CONTROL_BUS_RDATA(15),
      R => '0'
    );
\rdata_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(16),
      Q => s_axi_CONTROL_BUS_RDATA(16),
      R => '0'
    );
\rdata_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(17),
      Q => s_axi_CONTROL_BUS_RDATA(17),
      R => '0'
    );
\rdata_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(18),
      Q => s_axi_CONTROL_BUS_RDATA(18),
      R => '0'
    );
\rdata_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(19),
      Q => s_axi_CONTROL_BUS_RDATA(19),
      R => '0'
    );
\rdata_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(1),
      Q => s_axi_CONTROL_BUS_RDATA(1),
      R => '0'
    );
\rdata_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(20),
      Q => s_axi_CONTROL_BUS_RDATA(20),
      R => '0'
    );
\rdata_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(21),
      Q => s_axi_CONTROL_BUS_RDATA(21),
      R => '0'
    );
\rdata_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(22),
      Q => s_axi_CONTROL_BUS_RDATA(22),
      R => '0'
    );
\rdata_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(23),
      Q => s_axi_CONTROL_BUS_RDATA(23),
      R => '0'
    );
\rdata_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(24),
      Q => s_axi_CONTROL_BUS_RDATA(24),
      R => '0'
    );
\rdata_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(25),
      Q => s_axi_CONTROL_BUS_RDATA(25),
      R => '0'
    );
\rdata_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(26),
      Q => s_axi_CONTROL_BUS_RDATA(26),
      R => '0'
    );
\rdata_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(27),
      Q => s_axi_CONTROL_BUS_RDATA(27),
      R => '0'
    );
\rdata_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(28),
      Q => s_axi_CONTROL_BUS_RDATA(28),
      R => '0'
    );
\rdata_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(29),
      Q => s_axi_CONTROL_BUS_RDATA(29),
      R => '0'
    );
\rdata_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(2),
      Q => s_axi_CONTROL_BUS_RDATA(2),
      R => '0'
    );
\rdata_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(30),
      Q => s_axi_CONTROL_BUS_RDATA(30),
      R => '0'
    );
\rdata_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(31),
      Q => s_axi_CONTROL_BUS_RDATA(31),
      R => '0'
    );
\rdata_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(3),
      Q => s_axi_CONTROL_BUS_RDATA(3),
      R => '0'
    );
\rdata_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(4),
      Q => s_axi_CONTROL_BUS_RDATA(4),
      R => '0'
    );
\rdata_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(5),
      Q => s_axi_CONTROL_BUS_RDATA(5),
      R => '0'
    );
\rdata_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(6),
      Q => s_axi_CONTROL_BUS_RDATA(6),
      R => '0'
    );
\rdata_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(7),
      Q => s_axi_CONTROL_BUS_RDATA(7),
      R => '0'
    );
\rdata_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(8),
      Q => s_axi_CONTROL_BUS_RDATA(8),
      R => '0'
    );
\rdata_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(9),
      Q => s_axi_CONTROL_BUS_RDATA(9),
      R => '0'
    );
\rstate[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"003A"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARVALID,
      I1 => s_axi_CONTROL_BUS_RREADY,
      I2 => rstate(0),
      I3 => rstate(1),
      O => \rstate[0]_i_1_n_2\
    );
\rstate_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \rstate[0]_i_1_n_2\,
      Q => rstate(0),
      R => \^areset\
    );
\rstate_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => '0',
      Q => rstate(1),
      S => \^areset\
    );
s_axi_CONTROL_BUS_ARREADY_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rstate(0),
      I1 => rstate(1),
      O => s_axi_CONTROL_BUS_ARREADY
    );
s_axi_CONTROL_BUS_RVALID_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rstate(0),
      I1 => rstate(1),
      O => s_axi_CONTROL_BUS_RVALID
    );
\searched_read_reg_125[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Q(0),
      I1 => ap_start,
      O => \searched_read_reg_125_reg[0]\(0)
    );
\waddr[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out\(0),
      I1 => s_axi_CONTROL_BUS_AWVALID,
      O => waddr
    );
\waddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(0),
      Q => \waddr_reg_n_2_[0]\,
      R => '0'
    );
\waddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(1),
      Q => \waddr_reg_n_2_[1]\,
      R => '0'
    );
\waddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(2),
      Q => \waddr_reg_n_2_[2]\,
      R => '0'
    );
\waddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(3),
      Q => \waddr_reg_n_2_[3]\,
      R => '0'
    );
\waddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(4),
      Q => \waddr_reg_n_2_[4]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    INPUT_STREAM_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    INPUT_STREAM_TVALID : in STD_LOGIC;
    INPUT_STREAM_TREADY : out STD_LOGIC;
    INPUT_STREAM_TKEEP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TUSER : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INPUT_STREAM_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    INPUT_STREAM_TID : in STD_LOGIC_VECTOR ( 4 downto 0 );
    INPUT_STREAM_TDEST : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CONTROL_BUS_AWVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_AWADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_WVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_WREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CONTROL_BUS_ARVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_ARADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_RVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_RREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CONTROL_BUS_BVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_BREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    interrupt : out STD_LOGIC
  );
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 : entity is 5;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 : entity is 32;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 is
  signal \<const0>\ : STD_LOGIC;
  signal ARESET : STD_LOGIC;
  signal \^input_stream_tready\ : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \INPUT_STREAM_V_data_V_0_state_reg_n_2_[1]\ : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_payload_A : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_payload_B : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2 : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2 : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2\ : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \ap_CS_fsm[1]_i_2_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_2_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ap_NS_fsm10_out : STD_LOGIC;
  signal ap_NS_fsm11_out : STD_LOGIC;
  signal ap_done : STD_LOGIC;
  signal i_1_fu_115_p2 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal i_reg_96 : STD_LOGIC;
  signal \i_reg_96_reg_n_2_[0]\ : STD_LOGIC;
  signal \i_reg_96_reg_n_2_[1]\ : STD_LOGIC;
  signal \i_reg_96_reg_n_2_[2]\ : STD_LOGIC;
  signal \i_reg_96_reg_n_2_[3]\ : STD_LOGIC;
  signal \i_reg_96_reg_n_2_[4]\ : STD_LOGIC;
  signal searched : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal searched_read_reg_125 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal tmp_fu_107_p3 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \INPUT_STREAM_V_data_V_0_state[1]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \INPUT_STREAM_V_last_V_0_state[0]_i_2\ : label is "soft_lutpair24";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute SOFT_HLUTNM of \i_reg_96[0]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \i_reg_96[1]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \i_reg_96[2]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \i_reg_96[3]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \i_reg_96[4]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \i_reg_96[5]_i_3\ : label is "soft_lutpair25";
begin
  INPUT_STREAM_TREADY <= \^input_stream_tready\;
  s_axi_CONTROL_BUS_BRESP(1) <= \<const0>\;
  s_axi_CONTROL_BUS_BRESP(0) <= \<const0>\;
  s_axi_CONTROL_BUS_RRESP(1) <= \<const0>\;
  s_axi_CONTROL_BUS_RRESP(0) <= \<const0>\;
Adder2_CONTROL_BUS_s_axi_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
     port map (
      ARESET => ARESET,
      D(1 downto 0) => ap_NS_fsm(1 downto 0),
      E(0) => ap_NS_fsm10_out,
      \INPUT_STREAM_V_last_V_0_payload_B_reg[0]\ => \ap_CS_fsm[1]_i_2_n_2\,
      Q(2) => ap_done,
      Q(1) => ap_CS_fsm_state2,
      Q(0) => \ap_CS_fsm_reg_n_2_[0]\,
      SR(0) => i_reg_96,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      interrupt => interrupt,
      \out\(2) => s_axi_CONTROL_BUS_BVALID,
      \out\(1) => s_axi_CONTROL_BUS_WREADY,
      \out\(0) => s_axi_CONTROL_BUS_AWREADY,
      s_axi_CONTROL_BUS_ARADDR(4 downto 0) => s_axi_CONTROL_BUS_ARADDR(4 downto 0),
      s_axi_CONTROL_BUS_ARREADY => s_axi_CONTROL_BUS_ARREADY,
      s_axi_CONTROL_BUS_ARVALID => s_axi_CONTROL_BUS_ARVALID,
      s_axi_CONTROL_BUS_AWADDR(4 downto 0) => s_axi_CONTROL_BUS_AWADDR(4 downto 0),
      s_axi_CONTROL_BUS_AWVALID => s_axi_CONTROL_BUS_AWVALID,
      s_axi_CONTROL_BUS_BREADY => s_axi_CONTROL_BUS_BREADY,
      s_axi_CONTROL_BUS_RDATA(31 downto 0) => s_axi_CONTROL_BUS_RDATA(31 downto 0),
      s_axi_CONTROL_BUS_RREADY => s_axi_CONTROL_BUS_RREADY,
      s_axi_CONTROL_BUS_RVALID => s_axi_CONTROL_BUS_RVALID,
      s_axi_CONTROL_BUS_WDATA(31 downto 0) => s_axi_CONTROL_BUS_WDATA(31 downto 0),
      s_axi_CONTROL_BUS_WSTRB(3 downto 0) => s_axi_CONTROL_BUS_WSTRB(3 downto 0),
      s_axi_CONTROL_BUS_WVALID => s_axi_CONTROL_BUS_WVALID,
      \searched_read_reg_125_reg[0]\(0) => ap_NS_fsm11_out,
      \searched_read_reg_125_reg[31]\(31 downto 0) => searched(31 downto 0),
      \searched_read_reg_125_reg[31]_0\(31 downto 0) => searched_read_reg_125(31 downto 0)
    );
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
\INPUT_STREAM_V_data_V_0_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA80AA808A80AA80"
    )
        port map (
      I0 => ap_rst_n,
      I1 => INPUT_STREAM_TVALID,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[1]\,
      I3 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I4 => ap_CS_fsm_state2,
      I5 => tmp_fu_107_p3,
      O => \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2\
    );
\INPUT_STREAM_V_data_V_0_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FFF4F4F"
    )
        port map (
      I0 => tmp_fu_107_p3,
      I1 => ap_CS_fsm_state2,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I3 => INPUT_STREAM_TVALID,
      I4 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[1]\,
      O => INPUT_STREAM_V_data_V_0_state(1)
    );
\INPUT_STREAM_V_data_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2\,
      Q => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_data_V_0_state(1),
      Q => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[1]\,
      R => ARESET
    );
\INPUT_STREAM_V_dest_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8A80AA80"
    )
        port map (
      I0 => ap_rst_n,
      I1 => INPUT_STREAM_TVALID,
      I2 => \^input_stream_tready\,
      I3 => \INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0]\,
      I4 => \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2\,
      O => \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2\
    );
\INPUT_STREAM_V_dest_V_0_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55D5FFFF55D555D5"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0]\,
      I1 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I2 => ap_CS_fsm_state2,
      I3 => tmp_fu_107_p3,
      I4 => INPUT_STREAM_TVALID,
      I5 => \^input_stream_tready\,
      O => INPUT_STREAM_V_dest_V_0_state(1)
    );
\INPUT_STREAM_V_dest_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2\,
      Q => \INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_dest_V_0_state(1),
      Q => \^input_stream_tready\,
      R => ARESET
    );
\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEE2022"
    )
        port map (
      I0 => INPUT_STREAM_TLAST(0),
      I1 => INPUT_STREAM_V_last_V_0_sel_wr,
      I2 => INPUT_STREAM_V_last_V_0_ack_in,
      I3 => \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\,
      I4 => INPUT_STREAM_V_last_V_0_payload_A,
      O => \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2\
    );
\INPUT_STREAM_V_last_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2\,
      Q => INPUT_STREAM_V_last_V_0_payload_A,
      R => '0'
    );
\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBB8088"
    )
        port map (
      I0 => INPUT_STREAM_TLAST(0),
      I1 => INPUT_STREAM_V_last_V_0_sel_wr,
      I2 => INPUT_STREAM_V_last_V_0_ack_in,
      I3 => \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\,
      I4 => INPUT_STREAM_V_last_V_0_payload_B,
      O => \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2\
    );
\INPUT_STREAM_V_last_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2\,
      Q => INPUT_STREAM_V_last_V_0_payload_B,
      R => '0'
    );
INPUT_STREAM_V_last_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F0080"
    )
        port map (
      I0 => \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\,
      I1 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I2 => ap_CS_fsm_state2,
      I3 => tmp_fu_107_p3,
      I4 => INPUT_STREAM_V_last_V_0_sel,
      O => INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2
    );
INPUT_STREAM_V_last_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2,
      Q => INPUT_STREAM_V_last_V_0_sel,
      R => ARESET
    );
INPUT_STREAM_V_last_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_last_V_0_ack_in,
      I2 => INPUT_STREAM_V_last_V_0_sel_wr,
      O => INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2
    );
INPUT_STREAM_V_last_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2,
      Q => INPUT_STREAM_V_last_V_0_sel_wr,
      R => ARESET
    );
\INPUT_STREAM_V_last_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A088A888"
    )
        port map (
      I0 => ap_rst_n,
      I1 => \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\,
      I2 => INPUT_STREAM_TVALID,
      I3 => INPUT_STREAM_V_last_V_0_ack_in,
      I4 => \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2\,
      O => \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2\
    );
\INPUT_STREAM_V_last_V_0_state[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I1 => ap_CS_fsm_state2,
      I2 => tmp_fu_107_p3,
      O => \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2\
    );
\INPUT_STREAM_V_last_V_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55D5FFFF55D555D5"
    )
        port map (
      I0 => \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\,
      I1 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I2 => ap_CS_fsm_state2,
      I3 => tmp_fu_107_p3,
      I4 => INPUT_STREAM_TVALID,
      I5 => INPUT_STREAM_V_last_V_0_ack_in,
      O => INPUT_STREAM_V_last_V_0_state(1)
    );
\INPUT_STREAM_V_last_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2\,
      Q => \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_last_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_last_V_0_state(1),
      Q => INPUT_STREAM_V_last_V_0_ack_in,
      R => ARESET
    );
\ap_CS_fsm[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000047FF"
    )
        port map (
      I0 => INPUT_STREAM_V_last_V_0_payload_B,
      I1 => INPUT_STREAM_V_last_V_0_sel,
      I2 => INPUT_STREAM_V_last_V_0_payload_A,
      I3 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I4 => tmp_fu_107_p3,
      O => \ap_CS_fsm[1]_i_2_n_2\
    );
\ap_CS_fsm[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8A8A8888888A888"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => tmp_fu_107_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I3 => INPUT_STREAM_V_last_V_0_payload_A,
      I4 => INPUT_STREAM_V_last_V_0_sel,
      I5 => INPUT_STREAM_V_last_V_0_payload_B,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_2_[0]\,
      S => ARESET
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ARESET
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_done,
      R => ARESET
    );
\i_reg_96[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_reg_96_reg_n_2_[0]\,
      O => i_1_fu_115_p2(0)
    );
\i_reg_96[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_reg_96_reg_n_2_[1]\,
      I1 => \i_reg_96_reg_n_2_[0]\,
      O => i_1_fu_115_p2(1)
    );
\i_reg_96[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \i_reg_96_reg_n_2_[2]\,
      I1 => \i_reg_96_reg_n_2_[1]\,
      I2 => \i_reg_96_reg_n_2_[0]\,
      O => i_1_fu_115_p2(2)
    );
\i_reg_96[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \i_reg_96_reg_n_2_[0]\,
      I1 => \i_reg_96_reg_n_2_[1]\,
      I2 => \i_reg_96_reg_n_2_[2]\,
      I3 => \i_reg_96_reg_n_2_[3]\,
      O => i_1_fu_115_p2(3)
    );
\i_reg_96[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \i_reg_96_reg_n_2_[4]\,
      I1 => \i_reg_96_reg_n_2_[0]\,
      I2 => \i_reg_96_reg_n_2_[1]\,
      I3 => \i_reg_96_reg_n_2_[2]\,
      I4 => \i_reg_96_reg_n_2_[3]\,
      O => i_1_fu_115_p2(4)
    );
\i_reg_96[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000004040400040"
    )
        port map (
      I0 => tmp_fu_107_p3,
      I1 => ap_CS_fsm_state2,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0]\,
      I3 => INPUT_STREAM_V_last_V_0_payload_A,
      I4 => INPUT_STREAM_V_last_V_0_sel,
      I5 => INPUT_STREAM_V_last_V_0_payload_B,
      O => ap_NS_fsm10_out
    );
\i_reg_96[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \i_reg_96_reg_n_2_[3]\,
      I1 => \i_reg_96_reg_n_2_[2]\,
      I2 => \i_reg_96_reg_n_2_[1]\,
      I3 => \i_reg_96_reg_n_2_[0]\,
      I4 => \i_reg_96_reg_n_2_[4]\,
      O => i_1_fu_115_p2(5)
    );
\i_reg_96_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm10_out,
      D => i_1_fu_115_p2(0),
      Q => \i_reg_96_reg_n_2_[0]\,
      R => i_reg_96
    );
\i_reg_96_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm10_out,
      D => i_1_fu_115_p2(1),
      Q => \i_reg_96_reg_n_2_[1]\,
      R => i_reg_96
    );
\i_reg_96_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm10_out,
      D => i_1_fu_115_p2(2),
      Q => \i_reg_96_reg_n_2_[2]\,
      R => i_reg_96
    );
\i_reg_96_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm10_out,
      D => i_1_fu_115_p2(3),
      Q => \i_reg_96_reg_n_2_[3]\,
      R => i_reg_96
    );
\i_reg_96_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm10_out,
      D => i_1_fu_115_p2(4),
      Q => \i_reg_96_reg_n_2_[4]\,
      R => i_reg_96
    );
\i_reg_96_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm10_out,
      D => i_1_fu_115_p2(5),
      Q => tmp_fu_107_p3,
      R => i_reg_96
    );
\searched_read_reg_125_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(0),
      Q => searched_read_reg_125(0),
      R => '0'
    );
\searched_read_reg_125_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(10),
      Q => searched_read_reg_125(10),
      R => '0'
    );
\searched_read_reg_125_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(11),
      Q => searched_read_reg_125(11),
      R => '0'
    );
\searched_read_reg_125_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(12),
      Q => searched_read_reg_125(12),
      R => '0'
    );
\searched_read_reg_125_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(13),
      Q => searched_read_reg_125(13),
      R => '0'
    );
\searched_read_reg_125_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(14),
      Q => searched_read_reg_125(14),
      R => '0'
    );
\searched_read_reg_125_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(15),
      Q => searched_read_reg_125(15),
      R => '0'
    );
\searched_read_reg_125_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(16),
      Q => searched_read_reg_125(16),
      R => '0'
    );
\searched_read_reg_125_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(17),
      Q => searched_read_reg_125(17),
      R => '0'
    );
\searched_read_reg_125_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(18),
      Q => searched_read_reg_125(18),
      R => '0'
    );
\searched_read_reg_125_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(19),
      Q => searched_read_reg_125(19),
      R => '0'
    );
\searched_read_reg_125_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(1),
      Q => searched_read_reg_125(1),
      R => '0'
    );
\searched_read_reg_125_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(20),
      Q => searched_read_reg_125(20),
      R => '0'
    );
\searched_read_reg_125_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(21),
      Q => searched_read_reg_125(21),
      R => '0'
    );
\searched_read_reg_125_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(22),
      Q => searched_read_reg_125(22),
      R => '0'
    );
\searched_read_reg_125_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(23),
      Q => searched_read_reg_125(23),
      R => '0'
    );
\searched_read_reg_125_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(24),
      Q => searched_read_reg_125(24),
      R => '0'
    );
\searched_read_reg_125_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(25),
      Q => searched_read_reg_125(25),
      R => '0'
    );
\searched_read_reg_125_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(26),
      Q => searched_read_reg_125(26),
      R => '0'
    );
\searched_read_reg_125_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(27),
      Q => searched_read_reg_125(27),
      R => '0'
    );
\searched_read_reg_125_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(28),
      Q => searched_read_reg_125(28),
      R => '0'
    );
\searched_read_reg_125_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(29),
      Q => searched_read_reg_125(29),
      R => '0'
    );
\searched_read_reg_125_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(2),
      Q => searched_read_reg_125(2),
      R => '0'
    );
\searched_read_reg_125_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(30),
      Q => searched_read_reg_125(30),
      R => '0'
    );
\searched_read_reg_125_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(31),
      Q => searched_read_reg_125(31),
      R => '0'
    );
\searched_read_reg_125_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(3),
      Q => searched_read_reg_125(3),
      R => '0'
    );
\searched_read_reg_125_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(4),
      Q => searched_read_reg_125(4),
      R => '0'
    );
\searched_read_reg_125_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(5),
      Q => searched_read_reg_125(5),
      R => '0'
    );
\searched_read_reg_125_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(6),
      Q => searched_read_reg_125(6),
      R => '0'
    );
\searched_read_reg_125_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(7),
      Q => searched_read_reg_125(7),
      R => '0'
    );
\searched_read_reg_125_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(8),
      Q => searched_read_reg_125(8),
      R => '0'
    );
\searched_read_reg_125_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm11_out,
      D => searched(9),
      Q => searched_read_reg_125(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s_axi_CONTROL_BUS_AWADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_AWVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CONTROL_BUS_WVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_WREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CONTROL_BUS_BVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_BREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_ARVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CONTROL_BUS_RVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    interrupt : out STD_LOGIC;
    INPUT_STREAM_TVALID : in STD_LOGIC;
    INPUT_STREAM_TREADY : out STD_LOGIC;
    INPUT_STREAM_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    INPUT_STREAM_TDEST : in STD_LOGIC_VECTOR ( 5 downto 0 );
    INPUT_STREAM_TKEEP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TUSER : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INPUT_STREAM_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    INPUT_STREAM_TID : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "system_Adder2_0_0,Adder2,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Adder2,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH of U0 : label is 5;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH of U0 : label is 32;
  attribute x_interface_info : string;
  attribute x_interface_info of INPUT_STREAM_TREADY : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY";
  attribute x_interface_info of INPUT_STREAM_TVALID : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of INPUT_STREAM_TVALID : signal is "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute x_interface_parameter of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of interrupt : signal is "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT";
  attribute x_interface_parameter of interrupt : signal is "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1";
  attribute x_interface_info of s_axi_CONTROL_BUS_ARREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_ARVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_AWREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_AWVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_BREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_BVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_RREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_RVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_WREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_WVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID";
  attribute x_interface_info of INPUT_STREAM_TDATA : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA";
  attribute x_interface_info of INPUT_STREAM_TDEST : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST";
  attribute x_interface_info of INPUT_STREAM_TID : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TID";
  attribute x_interface_info of INPUT_STREAM_TKEEP : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP";
  attribute x_interface_info of INPUT_STREAM_TLAST : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST";
  attribute x_interface_info of INPUT_STREAM_TSTRB : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB";
  attribute x_interface_info of INPUT_STREAM_TUSER : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER";
  attribute x_interface_info of s_axi_CONTROL_BUS_ARADDR : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR";
  attribute x_interface_info of s_axi_CONTROL_BUS_AWADDR : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR";
  attribute x_interface_parameter of s_axi_CONTROL_BUS_AWADDR : signal is "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s_axi_CONTROL_BUS_BRESP : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP";
  attribute x_interface_info of s_axi_CONTROL_BUS_RDATA : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA";
  attribute x_interface_info of s_axi_CONTROL_BUS_RRESP : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP";
  attribute x_interface_info of s_axi_CONTROL_BUS_WDATA : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA";
  attribute x_interface_info of s_axi_CONTROL_BUS_WSTRB : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
     port map (
      INPUT_STREAM_TDATA(31 downto 0) => INPUT_STREAM_TDATA(31 downto 0),
      INPUT_STREAM_TDEST(5 downto 0) => INPUT_STREAM_TDEST(5 downto 0),
      INPUT_STREAM_TID(4 downto 0) => INPUT_STREAM_TID(4 downto 0),
      INPUT_STREAM_TKEEP(3 downto 0) => INPUT_STREAM_TKEEP(3 downto 0),
      INPUT_STREAM_TLAST(0) => INPUT_STREAM_TLAST(0),
      INPUT_STREAM_TREADY => INPUT_STREAM_TREADY,
      INPUT_STREAM_TSTRB(3 downto 0) => INPUT_STREAM_TSTRB(3 downto 0),
      INPUT_STREAM_TUSER(1 downto 0) => INPUT_STREAM_TUSER(1 downto 0),
      INPUT_STREAM_TVALID => INPUT_STREAM_TVALID,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      interrupt => interrupt,
      s_axi_CONTROL_BUS_ARADDR(4 downto 0) => s_axi_CONTROL_BUS_ARADDR(4 downto 0),
      s_axi_CONTROL_BUS_ARREADY => s_axi_CONTROL_BUS_ARREADY,
      s_axi_CONTROL_BUS_ARVALID => s_axi_CONTROL_BUS_ARVALID,
      s_axi_CONTROL_BUS_AWADDR(4 downto 0) => s_axi_CONTROL_BUS_AWADDR(4 downto 0),
      s_axi_CONTROL_BUS_AWREADY => s_axi_CONTROL_BUS_AWREADY,
      s_axi_CONTROL_BUS_AWVALID => s_axi_CONTROL_BUS_AWVALID,
      s_axi_CONTROL_BUS_BREADY => s_axi_CONTROL_BUS_BREADY,
      s_axi_CONTROL_BUS_BRESP(1 downto 0) => s_axi_CONTROL_BUS_BRESP(1 downto 0),
      s_axi_CONTROL_BUS_BVALID => s_axi_CONTROL_BUS_BVALID,
      s_axi_CONTROL_BUS_RDATA(31 downto 0) => s_axi_CONTROL_BUS_RDATA(31 downto 0),
      s_axi_CONTROL_BUS_RREADY => s_axi_CONTROL_BUS_RREADY,
      s_axi_CONTROL_BUS_RRESP(1 downto 0) => s_axi_CONTROL_BUS_RRESP(1 downto 0),
      s_axi_CONTROL_BUS_RVALID => s_axi_CONTROL_BUS_RVALID,
      s_axi_CONTROL_BUS_WDATA(31 downto 0) => s_axi_CONTROL_BUS_WDATA(31 downto 0),
      s_axi_CONTROL_BUS_WREADY => s_axi_CONTROL_BUS_WREADY,
      s_axi_CONTROL_BUS_WSTRB(3 downto 0) => s_axi_CONTROL_BUS_WSTRB(3 downto 0),
      s_axi_CONTROL_BUS_WVALID => s_axi_CONTROL_BUS_WVALID
    );
end STRUCTURE;
