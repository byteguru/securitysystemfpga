// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Mon Feb  5 13:43:23 2018
// Host        : DESKTOP-7E8QPPK running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    OUTPUT_STREAM_TDATA,
    OUTPUT_STREAM_TVALID,
    OUTPUT_STREAM_TREADY,
    OUTPUT_STREAM_TKEEP,
    OUTPUT_STREAM_TSTRB,
    OUTPUT_STREAM_TUSER,
    OUTPUT_STREAM_TLAST,
    OUTPUT_STREAM_TID,
    OUTPUT_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  output [31:0]OUTPUT_STREAM_TDATA;
  output OUTPUT_STREAM_TVALID;
  input OUTPUT_STREAM_TREADY;
  output [3:0]OUTPUT_STREAM_TKEEP;
  output [3:0]OUTPUT_STREAM_TSTRB;
  output [1:0]OUTPUT_STREAM_TUSER;
  output [0:0]OUTPUT_STREAM_TLAST;
  output [4:0]OUTPUT_STREAM_TID;
  output [5:0]OUTPUT_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire Adder2_CONTROL_BUS_s_axi_U_n_12;
  wire Adder2_stream_strea_U_n_4;
  wire Adder2_stream_strea_U_n_5;
  wire Adder2_stream_strea_U_n_6;
  wire Adder2_stream_strea_U_n_7;
  wire Adder2_stream_strea_U_n_8;
  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire INPUT_STREAM_V_data_V_0_ack_in;
  wire INPUT_STREAM_V_data_V_0_load_A;
  wire INPUT_STREAM_V_data_V_0_load_B;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_A;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_B;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_data_V_0_sel_wr;
  wire INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_dest_V_0_load_A;
  wire INPUT_STREAM_V_dest_V_0_load_B;
  wire [5:0]INPUT_STREAM_V_dest_V_0_payload_A;
  wire [5:0]INPUT_STREAM_V_dest_V_0_payload_B;
  wire INPUT_STREAM_V_dest_V_0_sel;
  wire INPUT_STREAM_V_dest_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_dest_V_0_sel_wr;
  wire INPUT_STREAM_V_dest_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_id_V_0_ack_in;
  wire INPUT_STREAM_V_id_V_0_load_A;
  wire INPUT_STREAM_V_id_V_0_load_B;
  wire [4:0]INPUT_STREAM_V_id_V_0_payload_A;
  wire [4:0]INPUT_STREAM_V_id_V_0_payload_B;
  wire INPUT_STREAM_V_id_V_0_sel;
  wire INPUT_STREAM_V_id_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_id_V_0_sel_wr;
  wire INPUT_STREAM_V_id_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_id_V_0_state;
  wire \INPUT_STREAM_V_id_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_keep_V_0_ack_in;
  wire INPUT_STREAM_V_keep_V_0_load_A;
  wire INPUT_STREAM_V_keep_V_0_load_B;
  wire [3:0]INPUT_STREAM_V_keep_V_0_payload_A;
  wire [3:0]INPUT_STREAM_V_keep_V_0_payload_B;
  wire INPUT_STREAM_V_keep_V_0_sel;
  wire INPUT_STREAM_V_keep_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_keep_V_0_sel_wr;
  wire INPUT_STREAM_V_keep_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_keep_V_0_state;
  wire \INPUT_STREAM_V_keep_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_strb_V_0_ack_in;
  wire INPUT_STREAM_V_strb_V_0_load_A;
  wire INPUT_STREAM_V_strb_V_0_load_B;
  wire [3:0]INPUT_STREAM_V_strb_V_0_payload_A;
  wire [3:0]INPUT_STREAM_V_strb_V_0_payload_B;
  wire INPUT_STREAM_V_strb_V_0_sel;
  wire INPUT_STREAM_V_strb_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_strb_V_0_sel_wr;
  wire INPUT_STREAM_V_strb_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_strb_V_0_state;
  wire \INPUT_STREAM_V_strb_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ;
  wire INPUT_STREAM_V_user_V_0_ack_in;
  wire [1:0]INPUT_STREAM_V_user_V_0_payload_A;
  wire \INPUT_STREAM_V_user_V_0_payload_A[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_user_V_0_payload_A[1]_i_1_n_4 ;
  wire [1:0]INPUT_STREAM_V_user_V_0_payload_B;
  wire \INPUT_STREAM_V_user_V_0_payload_B[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_user_V_0_payload_B[1]_i_1_n_4 ;
  wire INPUT_STREAM_V_user_V_0_sel;
  wire INPUT_STREAM_V_user_V_0_sel_rd_i_1_n_4;
  wire INPUT_STREAM_V_user_V_0_sel_wr;
  wire INPUT_STREAM_V_user_V_0_sel_wr_i_1_n_4;
  wire [1:1]INPUT_STREAM_V_user_V_0_state;
  wire \INPUT_STREAM_V_user_V_0_state[0]_i_1_n_4 ;
  wire \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ;
  wire [31:0]OUTPUT_STREAM_TDATA;
  wire [5:0]OUTPUT_STREAM_TDEST;
  wire [4:0]OUTPUT_STREAM_TID;
  wire [3:0]OUTPUT_STREAM_TKEEP;
  wire [0:0]OUTPUT_STREAM_TLAST;
  wire OUTPUT_STREAM_TREADY;
  wire [3:0]OUTPUT_STREAM_TSTRB;
  wire [1:0]OUTPUT_STREAM_TUSER;
  wire OUTPUT_STREAM_TVALID;
  wire OUTPUT_STREAM_V_data_V_1_ack_in;
  wire OUTPUT_STREAM_V_data_V_1_load_A;
  wire OUTPUT_STREAM_V_data_V_1_load_B;
  wire [31:0]OUTPUT_STREAM_V_data_V_1_payload_A;
  wire [31:0]OUTPUT_STREAM_V_data_V_1_payload_B;
  wire OUTPUT_STREAM_V_data_V_1_sel;
  wire OUTPUT_STREAM_V_data_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_data_V_1_sel_wr;
  wire OUTPUT_STREAM_V_data_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_data_V_1_state;
  wire \OUTPUT_STREAM_V_data_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ;
  wire OUTPUT_STREAM_V_dest_V_1_ack_in;
  wire OUTPUT_STREAM_V_dest_V_1_load_A;
  wire OUTPUT_STREAM_V_dest_V_1_load_B;
  wire [5:0]OUTPUT_STREAM_V_dest_V_1_payload_A;
  wire [5:0]OUTPUT_STREAM_V_dest_V_1_payload_B;
  wire OUTPUT_STREAM_V_dest_V_1_sel;
  wire OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_dest_V_1_sel_wr;
  wire OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_dest_V_1_state;
  wire \OUTPUT_STREAM_V_dest_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ;
  wire OUTPUT_STREAM_V_id_V_1_ack_in;
  wire OUTPUT_STREAM_V_id_V_1_load_A;
  wire OUTPUT_STREAM_V_id_V_1_load_B;
  wire [4:0]OUTPUT_STREAM_V_id_V_1_payload_A;
  wire [4:0]OUTPUT_STREAM_V_id_V_1_payload_B;
  wire OUTPUT_STREAM_V_id_V_1_sel;
  wire OUTPUT_STREAM_V_id_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_id_V_1_sel_wr;
  wire OUTPUT_STREAM_V_id_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_id_V_1_state;
  wire \OUTPUT_STREAM_V_id_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ;
  wire OUTPUT_STREAM_V_keep_V_1_ack_in;
  wire OUTPUT_STREAM_V_keep_V_1_load_A;
  wire OUTPUT_STREAM_V_keep_V_1_load_B;
  wire [3:0]OUTPUT_STREAM_V_keep_V_1_payload_A;
  wire [3:0]OUTPUT_STREAM_V_keep_V_1_payload_B;
  wire OUTPUT_STREAM_V_keep_V_1_sel;
  wire OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_keep_V_1_sel_wr;
  wire OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_keep_V_1_state;
  wire \OUTPUT_STREAM_V_keep_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ;
  wire OUTPUT_STREAM_V_last_V_1_ack_in;
  wire OUTPUT_STREAM_V_last_V_1_payload_A;
  wire OUTPUT_STREAM_V_last_V_1_payload_B;
  wire OUTPUT_STREAM_V_last_V_1_sel;
  wire OUTPUT_STREAM_V_last_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_last_V_1_sel_wr;
  wire OUTPUT_STREAM_V_last_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_last_V_1_state;
  wire \OUTPUT_STREAM_V_last_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0] ;
  wire OUTPUT_STREAM_V_strb_V_1_ack_in;
  wire OUTPUT_STREAM_V_strb_V_1_load_A;
  wire OUTPUT_STREAM_V_strb_V_1_load_B;
  wire [3:0]OUTPUT_STREAM_V_strb_V_1_payload_A;
  wire [3:0]OUTPUT_STREAM_V_strb_V_1_payload_B;
  wire OUTPUT_STREAM_V_strb_V_1_sel;
  wire OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_strb_V_1_sel_wr;
  wire OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_strb_V_1_state;
  wire \OUTPUT_STREAM_V_strb_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ;
  wire OUTPUT_STREAM_V_user_V_1_ack_in;
  wire [1:0]OUTPUT_STREAM_V_user_V_1_payload_A;
  wire [1:0]OUTPUT_STREAM_V_user_V_1_payload_B;
  wire OUTPUT_STREAM_V_user_V_1_sel;
  wire OUTPUT_STREAM_V_user_V_1_sel_rd_i_1_n_4;
  wire OUTPUT_STREAM_V_user_V_1_sel_wr;
  wire OUTPUT_STREAM_V_user_V_1_sel_wr_i_1_n_4;
  wire [1:1]OUTPUT_STREAM_V_user_V_1_state;
  wire \OUTPUT_STREAM_V_user_V_1_state[0]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0] ;
  wire \ap_CS_fsm[5]_i_2_n_4 ;
  wire \ap_CS_fsm[6]_i_2_n_4 ;
  wire \ap_CS_fsm[6]_i_3_n_4 ;
  wire ap_CS_fsm_pp1_stage0;
  wire \ap_CS_fsm_reg_n_4_[0] ;
  wire ap_CS_fsm_state10;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state5;
  wire ap_CS_fsm_state9;
  wire [7:0]ap_NS_fsm;
  wire ap_NS_fsm19_out;
  wire ap_block_pp1_stage0_subdone1_in;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire ap_enable_reg_pp1_iter0_i_1_n_4;
  wire ap_enable_reg_pp1_iter1_i_1_n_4;
  wire ap_enable_reg_pp1_iter1_reg_n_4;
  wire ap_enable_reg_pp1_iter2_i_1_n_4;
  wire ap_enable_reg_pp1_iter2_reg_n_4;
  wire ap_reg_pp1_iter1_exitcond_reg_509;
  wire \ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1_n_4 ;
  wire ap_rst_n;
  wire ce0;
  wire d0;
  wire \exitcond_reg_509[0]_i_1_n_4 ;
  wire \exitcond_reg_509_reg_n_4_[0] ;
  wire i_1_reg_356;
  wire i_1_reg_3560;
  wire \i_1_reg_356[5]_i_4_n_4 ;
  wire [5:0]i_1_reg_356_reg__0;
  wire [5:0]i_2_fu_397_p2;
  wire [5:0]i_3_fu_455_p2;
  wire \i_reg_345[5]_i_1_n_4 ;
  wire \i_reg_345_reg_n_4_[0] ;
  wire \i_reg_345_reg_n_4_[1] ;
  wire \i_reg_345_reg_n_4_[2] ;
  wire \i_reg_345_reg_n_4_[3] ;
  wire \i_reg_345_reg_n_4_[4] ;
  wire [6:0]indvarinc_fu_367_p2;
  wire [6:0]indvarinc_reg_479;
  wire \indvarinc_reg_479[6]_i_2_n_4 ;
  wire inputValues_1_U_n_4;
  wire inputValues_1_U_n_5;
  wire inputValues_1_U_n_6;
  wire inputValues_1_U_n_7;
  wire inputValues_2_U_n_4;
  wire inputValues_2_U_n_5;
  wire inputValues_2_U_n_6;
  wire inputValues_2_U_n_7;
  wire inputValues_3_U_n_4;
  wire inputValues_3_U_n_5;
  wire inputValues_3_U_n_6;
  wire inputValues_3_U_n_7;
  wire inputValues_4_U_n_5;
  wire inputValues_4_U_n_6;
  wire inputValues_5_U_n_4;
  wire inputValues_5_U_n_5;
  wire inputValues_5_U_n_6;
  wire inputValues_5_U_n_7;
  wire inputValues_5_U_n_8;
  wire inputValues_6_U_n_10;
  wire inputValues_6_U_n_11;
  wire inputValues_6_U_n_12;
  wire inputValues_6_U_n_13;
  wire inputValues_6_U_n_14;
  wire inputValues_6_U_n_15;
  wire inputValues_6_U_n_16;
  wire inputValues_6_U_n_17;
  wire inputValues_6_U_n_18;
  wire inputValues_6_U_n_19;
  wire inputValues_6_U_n_20;
  wire inputValues_6_U_n_21;
  wire inputValues_6_U_n_4;
  wire inputValues_6_U_n_5;
  wire inputValues_6_U_n_7;
  wire inputValues_6_U_n_8;
  wire inputValues_6_U_n_9;
  wire [31:0]input_q0;
  wire interrupt;
  wire invdar_reg_334;
  wire invdar_reg_3340;
  wire \invdar_reg_334_reg_n_4_[0] ;
  wire \invdar_reg_334_reg_n_4_[1] ;
  wire \invdar_reg_334_reg_n_4_[2] ;
  wire \invdar_reg_334_reg_n_4_[3] ;
  wire \invdar_reg_334_reg_n_4_[4] ;
  wire \invdar_reg_334_reg_n_4_[5] ;
  wire \invdar_reg_334_reg_n_4_[6] ;
  wire reset;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire \tmp_1_reg_494[0]_i_1_n_4 ;
  wire \tmp_1_reg_494[0]_i_2_n_4 ;
  wire \tmp_1_reg_494_reg_n_4_[0] ;
  wire tmp_2_fu_389_p3;
  wire [31:0]tmp_data_V_1_fu_472_p2;
  wire [6:0]tmp_reg_484;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.D(ap_NS_fsm[1:0]),
        .I1(input_q0),
        .OUTPUT_STREAM_V_data_V_1_ack_in(OUTPUT_STREAM_V_data_V_1_ack_in),
        .OUTPUT_STREAM_V_dest_V_1_ack_in(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .OUTPUT_STREAM_V_id_V_1_ack_in(OUTPUT_STREAM_V_id_V_1_ack_in),
        .OUTPUT_STREAM_V_keep_V_1_ack_in(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .OUTPUT_STREAM_V_last_V_1_ack_in(OUTPUT_STREAM_V_last_V_1_ack_in),
        .OUTPUT_STREAM_V_strb_V_1_ack_in(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .OUTPUT_STREAM_V_user_V_1_ack_in(OUTPUT_STREAM_V_user_V_1_ack_in),
        .Q({ap_CS_fsm_state10,ap_CS_fsm_state3,\ap_CS_fsm_reg_n_4_[0] }),
        .SR(invdar_reg_334),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .\int_ap_return_reg[31]_0 (Adder2_CONTROL_BUS_s_axi_U_n_12),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .reset(reset),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA({s_axi_CONTROL_BUS_WDATA[7],s_axi_CONTROL_BUS_WDATA[1:0]}),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB[0]),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .\tmp_1_reg_494_reg[0] (\tmp_1_reg_494_reg_n_4_[0] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb Adder2_stream_strea_U
       (.DIADI({Adder2_stream_strea_U_n_4,Adder2_stream_strea_U_n_5,Adder2_stream_strea_U_n_6,Adder2_stream_strea_U_n_7,Adder2_stream_strea_U_n_8}),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[4] (INPUT_STREAM_V_data_V_0_payload_B[4:0]),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4:0]),
        .\ap_CS_fsm_reg[3] ({ap_CS_fsm_state4,ce0}),
        .ap_clk(ap_clk),
        .\invdar_reg_334_reg[6] ({\invdar_reg_334_reg_n_4_[6] ,\invdar_reg_334_reg_n_4_[5] ,\invdar_reg_334_reg_n_4_[4] ,\invdar_reg_334_reg_n_4_[3] ,\invdar_reg_334_reg_n_4_[2] ,\invdar_reg_334_reg_n_4_[1] ,\invdar_reg_334_reg_n_4_[0] }));
  GND GND
       (.G(\<const0> ));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_A));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_B));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hDF20)) 
    INPUT_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I1(tmp_2_fu_389_p3),
        .I2(ap_CS_fsm_state4),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_data_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_data_V_0_sel_wr),
        .R(reset));
  LUT6 #(
    .INIT(64'hFDFFF00000000000)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_V_data_V_0_ack_in),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I5(ap_rst_n),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h4FFF4F4F)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(tmp_2_fu_389_p3),
        .I1(ap_CS_fsm_state4),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(INPUT_STREAM_TVALID),
        .I4(INPUT_STREAM_V_data_V_0_ack_in),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(INPUT_STREAM_V_data_V_0_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_dest_V_0_payload_A[5]_i_1 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_TREADY),
        .I2(INPUT_STREAM_V_dest_V_0_sel_wr),
        .O(INPUT_STREAM_V_dest_V_0_load_A));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_A),
        .D(INPUT_STREAM_TDEST[0]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_A),
        .D(INPUT_STREAM_TDEST[1]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_A),
        .D(INPUT_STREAM_TDEST[2]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_A),
        .D(INPUT_STREAM_TDEST[3]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_A),
        .D(INPUT_STREAM_TDEST[4]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_A),
        .D(INPUT_STREAM_TDEST[5]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A[5]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_dest_V_0_payload_B[5]_i_1 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_TREADY),
        .I2(INPUT_STREAM_V_dest_V_0_sel_wr),
        .O(INPUT_STREAM_V_dest_V_0_load_B));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_B),
        .D(INPUT_STREAM_TDEST[0]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_B),
        .D(INPUT_STREAM_TDEST[1]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_B),
        .D(INPUT_STREAM_TDEST[2]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_B),
        .D(INPUT_STREAM_TDEST[3]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_B),
        .D(INPUT_STREAM_TDEST[4]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_dest_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_dest_V_0_load_B),
        .D(INPUT_STREAM_TDEST[5]),
        .Q(INPUT_STREAM_V_dest_V_0_payload_B[5]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_dest_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_dest_V_0_sel),
        .O(INPUT_STREAM_V_dest_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_dest_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_dest_V_0_sel),
        .R(reset));
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_dest_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_TREADY),
        .I2(INPUT_STREAM_V_dest_V_0_sel_wr),
        .O(INPUT_STREAM_V_dest_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_dest_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_dest_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hF8B80000)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_TREADY),
        .I2(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .I3(inputValues_6_U_n_4),
        .I4(ap_rst_n),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_TREADY),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_id_V_0_payload_A[4]_i_1 
       (.I0(\INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_id_V_0_ack_in),
        .I2(INPUT_STREAM_V_id_V_0_sel_wr),
        .O(INPUT_STREAM_V_id_V_0_load_A));
  FDRE \INPUT_STREAM_V_id_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_A),
        .D(INPUT_STREAM_TID[0]),
        .Q(INPUT_STREAM_V_id_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_A),
        .D(INPUT_STREAM_TID[1]),
        .Q(INPUT_STREAM_V_id_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_A),
        .D(INPUT_STREAM_TID[2]),
        .Q(INPUT_STREAM_V_id_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_A),
        .D(INPUT_STREAM_TID[3]),
        .Q(INPUT_STREAM_V_id_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_A),
        .D(INPUT_STREAM_TID[4]),
        .Q(INPUT_STREAM_V_id_V_0_payload_A[4]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_id_V_0_payload_B[4]_i_1 
       (.I0(\INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_id_V_0_ack_in),
        .I2(INPUT_STREAM_V_id_V_0_sel_wr),
        .O(INPUT_STREAM_V_id_V_0_load_B));
  FDRE \INPUT_STREAM_V_id_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_B),
        .D(INPUT_STREAM_TID[0]),
        .Q(INPUT_STREAM_V_id_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_B),
        .D(INPUT_STREAM_TID[1]),
        .Q(INPUT_STREAM_V_id_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_B),
        .D(INPUT_STREAM_TID[2]),
        .Q(INPUT_STREAM_V_id_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_B),
        .D(INPUT_STREAM_TID[3]),
        .Q(INPUT_STREAM_V_id_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_id_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_id_V_0_load_B),
        .D(INPUT_STREAM_TID[4]),
        .Q(INPUT_STREAM_V_id_V_0_payload_B[4]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_id_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_id_V_0_sel),
        .O(INPUT_STREAM_V_id_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_id_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_id_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_id_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_id_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_id_V_0_ack_in),
        .I2(INPUT_STREAM_V_id_V_0_sel_wr),
        .O(INPUT_STREAM_V_id_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_id_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_id_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_id_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hF8B80000)) 
    \INPUT_STREAM_V_id_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_id_V_0_ack_in),
        .I2(\INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ),
        .I3(inputValues_6_U_n_4),
        .I4(ap_rst_n),
        .O(\INPUT_STREAM_V_id_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_id_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_id_V_0_ack_in),
        .O(INPUT_STREAM_V_id_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_id_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_id_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_id_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_id_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_id_V_0_state),
        .Q(INPUT_STREAM_V_id_V_0_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_keep_V_0_payload_A[3]_i_1 
       (.I0(\INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_keep_V_0_ack_in),
        .I2(INPUT_STREAM_V_keep_V_0_sel_wr),
        .O(INPUT_STREAM_V_keep_V_0_load_A));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_A),
        .D(INPUT_STREAM_TKEEP[0]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_A),
        .D(INPUT_STREAM_TKEEP[1]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_A),
        .D(INPUT_STREAM_TKEEP[2]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_A),
        .D(INPUT_STREAM_TKEEP[3]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_A[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_keep_V_0_payload_B[3]_i_1 
       (.I0(\INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_keep_V_0_ack_in),
        .I2(INPUT_STREAM_V_keep_V_0_sel_wr),
        .O(INPUT_STREAM_V_keep_V_0_load_B));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_B),
        .D(INPUT_STREAM_TKEEP[0]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_B),
        .D(INPUT_STREAM_TKEEP[1]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_B),
        .D(INPUT_STREAM_TKEEP[2]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_keep_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_keep_V_0_load_B),
        .D(INPUT_STREAM_TKEEP[3]),
        .Q(INPUT_STREAM_V_keep_V_0_payload_B[3]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_keep_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_keep_V_0_sel),
        .O(INPUT_STREAM_V_keep_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_keep_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_keep_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_keep_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_keep_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_keep_V_0_ack_in),
        .I2(INPUT_STREAM_V_keep_V_0_sel_wr),
        .O(INPUT_STREAM_V_keep_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_keep_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_keep_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_keep_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hF8B80000)) 
    \INPUT_STREAM_V_keep_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_keep_V_0_ack_in),
        .I2(\INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ),
        .I3(inputValues_6_U_n_4),
        .I4(ap_rst_n),
        .O(\INPUT_STREAM_V_keep_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_keep_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_keep_V_0_ack_in),
        .O(INPUT_STREAM_V_keep_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_keep_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_keep_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_keep_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_keep_V_0_state),
        .Q(INPUT_STREAM_V_keep_V_0_ack_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(INPUT_STREAM_V_last_V_0_sel_wr),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(INPUT_STREAM_V_last_V_0_sel_wr),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hF8B80000)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I3(inputValues_6_U_n_4),
        .I4(ap_rst_n),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_last_V_0_ack_in),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_strb_V_0_payload_A[3]_i_1 
       (.I0(\INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_strb_V_0_ack_in),
        .I2(INPUT_STREAM_V_strb_V_0_sel_wr),
        .O(INPUT_STREAM_V_strb_V_0_load_A));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_A),
        .D(INPUT_STREAM_TSTRB[0]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_A),
        .D(INPUT_STREAM_TSTRB[1]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_A),
        .D(INPUT_STREAM_TSTRB[2]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_A),
        .D(INPUT_STREAM_TSTRB[3]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_A[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_strb_V_0_payload_B[3]_i_1 
       (.I0(\INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ),
        .I1(INPUT_STREAM_V_strb_V_0_ack_in),
        .I2(INPUT_STREAM_V_strb_V_0_sel_wr),
        .O(INPUT_STREAM_V_strb_V_0_load_B));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_B),
        .D(INPUT_STREAM_TSTRB[0]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_B),
        .D(INPUT_STREAM_TSTRB[1]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_B),
        .D(INPUT_STREAM_TSTRB[2]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_strb_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_strb_V_0_load_B),
        .D(INPUT_STREAM_TSTRB[3]),
        .Q(INPUT_STREAM_V_strb_V_0_payload_B[3]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_strb_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_strb_V_0_sel),
        .O(INPUT_STREAM_V_strb_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_strb_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_strb_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_strb_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_strb_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_strb_V_0_ack_in),
        .I2(INPUT_STREAM_V_strb_V_0_sel_wr),
        .O(INPUT_STREAM_V_strb_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_strb_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_strb_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_strb_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hF8B80000)) 
    \INPUT_STREAM_V_strb_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_strb_V_0_ack_in),
        .I2(\INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ),
        .I3(inputValues_6_U_n_4),
        .I4(ap_rst_n),
        .O(\INPUT_STREAM_V_strb_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_strb_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_strb_V_0_ack_in),
        .O(INPUT_STREAM_V_strb_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_strb_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_strb_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_strb_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_strb_V_0_state),
        .Q(INPUT_STREAM_V_strb_V_0_ack_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \INPUT_STREAM_V_user_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TUSER[0]),
        .I1(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_user_V_0_ack_in),
        .I3(INPUT_STREAM_V_user_V_0_sel_wr),
        .I4(INPUT_STREAM_V_user_V_0_payload_A[0]),
        .O(\INPUT_STREAM_V_user_V_0_payload_A[0]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \INPUT_STREAM_V_user_V_0_payload_A[1]_i_1 
       (.I0(INPUT_STREAM_TUSER[1]),
        .I1(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_user_V_0_ack_in),
        .I3(INPUT_STREAM_V_user_V_0_sel_wr),
        .I4(INPUT_STREAM_V_user_V_0_payload_A[1]),
        .O(\INPUT_STREAM_V_user_V_0_payload_A[1]_i_1_n_4 ));
  FDRE \INPUT_STREAM_V_user_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_user_V_0_payload_A[0]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_user_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_user_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_user_V_0_payload_A[1]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_user_V_0_payload_A[1]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \INPUT_STREAM_V_user_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TUSER[0]),
        .I1(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_user_V_0_ack_in),
        .I3(INPUT_STREAM_V_user_V_0_sel_wr),
        .I4(INPUT_STREAM_V_user_V_0_payload_B[0]),
        .O(\INPUT_STREAM_V_user_V_0_payload_B[0]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \INPUT_STREAM_V_user_V_0_payload_B[1]_i_1 
       (.I0(INPUT_STREAM_TUSER[1]),
        .I1(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I2(INPUT_STREAM_V_user_V_0_ack_in),
        .I3(INPUT_STREAM_V_user_V_0_sel_wr),
        .I4(INPUT_STREAM_V_user_V_0_payload_B[1]),
        .O(\INPUT_STREAM_V_user_V_0_payload_B[1]_i_1_n_4 ));
  FDRE \INPUT_STREAM_V_user_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_user_V_0_payload_B[0]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_user_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_user_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_user_V_0_payload_B[1]_i_1_n_4 ),
        .Q(INPUT_STREAM_V_user_V_0_payload_B[1]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_user_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_V_user_V_0_sel),
        .O(INPUT_STREAM_V_user_V_0_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_user_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_user_V_0_sel_rd_i_1_n_4),
        .Q(INPUT_STREAM_V_user_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_user_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_user_V_0_ack_in),
        .I2(INPUT_STREAM_V_user_V_0_sel_wr),
        .O(INPUT_STREAM_V_user_V_0_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_user_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_user_V_0_sel_wr_i_1_n_4),
        .Q(INPUT_STREAM_V_user_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hF8B80000)) 
    \INPUT_STREAM_V_user_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_user_V_0_ack_in),
        .I2(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I3(inputValues_6_U_n_4),
        .I4(ap_rst_n),
        .O(\INPUT_STREAM_V_user_V_0_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_user_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_2_fu_389_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I3(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_user_V_0_ack_in),
        .O(INPUT_STREAM_V_user_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_user_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_user_V_0_state[0]_i_1_n_4 ),
        .Q(\INPUT_STREAM_V_user_V_0_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_user_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_user_V_0_state),
        .Q(INPUT_STREAM_V_user_V_0_ack_in),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[0]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[0]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[10]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[10]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[10]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[11]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[11]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[11]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[12]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[12]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[12]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[13]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[13]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[13]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[14]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[14]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[14]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[15]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[15]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[15]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[16]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[16]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[16]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[17]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[17]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[17]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[18]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[18]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[18]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[19]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[19]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[19]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[1]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[1]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[1]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[20]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[20]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[20]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[21]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[21]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[21]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[22]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[22]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[22]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[23]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[23]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[23]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[24]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[24]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[24]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[24]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[25]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[25]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[25]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[25]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[26]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[26]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[26]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[26]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[27]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[27]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[27]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[27]));
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[28]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[28]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[28]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[28]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[29]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[29]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[29]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[29]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[2]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[2]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[2]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[30]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[30]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[30]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[30]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[31]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[31]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[31]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[31]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[3]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[3]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[3]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[4]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[4]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[4]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[5]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[5]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[5]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[6]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[6]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[6]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[7]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[7]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[7]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[8]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[8]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[8]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \OUTPUT_STREAM_TDATA[9]_INST_0 
       (.I0(OUTPUT_STREAM_V_data_V_1_payload_B[9]),
        .I1(OUTPUT_STREAM_V_data_V_1_payload_A[9]),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_TDATA[9]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TDEST[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_dest_V_1_payload_B[0]),
        .I1(OUTPUT_STREAM_V_dest_V_1_sel),
        .I2(OUTPUT_STREAM_V_dest_V_1_payload_A[0]),
        .O(OUTPUT_STREAM_TDEST[0]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TDEST[1]_INST_0 
       (.I0(OUTPUT_STREAM_V_dest_V_1_payload_B[1]),
        .I1(OUTPUT_STREAM_V_dest_V_1_sel),
        .I2(OUTPUT_STREAM_V_dest_V_1_payload_A[1]),
        .O(OUTPUT_STREAM_TDEST[1]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TDEST[2]_INST_0 
       (.I0(OUTPUT_STREAM_V_dest_V_1_payload_B[2]),
        .I1(OUTPUT_STREAM_V_dest_V_1_sel),
        .I2(OUTPUT_STREAM_V_dest_V_1_payload_A[2]),
        .O(OUTPUT_STREAM_TDEST[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TDEST[3]_INST_0 
       (.I0(OUTPUT_STREAM_V_dest_V_1_payload_B[3]),
        .I1(OUTPUT_STREAM_V_dest_V_1_sel),
        .I2(OUTPUT_STREAM_V_dest_V_1_payload_A[3]),
        .O(OUTPUT_STREAM_TDEST[3]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TDEST[4]_INST_0 
       (.I0(OUTPUT_STREAM_V_dest_V_1_payload_B[4]),
        .I1(OUTPUT_STREAM_V_dest_V_1_sel),
        .I2(OUTPUT_STREAM_V_dest_V_1_payload_A[4]),
        .O(OUTPUT_STREAM_TDEST[4]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TDEST[5]_INST_0 
       (.I0(OUTPUT_STREAM_V_dest_V_1_payload_B[5]),
        .I1(OUTPUT_STREAM_V_dest_V_1_sel),
        .I2(OUTPUT_STREAM_V_dest_V_1_payload_A[5]),
        .O(OUTPUT_STREAM_TDEST[5]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TID[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_id_V_1_payload_B[0]),
        .I1(OUTPUT_STREAM_V_id_V_1_sel),
        .I2(OUTPUT_STREAM_V_id_V_1_payload_A[0]),
        .O(OUTPUT_STREAM_TID[0]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TID[1]_INST_0 
       (.I0(OUTPUT_STREAM_V_id_V_1_payload_B[1]),
        .I1(OUTPUT_STREAM_V_id_V_1_sel),
        .I2(OUTPUT_STREAM_V_id_V_1_payload_A[1]),
        .O(OUTPUT_STREAM_TID[1]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TID[2]_INST_0 
       (.I0(OUTPUT_STREAM_V_id_V_1_payload_B[2]),
        .I1(OUTPUT_STREAM_V_id_V_1_sel),
        .I2(OUTPUT_STREAM_V_id_V_1_payload_A[2]),
        .O(OUTPUT_STREAM_TID[2]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TID[3]_INST_0 
       (.I0(OUTPUT_STREAM_V_id_V_1_payload_B[3]),
        .I1(OUTPUT_STREAM_V_id_V_1_sel),
        .I2(OUTPUT_STREAM_V_id_V_1_payload_A[3]),
        .O(OUTPUT_STREAM_TID[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TID[4]_INST_0 
       (.I0(OUTPUT_STREAM_V_id_V_1_payload_B[4]),
        .I1(OUTPUT_STREAM_V_id_V_1_sel),
        .I2(OUTPUT_STREAM_V_id_V_1_payload_A[4]),
        .O(OUTPUT_STREAM_TID[4]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TKEEP[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_keep_V_1_payload_B[0]),
        .I1(OUTPUT_STREAM_V_keep_V_1_sel),
        .I2(OUTPUT_STREAM_V_keep_V_1_payload_A[0]),
        .O(OUTPUT_STREAM_TKEEP[0]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TKEEP[1]_INST_0 
       (.I0(OUTPUT_STREAM_V_keep_V_1_payload_B[1]),
        .I1(OUTPUT_STREAM_V_keep_V_1_sel),
        .I2(OUTPUT_STREAM_V_keep_V_1_payload_A[1]),
        .O(OUTPUT_STREAM_TKEEP[1]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TKEEP[2]_INST_0 
       (.I0(OUTPUT_STREAM_V_keep_V_1_payload_B[2]),
        .I1(OUTPUT_STREAM_V_keep_V_1_sel),
        .I2(OUTPUT_STREAM_V_keep_V_1_payload_A[2]),
        .O(OUTPUT_STREAM_TKEEP[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TKEEP[3]_INST_0 
       (.I0(OUTPUT_STREAM_V_keep_V_1_payload_B[3]),
        .I1(OUTPUT_STREAM_V_keep_V_1_sel),
        .I2(OUTPUT_STREAM_V_keep_V_1_payload_A[3]),
        .O(OUTPUT_STREAM_TKEEP[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TLAST[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_last_V_1_payload_B),
        .I1(OUTPUT_STREAM_V_last_V_1_sel),
        .I2(OUTPUT_STREAM_V_last_V_1_payload_A),
        .O(OUTPUT_STREAM_TLAST));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TSTRB[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_strb_V_1_payload_B[0]),
        .I1(OUTPUT_STREAM_V_strb_V_1_sel),
        .I2(OUTPUT_STREAM_V_strb_V_1_payload_A[0]),
        .O(OUTPUT_STREAM_TSTRB[0]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TSTRB[1]_INST_0 
       (.I0(OUTPUT_STREAM_V_strb_V_1_payload_B[1]),
        .I1(OUTPUT_STREAM_V_strb_V_1_sel),
        .I2(OUTPUT_STREAM_V_strb_V_1_payload_A[1]),
        .O(OUTPUT_STREAM_TSTRB[1]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TSTRB[2]_INST_0 
       (.I0(OUTPUT_STREAM_V_strb_V_1_payload_B[2]),
        .I1(OUTPUT_STREAM_V_strb_V_1_sel),
        .I2(OUTPUT_STREAM_V_strb_V_1_payload_A[2]),
        .O(OUTPUT_STREAM_TSTRB[2]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TSTRB[3]_INST_0 
       (.I0(OUTPUT_STREAM_V_strb_V_1_payload_B[3]),
        .I1(OUTPUT_STREAM_V_strb_V_1_sel),
        .I2(OUTPUT_STREAM_V_strb_V_1_payload_A[3]),
        .O(OUTPUT_STREAM_TSTRB[3]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TUSER[0]_INST_0 
       (.I0(OUTPUT_STREAM_V_user_V_1_payload_B[0]),
        .I1(OUTPUT_STREAM_V_user_V_1_sel),
        .I2(OUTPUT_STREAM_V_user_V_1_payload_A[0]),
        .O(OUTPUT_STREAM_TUSER[0]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \OUTPUT_STREAM_TUSER[1]_INST_0 
       (.I0(OUTPUT_STREAM_V_user_V_1_payload_B[1]),
        .I1(OUTPUT_STREAM_V_user_V_1_sel),
        .I2(OUTPUT_STREAM_V_user_V_1_payload_A[1]),
        .O(OUTPUT_STREAM_TUSER[1]));
  LUT3 #(
    .INIT(8'h0D)) 
    \OUTPUT_STREAM_V_data_V_1_payload_A[31]_i_1 
       (.I0(\OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_data_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_data_V_1_load_A));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[0]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[10]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[10]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[11]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[11]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[12]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[12]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[13]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[13]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[14]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[14]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[15]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[15]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[16]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[16]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[17]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[17]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[18]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[18]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[19]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[19]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[1]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[20]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[20]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[21]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[21]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[22]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[22]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[23]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[23]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[24]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[24]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[25]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[25]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[26]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[26]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[27]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[27]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[28]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[28]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[29]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[29]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[2]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[30]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[30]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[31]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[31]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[3]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[4]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[5]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[5]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[6]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[6]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[7]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[7]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[8]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[8]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_A),
        .D(tmp_data_V_1_fu_472_p2[9]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \OUTPUT_STREAM_V_data_V_1_payload_B[31]_i_1 
       (.I0(\OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_data_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_data_V_1_load_B));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[0]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[10]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[10]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[11]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[11]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[12]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[12]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[13]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[13]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[14]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[14]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[15]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[15]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[16]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[16]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[17]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[17]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[18]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[18]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[19]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[19]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[1]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[20]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[20]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[21]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[21]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[22]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[22]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[23]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[23]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[24]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[24]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[25]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[25]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[26]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[26]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[27]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[27]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[28]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[28]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[29]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[29]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[2]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[30]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[30]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[31]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[31]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[3]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[4]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[5]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[5]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[6]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[6]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[7]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[7]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[8]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[8]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_data_V_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_data_V_1_load_B),
        .D(tmp_data_V_1_fu_472_p2[9]),
        .Q(OUTPUT_STREAM_V_data_V_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_data_V_1_sel_rd_i_1
       (.I0(OUTPUT_STREAM_TREADY),
        .I1(\OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ),
        .I2(OUTPUT_STREAM_V_data_V_1_sel),
        .O(OUTPUT_STREAM_V_data_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_data_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_data_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_data_V_1_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    OUTPUT_STREAM_V_data_V_1_sel_wr_i_1
       (.I0(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I1(ap_enable_reg_pp1_iter1_reg_n_4),
        .I2(\exitcond_reg_509_reg_n_4_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(OUTPUT_STREAM_V_data_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_data_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_data_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_data_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_data_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F550000)) 
    \OUTPUT_STREAM_V_data_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_data_V_1_state[0]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'hFDFDDDFDFDFDFDFD)) 
    \OUTPUT_STREAM_V_data_V_1_state[1]_i_1 
       (.I0(\OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I3(ap_enable_reg_pp1_iter1_reg_n_4),
        .I4(\exitcond_reg_509_reg_n_4_[0] ),
        .I5(ap_CS_fsm_pp1_stage0),
        .O(OUTPUT_STREAM_V_data_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_data_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_data_V_1_state[0]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_data_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_data_V_1_state),
        .Q(OUTPUT_STREAM_V_data_V_1_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \OUTPUT_STREAM_V_dest_V_1_payload_A[5]_i_1 
       (.I0(OUTPUT_STREAM_TVALID),
        .I1(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_dest_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_dest_V_1_load_A));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_A),
        .D(inputValues_6_U_n_21),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_A),
        .D(inputValues_6_U_n_20),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_A),
        .D(inputValues_6_U_n_19),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_A),
        .D(inputValues_6_U_n_18),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_A),
        .D(inputValues_6_U_n_17),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_A[4]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_A),
        .D(inputValues_6_U_n_16),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_A[5]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \OUTPUT_STREAM_V_dest_V_1_payload_B[5]_i_1 
       (.I0(OUTPUT_STREAM_TVALID),
        .I1(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_dest_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_dest_V_1_load_B));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_B),
        .D(inputValues_6_U_n_21),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_B),
        .D(inputValues_6_U_n_20),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_B),
        .D(inputValues_6_U_n_19),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_B),
        .D(inputValues_6_U_n_18),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_B),
        .D(inputValues_6_U_n_17),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_B[4]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_dest_V_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_dest_V_1_load_B),
        .D(inputValues_6_U_n_16),
        .Q(OUTPUT_STREAM_V_dest_V_1_payload_B[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1
       (.I0(OUTPUT_STREAM_TREADY),
        .I1(OUTPUT_STREAM_TVALID),
        .I2(OUTPUT_STREAM_V_dest_V_1_sel),
        .O(OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_dest_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_dest_V_1_sel),
        .R(reset));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I5(OUTPUT_STREAM_V_dest_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_dest_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_dest_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F500000)) 
    \OUTPUT_STREAM_V_dest_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I3(OUTPUT_STREAM_TVALID),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .O(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hFDDD)) 
    \OUTPUT_STREAM_V_dest_V_1_state[1]_i_1 
       (.I0(OUTPUT_STREAM_TVALID),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .O(OUTPUT_STREAM_V_dest_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_dest_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_1_n_4 ),
        .Q(OUTPUT_STREAM_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_dest_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_dest_V_1_state),
        .Q(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \OUTPUT_STREAM_V_id_V_1_payload_A[4]_i_1 
       (.I0(\OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_id_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_id_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_id_V_1_load_A));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_A),
        .D(inputValues_5_U_n_8),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_A),
        .D(inputValues_5_U_n_7),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_A),
        .D(inputValues_5_U_n_6),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_A),
        .D(inputValues_5_U_n_5),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_A[3]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_A),
        .D(inputValues_5_U_n_4),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_A[4]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \OUTPUT_STREAM_V_id_V_1_payload_B[4]_i_1 
       (.I0(\OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_id_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_id_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_id_V_1_load_B));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_B),
        .D(inputValues_5_U_n_8),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_B),
        .D(inputValues_5_U_n_7),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_B),
        .D(inputValues_5_U_n_6),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_B),
        .D(inputValues_5_U_n_5),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_B[3]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_id_V_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_id_V_1_load_B),
        .D(inputValues_5_U_n_4),
        .Q(OUTPUT_STREAM_V_id_V_1_payload_B[4]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_id_V_1_sel_rd_i_1
       (.I0(\OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_id_V_1_sel),
        .O(OUTPUT_STREAM_V_id_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_id_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_id_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_id_V_1_sel),
        .R(reset));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    OUTPUT_STREAM_V_id_V_1_sel_wr_i_1
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_id_V_1_ack_in),
        .I5(OUTPUT_STREAM_V_id_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_id_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_id_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_id_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_id_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F500000)) 
    \OUTPUT_STREAM_V_id_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_id_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_id_V_1_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hFDDD)) 
    \OUTPUT_STREAM_V_id_V_1_state[1]_i_1 
       (.I0(\OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_id_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .O(OUTPUT_STREAM_V_id_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_id_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_id_V_1_state[0]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_id_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_id_V_1_state),
        .Q(OUTPUT_STREAM_V_id_V_1_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \OUTPUT_STREAM_V_keep_V_1_payload_A[3]_i_1 
       (.I0(\OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_keep_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_keep_V_1_load_A));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_A),
        .D(inputValues_1_U_n_7),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_A),
        .D(inputValues_1_U_n_6),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_A),
        .D(inputValues_1_U_n_5),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_A),
        .D(inputValues_1_U_n_4),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_A[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \OUTPUT_STREAM_V_keep_V_1_payload_B[3]_i_1 
       (.I0(\OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_keep_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_keep_V_1_load_B));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_B),
        .D(inputValues_1_U_n_7),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_B),
        .D(inputValues_1_U_n_6),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_B),
        .D(inputValues_1_U_n_5),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_keep_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_keep_V_1_load_B),
        .D(inputValues_1_U_n_4),
        .Q(OUTPUT_STREAM_V_keep_V_1_payload_B[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1
       (.I0(\OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_keep_V_1_sel),
        .O(OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_keep_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_keep_V_1_sel),
        .R(reset));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I5(OUTPUT_STREAM_V_keep_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_keep_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_keep_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F500000)) 
    \OUTPUT_STREAM_V_keep_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_keep_V_1_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFDDD)) 
    \OUTPUT_STREAM_V_keep_V_1_state[1]_i_1 
       (.I0(\OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .O(OUTPUT_STREAM_V_keep_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_keep_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_keep_V_1_state[0]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_keep_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_keep_V_1_state),
        .Q(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .R(reset));
  FDRE \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(inputValues_4_U_n_5),
        .Q(OUTPUT_STREAM_V_last_V_1_payload_A),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(inputValues_4_U_n_6),
        .Q(OUTPUT_STREAM_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_last_V_1_sel_rd_i_1
       (.I0(\OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_last_V_1_sel),
        .O(OUTPUT_STREAM_V_last_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_last_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_last_V_1_sel),
        .R(reset));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    OUTPUT_STREAM_V_last_V_1_sel_wr_i_1
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_last_V_1_ack_in),
        .I5(OUTPUT_STREAM_V_last_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_last_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_last_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_last_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F500000)) 
    \OUTPUT_STREAM_V_last_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_last_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0] ),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_last_V_1_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hFDDD)) 
    \OUTPUT_STREAM_V_last_V_1_state[1]_i_1 
       (.I0(\OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_last_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .O(OUTPUT_STREAM_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_last_V_1_state[0]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_last_V_1_state),
        .Q(OUTPUT_STREAM_V_last_V_1_ack_in),
        .R(reset));
  LUT3 #(
    .INIT(8'h0D)) 
    \OUTPUT_STREAM_V_strb_V_1_payload_A[3]_i_1 
       (.I0(\OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_strb_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_strb_V_1_load_A));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_A),
        .D(inputValues_2_U_n_7),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_A),
        .D(inputValues_2_U_n_6),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_A),
        .D(inputValues_2_U_n_5),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_A[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_A),
        .D(inputValues_2_U_n_4),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_A[3]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \OUTPUT_STREAM_V_strb_V_1_payload_B[3]_i_1 
       (.I0(\OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_strb_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_strb_V_1_load_B));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_B),
        .D(inputValues_2_U_n_7),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_B),
        .D(inputValues_2_U_n_6),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_B[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_B),
        .D(inputValues_2_U_n_5),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_B[2]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_strb_V_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(OUTPUT_STREAM_V_strb_V_1_load_B),
        .D(inputValues_2_U_n_4),
        .Q(OUTPUT_STREAM_V_strb_V_1_payload_B[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1
       (.I0(\OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_strb_V_1_sel),
        .O(OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_strb_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_strb_V_1_sel),
        .R(reset));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .I5(OUTPUT_STREAM_V_strb_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_strb_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_strb_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F500000)) 
    \OUTPUT_STREAM_V_strb_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_strb_V_1_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hFDDD)) 
    \OUTPUT_STREAM_V_strb_V_1_state[1]_i_1 
       (.I0(\OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .O(OUTPUT_STREAM_V_strb_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_strb_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_strb_V_1_state[0]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_strb_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_strb_V_1_state),
        .Q(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .R(reset));
  FDRE \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(inputValues_3_U_n_5),
        .Q(OUTPUT_STREAM_V_user_V_1_payload_A[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(inputValues_3_U_n_4),
        .Q(OUTPUT_STREAM_V_user_V_1_payload_A[1]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(inputValues_3_U_n_7),
        .Q(OUTPUT_STREAM_V_user_V_1_payload_B[0]),
        .R(1'b0));
  FDRE \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(inputValues_3_U_n_6),
        .Q(OUTPUT_STREAM_V_user_V_1_payload_B[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    OUTPUT_STREAM_V_user_V_1_sel_rd_i_1
       (.I0(OUTPUT_STREAM_TREADY),
        .I1(\OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0] ),
        .I2(OUTPUT_STREAM_V_user_V_1_sel),
        .O(OUTPUT_STREAM_V_user_V_1_sel_rd_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_user_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_user_V_1_sel_rd_i_1_n_4),
        .Q(OUTPUT_STREAM_V_user_V_1_sel),
        .R(reset));
  LUT6 #(
    .INIT(64'hDFFFFFFF20000000)) 
    OUTPUT_STREAM_V_user_V_1_sel_wr_i_1
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_509_reg_n_4_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I5(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .O(OUTPUT_STREAM_V_user_V_1_sel_wr_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    OUTPUT_STREAM_V_user_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_user_V_1_sel_wr_i_1_n_4),
        .Q(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'h7F500000)) 
    \OUTPUT_STREAM_V_user_V_1_state[0]_i_1 
       (.I0(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0] ),
        .I4(ap_rst_n),
        .O(\OUTPUT_STREAM_V_user_V_1_state[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'hFDDD)) 
    \OUTPUT_STREAM_V_user_V_1_state[1]_i_1 
       (.I0(\OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0] ),
        .I1(OUTPUT_STREAM_TREADY),
        .I2(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I3(\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4 ),
        .O(OUTPUT_STREAM_V_user_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_user_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\OUTPUT_STREAM_V_user_V_1_state[0]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \OUTPUT_STREAM_V_user_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(OUTPUT_STREAM_V_user_V_1_state),
        .Q(OUTPUT_STREAM_V_user_V_1_ack_in),
        .R(reset));
  LUT6 #(
    .INIT(64'h88888F888F888F88)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_CS_fsm_state3),
        .I1(\tmp_1_reg_494_reg_n_4_[0] ),
        .I2(tmp_2_fu_389_p3),
        .I3(ap_CS_fsm_state4),
        .I4(d0),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'hFEAEAAAA00000000)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(tmp_2_fu_389_p3),
        .I1(INPUT_STREAM_V_last_V_0_payload_A),
        .I2(INPUT_STREAM_V_last_V_0_sel),
        .I3(INPUT_STREAM_V_last_V_0_payload_B),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I5(ap_CS_fsm_state4),
        .O(ap_NS_fsm[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4404)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(ap_enable_reg_pp1_iter2_reg_n_4),
        .I1(ap_CS_fsm_pp1_stage0),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(\ap_CS_fsm[6]_i_2_n_4 ),
        .I4(\ap_CS_fsm[5]_i_2_n_4 ),
        .I5(ap_CS_fsm_state5),
        .O(ap_NS_fsm[5]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hAA00BA00)) 
    \ap_CS_fsm[5]_i_2 
       (.I0(ap_enable_reg_pp1_iter1_reg_n_4),
        .I1(ap_reg_pp1_iter1_exitcond_reg_509),
        .I2(ap_enable_reg_pp1_iter2_reg_n_4),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(OUTPUT_STREAM_V_data_V_1_ack_in),
        .O(\ap_CS_fsm[5]_i_2_n_4 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF01000000)) 
    \ap_CS_fsm[6]_i_1 
       (.I0(\ap_CS_fsm[6]_i_2_n_4 ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_4),
        .I2(ap_enable_reg_pp1_iter2_reg_n_4),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(ap_enable_reg_pp1_iter0),
        .I5(\ap_CS_fsm[6]_i_3_n_4 ),
        .O(ap_NS_fsm[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \ap_CS_fsm[6]_i_2 
       (.I0(i_1_reg_356_reg__0[4]),
        .I1(i_1_reg_356_reg__0[5]),
        .I2(i_1_reg_356_reg__0[2]),
        .I3(i_1_reg_356_reg__0[3]),
        .I4(i_1_reg_356_reg__0[1]),
        .I5(i_1_reg_356_reg__0[0]),
        .O(\ap_CS_fsm[6]_i_2_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h08080800)) 
    \ap_CS_fsm[6]_i_3 
       (.I0(ap_enable_reg_pp1_iter2_reg_n_4),
        .I1(ap_CS_fsm_pp1_stage0),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I4(ap_reg_pp1_iter1_exitcond_reg_509),
        .O(\ap_CS_fsm[6]_i_3_n_4 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF8AAAAAAA)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(ap_CS_fsm_state10),
        .I1(Adder2_CONTROL_BUS_s_axi_U_n_12),
        .I2(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I4(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I5(ap_CS_fsm_state9),
        .O(ap_NS_fsm[7]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_4_[0] ),
        .S(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ce0),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ce0),
        .Q(ap_CS_fsm_state3),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_pp1_stage0),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[6]),
        .Q(ap_CS_fsm_state9),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(ap_CS_fsm_state10),
        .R(reset));
  LUT6 #(
    .INIT(64'hA8A8A8A800A8A8A8)) 
    ap_enable_reg_pp1_iter0_i_1
       (.I0(ap_rst_n),
        .I1(ap_CS_fsm_state5),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(ap_block_pp1_stage0_subdone1_in),
        .I5(\ap_CS_fsm[6]_i_2_n_4 ),
        .O(ap_enable_reg_pp1_iter0_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter0_i_1_n_4),
        .Q(ap_enable_reg_pp1_iter0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h8800A0A0)) 
    ap_enable_reg_pp1_iter1_i_1
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(\ap_CS_fsm[6]_i_2_n_4 ),
        .I4(ap_block_pp1_stage0_subdone1_in),
        .O(ap_enable_reg_pp1_iter1_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter1_i_1_n_4),
        .Q(ap_enable_reg_pp1_iter1_reg_n_4),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h888800A0)) 
    ap_enable_reg_pp1_iter2_i_1
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp1_iter1_reg_n_4),
        .I2(ap_enable_reg_pp1_iter2_reg_n_4),
        .I3(ap_CS_fsm_state5),
        .I4(ap_block_pp1_stage0_subdone1_in),
        .O(ap_enable_reg_pp1_iter2_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter2_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter2_i_1_n_4),
        .Q(ap_enable_reg_pp1_iter2_reg_n_4),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFBFB5000F0F0F0F0)) 
    \ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1 
       (.I0(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I1(ap_enable_reg_pp1_iter2_reg_n_4),
        .I2(ap_reg_pp1_iter1_exitcond_reg_509),
        .I3(ap_enable_reg_pp1_iter1_reg_n_4),
        .I4(\exitcond_reg_509_reg_n_4_[0] ),
        .I5(ap_CS_fsm_pp1_stage0),
        .O(\ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1_n_4 ));
  FDRE \ap_reg_pp1_iter1_exitcond_reg_509_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1_n_4 ),
        .Q(ap_reg_pp1_iter1_exitcond_reg_509),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h7F40)) 
    \exitcond_reg_509[0]_i_1 
       (.I0(\ap_CS_fsm[6]_i_2_n_4 ),
        .I1(ap_block_pp1_stage0_subdone1_in),
        .I2(ap_CS_fsm_pp1_stage0),
        .I3(\exitcond_reg_509_reg_n_4_[0] ),
        .O(\exitcond_reg_509[0]_i_1_n_4 ));
  FDRE \exitcond_reg_509_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_509[0]_i_1_n_4 ),
        .Q(\exitcond_reg_509_reg_n_4_[0] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \i_1_reg_356[0]_i_1 
       (.I0(i_1_reg_356_reg__0[0]),
        .O(i_3_fu_455_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_1_reg_356[1]_i_1 
       (.I0(i_1_reg_356_reg__0[0]),
        .I1(i_1_reg_356_reg__0[1]),
        .O(i_3_fu_455_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_1_reg_356[2]_i_1 
       (.I0(i_1_reg_356_reg__0[1]),
        .I1(i_1_reg_356_reg__0[0]),
        .I2(i_1_reg_356_reg__0[2]),
        .O(i_3_fu_455_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_1_reg_356[3]_i_1 
       (.I0(i_1_reg_356_reg__0[2]),
        .I1(i_1_reg_356_reg__0[0]),
        .I2(i_1_reg_356_reg__0[1]),
        .I3(i_1_reg_356_reg__0[3]),
        .O(i_3_fu_455_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_1_reg_356[4]_i_1 
       (.I0(i_1_reg_356_reg__0[3]),
        .I1(i_1_reg_356_reg__0[1]),
        .I2(i_1_reg_356_reg__0[0]),
        .I3(i_1_reg_356_reg__0[2]),
        .I4(i_1_reg_356_reg__0[4]),
        .O(i_3_fu_455_p2[4]));
  LUT6 #(
    .INIT(64'h10F0F0F0F0F0F0F0)) 
    \i_1_reg_356[5]_i_1 
       (.I0(\i_1_reg_356[5]_i_4_n_4 ),
        .I1(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I2(ap_CS_fsm_state5),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(ap_enable_reg_pp1_iter0),
        .I5(\ap_CS_fsm[6]_i_2_n_4 ),
        .O(i_1_reg_356));
  LUT4 #(
    .INIT(16'h8000)) 
    \i_1_reg_356[5]_i_2 
       (.I0(ap_block_pp1_stage0_subdone1_in),
        .I1(\ap_CS_fsm[6]_i_2_n_4 ),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(ap_CS_fsm_pp1_stage0),
        .O(i_1_reg_3560));
  LUT6 #(
    .INIT(64'h6CCCCCCCCCCCCCCC)) 
    \i_1_reg_356[5]_i_3 
       (.I0(i_1_reg_356_reg__0[4]),
        .I1(i_1_reg_356_reg__0[5]),
        .I2(i_1_reg_356_reg__0[2]),
        .I3(i_1_reg_356_reg__0[0]),
        .I4(i_1_reg_356_reg__0[1]),
        .I5(i_1_reg_356_reg__0[3]),
        .O(i_3_fu_455_p2[5]));
  LUT4 #(
    .INIT(16'hDD0D)) 
    \i_1_reg_356[5]_i_4 
       (.I0(ap_enable_reg_pp1_iter2_reg_n_4),
        .I1(ap_reg_pp1_iter1_exitcond_reg_509),
        .I2(ap_enable_reg_pp1_iter1_reg_n_4),
        .I3(\exitcond_reg_509_reg_n_4_[0] ),
        .O(\i_1_reg_356[5]_i_4_n_4 ));
  FDRE \i_1_reg_356_reg[0] 
       (.C(ap_clk),
        .CE(i_1_reg_3560),
        .D(i_3_fu_455_p2[0]),
        .Q(i_1_reg_356_reg__0[0]),
        .R(i_1_reg_356));
  FDRE \i_1_reg_356_reg[1] 
       (.C(ap_clk),
        .CE(i_1_reg_3560),
        .D(i_3_fu_455_p2[1]),
        .Q(i_1_reg_356_reg__0[1]),
        .R(i_1_reg_356));
  FDRE \i_1_reg_356_reg[2] 
       (.C(ap_clk),
        .CE(i_1_reg_3560),
        .D(i_3_fu_455_p2[2]),
        .Q(i_1_reg_356_reg__0[2]),
        .R(i_1_reg_356));
  FDRE \i_1_reg_356_reg[3] 
       (.C(ap_clk),
        .CE(i_1_reg_3560),
        .D(i_3_fu_455_p2[3]),
        .Q(i_1_reg_356_reg__0[3]),
        .R(i_1_reg_356));
  FDRE \i_1_reg_356_reg[4] 
       (.C(ap_clk),
        .CE(i_1_reg_3560),
        .D(i_3_fu_455_p2[4]),
        .Q(i_1_reg_356_reg__0[4]),
        .R(i_1_reg_356));
  FDRE \i_1_reg_356_reg[5] 
       (.C(ap_clk),
        .CE(i_1_reg_3560),
        .D(i_3_fu_455_p2[5]),
        .Q(i_1_reg_356_reg__0[5]),
        .R(i_1_reg_356));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_345[0]_i_1 
       (.I0(\i_reg_345_reg_n_4_[0] ),
        .O(i_2_fu_397_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_reg_345[1]_i_1 
       (.I0(\i_reg_345_reg_n_4_[0] ),
        .I1(\i_reg_345_reg_n_4_[1] ),
        .O(i_2_fu_397_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_reg_345[2]_i_1 
       (.I0(\i_reg_345_reg_n_4_[1] ),
        .I1(\i_reg_345_reg_n_4_[0] ),
        .I2(\i_reg_345_reg_n_4_[2] ),
        .O(i_2_fu_397_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_reg_345[3]_i_1 
       (.I0(\i_reg_345_reg_n_4_[2] ),
        .I1(\i_reg_345_reg_n_4_[0] ),
        .I2(\i_reg_345_reg_n_4_[1] ),
        .I3(\i_reg_345_reg_n_4_[3] ),
        .O(i_2_fu_397_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_reg_345[4]_i_1 
       (.I0(\i_reg_345_reg_n_4_[3] ),
        .I1(\i_reg_345_reg_n_4_[1] ),
        .I2(\i_reg_345_reg_n_4_[0] ),
        .I3(\i_reg_345_reg_n_4_[2] ),
        .I4(\i_reg_345_reg_n_4_[4] ),
        .O(i_2_fu_397_p2[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \i_reg_345[5]_i_1 
       (.I0(\tmp_1_reg_494_reg_n_4_[0] ),
        .I1(ap_CS_fsm_state3),
        .O(\i_reg_345[5]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h00001D0000000000)) 
    \i_reg_345[5]_i_2 
       (.I0(INPUT_STREAM_V_last_V_0_payload_A),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_B),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .I4(tmp_2_fu_389_p3),
        .I5(ap_CS_fsm_state4),
        .O(ap_NS_fsm19_out));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_reg_345[5]_i_3 
       (.I0(\i_reg_345_reg_n_4_[4] ),
        .I1(\i_reg_345_reg_n_4_[2] ),
        .I2(\i_reg_345_reg_n_4_[0] ),
        .I3(\i_reg_345_reg_n_4_[1] ),
        .I4(\i_reg_345_reg_n_4_[3] ),
        .O(i_2_fu_397_p2[5]));
  FDRE \i_reg_345_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(i_2_fu_397_p2[0]),
        .Q(\i_reg_345_reg_n_4_[0] ),
        .R(\i_reg_345[5]_i_1_n_4 ));
  FDRE \i_reg_345_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(i_2_fu_397_p2[1]),
        .Q(\i_reg_345_reg_n_4_[1] ),
        .R(\i_reg_345[5]_i_1_n_4 ));
  FDRE \i_reg_345_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(i_2_fu_397_p2[2]),
        .Q(\i_reg_345_reg_n_4_[2] ),
        .R(\i_reg_345[5]_i_1_n_4 ));
  FDRE \i_reg_345_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(i_2_fu_397_p2[3]),
        .Q(\i_reg_345_reg_n_4_[3] ),
        .R(\i_reg_345[5]_i_1_n_4 ));
  FDRE \i_reg_345_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(i_2_fu_397_p2[4]),
        .Q(\i_reg_345_reg_n_4_[4] ),
        .R(\i_reg_345[5]_i_1_n_4 ));
  FDRE \i_reg_345_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm19_out),
        .D(i_2_fu_397_p2[5]),
        .Q(tmp_2_fu_389_p3),
        .R(\i_reg_345[5]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \indvarinc_reg_479[0]_i_1 
       (.I0(\invdar_reg_334_reg_n_4_[0] ),
        .O(indvarinc_fu_367_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \indvarinc_reg_479[1]_i_1 
       (.I0(\invdar_reg_334_reg_n_4_[0] ),
        .I1(\invdar_reg_334_reg_n_4_[1] ),
        .O(indvarinc_fu_367_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \indvarinc_reg_479[2]_i_1 
       (.I0(\invdar_reg_334_reg_n_4_[0] ),
        .I1(\invdar_reg_334_reg_n_4_[1] ),
        .I2(\invdar_reg_334_reg_n_4_[2] ),
        .O(indvarinc_fu_367_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \indvarinc_reg_479[3]_i_1 
       (.I0(\invdar_reg_334_reg_n_4_[1] ),
        .I1(\invdar_reg_334_reg_n_4_[0] ),
        .I2(\invdar_reg_334_reg_n_4_[2] ),
        .I3(\invdar_reg_334_reg_n_4_[3] ),
        .O(indvarinc_fu_367_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \indvarinc_reg_479[4]_i_1 
       (.I0(\invdar_reg_334_reg_n_4_[2] ),
        .I1(\invdar_reg_334_reg_n_4_[0] ),
        .I2(\invdar_reg_334_reg_n_4_[1] ),
        .I3(\invdar_reg_334_reg_n_4_[3] ),
        .I4(\invdar_reg_334_reg_n_4_[4] ),
        .O(indvarinc_fu_367_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \indvarinc_reg_479[5]_i_1 
       (.I0(\invdar_reg_334_reg_n_4_[3] ),
        .I1(\invdar_reg_334_reg_n_4_[1] ),
        .I2(\invdar_reg_334_reg_n_4_[0] ),
        .I3(\invdar_reg_334_reg_n_4_[2] ),
        .I4(\invdar_reg_334_reg_n_4_[4] ),
        .I5(\invdar_reg_334_reg_n_4_[5] ),
        .O(indvarinc_fu_367_p2[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \indvarinc_reg_479[6]_i_1 
       (.I0(\indvarinc_reg_479[6]_i_2_n_4 ),
        .I1(\invdar_reg_334_reg_n_4_[5] ),
        .I2(\invdar_reg_334_reg_n_4_[6] ),
        .O(indvarinc_fu_367_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \indvarinc_reg_479[6]_i_2 
       (.I0(\invdar_reg_334_reg_n_4_[4] ),
        .I1(\invdar_reg_334_reg_n_4_[2] ),
        .I2(\invdar_reg_334_reg_n_4_[0] ),
        .I3(\invdar_reg_334_reg_n_4_[1] ),
        .I4(\invdar_reg_334_reg_n_4_[3] ),
        .O(\indvarinc_reg_479[6]_i_2_n_4 ));
  FDRE \indvarinc_reg_479_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[0]),
        .Q(indvarinc_reg_479[0]),
        .R(1'b0));
  FDRE \indvarinc_reg_479_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[1]),
        .Q(indvarinc_reg_479[1]),
        .R(1'b0));
  FDRE \indvarinc_reg_479_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[2]),
        .Q(indvarinc_reg_479[2]),
        .R(1'b0));
  FDRE \indvarinc_reg_479_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[3]),
        .Q(indvarinc_reg_479[3]),
        .R(1'b0));
  FDRE \indvarinc_reg_479_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[4]),
        .Q(indvarinc_reg_479[4]),
        .R(1'b0));
  FDRE \indvarinc_reg_479_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[5]),
        .Q(indvarinc_reg_479[5]),
        .R(1'b0));
  FDRE \indvarinc_reg_479_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_367_p2[6]),
        .Q(indvarinc_reg_479[6]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud inputValues_1_U
       (.E(inputValues_6_U_n_5),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (inputValues_6_U_n_15),
        .\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] (INPUT_STREAM_V_keep_V_0_payload_A),
        .INPUT_STREAM_V_keep_V_0_sel(INPUT_STREAM_V_keep_V_0_sel),
        .\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ({inputValues_1_U_n_4,inputValues_1_U_n_5,inputValues_1_U_n_6,inputValues_1_U_n_7}),
        .Q(INPUT_STREAM_V_keep_V_0_payload_B),
        .\ap_CS_fsm_reg[3] (inputValues_6_U_n_4),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (inputValues_6_U_n_7),
        .\i_reg_345_reg[1] (inputValues_6_U_n_8),
        .\i_reg_345_reg[2] (inputValues_6_U_n_9),
        .\i_reg_345_reg[3] (inputValues_6_U_n_10),
        .\i_reg_345_reg[4] (inputValues_6_U_n_11),
        .\i_reg_345_reg[5] (inputValues_6_U_n_13),
        .\i_reg_345_reg[5]_0 (inputValues_6_U_n_14),
        .\i_reg_345_reg[5]_1 (inputValues_6_U_n_12));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0 inputValues_2_U
       (.E(inputValues_6_U_n_5),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (inputValues_6_U_n_15),
        .\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] (INPUT_STREAM_V_strb_V_0_payload_A),
        .INPUT_STREAM_V_strb_V_0_sel(INPUT_STREAM_V_strb_V_0_sel),
        .\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ({inputValues_2_U_n_4,inputValues_2_U_n_5,inputValues_2_U_n_6,inputValues_2_U_n_7}),
        .Q(INPUT_STREAM_V_strb_V_0_payload_B),
        .\ap_CS_fsm_reg[3] (inputValues_6_U_n_4),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (inputValues_6_U_n_7),
        .\i_reg_345_reg[1] (inputValues_6_U_n_8),
        .\i_reg_345_reg[2] (inputValues_6_U_n_9),
        .\i_reg_345_reg[3] (inputValues_6_U_n_10),
        .\i_reg_345_reg[4] (inputValues_6_U_n_11),
        .\i_reg_345_reg[5] (inputValues_6_U_n_13),
        .\i_reg_345_reg[5]_0 (inputValues_6_U_n_14),
        .\i_reg_345_reg[5]_1 (inputValues_6_U_n_12));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg inputValues_3_U
       (.E(inputValues_6_U_n_5),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (inputValues_6_U_n_15),
        .INPUT_STREAM_V_user_V_0_payload_A(INPUT_STREAM_V_user_V_0_payload_A),
        .INPUT_STREAM_V_user_V_0_payload_B(INPUT_STREAM_V_user_V_0_payload_B),
        .INPUT_STREAM_V_user_V_0_sel(INPUT_STREAM_V_user_V_0_sel),
        .OUTPUT_STREAM_V_user_V_1_ack_in(OUTPUT_STREAM_V_user_V_1_ack_in),
        .OUTPUT_STREAM_V_user_V_1_payload_A(OUTPUT_STREAM_V_user_V_1_payload_A),
        .\OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] (inputValues_3_U_n_5),
        .\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] (inputValues_3_U_n_4),
        .OUTPUT_STREAM_V_user_V_1_payload_B(OUTPUT_STREAM_V_user_V_1_payload_B),
        .\OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] (inputValues_3_U_n_7),
        .\OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] (inputValues_3_U_n_6),
        .OUTPUT_STREAM_V_user_V_1_sel_wr(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .\OUTPUT_STREAM_V_user_V_1_state_reg[0] (\OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0] ),
        .\ap_CS_fsm_reg[3] (inputValues_6_U_n_4),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (inputValues_6_U_n_7),
        .\i_reg_345_reg[1] (inputValues_6_U_n_8),
        .\i_reg_345_reg[2] (inputValues_6_U_n_9),
        .\i_reg_345_reg[3] (inputValues_6_U_n_10),
        .\i_reg_345_reg[4] (inputValues_6_U_n_11),
        .\i_reg_345_reg[5] (inputValues_6_U_n_13),
        .\i_reg_345_reg[5]_0 (inputValues_6_U_n_14),
        .\i_reg_345_reg[5]_1 (inputValues_6_U_n_12));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi inputValues_4_U
       (.E(inputValues_6_U_n_5),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (inputValues_6_U_n_15),
        .INPUT_STREAM_V_last_V_0_payload_A(INPUT_STREAM_V_last_V_0_payload_A),
        .INPUT_STREAM_V_last_V_0_payload_B(INPUT_STREAM_V_last_V_0_payload_B),
        .INPUT_STREAM_V_last_V_0_sel(INPUT_STREAM_V_last_V_0_sel),
        .OUTPUT_STREAM_V_last_V_1_ack_in(OUTPUT_STREAM_V_last_V_1_ack_in),
        .OUTPUT_STREAM_V_last_V_1_payload_A(OUTPUT_STREAM_V_last_V_1_payload_A),
        .\OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] (inputValues_4_U_n_5),
        .OUTPUT_STREAM_V_last_V_1_payload_B(OUTPUT_STREAM_V_last_V_1_payload_B),
        .\OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] (inputValues_4_U_n_6),
        .OUTPUT_STREAM_V_last_V_1_sel_wr(OUTPUT_STREAM_V_last_V_1_sel_wr),
        .\OUTPUT_STREAM_V_last_V_1_state_reg[0] (\OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0] ),
        .\ap_CS_fsm_reg[3] (inputValues_6_U_n_4),
        .ap_clk(ap_clk),
        .d0(d0),
        .\i_reg_345_reg[0] (inputValues_6_U_n_7),
        .\i_reg_345_reg[1] (inputValues_6_U_n_8),
        .\i_reg_345_reg[2] (inputValues_6_U_n_9),
        .\i_reg_345_reg[3] (inputValues_6_U_n_10),
        .\i_reg_345_reg[4] (inputValues_6_U_n_11),
        .\i_reg_345_reg[5] (inputValues_6_U_n_13),
        .\i_reg_345_reg[5]_0 (inputValues_6_U_n_14),
        .\i_reg_345_reg[5]_1 (inputValues_6_U_n_12));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j inputValues_5_U
       (.E(inputValues_6_U_n_5),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (inputValues_6_U_n_15),
        .\INPUT_STREAM_V_id_V_0_payload_A_reg[4] (INPUT_STREAM_V_id_V_0_payload_A),
        .INPUT_STREAM_V_id_V_0_sel(INPUT_STREAM_V_id_V_0_sel),
        .\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ({inputValues_5_U_n_4,inputValues_5_U_n_5,inputValues_5_U_n_6,inputValues_5_U_n_7,inputValues_5_U_n_8}),
        .Q(INPUT_STREAM_V_id_V_0_payload_B),
        .\ap_CS_fsm_reg[3] (inputValues_6_U_n_4),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (inputValues_6_U_n_7),
        .\i_reg_345_reg[1] (inputValues_6_U_n_8),
        .\i_reg_345_reg[2] (inputValues_6_U_n_9),
        .\i_reg_345_reg[3] (inputValues_6_U_n_10),
        .\i_reg_345_reg[4] (inputValues_6_U_n_11),
        .\i_reg_345_reg[5] (inputValues_6_U_n_13),
        .\i_reg_345_reg[5]_0 (inputValues_6_U_n_14),
        .\i_reg_345_reg[5]_1 (inputValues_6_U_n_12));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi inputValues_6_U
       (.E(inputValues_6_U_n_5),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] (INPUT_STREAM_V_dest_V_0_payload_B),
        .INPUT_STREAM_V_dest_V_0_sel(INPUT_STREAM_V_dest_V_0_sel),
        .OUTPUT_STREAM_V_data_V_1_ack_in(OUTPUT_STREAM_V_data_V_1_ack_in),
        .\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ({inputValues_6_U_n_16,inputValues_6_U_n_17,inputValues_6_U_n_18,inputValues_6_U_n_19,inputValues_6_U_n_20,inputValues_6_U_n_21}),
        .Q(INPUT_STREAM_V_dest_V_0_payload_A),
        .\ap_CS_fsm_reg[5] ({ap_CS_fsm_pp1_stage0,ap_CS_fsm_state4}),
        .ap_block_pp1_stage0_subdone1_in(ap_block_pp1_stage0_subdone1_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter0(ap_enable_reg_pp1_iter0),
        .ap_enable_reg_pp1_iter1_reg(ap_enable_reg_pp1_iter1_reg_n_4),
        .ap_enable_reg_pp1_iter2_reg(ap_enable_reg_pp1_iter2_reg_n_4),
        .ap_reg_pp1_iter1_exitcond_reg_509(ap_reg_pp1_iter1_exitcond_reg_509),
        .\exitcond_reg_509_reg[0] (\exitcond_reg_509_reg_n_4_[0] ),
        .\i_1_reg_356_reg[5] (i_1_reg_356_reg__0),
        .\i_reg_345_reg[5] ({tmp_2_fu_389_p3,\i_reg_345_reg_n_4_[4] ,\i_reg_345_reg_n_4_[3] ,\i_reg_345_reg_n_4_[2] ,\i_reg_345_reg_n_4_[1] ,\i_reg_345_reg_n_4_[0] }),
        .\q0_reg[0] (inputValues_6_U_n_7),
        .\q0_reg[0]_0 (inputValues_6_U_n_8),
        .\q0_reg[0]_1 (inputValues_6_U_n_9),
        .\q0_reg[0]_2 (inputValues_6_U_n_10),
        .\q0_reg[0]_3 (inputValues_6_U_n_15),
        .\q0_reg[4] (inputValues_6_U_n_13),
        .\q0_reg[5] (inputValues_6_U_n_4),
        .\q0_reg[5]_0 (inputValues_6_U_n_11),
        .\q0_reg[5]_1 (inputValues_6_U_n_12),
        .\q0_reg[5]_2 (inputValues_6_U_n_14));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input input_U
       (.D(tmp_data_V_1_fu_472_p2),
        .DIADI({Adder2_stream_strea_U_n_4,Adder2_stream_strea_U_n_5,Adder2_stream_strea_U_n_6,Adder2_stream_strea_U_n_7,Adder2_stream_strea_U_n_8}),
        .E(inputValues_6_U_n_5),
        .I1(input_q0),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (INPUT_STREAM_V_data_V_0_payload_A[31:5]),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (INPUT_STREAM_V_data_V_0_payload_B[31:5]),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_4_[0] ),
        .Q({ap_CS_fsm_state9,ap_CS_fsm_pp1_stage0,ap_CS_fsm_state4,ap_CS_fsm_state3}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter0(ap_enable_reg_pp1_iter0),
        .\i_1_reg_356_reg[5] (i_1_reg_356_reg__0),
        .\i_reg_345_reg[5] ({tmp_2_fu_389_p3,\i_reg_345_reg_n_4_[4] ,\i_reg_345_reg_n_4_[3] ,\i_reg_345_reg_n_4_[2] ,\i_reg_345_reg_n_4_[1] ,\i_reg_345_reg_n_4_[0] }),
        .\tmp_reg_484_reg[6] (tmp_reg_484));
  LUT2 #(
    .INIT(4'h2)) 
    \invdar_reg_334[6]_i_2 
       (.I0(ap_CS_fsm_state3),
        .I1(\tmp_1_reg_494_reg_n_4_[0] ),
        .O(invdar_reg_3340));
  FDRE \invdar_reg_334_reg[0] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[0]),
        .Q(\invdar_reg_334_reg_n_4_[0] ),
        .R(invdar_reg_334));
  FDRE \invdar_reg_334_reg[1] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[1]),
        .Q(\invdar_reg_334_reg_n_4_[1] ),
        .R(invdar_reg_334));
  FDRE \invdar_reg_334_reg[2] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[2]),
        .Q(\invdar_reg_334_reg_n_4_[2] ),
        .R(invdar_reg_334));
  FDRE \invdar_reg_334_reg[3] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[3]),
        .Q(\invdar_reg_334_reg_n_4_[3] ),
        .R(invdar_reg_334));
  FDRE \invdar_reg_334_reg[4] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[4]),
        .Q(\invdar_reg_334_reg_n_4_[4] ),
        .R(invdar_reg_334));
  FDRE \invdar_reg_334_reg[5] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[5]),
        .Q(\invdar_reg_334_reg_n_4_[5] ),
        .R(invdar_reg_334));
  FDRE \invdar_reg_334_reg[6] 
       (.C(ap_clk),
        .CE(invdar_reg_3340),
        .D(indvarinc_reg_479[6]),
        .Q(\invdar_reg_334_reg_n_4_[6] ),
        .R(invdar_reg_334));
  LUT6 #(
    .INIT(64'h00000300AAAAAAAA)) 
    \tmp_1_reg_494[0]_i_1 
       (.I0(\tmp_1_reg_494_reg_n_4_[0] ),
        .I1(\invdar_reg_334_reg_n_4_[5] ),
        .I2(\invdar_reg_334_reg_n_4_[1] ),
        .I3(\invdar_reg_334_reg_n_4_[0] ),
        .I4(\tmp_1_reg_494[0]_i_2_n_4 ),
        .I5(ce0),
        .O(\tmp_1_reg_494[0]_i_1_n_4 ));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \tmp_1_reg_494[0]_i_2 
       (.I0(\invdar_reg_334_reg_n_4_[3] ),
        .I1(\invdar_reg_334_reg_n_4_[4] ),
        .I2(\invdar_reg_334_reg_n_4_[2] ),
        .I3(\invdar_reg_334_reg_n_4_[6] ),
        .O(\tmp_1_reg_494[0]_i_2_n_4 ));
  FDRE \tmp_1_reg_494_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_1_reg_494[0]_i_1_n_4 ),
        .Q(\tmp_1_reg_494_reg_n_4_[0] ),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[0] ),
        .Q(tmp_reg_484[0]),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[1] ),
        .Q(tmp_reg_484[1]),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[2] ),
        .Q(tmp_reg_484[2]),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[3] ),
        .Q(tmp_reg_484[3]),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[4] ),
        .Q(tmp_reg_484[4]),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[5] ),
        .Q(tmp_reg_484[5]),
        .R(1'b0));
  FDRE \tmp_reg_484_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_334_reg_n_4_[6] ),
        .Q(tmp_reg_484[6]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb
   (DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4] ,
    INPUT_STREAM_V_data_V_0_sel,
    \ap_CS_fsm_reg[3] ,
    \invdar_reg_334_reg[6] ,
    ap_clk);
  output [4:0]DIADI;
  input [4:0]Q;
  input [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [1:0]\ap_CS_fsm_reg[3] ;
  input [6:0]\invdar_reg_334_reg[6] ;
  input ap_clk;

  wire [4:0]DIADI;
  wire [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire [4:0]Q;
  wire [1:0]\ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire [6:0]\invdar_reg_334_reg[6] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom Adder2_Adder2_strbkb_rom_U
       (.DIADI(DIADI),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[4] (\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .Q(Q),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\invdar_reg_334_reg[6] (\invdar_reg_334_reg[6] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom
   (DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4] ,
    INPUT_STREAM_V_data_V_0_sel,
    \ap_CS_fsm_reg[3] ,
    \invdar_reg_334_reg[6] ,
    ap_clk);
  output [4:0]DIADI;
  input [4:0]Q;
  input [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [1:0]\ap_CS_fsm_reg[3] ;
  input [6:0]\invdar_reg_334_reg[6] ;
  input ap_clk;

  wire [4:0]DIADI;
  wire [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire [4:0]Q;
  wire [1:0]\ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire g0_b0_n_4;
  wire g0_b1_n_4;
  wire g0_b2_n_4;
  wire g0_b3_n_4;
  wire g0_b4_n_4;
  wire g1_b1_n_4;
  wire g1_b2_n_4;
  wire g1_b4_n_4;
  wire [6:0]\invdar_reg_334_reg[6] ;
  wire [4:0]q0;
  wire \q0[0]_i_1_n_4 ;
  wire \q0[1]_i_1_n_4 ;
  wire \q0[3]_i_1__3_n_4 ;
  wire \q0_reg[2]_i_1_n_4 ;
  wire \q0_reg[4]_i_1_n_4 ;

  LUT6 #(
    .INIT(64'h552AA554AA9552AA)) 
    g0_b0
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g0_b0_n_4));
  LUT6 #(
    .INIT(64'h99B33666CCD99B33)) 
    g0_b1
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g0_b1_n_4));
  LUT6 #(
    .INIT(64'h1E43C8790F21E43C)) 
    g0_b2
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g0_b2_n_4));
  LUT6 #(
    .INIT(64'h1F83F07E0FC1F83F)) 
    g0_b3
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g0_b3_n_4));
  LUT6 #(
    .INIT(64'hE07C0F81F03E07C0)) 
    g0_b4
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g0_b4_n_4));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    g1_b1
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g1_b1_n_4));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    g1_b2
       (.I0(\invdar_reg_334_reg[6] [0]),
        .I1(\invdar_reg_334_reg[6] [1]),
        .I2(\invdar_reg_334_reg[6] [2]),
        .I3(\invdar_reg_334_reg[6] [3]),
        .I4(\invdar_reg_334_reg[6] [4]),
        .I5(\invdar_reg_334_reg[6] [5]),
        .O(g1_b2_n_4));
  LUT5 #(
    .INIT(32'h00000001)) 
    g1_b4
       (.I0(\invdar_reg_334_reg[6] [1]),
        .I1(\invdar_reg_334_reg[6] [2]),
        .I2(\invdar_reg_334_reg[6] [3]),
        .I3(\invdar_reg_334_reg[6] [4]),
        .I4(\invdar_reg_334_reg[6] [5]),
        .O(g1_b4_n_4));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[0]_i_1 
       (.I0(g1_b1_n_4),
        .I1(\invdar_reg_334_reg[6] [6]),
        .I2(g0_b0_n_4),
        .O(\q0[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[1]_i_1 
       (.I0(g1_b1_n_4),
        .I1(\invdar_reg_334_reg[6] [6]),
        .I2(g0_b1_n_4),
        .O(\q0[1]_i_1_n_4 ));
  LUT2 #(
    .INIT(4'h2)) 
    \q0[3]_i_1__3 
       (.I0(g0_b3_n_4),
        .I1(\invdar_reg_334_reg[6] [6]),
        .O(\q0[3]_i_1__3_n_4 ));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[0]_i_1_n_4 ),
        .Q(q0[0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[1]_i_1_n_4 ),
        .Q(q0[1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0_reg[2]_i_1_n_4 ),
        .Q(q0[2]),
        .R(1'b0));
  MUXF7 \q0_reg[2]_i_1 
       (.I0(g0_b2_n_4),
        .I1(g1_b2_n_4),
        .O(\q0_reg[2]_i_1_n_4 ),
        .S(\invdar_reg_334_reg[6] [6]));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[3]_i_1__3_n_4 ),
        .Q(q0[3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0_reg[4]_i_1_n_4 ),
        .Q(q0[4]),
        .R(1'b0));
  MUXF7 \q0_reg[4]_i_1 
       (.I0(g0_b4_n_4),
        .I1(g1_b4_n_4),
        .O(\q0_reg[4]_i_1_n_4 ),
        .S(\invdar_reg_334_reg[6] [6]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_20
       (.I0(q0[4]),
        .I1(Q[4]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [4]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[4]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_21
       (.I0(q0[3]),
        .I1(Q[3]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [3]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[3]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_22
       (.I0(q0[2]),
        .I1(Q[2]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [2]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[2]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_23
       (.I0(q0[1]),
        .I1(Q[1]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[1]));
  LUT5 #(
    .INIT(32'hF0CCAAAA)) 
    ram_reg_i_24
       (.I0(q0[0]),
        .I1(Q[0]),
        .I2(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [0]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(\ap_CS_fsm_reg[3] [1]),
        .O(DIADI[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (reset,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    D,
    \int_ap_return_reg[31]_0 ,
    interrupt,
    SR,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WSTRB,
    Q,
    OUTPUT_STREAM_V_data_V_1_ack_in,
    OUTPUT_STREAM_V_keep_V_1_ack_in,
    OUTPUT_STREAM_V_dest_V_1_ack_in,
    OUTPUT_STREAM_V_last_V_1_ack_in,
    OUTPUT_STREAM_V_id_V_1_ack_in,
    OUTPUT_STREAM_V_strb_V_1_ack_in,
    OUTPUT_STREAM_V_user_V_1_ack_in,
    s_axi_CONTROL_BUS_ARADDR,
    ap_rst_n,
    \tmp_1_reg_494_reg[0] ,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR,
    I1);
  output reset;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [1:0]D;
  output \int_ap_return_reg[31]_0 ;
  output interrupt;
  output [0:0]SR;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input s_axi_CONTROL_BUS_ARVALID;
  input s_axi_CONTROL_BUS_RREADY;
  input [2:0]s_axi_CONTROL_BUS_WDATA;
  input s_axi_CONTROL_BUS_WVALID;
  input [0:0]s_axi_CONTROL_BUS_WSTRB;
  input [2:0]Q;
  input OUTPUT_STREAM_V_data_V_1_ack_in;
  input OUTPUT_STREAM_V_keep_V_1_ack_in;
  input OUTPUT_STREAM_V_dest_V_1_ack_in;
  input OUTPUT_STREAM_V_last_V_1_ack_in;
  input OUTPUT_STREAM_V_id_V_1_ack_in;
  input OUTPUT_STREAM_V_strb_V_1_ack_in;
  input OUTPUT_STREAM_V_user_V_1_ack_in;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  input ap_rst_n;
  input \tmp_1_reg_494_reg[0] ;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input [31:0]I1;

  wire \/FSM_onehot_wstate[1]_i_1_n_4 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_4 ;
  wire [1:0]D;
  wire \FSM_onehot_wstate[3]_i_1_n_4 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_4_[0] ;
  wire [31:0]I1;
  wire OUTPUT_STREAM_V_data_V_1_ack_in;
  wire OUTPUT_STREAM_V_dest_V_1_ack_in;
  wire OUTPUT_STREAM_V_id_V_1_ack_in;
  wire OUTPUT_STREAM_V_keep_V_1_ack_in;
  wire OUTPUT_STREAM_V_last_V_1_ack_in;
  wire OUTPUT_STREAM_V_strb_V_1_ack_in;
  wire OUTPUT_STREAM_V_user_V_1_ack_in;
  wire [2:0]Q;
  wire [0:0]SR;
  wire ap_clk;
  wire ap_done;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire int_ap_done;
  wire int_ap_done_i_1_n_4;
  wire int_ap_idle;
  wire int_ap_idle_i_1_n_4;
  wire int_ap_ready;
  wire [31:0]int_ap_return;
  wire \int_ap_return_reg[31]_0 ;
  wire int_ap_start3_out;
  wire int_ap_start_i_1_n_4;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_4;
  wire int_gie_i_1_n_4;
  wire int_gie_reg_n_4;
  wire \int_ier[0]_i_1_n_4 ;
  wire \int_ier[1]_i_1_n_4 ;
  wire \int_ier[1]_i_2_n_4 ;
  wire \int_ier_reg_n_4_[0] ;
  wire int_isr6_out;
  wire \int_isr[0]_i_1_n_4 ;
  wire \int_isr[1]_i_1_n_4 ;
  wire \int_isr_reg_n_4_[0] ;
  wire interrupt;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_1_in;
  wire [7:0]rdata_data;
  wire \rdata_data[0]_i_2_n_4 ;
  wire \rdata_data[0]_i_3_n_4 ;
  wire \rdata_data[1]_i_2_n_4 ;
  wire \rdata_data[1]_i_3_n_4 ;
  wire \rdata_data[31]_i_1_n_4 ;
  wire \rdata_data[7]_i_2_n_4 ;
  wire \rdata_data[7]_i_3_n_4 ;
  wire reset;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_4 ;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [2:0]s_axi_CONTROL_BUS_WDATA;
  wire [0:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire \tmp_1_reg_494_reg[0] ;
  wire waddr;
  wire \waddr_reg_n_4_[0] ;
  wire \waddr_reg_n_4_[1] ;
  wire \waddr_reg_n_4_[2] ;
  wire \waddr_reg_n_4_[3] ;
  wire \waddr_reg_n_4_[4] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_4 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_4 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_4_[0] ),
        .S(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_4 ),
        .Q(out[0]),
        .R(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_4 ),
        .Q(out[1]),
        .R(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_4 ),
        .Q(out[2]),
        .R(reset));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(reset));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(ap_done),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h88F8)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\tmp_1_reg_494_reg[0] ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFFFFDFFFFFF0000)) 
    int_ap_done_i_1
       (.I0(\rdata_data[7]_i_3_n_4 ),
        .I1(rstate[1]),
        .I2(rstate[0]),
        .I3(s_axi_CONTROL_BUS_ARVALID),
        .I4(ap_done),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_4),
        .Q(int_ap_done),
        .R(reset));
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(int_ap_idle_i_1_n_4));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_idle_i_1_n_4),
        .Q(int_ap_idle),
        .R(reset));
  LUT5 #(
    .INIT(32'h00008000)) 
    int_ap_ready_i_1
       (.I0(Q[2]),
        .I1(OUTPUT_STREAM_V_data_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_keep_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_dest_V_1_ack_in),
        .I4(\int_ap_return_reg[31]_0 ),
        .O(ap_done));
  LUT4 #(
    .INIT(16'h7FFF)) 
    int_ap_ready_i_2
       (.I0(OUTPUT_STREAM_V_last_V_1_ack_in),
        .I1(OUTPUT_STREAM_V_id_V_1_ack_in),
        .I2(OUTPUT_STREAM_V_strb_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_user_V_1_ack_in),
        .O(\int_ap_return_reg[31]_0 ));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_done),
        .Q(int_ap_ready),
        .R(reset));
  FDRE \int_ap_return_reg[0] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[0]),
        .Q(int_ap_return[0]),
        .R(reset));
  FDRE \int_ap_return_reg[10] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[10]),
        .Q(int_ap_return[10]),
        .R(reset));
  FDRE \int_ap_return_reg[11] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[11]),
        .Q(int_ap_return[11]),
        .R(reset));
  FDRE \int_ap_return_reg[12] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[12]),
        .Q(int_ap_return[12]),
        .R(reset));
  FDRE \int_ap_return_reg[13] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[13]),
        .Q(int_ap_return[13]),
        .R(reset));
  FDRE \int_ap_return_reg[14] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[14]),
        .Q(int_ap_return[14]),
        .R(reset));
  FDRE \int_ap_return_reg[15] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[15]),
        .Q(int_ap_return[15]),
        .R(reset));
  FDRE \int_ap_return_reg[16] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[16]),
        .Q(int_ap_return[16]),
        .R(reset));
  FDRE \int_ap_return_reg[17] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[17]),
        .Q(int_ap_return[17]),
        .R(reset));
  FDRE \int_ap_return_reg[18] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[18]),
        .Q(int_ap_return[18]),
        .R(reset));
  FDRE \int_ap_return_reg[19] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[19]),
        .Q(int_ap_return[19]),
        .R(reset));
  FDRE \int_ap_return_reg[1] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[1]),
        .Q(int_ap_return[1]),
        .R(reset));
  FDRE \int_ap_return_reg[20] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[20]),
        .Q(int_ap_return[20]),
        .R(reset));
  FDRE \int_ap_return_reg[21] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[21]),
        .Q(int_ap_return[21]),
        .R(reset));
  FDRE \int_ap_return_reg[22] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[22]),
        .Q(int_ap_return[22]),
        .R(reset));
  FDRE \int_ap_return_reg[23] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[23]),
        .Q(int_ap_return[23]),
        .R(reset));
  FDRE \int_ap_return_reg[24] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[24]),
        .Q(int_ap_return[24]),
        .R(reset));
  FDRE \int_ap_return_reg[25] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[25]),
        .Q(int_ap_return[25]),
        .R(reset));
  FDRE \int_ap_return_reg[26] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[26]),
        .Q(int_ap_return[26]),
        .R(reset));
  FDRE \int_ap_return_reg[27] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[27]),
        .Q(int_ap_return[27]),
        .R(reset));
  FDRE \int_ap_return_reg[28] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[28]),
        .Q(int_ap_return[28]),
        .R(reset));
  FDRE \int_ap_return_reg[29] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[29]),
        .Q(int_ap_return[29]),
        .R(reset));
  FDRE \int_ap_return_reg[2] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[2]),
        .Q(int_ap_return[2]),
        .R(reset));
  FDRE \int_ap_return_reg[30] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[30]),
        .Q(int_ap_return[30]),
        .R(reset));
  FDRE \int_ap_return_reg[31] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[31]),
        .Q(int_ap_return[31]),
        .R(reset));
  FDRE \int_ap_return_reg[3] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[3]),
        .Q(int_ap_return[3]),
        .R(reset));
  FDRE \int_ap_return_reg[4] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[4]),
        .Q(int_ap_return[4]),
        .R(reset));
  FDRE \int_ap_return_reg[5] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[5]),
        .Q(int_ap_return[5]),
        .R(reset));
  FDRE \int_ap_return_reg[6] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[6]),
        .Q(int_ap_return[6]),
        .R(reset));
  FDRE \int_ap_return_reg[7] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[7]),
        .Q(int_ap_return[7]),
        .R(reset));
  FDRE \int_ap_return_reg[8] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[8]),
        .Q(int_ap_return[8]),
        .R(reset));
  FDRE \int_ap_return_reg[9] 
       (.C(ap_clk),
        .CE(ap_done),
        .D(I1[9]),
        .Q(int_ap_return[9]),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFBF8)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(ap_done),
        .I2(int_ap_start3_out),
        .I3(ap_start),
        .O(int_ap_start_i_1_n_4));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    int_ap_start_i_2
       (.I0(\waddr_reg_n_4_[2] ),
        .I1(s_axi_CONTROL_BUS_WDATA[0]),
        .I2(\waddr_reg_n_4_[3] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .O(int_ap_start3_out));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_4),
        .Q(ap_start),
        .R(reset));
  LUT5 #(
    .INIT(32'hFEFF0200)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(\waddr_reg_n_4_[3] ),
        .I2(\waddr_reg_n_4_[2] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_4),
        .Q(int_auto_restart),
        .R(reset));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_4_[3] ),
        .I2(\waddr_reg_n_4_[2] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(int_gie_reg_n_4),
        .O(int_gie_i_1_n_4));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_4),
        .Q(int_gie_reg_n_4),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_4_[2] ),
        .I2(\waddr_reg_n_4_[3] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(\int_ier_reg_n_4_[0] ),
        .O(\int_ier[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_4_[2] ),
        .I2(\waddr_reg_n_4_[3] ),
        .I3(\int_ier[1]_i_2_n_4 ),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_4 ));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    \int_ier[1]_i_2 
       (.I0(\waddr_reg_n_4_[1] ),
        .I1(\waddr_reg_n_4_[4] ),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(\waddr_reg_n_4_[0] ),
        .I5(s_axi_CONTROL_BUS_WSTRB),
        .O(\int_ier[1]_i_2_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_4 ),
        .Q(\int_ier_reg_n_4_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_4 ),
        .Q(p_0_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_isr6_out),
        .I2(\int_ier_reg_n_4_[0] ),
        .I3(ap_done),
        .I4(\int_isr_reg_n_4_[0] ),
        .O(\int_isr[0]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \int_isr[0]_i_2 
       (.I0(\waddr_reg_n_4_[3] ),
        .I1(\waddr_reg_n_4_[2] ),
        .I2(\int_ier[1]_i_2_n_4 ),
        .O(int_isr6_out));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_isr6_out),
        .I2(p_0_in),
        .I3(ap_done),
        .I4(p_1_in),
        .O(\int_isr[1]_i_1_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_4 ),
        .Q(\int_isr_reg_n_4_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_4 ),
        .Q(p_1_in),
        .R(reset));
  LUT3 #(
    .INIT(8'hE0)) 
    interrupt_INST_0
       (.I0(\int_isr_reg_n_4_[0] ),
        .I1(p_1_in),
        .I2(int_gie_reg_n_4),
        .O(interrupt));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h8808)) 
    \invdar_reg_334[6]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\tmp_1_reg_494_reg[0] ),
        .O(SR));
  LUT6 #(
    .INIT(64'hAAFBAAAAAAEAAAAA)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[0]_i_2_n_4 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(\int_ier_reg_n_4_[0] ),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(\rdata_data[1]_i_3_n_4 ),
        .I5(ap_start),
        .O(rdata_data[0]));
  LUT6 #(
    .INIT(64'hF8FFF88888888888)) 
    \rdata_data[0]_i_2 
       (.I0(int_ap_return[0]),
        .I1(\rdata_data[7]_i_2_n_4 ),
        .I2(\int_isr_reg_n_4_[0] ),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(int_gie_reg_n_4),
        .I5(\rdata_data[0]_i_3_n_4 ),
        .O(\rdata_data[0]_i_2_n_4 ));
  LUT4 #(
    .INIT(16'h0004)) 
    \rdata_data[0]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[0]_i_3_n_4 ));
  LUT6 #(
    .INIT(64'h3B0B300038083000)) 
    \rdata_data[1]_i_1 
       (.I0(int_ap_return[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(\rdata_data[1]_i_2_n_4 ),
        .I4(\rdata_data[1]_i_3_n_4 ),
        .I5(int_ap_done),
        .O(rdata_data[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h000A000C)) 
    \rdata_data[1]_i_2 
       (.I0(p_1_in),
        .I1(p_0_in),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[1]_i_2_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \rdata_data[1]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[1]_i_3_n_4 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[2]_i_1 
       (.I0(int_ap_return[2]),
        .I1(\rdata_data[7]_i_2_n_4 ),
        .I2(int_ap_idle),
        .I3(\rdata_data[7]_i_3_n_4 ),
        .O(rdata_data[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA2)) 
    \rdata_data[31]_i_1 
       (.I0(ar_hs),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[31]_i_1_n_4 ));
  LUT3 #(
    .INIT(8'h10)) 
    \rdata_data[31]_i_2 
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .O(ar_hs));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[3]_i_1 
       (.I0(int_ap_return[3]),
        .I1(\rdata_data[7]_i_2_n_4 ),
        .I2(int_ap_ready),
        .I3(\rdata_data[7]_i_3_n_4 ),
        .O(rdata_data[3]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[7]_i_1 
       (.I0(int_ap_return[7]),
        .I1(\rdata_data[7]_i_2_n_4 ),
        .I2(int_auto_restart),
        .I3(\rdata_data[7]_i_3_n_4 ),
        .O(rdata_data[7]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    \rdata_data[7]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[7]_i_2_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \rdata_data[7]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[7]_i_3_n_4 ));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[10]),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[11]),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[12]),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[13]),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[14]),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[15]),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[16]),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[17]),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[18]),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[19]),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[20]),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[21]),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[22]),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[23]),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[24]),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[25]),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[26]),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[27]),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[28]),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[29]),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[30]),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[31]),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(1'b0));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[4]),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[5]),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[6]),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(1'b0));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[8]),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .R(\rdata_data[31]_i_1_n_4 ));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(int_ap_return[9]),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .R(\rdata_data[31]_i_1_n_4 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h020E)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(rstate[0]),
        .I2(rstate[1]),
        .I3(s_axi_CONTROL_BUS_RREADY),
        .O(\rstate[0]_i_1_n_4 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_4 ),
        .Q(rstate[0]),
        .R(reset));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(reset));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[4]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_4_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_4_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_4_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_4_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_4_[4] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input
   (I1,
    D,
    ap_clk,
    DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[5] ,
    E,
    \tmp_reg_484_reg[6] ,
    \i_1_reg_356_reg[5] ,
    ap_enable_reg_pp1_iter0,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [31:0]I1;
  output [31:0]D;
  input ap_clk;
  input [4:0]DIADI;
  input [3:0]Q;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]\i_reg_345_reg[5] ;
  input [0:0]E;
  input [6:0]\tmp_reg_484_reg[6] ;
  input [5:0]\i_1_reg_356_reg[5] ;
  input ap_enable_reg_pp1_iter0;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [31:0]D;
  wire [4:0]DIADI;
  wire [0:0]E;
  wire [31:0]I1;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [3:0]Q;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire [5:0]\i_1_reg_356_reg[5] ;
  wire [5:0]\i_reg_345_reg[5] ;
  wire [6:0]\tmp_reg_484_reg[6] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram Adder2_input_ram_U
       (.D(D),
        .DIADI(DIADI),
        .E(E),
        .I1(I1),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter0(ap_enable_reg_pp1_iter0),
        .\i_1_reg_356_reg[5] (\i_1_reg_356_reg[5] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\tmp_reg_484_reg[6] (\tmp_reg_484_reg[6] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud
   (\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ,
    \ap_CS_fsm_reg[3] ,
    Q,
    INPUT_STREAM_V_keep_V_0_sel,
    \INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output [3:0]\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ;
  input \ap_CS_fsm_reg[3] ;
  input [3:0]Q;
  input INPUT_STREAM_V_keep_V_0_sel;
  input [3:0]\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [3:0]\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ;
  wire INPUT_STREAM_V_keep_V_0_sel;
  wire [3:0]\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ;
  wire [3:0]Q;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1 Adder2_inputValuecud_ram_U
       (.E(E),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] (\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ),
        .INPUT_STREAM_V_keep_V_0_sel(INPUT_STREAM_V_keep_V_0_sel),
        .\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] (\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ),
        .Q(Q),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (\i_reg_345_reg[0] ),
        .\i_reg_345_reg[1] (\i_reg_345_reg[1] ),
        .\i_reg_345_reg[2] (\i_reg_345_reg[2] ),
        .\i_reg_345_reg[3] (\i_reg_345_reg[3] ),
        .\i_reg_345_reg[4] (\i_reg_345_reg[4] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\i_reg_345_reg[5]_0 (\i_reg_345_reg[5]_0 ),
        .\i_reg_345_reg[5]_1 (\i_reg_345_reg[5]_1 ));
endmodule

(* ORIG_REF_NAME = "Adder2_inputValuecud" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0
   (\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ,
    \ap_CS_fsm_reg[3] ,
    Q,
    INPUT_STREAM_V_strb_V_0_sel,
    \INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output [3:0]\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ;
  input \ap_CS_fsm_reg[3] ;
  input [3:0]Q;
  input INPUT_STREAM_V_strb_V_0_sel;
  input [3:0]\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [3:0]\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ;
  wire INPUT_STREAM_V_strb_V_0_sel;
  wire [3:0]\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ;
  wire [3:0]Q;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram Adder2_inputValuecud_ram_U
       (.E(E),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] (\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ),
        .INPUT_STREAM_V_strb_V_0_sel(INPUT_STREAM_V_strb_V_0_sel),
        .\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] (\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ),
        .Q(Q),
        .address0({\i_reg_345_reg[3] ,\i_reg_345_reg[2] ,\i_reg_345_reg[1] ,\i_reg_345_reg[0] }),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[4] (\i_reg_345_reg[4] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\i_reg_345_reg[5]_0 (\i_reg_345_reg[5]_0 ),
        .\i_reg_345_reg[5]_1 (\i_reg_345_reg[5]_1 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram
   (\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ,
    \ap_CS_fsm_reg[3] ,
    Q,
    INPUT_STREAM_V_strb_V_0_sel,
    \INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    address0,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output [3:0]\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ;
  input \ap_CS_fsm_reg[3] ;
  input [3:0]Q;
  input INPUT_STREAM_V_strb_V_0_sel;
  input [3:0]\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [3:0]address0;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [3:0]\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] ;
  wire INPUT_STREAM_V_strb_V_0_sel;
  wire [3:0]\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] ;
  wire [3:0]Q;
  wire [3:0]address0;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;
  wire [3:0]p_1_out;
  wire \q0[0]_i_1__1_n_4 ;
  wire \q0[1]_i_1__1_n_4 ;
  wire \q0[2]_i_1__0_n_4 ;
  wire \q0[3]_i_1_n_4 ;
  wire ram_reg_0_15_0_0__0_n_4;
  wire ram_reg_0_15_0_0__1_n_4;
  wire ram_reg_0_15_0_0__2_n_4;
  wire ram_reg_0_15_0_0__3_n_4;
  wire ram_reg_0_15_0_0__4_n_4;
  wire ram_reg_0_15_0_0__5_n_4;
  wire ram_reg_0_15_0_0__6_n_4;
  wire ram_reg_0_15_0_0_n_4;
  wire ram_reg_0_31_0_0__0_i_1__0_n_4;
  wire ram_reg_0_31_0_0__0_n_4;
  wire ram_reg_0_31_0_0__1_i_1__0_n_4;
  wire ram_reg_0_31_0_0__1_n_4;
  wire ram_reg_0_31_0_0__2_i_1__0_n_4;
  wire ram_reg_0_31_0_0__2_n_4;
  wire ram_reg_0_31_0_0_i_1__1_n_4;
  wire ram_reg_0_31_0_0_n_4;

  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[0]_i_1__1 
       (.I0(p_1_out[0]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[0]),
        .I3(INPUT_STREAM_V_strb_V_0_sel),
        .I4(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [0]),
        .O(\q0[0]_i_1__1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[0]_i_2__0 
       (.I0(ram_reg_0_15_0_0__0_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0_n_4),
        .O(p_1_out[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[1]_i_1__1 
       (.I0(p_1_out[1]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_strb_V_0_sel),
        .I4(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [1]),
        .O(\q0[1]_i_1__1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[1]_i_2__0 
       (.I0(ram_reg_0_15_0_0__2_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__1_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__0_n_4),
        .O(p_1_out[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[2]_i_1__0 
       (.I0(p_1_out[2]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[2]),
        .I3(INPUT_STREAM_V_strb_V_0_sel),
        .I4(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [2]),
        .O(\q0[2]_i_1__0_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[2]_i_2__0 
       (.I0(ram_reg_0_15_0_0__4_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__3_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__1_n_4),
        .O(p_1_out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[3]_i_1 
       (.I0(p_1_out[3]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[3]),
        .I3(INPUT_STREAM_V_strb_V_0_sel),
        .I4(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [3]),
        .O(\q0[3]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[3]_i_2 
       (.I0(ram_reg_0_15_0_0__6_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__5_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__2_n_4),
        .O(p_1_out[3]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[0]_i_1__1_n_4 ),
        .Q(\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] [0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[1]_i_1__1_n_4 ),
        .Q(\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] [1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[2]_i_1__0_n_4 ),
        .Q(\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] [2]),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[3]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3] [3]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__1_n_4),
        .O(ram_reg_0_15_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__3
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__3_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__4
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__4_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__5
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__5_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__6
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__6_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  RAM32X1S ram_reg_0_31_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0_i_1__1_n_4),
        .O(ram_reg_0_31_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  RAM32X1S ram_reg_0_31_0_0__0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__0_i_1__0_n_4),
        .O(ram_reg_0_31_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__0_i_1__0
       (.I0(Q[1]),
        .I1(INPUT_STREAM_V_strb_V_0_sel),
        .I2(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [1]),
        .O(ram_reg_0_31_0_0__0_i_1__0_n_4));
  RAM32X1S ram_reg_0_31_0_0__1
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__1_i_1__0_n_4),
        .O(ram_reg_0_31_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__1_i_1__0
       (.I0(Q[2]),
        .I1(INPUT_STREAM_V_strb_V_0_sel),
        .I2(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [2]),
        .O(ram_reg_0_31_0_0__1_i_1__0_n_4));
  RAM32X1S ram_reg_0_31_0_0__2
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__2_i_1__0_n_4),
        .O(ram_reg_0_31_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__2_i_1__0
       (.I0(Q[3]),
        .I1(INPUT_STREAM_V_strb_V_0_sel),
        .I2(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [3]),
        .O(ram_reg_0_31_0_0__2_i_1__0_n_4));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0_i_1__1
       (.I0(Q[0]),
        .I1(INPUT_STREAM_V_strb_V_0_sel),
        .I2(\INPUT_STREAM_V_strb_V_0_payload_A_reg[3] [0]),
        .O(ram_reg_0_31_0_0_i_1__1_n_4));
endmodule

(* ORIG_REF_NAME = "Adder2_inputValuecud_ram" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1
   (\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ,
    \ap_CS_fsm_reg[3] ,
    Q,
    INPUT_STREAM_V_keep_V_0_sel,
    \INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output [3:0]\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ;
  input \ap_CS_fsm_reg[3] ;
  input [3:0]Q;
  input INPUT_STREAM_V_keep_V_0_sel;
  input [3:0]\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [3:0]\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] ;
  wire INPUT_STREAM_V_keep_V_0_sel;
  wire [3:0]\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] ;
  wire [3:0]Q;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;
  wire [3:0]p_1_out;
  wire \q0[0]_i_1__0_n_4 ;
  wire \q0[1]_i_1__0_n_4 ;
  wire \q0[2]_i_1_n_4 ;
  wire \q0[3]_i_2__2_n_4 ;
  wire ram_reg_0_15_0_0__0_n_4;
  wire ram_reg_0_15_0_0__1_n_4;
  wire ram_reg_0_15_0_0__2_n_4;
  wire ram_reg_0_15_0_0__3_n_4;
  wire ram_reg_0_15_0_0__4_n_4;
  wire ram_reg_0_15_0_0__5_n_4;
  wire ram_reg_0_15_0_0__6_n_4;
  wire ram_reg_0_15_0_0_n_4;
  wire ram_reg_0_31_0_0__0_i_1_n_4;
  wire ram_reg_0_31_0_0__0_n_4;
  wire ram_reg_0_31_0_0__1_i_1_n_4;
  wire ram_reg_0_31_0_0__1_n_4;
  wire ram_reg_0_31_0_0__2_i_1_n_4;
  wire ram_reg_0_31_0_0__2_n_4;
  wire ram_reg_0_31_0_0_i_1__0_n_4;
  wire ram_reg_0_31_0_0_n_4;

  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[0]_i_1__0 
       (.I0(p_1_out[0]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[0]),
        .I3(INPUT_STREAM_V_keep_V_0_sel),
        .I4(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [0]),
        .O(\q0[0]_i_1__0_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[0]_i_2 
       (.I0(ram_reg_0_15_0_0__0_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0_n_4),
        .O(p_1_out[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[1]_i_1__0 
       (.I0(p_1_out[1]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_keep_V_0_sel),
        .I4(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [1]),
        .O(\q0[1]_i_1__0_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[1]_i_2 
       (.I0(ram_reg_0_15_0_0__2_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__1_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__0_n_4),
        .O(p_1_out[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[2]_i_1 
       (.I0(p_1_out[2]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[2]),
        .I3(INPUT_STREAM_V_keep_V_0_sel),
        .I4(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [2]),
        .O(\q0[2]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[2]_i_2 
       (.I0(ram_reg_0_15_0_0__4_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__3_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__1_n_4),
        .O(p_1_out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[3]_i_2__2 
       (.I0(p_1_out[3]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[3]),
        .I3(INPUT_STREAM_V_keep_V_0_sel),
        .I4(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [3]),
        .O(\q0[3]_i_2__2_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[3]_i_4 
       (.I0(ram_reg_0_15_0_0__6_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__5_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__2_n_4),
        .O(p_1_out[3]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[0]_i_1__0_n_4 ),
        .Q(\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] [0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[1]_i_1__0_n_4 ),
        .Q(\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] [1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[2]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] [2]),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[3]_i_2__2_n_4 ),
        .Q(\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3] [3]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__0_n_4),
        .O(ram_reg_0_15_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1_n_4),
        .O(ram_reg_0_15_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1_n_4),
        .O(ram_reg_0_15_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__3
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1_n_4),
        .O(ram_reg_0_15_0_0__3_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__4
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1_n_4),
        .O(ram_reg_0_15_0_0__4_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__5
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1_n_4),
        .O(ram_reg_0_15_0_0__5_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__6
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1_n_4),
        .O(ram_reg_0_15_0_0__6_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  RAM32X1S ram_reg_0_31_0_0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0_i_1__0_n_4),
        .O(ram_reg_0_31_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  RAM32X1S ram_reg_0_31_0_0__0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__0_i_1_n_4),
        .O(ram_reg_0_31_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__0_i_1
       (.I0(Q[1]),
        .I1(INPUT_STREAM_V_keep_V_0_sel),
        .I2(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [1]),
        .O(ram_reg_0_31_0_0__0_i_1_n_4));
  RAM32X1S ram_reg_0_31_0_0__1
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__1_i_1_n_4),
        .O(ram_reg_0_31_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__1_i_1
       (.I0(Q[2]),
        .I1(INPUT_STREAM_V_keep_V_0_sel),
        .I2(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [2]),
        .O(ram_reg_0_31_0_0__1_i_1_n_4));
  RAM32X1S ram_reg_0_31_0_0__2
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__2_i_1_n_4),
        .O(ram_reg_0_31_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__2_i_1
       (.I0(Q[3]),
        .I1(INPUT_STREAM_V_keep_V_0_sel),
        .I2(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [3]),
        .O(ram_reg_0_31_0_0__2_i_1_n_4));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0_i_1__0
       (.I0(Q[0]),
        .I1(INPUT_STREAM_V_keep_V_0_sel),
        .I2(\INPUT_STREAM_V_keep_V_0_payload_A_reg[3] [0]),
        .O(ram_reg_0_31_0_0_i_1__0_n_4));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg
   (\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ,
    \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ,
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ,
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ,
    \OUTPUT_STREAM_V_user_V_1_state_reg[0] ,
    OUTPUT_STREAM_V_user_V_1_ack_in,
    OUTPUT_STREAM_V_user_V_1_sel_wr,
    OUTPUT_STREAM_V_user_V_1_payload_A,
    OUTPUT_STREAM_V_user_V_1_payload_B,
    \ap_CS_fsm_reg[3] ,
    INPUT_STREAM_V_user_V_0_payload_B,
    INPUT_STREAM_V_user_V_0_sel,
    INPUT_STREAM_V_user_V_0_payload_A,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ;
  output \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ;
  output \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ;
  output \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ;
  input \OUTPUT_STREAM_V_user_V_1_state_reg[0] ;
  input OUTPUT_STREAM_V_user_V_1_ack_in;
  input OUTPUT_STREAM_V_user_V_1_sel_wr;
  input [1:0]OUTPUT_STREAM_V_user_V_1_payload_A;
  input [1:0]OUTPUT_STREAM_V_user_V_1_payload_B;
  input \ap_CS_fsm_reg[3] ;
  input [1:0]INPUT_STREAM_V_user_V_0_payload_B;
  input INPUT_STREAM_V_user_V_0_sel;
  input [1:0]INPUT_STREAM_V_user_V_0_payload_A;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [1:0]INPUT_STREAM_V_user_V_0_payload_A;
  wire [1:0]INPUT_STREAM_V_user_V_0_payload_B;
  wire INPUT_STREAM_V_user_V_0_sel;
  wire OUTPUT_STREAM_V_user_V_1_ack_in;
  wire [1:0]OUTPUT_STREAM_V_user_V_1_payload_A;
  wire \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ;
  wire \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ;
  wire [1:0]OUTPUT_STREAM_V_user_V_1_payload_B;
  wire \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ;
  wire \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ;
  wire OUTPUT_STREAM_V_user_V_1_sel_wr;
  wire \OUTPUT_STREAM_V_user_V_1_state_reg[0] ;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg_ram Adder2_inputValueeOg_ram_U
       (.E(E),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .INPUT_STREAM_V_user_V_0_payload_A(INPUT_STREAM_V_user_V_0_payload_A),
        .INPUT_STREAM_V_user_V_0_payload_B(INPUT_STREAM_V_user_V_0_payload_B),
        .INPUT_STREAM_V_user_V_0_sel(INPUT_STREAM_V_user_V_0_sel),
        .OUTPUT_STREAM_V_user_V_1_ack_in(OUTPUT_STREAM_V_user_V_1_ack_in),
        .OUTPUT_STREAM_V_user_V_1_payload_A(OUTPUT_STREAM_V_user_V_1_payload_A),
        .\OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] (\OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ),
        .\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] (\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ),
        .OUTPUT_STREAM_V_user_V_1_payload_B(OUTPUT_STREAM_V_user_V_1_payload_B),
        .\OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] (\OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ),
        .\OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] (\OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ),
        .OUTPUT_STREAM_V_user_V_1_sel_wr(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .\OUTPUT_STREAM_V_user_V_1_state_reg[0] (\OUTPUT_STREAM_V_user_V_1_state_reg[0] ),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (\i_reg_345_reg[0] ),
        .\i_reg_345_reg[1] (\i_reg_345_reg[1] ),
        .\i_reg_345_reg[2] (\i_reg_345_reg[2] ),
        .\i_reg_345_reg[3] (\i_reg_345_reg[3] ),
        .\i_reg_345_reg[4] (\i_reg_345_reg[4] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\i_reg_345_reg[5]_0 (\i_reg_345_reg[5]_0 ),
        .\i_reg_345_reg[5]_1 (\i_reg_345_reg[5]_1 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg_ram
   (\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ,
    \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ,
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ,
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ,
    \OUTPUT_STREAM_V_user_V_1_state_reg[0] ,
    OUTPUT_STREAM_V_user_V_1_ack_in,
    OUTPUT_STREAM_V_user_V_1_sel_wr,
    OUTPUT_STREAM_V_user_V_1_payload_A,
    OUTPUT_STREAM_V_user_V_1_payload_B,
    \ap_CS_fsm_reg[3] ,
    INPUT_STREAM_V_user_V_0_payload_B,
    INPUT_STREAM_V_user_V_0_sel,
    INPUT_STREAM_V_user_V_0_payload_A,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ;
  output \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ;
  output \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ;
  output \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ;
  input \OUTPUT_STREAM_V_user_V_1_state_reg[0] ;
  input OUTPUT_STREAM_V_user_V_1_ack_in;
  input OUTPUT_STREAM_V_user_V_1_sel_wr;
  input [1:0]OUTPUT_STREAM_V_user_V_1_payload_A;
  input [1:0]OUTPUT_STREAM_V_user_V_1_payload_B;
  input \ap_CS_fsm_reg[3] ;
  input [1:0]INPUT_STREAM_V_user_V_0_payload_B;
  input INPUT_STREAM_V_user_V_0_sel;
  input [1:0]INPUT_STREAM_V_user_V_0_payload_A;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [1:0]INPUT_STREAM_V_user_V_0_payload_A;
  wire [1:0]INPUT_STREAM_V_user_V_0_payload_B;
  wire INPUT_STREAM_V_user_V_0_sel;
  wire OUTPUT_STREAM_V_user_V_1_ack_in;
  wire [1:0]OUTPUT_STREAM_V_user_V_1_payload_A;
  wire \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ;
  wire \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ;
  wire [1:0]OUTPUT_STREAM_V_user_V_1_payload_B;
  wire \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ;
  wire \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ;
  wire OUTPUT_STREAM_V_user_V_1_sel_wr;
  wire \OUTPUT_STREAM_V_user_V_1_state_reg[0] ;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;
  wire [1:0]p_1_out;
  wire \q0[0]_i_1__2_n_4 ;
  wire \q0[1]_i_1__2_n_4 ;
  wire \q0_reg_n_4_[0] ;
  wire \q0_reg_n_4_[1] ;
  wire ram_reg_0_15_0_0__0_n_4;
  wire ram_reg_0_15_0_0__1_n_4;
  wire ram_reg_0_15_0_0__2_n_4;
  wire ram_reg_0_15_0_0_n_4;
  wire ram_reg_0_31_0_0__0_i_1__1_n_4;
  wire ram_reg_0_31_0_0__0_n_4;
  wire ram_reg_0_31_0_0_i_1__2_n_4;
  wire ram_reg_0_31_0_0_n_4;

  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \OUTPUT_STREAM_V_user_V_1_payload_A[0]_i_1 
       (.I0(\q0_reg_n_4_[0] ),
        .I1(\OUTPUT_STREAM_V_user_V_1_state_reg[0] ),
        .I2(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .I4(OUTPUT_STREAM_V_user_V_1_payload_A[0]),
        .O(\OUTPUT_STREAM_V_user_V_1_payload_A_reg[0] ));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \OUTPUT_STREAM_V_user_V_1_payload_A[1]_i_1 
       (.I0(\q0_reg_n_4_[1] ),
        .I1(\OUTPUT_STREAM_V_user_V_1_state_reg[0] ),
        .I2(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .I4(OUTPUT_STREAM_V_user_V_1_payload_A[1]),
        .O(\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1] ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \OUTPUT_STREAM_V_user_V_1_payload_B[0]_i_1 
       (.I0(\q0_reg_n_4_[0] ),
        .I1(\OUTPUT_STREAM_V_user_V_1_state_reg[0] ),
        .I2(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .I4(OUTPUT_STREAM_V_user_V_1_payload_B[0]),
        .O(\OUTPUT_STREAM_V_user_V_1_payload_B_reg[0] ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \OUTPUT_STREAM_V_user_V_1_payload_B[1]_i_1 
       (.I0(\q0_reg_n_4_[1] ),
        .I1(\OUTPUT_STREAM_V_user_V_1_state_reg[0] ),
        .I2(OUTPUT_STREAM_V_user_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_user_V_1_sel_wr),
        .I4(OUTPUT_STREAM_V_user_V_1_payload_B[1]),
        .O(\OUTPUT_STREAM_V_user_V_1_payload_B_reg[1] ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[0]_i_1__2 
       (.I0(p_1_out[0]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(INPUT_STREAM_V_user_V_0_payload_B[0]),
        .I3(INPUT_STREAM_V_user_V_0_sel),
        .I4(INPUT_STREAM_V_user_V_0_payload_A[0]),
        .O(\q0[0]_i_1__2_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[0]_i_2__1 
       (.I0(ram_reg_0_15_0_0__0_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0_n_4),
        .O(p_1_out[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[1]_i_1__2 
       (.I0(p_1_out[1]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(INPUT_STREAM_V_user_V_0_payload_B[1]),
        .I3(INPUT_STREAM_V_user_V_0_sel),
        .I4(INPUT_STREAM_V_user_V_0_payload_A[1]),
        .O(\q0[1]_i_1__2_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[1]_i_2__1 
       (.I0(ram_reg_0_15_0_0__2_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__1_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__0_n_4),
        .O(p_1_out[1]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[0]_i_1__2_n_4 ),
        .Q(\q0_reg_n_4_[0] ),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[1]_i_1__2_n_4 ),
        .Q(\q0_reg_n_4_[1] ),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__2_n_4),
        .O(ram_reg_0_15_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  RAM32X1S ram_reg_0_31_0_0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0_i_1__2_n_4),
        .O(ram_reg_0_31_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  RAM32X1S ram_reg_0_31_0_0__0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__0_i_1__1_n_4),
        .O(ram_reg_0_31_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__0_i_1__1
       (.I0(INPUT_STREAM_V_user_V_0_payload_B[1]),
        .I1(INPUT_STREAM_V_user_V_0_sel),
        .I2(INPUT_STREAM_V_user_V_0_payload_A[1]),
        .O(ram_reg_0_31_0_0__0_i_1__1_n_4));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0_i_1__2
       (.I0(INPUT_STREAM_V_user_V_0_payload_B[0]),
        .I1(INPUT_STREAM_V_user_V_0_sel),
        .I2(INPUT_STREAM_V_user_V_0_payload_A[0]),
        .O(ram_reg_0_31_0_0_i_1__2_n_4));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi
   (d0,
    \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ,
    \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ,
    E,
    ap_clk,
    \ap_CS_fsm_reg[3] ,
    INPUT_STREAM_V_last_V_0_payload_B,
    INPUT_STREAM_V_last_V_0_sel,
    INPUT_STREAM_V_last_V_0_payload_A,
    \OUTPUT_STREAM_V_last_V_1_state_reg[0] ,
    OUTPUT_STREAM_V_last_V_1_ack_in,
    OUTPUT_STREAM_V_last_V_1_sel_wr,
    OUTPUT_STREAM_V_last_V_1_payload_A,
    OUTPUT_STREAM_V_last_V_1_payload_B,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 );
  output d0;
  output \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ;
  output \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ;
  input [0:0]E;
  input ap_clk;
  input \ap_CS_fsm_reg[3] ;
  input INPUT_STREAM_V_last_V_0_payload_B;
  input INPUT_STREAM_V_last_V_0_sel;
  input INPUT_STREAM_V_last_V_0_payload_A;
  input \OUTPUT_STREAM_V_last_V_1_state_reg[0] ;
  input OUTPUT_STREAM_V_last_V_1_ack_in;
  input OUTPUT_STREAM_V_last_V_1_sel_wr;
  input OUTPUT_STREAM_V_last_V_1_payload_A;
  input OUTPUT_STREAM_V_last_V_1_payload_B;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire OUTPUT_STREAM_V_last_V_1_ack_in;
  wire OUTPUT_STREAM_V_last_V_1_payload_A;
  wire \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ;
  wire OUTPUT_STREAM_V_last_V_1_payload_B;
  wire \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ;
  wire OUTPUT_STREAM_V_last_V_1_sel_wr;
  wire \OUTPUT_STREAM_V_last_V_1_state_reg[0] ;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire d0;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi_ram Adder2_inputValuefYi_ram_U
       (.E(E),
        .I8(d0),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .INPUT_STREAM_V_last_V_0_payload_A(INPUT_STREAM_V_last_V_0_payload_A),
        .INPUT_STREAM_V_last_V_0_payload_B(INPUT_STREAM_V_last_V_0_payload_B),
        .INPUT_STREAM_V_last_V_0_sel(INPUT_STREAM_V_last_V_0_sel),
        .OUTPUT_STREAM_V_last_V_1_ack_in(OUTPUT_STREAM_V_last_V_1_ack_in),
        .OUTPUT_STREAM_V_last_V_1_payload_A(OUTPUT_STREAM_V_last_V_1_payload_A),
        .\OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] (\OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ),
        .OUTPUT_STREAM_V_last_V_1_payload_B(OUTPUT_STREAM_V_last_V_1_payload_B),
        .\OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] (\OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ),
        .OUTPUT_STREAM_V_last_V_1_sel_wr(OUTPUT_STREAM_V_last_V_1_sel_wr),
        .\OUTPUT_STREAM_V_last_V_1_state_reg[0] (\OUTPUT_STREAM_V_last_V_1_state_reg[0] ),
        .address0({\i_reg_345_reg[3] ,\i_reg_345_reg[2] ,\i_reg_345_reg[1] ,\i_reg_345_reg[0] }),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[4] (\i_reg_345_reg[4] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\i_reg_345_reg[5]_0 (\i_reg_345_reg[5]_0 ),
        .\i_reg_345_reg[5]_1 (\i_reg_345_reg[5]_1 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi_ram
   (I8,
    \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ,
    \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ,
    E,
    ap_clk,
    \ap_CS_fsm_reg[3] ,
    INPUT_STREAM_V_last_V_0_payload_B,
    INPUT_STREAM_V_last_V_0_sel,
    INPUT_STREAM_V_last_V_0_payload_A,
    \OUTPUT_STREAM_V_last_V_1_state_reg[0] ,
    OUTPUT_STREAM_V_last_V_1_ack_in,
    OUTPUT_STREAM_V_last_V_1_sel_wr,
    OUTPUT_STREAM_V_last_V_1_payload_A,
    OUTPUT_STREAM_V_last_V_1_payload_B,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    address0,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 );
  output [0:0]I8;
  output \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ;
  output \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ;
  input [0:0]E;
  input ap_clk;
  input \ap_CS_fsm_reg[3] ;
  input INPUT_STREAM_V_last_V_0_payload_B;
  input INPUT_STREAM_V_last_V_0_sel;
  input INPUT_STREAM_V_last_V_0_payload_A;
  input \OUTPUT_STREAM_V_last_V_1_state_reg[0] ;
  input OUTPUT_STREAM_V_last_V_1_ack_in;
  input OUTPUT_STREAM_V_last_V_1_sel_wr;
  input OUTPUT_STREAM_V_last_V_1_payload_A;
  input OUTPUT_STREAM_V_last_V_1_payload_B;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [3:0]address0;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;

  wire [0:0]E;
  wire [0:0]I8;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire OUTPUT_STREAM_V_last_V_1_ack_in;
  wire OUTPUT_STREAM_V_last_V_1_payload_A;
  wire \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ;
  wire OUTPUT_STREAM_V_last_V_1_payload_B;
  wire \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ;
  wire OUTPUT_STREAM_V_last_V_1_sel_wr;
  wire \OUTPUT_STREAM_V_last_V_1_state_reg[0] ;
  wire [3:0]address0;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;
  wire p_1_out;
  wire \q0[0]_i_1__3_n_4 ;
  wire \q0_reg_n_4_[0] ;
  wire ram_reg_0_15_0_0__0_n_4;
  wire ram_reg_0_15_0_0_n_4;
  wire ram_reg_0_31_0_0_n_4;

  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \OUTPUT_STREAM_V_last_V_1_payload_A[0]_i_1 
       (.I0(\q0_reg_n_4_[0] ),
        .I1(\OUTPUT_STREAM_V_last_V_1_state_reg[0] ),
        .I2(OUTPUT_STREAM_V_last_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_last_V_1_sel_wr),
        .I4(OUTPUT_STREAM_V_last_V_1_payload_A),
        .O(\OUTPUT_STREAM_V_last_V_1_payload_A_reg[0] ));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \OUTPUT_STREAM_V_last_V_1_payload_B[0]_i_1 
       (.I0(\q0_reg_n_4_[0] ),
        .I1(\OUTPUT_STREAM_V_last_V_1_state_reg[0] ),
        .I2(OUTPUT_STREAM_V_last_V_1_ack_in),
        .I3(OUTPUT_STREAM_V_last_V_1_sel_wr),
        .I4(OUTPUT_STREAM_V_last_V_1_payload_B),
        .O(\OUTPUT_STREAM_V_last_V_1_payload_B_reg[0] ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[0]_i_1__3 
       (.I0(p_1_out),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(INPUT_STREAM_V_last_V_0_payload_B),
        .I3(INPUT_STREAM_V_last_V_0_sel),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\q0[0]_i_1__3_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[0]_i_2__2 
       (.I0(ram_reg_0_15_0_0__0_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0_n_4),
        .O(p_1_out));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[0]_i_1__3_n_4 ),
        .Q(\q0_reg_n_4_[0] ),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(I8),
        .O(ram_reg_0_15_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(I8),
        .O(ram_reg_0_15_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  RAM32X1S ram_reg_0_31_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\i_reg_345_reg[4] ),
        .D(I8),
        .O(ram_reg_0_31_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0_i_1
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .O(I8));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j
   (\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ,
    \ap_CS_fsm_reg[3] ,
    Q,
    INPUT_STREAM_V_id_V_0_sel,
    \INPUT_STREAM_V_id_V_0_payload_A_reg[4] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output [4:0]\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ;
  input \ap_CS_fsm_reg[3] ;
  input [4:0]Q;
  input INPUT_STREAM_V_id_V_0_sel;
  input [4:0]\INPUT_STREAM_V_id_V_0_payload_A_reg[4] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [4:0]\INPUT_STREAM_V_id_V_0_payload_A_reg[4] ;
  wire INPUT_STREAM_V_id_V_0_sel;
  wire [4:0]\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ;
  wire [4:0]Q;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j_ram Adder2_inputValueg8j_ram_U
       (.E(E),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .\INPUT_STREAM_V_id_V_0_payload_A_reg[4] (\INPUT_STREAM_V_id_V_0_payload_A_reg[4] ),
        .INPUT_STREAM_V_id_V_0_sel(INPUT_STREAM_V_id_V_0_sel),
        .\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] (\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ),
        .Q(Q),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\i_reg_345_reg[0] (\i_reg_345_reg[0] ),
        .\i_reg_345_reg[1] (\i_reg_345_reg[1] ),
        .\i_reg_345_reg[2] (\i_reg_345_reg[2] ),
        .\i_reg_345_reg[3] (\i_reg_345_reg[3] ),
        .\i_reg_345_reg[4] (\i_reg_345_reg[4] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\i_reg_345_reg[5]_0 (\i_reg_345_reg[5]_0 ),
        .\i_reg_345_reg[5]_1 (\i_reg_345_reg[5]_1 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j_ram
   (\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ,
    \ap_CS_fsm_reg[3] ,
    Q,
    INPUT_STREAM_V_id_V_0_sel,
    \INPUT_STREAM_V_id_V_0_payload_A_reg[4] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[0] ,
    \i_reg_345_reg[1] ,
    \i_reg_345_reg[2] ,
    \i_reg_345_reg[3] ,
    \i_reg_345_reg[4] ,
    \i_reg_345_reg[5] ,
    \i_reg_345_reg[5]_0 ,
    \i_reg_345_reg[5]_1 ,
    E);
  output [4:0]\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ;
  input \ap_CS_fsm_reg[3] ;
  input [4:0]Q;
  input INPUT_STREAM_V_id_V_0_sel;
  input [4:0]\INPUT_STREAM_V_id_V_0_payload_A_reg[4] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \i_reg_345_reg[0] ;
  input \i_reg_345_reg[1] ;
  input \i_reg_345_reg[2] ;
  input \i_reg_345_reg[3] ;
  input \i_reg_345_reg[4] ;
  input \i_reg_345_reg[5] ;
  input \i_reg_345_reg[5]_0 ;
  input \i_reg_345_reg[5]_1 ;
  input [0:0]E;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [4:0]\INPUT_STREAM_V_id_V_0_payload_A_reg[4] ;
  wire INPUT_STREAM_V_id_V_0_sel;
  wire [4:0]\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] ;
  wire [4:0]Q;
  wire \ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire \i_reg_345_reg[0] ;
  wire \i_reg_345_reg[1] ;
  wire \i_reg_345_reg[2] ;
  wire \i_reg_345_reg[3] ;
  wire \i_reg_345_reg[4] ;
  wire \i_reg_345_reg[5] ;
  wire \i_reg_345_reg[5]_0 ;
  wire \i_reg_345_reg[5]_1 ;
  wire [4:0]p_1_out;
  wire \q0[0]_i_1__4_n_4 ;
  wire \q0[1]_i_1__3_n_4 ;
  wire \q0[2]_i_1__1_n_4 ;
  wire \q0[3]_i_1__0_n_4 ;
  wire \q0[4]_i_1_n_4 ;
  wire ram_reg_0_15_0_0__0_n_4;
  wire ram_reg_0_15_0_0__1_n_4;
  wire ram_reg_0_15_0_0__2_n_4;
  wire ram_reg_0_15_0_0__3_n_4;
  wire ram_reg_0_15_0_0__4_n_4;
  wire ram_reg_0_15_0_0__5_n_4;
  wire ram_reg_0_15_0_0__6_n_4;
  wire ram_reg_0_15_0_0__7_n_4;
  wire ram_reg_0_15_0_0__8_n_4;
  wire ram_reg_0_15_0_0_n_4;
  wire ram_reg_0_31_0_0__0_i_1__2_n_4;
  wire ram_reg_0_31_0_0__0_n_4;
  wire ram_reg_0_31_0_0__1_i_1__1_n_4;
  wire ram_reg_0_31_0_0__1_n_4;
  wire ram_reg_0_31_0_0__2_i_1__1_n_4;
  wire ram_reg_0_31_0_0__2_n_4;
  wire ram_reg_0_31_0_0__3_i_1_n_4;
  wire ram_reg_0_31_0_0__3_n_4;
  wire ram_reg_0_31_0_0_i_1__3_n_4;
  wire ram_reg_0_31_0_0_n_4;

  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[0]_i_1__4 
       (.I0(p_1_out[0]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[0]),
        .I3(INPUT_STREAM_V_id_V_0_sel),
        .I4(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [0]),
        .O(\q0[0]_i_1__4_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[0]_i_2__3 
       (.I0(ram_reg_0_15_0_0__0_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0_n_4),
        .O(p_1_out[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[1]_i_1__3 
       (.I0(p_1_out[1]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_id_V_0_sel),
        .I4(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [1]),
        .O(\q0[1]_i_1__3_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[1]_i_2__2 
       (.I0(ram_reg_0_15_0_0__2_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__1_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__0_n_4),
        .O(p_1_out[1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[2]_i_1__1 
       (.I0(p_1_out[2]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[2]),
        .I3(INPUT_STREAM_V_id_V_0_sel),
        .I4(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [2]),
        .O(\q0[2]_i_1__1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[2]_i_2__1 
       (.I0(ram_reg_0_15_0_0__4_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__3_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__1_n_4),
        .O(p_1_out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[3]_i_1__0 
       (.I0(p_1_out[3]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[3]),
        .I3(INPUT_STREAM_V_id_V_0_sel),
        .I4(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [3]),
        .O(\q0[3]_i_1__0_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[3]_i_2__0 
       (.I0(ram_reg_0_15_0_0__6_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__5_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__2_n_4),
        .O(p_1_out[3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \q0[4]_i_1 
       (.I0(p_1_out[4]),
        .I1(\ap_CS_fsm_reg[3] ),
        .I2(Q[4]),
        .I3(INPUT_STREAM_V_id_V_0_sel),
        .I4(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [4]),
        .O(\q0[4]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[4]_i_2 
       (.I0(ram_reg_0_15_0_0__8_n_4),
        .I1(\i_reg_345_reg[4] ),
        .I2(ram_reg_0_15_0_0__7_n_4),
        .I3(\i_reg_345_reg[5]_1 ),
        .I4(ram_reg_0_31_0_0__3_n_4),
        .O(p_1_out[4]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[0]_i_1__4_n_4 ),
        .Q(\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] [0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[1]_i_1__3_n_4 ),
        .Q(\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] [1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[2]_i_1__1_n_4 ),
        .Q(\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] [2]),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[3]_i_1__0_n_4 ),
        .Q(\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] [3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[4]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4] [4]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__3_n_4),
        .O(ram_reg_0_15_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__3_n_4),
        .O(ram_reg_0_15_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__3
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__3_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__4
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__4_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__5
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__5_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__6
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1__1_n_4),
        .O(ram_reg_0_15_0_0__6_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__7
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__3_i_1_n_4),
        .O(ram_reg_0_15_0_0__7_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5] ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__8
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__3_i_1_n_4),
        .O(ram_reg_0_15_0_0__8_n_4),
        .WCLK(ap_clk),
        .WE(\i_reg_345_reg[5]_0 ));
  RAM32X1S ram_reg_0_31_0_0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0_i_1__3_n_4),
        .O(ram_reg_0_31_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  RAM32X1S ram_reg_0_31_0_0__0
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__0_i_1__2_n_4),
        .O(ram_reg_0_31_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__0_i_1__2
       (.I0(Q[1]),
        .I1(INPUT_STREAM_V_id_V_0_sel),
        .I2(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [1]),
        .O(ram_reg_0_31_0_0__0_i_1__2_n_4));
  RAM32X1S ram_reg_0_31_0_0__1
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__1_i_1__1_n_4),
        .O(ram_reg_0_31_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__1_i_1__1
       (.I0(Q[2]),
        .I1(INPUT_STREAM_V_id_V_0_sel),
        .I2(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [2]),
        .O(ram_reg_0_31_0_0__1_i_1__1_n_4));
  RAM32X1S ram_reg_0_31_0_0__2
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__2_i_1__1_n_4),
        .O(ram_reg_0_31_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__2_i_1__1
       (.I0(Q[3]),
        .I1(INPUT_STREAM_V_id_V_0_sel),
        .I2(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [3]),
        .O(ram_reg_0_31_0_0__2_i_1__1_n_4));
  RAM32X1S ram_reg_0_31_0_0__3
       (.A0(\i_reg_345_reg[0] ),
        .A1(\i_reg_345_reg[1] ),
        .A2(\i_reg_345_reg[2] ),
        .A3(\i_reg_345_reg[3] ),
        .A4(\i_reg_345_reg[4] ),
        .D(ram_reg_0_31_0_0__3_i_1_n_4),
        .O(ram_reg_0_31_0_0__3_n_4),
        .WCLK(ap_clk),
        .WE(\INPUT_STREAM_V_data_V_0_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__3_i_1
       (.I0(Q[4]),
        .I1(INPUT_STREAM_V_id_V_0_sel),
        .I2(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [4]),
        .O(ram_reg_0_31_0_0__3_i_1_n_4));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0_i_1__3
       (.I0(Q[0]),
        .I1(INPUT_STREAM_V_id_V_0_sel),
        .I2(\INPUT_STREAM_V_id_V_0_payload_A_reg[4] [0]),
        .O(ram_reg_0_31_0_0_i_1__3_n_4));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi
   (\q0_reg[5] ,
    E,
    ap_block_pp1_stage0_subdone1_in,
    \q0_reg[0] ,
    \q0_reg[0]_0 ,
    \q0_reg[0]_1 ,
    \q0_reg[0]_2 ,
    \q0_reg[5]_0 ,
    \q0_reg[5]_1 ,
    \q0_reg[4] ,
    \q0_reg[5]_2 ,
    \q0_reg[0]_3 ,
    \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ,
    INPUT_STREAM_V_dest_V_0_sel,
    Q,
    \INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ,
    \ap_CS_fsm_reg[5] ,
    \i_reg_345_reg[5] ,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    ap_enable_reg_pp1_iter0,
    \i_1_reg_356_reg[5] ,
    \exitcond_reg_509_reg[0] ,
    ap_enable_reg_pp1_iter1_reg,
    ap_reg_pp1_iter1_exitcond_reg_509,
    ap_enable_reg_pp1_iter2_reg,
    OUTPUT_STREAM_V_data_V_1_ack_in,
    ap_clk);
  output \q0_reg[5] ;
  output [0:0]E;
  output ap_block_pp1_stage0_subdone1_in;
  output \q0_reg[0] ;
  output \q0_reg[0]_0 ;
  output \q0_reg[0]_1 ;
  output \q0_reg[0]_2 ;
  output \q0_reg[5]_0 ;
  output \q0_reg[5]_1 ;
  output \q0_reg[4] ;
  output \q0_reg[5]_2 ;
  output \q0_reg[0]_3 ;
  output [5:0]\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ;
  input INPUT_STREAM_V_dest_V_0_sel;
  input [5:0]Q;
  input [5:0]\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ;
  input [1:0]\ap_CS_fsm_reg[5] ;
  input [5:0]\i_reg_345_reg[5] ;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input ap_enable_reg_pp1_iter0;
  input [5:0]\i_1_reg_356_reg[5] ;
  input \exitcond_reg_509_reg[0] ;
  input ap_enable_reg_pp1_iter1_reg;
  input ap_reg_pp1_iter1_exitcond_reg_509;
  input ap_enable_reg_pp1_iter2_reg;
  input OUTPUT_STREAM_V_data_V_1_ack_in;
  input ap_clk;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [5:0]\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ;
  wire INPUT_STREAM_V_dest_V_0_sel;
  wire OUTPUT_STREAM_V_data_V_1_ack_in;
  wire [5:0]\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ;
  wire [5:0]Q;
  wire [1:0]\ap_CS_fsm_reg[5] ;
  wire ap_block_pp1_stage0_subdone1_in;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire ap_enable_reg_pp1_iter1_reg;
  wire ap_enable_reg_pp1_iter2_reg;
  wire ap_reg_pp1_iter1_exitcond_reg_509;
  wire \exitcond_reg_509_reg[0] ;
  wire [5:0]\i_1_reg_356_reg[5] ;
  wire [5:0]\i_reg_345_reg[5] ;
  wire \q0_reg[0] ;
  wire \q0_reg[0]_0 ;
  wire \q0_reg[0]_1 ;
  wire \q0_reg[0]_2 ;
  wire \q0_reg[0]_3 ;
  wire \q0_reg[4] ;
  wire \q0_reg[5] ;
  wire \q0_reg[5]_0 ;
  wire \q0_reg[5]_1 ;
  wire \q0_reg[5]_2 ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi_ram Adder2_inputValuehbi_ram_U
       (.E(E),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] (\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ),
        .INPUT_STREAM_V_dest_V_0_sel(INPUT_STREAM_V_dest_V_0_sel),
        .OUTPUT_STREAM_V_data_V_1_ack_in(OUTPUT_STREAM_V_data_V_1_ack_in),
        .\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] (\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ),
        .Q(Q),
        .address0({\q0_reg[0]_2 ,\q0_reg[0]_1 ,\q0_reg[0]_0 ,\q0_reg[0] }),
        .\ap_CS_fsm_reg[5] (\ap_CS_fsm_reg[5] ),
        .ap_block_pp1_stage0_subdone1_in(ap_block_pp1_stage0_subdone1_in),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter0(ap_enable_reg_pp1_iter0),
        .ap_enable_reg_pp1_iter1_reg(ap_enable_reg_pp1_iter1_reg),
        .ap_enable_reg_pp1_iter2_reg(ap_enable_reg_pp1_iter2_reg),
        .ap_reg_pp1_iter1_exitcond_reg_509(ap_reg_pp1_iter1_exitcond_reg_509),
        .\exitcond_reg_509_reg[0] (\exitcond_reg_509_reg[0] ),
        .\i_1_reg_356_reg[5] (\i_1_reg_356_reg[5] ),
        .\i_reg_345_reg[5] (\i_reg_345_reg[5] ),
        .\q0_reg[0]_0 (\q0_reg[0]_3 ),
        .\q0_reg[4]_0 (\q0_reg[4] ),
        .\q0_reg[5]_0 (\q0_reg[5] ),
        .\q0_reg[5]_1 (\q0_reg[5]_0 ),
        .\q0_reg[5]_2 (\q0_reg[5]_1 ),
        .\q0_reg[5]_3 (\q0_reg[5]_2 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi_ram
   (\q0_reg[5]_0 ,
    E,
    ap_block_pp1_stage0_subdone1_in,
    address0,
    \q0_reg[5]_1 ,
    \q0_reg[5]_2 ,
    \q0_reg[4]_0 ,
    \q0_reg[5]_3 ,
    \q0_reg[0]_0 ,
    \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ,
    INPUT_STREAM_V_dest_V_0_sel,
    Q,
    \INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ,
    \ap_CS_fsm_reg[5] ,
    \i_reg_345_reg[5] ,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    ap_enable_reg_pp1_iter0,
    \i_1_reg_356_reg[5] ,
    \exitcond_reg_509_reg[0] ,
    ap_enable_reg_pp1_iter1_reg,
    ap_reg_pp1_iter1_exitcond_reg_509,
    ap_enable_reg_pp1_iter2_reg,
    OUTPUT_STREAM_V_data_V_1_ack_in,
    ap_clk);
  output \q0_reg[5]_0 ;
  output [0:0]E;
  output ap_block_pp1_stage0_subdone1_in;
  output [3:0]address0;
  output \q0_reg[5]_1 ;
  output \q0_reg[5]_2 ;
  output \q0_reg[4]_0 ;
  output \q0_reg[5]_3 ;
  output \q0_reg[0]_0 ;
  output [5:0]\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ;
  input INPUT_STREAM_V_dest_V_0_sel;
  input [5:0]Q;
  input [5:0]\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ;
  input [1:0]\ap_CS_fsm_reg[5] ;
  input [5:0]\i_reg_345_reg[5] ;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input ap_enable_reg_pp1_iter0;
  input [5:0]\i_1_reg_356_reg[5] ;
  input \exitcond_reg_509_reg[0] ;
  input ap_enable_reg_pp1_iter1_reg;
  input ap_reg_pp1_iter1_exitcond_reg_509;
  input ap_enable_reg_pp1_iter2_reg;
  input OUTPUT_STREAM_V_data_V_1_ack_in;
  input ap_clk;

  wire [0:0]E;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [5:0]\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] ;
  wire INPUT_STREAM_V_dest_V_0_sel;
  wire OUTPUT_STREAM_V_data_V_1_ack_in;
  wire [5:0]\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] ;
  wire [5:0]Q;
  wire [3:0]address0;
  wire [1:0]\ap_CS_fsm_reg[5] ;
  wire ap_block_pp1_stage0_subdone1_in;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire ap_enable_reg_pp1_iter1_reg;
  wire ap_enable_reg_pp1_iter2_reg;
  wire ap_reg_pp1_iter1_exitcond_reg_509;
  wire \exitcond_reg_509_reg[0] ;
  wire [5:0]\i_1_reg_356_reg[5] ;
  wire [5:0]\i_reg_345_reg[5] ;
  wire [5:0]p_1_out;
  wire \q0[0]_i_1__5_n_4 ;
  wire \q0[1]_i_1__4_n_4 ;
  wire \q0[2]_i_1__2_n_4 ;
  wire \q0[3]_i_1__1_n_4 ;
  wire \q0[4]_i_1__0_n_4 ;
  wire \q0[5]_i_1_n_4 ;
  wire \q0_reg[0]_0 ;
  wire \q0_reg[4]_0 ;
  wire \q0_reg[5]_0 ;
  wire \q0_reg[5]_1 ;
  wire \q0_reg[5]_2 ;
  wire \q0_reg[5]_3 ;
  wire ram_reg_0_15_0_0__0_n_4;
  wire ram_reg_0_15_0_0__10_n_4;
  wire ram_reg_0_15_0_0__1_n_4;
  wire ram_reg_0_15_0_0__2_n_4;
  wire ram_reg_0_15_0_0__3_n_4;
  wire ram_reg_0_15_0_0__4_n_4;
  wire ram_reg_0_15_0_0__5_n_4;
  wire ram_reg_0_15_0_0__6_n_4;
  wire ram_reg_0_15_0_0__7_n_4;
  wire ram_reg_0_15_0_0__8_n_4;
  wire ram_reg_0_15_0_0__9_n_4;
  wire ram_reg_0_15_0_0_n_4;
  wire ram_reg_0_31_0_0__0_i_1__3_n_4;
  wire ram_reg_0_31_0_0__0_n_4;
  wire ram_reg_0_31_0_0__1_i_1__2_n_4;
  wire ram_reg_0_31_0_0__1_n_4;
  wire ram_reg_0_31_0_0__2_i_1__2_n_4;
  wire ram_reg_0_31_0_0__2_n_4;
  wire ram_reg_0_31_0_0__3_i_1__0_n_4;
  wire ram_reg_0_31_0_0__3_n_4;
  wire ram_reg_0_31_0_0__4_i_1_n_4;
  wire ram_reg_0_31_0_0__4_n_4;
  wire ram_reg_0_31_0_0_i_1__4_n_4;
  wire ram_reg_0_31_0_0_n_4;

  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    \q0[0]_i_1__5 
       (.I0(INPUT_STREAM_V_dest_V_0_sel),
        .I1(Q[0]),
        .I2(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [0]),
        .I3(p_1_out[0]),
        .I4(\q0_reg[5]_0 ),
        .O(\q0[0]_i_1__5_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[0]_i_2__4 
       (.I0(ram_reg_0_15_0_0__0_n_4),
        .I1(\q0_reg[5]_1 ),
        .I2(ram_reg_0_15_0_0_n_4),
        .I3(\q0_reg[5]_2 ),
        .I4(ram_reg_0_31_0_0_n_4),
        .O(p_1_out[0]));
  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    \q0[1]_i_1__4 
       (.I0(INPUT_STREAM_V_dest_V_0_sel),
        .I1(Q[1]),
        .I2(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [1]),
        .I3(p_1_out[1]),
        .I4(\q0_reg[5]_0 ),
        .O(\q0[1]_i_1__4_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[1]_i_2__3 
       (.I0(ram_reg_0_15_0_0__2_n_4),
        .I1(\q0_reg[5]_1 ),
        .I2(ram_reg_0_15_0_0__1_n_4),
        .I3(\q0_reg[5]_2 ),
        .I4(ram_reg_0_31_0_0__0_n_4),
        .O(p_1_out[1]));
  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    \q0[2]_i_1__2 
       (.I0(INPUT_STREAM_V_dest_V_0_sel),
        .I1(Q[2]),
        .I2(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [2]),
        .I3(p_1_out[2]),
        .I4(\q0_reg[5]_0 ),
        .O(\q0[2]_i_1__2_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[2]_i_2__2 
       (.I0(ram_reg_0_15_0_0__4_n_4),
        .I1(\q0_reg[5]_1 ),
        .I2(ram_reg_0_15_0_0__3_n_4),
        .I3(\q0_reg[5]_2 ),
        .I4(ram_reg_0_31_0_0__1_n_4),
        .O(p_1_out[2]));
  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    \q0[3]_i_1__1 
       (.I0(INPUT_STREAM_V_dest_V_0_sel),
        .I1(Q[3]),
        .I2(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [3]),
        .I3(p_1_out[3]),
        .I4(\q0_reg[5]_0 ),
        .O(\q0[3]_i_1__1_n_4 ));
  LUT6 #(
    .INIT(64'hFF80FF80FF808080)) 
    \q0[3]_i_1__2 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(\ap_CS_fsm_reg[5] [1]),
        .I2(ap_block_pp1_stage0_subdone1_in),
        .I3(\ap_CS_fsm_reg[5] [0]),
        .I4(\i_reg_345_reg[5] [5]),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .O(E));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[3]_i_2__1 
       (.I0(ram_reg_0_15_0_0__6_n_4),
        .I1(\q0_reg[5]_1 ),
        .I2(ram_reg_0_15_0_0__5_n_4),
        .I3(\q0_reg[5]_2 ),
        .I4(ram_reg_0_31_0_0__2_n_4),
        .O(p_1_out[3]));
  LUT5 #(
    .INIT(32'hFFFFB0BB)) 
    \q0[3]_i_3 
       (.I0(\exitcond_reg_509_reg[0] ),
        .I1(ap_enable_reg_pp1_iter1_reg),
        .I2(ap_reg_pp1_iter1_exitcond_reg_509),
        .I3(ap_enable_reg_pp1_iter2_reg),
        .I4(OUTPUT_STREAM_V_data_V_1_ack_in),
        .O(ap_block_pp1_stage0_subdone1_in));
  LUT4 #(
    .INIT(16'hEA2A)) 
    \q0[3]_i_5 
       (.I0(\i_reg_345_reg[5] [5]),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [5]),
        .O(\q0_reg[5]_2 ));
  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    \q0[4]_i_1__0 
       (.I0(INPUT_STREAM_V_dest_V_0_sel),
        .I1(Q[4]),
        .I2(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [4]),
        .I3(p_1_out[4]),
        .I4(\q0_reg[5]_0 ),
        .O(\q0[4]_i_1__0_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[4]_i_2__0 
       (.I0(ram_reg_0_15_0_0__8_n_4),
        .I1(\q0_reg[5]_1 ),
        .I2(ram_reg_0_15_0_0__7_n_4),
        .I3(\q0_reg[5]_2 ),
        .I4(ram_reg_0_31_0_0__3_n_4),
        .O(p_1_out[4]));
  LUT5 #(
    .INIT(32'hFF00E4E4)) 
    \q0[5]_i_1 
       (.I0(INPUT_STREAM_V_dest_V_0_sel),
        .I1(Q[5]),
        .I2(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [5]),
        .I3(p_1_out[5]),
        .I4(\q0_reg[5]_0 ),
        .O(\q0[5]_i_1_n_4 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \q0[5]_i_2 
       (.I0(ram_reg_0_15_0_0__10_n_4),
        .I1(\q0_reg[5]_1 ),
        .I2(ram_reg_0_15_0_0__9_n_4),
        .I3(\q0_reg[5]_2 ),
        .I4(ram_reg_0_31_0_0__4_n_4),
        .O(p_1_out[5]));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[0]_i_1__5_n_4 ),
        .Q(\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] [0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[1]_i_1__4_n_4 ),
        .Q(\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] [1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[2]_i_1__2_n_4 ),
        .Q(\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] [2]),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[3]_i_1__1_n_4 ),
        .Q(\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] [3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[4]_i_1__0_n_4 ),
        .Q(\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] [4]),
        .R(1'b0));
  FDRE \q0_reg[5] 
       (.C(ap_clk),
        .CE(E),
        .D(\q0[5]_i_1_n_4 ),
        .Q(\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5] [5]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__4_n_4),
        .O(ram_reg_0_15_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[4]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0_i_1__4_n_4),
        .O(ram_reg_0_15_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[5]_3 ));
  LUT6 #(
    .INIT(64'h00000000A8880888)) 
    ram_reg_0_15_0_0__0_i_1
       (.I0(\q0_reg[5]_1 ),
        .I1(\i_reg_345_reg[5] [5]),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(\ap_CS_fsm_reg[5] [1]),
        .I4(\i_1_reg_356_reg[5] [5]),
        .I5(\q0_reg[5]_0 ),
        .O(\q0_reg[5]_3 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__1
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__3_n_4),
        .O(ram_reg_0_15_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[4]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__10
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__4_i_1_n_4),
        .O(ram_reg_0_15_0_0__10_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[5]_3 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__2
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__0_i_1__3_n_4),
        .O(ram_reg_0_15_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[5]_3 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__3
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__3_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[4]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__4
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__1_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__4_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[5]_3 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__5
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__5_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[4]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__6
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__2_i_1__2_n_4),
        .O(ram_reg_0_15_0_0__6_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[5]_3 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__7
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__3_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__7_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[4]_0 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__8
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__3_i_1__0_n_4),
        .O(ram_reg_0_15_0_0__8_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[5]_3 ));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0__9
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(ram_reg_0_31_0_0__4_i_1_n_4),
        .O(ram_reg_0_15_0_0__9_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h0000000054440444)) 
    ram_reg_0_15_0_0_i_1
       (.I0(\q0_reg[5]_1 ),
        .I1(\i_reg_345_reg[5] [5]),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(\ap_CS_fsm_reg[5] [1]),
        .I4(\i_1_reg_356_reg[5] [5]),
        .I5(\q0_reg[5]_0 ),
        .O(\q0_reg[4]_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    ram_reg_0_15_0_0_i_2
       (.I0(\ap_CS_fsm_reg[5] [0]),
        .I1(\i_reg_345_reg[5] [5]),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .O(\q0_reg[5]_0 ));
  RAM32X1S ram_reg_0_31_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\q0_reg[5]_1 ),
        .D(ram_reg_0_31_0_0_i_1__4_n_4),
        .O(ram_reg_0_31_0_0_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  RAM32X1S ram_reg_0_31_0_0__0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\q0_reg[5]_1 ),
        .D(ram_reg_0_31_0_0__0_i_1__3_n_4),
        .O(ram_reg_0_31_0_0__0_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__0_i_1__3
       (.I0(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [1]),
        .I1(INPUT_STREAM_V_dest_V_0_sel),
        .I2(Q[1]),
        .O(ram_reg_0_31_0_0__0_i_1__3_n_4));
  RAM32X1S ram_reg_0_31_0_0__1
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\q0_reg[5]_1 ),
        .D(ram_reg_0_31_0_0__1_i_1__2_n_4),
        .O(ram_reg_0_31_0_0__1_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__1_i_1__2
       (.I0(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [2]),
        .I1(INPUT_STREAM_V_dest_V_0_sel),
        .I2(Q[2]),
        .O(ram_reg_0_31_0_0__1_i_1__2_n_4));
  RAM32X1S ram_reg_0_31_0_0__2
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\q0_reg[5]_1 ),
        .D(ram_reg_0_31_0_0__2_i_1__2_n_4),
        .O(ram_reg_0_31_0_0__2_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__2_i_1__2
       (.I0(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [3]),
        .I1(INPUT_STREAM_V_dest_V_0_sel),
        .I2(Q[3]),
        .O(ram_reg_0_31_0_0__2_i_1__2_n_4));
  RAM32X1S ram_reg_0_31_0_0__3
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\q0_reg[5]_1 ),
        .D(ram_reg_0_31_0_0__3_i_1__0_n_4),
        .O(ram_reg_0_31_0_0__3_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__3_i_1__0
       (.I0(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [4]),
        .I1(INPUT_STREAM_V_dest_V_0_sel),
        .I2(Q[4]),
        .O(ram_reg_0_31_0_0__3_i_1__0_n_4));
  RAM32X1S ram_reg_0_31_0_0__4
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(\q0_reg[5]_1 ),
        .D(ram_reg_0_31_0_0__4_i_1_n_4),
        .O(ram_reg_0_31_0_0__4_n_4),
        .WCLK(ap_clk),
        .WE(\q0_reg[0]_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0__4_i_1
       (.I0(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [5]),
        .I1(INPUT_STREAM_V_dest_V_0_sel),
        .I2(Q[5]),
        .O(ram_reg_0_31_0_0__4_i_1_n_4));
  LUT3 #(
    .INIT(8'hB8)) 
    ram_reg_0_31_0_0_i_1__4
       (.I0(\INPUT_STREAM_V_dest_V_0_payload_B_reg[5] [0]),
        .I1(INPUT_STREAM_V_dest_V_0_sel),
        .I2(Q[0]),
        .O(ram_reg_0_31_0_0_i_1__4_n_4));
  LUT6 #(
    .INIT(64'h0000000008888888)) 
    ram_reg_0_31_0_0_i_2
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I1(\ap_CS_fsm_reg[5] [0]),
        .I2(\i_1_reg_356_reg[5] [5]),
        .I3(\ap_CS_fsm_reg[5] [1]),
        .I4(ap_enable_reg_pp1_iter0),
        .I5(\i_reg_345_reg[5] [5]),
        .O(\q0_reg[0]_0 ));
  LUT4 #(
    .INIT(16'hEA2A)) 
    ram_reg_0_31_0_0_i_3
       (.I0(\i_reg_345_reg[5] [0]),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [0]),
        .O(address0[0]));
  LUT4 #(
    .INIT(16'hEA2A)) 
    ram_reg_0_31_0_0_i_4
       (.I0(\i_reg_345_reg[5] [1]),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [1]),
        .O(address0[1]));
  LUT4 #(
    .INIT(16'hEA2A)) 
    ram_reg_0_31_0_0_i_5
       (.I0(\i_reg_345_reg[5] [2]),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [2]),
        .O(address0[2]));
  LUT4 #(
    .INIT(16'hEA2A)) 
    ram_reg_0_31_0_0_i_6
       (.I0(\i_reg_345_reg[5] [3]),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [3]),
        .O(address0[3]));
  LUT4 #(
    .INIT(16'hEA2A)) 
    ram_reg_0_31_0_0_i_7
       (.I0(\i_reg_345_reg[5] [4]),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [4]),
        .O(\q0_reg[5]_1 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram
   (I1,
    D,
    ap_clk,
    DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \i_reg_345_reg[5] ,
    E,
    \tmp_reg_484_reg[6] ,
    \i_1_reg_356_reg[5] ,
    ap_enable_reg_pp1_iter0,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [31:0]I1;
  output [31:0]D;
  input ap_clk;
  input [4:0]DIADI;
  input [3:0]Q;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]\i_reg_345_reg[5] ;
  input [0:0]E;
  input [6:0]\tmp_reg_484_reg[6] ;
  input [5:0]\i_1_reg_356_reg[5] ;
  input ap_enable_reg_pp1_iter0;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [31:0]D;
  wire [4:0]DIADI;
  wire [0:0]E;
  wire [31:0]I1;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_7 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_4 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_5 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_6 ;
  wire \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_7 ;
  wire [3:0]Q;
  wire [6:0]address0;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter0;
  wire ce016_out;
  wire [5:0]\i_1_reg_356_reg[5] ;
  wire [5:0]\i_reg_345_reg[5] ;
  wire ram_reg_i_10_n_4;
  wire ram_reg_i_11_n_4;
  wire ram_reg_i_12_n_4;
  wire ram_reg_i_13_n_4;
  wire ram_reg_i_14_n_4;
  wire ram_reg_i_15_n_4;
  wire ram_reg_i_16_n_4;
  wire ram_reg_i_17_n_4;
  wire ram_reg_i_18_n_4;
  wire ram_reg_i_19_n_4;
  wire ram_reg_i_25_n_4;
  wire ram_reg_i_26_n_4;
  wire ram_reg_i_27_n_4;
  wire ram_reg_i_28_n_4;
  wire ram_reg_i_29_n_4;
  wire ram_reg_i_30_n_4;
  wire ram_reg_i_31_n_4;
  wire ram_reg_i_32_n_4;
  wire ram_reg_i_33_n_4;
  wire ram_reg_i_34_n_4;
  wire ram_reg_i_35_n_4;
  wire ram_reg_i_36_n_4;
  wire ram_reg_i_37_n_4;
  wire ram_reg_i_38_n_4;
  wire ram_reg_i_39_n_4;
  wire ram_reg_i_40_n_4;
  wire ram_reg_i_42_n_4;
  wire ram_reg_i_9_n_4;
  wire [6:0]\tmp_reg_484_reg[6] ;
  wire we014_out;
  wire [3:3]\NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_CO_UNCONNECTED ;
  wire [0:0]\NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_O_UNCONNECTED ;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT1 #(
    .INIT(2'h1)) 
    \OUTPUT_STREAM_V_data_V_1_payload_A[0]_i_1 
       (.I0(I1[0]),
        .O(D[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2 
       (.I0(I1[0]),
        .O(\OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2_n_4 ));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_4 ),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[11:8]),
        .S(I1[11:8]));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_4 ),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[15:12]),
        .S(I1[15:12]));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_4 ),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[19:16]),
        .S(I1[19:16]));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_4 ),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[23:20]),
        .S(I1[23:20]));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_4 ),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[27:24]),
        .S(I1[27:24]));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_4 ),
        .CO({\NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_CO_UNCONNECTED [3],\OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[31:28]),
        .S(I1[31:28]));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,I1[0]}),
        .O({D[3:1],\NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_O_UNCONNECTED [0]}),
        .S({I1[3:1],\OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2_n_4 }));
  CARRY4 \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1 
       (.CI(\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_4 ),
        .CO({\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_4 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_5 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_6 ,\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_7 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(D[7:4]),
        .S(I1[7:4]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "2112" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "65" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({ram_reg_i_9_n_4,ram_reg_i_10_n_4,ram_reg_i_11_n_4,ram_reg_i_12_n_4,ram_reg_i_13_n_4,ram_reg_i_14_n_4,ram_reg_i_15_n_4,ram_reg_i_16_n_4,ram_reg_i_17_n_4,ram_reg_i_18_n_4,ram_reg_i_19_n_4,DIADI}),
        .DIBDI({1'b1,1'b1,ram_reg_i_25_n_4,ram_reg_i_26_n_4,ram_reg_i_27_n_4,ram_reg_i_28_n_4,ram_reg_i_29_n_4,ram_reg_i_30_n_4,ram_reg_i_31_n_4,ram_reg_i_32_n_4,ram_reg_i_33_n_4,ram_reg_i_34_n_4,ram_reg_i_35_n_4,ram_reg_i_36_n_4,ram_reg_i_37_n_4,ram_reg_i_38_n_4}),
        .DIPADIP({ram_reg_i_39_n_4,ram_reg_i_40_n_4}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(I1[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],I1[31:18]}),
        .DOPADOP(I1[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ce016_out),
        .ENBWREN(ce016_out),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({we014_out,we014_out}),
        .WEBWE({1'b0,1'b0,we014_out,we014_out}));
  LUT3 #(
    .INIT(8'hFE)) 
    ram_reg_i_1
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(E),
        .O(ce016_out));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_10
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_10_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_11
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_11_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_12
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_12_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_13
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_13_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_14
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_14_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_15
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_15_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_16
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_16_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_17
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_17_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_18
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_18_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_19
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_19_n_4));
  LUT5 #(
    .INIT(32'h00150000)) 
    ram_reg_i_2
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(Q[3]),
        .I4(\tmp_reg_484_reg[6] [6]),
        .O(address0[6]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_25
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_25_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_26
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_26_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_27
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_27_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_28
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_28_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_29
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_29_n_4));
  LUT6 #(
    .INIT(64'h00000000E4E4FF00)) 
    ram_reg_i_3
       (.I0(Q[1]),
        .I1(\tmp_reg_484_reg[6] [5]),
        .I2(\i_reg_345_reg[5] [5]),
        .I3(\i_1_reg_356_reg[5] [5]),
        .I4(ram_reg_i_42_n_4),
        .I5(Q[3]),
        .O(address0[5]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_30
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_30_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_31
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_31_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_32
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_32_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_33
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_33_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_34
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_34_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_35
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_35_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_36
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_36_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_37
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_37_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_38
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_38_n_4));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_39
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_39_n_4));
  LUT6 #(
    .INIT(64'h00000000E4E4FF00)) 
    ram_reg_i_4
       (.I0(Q[1]),
        .I1(\tmp_reg_484_reg[6] [4]),
        .I2(\i_reg_345_reg[5] [4]),
        .I3(\i_1_reg_356_reg[5] [4]),
        .I4(ram_reg_i_42_n_4),
        .I5(Q[3]),
        .O(address0[4]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_40
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_40_n_4));
  LUT4 #(
    .INIT(16'hAEAA)) 
    ram_reg_i_41
       (.I0(Q[0]),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I2(\i_reg_345_reg[5] [5]),
        .I3(Q[1]),
        .O(we014_out));
  LUT2 #(
    .INIT(4'h7)) 
    ram_reg_i_42
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(Q[2]),
        .O(ram_reg_i_42_n_4));
  LUT6 #(
    .INIT(64'h00000000E4E4FF00)) 
    ram_reg_i_5
       (.I0(Q[1]),
        .I1(\tmp_reg_484_reg[6] [3]),
        .I2(\i_reg_345_reg[5] [3]),
        .I3(\i_1_reg_356_reg[5] [3]),
        .I4(ram_reg_i_42_n_4),
        .I5(Q[3]),
        .O(address0[3]));
  LUT6 #(
    .INIT(64'h00000000E4E4FF00)) 
    ram_reg_i_6
       (.I0(Q[1]),
        .I1(\tmp_reg_484_reg[6] [2]),
        .I2(\i_reg_345_reg[5] [2]),
        .I3(\i_1_reg_356_reg[5] [2]),
        .I4(ram_reg_i_42_n_4),
        .I5(Q[3]),
        .O(address0[2]));
  LUT6 #(
    .INIT(64'h00000000E4E4FF00)) 
    ram_reg_i_7
       (.I0(Q[1]),
        .I1(\tmp_reg_484_reg[6] [1]),
        .I2(\i_reg_345_reg[5] [1]),
        .I3(\i_1_reg_356_reg[5] [1]),
        .I4(ram_reg_i_42_n_4),
        .I5(Q[3]),
        .O(address0[1]));
  LUT6 #(
    .INIT(64'h00000000E4E4FF00)) 
    ram_reg_i_8
       (.I0(Q[1]),
        .I1(\tmp_reg_484_reg[6] [0]),
        .I2(\i_reg_345_reg[5] [0]),
        .I3(\i_1_reg_356_reg[5] [0]),
        .I4(ram_reg_i_42_n_4),
        .I5(Q[3]),
        .O(address0[0]));
  LUT4 #(
    .INIT(16'hA0C0)) 
    ram_reg_i_9
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I2(Q[1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_9_n_4));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    OUTPUT_STREAM_TVALID,
    OUTPUT_STREAM_TREADY,
    OUTPUT_STREAM_TDATA,
    OUTPUT_STREAM_TDEST,
    OUTPUT_STREAM_TKEEP,
    OUTPUT_STREAM_TSTRB,
    OUTPUT_STREAM_TUSER,
    OUTPUT_STREAM_TLAST,
    OUTPUT_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [4:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM:OUTPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME OUTPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 32 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 2} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 2}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) output OUTPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TREADY" *) input OUTPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TDATA" *) output [31:0]OUTPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TDEST" *) output [5:0]OUTPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TKEEP" *) output [3:0]OUTPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TSTRB" *) output [3:0]OUTPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TUSER" *) output [1:0]OUTPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TLAST" *) output [0:0]OUTPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TID" *) output [4:0]OUTPUT_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire [31:0]OUTPUT_STREAM_TDATA;
  wire [5:0]OUTPUT_STREAM_TDEST;
  wire [4:0]OUTPUT_STREAM_TID;
  wire [3:0]OUTPUT_STREAM_TKEEP;
  wire [0:0]OUTPUT_STREAM_TLAST;
  wire OUTPUT_STREAM_TREADY;
  wire [3:0]OUTPUT_STREAM_TSTRB;
  wire [1:0]OUTPUT_STREAM_TUSER;
  wire OUTPUT_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .OUTPUT_STREAM_TDATA(OUTPUT_STREAM_TDATA),
        .OUTPUT_STREAM_TDEST(OUTPUT_STREAM_TDEST),
        .OUTPUT_STREAM_TID(OUTPUT_STREAM_TID),
        .OUTPUT_STREAM_TKEEP(OUTPUT_STREAM_TKEEP),
        .OUTPUT_STREAM_TLAST(OUTPUT_STREAM_TLAST),
        .OUTPUT_STREAM_TREADY(OUTPUT_STREAM_TREADY),
        .OUTPUT_STREAM_TSTRB(OUTPUT_STREAM_TSTRB),
        .OUTPUT_STREAM_TUSER(OUTPUT_STREAM_TUSER),
        .OUTPUT_STREAM_TVALID(OUTPUT_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
