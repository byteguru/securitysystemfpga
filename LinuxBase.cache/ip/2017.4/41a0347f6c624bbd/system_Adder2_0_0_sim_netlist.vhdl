-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Mon Feb  5 13:43:23 2018
-- Host        : DESKTOP-7E8QPPK running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.vhdl
-- Design      : system_Adder2_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom is
  port (
    DIADI : out STD_LOGIC_VECTOR ( 4 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    INPUT_STREAM_V_data_V_0_sel : in STD_LOGIC;
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \invdar_reg_334_reg[6]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ap_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom is
  signal g0_b0_n_4 : STD_LOGIC;
  signal g0_b1_n_4 : STD_LOGIC;
  signal g0_b2_n_4 : STD_LOGIC;
  signal g0_b3_n_4 : STD_LOGIC;
  signal g0_b4_n_4 : STD_LOGIC;
  signal g1_b1_n_4 : STD_LOGIC;
  signal g1_b2_n_4 : STD_LOGIC;
  signal g1_b4_n_4 : STD_LOGIC;
  signal q0 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \q0[0]_i_1_n_4\ : STD_LOGIC;
  signal \q0[1]_i_1_n_4\ : STD_LOGIC;
  signal \q0[3]_i_1__3_n_4\ : STD_LOGIC;
  signal \q0_reg[2]_i_1_n_4\ : STD_LOGIC;
  signal \q0_reg[4]_i_1_n_4\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q0[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \q0[1]_i_1\ : label is "soft_lutpair7";
begin
g0_b0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"552AA554AA9552AA"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g0_b0_n_4
    );
g0_b1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"99B33666CCD99B33"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g0_b1_n_4
    );
g0_b2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1E43C8790F21E43C"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g0_b2_n_4
    );
g0_b3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1F83F07E0FC1F83F"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g0_b3_n_4
    );
g0_b4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E07C0F81F03E07C0"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g0_b4_n_4
    );
g1_b1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g1_b1_n_4
    );
g1_b2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(0),
      I1 => \invdar_reg_334_reg[6]\(1),
      I2 => \invdar_reg_334_reg[6]\(2),
      I3 => \invdar_reg_334_reg[6]\(3),
      I4 => \invdar_reg_334_reg[6]\(4),
      I5 => \invdar_reg_334_reg[6]\(5),
      O => g1_b2_n_4
    );
g1_b4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \invdar_reg_334_reg[6]\(1),
      I1 => \invdar_reg_334_reg[6]\(2),
      I2 => \invdar_reg_334_reg[6]\(3),
      I3 => \invdar_reg_334_reg[6]\(4),
      I4 => \invdar_reg_334_reg[6]\(5),
      O => g1_b4_n_4
    );
\q0[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => g1_b1_n_4,
      I1 => \invdar_reg_334_reg[6]\(6),
      I2 => g0_b0_n_4,
      O => \q0[0]_i_1_n_4\
    );
\q0[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => g1_b1_n_4,
      I1 => \invdar_reg_334_reg[6]\(6),
      I2 => g0_b1_n_4,
      O => \q0[1]_i_1_n_4\
    );
\q0[3]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => g0_b3_n_4,
      I1 => \invdar_reg_334_reg[6]\(6),
      O => \q0[3]_i_1__3_n_4\
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg[3]\(0),
      D => \q0[0]_i_1_n_4\,
      Q => q0(0),
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg[3]\(0),
      D => \q0[1]_i_1_n_4\,
      Q => q0(1),
      R => '0'
    );
\q0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg[3]\(0),
      D => \q0_reg[2]_i_1_n_4\,
      Q => q0(2),
      R => '0'
    );
\q0_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => g0_b2_n_4,
      I1 => g1_b2_n_4,
      O => \q0_reg[2]_i_1_n_4\,
      S => \invdar_reg_334_reg[6]\(6)
    );
\q0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg[3]\(0),
      D => \q0[3]_i_1__3_n_4\,
      Q => q0(3),
      R => '0'
    );
\q0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \ap_CS_fsm_reg[3]\(0),
      D => \q0_reg[4]_i_1_n_4\,
      Q => q0(4),
      R => '0'
    );
\q0_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => g0_b4_n_4,
      I1 => g1_b4_n_4,
      O => \q0_reg[4]_i_1_n_4\,
      S => \invdar_reg_334_reg[6]\(6)
    );
ram_reg_i_20: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAAAA"
    )
        port map (
      I0 => q0(4),
      I1 => Q(4),
      I2 => \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(4),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      I4 => \ap_CS_fsm_reg[3]\(1),
      O => DIADI(4)
    );
ram_reg_i_21: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAAAA"
    )
        port map (
      I0 => q0(3),
      I1 => Q(3),
      I2 => \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(3),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      I4 => \ap_CS_fsm_reg[3]\(1),
      O => DIADI(3)
    );
ram_reg_i_22: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAAAA"
    )
        port map (
      I0 => q0(2),
      I1 => Q(2),
      I2 => \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(2),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      I4 => \ap_CS_fsm_reg[3]\(1),
      O => DIADI(2)
    );
ram_reg_i_23: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAAAA"
    )
        port map (
      I0 => q0(1),
      I1 => Q(1),
      I2 => \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      I4 => \ap_CS_fsm_reg[3]\(1),
      O => DIADI(1)
    );
ram_reg_i_24: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAAAA"
    )
        port map (
      I0 => q0(0),
      I1 => Q(0),
      I2 => \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(0),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      I4 => \ap_CS_fsm_reg[3]\(1),
      O => DIADI(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi is
  port (
    reset : out STD_LOGIC;
    s_axi_CONTROL_BUS_RVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_ARREADY : out STD_LOGIC;
    \out\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \int_ap_return_reg[31]_0\ : out STD_LOGIC;
    interrupt : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_CONTROL_BUS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ap_clk : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_RREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_WDATA : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_CONTROL_BUS_WVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_WSTRB : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    OUTPUT_STREAM_V_data_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_keep_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_dest_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_id_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_strb_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_ack_in : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ap_rst_n : in STD_LOGIC;
    \tmp_1_reg_494_reg[0]\ : in STD_LOGIC;
    s_axi_CONTROL_BUS_BREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    I1 : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi is
  signal \/FSM_onehot_wstate[1]_i_1_n_4\ : STD_LOGIC;
  signal \/FSM_onehot_wstate[2]_i_1_n_4\ : STD_LOGIC;
  signal \FSM_onehot_wstate[3]_i_1_n_4\ : STD_LOGIC;
  signal \FSM_onehot_wstate_reg_n_4_[0]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_wstate_reg_n_4_[0]\ : signal is "yes";
  signal ap_done : STD_LOGIC;
  signal ap_start : STD_LOGIC;
  signal ar_hs : STD_LOGIC;
  signal int_ap_done : STD_LOGIC;
  signal int_ap_done_i_1_n_4 : STD_LOGIC;
  signal int_ap_idle : STD_LOGIC;
  signal int_ap_idle_i_1_n_4 : STD_LOGIC;
  signal int_ap_ready : STD_LOGIC;
  signal int_ap_return : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^int_ap_return_reg[31]_0\ : STD_LOGIC;
  signal int_ap_start3_out : STD_LOGIC;
  signal int_ap_start_i_1_n_4 : STD_LOGIC;
  signal int_auto_restart : STD_LOGIC;
  signal int_auto_restart_i_1_n_4 : STD_LOGIC;
  signal int_gie_i_1_n_4 : STD_LOGIC;
  signal int_gie_reg_n_4 : STD_LOGIC;
  signal \int_ier[0]_i_1_n_4\ : STD_LOGIC;
  signal \int_ier[1]_i_1_n_4\ : STD_LOGIC;
  signal \int_ier[1]_i_2_n_4\ : STD_LOGIC;
  signal \int_ier_reg_n_4_[0]\ : STD_LOGIC;
  signal int_isr6_out : STD_LOGIC;
  signal \int_isr[0]_i_1_n_4\ : STD_LOGIC;
  signal \int_isr[1]_i_1_n_4\ : STD_LOGIC;
  signal \int_isr_reg_n_4_[0]\ : STD_LOGIC;
  signal \^out\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute RTL_KEEP of \^out\ : signal is "yes";
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal rdata_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \rdata_data[0]_i_2_n_4\ : STD_LOGIC;
  signal \rdata_data[0]_i_3_n_4\ : STD_LOGIC;
  signal \rdata_data[1]_i_2_n_4\ : STD_LOGIC;
  signal \rdata_data[1]_i_3_n_4\ : STD_LOGIC;
  signal \rdata_data[31]_i_1_n_4\ : STD_LOGIC;
  signal \rdata_data[7]_i_2_n_4\ : STD_LOGIC;
  signal \rdata_data[7]_i_3_n_4\ : STD_LOGIC;
  signal \^reset\ : STD_LOGIC;
  signal rstate : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rstate[0]_i_1_n_4\ : STD_LOGIC;
  signal waddr : STD_LOGIC;
  signal \waddr_reg_n_4_[0]\ : STD_LOGIC;
  signal \waddr_reg_n_4_[1]\ : STD_LOGIC;
  signal \waddr_reg_n_4_[2]\ : STD_LOGIC;
  signal \waddr_reg_n_4_[3]\ : STD_LOGIC;
  signal \waddr_reg_n_4_[4]\ : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[0]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_wstate_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[1]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[2]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[3]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ap_CS_fsm[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of int_ap_start_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of int_ap_start_i_2 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \int_ier[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \int_ier[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \int_isr[0]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \invdar_reg_334[6]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \rdata_data[1]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \rdata_data[1]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \rdata_data[7]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \rdata_data[7]_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \rstate[0]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of s_axi_CONTROL_BUS_RVALID_INST_0 : label is "soft_lutpair6";
begin
  \int_ap_return_reg[31]_0\ <= \^int_ap_return_reg[31]_0\;
  \out\(2 downto 0) <= \^out\(2 downto 0);
  reset <= \^reset\;
\/FSM_onehot_wstate[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000BFF0B"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_BREADY,
      I1 => \^out\(2),
      I2 => \^out\(1),
      I3 => \^out\(0),
      I4 => s_axi_CONTROL_BUS_AWVALID,
      O => \/FSM_onehot_wstate[1]_i_1_n_4\
    );
\/FSM_onehot_wstate[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_AWVALID,
      I1 => \^out\(0),
      I2 => \^out\(1),
      I3 => s_axi_CONTROL_BUS_WVALID,
      O => \/FSM_onehot_wstate[2]_i_1_n_4\
    );
\FSM_onehot_wstate[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000F404"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_BREADY,
      I1 => \^out\(2),
      I2 => \^out\(1),
      I3 => s_axi_CONTROL_BUS_WVALID,
      I4 => \^out\(0),
      O => \FSM_onehot_wstate[3]_i_1_n_4\
    );
\FSM_onehot_wstate_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => '0',
      Q => \FSM_onehot_wstate_reg_n_4_[0]\,
      S => \^reset\
    );
\FSM_onehot_wstate_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \/FSM_onehot_wstate[1]_i_1_n_4\,
      Q => \^out\(0),
      R => \^reset\
    );
\FSM_onehot_wstate_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \/FSM_onehot_wstate[2]_i_1_n_4\,
      Q => \^out\(1),
      R => \^reset\
    );
\FSM_onehot_wstate_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \FSM_onehot_wstate[3]_i_1_n_4\,
      Q => \^out\(2),
      R => \^reset\
    );
\INPUT_STREAM_V_dest_V_0_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => \^reset\
    );
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => ap_start,
      I1 => Q(0),
      I2 => ap_done,
      O => D(0)
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88F8"
    )
        port map (
      I0 => ap_start,
      I1 => Q(0),
      I2 => Q(1),
      I3 => \tmp_1_reg_494_reg[0]\,
      O => D(1)
    );
int_ap_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFDFFFFFF0000"
    )
        port map (
      I0 => \rdata_data[7]_i_3_n_4\,
      I1 => rstate(1),
      I2 => rstate(0),
      I3 => s_axi_CONTROL_BUS_ARVALID,
      I4 => ap_done,
      I5 => int_ap_done,
      O => int_ap_done_i_1_n_4
    );
int_ap_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_done_i_1_n_4,
      Q => int_ap_done,
      R => \^reset\
    );
int_ap_idle_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Q(0),
      I1 => ap_start,
      O => int_ap_idle_i_1_n_4
    );
int_ap_idle_reg: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_idle_i_1_n_4,
      Q => int_ap_idle,
      R => \^reset\
    );
int_ap_ready_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => Q(2),
      I1 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I4 => \^int_ap_return_reg[31]_0\,
      O => ap_done
    );
int_ap_ready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => OUTPUT_STREAM_V_last_V_1_ack_in,
      I1 => OUTPUT_STREAM_V_id_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_strb_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_user_V_1_ack_in,
      O => \^int_ap_return_reg[31]_0\
    );
int_ap_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => ap_done,
      Q => int_ap_ready,
      R => \^reset\
    );
\int_ap_return_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(0),
      Q => int_ap_return(0),
      R => \^reset\
    );
\int_ap_return_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(10),
      Q => int_ap_return(10),
      R => \^reset\
    );
\int_ap_return_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(11),
      Q => int_ap_return(11),
      R => \^reset\
    );
\int_ap_return_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(12),
      Q => int_ap_return(12),
      R => \^reset\
    );
\int_ap_return_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(13),
      Q => int_ap_return(13),
      R => \^reset\
    );
\int_ap_return_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(14),
      Q => int_ap_return(14),
      R => \^reset\
    );
\int_ap_return_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(15),
      Q => int_ap_return(15),
      R => \^reset\
    );
\int_ap_return_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(16),
      Q => int_ap_return(16),
      R => \^reset\
    );
\int_ap_return_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(17),
      Q => int_ap_return(17),
      R => \^reset\
    );
\int_ap_return_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(18),
      Q => int_ap_return(18),
      R => \^reset\
    );
\int_ap_return_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(19),
      Q => int_ap_return(19),
      R => \^reset\
    );
\int_ap_return_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(1),
      Q => int_ap_return(1),
      R => \^reset\
    );
\int_ap_return_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(20),
      Q => int_ap_return(20),
      R => \^reset\
    );
\int_ap_return_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(21),
      Q => int_ap_return(21),
      R => \^reset\
    );
\int_ap_return_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(22),
      Q => int_ap_return(22),
      R => \^reset\
    );
\int_ap_return_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(23),
      Q => int_ap_return(23),
      R => \^reset\
    );
\int_ap_return_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(24),
      Q => int_ap_return(24),
      R => \^reset\
    );
\int_ap_return_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(25),
      Q => int_ap_return(25),
      R => \^reset\
    );
\int_ap_return_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(26),
      Q => int_ap_return(26),
      R => \^reset\
    );
\int_ap_return_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(27),
      Q => int_ap_return(27),
      R => \^reset\
    );
\int_ap_return_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(28),
      Q => int_ap_return(28),
      R => \^reset\
    );
\int_ap_return_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(29),
      Q => int_ap_return(29),
      R => \^reset\
    );
\int_ap_return_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(2),
      Q => int_ap_return(2),
      R => \^reset\
    );
\int_ap_return_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(30),
      Q => int_ap_return(30),
      R => \^reset\
    );
\int_ap_return_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(31),
      Q => int_ap_return(31),
      R => \^reset\
    );
\int_ap_return_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(3),
      Q => int_ap_return(3),
      R => \^reset\
    );
\int_ap_return_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(4),
      Q => int_ap_return(4),
      R => \^reset\
    );
\int_ap_return_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(5),
      Q => int_ap_return(5),
      R => \^reset\
    );
\int_ap_return_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(6),
      Q => int_ap_return(6),
      R => \^reset\
    );
\int_ap_return_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(7),
      Q => int_ap_return(7),
      R => \^reset\
    );
\int_ap_return_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(8),
      Q => int_ap_return(8),
      R => \^reset\
    );
\int_ap_return_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => I1(9),
      Q => int_ap_return(9),
      R => \^reset\
    );
int_ap_start_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBF8"
    )
        port map (
      I0 => int_auto_restart,
      I1 => ap_done,
      I2 => int_ap_start3_out,
      I3 => ap_start,
      O => int_ap_start_i_1_n_4
    );
int_ap_start_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => \waddr_reg_n_4_[2]\,
      I1 => s_axi_CONTROL_BUS_WDATA(0),
      I2 => \waddr_reg_n_4_[3]\,
      I3 => \int_ier[1]_i_2_n_4\,
      O => int_ap_start3_out
    );
int_ap_start_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_start_i_1_n_4,
      Q => ap_start,
      R => \^reset\
    );
int_auto_restart_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0200"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(2),
      I1 => \waddr_reg_n_4_[3]\,
      I2 => \waddr_reg_n_4_[2]\,
      I3 => \int_ier[1]_i_2_n_4\,
      I4 => int_auto_restart,
      O => int_auto_restart_i_1_n_4
    );
int_auto_restart_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_auto_restart_i_1_n_4,
      Q => int_auto_restart,
      R => \^reset\
    );
int_gie_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => \waddr_reg_n_4_[3]\,
      I2 => \waddr_reg_n_4_[2]\,
      I3 => \int_ier[1]_i_2_n_4\,
      I4 => int_gie_reg_n_4,
      O => int_gie_i_1_n_4
    );
int_gie_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_gie_i_1_n_4,
      Q => int_gie_reg_n_4,
      R => \^reset\
    );
\int_ier[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => \waddr_reg_n_4_[2]\,
      I2 => \waddr_reg_n_4_[3]\,
      I3 => \int_ier[1]_i_2_n_4\,
      I4 => \int_ier_reg_n_4_[0]\,
      O => \int_ier[0]_i_1_n_4\
    );
\int_ier[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFF2000"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(1),
      I1 => \waddr_reg_n_4_[2]\,
      I2 => \waddr_reg_n_4_[3]\,
      I3 => \int_ier[1]_i_2_n_4\,
      I4 => p_0_in,
      O => \int_ier[1]_i_1_n_4\
    );
\int_ier[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000000000"
    )
        port map (
      I0 => \waddr_reg_n_4_[1]\,
      I1 => \waddr_reg_n_4_[4]\,
      I2 => \^out\(1),
      I3 => s_axi_CONTROL_BUS_WVALID,
      I4 => \waddr_reg_n_4_[0]\,
      I5 => s_axi_CONTROL_BUS_WSTRB(0),
      O => \int_ier[1]_i_2_n_4\
    );
\int_ier_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_ier[0]_i_1_n_4\,
      Q => \int_ier_reg_n_4_[0]\,
      R => \^reset\
    );
\int_ier_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_ier[1]_i_1_n_4\,
      Q => p_0_in,
      R => \^reset\
    );
\int_isr[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F777F888"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(0),
      I1 => int_isr6_out,
      I2 => \int_ier_reg_n_4_[0]\,
      I3 => ap_done,
      I4 => \int_isr_reg_n_4_[0]\,
      O => \int_isr[0]_i_1_n_4\
    );
\int_isr[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \waddr_reg_n_4_[3]\,
      I1 => \waddr_reg_n_4_[2]\,
      I2 => \int_ier[1]_i_2_n_4\,
      O => int_isr6_out
    );
\int_isr[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F777F888"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_WDATA(1),
      I1 => int_isr6_out,
      I2 => p_0_in,
      I3 => ap_done,
      I4 => p_1_in,
      O => \int_isr[1]_i_1_n_4\
    );
\int_isr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_isr[0]_i_1_n_4\,
      Q => \int_isr_reg_n_4_[0]\,
      R => \^reset\
    );
\int_isr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \int_isr[1]_i_1_n_4\,
      Q => p_1_in,
      R => \^reset\
    );
interrupt_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \int_isr_reg_n_4_[0]\,
      I1 => p_1_in,
      I2 => int_gie_reg_n_4,
      O => interrupt
    );
\invdar_reg_334[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8808"
    )
        port map (
      I0 => ap_start,
      I1 => Q(0),
      I2 => Q(1),
      I3 => \tmp_1_reg_494_reg[0]\,
      O => SR(0)
    );
\rdata_data[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFBAAAAAAEAAAAA"
    )
        port map (
      I0 => \rdata_data[0]_i_2_n_4\,
      I1 => s_axi_CONTROL_BUS_ARADDR(3),
      I2 => \int_ier_reg_n_4_[0]\,
      I3 => s_axi_CONTROL_BUS_ARADDR(4),
      I4 => \rdata_data[1]_i_3_n_4\,
      I5 => ap_start,
      O => rdata_data(0)
    );
\rdata_data[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8FFF88888888888"
    )
        port map (
      I0 => int_ap_return(0),
      I1 => \rdata_data[7]_i_2_n_4\,
      I2 => \int_isr_reg_n_4_[0]\,
      I3 => s_axi_CONTROL_BUS_ARADDR(3),
      I4 => int_gie_reg_n_4,
      I5 => \rdata_data[0]_i_3_n_4\,
      O => \rdata_data[0]_i_2_n_4\
    );
\rdata_data[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(4),
      I1 => s_axi_CONTROL_BUS_ARADDR(2),
      I2 => s_axi_CONTROL_BUS_ARADDR(1),
      I3 => s_axi_CONTROL_BUS_ARADDR(0),
      O => \rdata_data[0]_i_3_n_4\
    );
\rdata_data[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3B0B300038083000"
    )
        port map (
      I0 => int_ap_return(1),
      I1 => s_axi_CONTROL_BUS_ARADDR(4),
      I2 => s_axi_CONTROL_BUS_ARADDR(3),
      I3 => \rdata_data[1]_i_2_n_4\,
      I4 => \rdata_data[1]_i_3_n_4\,
      I5 => int_ap_done,
      O => rdata_data(1)
    );
\rdata_data[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000A000C"
    )
        port map (
      I0 => p_1_in,
      I1 => p_0_in,
      I2 => s_axi_CONTROL_BUS_ARADDR(1),
      I3 => s_axi_CONTROL_BUS_ARADDR(0),
      I4 => s_axi_CONTROL_BUS_ARADDR(2),
      O => \rdata_data[1]_i_2_n_4\
    );
\rdata_data[1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(1),
      I1 => s_axi_CONTROL_BUS_ARADDR(0),
      I2 => s_axi_CONTROL_BUS_ARADDR(2),
      O => \rdata_data[1]_i_3_n_4\
    );
\rdata_data[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => int_ap_return(2),
      I1 => \rdata_data[7]_i_2_n_4\,
      I2 => int_ap_idle,
      I3 => \rdata_data[7]_i_3_n_4\,
      O => rdata_data(2)
    );
\rdata_data[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA2"
    )
        port map (
      I0 => ar_hs,
      I1 => s_axi_CONTROL_BUS_ARADDR(4),
      I2 => s_axi_CONTROL_BUS_ARADDR(2),
      I3 => s_axi_CONTROL_BUS_ARADDR(0),
      I4 => s_axi_CONTROL_BUS_ARADDR(1),
      I5 => s_axi_CONTROL_BUS_ARADDR(3),
      O => \rdata_data[31]_i_1_n_4\
    );
\rdata_data[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => rstate(1),
      I1 => rstate(0),
      I2 => s_axi_CONTROL_BUS_ARVALID,
      O => ar_hs
    );
\rdata_data[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => int_ap_return(3),
      I1 => \rdata_data[7]_i_2_n_4\,
      I2 => int_ap_ready,
      I3 => \rdata_data[7]_i_3_n_4\,
      O => rdata_data(3)
    );
\rdata_data[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => int_ap_return(7),
      I1 => \rdata_data[7]_i_2_n_4\,
      I2 => int_auto_restart,
      I3 => \rdata_data[7]_i_3_n_4\,
      O => rdata_data(7)
    );
\rdata_data[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(3),
      I1 => s_axi_CONTROL_BUS_ARADDR(1),
      I2 => s_axi_CONTROL_BUS_ARADDR(0),
      I3 => s_axi_CONTROL_BUS_ARADDR(2),
      I4 => s_axi_CONTROL_BUS_ARADDR(4),
      O => \rdata_data[7]_i_2_n_4\
    );
\rdata_data[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARADDR(3),
      I1 => s_axi_CONTROL_BUS_ARADDR(1),
      I2 => s_axi_CONTROL_BUS_ARADDR(0),
      I3 => s_axi_CONTROL_BUS_ARADDR(2),
      I4 => s_axi_CONTROL_BUS_ARADDR(4),
      O => \rdata_data[7]_i_3_n_4\
    );
\rdata_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(0),
      Q => s_axi_CONTROL_BUS_RDATA(0),
      R => '0'
    );
\rdata_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(10),
      Q => s_axi_CONTROL_BUS_RDATA(10),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(11),
      Q => s_axi_CONTROL_BUS_RDATA(11),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(12),
      Q => s_axi_CONTROL_BUS_RDATA(12),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(13),
      Q => s_axi_CONTROL_BUS_RDATA(13),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(14),
      Q => s_axi_CONTROL_BUS_RDATA(14),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(15),
      Q => s_axi_CONTROL_BUS_RDATA(15),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(16),
      Q => s_axi_CONTROL_BUS_RDATA(16),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(17),
      Q => s_axi_CONTROL_BUS_RDATA(17),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(18),
      Q => s_axi_CONTROL_BUS_RDATA(18),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(19),
      Q => s_axi_CONTROL_BUS_RDATA(19),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(1),
      Q => s_axi_CONTROL_BUS_RDATA(1),
      R => '0'
    );
\rdata_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(20),
      Q => s_axi_CONTROL_BUS_RDATA(20),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(21),
      Q => s_axi_CONTROL_BUS_RDATA(21),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(22),
      Q => s_axi_CONTROL_BUS_RDATA(22),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(23),
      Q => s_axi_CONTROL_BUS_RDATA(23),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(24),
      Q => s_axi_CONTROL_BUS_RDATA(24),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(25),
      Q => s_axi_CONTROL_BUS_RDATA(25),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(26),
      Q => s_axi_CONTROL_BUS_RDATA(26),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(27),
      Q => s_axi_CONTROL_BUS_RDATA(27),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(28),
      Q => s_axi_CONTROL_BUS_RDATA(28),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(29),
      Q => s_axi_CONTROL_BUS_RDATA(29),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(2),
      Q => s_axi_CONTROL_BUS_RDATA(2),
      R => '0'
    );
\rdata_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(30),
      Q => s_axi_CONTROL_BUS_RDATA(30),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(31),
      Q => s_axi_CONTROL_BUS_RDATA(31),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(3),
      Q => s_axi_CONTROL_BUS_RDATA(3),
      R => '0'
    );
\rdata_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(4),
      Q => s_axi_CONTROL_BUS_RDATA(4),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(5),
      Q => s_axi_CONTROL_BUS_RDATA(5),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(6),
      Q => s_axi_CONTROL_BUS_RDATA(6),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(7),
      Q => s_axi_CONTROL_BUS_RDATA(7),
      R => '0'
    );
\rdata_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(8),
      Q => s_axi_CONTROL_BUS_RDATA(8),
      R => \rdata_data[31]_i_1_n_4\
    );
\rdata_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => int_ap_return(9),
      Q => s_axi_CONTROL_BUS_RDATA(9),
      R => \rdata_data[31]_i_1_n_4\
    );
\rstate[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"020E"
    )
        port map (
      I0 => s_axi_CONTROL_BUS_ARVALID,
      I1 => rstate(0),
      I2 => rstate(1),
      I3 => s_axi_CONTROL_BUS_RREADY,
      O => \rstate[0]_i_1_n_4\
    );
\rstate_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \rstate[0]_i_1_n_4\,
      Q => rstate(0),
      R => \^reset\
    );
\rstate_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => '0',
      Q => rstate(1),
      S => \^reset\
    );
s_axi_CONTROL_BUS_ARREADY_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rstate(0),
      I1 => rstate(1),
      O => s_axi_CONTROL_BUS_ARREADY
    );
s_axi_CONTROL_BUS_RVALID_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rstate(0),
      I1 => rstate(1),
      O => s_axi_CONTROL_BUS_RVALID
    );
\waddr[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out\(0),
      I1 => s_axi_CONTROL_BUS_AWVALID,
      O => waddr
    );
\waddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(0),
      Q => \waddr_reg_n_4_[0]\,
      R => '0'
    );
\waddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(1),
      Q => \waddr_reg_n_4_[1]\,
      R => '0'
    );
\waddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(2),
      Q => \waddr_reg_n_4_[2]\,
      R => '0'
    );
\waddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(3),
      Q => \waddr_reg_n_4_[3]\,
      R => '0'
    );
\waddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CONTROL_BUS_AWADDR(4),
      Q => \waddr_reg_n_4_[4]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram is
  port (
    \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_V_strb_V_0_sel : in STD_LOGIC;
    \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    address0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram is
  signal p_1_out : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \q0[0]_i_1__1_n_4\ : STD_LOGIC;
  signal \q0[1]_i_1__1_n_4\ : STD_LOGIC;
  signal \q0[2]_i_1__0_n_4\ : STD_LOGIC;
  signal \q0[3]_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__4_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__5_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__6_n_4\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_n_4 : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_i_1__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_i_1__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_i_1__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0_i_1__1_n_4\ : STD_LOGIC;
  signal ram_reg_0_31_0_0_n_4 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__0\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__1\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__2\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__3\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__4\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__5\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__6\ : label is "RAM16X1S";
begin
\q0[0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(0),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(0),
      I3 => INPUT_STREAM_V_strb_V_0_sel,
      I4 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(0),
      O => \q0[0]_i_1__1_n_4\
    );
\q0[0]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__0_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => ram_reg_0_15_0_0_n_4,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => ram_reg_0_31_0_0_n_4,
      O => p_1_out(0)
    );
\q0[1]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(1),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(1),
      I3 => INPUT_STREAM_V_strb_V_0_sel,
      I4 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(1),
      O => \q0[1]_i_1__1_n_4\
    );
\q0[1]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__2_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__1_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__0_n_4\,
      O => p_1_out(1)
    );
\q0[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(2),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(2),
      I3 => INPUT_STREAM_V_strb_V_0_sel,
      I4 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(2),
      O => \q0[2]_i_1__0_n_4\
    );
\q0[2]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__4_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__3_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__1_n_4\,
      O => p_1_out(2)
    );
\q0[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(3),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(3),
      I3 => INPUT_STREAM_V_strb_V_0_sel,
      I4 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(3),
      O => \q0[3]_i_1_n_4\
    );
\q0[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__6_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__5_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__2_n_4\,
      O => p_1_out(3)
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[0]_i_1__1_n_4\,
      Q => \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(0),
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[1]_i_1__1_n_4\,
      Q => \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(1),
      R => '0'
    );
\q0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[2]_i_1__0_n_4\,
      Q => \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(2),
      R => '0'
    );
\q0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[3]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(3),
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__1_n_4\,
      O => ram_reg_0_15_0_0_n_4,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__0\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__1\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__2\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__3\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__3_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__4\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__4_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__5\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__5_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__6\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__6_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
ram_reg_0_31_0_0: unisim.vcomponents.RAM32X1S
     port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0_i_1__1_n_4\,
      O => ram_reg_0_31_0_0_n_4,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__0_i_1__0_n_4\,
      O => \ram_reg_0_31_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => INPUT_STREAM_V_strb_V_0_sel,
      I2 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(1),
      O => \ram_reg_0_31_0_0__0_i_1__0_n_4\
    );
\ram_reg_0_31_0_0__1\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__1_i_1__0_n_4\,
      O => \ram_reg_0_31_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__1_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(2),
      I1 => INPUT_STREAM_V_strb_V_0_sel,
      I2 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(2),
      O => \ram_reg_0_31_0_0__1_i_1__0_n_4\
    );
\ram_reg_0_31_0_0__2\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__2_i_1__0_n_4\,
      O => \ram_reg_0_31_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__2_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(3),
      I1 => INPUT_STREAM_V_strb_V_0_sel,
      I2 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(3),
      O => \ram_reg_0_31_0_0__2_i_1__0_n_4\
    );
\ram_reg_0_31_0_0_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => INPUT_STREAM_V_strb_V_0_sel,
      I2 => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(0),
      O => \ram_reg_0_31_0_0_i_1__1_n_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1 is
  port (
    \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_V_keep_V_0_sel : in STD_LOGIC;
    \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1 : entity is "Adder2_inputValuecud_ram";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1 is
  signal p_1_out : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \q0[0]_i_1__0_n_4\ : STD_LOGIC;
  signal \q0[1]_i_1__0_n_4\ : STD_LOGIC;
  signal \q0[2]_i_1_n_4\ : STD_LOGIC;
  signal \q0[3]_i_2__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__4_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__5_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__6_n_4\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_n_4 : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0_i_1__0_n_4\ : STD_LOGIC;
  signal ram_reg_0_31_0_0_n_4 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__0\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__1\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__2\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__3\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__4\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__5\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__6\ : label is "RAM16X1S";
begin
\q0[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(0),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(0),
      I3 => INPUT_STREAM_V_keep_V_0_sel,
      I4 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(0),
      O => \q0[0]_i_1__0_n_4\
    );
\q0[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__0_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => ram_reg_0_15_0_0_n_4,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => ram_reg_0_31_0_0_n_4,
      O => p_1_out(0)
    );
\q0[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(1),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(1),
      I3 => INPUT_STREAM_V_keep_V_0_sel,
      I4 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(1),
      O => \q0[1]_i_1__0_n_4\
    );
\q0[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__2_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__1_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__0_n_4\,
      O => p_1_out(1)
    );
\q0[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(2),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(2),
      I3 => INPUT_STREAM_V_keep_V_0_sel,
      I4 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(2),
      O => \q0[2]_i_1_n_4\
    );
\q0[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__4_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__3_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__1_n_4\,
      O => p_1_out(2)
    );
\q0[3]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(3),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(3),
      I3 => INPUT_STREAM_V_keep_V_0_sel,
      I4 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(3),
      O => \q0[3]_i_2__2_n_4\
    );
\q0[3]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__6_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__5_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__2_n_4\,
      O => p_1_out(3)
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[0]_i_1__0_n_4\,
      Q => \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(0),
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[1]_i_1__0_n_4\,
      Q => \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(1),
      R => '0'
    );
\q0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[2]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(2),
      R => '0'
    );
\q0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[3]_i_2__2_n_4\,
      Q => \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(3),
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__0_n_4\,
      O => ram_reg_0_15_0_0_n_4,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__0\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__1\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1_n_4\,
      O => \ram_reg_0_15_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__2\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1_n_4\,
      O => \ram_reg_0_15_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__3\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1_n_4\,
      O => \ram_reg_0_15_0_0__3_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__4\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1_n_4\,
      O => \ram_reg_0_15_0_0__4_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__5\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1_n_4\,
      O => \ram_reg_0_15_0_0__5_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__6\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1_n_4\,
      O => \ram_reg_0_15_0_0__6_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
ram_reg_0_31_0_0: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0_i_1__0_n_4\,
      O => ram_reg_0_31_0_0_n_4,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__0_i_1_n_4\,
      O => \ram_reg_0_31_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => INPUT_STREAM_V_keep_V_0_sel,
      I2 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(1),
      O => \ram_reg_0_31_0_0__0_i_1_n_4\
    );
\ram_reg_0_31_0_0__1\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__1_i_1_n_4\,
      O => \ram_reg_0_31_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(2),
      I1 => INPUT_STREAM_V_keep_V_0_sel,
      I2 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(2),
      O => \ram_reg_0_31_0_0__1_i_1_n_4\
    );
\ram_reg_0_31_0_0__2\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__2_i_1_n_4\,
      O => \ram_reg_0_31_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(3),
      I1 => INPUT_STREAM_V_keep_V_0_sel,
      I2 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(3),
      O => \ram_reg_0_31_0_0__2_i_1_n_4\
    );
\ram_reg_0_31_0_0_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => INPUT_STREAM_V_keep_V_0_sel,
      I2 => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(0),
      O => \ram_reg_0_31_0_0_i_1__0_n_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg_ram is
  port (
    \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_state_reg[0]\ : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_sel_wr : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_payload_A : in STD_LOGIC_VECTOR ( 1 downto 0 );
    OUTPUT_STREAM_V_user_V_1_payload_B : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    INPUT_STREAM_V_user_V_0_payload_B : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INPUT_STREAM_V_user_V_0_sel : in STD_LOGIC;
    INPUT_STREAM_V_user_V_0_payload_A : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg_ram is
  signal p_1_out : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \q0[0]_i_1__2_n_4\ : STD_LOGIC;
  signal \q0[1]_i_1__2_n_4\ : STD_LOGIC;
  signal \q0_reg_n_4_[0]\ : STD_LOGIC;
  signal \q0_reg_n_4_[1]\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__2_n_4\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_n_4 : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_i_1__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0_i_1__2_n_4\ : STD_LOGIC;
  signal ram_reg_0_31_0_0_n_4 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__0\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__1\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__2\ : label is "RAM16X1S";
begin
\OUTPUT_STREAM_V_user_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => \q0_reg_n_4_[0]\,
      I1 => \OUTPUT_STREAM_V_user_V_1_state_reg[0]\,
      I2 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_user_V_1_sel_wr,
      I4 => OUTPUT_STREAM_V_user_V_1_payload_A(0),
      O => \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\
    );
\OUTPUT_STREAM_V_user_V_1_payload_A[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => \q0_reg_n_4_[1]\,
      I1 => \OUTPUT_STREAM_V_user_V_1_state_reg[0]\,
      I2 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_user_V_1_sel_wr,
      I4 => OUTPUT_STREAM_V_user_V_1_payload_A(1),
      O => \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\
    );
\OUTPUT_STREAM_V_user_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => \q0_reg_n_4_[0]\,
      I1 => \OUTPUT_STREAM_V_user_V_1_state_reg[0]\,
      I2 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_user_V_1_sel_wr,
      I4 => OUTPUT_STREAM_V_user_V_1_payload_B(0),
      O => \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\
    );
\OUTPUT_STREAM_V_user_V_1_payload_B[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => \q0_reg_n_4_[1]\,
      I1 => \OUTPUT_STREAM_V_user_V_1_state_reg[0]\,
      I2 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_user_V_1_sel_wr,
      I4 => OUTPUT_STREAM_V_user_V_1_payload_B(1),
      O => \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\
    );
\q0[0]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(0),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => INPUT_STREAM_V_user_V_0_payload_B(0),
      I3 => INPUT_STREAM_V_user_V_0_sel,
      I4 => INPUT_STREAM_V_user_V_0_payload_A(0),
      O => \q0[0]_i_1__2_n_4\
    );
\q0[0]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__0_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => ram_reg_0_15_0_0_n_4,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => ram_reg_0_31_0_0_n_4,
      O => p_1_out(0)
    );
\q0[1]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(1),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => INPUT_STREAM_V_user_V_0_payload_B(1),
      I3 => INPUT_STREAM_V_user_V_0_sel,
      I4 => INPUT_STREAM_V_user_V_0_payload_A(1),
      O => \q0[1]_i_1__2_n_4\
    );
\q0[1]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__2_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__1_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__0_n_4\,
      O => p_1_out(1)
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[0]_i_1__2_n_4\,
      Q => \q0_reg_n_4_[0]\,
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[1]_i_1__2_n_4\,
      Q => \q0_reg_n_4_[1]\,
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__2_n_4\,
      O => ram_reg_0_15_0_0_n_4,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__0\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__1\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__2\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
ram_reg_0_31_0_0: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0_i_1__2_n_4\,
      O => ram_reg_0_31_0_0_n_4,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__0_i_1__1_n_4\,
      O => \ram_reg_0_31_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => INPUT_STREAM_V_user_V_0_payload_B(1),
      I1 => INPUT_STREAM_V_user_V_0_sel,
      I2 => INPUT_STREAM_V_user_V_0_payload_A(1),
      O => \ram_reg_0_31_0_0__0_i_1__1_n_4\
    );
\ram_reg_0_31_0_0_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => INPUT_STREAM_V_user_V_0_payload_B(0),
      I1 => INPUT_STREAM_V_user_V_0_sel,
      I2 => INPUT_STREAM_V_user_V_0_payload_A(0),
      O => \ram_reg_0_31_0_0_i_1__2_n_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi_ram is
  port (
    I8 : out STD_LOGIC_VECTOR ( 0 to 0 );
    \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\ : out STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    INPUT_STREAM_V_last_V_0_payload_B : in STD_LOGIC;
    INPUT_STREAM_V_last_V_0_sel : in STD_LOGIC;
    INPUT_STREAM_V_last_V_0_payload_A : in STD_LOGIC;
    \OUTPUT_STREAM_V_last_V_1_state_reg[0]\ : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_sel_wr : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_payload_A : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_payload_B : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    address0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi_ram is
  signal \^i8\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal p_1_out : STD_LOGIC;
  signal \q0[0]_i_1__3_n_4\ : STD_LOGIC;
  signal \q0_reg_n_4_[0]\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__0_n_4\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_n_4 : STD_LOGIC;
  signal ram_reg_0_31_0_0_n_4 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__0\ : label is "RAM16X1S";
begin
  I8(0) <= \^i8\(0);
\OUTPUT_STREAM_V_last_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => \q0_reg_n_4_[0]\,
      I1 => \OUTPUT_STREAM_V_last_V_1_state_reg[0]\,
      I2 => OUTPUT_STREAM_V_last_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_last_V_1_sel_wr,
      I4 => OUTPUT_STREAM_V_last_V_1_payload_A,
      O => \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\
    );
\OUTPUT_STREAM_V_last_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => \q0_reg_n_4_[0]\,
      I1 => \OUTPUT_STREAM_V_last_V_1_state_reg[0]\,
      I2 => OUTPUT_STREAM_V_last_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_last_V_1_sel_wr,
      I4 => OUTPUT_STREAM_V_last_V_1_payload_B,
      O => \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\
    );
\q0[0]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out,
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => INPUT_STREAM_V_last_V_0_payload_B,
      I3 => INPUT_STREAM_V_last_V_0_sel,
      I4 => INPUT_STREAM_V_last_V_0_payload_A,
      O => \q0[0]_i_1__3_n_4\
    );
\q0[0]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__0_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => ram_reg_0_15_0_0_n_4,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => ram_reg_0_31_0_0_n_4,
      O => p_1_out
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[0]_i_1__3_n_4\,
      Q => \q0_reg_n_4_[0]\,
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \^i8\(0),
      O => ram_reg_0_15_0_0_n_4,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__0\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => \^i8\(0),
      O => \ram_reg_0_15_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
ram_reg_0_31_0_0: unisim.vcomponents.RAM32X1S
     port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => \i_reg_345_reg[4]\,
      D => \^i8\(0),
      O => ram_reg_0_31_0_0_n_4,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
ram_reg_0_31_0_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => INPUT_STREAM_V_last_V_0_payload_B,
      I1 => INPUT_STREAM_V_last_V_0_sel,
      I2 => INPUT_STREAM_V_last_V_0_payload_A,
      O => \^i8\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j_ram is
  port (
    \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    INPUT_STREAM_V_id_V_0_sel : in STD_LOGIC;
    \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j_ram is
  signal p_1_out : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \q0[0]_i_1__4_n_4\ : STD_LOGIC;
  signal \q0[1]_i_1__3_n_4\ : STD_LOGIC;
  signal \q0[2]_i_1__1_n_4\ : STD_LOGIC;
  signal \q0[3]_i_1__0_n_4\ : STD_LOGIC;
  signal \q0[4]_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__4_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__5_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__6_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__7_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__8_n_4\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_n_4 : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_i_1__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_i_1__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_i_1__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__3_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0_i_1__3_n_4\ : STD_LOGIC;
  signal ram_reg_0_31_0_0_n_4 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__0\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__1\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__2\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__3\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__4\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__5\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__6\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__7\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__8\ : label is "RAM16X1S";
begin
\q0[0]_i_1__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(0),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(0),
      I3 => INPUT_STREAM_V_id_V_0_sel,
      I4 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(0),
      O => \q0[0]_i_1__4_n_4\
    );
\q0[0]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__0_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => ram_reg_0_15_0_0_n_4,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => ram_reg_0_31_0_0_n_4,
      O => p_1_out(0)
    );
\q0[1]_i_1__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(1),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(1),
      I3 => INPUT_STREAM_V_id_V_0_sel,
      I4 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(1),
      O => \q0[1]_i_1__3_n_4\
    );
\q0[1]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__2_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__1_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__0_n_4\,
      O => p_1_out(1)
    );
\q0[2]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(2),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(2),
      I3 => INPUT_STREAM_V_id_V_0_sel,
      I4 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(2),
      O => \q0[2]_i_1__1_n_4\
    );
\q0[2]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__4_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__3_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__1_n_4\,
      O => p_1_out(2)
    );
\q0[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(3),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(3),
      I3 => INPUT_STREAM_V_id_V_0_sel,
      I4 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(3),
      O => \q0[3]_i_1__0_n_4\
    );
\q0[3]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__6_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__5_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__2_n_4\,
      O => p_1_out(3)
    );
\q0[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => p_1_out(4),
      I1 => \ap_CS_fsm_reg[3]\,
      I2 => Q(4),
      I3 => INPUT_STREAM_V_id_V_0_sel,
      I4 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(4),
      O => \q0[4]_i_1_n_4\
    );
\q0[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__8_n_4\,
      I1 => \i_reg_345_reg[4]\,
      I2 => \ram_reg_0_15_0_0__7_n_4\,
      I3 => \i_reg_345_reg[5]_1\,
      I4 => \ram_reg_0_31_0_0__3_n_4\,
      O => p_1_out(4)
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[0]_i_1__4_n_4\,
      Q => \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(0),
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[1]_i_1__3_n_4\,
      Q => \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(1),
      R => '0'
    );
\q0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[2]_i_1__1_n_4\,
      Q => \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(2),
      R => '0'
    );
\q0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[3]_i_1__0_n_4\,
      Q => \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(3),
      R => '0'
    );
\q0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => E(0),
      D => \q0[4]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(4),
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__3_n_4\,
      O => ram_reg_0_15_0_0_n_4,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__0\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__3_n_4\,
      O => \ram_reg_0_15_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__1\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__2\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__3\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__3_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__4\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__4_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__5\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__5_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__6\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1__1_n_4\,
      O => \ram_reg_0_15_0_0__6_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
\ram_reg_0_15_0_0__7\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__3_i_1_n_4\,
      O => \ram_reg_0_15_0_0__7_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]\
    );
\ram_reg_0_15_0_0__8\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => '0',
      D => \ram_reg_0_31_0_0__3_i_1_n_4\,
      O => \ram_reg_0_15_0_0__8_n_4\,
      WCLK => ap_clk,
      WE => \i_reg_345_reg[5]_0\
    );
ram_reg_0_31_0_0: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0_i_1__3_n_4\,
      O => ram_reg_0_31_0_0_n_4,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__0_i_1__2_n_4\,
      O => \ram_reg_0_31_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__0_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => INPUT_STREAM_V_id_V_0_sel,
      I2 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(1),
      O => \ram_reg_0_31_0_0__0_i_1__2_n_4\
    );
\ram_reg_0_31_0_0__1\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__1_i_1__1_n_4\,
      O => \ram_reg_0_31_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__1_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(2),
      I1 => INPUT_STREAM_V_id_V_0_sel,
      I2 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(2),
      O => \ram_reg_0_31_0_0__1_i_1__1_n_4\
    );
\ram_reg_0_31_0_0__2\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__2_i_1__1_n_4\,
      O => \ram_reg_0_31_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__2_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(3),
      I1 => INPUT_STREAM_V_id_V_0_sel,
      I2 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(3),
      O => \ram_reg_0_31_0_0__2_i_1__1_n_4\
    );
\ram_reg_0_31_0_0__3\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \i_reg_345_reg[0]\,
      A1 => \i_reg_345_reg[1]\,
      A2 => \i_reg_345_reg[2]\,
      A3 => \i_reg_345_reg[3]\,
      A4 => \i_reg_345_reg[4]\,
      D => \ram_reg_0_31_0_0__3_i_1_n_4\,
      O => \ram_reg_0_31_0_0__3_n_4\,
      WCLK => ap_clk,
      WE => \INPUT_STREAM_V_data_V_0_state_reg[0]\
    );
\ram_reg_0_31_0_0__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(4),
      I1 => INPUT_STREAM_V_id_V_0_sel,
      I2 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(4),
      O => \ram_reg_0_31_0_0__3_i_1_n_4\
    );
\ram_reg_0_31_0_0_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => INPUT_STREAM_V_id_V_0_sel,
      I2 => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(0),
      O => \ram_reg_0_31_0_0_i_1__3_n_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi_ram is
  port (
    \q0_reg[5]_0\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    ap_block_pp1_stage0_subdone1_in : out STD_LOGIC;
    address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q0_reg[5]_1\ : out STD_LOGIC;
    \q0_reg[5]_2\ : out STD_LOGIC;
    \q0_reg[4]_0\ : out STD_LOGIC;
    \q0_reg[5]_3\ : out STD_LOGIC;
    \q0_reg[0]_0\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    INPUT_STREAM_V_dest_V_0_sel : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \ap_CS_fsm_reg[5]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \i_reg_345_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    ap_enable_reg_pp1_iter0 : in STD_LOGIC;
    \i_1_reg_356_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \exitcond_reg_509_reg[0]\ : in STD_LOGIC;
    ap_enable_reg_pp1_iter1_reg : in STD_LOGIC;
    ap_reg_pp1_iter1_exitcond_reg_509 : in STD_LOGIC;
    ap_enable_reg_pp1_iter2_reg : in STD_LOGIC;
    OUTPUT_STREAM_V_data_V_1_ack_in : in STD_LOGIC;
    ap_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi_ram is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^address0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^ap_block_pp1_stage0_subdone1_in\ : STD_LOGIC;
  signal p_1_out : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \q0[0]_i_1__5_n_4\ : STD_LOGIC;
  signal \q0[1]_i_1__4_n_4\ : STD_LOGIC;
  signal \q0[2]_i_1__2_n_4\ : STD_LOGIC;
  signal \q0[3]_i_1__1_n_4\ : STD_LOGIC;
  signal \q0[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \q0[5]_i_1_n_4\ : STD_LOGIC;
  signal \^q0_reg[0]_0\ : STD_LOGIC;
  signal \^q0_reg[4]_0\ : STD_LOGIC;
  signal \^q0_reg[5]_0\ : STD_LOGIC;
  signal \^q0_reg[5]_1\ : STD_LOGIC;
  signal \^q0_reg[5]_2\ : STD_LOGIC;
  signal \^q0_reg[5]_3\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__10_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__4_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__5_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__6_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__7_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__8_n_4\ : STD_LOGIC;
  signal \ram_reg_0_15_0_0__9_n_4\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_n_4 : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_i_1__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_i_1__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_i_1__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__2_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__3_i_1__0_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__3_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__4_i_1_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0__4_n_4\ : STD_LOGIC;
  signal \ram_reg_0_31_0_0_i_1__4_n_4\ : STD_LOGIC;
  signal ram_reg_0_31_0_0_n_4 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__0\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__1\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__10\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__2\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__3\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__4\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__5\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__6\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__7\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__8\ : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of \ram_reg_0_15_0_0__9\ : label is "RAM16X1S";
begin
  E(0) <= \^e\(0);
  address0(3 downto 0) <= \^address0\(3 downto 0);
  ap_block_pp1_stage0_subdone1_in <= \^ap_block_pp1_stage0_subdone1_in\;
  \q0_reg[0]_0\ <= \^q0_reg[0]_0\;
  \q0_reg[4]_0\ <= \^q0_reg[4]_0\;
  \q0_reg[5]_0\ <= \^q0_reg[5]_0\;
  \q0_reg[5]_1\ <= \^q0_reg[5]_1\;
  \q0_reg[5]_2\ <= \^q0_reg[5]_2\;
  \q0_reg[5]_3\ <= \^q0_reg[5]_3\;
\q0[0]_i_1__5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E4E4"
    )
        port map (
      I0 => INPUT_STREAM_V_dest_V_0_sel,
      I1 => Q(0),
      I2 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(0),
      I3 => p_1_out(0),
      I4 => \^q0_reg[5]_0\,
      O => \q0[0]_i_1__5_n_4\
    );
\q0[0]_i_2__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__0_n_4\,
      I1 => \^q0_reg[5]_1\,
      I2 => ram_reg_0_15_0_0_n_4,
      I3 => \^q0_reg[5]_2\,
      I4 => ram_reg_0_31_0_0_n_4,
      O => p_1_out(0)
    );
\q0[1]_i_1__4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E4E4"
    )
        port map (
      I0 => INPUT_STREAM_V_dest_V_0_sel,
      I1 => Q(1),
      I2 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(1),
      I3 => p_1_out(1),
      I4 => \^q0_reg[5]_0\,
      O => \q0[1]_i_1__4_n_4\
    );
\q0[1]_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__2_n_4\,
      I1 => \^q0_reg[5]_1\,
      I2 => \ram_reg_0_15_0_0__1_n_4\,
      I3 => \^q0_reg[5]_2\,
      I4 => \ram_reg_0_31_0_0__0_n_4\,
      O => p_1_out(1)
    );
\q0[2]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E4E4"
    )
        port map (
      I0 => INPUT_STREAM_V_dest_V_0_sel,
      I1 => Q(2),
      I2 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(2),
      I3 => p_1_out(2),
      I4 => \^q0_reg[5]_0\,
      O => \q0[2]_i_1__2_n_4\
    );
\q0[2]_i_2__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__4_n_4\,
      I1 => \^q0_reg[5]_1\,
      I2 => \ram_reg_0_15_0_0__3_n_4\,
      I3 => \^q0_reg[5]_2\,
      I4 => \ram_reg_0_31_0_0__1_n_4\,
      O => p_1_out(2)
    );
\q0[3]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E4E4"
    )
        port map (
      I0 => INPUT_STREAM_V_dest_V_0_sel,
      I1 => Q(3),
      I2 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(3),
      I3 => p_1_out(3),
      I4 => \^q0_reg[5]_0\,
      O => \q0[3]_i_1__1_n_4\
    );
\q0[3]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80FF80FF808080"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter0,
      I1 => \ap_CS_fsm_reg[5]\(1),
      I2 => \^ap_block_pp1_stage0_subdone1_in\,
      I3 => \ap_CS_fsm_reg[5]\(0),
      I4 => \i_reg_345_reg[5]\(5),
      I5 => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      O => \^e\(0)
    );
\q0[3]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__6_n_4\,
      I1 => \^q0_reg[5]_1\,
      I2 => \ram_reg_0_15_0_0__5_n_4\,
      I3 => \^q0_reg[5]_2\,
      I4 => \ram_reg_0_31_0_0__2_n_4\,
      O => p_1_out(3)
    );
\q0[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFB0BB"
    )
        port map (
      I0 => \exitcond_reg_509_reg[0]\,
      I1 => ap_enable_reg_pp1_iter1_reg,
      I2 => ap_reg_pp1_iter1_exitcond_reg_509,
      I3 => ap_enable_reg_pp1_iter2_reg,
      I4 => OUTPUT_STREAM_V_data_V_1_ack_in,
      O => \^ap_block_pp1_stage0_subdone1_in\
    );
\q0[3]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EA2A"
    )
        port map (
      I0 => \i_reg_345_reg[5]\(5),
      I1 => ap_enable_reg_pp1_iter0,
      I2 => \ap_CS_fsm_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(5),
      O => \^q0_reg[5]_2\
    );
\q0[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E4E4"
    )
        port map (
      I0 => INPUT_STREAM_V_dest_V_0_sel,
      I1 => Q(4),
      I2 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(4),
      I3 => p_1_out(4),
      I4 => \^q0_reg[5]_0\,
      O => \q0[4]_i_1__0_n_4\
    );
\q0[4]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__8_n_4\,
      I1 => \^q0_reg[5]_1\,
      I2 => \ram_reg_0_15_0_0__7_n_4\,
      I3 => \^q0_reg[5]_2\,
      I4 => \ram_reg_0_31_0_0__3_n_4\,
      O => p_1_out(4)
    );
\q0[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E4E4"
    )
        port map (
      I0 => INPUT_STREAM_V_dest_V_0_sel,
      I1 => Q(5),
      I2 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(5),
      I3 => p_1_out(5),
      I4 => \^q0_reg[5]_0\,
      O => \q0[5]_i_1_n_4\
    );
\q0[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => \ram_reg_0_15_0_0__10_n_4\,
      I1 => \^q0_reg[5]_1\,
      I2 => \ram_reg_0_15_0_0__9_n_4\,
      I3 => \^q0_reg[5]_2\,
      I4 => \ram_reg_0_31_0_0__4_n_4\,
      O => p_1_out(5)
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \q0[0]_i_1__5_n_4\,
      Q => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(0),
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \q0[1]_i_1__4_n_4\,
      Q => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(1),
      R => '0'
    );
\q0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \q0[2]_i_1__2_n_4\,
      Q => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(2),
      R => '0'
    );
\q0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \q0[3]_i_1__1_n_4\,
      Q => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(3),
      R => '0'
    );
\q0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \q0[4]_i_1__0_n_4\,
      Q => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(4),
      R => '0'
    );
\q0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^e\(0),
      D => \q0[5]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(5),
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__4_n_4\,
      O => ram_reg_0_15_0_0_n_4,
      WCLK => ap_clk,
      WE => \^q0_reg[4]_0\
    );
\ram_reg_0_15_0_0__0\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0_i_1__4_n_4\,
      O => \ram_reg_0_15_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000A8880888"
    )
        port map (
      I0 => \^q0_reg[5]_1\,
      I1 => \i_reg_345_reg[5]\(5),
      I2 => ap_enable_reg_pp1_iter0,
      I3 => \ap_CS_fsm_reg[5]\(1),
      I4 => \i_1_reg_356_reg[5]\(5),
      I5 => \^q0_reg[5]_0\,
      O => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__1\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__3_n_4\,
      O => \ram_reg_0_15_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[4]_0\
    );
\ram_reg_0_15_0_0__10\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__4_i_1_n_4\,
      O => \ram_reg_0_15_0_0__10_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__2\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__0_i_1__3_n_4\,
      O => \ram_reg_0_15_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__3\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__3_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[4]_0\
    );
\ram_reg_0_15_0_0__4\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__1_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__4_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__5\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__5_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[4]_0\
    );
\ram_reg_0_15_0_0__6\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__2_i_1__2_n_4\,
      O => \ram_reg_0_15_0_0__6_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__7\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__3_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__7_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[4]_0\
    );
\ram_reg_0_15_0_0__8\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__3_i_1__0_n_4\,
      O => \ram_reg_0_15_0_0__8_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[5]_3\
    );
\ram_reg_0_15_0_0__9\: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => '0',
      D => \ram_reg_0_31_0_0__4_i_1_n_4\,
      O => \ram_reg_0_15_0_0__9_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[4]_0\
    );
ram_reg_0_15_0_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000054440444"
    )
        port map (
      I0 => \^q0_reg[5]_1\,
      I1 => \i_reg_345_reg[5]\(5),
      I2 => ap_enable_reg_pp1_iter0,
      I3 => \ap_CS_fsm_reg[5]\(1),
      I4 => \i_1_reg_356_reg[5]\(5),
      I5 => \^q0_reg[5]_0\,
      O => \^q0_reg[4]_0\
    );
ram_reg_0_15_0_0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \ap_CS_fsm_reg[5]\(0),
      I1 => \i_reg_345_reg[5]\(5),
      I2 => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      O => \^q0_reg[5]_0\
    );
ram_reg_0_31_0_0: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => \^q0_reg[5]_1\,
      D => \ram_reg_0_31_0_0_i_1__4_n_4\,
      O => ram_reg_0_31_0_0_n_4,
      WCLK => ap_clk,
      WE => \^q0_reg[0]_0\
    );
\ram_reg_0_31_0_0__0\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => \^q0_reg[5]_1\,
      D => \ram_reg_0_31_0_0__0_i_1__3_n_4\,
      O => \ram_reg_0_31_0_0__0_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[0]_0\
    );
\ram_reg_0_31_0_0__0_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(1),
      I1 => INPUT_STREAM_V_dest_V_0_sel,
      I2 => Q(1),
      O => \ram_reg_0_31_0_0__0_i_1__3_n_4\
    );
\ram_reg_0_31_0_0__1\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => \^q0_reg[5]_1\,
      D => \ram_reg_0_31_0_0__1_i_1__2_n_4\,
      O => \ram_reg_0_31_0_0__1_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[0]_0\
    );
\ram_reg_0_31_0_0__1_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(2),
      I1 => INPUT_STREAM_V_dest_V_0_sel,
      I2 => Q(2),
      O => \ram_reg_0_31_0_0__1_i_1__2_n_4\
    );
\ram_reg_0_31_0_0__2\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => \^q0_reg[5]_1\,
      D => \ram_reg_0_31_0_0__2_i_1__2_n_4\,
      O => \ram_reg_0_31_0_0__2_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[0]_0\
    );
\ram_reg_0_31_0_0__2_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(3),
      I1 => INPUT_STREAM_V_dest_V_0_sel,
      I2 => Q(3),
      O => \ram_reg_0_31_0_0__2_i_1__2_n_4\
    );
\ram_reg_0_31_0_0__3\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => \^q0_reg[5]_1\,
      D => \ram_reg_0_31_0_0__3_i_1__0_n_4\,
      O => \ram_reg_0_31_0_0__3_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[0]_0\
    );
\ram_reg_0_31_0_0__3_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(4),
      I1 => INPUT_STREAM_V_dest_V_0_sel,
      I2 => Q(4),
      O => \ram_reg_0_31_0_0__3_i_1__0_n_4\
    );
\ram_reg_0_31_0_0__4\: unisim.vcomponents.RAM32X1S
     port map (
      A0 => \^address0\(0),
      A1 => \^address0\(1),
      A2 => \^address0\(2),
      A3 => \^address0\(3),
      A4 => \^q0_reg[5]_1\,
      D => \ram_reg_0_31_0_0__4_i_1_n_4\,
      O => \ram_reg_0_31_0_0__4_n_4\,
      WCLK => ap_clk,
      WE => \^q0_reg[0]_0\
    );
\ram_reg_0_31_0_0__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(5),
      I1 => INPUT_STREAM_V_dest_V_0_sel,
      I2 => Q(5),
      O => \ram_reg_0_31_0_0__4_i_1_n_4\
    );
\ram_reg_0_31_0_0_i_1__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(0),
      I1 => INPUT_STREAM_V_dest_V_0_sel,
      I2 => Q(0),
      O => \ram_reg_0_31_0_0_i_1__4_n_4\
    );
ram_reg_0_31_0_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000008888888"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      I1 => \ap_CS_fsm_reg[5]\(0),
      I2 => \i_1_reg_356_reg[5]\(5),
      I3 => \ap_CS_fsm_reg[5]\(1),
      I4 => ap_enable_reg_pp1_iter0,
      I5 => \i_reg_345_reg[5]\(5),
      O => \^q0_reg[0]_0\
    );
ram_reg_0_31_0_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EA2A"
    )
        port map (
      I0 => \i_reg_345_reg[5]\(0),
      I1 => ap_enable_reg_pp1_iter0,
      I2 => \ap_CS_fsm_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(0),
      O => \^address0\(0)
    );
ram_reg_0_31_0_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EA2A"
    )
        port map (
      I0 => \i_reg_345_reg[5]\(1),
      I1 => ap_enable_reg_pp1_iter0,
      I2 => \ap_CS_fsm_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(1),
      O => \^address0\(1)
    );
ram_reg_0_31_0_0_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EA2A"
    )
        port map (
      I0 => \i_reg_345_reg[5]\(2),
      I1 => ap_enable_reg_pp1_iter0,
      I2 => \ap_CS_fsm_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(2),
      O => \^address0\(2)
    );
ram_reg_0_31_0_0_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EA2A"
    )
        port map (
      I0 => \i_reg_345_reg[5]\(3),
      I1 => ap_enable_reg_pp1_iter0,
      I2 => \ap_CS_fsm_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(3),
      O => \^address0\(3)
    );
ram_reg_0_31_0_0_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EA2A"
    )
        port map (
      I0 => \i_reg_345_reg[5]\(4),
      I1 => ap_enable_reg_pp1_iter0,
      I2 => \ap_CS_fsm_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(4),
      O => \^q0_reg[5]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram is
  port (
    I1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ap_clk : in STD_LOGIC;
    DIADI : in STD_LOGIC_VECTOR ( 4 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \tmp_reg_484_reg[6]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \i_1_reg_356_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ap_enable_reg_pp1_iter0 : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    INPUT_STREAM_V_data_V_0_sel : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram is
  signal \^i1\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal address0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal ce016_out : STD_LOGIC;
  signal ram_reg_i_10_n_4 : STD_LOGIC;
  signal ram_reg_i_11_n_4 : STD_LOGIC;
  signal ram_reg_i_12_n_4 : STD_LOGIC;
  signal ram_reg_i_13_n_4 : STD_LOGIC;
  signal ram_reg_i_14_n_4 : STD_LOGIC;
  signal ram_reg_i_15_n_4 : STD_LOGIC;
  signal ram_reg_i_16_n_4 : STD_LOGIC;
  signal ram_reg_i_17_n_4 : STD_LOGIC;
  signal ram_reg_i_18_n_4 : STD_LOGIC;
  signal ram_reg_i_19_n_4 : STD_LOGIC;
  signal ram_reg_i_25_n_4 : STD_LOGIC;
  signal ram_reg_i_26_n_4 : STD_LOGIC;
  signal ram_reg_i_27_n_4 : STD_LOGIC;
  signal ram_reg_i_28_n_4 : STD_LOGIC;
  signal ram_reg_i_29_n_4 : STD_LOGIC;
  signal ram_reg_i_30_n_4 : STD_LOGIC;
  signal ram_reg_i_31_n_4 : STD_LOGIC;
  signal ram_reg_i_32_n_4 : STD_LOGIC;
  signal ram_reg_i_33_n_4 : STD_LOGIC;
  signal ram_reg_i_34_n_4 : STD_LOGIC;
  signal ram_reg_i_35_n_4 : STD_LOGIC;
  signal ram_reg_i_36_n_4 : STD_LOGIC;
  signal ram_reg_i_37_n_4 : STD_LOGIC;
  signal ram_reg_i_38_n_4 : STD_LOGIC;
  signal ram_reg_i_39_n_4 : STD_LOGIC;
  signal ram_reg_i_40_n_4 : STD_LOGIC;
  signal ram_reg_i_42_n_4 : STD_LOGIC;
  signal ram_reg_i_9_n_4 : STD_LOGIC;
  signal we014_out : STD_LOGIC;
  signal \NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_ram_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 14 );
  signal NLW_ram_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg : label is "p2_d16";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg : label is 2112;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg : label is 65;
  attribute bram_ext_slice_begin : integer;
  attribute bram_ext_slice_begin of ram_reg : label is 18;
  attribute bram_ext_slice_end : integer;
  attribute bram_ext_slice_end of ram_reg : label is 31;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg : label is 17;
begin
  I1(31 downto 0) <= \^i1\(31 downto 0);
\OUTPUT_STREAM_V_data_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^i1\(0),
      O => D(0)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^i1\(0),
      O => \OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2_n_4\
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_4\,
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(11 downto 8),
      S(3 downto 0) => \^i1\(11 downto 8)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]_i_1_n_4\,
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(15 downto 12),
      S(3 downto 0) => \^i1\(15 downto 12)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]_i_1_n_4\,
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(19 downto 16),
      S(3 downto 0) => \^i1\(19 downto 16)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]_i_1_n_4\,
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(23 downto 20),
      S(3 downto 0) => \^i1\(23 downto 20)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]_i_1_n_4\,
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(27 downto 24),
      S(3 downto 0) => \^i1\(27 downto 24)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]_i_1_n_4\,
      CO(3) => \NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]_i_2_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(31 downto 28),
      S(3 downto 0) => \^i1\(31 downto 28)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \^i1\(0),
      O(3 downto 1) => D(3 downto 1),
      O(0) => \NLW_OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_O_UNCONNECTED\(0),
      S(3 downto 1) => \^i1\(3 downto 1),
      S(0) => \OUTPUT_STREAM_V_data_V_1_payload_A[3]_i_2_n_4\
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]_i_1_n_4\,
      CO(3) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_4\,
      CO(2) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_5\,
      CO(1) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_6\,
      CO(0) => \OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]_i_1_n_7\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => D(7 downto 4),
      S(3 downto 0) => \^i1\(7 downto 4)
    );
ram_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 11) => B"011",
      ADDRARDADDR(10 downto 4) => address0(6 downto 0),
      ADDRARDADDR(3 downto 0) => B"1111",
      ADDRBWRADDR(13 downto 11) => B"111",
      ADDRBWRADDR(10 downto 4) => address0(6 downto 0),
      ADDRBWRADDR(3 downto 0) => B"1111",
      CLKARDCLK => ap_clk,
      CLKBWRCLK => ap_clk,
      DIADI(15) => ram_reg_i_9_n_4,
      DIADI(14) => ram_reg_i_10_n_4,
      DIADI(13) => ram_reg_i_11_n_4,
      DIADI(12) => ram_reg_i_12_n_4,
      DIADI(11) => ram_reg_i_13_n_4,
      DIADI(10) => ram_reg_i_14_n_4,
      DIADI(9) => ram_reg_i_15_n_4,
      DIADI(8) => ram_reg_i_16_n_4,
      DIADI(7) => ram_reg_i_17_n_4,
      DIADI(6) => ram_reg_i_18_n_4,
      DIADI(5) => ram_reg_i_19_n_4,
      DIADI(4 downto 0) => DIADI(4 downto 0),
      DIBDI(15 downto 14) => B"11",
      DIBDI(13) => ram_reg_i_25_n_4,
      DIBDI(12) => ram_reg_i_26_n_4,
      DIBDI(11) => ram_reg_i_27_n_4,
      DIBDI(10) => ram_reg_i_28_n_4,
      DIBDI(9) => ram_reg_i_29_n_4,
      DIBDI(8) => ram_reg_i_30_n_4,
      DIBDI(7) => ram_reg_i_31_n_4,
      DIBDI(6) => ram_reg_i_32_n_4,
      DIBDI(5) => ram_reg_i_33_n_4,
      DIBDI(4) => ram_reg_i_34_n_4,
      DIBDI(3) => ram_reg_i_35_n_4,
      DIBDI(2) => ram_reg_i_36_n_4,
      DIBDI(1) => ram_reg_i_37_n_4,
      DIBDI(0) => ram_reg_i_38_n_4,
      DIPADIP(1) => ram_reg_i_39_n_4,
      DIPADIP(0) => ram_reg_i_40_n_4,
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 0) => \^i1\(15 downto 0),
      DOBDO(15 downto 14) => NLW_ram_reg_DOBDO_UNCONNECTED(15 downto 14),
      DOBDO(13 downto 0) => \^i1\(31 downto 18),
      DOPADOP(1 downto 0) => \^i1\(17 downto 16),
      DOPBDOP(1 downto 0) => NLW_ram_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => ce016_out,
      ENBWREN => ce016_out,
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => we014_out,
      WEA(0) => we014_out,
      WEBWE(3 downto 2) => B"00",
      WEBWE(1) => we014_out,
      WEBWE(0) => we014_out
    );
ram_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => Q(3),
      I1 => Q(0),
      I2 => E(0),
      O => ce016_out
    );
ram_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(9),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(9),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_10_n_4
    );
ram_reg_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(8),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(8),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_11_n_4
    );
ram_reg_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(7),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(7),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_12_n_4
    );
ram_reg_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(6),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(6),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_13_n_4
    );
ram_reg_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(5),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(5),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_14_n_4
    );
ram_reg_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(4),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(4),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_15_n_4
    );
ram_reg_i_16: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(3),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(3),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_16_n_4
    );
ram_reg_i_17: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(2),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(2),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_17_n_4
    );
ram_reg_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(1),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(1),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_18_n_4
    );
ram_reg_i_19: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(0),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(0),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_19_n_4
    );
ram_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00150000"
    )
        port map (
      I0 => Q(1),
      I1 => Q(2),
      I2 => ap_enable_reg_pp1_iter0,
      I3 => Q(3),
      I4 => \tmp_reg_484_reg[6]\(6),
      O => address0(6)
    );
ram_reg_i_25: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(26),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(26),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_25_n_4
    );
ram_reg_i_26: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(25),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(25),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_26_n_4
    );
ram_reg_i_27: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(24),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(24),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_27_n_4
    );
ram_reg_i_28: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(23),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(23),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_28_n_4
    );
ram_reg_i_29: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(22),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(22),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_29_n_4
    );
ram_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E4E4FF00"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_reg_484_reg[6]\(5),
      I2 => \i_reg_345_reg[5]\(5),
      I3 => \i_1_reg_356_reg[5]\(5),
      I4 => ram_reg_i_42_n_4,
      I5 => Q(3),
      O => address0(5)
    );
ram_reg_i_30: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(21),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(21),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_30_n_4
    );
ram_reg_i_31: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(20),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(20),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_31_n_4
    );
ram_reg_i_32: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(19),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(19),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_32_n_4
    );
ram_reg_i_33: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(18),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(18),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_33_n_4
    );
ram_reg_i_34: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(17),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(17),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_34_n_4
    );
ram_reg_i_35: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(16),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(16),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_35_n_4
    );
ram_reg_i_36: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(15),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(15),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_36_n_4
    );
ram_reg_i_37: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(14),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(14),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_37_n_4
    );
ram_reg_i_38: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(13),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(13),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_38_n_4
    );
ram_reg_i_39: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(12),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(12),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_39_n_4
    );
ram_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E4E4FF00"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_reg_484_reg[6]\(4),
      I2 => \i_reg_345_reg[5]\(4),
      I3 => \i_1_reg_356_reg[5]\(4),
      I4 => ram_reg_i_42_n_4,
      I5 => Q(3),
      O => address0(4)
    );
ram_reg_i_40: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(11),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(11),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_40_n_4
    );
ram_reg_i_41: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AEAA"
    )
        port map (
      I0 => Q(0),
      I1 => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      I2 => \i_reg_345_reg[5]\(5),
      I3 => Q(1),
      O => we014_out
    );
ram_reg_i_42: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter0,
      I1 => Q(2),
      O => ram_reg_i_42_n_4
    );
ram_reg_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E4E4FF00"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_reg_484_reg[6]\(3),
      I2 => \i_reg_345_reg[5]\(3),
      I3 => \i_1_reg_356_reg[5]\(3),
      I4 => ram_reg_i_42_n_4,
      I5 => Q(3),
      O => address0(3)
    );
ram_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E4E4FF00"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_reg_484_reg[6]\(2),
      I2 => \i_reg_345_reg[5]\(2),
      I3 => \i_1_reg_356_reg[5]\(2),
      I4 => ram_reg_i_42_n_4,
      I5 => Q(3),
      O => address0(2)
    );
ram_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E4E4FF00"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_reg_484_reg[6]\(1),
      I2 => \i_reg_345_reg[5]\(1),
      I3 => \i_1_reg_356_reg[5]\(1),
      I4 => ram_reg_i_42_n_4,
      I5 => Q(3),
      O => address0(1)
    );
ram_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E4E4FF00"
    )
        port map (
      I0 => Q(1),
      I1 => \tmp_reg_484_reg[6]\(0),
      I2 => \i_reg_345_reg[5]\(0),
      I3 => \i_1_reg_356_reg[5]\(0),
      I4 => ram_reg_i_42_n_4,
      I5 => Q(3),
      O => address0(0)
    );
ram_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A0C0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(10),
      I1 => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(10),
      I2 => Q(1),
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => ram_reg_i_9_n_4
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb is
  port (
    DIADI : out STD_LOGIC_VECTOR ( 4 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    INPUT_STREAM_V_data_V_0_sel : in STD_LOGIC;
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \invdar_reg_334_reg[6]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    ap_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb is
begin
Adder2_Adder2_strbkb_rom_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom
     port map (
      DIADI(4 downto 0) => DIADI(4 downto 0),
      \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(4 downto 0) => \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(4 downto 0),
      INPUT_STREAM_V_data_V_0_sel => INPUT_STREAM_V_data_V_0_sel,
      Q(4 downto 0) => Q(4 downto 0),
      \ap_CS_fsm_reg[3]\(1 downto 0) => \ap_CS_fsm_reg[3]\(1 downto 0),
      ap_clk => ap_clk,
      \invdar_reg_334_reg[6]\(6 downto 0) => \invdar_reg_334_reg[6]\(6 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input is
  port (
    I1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ap_clk : in STD_LOGIC;
    DIADI : in STD_LOGIC_VECTOR ( 4 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \tmp_reg_484_reg[6]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \i_1_reg_356_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ap_enable_reg_pp1_iter0 : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\ : in STD_LOGIC_VECTOR ( 26 downto 0 );
    INPUT_STREAM_V_data_V_0_sel : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input is
begin
Adder2_input_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram
     port map (
      D(31 downto 0) => D(31 downto 0),
      DIADI(4 downto 0) => DIADI(4 downto 0),
      E(0) => E(0),
      I1(31 downto 0) => I1(31 downto 0),
      \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(26 downto 0) => \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(26 downto 0),
      \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(26 downto 0) => \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(26 downto 0),
      INPUT_STREAM_V_data_V_0_sel => INPUT_STREAM_V_data_V_0_sel,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      Q(3 downto 0) => Q(3 downto 0),
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter0 => ap_enable_reg_pp1_iter0,
      \i_1_reg_356_reg[5]\(5 downto 0) => \i_1_reg_356_reg[5]\(5 downto 0),
      \i_reg_345_reg[5]\(5 downto 0) => \i_reg_345_reg[5]\(5 downto 0),
      \tmp_reg_484_reg[6]\(6 downto 0) => \tmp_reg_484_reg[6]\(6 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud is
  port (
    \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_V_keep_V_0_sel : in STD_LOGIC;
    \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud is
begin
Adder2_inputValuecud_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram_1
     port map (
      E(0) => E(0),
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(3 downto 0) => \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(3 downto 0),
      INPUT_STREAM_V_keep_V_0_sel => INPUT_STREAM_V_keep_V_0_sel,
      \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(3 downto 0) => \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(3 downto 0),
      Q(3 downto 0) => Q(3 downto 0),
      \ap_CS_fsm_reg[3]\ => \ap_CS_fsm_reg[3]\,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => \i_reg_345_reg[0]\,
      \i_reg_345_reg[1]\ => \i_reg_345_reg[1]\,
      \i_reg_345_reg[2]\ => \i_reg_345_reg[2]\,
      \i_reg_345_reg[3]\ => \i_reg_345_reg[3]\,
      \i_reg_345_reg[4]\ => \i_reg_345_reg[4]\,
      \i_reg_345_reg[5]\ => \i_reg_345_reg[5]\,
      \i_reg_345_reg[5]_0\ => \i_reg_345_reg[5]_0\,
      \i_reg_345_reg[5]_1\ => \i_reg_345_reg[5]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0 is
  port (
    \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_V_strb_V_0_sel : in STD_LOGIC;
    \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0 : entity is "Adder2_inputValuecud";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0 is
begin
Adder2_inputValuecud_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_ram
     port map (
      E(0) => E(0),
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(3 downto 0) => \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(3 downto 0),
      INPUT_STREAM_V_strb_V_0_sel => INPUT_STREAM_V_strb_V_0_sel,
      \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(3 downto 0) => \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(3 downto 0),
      Q(3 downto 0) => Q(3 downto 0),
      address0(3) => \i_reg_345_reg[3]\,
      address0(2) => \i_reg_345_reg[2]\,
      address0(1) => \i_reg_345_reg[1]\,
      address0(0) => \i_reg_345_reg[0]\,
      \ap_CS_fsm_reg[3]\ => \ap_CS_fsm_reg[3]\,
      ap_clk => ap_clk,
      \i_reg_345_reg[4]\ => \i_reg_345_reg[4]\,
      \i_reg_345_reg[5]\ => \i_reg_345_reg[5]\,
      \i_reg_345_reg[5]_0\ => \i_reg_345_reg[5]_0\,
      \i_reg_345_reg[5]_1\ => \i_reg_345_reg[5]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg is
  port (
    \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_user_V_1_state_reg[0]\ : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_sel_wr : in STD_LOGIC;
    OUTPUT_STREAM_V_user_V_1_payload_A : in STD_LOGIC_VECTOR ( 1 downto 0 );
    OUTPUT_STREAM_V_user_V_1_payload_B : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    INPUT_STREAM_V_user_V_0_payload_B : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INPUT_STREAM_V_user_V_0_sel : in STD_LOGIC;
    INPUT_STREAM_V_user_V_0_payload_A : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg is
begin
Adder2_inputValueeOg_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg_ram
     port map (
      E(0) => E(0),
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      INPUT_STREAM_V_user_V_0_payload_A(1 downto 0) => INPUT_STREAM_V_user_V_0_payload_A(1 downto 0),
      INPUT_STREAM_V_user_V_0_payload_B(1 downto 0) => INPUT_STREAM_V_user_V_0_payload_B(1 downto 0),
      INPUT_STREAM_V_user_V_0_sel => INPUT_STREAM_V_user_V_0_sel,
      OUTPUT_STREAM_V_user_V_1_ack_in => OUTPUT_STREAM_V_user_V_1_ack_in,
      OUTPUT_STREAM_V_user_V_1_payload_A(1 downto 0) => OUTPUT_STREAM_V_user_V_1_payload_A(1 downto 0),
      \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\ => \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\,
      \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\ => \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\,
      OUTPUT_STREAM_V_user_V_1_payload_B(1 downto 0) => OUTPUT_STREAM_V_user_V_1_payload_B(1 downto 0),
      \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\ => \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\,
      \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\ => \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\,
      OUTPUT_STREAM_V_user_V_1_sel_wr => OUTPUT_STREAM_V_user_V_1_sel_wr,
      \OUTPUT_STREAM_V_user_V_1_state_reg[0]\ => \OUTPUT_STREAM_V_user_V_1_state_reg[0]\,
      \ap_CS_fsm_reg[3]\ => \ap_CS_fsm_reg[3]\,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => \i_reg_345_reg[0]\,
      \i_reg_345_reg[1]\ => \i_reg_345_reg[1]\,
      \i_reg_345_reg[2]\ => \i_reg_345_reg[2]\,
      \i_reg_345_reg[3]\ => \i_reg_345_reg[3]\,
      \i_reg_345_reg[4]\ => \i_reg_345_reg[4]\,
      \i_reg_345_reg[5]\ => \i_reg_345_reg[5]\,
      \i_reg_345_reg[5]_0\ => \i_reg_345_reg[5]_0\,
      \i_reg_345_reg[5]_1\ => \i_reg_345_reg[5]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi is
  port (
    d0 : out STD_LOGIC;
    \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\ : out STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    ap_clk : in STD_LOGIC;
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    INPUT_STREAM_V_last_V_0_payload_B : in STD_LOGIC;
    INPUT_STREAM_V_last_V_0_sel : in STD_LOGIC;
    INPUT_STREAM_V_last_V_0_payload_A : in STD_LOGIC;
    \OUTPUT_STREAM_V_last_V_1_state_reg[0]\ : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_ack_in : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_sel_wr : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_payload_A : in STD_LOGIC;
    OUTPUT_STREAM_V_last_V_1_payload_B : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi is
begin
Adder2_inputValuefYi_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi_ram
     port map (
      E(0) => E(0),
      I8(0) => d0,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      INPUT_STREAM_V_last_V_0_payload_A => INPUT_STREAM_V_last_V_0_payload_A,
      INPUT_STREAM_V_last_V_0_payload_B => INPUT_STREAM_V_last_V_0_payload_B,
      INPUT_STREAM_V_last_V_0_sel => INPUT_STREAM_V_last_V_0_sel,
      OUTPUT_STREAM_V_last_V_1_ack_in => OUTPUT_STREAM_V_last_V_1_ack_in,
      OUTPUT_STREAM_V_last_V_1_payload_A => OUTPUT_STREAM_V_last_V_1_payload_A,
      \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\ => \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\,
      OUTPUT_STREAM_V_last_V_1_payload_B => OUTPUT_STREAM_V_last_V_1_payload_B,
      \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\ => \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\,
      OUTPUT_STREAM_V_last_V_1_sel_wr => OUTPUT_STREAM_V_last_V_1_sel_wr,
      \OUTPUT_STREAM_V_last_V_1_state_reg[0]\ => \OUTPUT_STREAM_V_last_V_1_state_reg[0]\,
      address0(3) => \i_reg_345_reg[3]\,
      address0(2) => \i_reg_345_reg[2]\,
      address0(1) => \i_reg_345_reg[1]\,
      address0(0) => \i_reg_345_reg[0]\,
      \ap_CS_fsm_reg[3]\ => \ap_CS_fsm_reg[3]\,
      ap_clk => ap_clk,
      \i_reg_345_reg[4]\ => \i_reg_345_reg[4]\,
      \i_reg_345_reg[5]\ => \i_reg_345_reg[5]\,
      \i_reg_345_reg[5]_0\ => \i_reg_345_reg[5]_0\,
      \i_reg_345_reg[5]_1\ => \i_reg_345_reg[5]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j is
  port (
    \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \ap_CS_fsm_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    INPUT_STREAM_V_id_V_0_sel : in STD_LOGIC;
    \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ap_clk : in STD_LOGIC;
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[0]\ : in STD_LOGIC;
    \i_reg_345_reg[1]\ : in STD_LOGIC;
    \i_reg_345_reg[2]\ : in STD_LOGIC;
    \i_reg_345_reg[3]\ : in STD_LOGIC;
    \i_reg_345_reg[4]\ : in STD_LOGIC;
    \i_reg_345_reg[5]\ : in STD_LOGIC;
    \i_reg_345_reg[5]_0\ : in STD_LOGIC;
    \i_reg_345_reg[5]_1\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j is
begin
Adder2_inputValueg8j_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j_ram
     port map (
      E(0) => E(0),
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(4 downto 0) => \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(4 downto 0),
      INPUT_STREAM_V_id_V_0_sel => INPUT_STREAM_V_id_V_0_sel,
      \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(4 downto 0) => \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      \ap_CS_fsm_reg[3]\ => \ap_CS_fsm_reg[3]\,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => \i_reg_345_reg[0]\,
      \i_reg_345_reg[1]\ => \i_reg_345_reg[1]\,
      \i_reg_345_reg[2]\ => \i_reg_345_reg[2]\,
      \i_reg_345_reg[3]\ => \i_reg_345_reg[3]\,
      \i_reg_345_reg[4]\ => \i_reg_345_reg[4]\,
      \i_reg_345_reg[5]\ => \i_reg_345_reg[5]\,
      \i_reg_345_reg[5]_0\ => \i_reg_345_reg[5]_0\,
      \i_reg_345_reg[5]_1\ => \i_reg_345_reg[5]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi is
  port (
    \q0_reg[5]\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    ap_block_pp1_stage0_subdone1_in : out STD_LOGIC;
    \q0_reg[0]\ : out STD_LOGIC;
    \q0_reg[0]_0\ : out STD_LOGIC;
    \q0_reg[0]_1\ : out STD_LOGIC;
    \q0_reg[0]_2\ : out STD_LOGIC;
    \q0_reg[5]_0\ : out STD_LOGIC;
    \q0_reg[5]_1\ : out STD_LOGIC;
    \q0_reg[4]\ : out STD_LOGIC;
    \q0_reg[5]_2\ : out STD_LOGIC;
    \q0_reg[0]_3\ : out STD_LOGIC;
    \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    INPUT_STREAM_V_dest_V_0_sel : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \ap_CS_fsm_reg[5]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \i_reg_345_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \INPUT_STREAM_V_data_V_0_state_reg[0]\ : in STD_LOGIC;
    ap_enable_reg_pp1_iter0 : in STD_LOGIC;
    \i_1_reg_356_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \exitcond_reg_509_reg[0]\ : in STD_LOGIC;
    ap_enable_reg_pp1_iter1_reg : in STD_LOGIC;
    ap_reg_pp1_iter1_exitcond_reg_509 : in STD_LOGIC;
    ap_enable_reg_pp1_iter2_reg : in STD_LOGIC;
    OUTPUT_STREAM_V_data_V_1_ack_in : in STD_LOGIC;
    ap_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi is
begin
Adder2_inputValuehbi_ram_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi_ram
     port map (
      E(0) => E(0),
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg[0]\,
      \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(5 downto 0) => \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(5 downto 0),
      INPUT_STREAM_V_dest_V_0_sel => INPUT_STREAM_V_dest_V_0_sel,
      OUTPUT_STREAM_V_data_V_1_ack_in => OUTPUT_STREAM_V_data_V_1_ack_in,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(5 downto 0) => \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(5 downto 0),
      Q(5 downto 0) => Q(5 downto 0),
      address0(3) => \q0_reg[0]_2\,
      address0(2) => \q0_reg[0]_1\,
      address0(1) => \q0_reg[0]_0\,
      address0(0) => \q0_reg[0]\,
      \ap_CS_fsm_reg[5]\(1 downto 0) => \ap_CS_fsm_reg[5]\(1 downto 0),
      ap_block_pp1_stage0_subdone1_in => ap_block_pp1_stage0_subdone1_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter0 => ap_enable_reg_pp1_iter0,
      ap_enable_reg_pp1_iter1_reg => ap_enable_reg_pp1_iter1_reg,
      ap_enable_reg_pp1_iter2_reg => ap_enable_reg_pp1_iter2_reg,
      ap_reg_pp1_iter1_exitcond_reg_509 => ap_reg_pp1_iter1_exitcond_reg_509,
      \exitcond_reg_509_reg[0]\ => \exitcond_reg_509_reg[0]\,
      \i_1_reg_356_reg[5]\(5 downto 0) => \i_1_reg_356_reg[5]\(5 downto 0),
      \i_reg_345_reg[5]\(5 downto 0) => \i_reg_345_reg[5]\(5 downto 0),
      \q0_reg[0]_0\ => \q0_reg[0]_3\,
      \q0_reg[4]_0\ => \q0_reg[4]\,
      \q0_reg[5]_0\ => \q0_reg[5]\,
      \q0_reg[5]_1\ => \q0_reg[5]_0\,
      \q0_reg[5]_2\ => \q0_reg[5]_1\,
      \q0_reg[5]_3\ => \q0_reg[5]_2\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    INPUT_STREAM_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    INPUT_STREAM_TVALID : in STD_LOGIC;
    INPUT_STREAM_TREADY : out STD_LOGIC;
    INPUT_STREAM_TKEEP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TUSER : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INPUT_STREAM_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    INPUT_STREAM_TID : in STD_LOGIC_VECTOR ( 4 downto 0 );
    INPUT_STREAM_TDEST : in STD_LOGIC_VECTOR ( 5 downto 0 );
    OUTPUT_STREAM_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OUTPUT_STREAM_TVALID : out STD_LOGIC;
    OUTPUT_STREAM_TREADY : in STD_LOGIC;
    OUTPUT_STREAM_TKEEP : out STD_LOGIC_VECTOR ( 3 downto 0 );
    OUTPUT_STREAM_TSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    OUTPUT_STREAM_TUSER : out STD_LOGIC_VECTOR ( 1 downto 0 );
    OUTPUT_STREAM_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    OUTPUT_STREAM_TID : out STD_LOGIC_VECTOR ( 4 downto 0 );
    OUTPUT_STREAM_TDEST : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CONTROL_BUS_AWVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_AWADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_WVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_WREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CONTROL_BUS_ARVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_ARADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_RVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_RREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CONTROL_BUS_BVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_BREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    interrupt : out STD_LOGIC
  );
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 : entity is 5;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 : entity is 32;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 is
  signal \<const0>\ : STD_LOGIC;
  signal Adder2_CONTROL_BUS_s_axi_U_n_12 : STD_LOGIC;
  signal Adder2_stream_strea_U_n_4 : STD_LOGIC;
  signal Adder2_stream_strea_U_n_5 : STD_LOGIC;
  signal Adder2_stream_strea_U_n_6 : STD_LOGIC;
  signal Adder2_stream_strea_U_n_7 : STD_LOGIC;
  signal Adder2_stream_strea_U_n_8 : STD_LOGIC;
  signal \^input_stream_tready\ : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_load_A : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_load_B : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_payload_A : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal INPUT_STREAM_V_data_V_0_payload_B : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal INPUT_STREAM_V_data_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_data_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_load_A : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_load_B : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_payload_A : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal INPUT_STREAM_V_dest_V_0_payload_B : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal INPUT_STREAM_V_dest_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_dest_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_load_A : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_load_B : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_payload_A : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal INPUT_STREAM_V_id_V_0_payload_B : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal INPUT_STREAM_V_id_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_id_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_id_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_load_A : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_load_B : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_payload_A : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal INPUT_STREAM_V_keep_V_0_payload_B : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal INPUT_STREAM_V_keep_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_keep_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_keep_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_payload_A : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4\ : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_payload_B : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4\ : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_last_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_load_A : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_load_B : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_payload_A : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal INPUT_STREAM_V_strb_V_0_payload_B : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal INPUT_STREAM_V_strb_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_strb_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_strb_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_ack_in : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_payload_A : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \INPUT_STREAM_V_user_V_0_payload_A[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_user_V_0_payload_A[1]_i_1_n_4\ : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_payload_B : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \INPUT_STREAM_V_user_V_0_payload_B[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_user_V_0_payload_B[1]_i_1_n_4\ : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_sel : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_sel_rd_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_sel_wr : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_sel_wr_i_1_n_4 : STD_LOGIC;
  signal INPUT_STREAM_V_user_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \INPUT_STREAM_V_user_V_0_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\ : STD_LOGIC;
  signal \^output_stream_tvalid\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_load_A : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_load_B : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_payload_A : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal OUTPUT_STREAM_V_data_V_1_payload_B : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal OUTPUT_STREAM_V_data_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_data_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_data_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_load_A : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_load_B : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_payload_A : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal OUTPUT_STREAM_V_dest_V_1_payload_B : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal OUTPUT_STREAM_V_dest_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_dest_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_dest_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_load_A : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_load_B : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_payload_A : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal OUTPUT_STREAM_V_id_V_1_payload_B : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal OUTPUT_STREAM_V_id_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_id_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_id_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_load_A : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_load_B : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_payload_A : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal OUTPUT_STREAM_V_keep_V_1_payload_B : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal OUTPUT_STREAM_V_keep_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_keep_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_keep_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_payload_A : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_payload_B : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_last_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_last_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0]\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_load_A : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_load_B : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_payload_A : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal OUTPUT_STREAM_V_strb_V_1_payload_B : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal OUTPUT_STREAM_V_strb_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_strb_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_strb_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\ : STD_LOGIC;
  signal OUTPUT_STREAM_V_user_V_1_ack_in : STD_LOGIC;
  signal OUTPUT_STREAM_V_user_V_1_payload_A : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal OUTPUT_STREAM_V_user_V_1_payload_B : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal OUTPUT_STREAM_V_user_V_1_sel : STD_LOGIC;
  signal OUTPUT_STREAM_V_user_V_1_sel_rd_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_user_V_1_sel_wr : STD_LOGIC;
  signal OUTPUT_STREAM_V_user_V_1_sel_wr_i_1_n_4 : STD_LOGIC;
  signal OUTPUT_STREAM_V_user_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \OUTPUT_STREAM_V_user_V_1_state[0]_i_1_n_4\ : STD_LOGIC;
  signal \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0]\ : STD_LOGIC;
  signal \ap_CS_fsm[5]_i_2_n_4\ : STD_LOGIC;
  signal \ap_CS_fsm[6]_i_2_n_4\ : STD_LOGIC;
  signal \ap_CS_fsm[6]_i_3_n_4\ : STD_LOGIC;
  signal ap_CS_fsm_pp1_stage0 : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_4_[0]\ : STD_LOGIC;
  signal ap_CS_fsm_state10 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_CS_fsm_state4 : STD_LOGIC;
  signal ap_CS_fsm_state5 : STD_LOGIC;
  signal ap_CS_fsm_state9 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal ap_NS_fsm19_out : STD_LOGIC;
  signal ap_block_pp1_stage0_subdone1_in : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter0_i_1_n_4 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter1_i_1_n_4 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter1_reg_n_4 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter2_i_1_n_4 : STD_LOGIC;
  signal ap_enable_reg_pp1_iter2_reg_n_4 : STD_LOGIC;
  signal ap_reg_pp1_iter1_exitcond_reg_509 : STD_LOGIC;
  signal \ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1_n_4\ : STD_LOGIC;
  signal ce0 : STD_LOGIC;
  signal d0 : STD_LOGIC;
  signal \exitcond_reg_509[0]_i_1_n_4\ : STD_LOGIC;
  signal \exitcond_reg_509_reg_n_4_[0]\ : STD_LOGIC;
  signal i_1_reg_356 : STD_LOGIC;
  signal i_1_reg_3560 : STD_LOGIC;
  signal \i_1_reg_356[5]_i_4_n_4\ : STD_LOGIC;
  signal \i_1_reg_356_reg__0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal i_2_fu_397_p2 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal i_3_fu_455_p2 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \i_reg_345[5]_i_1_n_4\ : STD_LOGIC;
  signal \i_reg_345_reg_n_4_[0]\ : STD_LOGIC;
  signal \i_reg_345_reg_n_4_[1]\ : STD_LOGIC;
  signal \i_reg_345_reg_n_4_[2]\ : STD_LOGIC;
  signal \i_reg_345_reg_n_4_[3]\ : STD_LOGIC;
  signal \i_reg_345_reg_n_4_[4]\ : STD_LOGIC;
  signal indvarinc_fu_367_p2 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal indvarinc_reg_479 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \indvarinc_reg_479[6]_i_2_n_4\ : STD_LOGIC;
  signal inputValues_1_U_n_4 : STD_LOGIC;
  signal inputValues_1_U_n_5 : STD_LOGIC;
  signal inputValues_1_U_n_6 : STD_LOGIC;
  signal inputValues_1_U_n_7 : STD_LOGIC;
  signal inputValues_2_U_n_4 : STD_LOGIC;
  signal inputValues_2_U_n_5 : STD_LOGIC;
  signal inputValues_2_U_n_6 : STD_LOGIC;
  signal inputValues_2_U_n_7 : STD_LOGIC;
  signal inputValues_3_U_n_4 : STD_LOGIC;
  signal inputValues_3_U_n_5 : STD_LOGIC;
  signal inputValues_3_U_n_6 : STD_LOGIC;
  signal inputValues_3_U_n_7 : STD_LOGIC;
  signal inputValues_4_U_n_5 : STD_LOGIC;
  signal inputValues_4_U_n_6 : STD_LOGIC;
  signal inputValues_5_U_n_4 : STD_LOGIC;
  signal inputValues_5_U_n_5 : STD_LOGIC;
  signal inputValues_5_U_n_6 : STD_LOGIC;
  signal inputValues_5_U_n_7 : STD_LOGIC;
  signal inputValues_5_U_n_8 : STD_LOGIC;
  signal inputValues_6_U_n_10 : STD_LOGIC;
  signal inputValues_6_U_n_11 : STD_LOGIC;
  signal inputValues_6_U_n_12 : STD_LOGIC;
  signal inputValues_6_U_n_13 : STD_LOGIC;
  signal inputValues_6_U_n_14 : STD_LOGIC;
  signal inputValues_6_U_n_15 : STD_LOGIC;
  signal inputValues_6_U_n_16 : STD_LOGIC;
  signal inputValues_6_U_n_17 : STD_LOGIC;
  signal inputValues_6_U_n_18 : STD_LOGIC;
  signal inputValues_6_U_n_19 : STD_LOGIC;
  signal inputValues_6_U_n_20 : STD_LOGIC;
  signal inputValues_6_U_n_21 : STD_LOGIC;
  signal inputValues_6_U_n_4 : STD_LOGIC;
  signal inputValues_6_U_n_5 : STD_LOGIC;
  signal inputValues_6_U_n_7 : STD_LOGIC;
  signal inputValues_6_U_n_8 : STD_LOGIC;
  signal inputValues_6_U_n_9 : STD_LOGIC;
  signal input_q0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal invdar_reg_334 : STD_LOGIC;
  signal invdar_reg_3340 : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[0]\ : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[1]\ : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[2]\ : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[3]\ : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[4]\ : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[5]\ : STD_LOGIC;
  signal \invdar_reg_334_reg_n_4_[6]\ : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal \tmp_1_reg_494[0]_i_1_n_4\ : STD_LOGIC;
  signal \tmp_1_reg_494[0]_i_2_n_4\ : STD_LOGIC;
  signal \tmp_1_reg_494_reg_n_4_[0]\ : STD_LOGIC;
  signal tmp_2_fu_389_p3 : STD_LOGIC;
  signal tmp_data_V_1_fu_472_p2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal tmp_reg_484 : STD_LOGIC_VECTOR ( 6 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of INPUT_STREAM_V_data_V_0_sel_wr_i_1 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of INPUT_STREAM_V_id_V_0_sel_wr_i_1 : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of INPUT_STREAM_V_keep_V_0_sel_wr_i_1 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of INPUT_STREAM_V_last_V_0_sel_wr_i_1 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of INPUT_STREAM_V_strb_V_0_sel_wr_i_1 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of INPUT_STREAM_V_user_V_0_sel_wr_i_1 : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[0]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[10]_INST_0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[11]_INST_0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[12]_INST_0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[13]_INST_0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[14]_INST_0\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[15]_INST_0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[16]_INST_0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[17]_INST_0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[18]_INST_0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[19]_INST_0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[1]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[20]_INST_0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[21]_INST_0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[22]_INST_0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[23]_INST_0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[24]_INST_0\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[25]_INST_0\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[26]_INST_0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[27]_INST_0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[29]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[2]_INST_0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[30]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[31]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[3]_INST_0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[4]_INST_0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[5]_INST_0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[6]_INST_0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[7]_INST_0\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[8]_INST_0\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDATA[9]_INST_0\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDEST[0]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDEST[1]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDEST[2]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDEST[3]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDEST[4]_INST_0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TDEST[5]_INST_0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TID[0]_INST_0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TID[1]_INST_0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TID[2]_INST_0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TID[3]_INST_0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TKEEP[0]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TKEEP[1]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TKEEP[2]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TKEEP[3]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TSTRB[0]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TSTRB[1]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TSTRB[2]_INST_0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TSTRB[3]_INST_0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TUSER[0]_INST_0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_TUSER[1]_INST_0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_data_V_1_sel_rd_i_1 : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_data_V_1_sel_wr_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_dest_V_1_state[1]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_id_V_1_sel_rd_i_1 : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_id_V_1_state[1]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_keep_V_1_state[1]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_last_V_1_sel_rd_i_1 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_last_V_1_state[1]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_strb_V_1_state[1]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of OUTPUT_STREAM_V_user_V_1_sel_rd_i_1 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \OUTPUT_STREAM_V_user_V_1_state[1]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \ap_CS_fsm[5]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \ap_CS_fsm[6]_i_3\ : label is "soft_lutpair8";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[5]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[6]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[7]\ : label is "none";
  attribute SOFT_HLUTNM of \i_1_reg_356[1]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \i_1_reg_356[2]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \i_1_reg_356[3]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \i_1_reg_356[4]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \i_reg_345[0]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \i_reg_345[1]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \i_reg_345[2]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \i_reg_345[3]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \i_reg_345[4]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \i_reg_345[5]_i_3\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \indvarinc_reg_479[0]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \indvarinc_reg_479[1]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \indvarinc_reg_479[2]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \indvarinc_reg_479[3]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \indvarinc_reg_479[4]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \indvarinc_reg_479[6]_i_2\ : label is "soft_lutpair9";
begin
  INPUT_STREAM_TREADY <= \^input_stream_tready\;
  OUTPUT_STREAM_TVALID <= \^output_stream_tvalid\;
  s_axi_CONTROL_BUS_BRESP(1) <= \<const0>\;
  s_axi_CONTROL_BUS_BRESP(0) <= \<const0>\;
  s_axi_CONTROL_BUS_RRESP(1) <= \<const0>\;
  s_axi_CONTROL_BUS_RRESP(0) <= \<const0>\;
Adder2_CONTROL_BUS_s_axi_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
     port map (
      D(1 downto 0) => ap_NS_fsm(1 downto 0),
      I1(31 downto 0) => input_q0(31 downto 0),
      OUTPUT_STREAM_V_data_V_1_ack_in => OUTPUT_STREAM_V_data_V_1_ack_in,
      OUTPUT_STREAM_V_dest_V_1_ack_in => OUTPUT_STREAM_V_dest_V_1_ack_in,
      OUTPUT_STREAM_V_id_V_1_ack_in => OUTPUT_STREAM_V_id_V_1_ack_in,
      OUTPUT_STREAM_V_keep_V_1_ack_in => OUTPUT_STREAM_V_keep_V_1_ack_in,
      OUTPUT_STREAM_V_last_V_1_ack_in => OUTPUT_STREAM_V_last_V_1_ack_in,
      OUTPUT_STREAM_V_strb_V_1_ack_in => OUTPUT_STREAM_V_strb_V_1_ack_in,
      OUTPUT_STREAM_V_user_V_1_ack_in => OUTPUT_STREAM_V_user_V_1_ack_in,
      Q(2) => ap_CS_fsm_state10,
      Q(1) => ap_CS_fsm_state3,
      Q(0) => \ap_CS_fsm_reg_n_4_[0]\,
      SR(0) => invdar_reg_334,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      \int_ap_return_reg[31]_0\ => Adder2_CONTROL_BUS_s_axi_U_n_12,
      interrupt => interrupt,
      \out\(2) => s_axi_CONTROL_BUS_BVALID,
      \out\(1) => s_axi_CONTROL_BUS_WREADY,
      \out\(0) => s_axi_CONTROL_BUS_AWREADY,
      reset => reset,
      s_axi_CONTROL_BUS_ARADDR(4 downto 0) => s_axi_CONTROL_BUS_ARADDR(4 downto 0),
      s_axi_CONTROL_BUS_ARREADY => s_axi_CONTROL_BUS_ARREADY,
      s_axi_CONTROL_BUS_ARVALID => s_axi_CONTROL_BUS_ARVALID,
      s_axi_CONTROL_BUS_AWADDR(4 downto 0) => s_axi_CONTROL_BUS_AWADDR(4 downto 0),
      s_axi_CONTROL_BUS_AWVALID => s_axi_CONTROL_BUS_AWVALID,
      s_axi_CONTROL_BUS_BREADY => s_axi_CONTROL_BUS_BREADY,
      s_axi_CONTROL_BUS_RDATA(31 downto 0) => s_axi_CONTROL_BUS_RDATA(31 downto 0),
      s_axi_CONTROL_BUS_RREADY => s_axi_CONTROL_BUS_RREADY,
      s_axi_CONTROL_BUS_RVALID => s_axi_CONTROL_BUS_RVALID,
      s_axi_CONTROL_BUS_WDATA(2) => s_axi_CONTROL_BUS_WDATA(7),
      s_axi_CONTROL_BUS_WDATA(1 downto 0) => s_axi_CONTROL_BUS_WDATA(1 downto 0),
      s_axi_CONTROL_BUS_WSTRB(0) => s_axi_CONTROL_BUS_WSTRB(0),
      s_axi_CONTROL_BUS_WVALID => s_axi_CONTROL_BUS_WVALID,
      \tmp_1_reg_494_reg[0]\ => \tmp_1_reg_494_reg_n_4_[0]\
    );
Adder2_stream_strea_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb
     port map (
      DIADI(4) => Adder2_stream_strea_U_n_4,
      DIADI(3) => Adder2_stream_strea_U_n_5,
      DIADI(2) => Adder2_stream_strea_U_n_6,
      DIADI(1) => Adder2_stream_strea_U_n_7,
      DIADI(0) => Adder2_stream_strea_U_n_8,
      \INPUT_STREAM_V_data_V_0_payload_B_reg[4]\(4 downto 0) => INPUT_STREAM_V_data_V_0_payload_B(4 downto 0),
      INPUT_STREAM_V_data_V_0_sel => INPUT_STREAM_V_data_V_0_sel,
      Q(4 downto 0) => INPUT_STREAM_V_data_V_0_payload_A(4 downto 0),
      \ap_CS_fsm_reg[3]\(1) => ap_CS_fsm_state4,
      \ap_CS_fsm_reg[3]\(0) => ce0,
      ap_clk => ap_clk,
      \invdar_reg_334_reg[6]\(6) => \invdar_reg_334_reg_n_4_[6]\,
      \invdar_reg_334_reg[6]\(5) => \invdar_reg_334_reg_n_4_[5]\,
      \invdar_reg_334_reg[6]\(4) => \invdar_reg_334_reg_n_4_[4]\,
      \invdar_reg_334_reg[6]\(3) => \invdar_reg_334_reg_n_4_[3]\,
      \invdar_reg_334_reg[6]\(2) => \invdar_reg_334_reg_n_4_[2]\,
      \invdar_reg_334_reg[6]\(1) => \invdar_reg_334_reg_n_4_[1]\,
      \invdar_reg_334_reg[6]\(0) => \invdar_reg_334_reg_n_4_[0]\
    );
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
\INPUT_STREAM_V_data_V_0_payload_A[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_data_V_0_ack_in,
      I2 => INPUT_STREAM_V_data_V_0_sel_wr,
      O => INPUT_STREAM_V_data_V_0_load_A
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(0),
      Q => INPUT_STREAM_V_data_V_0_payload_A(0),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(10),
      Q => INPUT_STREAM_V_data_V_0_payload_A(10),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(11),
      Q => INPUT_STREAM_V_data_V_0_payload_A(11),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(12),
      Q => INPUT_STREAM_V_data_V_0_payload_A(12),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(13),
      Q => INPUT_STREAM_V_data_V_0_payload_A(13),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(14),
      Q => INPUT_STREAM_V_data_V_0_payload_A(14),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(15),
      Q => INPUT_STREAM_V_data_V_0_payload_A(15),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(16),
      Q => INPUT_STREAM_V_data_V_0_payload_A(16),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(17),
      Q => INPUT_STREAM_V_data_V_0_payload_A(17),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(18),
      Q => INPUT_STREAM_V_data_V_0_payload_A(18),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(19),
      Q => INPUT_STREAM_V_data_V_0_payload_A(19),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(1),
      Q => INPUT_STREAM_V_data_V_0_payload_A(1),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(20),
      Q => INPUT_STREAM_V_data_V_0_payload_A(20),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(21),
      Q => INPUT_STREAM_V_data_V_0_payload_A(21),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(22),
      Q => INPUT_STREAM_V_data_V_0_payload_A(22),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(23),
      Q => INPUT_STREAM_V_data_V_0_payload_A(23),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(24),
      Q => INPUT_STREAM_V_data_V_0_payload_A(24),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(25),
      Q => INPUT_STREAM_V_data_V_0_payload_A(25),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(26),
      Q => INPUT_STREAM_V_data_V_0_payload_A(26),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(27),
      Q => INPUT_STREAM_V_data_V_0_payload_A(27),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(28),
      Q => INPUT_STREAM_V_data_V_0_payload_A(28),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(29),
      Q => INPUT_STREAM_V_data_V_0_payload_A(29),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(2),
      Q => INPUT_STREAM_V_data_V_0_payload_A(2),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(30),
      Q => INPUT_STREAM_V_data_V_0_payload_A(30),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(31),
      Q => INPUT_STREAM_V_data_V_0_payload_A(31),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(3),
      Q => INPUT_STREAM_V_data_V_0_payload_A(3),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(4),
      Q => INPUT_STREAM_V_data_V_0_payload_A(4),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(5),
      Q => INPUT_STREAM_V_data_V_0_payload_A(5),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(6),
      Q => INPUT_STREAM_V_data_V_0_payload_A(6),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(7),
      Q => INPUT_STREAM_V_data_V_0_payload_A(7),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(8),
      Q => INPUT_STREAM_V_data_V_0_payload_A(8),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_A,
      D => INPUT_STREAM_TDATA(9),
      Q => INPUT_STREAM_V_data_V_0_payload_A(9),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_data_V_0_ack_in,
      I2 => INPUT_STREAM_V_data_V_0_sel_wr,
      O => INPUT_STREAM_V_data_V_0_load_B
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(0),
      Q => INPUT_STREAM_V_data_V_0_payload_B(0),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(10),
      Q => INPUT_STREAM_V_data_V_0_payload_B(10),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(11),
      Q => INPUT_STREAM_V_data_V_0_payload_B(11),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(12),
      Q => INPUT_STREAM_V_data_V_0_payload_B(12),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(13),
      Q => INPUT_STREAM_V_data_V_0_payload_B(13),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(14),
      Q => INPUT_STREAM_V_data_V_0_payload_B(14),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(15),
      Q => INPUT_STREAM_V_data_V_0_payload_B(15),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(16),
      Q => INPUT_STREAM_V_data_V_0_payload_B(16),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(17),
      Q => INPUT_STREAM_V_data_V_0_payload_B(17),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(18),
      Q => INPUT_STREAM_V_data_V_0_payload_B(18),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(19),
      Q => INPUT_STREAM_V_data_V_0_payload_B(19),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(1),
      Q => INPUT_STREAM_V_data_V_0_payload_B(1),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(20),
      Q => INPUT_STREAM_V_data_V_0_payload_B(20),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(21),
      Q => INPUT_STREAM_V_data_V_0_payload_B(21),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(22),
      Q => INPUT_STREAM_V_data_V_0_payload_B(22),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(23),
      Q => INPUT_STREAM_V_data_V_0_payload_B(23),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(24),
      Q => INPUT_STREAM_V_data_V_0_payload_B(24),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(25),
      Q => INPUT_STREAM_V_data_V_0_payload_B(25),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(26),
      Q => INPUT_STREAM_V_data_V_0_payload_B(26),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(27),
      Q => INPUT_STREAM_V_data_V_0_payload_B(27),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(28),
      Q => INPUT_STREAM_V_data_V_0_payload_B(28),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(29),
      Q => INPUT_STREAM_V_data_V_0_payload_B(29),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(2),
      Q => INPUT_STREAM_V_data_V_0_payload_B(2),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(30),
      Q => INPUT_STREAM_V_data_V_0_payload_B(30),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(31),
      Q => INPUT_STREAM_V_data_V_0_payload_B(31),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(3),
      Q => INPUT_STREAM_V_data_V_0_payload_B(3),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(4),
      Q => INPUT_STREAM_V_data_V_0_payload_B(4),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(5),
      Q => INPUT_STREAM_V_data_V_0_payload_B(5),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(6),
      Q => INPUT_STREAM_V_data_V_0_payload_B(6),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(7),
      Q => INPUT_STREAM_V_data_V_0_payload_B(7),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(8),
      Q => INPUT_STREAM_V_data_V_0_payload_B(8),
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_data_V_0_load_B,
      D => INPUT_STREAM_TDATA(9),
      Q => INPUT_STREAM_V_data_V_0_payload_B(9),
      R => '0'
    );
INPUT_STREAM_V_data_V_0_sel_rd_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DF20"
    )
        port map (
      I0 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I1 => tmp_2_fu_389_p3,
      I2 => ap_CS_fsm_state4,
      I3 => INPUT_STREAM_V_data_V_0_sel,
      O => INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_data_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_data_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_data_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_data_V_0_ack_in,
      I2 => INPUT_STREAM_V_data_V_0_sel_wr,
      O => INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_data_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_data_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_data_V_0_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFF00000000000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => INPUT_STREAM_TVALID,
      I3 => INPUT_STREAM_V_data_V_0_ack_in,
      I4 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I5 => ap_rst_n,
      O => \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_data_V_0_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4FFF4F4F"
    )
        port map (
      I0 => tmp_2_fu_389_p3,
      I1 => ap_CS_fsm_state4,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => INPUT_STREAM_TVALID,
      I4 => INPUT_STREAM_V_data_V_0_ack_in,
      O => INPUT_STREAM_V_data_V_0_state(1)
    );
\INPUT_STREAM_V_data_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_data_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_data_V_0_state(1),
      Q => INPUT_STREAM_V_data_V_0_ack_in,
      R => reset
    );
\INPUT_STREAM_V_dest_V_0_payload_A[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\,
      I1 => \^input_stream_tready\,
      I2 => INPUT_STREAM_V_dest_V_0_sel_wr,
      O => INPUT_STREAM_V_dest_V_0_load_A
    );
\INPUT_STREAM_V_dest_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_A,
      D => INPUT_STREAM_TDEST(0),
      Q => INPUT_STREAM_V_dest_V_0_payload_A(0),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_A,
      D => INPUT_STREAM_TDEST(1),
      Q => INPUT_STREAM_V_dest_V_0_payload_A(1),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_A,
      D => INPUT_STREAM_TDEST(2),
      Q => INPUT_STREAM_V_dest_V_0_payload_A(2),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_A,
      D => INPUT_STREAM_TDEST(3),
      Q => INPUT_STREAM_V_dest_V_0_payload_A(3),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_A,
      D => INPUT_STREAM_TDEST(4),
      Q => INPUT_STREAM_V_dest_V_0_payload_A(4),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_A,
      D => INPUT_STREAM_TDEST(5),
      Q => INPUT_STREAM_V_dest_V_0_payload_A(5),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_B[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\,
      I1 => \^input_stream_tready\,
      I2 => INPUT_STREAM_V_dest_V_0_sel_wr,
      O => INPUT_STREAM_V_dest_V_0_load_B
    );
\INPUT_STREAM_V_dest_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_B,
      D => INPUT_STREAM_TDEST(0),
      Q => INPUT_STREAM_V_dest_V_0_payload_B(0),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_B,
      D => INPUT_STREAM_TDEST(1),
      Q => INPUT_STREAM_V_dest_V_0_payload_B(1),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_B,
      D => INPUT_STREAM_TDEST(2),
      Q => INPUT_STREAM_V_dest_V_0_payload_B(2),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_B,
      D => INPUT_STREAM_TDEST(3),
      Q => INPUT_STREAM_V_dest_V_0_payload_B(3),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_B,
      D => INPUT_STREAM_TDEST(4),
      Q => INPUT_STREAM_V_dest_V_0_payload_B(4),
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_dest_V_0_load_B,
      D => INPUT_STREAM_TDEST(5),
      Q => INPUT_STREAM_V_dest_V_0_payload_B(5),
      R => '0'
    );
INPUT_STREAM_V_dest_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_V_dest_V_0_sel,
      O => INPUT_STREAM_V_dest_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_dest_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_dest_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_dest_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_dest_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => \^input_stream_tready\,
      I2 => INPUT_STREAM_V_dest_V_0_sel_wr,
      O => INPUT_STREAM_V_dest_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_dest_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_dest_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_dest_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_dest_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8B80000"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => \^input_stream_tready\,
      I2 => \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\,
      I3 => inputValues_6_U_n_4,
      I4 => ap_rst_n,
      O => \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_dest_V_0_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20FF20FF"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_TVALID,
      I5 => \^input_stream_tready\,
      O => INPUT_STREAM_V_dest_V_0_state(1)
    );
\INPUT_STREAM_V_dest_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_dest_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_dest_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_dest_V_0_state(1),
      Q => \^input_stream_tready\,
      R => reset
    );
\INPUT_STREAM_V_id_V_0_payload_A[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_id_V_0_ack_in,
      I2 => INPUT_STREAM_V_id_V_0_sel_wr,
      O => INPUT_STREAM_V_id_V_0_load_A
    );
\INPUT_STREAM_V_id_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_A,
      D => INPUT_STREAM_TID(0),
      Q => INPUT_STREAM_V_id_V_0_payload_A(0),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_A,
      D => INPUT_STREAM_TID(1),
      Q => INPUT_STREAM_V_id_V_0_payload_A(1),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_A,
      D => INPUT_STREAM_TID(2),
      Q => INPUT_STREAM_V_id_V_0_payload_A(2),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_A,
      D => INPUT_STREAM_TID(3),
      Q => INPUT_STREAM_V_id_V_0_payload_A(3),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_A,
      D => INPUT_STREAM_TID(4),
      Q => INPUT_STREAM_V_id_V_0_payload_A(4),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_B[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_id_V_0_ack_in,
      I2 => INPUT_STREAM_V_id_V_0_sel_wr,
      O => INPUT_STREAM_V_id_V_0_load_B
    );
\INPUT_STREAM_V_id_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_B,
      D => INPUT_STREAM_TID(0),
      Q => INPUT_STREAM_V_id_V_0_payload_B(0),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_B,
      D => INPUT_STREAM_TID(1),
      Q => INPUT_STREAM_V_id_V_0_payload_B(1),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_B,
      D => INPUT_STREAM_TID(2),
      Q => INPUT_STREAM_V_id_V_0_payload_B(2),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_B,
      D => INPUT_STREAM_TID(3),
      Q => INPUT_STREAM_V_id_V_0_payload_B(3),
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_id_V_0_load_B,
      D => INPUT_STREAM_TID(4),
      Q => INPUT_STREAM_V_id_V_0_payload_B(4),
      R => '0'
    );
INPUT_STREAM_V_id_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_V_id_V_0_sel,
      O => INPUT_STREAM_V_id_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_id_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_id_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_id_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_id_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_id_V_0_ack_in,
      I2 => INPUT_STREAM_V_id_V_0_sel_wr,
      O => INPUT_STREAM_V_id_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_id_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_id_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_id_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_id_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8B80000"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_id_V_0_ack_in,
      I2 => \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\,
      I3 => inputValues_6_U_n_4,
      I4 => ap_rst_n,
      O => \INPUT_STREAM_V_id_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_id_V_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20FF20FF"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_TVALID,
      I5 => INPUT_STREAM_V_id_V_0_ack_in,
      O => INPUT_STREAM_V_id_V_0_state(1)
    );
\INPUT_STREAM_V_id_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_id_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_id_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_id_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_id_V_0_state(1),
      Q => INPUT_STREAM_V_id_V_0_ack_in,
      R => reset
    );
\INPUT_STREAM_V_keep_V_0_payload_A[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_keep_V_0_ack_in,
      I2 => INPUT_STREAM_V_keep_V_0_sel_wr,
      O => INPUT_STREAM_V_keep_V_0_load_A
    );
\INPUT_STREAM_V_keep_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_A,
      D => INPUT_STREAM_TKEEP(0),
      Q => INPUT_STREAM_V_keep_V_0_payload_A(0),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_A,
      D => INPUT_STREAM_TKEEP(1),
      Q => INPUT_STREAM_V_keep_V_0_payload_A(1),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_A,
      D => INPUT_STREAM_TKEEP(2),
      Q => INPUT_STREAM_V_keep_V_0_payload_A(2),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_A,
      D => INPUT_STREAM_TKEEP(3),
      Q => INPUT_STREAM_V_keep_V_0_payload_A(3),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_B[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_keep_V_0_ack_in,
      I2 => INPUT_STREAM_V_keep_V_0_sel_wr,
      O => INPUT_STREAM_V_keep_V_0_load_B
    );
\INPUT_STREAM_V_keep_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_B,
      D => INPUT_STREAM_TKEEP(0),
      Q => INPUT_STREAM_V_keep_V_0_payload_B(0),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_B,
      D => INPUT_STREAM_TKEEP(1),
      Q => INPUT_STREAM_V_keep_V_0_payload_B(1),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_B,
      D => INPUT_STREAM_TKEEP(2),
      Q => INPUT_STREAM_V_keep_V_0_payload_B(2),
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_keep_V_0_load_B,
      D => INPUT_STREAM_TKEEP(3),
      Q => INPUT_STREAM_V_keep_V_0_payload_B(3),
      R => '0'
    );
INPUT_STREAM_V_keep_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_V_keep_V_0_sel,
      O => INPUT_STREAM_V_keep_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_keep_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_keep_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_keep_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_keep_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_keep_V_0_ack_in,
      I2 => INPUT_STREAM_V_keep_V_0_sel_wr,
      O => INPUT_STREAM_V_keep_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_keep_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_keep_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_keep_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_keep_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8B80000"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_keep_V_0_ack_in,
      I2 => \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\,
      I3 => inputValues_6_U_n_4,
      I4 => ap_rst_n,
      O => \INPUT_STREAM_V_keep_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_keep_V_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20FF20FF"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_TVALID,
      I5 => INPUT_STREAM_V_keep_V_0_ack_in,
      O => INPUT_STREAM_V_keep_V_0_state(1)
    );
\INPUT_STREAM_V_keep_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_keep_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_keep_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_keep_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_keep_V_0_state(1),
      Q => INPUT_STREAM_V_keep_V_0_ack_in,
      R => reset
    );
\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => INPUT_STREAM_TLAST(0),
      I1 => \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\,
      I2 => INPUT_STREAM_V_last_V_0_ack_in,
      I3 => INPUT_STREAM_V_last_V_0_sel_wr,
      I4 => INPUT_STREAM_V_last_V_0_payload_A,
      O => \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4\
    );
\INPUT_STREAM_V_last_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_4\,
      Q => INPUT_STREAM_V_last_V_0_payload_A,
      R => '0'
    );
\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => INPUT_STREAM_TLAST(0),
      I1 => \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\,
      I2 => INPUT_STREAM_V_last_V_0_ack_in,
      I3 => INPUT_STREAM_V_last_V_0_sel_wr,
      I4 => INPUT_STREAM_V_last_V_0_payload_B,
      O => \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4\
    );
\INPUT_STREAM_V_last_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_4\,
      Q => INPUT_STREAM_V_last_V_0_payload_B,
      R => '0'
    );
INPUT_STREAM_V_last_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_V_last_V_0_sel,
      O => INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_last_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_last_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_last_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_last_V_0_ack_in,
      I2 => INPUT_STREAM_V_last_V_0_sel_wr,
      O => INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_last_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_last_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_last_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8B80000"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_last_V_0_ack_in,
      I2 => \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\,
      I3 => inputValues_6_U_n_4,
      I4 => ap_rst_n,
      O => \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_last_V_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20FF20FF"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_TVALID,
      I5 => INPUT_STREAM_V_last_V_0_ack_in,
      O => INPUT_STREAM_V_last_V_0_state(1)
    );
\INPUT_STREAM_V_last_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_last_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_last_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_last_V_0_state(1),
      Q => INPUT_STREAM_V_last_V_0_ack_in,
      R => reset
    );
\INPUT_STREAM_V_strb_V_0_payload_A[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_strb_V_0_ack_in,
      I2 => INPUT_STREAM_V_strb_V_0_sel_wr,
      O => INPUT_STREAM_V_strb_V_0_load_A
    );
\INPUT_STREAM_V_strb_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_A,
      D => INPUT_STREAM_TSTRB(0),
      Q => INPUT_STREAM_V_strb_V_0_payload_A(0),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_A,
      D => INPUT_STREAM_TSTRB(1),
      Q => INPUT_STREAM_V_strb_V_0_payload_A(1),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_A,
      D => INPUT_STREAM_TSTRB(2),
      Q => INPUT_STREAM_V_strb_V_0_payload_A(2),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_A,
      D => INPUT_STREAM_TSTRB(3),
      Q => INPUT_STREAM_V_strb_V_0_payload_A(3),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_B[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\,
      I1 => INPUT_STREAM_V_strb_V_0_ack_in,
      I2 => INPUT_STREAM_V_strb_V_0_sel_wr,
      O => INPUT_STREAM_V_strb_V_0_load_B
    );
\INPUT_STREAM_V_strb_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_B,
      D => INPUT_STREAM_TSTRB(0),
      Q => INPUT_STREAM_V_strb_V_0_payload_B(0),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_B,
      D => INPUT_STREAM_TSTRB(1),
      Q => INPUT_STREAM_V_strb_V_0_payload_B(1),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_B,
      D => INPUT_STREAM_TSTRB(2),
      Q => INPUT_STREAM_V_strb_V_0_payload_B(2),
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => INPUT_STREAM_V_strb_V_0_load_B,
      D => INPUT_STREAM_TSTRB(3),
      Q => INPUT_STREAM_V_strb_V_0_payload_B(3),
      R => '0'
    );
INPUT_STREAM_V_strb_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_V_strb_V_0_sel,
      O => INPUT_STREAM_V_strb_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_strb_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_strb_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_strb_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_strb_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_strb_V_0_ack_in,
      I2 => INPUT_STREAM_V_strb_V_0_sel_wr,
      O => INPUT_STREAM_V_strb_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_strb_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_strb_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_strb_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_strb_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8B80000"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_strb_V_0_ack_in,
      I2 => \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\,
      I3 => inputValues_6_U_n_4,
      I4 => ap_rst_n,
      O => \INPUT_STREAM_V_strb_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_strb_V_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20FF20FF"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_TVALID,
      I5 => INPUT_STREAM_V_strb_V_0_ack_in,
      O => INPUT_STREAM_V_strb_V_0_state(1)
    );
\INPUT_STREAM_V_strb_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_strb_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_strb_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_strb_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_strb_V_0_state(1),
      Q => INPUT_STREAM_V_strb_V_0_ack_in,
      R => reset
    );
\INPUT_STREAM_V_user_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => INPUT_STREAM_TUSER(0),
      I1 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I2 => INPUT_STREAM_V_user_V_0_ack_in,
      I3 => INPUT_STREAM_V_user_V_0_sel_wr,
      I4 => INPUT_STREAM_V_user_V_0_payload_A(0),
      O => \INPUT_STREAM_V_user_V_0_payload_A[0]_i_1_n_4\
    );
\INPUT_STREAM_V_user_V_0_payload_A[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => INPUT_STREAM_TUSER(1),
      I1 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I2 => INPUT_STREAM_V_user_V_0_ack_in,
      I3 => INPUT_STREAM_V_user_V_0_sel_wr,
      I4 => INPUT_STREAM_V_user_V_0_payload_A(1),
      O => \INPUT_STREAM_V_user_V_0_payload_A[1]_i_1_n_4\
    );
\INPUT_STREAM_V_user_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_user_V_0_payload_A[0]_i_1_n_4\,
      Q => INPUT_STREAM_V_user_V_0_payload_A(0),
      R => '0'
    );
\INPUT_STREAM_V_user_V_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_user_V_0_payload_A[1]_i_1_n_4\,
      Q => INPUT_STREAM_V_user_V_0_payload_A(1),
      R => '0'
    );
\INPUT_STREAM_V_user_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => INPUT_STREAM_TUSER(0),
      I1 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I2 => INPUT_STREAM_V_user_V_0_ack_in,
      I3 => INPUT_STREAM_V_user_V_0_sel_wr,
      I4 => INPUT_STREAM_V_user_V_0_payload_B(0),
      O => \INPUT_STREAM_V_user_V_0_payload_B[0]_i_1_n_4\
    );
\INPUT_STREAM_V_user_V_0_payload_B[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => INPUT_STREAM_TUSER(1),
      I1 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I2 => INPUT_STREAM_V_user_V_0_ack_in,
      I3 => INPUT_STREAM_V_user_V_0_sel_wr,
      I4 => INPUT_STREAM_V_user_V_0_payload_B(1),
      O => \INPUT_STREAM_V_user_V_0_payload_B[1]_i_1_n_4\
    );
\INPUT_STREAM_V_user_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_user_V_0_payload_B[0]_i_1_n_4\,
      Q => INPUT_STREAM_V_user_V_0_payload_B(0),
      R => '0'
    );
\INPUT_STREAM_V_user_V_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_user_V_0_payload_B[1]_i_1_n_4\,
      Q => INPUT_STREAM_V_user_V_0_payload_B(1),
      R => '0'
    );
INPUT_STREAM_V_user_V_0_sel_rd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_V_user_V_0_sel,
      O => INPUT_STREAM_V_user_V_0_sel_rd_i_1_n_4
    );
INPUT_STREAM_V_user_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_user_V_0_sel_rd_i_1_n_4,
      Q => INPUT_STREAM_V_user_V_0_sel,
      R => reset
    );
INPUT_STREAM_V_user_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_user_V_0_ack_in,
      I2 => INPUT_STREAM_V_user_V_0_sel_wr,
      O => INPUT_STREAM_V_user_V_0_sel_wr_i_1_n_4
    );
INPUT_STREAM_V_user_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_user_V_0_sel_wr_i_1_n_4,
      Q => INPUT_STREAM_V_user_V_0_sel_wr,
      R => reset
    );
\INPUT_STREAM_V_user_V_0_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8B80000"
    )
        port map (
      I0 => INPUT_STREAM_TVALID,
      I1 => INPUT_STREAM_V_user_V_0_ack_in,
      I2 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I3 => inputValues_6_U_n_4,
      I4 => ap_rst_n,
      O => \INPUT_STREAM_V_user_V_0_state[0]_i_1_n_4\
    );
\INPUT_STREAM_V_user_V_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20FF20FF"
    )
        port map (
      I0 => ap_CS_fsm_state4,
      I1 => tmp_2_fu_389_p3,
      I2 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I3 => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      I4 => INPUT_STREAM_TVALID,
      I5 => INPUT_STREAM_V_user_V_0_ack_in,
      O => INPUT_STREAM_V_user_V_0_state(1)
    );
\INPUT_STREAM_V_user_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \INPUT_STREAM_V_user_V_0_state[0]_i_1_n_4\,
      Q => \INPUT_STREAM_V_user_V_0_state_reg_n_4_[0]\,
      R => '0'
    );
\INPUT_STREAM_V_user_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => INPUT_STREAM_V_user_V_0_state(1),
      Q => INPUT_STREAM_V_user_V_0_ack_in,
      R => reset
    );
\OUTPUT_STREAM_TDATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(0),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(0),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(0)
    );
\OUTPUT_STREAM_TDATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(10),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(10),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(10)
    );
\OUTPUT_STREAM_TDATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(11),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(11),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(11)
    );
\OUTPUT_STREAM_TDATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(12),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(12),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(12)
    );
\OUTPUT_STREAM_TDATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(13),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(13),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(13)
    );
\OUTPUT_STREAM_TDATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(14),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(14),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(14)
    );
\OUTPUT_STREAM_TDATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(15),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(15),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(15)
    );
\OUTPUT_STREAM_TDATA[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(16),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(16),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(16)
    );
\OUTPUT_STREAM_TDATA[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(17),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(17),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(17)
    );
\OUTPUT_STREAM_TDATA[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(18),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(18),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(18)
    );
\OUTPUT_STREAM_TDATA[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(19),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(19),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(19)
    );
\OUTPUT_STREAM_TDATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(1),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(1),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(1)
    );
\OUTPUT_STREAM_TDATA[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(20),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(20),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(20)
    );
\OUTPUT_STREAM_TDATA[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(21),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(21),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(21)
    );
\OUTPUT_STREAM_TDATA[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(22),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(22),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(22)
    );
\OUTPUT_STREAM_TDATA[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(23),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(23),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(23)
    );
\OUTPUT_STREAM_TDATA[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(24),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(24),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(24)
    );
\OUTPUT_STREAM_TDATA[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(25),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(25),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(25)
    );
\OUTPUT_STREAM_TDATA[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(26),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(26),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(26)
    );
\OUTPUT_STREAM_TDATA[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(27),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(27),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(27)
    );
\OUTPUT_STREAM_TDATA[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(28),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(28),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(28)
    );
\OUTPUT_STREAM_TDATA[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(29),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(29),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(29)
    );
\OUTPUT_STREAM_TDATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(2),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(2),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(2)
    );
\OUTPUT_STREAM_TDATA[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(30),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(30),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(30)
    );
\OUTPUT_STREAM_TDATA[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(31),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(31),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(31)
    );
\OUTPUT_STREAM_TDATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(3),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(3),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(3)
    );
\OUTPUT_STREAM_TDATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(4),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(4),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(4)
    );
\OUTPUT_STREAM_TDATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(5),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(5),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(5)
    );
\OUTPUT_STREAM_TDATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(6),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(6),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(6)
    );
\OUTPUT_STREAM_TDATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(7),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(7),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(7)
    );
\OUTPUT_STREAM_TDATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(8),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(8),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(8)
    );
\OUTPUT_STREAM_TDATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_payload_B(9),
      I1 => OUTPUT_STREAM_V_data_V_1_payload_A(9),
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_TDATA(9)
    );
\OUTPUT_STREAM_TDEST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_dest_V_1_payload_B(0),
      I1 => OUTPUT_STREAM_V_dest_V_1_sel,
      I2 => OUTPUT_STREAM_V_dest_V_1_payload_A(0),
      O => OUTPUT_STREAM_TDEST(0)
    );
\OUTPUT_STREAM_TDEST[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_dest_V_1_payload_B(1),
      I1 => OUTPUT_STREAM_V_dest_V_1_sel,
      I2 => OUTPUT_STREAM_V_dest_V_1_payload_A(1),
      O => OUTPUT_STREAM_TDEST(1)
    );
\OUTPUT_STREAM_TDEST[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_dest_V_1_payload_B(2),
      I1 => OUTPUT_STREAM_V_dest_V_1_sel,
      I2 => OUTPUT_STREAM_V_dest_V_1_payload_A(2),
      O => OUTPUT_STREAM_TDEST(2)
    );
\OUTPUT_STREAM_TDEST[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_dest_V_1_payload_B(3),
      I1 => OUTPUT_STREAM_V_dest_V_1_sel,
      I2 => OUTPUT_STREAM_V_dest_V_1_payload_A(3),
      O => OUTPUT_STREAM_TDEST(3)
    );
\OUTPUT_STREAM_TDEST[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_dest_V_1_payload_B(4),
      I1 => OUTPUT_STREAM_V_dest_V_1_sel,
      I2 => OUTPUT_STREAM_V_dest_V_1_payload_A(4),
      O => OUTPUT_STREAM_TDEST(4)
    );
\OUTPUT_STREAM_TDEST[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_dest_V_1_payload_B(5),
      I1 => OUTPUT_STREAM_V_dest_V_1_sel,
      I2 => OUTPUT_STREAM_V_dest_V_1_payload_A(5),
      O => OUTPUT_STREAM_TDEST(5)
    );
\OUTPUT_STREAM_TID[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_id_V_1_payload_B(0),
      I1 => OUTPUT_STREAM_V_id_V_1_sel,
      I2 => OUTPUT_STREAM_V_id_V_1_payload_A(0),
      O => OUTPUT_STREAM_TID(0)
    );
\OUTPUT_STREAM_TID[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_id_V_1_payload_B(1),
      I1 => OUTPUT_STREAM_V_id_V_1_sel,
      I2 => OUTPUT_STREAM_V_id_V_1_payload_A(1),
      O => OUTPUT_STREAM_TID(1)
    );
\OUTPUT_STREAM_TID[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_id_V_1_payload_B(2),
      I1 => OUTPUT_STREAM_V_id_V_1_sel,
      I2 => OUTPUT_STREAM_V_id_V_1_payload_A(2),
      O => OUTPUT_STREAM_TID(2)
    );
\OUTPUT_STREAM_TID[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_id_V_1_payload_B(3),
      I1 => OUTPUT_STREAM_V_id_V_1_sel,
      I2 => OUTPUT_STREAM_V_id_V_1_payload_A(3),
      O => OUTPUT_STREAM_TID(3)
    );
\OUTPUT_STREAM_TID[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_id_V_1_payload_B(4),
      I1 => OUTPUT_STREAM_V_id_V_1_sel,
      I2 => OUTPUT_STREAM_V_id_V_1_payload_A(4),
      O => OUTPUT_STREAM_TID(4)
    );
\OUTPUT_STREAM_TKEEP[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_keep_V_1_payload_B(0),
      I1 => OUTPUT_STREAM_V_keep_V_1_sel,
      I2 => OUTPUT_STREAM_V_keep_V_1_payload_A(0),
      O => OUTPUT_STREAM_TKEEP(0)
    );
\OUTPUT_STREAM_TKEEP[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_keep_V_1_payload_B(1),
      I1 => OUTPUT_STREAM_V_keep_V_1_sel,
      I2 => OUTPUT_STREAM_V_keep_V_1_payload_A(1),
      O => OUTPUT_STREAM_TKEEP(1)
    );
\OUTPUT_STREAM_TKEEP[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_keep_V_1_payload_B(2),
      I1 => OUTPUT_STREAM_V_keep_V_1_sel,
      I2 => OUTPUT_STREAM_V_keep_V_1_payload_A(2),
      O => OUTPUT_STREAM_TKEEP(2)
    );
\OUTPUT_STREAM_TKEEP[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_keep_V_1_payload_B(3),
      I1 => OUTPUT_STREAM_V_keep_V_1_sel,
      I2 => OUTPUT_STREAM_V_keep_V_1_payload_A(3),
      O => OUTPUT_STREAM_TKEEP(3)
    );
\OUTPUT_STREAM_TLAST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_last_V_1_payload_B,
      I1 => OUTPUT_STREAM_V_last_V_1_sel,
      I2 => OUTPUT_STREAM_V_last_V_1_payload_A,
      O => OUTPUT_STREAM_TLAST(0)
    );
\OUTPUT_STREAM_TSTRB[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_strb_V_1_payload_B(0),
      I1 => OUTPUT_STREAM_V_strb_V_1_sel,
      I2 => OUTPUT_STREAM_V_strb_V_1_payload_A(0),
      O => OUTPUT_STREAM_TSTRB(0)
    );
\OUTPUT_STREAM_TSTRB[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_strb_V_1_payload_B(1),
      I1 => OUTPUT_STREAM_V_strb_V_1_sel,
      I2 => OUTPUT_STREAM_V_strb_V_1_payload_A(1),
      O => OUTPUT_STREAM_TSTRB(1)
    );
\OUTPUT_STREAM_TSTRB[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_strb_V_1_payload_B(2),
      I1 => OUTPUT_STREAM_V_strb_V_1_sel,
      I2 => OUTPUT_STREAM_V_strb_V_1_payload_A(2),
      O => OUTPUT_STREAM_TSTRB(2)
    );
\OUTPUT_STREAM_TSTRB[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_strb_V_1_payload_B(3),
      I1 => OUTPUT_STREAM_V_strb_V_1_sel,
      I2 => OUTPUT_STREAM_V_strb_V_1_payload_A(3),
      O => OUTPUT_STREAM_TSTRB(3)
    );
\OUTPUT_STREAM_TUSER[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_user_V_1_payload_B(0),
      I1 => OUTPUT_STREAM_V_user_V_1_sel,
      I2 => OUTPUT_STREAM_V_user_V_1_payload_A(0),
      O => OUTPUT_STREAM_TUSER(0)
    );
\OUTPUT_STREAM_TUSER[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => OUTPUT_STREAM_V_user_V_1_payload_B(1),
      I1 => OUTPUT_STREAM_V_user_V_1_sel,
      I2 => OUTPUT_STREAM_V_user_V_1_payload_A(1),
      O => OUTPUT_STREAM_TUSER(1)
    );
\OUTPUT_STREAM_V_data_V_1_payload_A[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_data_V_1_sel_wr,
      O => OUTPUT_STREAM_V_data_V_1_load_A
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(0),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(0),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(10),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(10),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(11),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(11),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(12),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(12),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(13),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(13),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(14),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(14),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(15),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(15),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(16),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(16),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(17),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(17),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(18),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(18),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(19),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(19),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(1),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(1),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(20),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(20),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(21),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(21),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(22),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(22),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(23),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(23),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(24),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(24),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(25),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(25),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(26),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(26),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(27),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(27),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(28),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(28),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(29),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(29),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(2),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(2),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(30),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(30),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(31),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(31),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(3),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(3),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(4),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(4),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(5),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(5),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(6),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(6),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(7),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(7),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(8),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(8),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_A,
      D => tmp_data_V_1_fu_472_p2(9),
      Q => OUTPUT_STREAM_V_data_V_1_payload_A(9),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_data_V_1_sel_wr,
      O => OUTPUT_STREAM_V_data_V_1_load_B
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(0),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(0),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(10),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(10),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(11),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(11),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(12),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(12),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(13),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(13),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(14),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(14),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(15),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(15),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(16),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(16),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(17),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(17),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(18),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(18),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(19),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(19),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(1),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(1),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(20),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(20),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(21),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(21),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(22),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(22),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(23),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(23),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(24),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(24),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(25),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(25),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(26),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(26),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(27),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(27),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(28),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(28),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(29),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(29),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(2),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(2),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(30),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(30),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(31),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(31),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(3),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(3),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(4),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(4),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(5),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(5),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(6),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(6),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(7),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(7),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(8),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(8),
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_data_V_1_load_B,
      D => tmp_data_V_1_fu_472_p2(9),
      Q => OUTPUT_STREAM_V_data_V_1_payload_B(9),
      R => '0'
    );
OUTPUT_STREAM_V_data_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => OUTPUT_STREAM_TREADY,
      I1 => \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\,
      I2 => OUTPUT_STREAM_V_data_V_1_sel,
      O => OUTPUT_STREAM_V_data_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_data_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_data_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_data_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_data_V_1_sel_wr_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7FF0800"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I1 => ap_enable_reg_pp1_iter1_reg_n_4,
      I2 => \exitcond_reg_509_reg_n_4_[0]\,
      I3 => ap_CS_fsm_pp1_stage0,
      I4 => OUTPUT_STREAM_V_data_V_1_sel_wr,
      O => OUTPUT_STREAM_V_data_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_data_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_data_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_data_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_data_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F550000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_data_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_data_V_1_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFDDDFDFDFDFDFD"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I3 => ap_enable_reg_pp1_iter1_reg_n_4,
      I4 => \exitcond_reg_509_reg_n_4_[0]\,
      I5 => ap_CS_fsm_pp1_stage0,
      O => OUTPUT_STREAM_V_data_V_1_state(1)
    );
\OUTPUT_STREAM_V_data_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_data_V_1_state[0]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_data_V_1_state_reg_n_4_[0]\,
      R => '0'
    );
\OUTPUT_STREAM_V_data_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_data_V_1_state(1),
      Q => OUTPUT_STREAM_V_data_V_1_ack_in,
      R => reset
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \^output_stream_tvalid\,
      I1 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_dest_V_1_sel_wr,
      O => OUTPUT_STREAM_V_dest_V_1_load_A
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_A,
      D => inputValues_6_U_n_21,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_A(0),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_A,
      D => inputValues_6_U_n_20,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_A(1),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_A,
      D => inputValues_6_U_n_19,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_A(2),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_A,
      D => inputValues_6_U_n_18,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_A(3),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_A,
      D => inputValues_6_U_n_17,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_A(4),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_A,
      D => inputValues_6_U_n_16,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_A(5),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \^output_stream_tvalid\,
      I1 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_dest_V_1_sel_wr,
      O => OUTPUT_STREAM_V_dest_V_1_load_B
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_B,
      D => inputValues_6_U_n_21,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_B(0),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_B,
      D => inputValues_6_U_n_20,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_B(1),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_B,
      D => inputValues_6_U_n_19,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_B(2),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_B,
      D => inputValues_6_U_n_18,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_B(3),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_B,
      D => inputValues_6_U_n_17,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_B(4),
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_dest_V_1_load_B,
      D => inputValues_6_U_n_16,
      Q => OUTPUT_STREAM_V_dest_V_1_payload_B(5),
      R => '0'
    );
OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => OUTPUT_STREAM_TREADY,
      I1 => \^output_stream_tvalid\,
      I2 => OUTPUT_STREAM_V_dest_V_1_sel,
      O => OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_dest_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_dest_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_dest_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFF20000000"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I5 => OUTPUT_STREAM_V_dest_V_1_sel_wr,
      O => OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_dest_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_dest_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_dest_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_dest_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F500000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I3 => \^output_stream_tvalid\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_dest_V_1_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      O => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\
    );
\OUTPUT_STREAM_V_dest_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDD"
    )
        port map (
      I0 => \^output_stream_tvalid\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      O => OUTPUT_STREAM_V_dest_V_1_state(1)
    );
\OUTPUT_STREAM_V_dest_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_1_n_4\,
      Q => \^output_stream_tvalid\,
      R => '0'
    );
\OUTPUT_STREAM_V_dest_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_dest_V_1_state(1),
      Q => OUTPUT_STREAM_V_dest_V_1_ack_in,
      R => reset
    );
\OUTPUT_STREAM_V_id_V_1_payload_A[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_id_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_id_V_1_sel_wr,
      O => OUTPUT_STREAM_V_id_V_1_load_A
    );
\OUTPUT_STREAM_V_id_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_A,
      D => inputValues_5_U_n_8,
      Q => OUTPUT_STREAM_V_id_V_1_payload_A(0),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_A,
      D => inputValues_5_U_n_7,
      Q => OUTPUT_STREAM_V_id_V_1_payload_A(1),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_A,
      D => inputValues_5_U_n_6,
      Q => OUTPUT_STREAM_V_id_V_1_payload_A(2),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_A,
      D => inputValues_5_U_n_5,
      Q => OUTPUT_STREAM_V_id_V_1_payload_A(3),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_A,
      D => inputValues_5_U_n_4,
      Q => OUTPUT_STREAM_V_id_V_1_payload_A(4),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_B[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_id_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_id_V_1_sel_wr,
      O => OUTPUT_STREAM_V_id_V_1_load_B
    );
\OUTPUT_STREAM_V_id_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_B,
      D => inputValues_5_U_n_8,
      Q => OUTPUT_STREAM_V_id_V_1_payload_B(0),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_B,
      D => inputValues_5_U_n_7,
      Q => OUTPUT_STREAM_V_id_V_1_payload_B(1),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_B,
      D => inputValues_5_U_n_6,
      Q => OUTPUT_STREAM_V_id_V_1_payload_B(2),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_B,
      D => inputValues_5_U_n_5,
      Q => OUTPUT_STREAM_V_id_V_1_payload_B(3),
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_id_V_1_load_B,
      D => inputValues_5_U_n_4,
      Q => OUTPUT_STREAM_V_id_V_1_payload_B(4),
      R => '0'
    );
OUTPUT_STREAM_V_id_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_id_V_1_sel,
      O => OUTPUT_STREAM_V_id_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_id_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_id_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_id_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_id_V_1_sel_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFF20000000"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_id_V_1_ack_in,
      I5 => OUTPUT_STREAM_V_id_V_1_sel_wr,
      O => OUTPUT_STREAM_V_id_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_id_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_id_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_id_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_id_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F500000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_id_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_id_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_id_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDD"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_id_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      O => OUTPUT_STREAM_V_id_V_1_state(1)
    );
\OUTPUT_STREAM_V_id_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_id_V_1_state[0]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_id_V_1_state_reg_n_4_[0]\,
      R => '0'
    );
\OUTPUT_STREAM_V_id_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_id_V_1_state(1),
      Q => OUTPUT_STREAM_V_id_V_1_ack_in,
      R => reset
    );
\OUTPUT_STREAM_V_keep_V_1_payload_A[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_keep_V_1_sel_wr,
      O => OUTPUT_STREAM_V_keep_V_1_load_A
    );
\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_A,
      D => inputValues_1_U_n_7,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_A(0),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_A,
      D => inputValues_1_U_n_6,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_A(1),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_A,
      D => inputValues_1_U_n_5,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_A(2),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_A,
      D => inputValues_1_U_n_4,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_A(3),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_B[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_keep_V_1_sel_wr,
      O => OUTPUT_STREAM_V_keep_V_1_load_B
    );
\OUTPUT_STREAM_V_keep_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_B,
      D => inputValues_1_U_n_7,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_B(0),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_B,
      D => inputValues_1_U_n_6,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_B(1),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_B,
      D => inputValues_1_U_n_5,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_B(2),
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_keep_V_1_load_B,
      D => inputValues_1_U_n_4,
      Q => OUTPUT_STREAM_V_keep_V_1_payload_B(3),
      R => '0'
    );
OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_keep_V_1_sel,
      O => OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_keep_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_keep_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_keep_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFF20000000"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I5 => OUTPUT_STREAM_V_keep_V_1_sel_wr,
      O => OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_keep_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_keep_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_keep_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_keep_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F500000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_keep_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_keep_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDD"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      O => OUTPUT_STREAM_V_keep_V_1_state(1)
    );
\OUTPUT_STREAM_V_keep_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_keep_V_1_state[0]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_keep_V_1_state_reg_n_4_[0]\,
      R => '0'
    );
\OUTPUT_STREAM_V_keep_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_keep_V_1_state(1),
      Q => OUTPUT_STREAM_V_keep_V_1_ack_in,
      R => reset
    );
\OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => inputValues_4_U_n_5,
      Q => OUTPUT_STREAM_V_last_V_1_payload_A,
      R => '0'
    );
\OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => inputValues_4_U_n_6,
      Q => OUTPUT_STREAM_V_last_V_1_payload_B,
      R => '0'
    );
OUTPUT_STREAM_V_last_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_last_V_1_sel,
      O => OUTPUT_STREAM_V_last_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_last_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_last_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_last_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_last_V_1_sel_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFF20000000"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_last_V_1_ack_in,
      I5 => OUTPUT_STREAM_V_last_V_1_sel_wr,
      O => OUTPUT_STREAM_V_last_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_last_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_last_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_last_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_last_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F500000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_last_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0]\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_last_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_last_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDD"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_last_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      O => OUTPUT_STREAM_V_last_V_1_state(1)
    );
\OUTPUT_STREAM_V_last_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_last_V_1_state[0]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0]\,
      R => '0'
    );
\OUTPUT_STREAM_V_last_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_last_V_1_state(1),
      Q => OUTPUT_STREAM_V_last_V_1_ack_in,
      R => reset
    );
\OUTPUT_STREAM_V_strb_V_1_payload_A[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_strb_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_strb_V_1_sel_wr,
      O => OUTPUT_STREAM_V_strb_V_1_load_A
    );
\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_A,
      D => inputValues_2_U_n_7,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_A(0),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_A,
      D => inputValues_2_U_n_6,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_A(1),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_A,
      D => inputValues_2_U_n_5,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_A(2),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_A,
      D => inputValues_2_U_n_4,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_A(3),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_B[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_V_strb_V_1_ack_in,
      I2 => OUTPUT_STREAM_V_strb_V_1_sel_wr,
      O => OUTPUT_STREAM_V_strb_V_1_load_B
    );
\OUTPUT_STREAM_V_strb_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_B,
      D => inputValues_2_U_n_7,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_B(0),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_B,
      D => inputValues_2_U_n_6,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_B(1),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_B,
      D => inputValues_2_U_n_5,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_B(2),
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => OUTPUT_STREAM_V_strb_V_1_load_B,
      D => inputValues_2_U_n_4,
      Q => OUTPUT_STREAM_V_strb_V_1_payload_B(3),
      R => '0'
    );
OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_strb_V_1_sel,
      O => OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_strb_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_strb_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_strb_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFF20000000"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_strb_V_1_ack_in,
      I5 => OUTPUT_STREAM_V_strb_V_1_sel_wr,
      O => OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_strb_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_strb_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_strb_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_strb_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F500000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_strb_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_strb_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_strb_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDD"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_strb_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      O => OUTPUT_STREAM_V_strb_V_1_state(1)
    );
\OUTPUT_STREAM_V_strb_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_strb_V_1_state[0]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_strb_V_1_state_reg_n_4_[0]\,
      R => '0'
    );
\OUTPUT_STREAM_V_strb_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_strb_V_1_state(1),
      Q => OUTPUT_STREAM_V_strb_V_1_ack_in,
      R => reset
    );
\OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => inputValues_3_U_n_5,
      Q => OUTPUT_STREAM_V_user_V_1_payload_A(0),
      R => '0'
    );
\OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => inputValues_3_U_n_4,
      Q => OUTPUT_STREAM_V_user_V_1_payload_A(1),
      R => '0'
    );
\OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => inputValues_3_U_n_7,
      Q => OUTPUT_STREAM_V_user_V_1_payload_B(0),
      R => '0'
    );
\OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => inputValues_3_U_n_6,
      Q => OUTPUT_STREAM_V_user_V_1_payload_B(1),
      R => '0'
    );
OUTPUT_STREAM_V_user_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => OUTPUT_STREAM_TREADY,
      I1 => \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0]\,
      I2 => OUTPUT_STREAM_V_user_V_1_sel,
      O => OUTPUT_STREAM_V_user_V_1_sel_rd_i_1_n_4
    );
OUTPUT_STREAM_V_user_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_user_V_1_sel_rd_i_1_n_4,
      Q => OUTPUT_STREAM_V_user_V_1_sel,
      R => reset
    );
OUTPUT_STREAM_V_user_V_1_sel_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFFFFF20000000"
    )
        port map (
      I0 => ap_CS_fsm_pp1_stage0,
      I1 => \exitcond_reg_509_reg_n_4_[0]\,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I5 => OUTPUT_STREAM_V_user_V_1_sel_wr,
      O => OUTPUT_STREAM_V_user_V_1_sel_wr_i_1_n_4
    );
OUTPUT_STREAM_V_user_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_user_V_1_sel_wr_i_1_n_4,
      Q => OUTPUT_STREAM_V_user_V_1_sel_wr,
      R => reset
    );
\OUTPUT_STREAM_V_user_V_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7F500000"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0]\,
      I4 => ap_rst_n,
      O => \OUTPUT_STREAM_V_user_V_1_state[0]_i_1_n_4\
    );
\OUTPUT_STREAM_V_user_V_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDD"
    )
        port map (
      I0 => \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0]\,
      I1 => OUTPUT_STREAM_TREADY,
      I2 => OUTPUT_STREAM_V_user_V_1_ack_in,
      I3 => \OUTPUT_STREAM_V_dest_V_1_state[0]_i_2_n_4\,
      O => OUTPUT_STREAM_V_user_V_1_state(1)
    );
\OUTPUT_STREAM_V_user_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \OUTPUT_STREAM_V_user_V_1_state[0]_i_1_n_4\,
      Q => \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0]\,
      R => '0'
    );
\OUTPUT_STREAM_V_user_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => OUTPUT_STREAM_V_user_V_1_state(1),
      Q => OUTPUT_STREAM_V_user_V_1_ack_in,
      R => reset
    );
\ap_CS_fsm[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888F888F888F88"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => \tmp_1_reg_494_reg_n_4_[0]\,
      I2 => tmp_2_fu_389_p3,
      I3 => ap_CS_fsm_state4,
      I4 => d0,
      I5 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAEAAAA00000000"
    )
        port map (
      I0 => tmp_2_fu_389_p3,
      I1 => INPUT_STREAM_V_last_V_0_payload_A,
      I2 => INPUT_STREAM_V_last_V_0_sel,
      I3 => INPUT_STREAM_V_last_V_0_payload_B,
      I4 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I5 => ap_CS_fsm_state4,
      O => ap_NS_fsm(4)
    );
\ap_CS_fsm[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF4404"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter2_reg_n_4,
      I1 => ap_CS_fsm_pp1_stage0,
      I2 => ap_enable_reg_pp1_iter0,
      I3 => \ap_CS_fsm[6]_i_2_n_4\,
      I4 => \ap_CS_fsm[5]_i_2_n_4\,
      I5 => ap_CS_fsm_state5,
      O => ap_NS_fsm(5)
    );
\ap_CS_fsm[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00BA00"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter1_reg_n_4,
      I1 => ap_reg_pp1_iter1_exitcond_reg_509,
      I2 => ap_enable_reg_pp1_iter2_reg_n_4,
      I3 => ap_CS_fsm_pp1_stage0,
      I4 => OUTPUT_STREAM_V_data_V_1_ack_in,
      O => \ap_CS_fsm[5]_i_2_n_4\
    );
\ap_CS_fsm[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF01000000"
    )
        port map (
      I0 => \ap_CS_fsm[6]_i_2_n_4\,
      I1 => ap_enable_reg_pp1_iter1_reg_n_4,
      I2 => ap_enable_reg_pp1_iter2_reg_n_4,
      I3 => ap_CS_fsm_pp1_stage0,
      I4 => ap_enable_reg_pp1_iter0,
      I5 => \ap_CS_fsm[6]_i_3_n_4\,
      O => ap_NS_fsm(6)
    );
\ap_CS_fsm[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(4),
      I1 => \i_1_reg_356_reg__0\(5),
      I2 => \i_1_reg_356_reg__0\(2),
      I3 => \i_1_reg_356_reg__0\(3),
      I4 => \i_1_reg_356_reg__0\(1),
      I5 => \i_1_reg_356_reg__0\(0),
      O => \ap_CS_fsm[6]_i_2_n_4\
    );
\ap_CS_fsm[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08080800"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter2_reg_n_4,
      I1 => ap_CS_fsm_pp1_stage0,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I4 => ap_reg_pp1_iter1_exitcond_reg_509,
      O => \ap_CS_fsm[6]_i_3_n_4\
    );
\ap_CS_fsm[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF8AAAAAAA"
    )
        port map (
      I0 => ap_CS_fsm_state10,
      I1 => Adder2_CONTROL_BUS_s_axi_U_n_12,
      I2 => OUTPUT_STREAM_V_dest_V_1_ack_in,
      I3 => OUTPUT_STREAM_V_keep_V_1_ack_in,
      I4 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I5 => ap_CS_fsm_state9,
      O => ap_NS_fsm(7)
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_4_[0]\,
      S => reset
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ce0,
      R => reset
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ce0,
      Q => ap_CS_fsm_state3,
      R => reset
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_state4,
      R => reset
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(4),
      Q => ap_CS_fsm_state5,
      R => reset
    );
\ap_CS_fsm_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(5),
      Q => ap_CS_fsm_pp1_stage0,
      R => reset
    );
\ap_CS_fsm_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(6),
      Q => ap_CS_fsm_state9,
      R => reset
    );
\ap_CS_fsm_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(7),
      Q => ap_CS_fsm_state10,
      R => reset
    );
ap_enable_reg_pp1_iter0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8A8A8A800A8A8A8"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_CS_fsm_state5,
      I2 => ap_enable_reg_pp1_iter0,
      I3 => ap_CS_fsm_pp1_stage0,
      I4 => ap_block_pp1_stage0_subdone1_in,
      I5 => \ap_CS_fsm[6]_i_2_n_4\,
      O => ap_enable_reg_pp1_iter0_i_1_n_4
    );
ap_enable_reg_pp1_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter0_i_1_n_4,
      Q => ap_enable_reg_pp1_iter0,
      R => '0'
    );
ap_enable_reg_pp1_iter1_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8800A0A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_enable_reg_pp1_iter0,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => \ap_CS_fsm[6]_i_2_n_4\,
      I4 => ap_block_pp1_stage0_subdone1_in,
      O => ap_enable_reg_pp1_iter1_i_1_n_4
    );
ap_enable_reg_pp1_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter1_i_1_n_4,
      Q => ap_enable_reg_pp1_iter1_reg_n_4,
      R => '0'
    );
ap_enable_reg_pp1_iter2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"888800A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_enable_reg_pp1_iter1_reg_n_4,
      I2 => ap_enable_reg_pp1_iter2_reg_n_4,
      I3 => ap_CS_fsm_state5,
      I4 => ap_block_pp1_stage0_subdone1_in,
      O => ap_enable_reg_pp1_iter2_i_1_n_4
    );
ap_enable_reg_pp1_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp1_iter2_i_1_n_4,
      Q => ap_enable_reg_pp1_iter2_reg_n_4,
      R => '0'
    );
\ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFB5000F0F0F0F0"
    )
        port map (
      I0 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I1 => ap_enable_reg_pp1_iter2_reg_n_4,
      I2 => ap_reg_pp1_iter1_exitcond_reg_509,
      I3 => ap_enable_reg_pp1_iter1_reg_n_4,
      I4 => \exitcond_reg_509_reg_n_4_[0]\,
      I5 => ap_CS_fsm_pp1_stage0,
      O => \ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1_n_4\
    );
\ap_reg_pp1_iter1_exitcond_reg_509_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \ap_reg_pp1_iter1_exitcond_reg_509[0]_i_1_n_4\,
      Q => ap_reg_pp1_iter1_exitcond_reg_509,
      R => '0'
    );
\exitcond_reg_509[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F40"
    )
        port map (
      I0 => \ap_CS_fsm[6]_i_2_n_4\,
      I1 => ap_block_pp1_stage0_subdone1_in,
      I2 => ap_CS_fsm_pp1_stage0,
      I3 => \exitcond_reg_509_reg_n_4_[0]\,
      O => \exitcond_reg_509[0]_i_1_n_4\
    );
\exitcond_reg_509_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \exitcond_reg_509[0]_i_1_n_4\,
      Q => \exitcond_reg_509_reg_n_4_[0]\,
      R => '0'
    );
\i_1_reg_356[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(0),
      O => i_3_fu_455_p2(0)
    );
\i_1_reg_356[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(0),
      I1 => \i_1_reg_356_reg__0\(1),
      O => i_3_fu_455_p2(1)
    );
\i_1_reg_356[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(1),
      I1 => \i_1_reg_356_reg__0\(0),
      I2 => \i_1_reg_356_reg__0\(2),
      O => i_3_fu_455_p2(2)
    );
\i_1_reg_356[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(2),
      I1 => \i_1_reg_356_reg__0\(0),
      I2 => \i_1_reg_356_reg__0\(1),
      I3 => \i_1_reg_356_reg__0\(3),
      O => i_3_fu_455_p2(3)
    );
\i_1_reg_356[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(3),
      I1 => \i_1_reg_356_reg__0\(1),
      I2 => \i_1_reg_356_reg__0\(0),
      I3 => \i_1_reg_356_reg__0\(2),
      I4 => \i_1_reg_356_reg__0\(4),
      O => i_3_fu_455_p2(4)
    );
\i_1_reg_356[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"10F0F0F0F0F0F0F0"
    )
        port map (
      I0 => \i_1_reg_356[5]_i_4_n_4\,
      I1 => OUTPUT_STREAM_V_data_V_1_ack_in,
      I2 => ap_CS_fsm_state5,
      I3 => ap_CS_fsm_pp1_stage0,
      I4 => ap_enable_reg_pp1_iter0,
      I5 => \ap_CS_fsm[6]_i_2_n_4\,
      O => i_1_reg_356
    );
\i_1_reg_356[5]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => ap_block_pp1_stage0_subdone1_in,
      I1 => \ap_CS_fsm[6]_i_2_n_4\,
      I2 => ap_enable_reg_pp1_iter0,
      I3 => ap_CS_fsm_pp1_stage0,
      O => i_1_reg_3560
    );
\i_1_reg_356[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6CCCCCCCCCCCCCCC"
    )
        port map (
      I0 => \i_1_reg_356_reg__0\(4),
      I1 => \i_1_reg_356_reg__0\(5),
      I2 => \i_1_reg_356_reg__0\(2),
      I3 => \i_1_reg_356_reg__0\(0),
      I4 => \i_1_reg_356_reg__0\(1),
      I5 => \i_1_reg_356_reg__0\(3),
      O => i_3_fu_455_p2(5)
    );
\i_1_reg_356[5]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DD0D"
    )
        port map (
      I0 => ap_enable_reg_pp1_iter2_reg_n_4,
      I1 => ap_reg_pp1_iter1_exitcond_reg_509,
      I2 => ap_enable_reg_pp1_iter1_reg_n_4,
      I3 => \exitcond_reg_509_reg_n_4_[0]\,
      O => \i_1_reg_356[5]_i_4_n_4\
    );
\i_1_reg_356_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_1_reg_3560,
      D => i_3_fu_455_p2(0),
      Q => \i_1_reg_356_reg__0\(0),
      R => i_1_reg_356
    );
\i_1_reg_356_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_1_reg_3560,
      D => i_3_fu_455_p2(1),
      Q => \i_1_reg_356_reg__0\(1),
      R => i_1_reg_356
    );
\i_1_reg_356_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_1_reg_3560,
      D => i_3_fu_455_p2(2),
      Q => \i_1_reg_356_reg__0\(2),
      R => i_1_reg_356
    );
\i_1_reg_356_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_1_reg_3560,
      D => i_3_fu_455_p2(3),
      Q => \i_1_reg_356_reg__0\(3),
      R => i_1_reg_356
    );
\i_1_reg_356_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_1_reg_3560,
      D => i_3_fu_455_p2(4),
      Q => \i_1_reg_356_reg__0\(4),
      R => i_1_reg_356
    );
\i_1_reg_356_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_1_reg_3560,
      D => i_3_fu_455_p2(5),
      Q => \i_1_reg_356_reg__0\(5),
      R => i_1_reg_356
    );
\i_reg_345[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_reg_345_reg_n_4_[0]\,
      O => i_2_fu_397_p2(0)
    );
\i_reg_345[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i_reg_345_reg_n_4_[0]\,
      I1 => \i_reg_345_reg_n_4_[1]\,
      O => i_2_fu_397_p2(1)
    );
\i_reg_345[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \i_reg_345_reg_n_4_[1]\,
      I1 => \i_reg_345_reg_n_4_[0]\,
      I2 => \i_reg_345_reg_n_4_[2]\,
      O => i_2_fu_397_p2(2)
    );
\i_reg_345[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \i_reg_345_reg_n_4_[2]\,
      I1 => \i_reg_345_reg_n_4_[0]\,
      I2 => \i_reg_345_reg_n_4_[1]\,
      I3 => \i_reg_345_reg_n_4_[3]\,
      O => i_2_fu_397_p2(3)
    );
\i_reg_345[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \i_reg_345_reg_n_4_[3]\,
      I1 => \i_reg_345_reg_n_4_[1]\,
      I2 => \i_reg_345_reg_n_4_[0]\,
      I3 => \i_reg_345_reg_n_4_[2]\,
      I4 => \i_reg_345_reg_n_4_[4]\,
      O => i_2_fu_397_p2(4)
    );
\i_reg_345[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \tmp_1_reg_494_reg_n_4_[0]\,
      I1 => ap_CS_fsm_state3,
      O => \i_reg_345[5]_i_1_n_4\
    );
\i_reg_345[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00001D0000000000"
    )
        port map (
      I0 => INPUT_STREAM_V_last_V_0_payload_A,
      I1 => INPUT_STREAM_V_last_V_0_sel,
      I2 => INPUT_STREAM_V_last_V_0_payload_B,
      I3 => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      I4 => tmp_2_fu_389_p3,
      I5 => ap_CS_fsm_state4,
      O => ap_NS_fsm19_out
    );
\i_reg_345[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \i_reg_345_reg_n_4_[4]\,
      I1 => \i_reg_345_reg_n_4_[2]\,
      I2 => \i_reg_345_reg_n_4_[0]\,
      I3 => \i_reg_345_reg_n_4_[1]\,
      I4 => \i_reg_345_reg_n_4_[3]\,
      O => i_2_fu_397_p2(5)
    );
\i_reg_345_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm19_out,
      D => i_2_fu_397_p2(0),
      Q => \i_reg_345_reg_n_4_[0]\,
      R => \i_reg_345[5]_i_1_n_4\
    );
\i_reg_345_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm19_out,
      D => i_2_fu_397_p2(1),
      Q => \i_reg_345_reg_n_4_[1]\,
      R => \i_reg_345[5]_i_1_n_4\
    );
\i_reg_345_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm19_out,
      D => i_2_fu_397_p2(2),
      Q => \i_reg_345_reg_n_4_[2]\,
      R => \i_reg_345[5]_i_1_n_4\
    );
\i_reg_345_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm19_out,
      D => i_2_fu_397_p2(3),
      Q => \i_reg_345_reg_n_4_[3]\,
      R => \i_reg_345[5]_i_1_n_4\
    );
\i_reg_345_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm19_out,
      D => i_2_fu_397_p2(4),
      Q => \i_reg_345_reg_n_4_[4]\,
      R => \i_reg_345[5]_i_1_n_4\
    );
\i_reg_345_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_NS_fsm19_out,
      D => i_2_fu_397_p2(5),
      Q => tmp_2_fu_389_p3,
      R => \i_reg_345[5]_i_1_n_4\
    );
\indvarinc_reg_479[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[0]\,
      O => indvarinc_fu_367_p2(0)
    );
\indvarinc_reg_479[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[0]\,
      I1 => \invdar_reg_334_reg_n_4_[1]\,
      O => indvarinc_fu_367_p2(1)
    );
\indvarinc_reg_479[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[0]\,
      I1 => \invdar_reg_334_reg_n_4_[1]\,
      I2 => \invdar_reg_334_reg_n_4_[2]\,
      O => indvarinc_fu_367_p2(2)
    );
\indvarinc_reg_479[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[1]\,
      I1 => \invdar_reg_334_reg_n_4_[0]\,
      I2 => \invdar_reg_334_reg_n_4_[2]\,
      I3 => \invdar_reg_334_reg_n_4_[3]\,
      O => indvarinc_fu_367_p2(3)
    );
\indvarinc_reg_479[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[2]\,
      I1 => \invdar_reg_334_reg_n_4_[0]\,
      I2 => \invdar_reg_334_reg_n_4_[1]\,
      I3 => \invdar_reg_334_reg_n_4_[3]\,
      I4 => \invdar_reg_334_reg_n_4_[4]\,
      O => indvarinc_fu_367_p2(4)
    );
\indvarinc_reg_479[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[3]\,
      I1 => \invdar_reg_334_reg_n_4_[1]\,
      I2 => \invdar_reg_334_reg_n_4_[0]\,
      I3 => \invdar_reg_334_reg_n_4_[2]\,
      I4 => \invdar_reg_334_reg_n_4_[4]\,
      I5 => \invdar_reg_334_reg_n_4_[5]\,
      O => indvarinc_fu_367_p2(5)
    );
\indvarinc_reg_479[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \indvarinc_reg_479[6]_i_2_n_4\,
      I1 => \invdar_reg_334_reg_n_4_[5]\,
      I2 => \invdar_reg_334_reg_n_4_[6]\,
      O => indvarinc_fu_367_p2(6)
    );
\indvarinc_reg_479[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[4]\,
      I1 => \invdar_reg_334_reg_n_4_[2]\,
      I2 => \invdar_reg_334_reg_n_4_[0]\,
      I3 => \invdar_reg_334_reg_n_4_[1]\,
      I4 => \invdar_reg_334_reg_n_4_[3]\,
      O => \indvarinc_reg_479[6]_i_2_n_4\
    );
\indvarinc_reg_479_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(0),
      Q => indvarinc_reg_479(0),
      R => '0'
    );
\indvarinc_reg_479_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(1),
      Q => indvarinc_reg_479(1),
      R => '0'
    );
\indvarinc_reg_479_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(2),
      Q => indvarinc_reg_479(2),
      R => '0'
    );
\indvarinc_reg_479_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(3),
      Q => indvarinc_reg_479(3),
      R => '0'
    );
\indvarinc_reg_479_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(4),
      Q => indvarinc_reg_479(4),
      R => '0'
    );
\indvarinc_reg_479_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(5),
      Q => indvarinc_reg_479(5),
      R => '0'
    );
\indvarinc_reg_479_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => indvarinc_fu_367_p2(6),
      Q => indvarinc_reg_479(6),
      R => '0'
    );
inputValues_1_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud
     port map (
      E(0) => inputValues_6_U_n_5,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => inputValues_6_U_n_15,
      \INPUT_STREAM_V_keep_V_0_payload_A_reg[3]\(3 downto 0) => INPUT_STREAM_V_keep_V_0_payload_A(3 downto 0),
      INPUT_STREAM_V_keep_V_0_sel => INPUT_STREAM_V_keep_V_0_sel,
      \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(3) => inputValues_1_U_n_4,
      \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(2) => inputValues_1_U_n_5,
      \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(1) => inputValues_1_U_n_6,
      \OUTPUT_STREAM_V_keep_V_1_payload_A_reg[3]\(0) => inputValues_1_U_n_7,
      Q(3 downto 0) => INPUT_STREAM_V_keep_V_0_payload_B(3 downto 0),
      \ap_CS_fsm_reg[3]\ => inputValues_6_U_n_4,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => inputValues_6_U_n_7,
      \i_reg_345_reg[1]\ => inputValues_6_U_n_8,
      \i_reg_345_reg[2]\ => inputValues_6_U_n_9,
      \i_reg_345_reg[3]\ => inputValues_6_U_n_10,
      \i_reg_345_reg[4]\ => inputValues_6_U_n_11,
      \i_reg_345_reg[5]\ => inputValues_6_U_n_13,
      \i_reg_345_reg[5]_0\ => inputValues_6_U_n_14,
      \i_reg_345_reg[5]_1\ => inputValues_6_U_n_12
    );
inputValues_2_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuecud_0
     port map (
      E(0) => inputValues_6_U_n_5,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => inputValues_6_U_n_15,
      \INPUT_STREAM_V_strb_V_0_payload_A_reg[3]\(3 downto 0) => INPUT_STREAM_V_strb_V_0_payload_A(3 downto 0),
      INPUT_STREAM_V_strb_V_0_sel => INPUT_STREAM_V_strb_V_0_sel,
      \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(3) => inputValues_2_U_n_4,
      \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(2) => inputValues_2_U_n_5,
      \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(1) => inputValues_2_U_n_6,
      \OUTPUT_STREAM_V_strb_V_1_payload_A_reg[3]\(0) => inputValues_2_U_n_7,
      Q(3 downto 0) => INPUT_STREAM_V_strb_V_0_payload_B(3 downto 0),
      \ap_CS_fsm_reg[3]\ => inputValues_6_U_n_4,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => inputValues_6_U_n_7,
      \i_reg_345_reg[1]\ => inputValues_6_U_n_8,
      \i_reg_345_reg[2]\ => inputValues_6_U_n_9,
      \i_reg_345_reg[3]\ => inputValues_6_U_n_10,
      \i_reg_345_reg[4]\ => inputValues_6_U_n_11,
      \i_reg_345_reg[5]\ => inputValues_6_U_n_13,
      \i_reg_345_reg[5]_0\ => inputValues_6_U_n_14,
      \i_reg_345_reg[5]_1\ => inputValues_6_U_n_12
    );
inputValues_3_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueeOg
     port map (
      E(0) => inputValues_6_U_n_5,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => inputValues_6_U_n_15,
      INPUT_STREAM_V_user_V_0_payload_A(1 downto 0) => INPUT_STREAM_V_user_V_0_payload_A(1 downto 0),
      INPUT_STREAM_V_user_V_0_payload_B(1 downto 0) => INPUT_STREAM_V_user_V_0_payload_B(1 downto 0),
      INPUT_STREAM_V_user_V_0_sel => INPUT_STREAM_V_user_V_0_sel,
      OUTPUT_STREAM_V_user_V_1_ack_in => OUTPUT_STREAM_V_user_V_1_ack_in,
      OUTPUT_STREAM_V_user_V_1_payload_A(1 downto 0) => OUTPUT_STREAM_V_user_V_1_payload_A(1 downto 0),
      \OUTPUT_STREAM_V_user_V_1_payload_A_reg[0]\ => inputValues_3_U_n_5,
      \OUTPUT_STREAM_V_user_V_1_payload_A_reg[1]\ => inputValues_3_U_n_4,
      OUTPUT_STREAM_V_user_V_1_payload_B(1 downto 0) => OUTPUT_STREAM_V_user_V_1_payload_B(1 downto 0),
      \OUTPUT_STREAM_V_user_V_1_payload_B_reg[0]\ => inputValues_3_U_n_7,
      \OUTPUT_STREAM_V_user_V_1_payload_B_reg[1]\ => inputValues_3_U_n_6,
      OUTPUT_STREAM_V_user_V_1_sel_wr => OUTPUT_STREAM_V_user_V_1_sel_wr,
      \OUTPUT_STREAM_V_user_V_1_state_reg[0]\ => \OUTPUT_STREAM_V_user_V_1_state_reg_n_4_[0]\,
      \ap_CS_fsm_reg[3]\ => inputValues_6_U_n_4,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => inputValues_6_U_n_7,
      \i_reg_345_reg[1]\ => inputValues_6_U_n_8,
      \i_reg_345_reg[2]\ => inputValues_6_U_n_9,
      \i_reg_345_reg[3]\ => inputValues_6_U_n_10,
      \i_reg_345_reg[4]\ => inputValues_6_U_n_11,
      \i_reg_345_reg[5]\ => inputValues_6_U_n_13,
      \i_reg_345_reg[5]_0\ => inputValues_6_U_n_14,
      \i_reg_345_reg[5]_1\ => inputValues_6_U_n_12
    );
inputValues_4_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuefYi
     port map (
      E(0) => inputValues_6_U_n_5,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => inputValues_6_U_n_15,
      INPUT_STREAM_V_last_V_0_payload_A => INPUT_STREAM_V_last_V_0_payload_A,
      INPUT_STREAM_V_last_V_0_payload_B => INPUT_STREAM_V_last_V_0_payload_B,
      INPUT_STREAM_V_last_V_0_sel => INPUT_STREAM_V_last_V_0_sel,
      OUTPUT_STREAM_V_last_V_1_ack_in => OUTPUT_STREAM_V_last_V_1_ack_in,
      OUTPUT_STREAM_V_last_V_1_payload_A => OUTPUT_STREAM_V_last_V_1_payload_A,
      \OUTPUT_STREAM_V_last_V_1_payload_A_reg[0]\ => inputValues_4_U_n_5,
      OUTPUT_STREAM_V_last_V_1_payload_B => OUTPUT_STREAM_V_last_V_1_payload_B,
      \OUTPUT_STREAM_V_last_V_1_payload_B_reg[0]\ => inputValues_4_U_n_6,
      OUTPUT_STREAM_V_last_V_1_sel_wr => OUTPUT_STREAM_V_last_V_1_sel_wr,
      \OUTPUT_STREAM_V_last_V_1_state_reg[0]\ => \OUTPUT_STREAM_V_last_V_1_state_reg_n_4_[0]\,
      \ap_CS_fsm_reg[3]\ => inputValues_6_U_n_4,
      ap_clk => ap_clk,
      d0 => d0,
      \i_reg_345_reg[0]\ => inputValues_6_U_n_7,
      \i_reg_345_reg[1]\ => inputValues_6_U_n_8,
      \i_reg_345_reg[2]\ => inputValues_6_U_n_9,
      \i_reg_345_reg[3]\ => inputValues_6_U_n_10,
      \i_reg_345_reg[4]\ => inputValues_6_U_n_11,
      \i_reg_345_reg[5]\ => inputValues_6_U_n_13,
      \i_reg_345_reg[5]_0\ => inputValues_6_U_n_14,
      \i_reg_345_reg[5]_1\ => inputValues_6_U_n_12
    );
inputValues_5_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValueg8j
     port map (
      E(0) => inputValues_6_U_n_5,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => inputValues_6_U_n_15,
      \INPUT_STREAM_V_id_V_0_payload_A_reg[4]\(4 downto 0) => INPUT_STREAM_V_id_V_0_payload_A(4 downto 0),
      INPUT_STREAM_V_id_V_0_sel => INPUT_STREAM_V_id_V_0_sel,
      \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(4) => inputValues_5_U_n_4,
      \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(3) => inputValues_5_U_n_5,
      \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(2) => inputValues_5_U_n_6,
      \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(1) => inputValues_5_U_n_7,
      \OUTPUT_STREAM_V_id_V_1_payload_A_reg[4]\(0) => inputValues_5_U_n_8,
      Q(4 downto 0) => INPUT_STREAM_V_id_V_0_payload_B(4 downto 0),
      \ap_CS_fsm_reg[3]\ => inputValues_6_U_n_4,
      ap_clk => ap_clk,
      \i_reg_345_reg[0]\ => inputValues_6_U_n_7,
      \i_reg_345_reg[1]\ => inputValues_6_U_n_8,
      \i_reg_345_reg[2]\ => inputValues_6_U_n_9,
      \i_reg_345_reg[3]\ => inputValues_6_U_n_10,
      \i_reg_345_reg[4]\ => inputValues_6_U_n_11,
      \i_reg_345_reg[5]\ => inputValues_6_U_n_13,
      \i_reg_345_reg[5]_0\ => inputValues_6_U_n_14,
      \i_reg_345_reg[5]_1\ => inputValues_6_U_n_12
    );
inputValues_6_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuehbi
     port map (
      E(0) => inputValues_6_U_n_5,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      \INPUT_STREAM_V_dest_V_0_payload_B_reg[5]\(5 downto 0) => INPUT_STREAM_V_dest_V_0_payload_B(5 downto 0),
      INPUT_STREAM_V_dest_V_0_sel => INPUT_STREAM_V_dest_V_0_sel,
      OUTPUT_STREAM_V_data_V_1_ack_in => OUTPUT_STREAM_V_data_V_1_ack_in,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(5) => inputValues_6_U_n_16,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(4) => inputValues_6_U_n_17,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(3) => inputValues_6_U_n_18,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(2) => inputValues_6_U_n_19,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(1) => inputValues_6_U_n_20,
      \OUTPUT_STREAM_V_dest_V_1_payload_A_reg[5]\(0) => inputValues_6_U_n_21,
      Q(5 downto 0) => INPUT_STREAM_V_dest_V_0_payload_A(5 downto 0),
      \ap_CS_fsm_reg[5]\(1) => ap_CS_fsm_pp1_stage0,
      \ap_CS_fsm_reg[5]\(0) => ap_CS_fsm_state4,
      ap_block_pp1_stage0_subdone1_in => ap_block_pp1_stage0_subdone1_in,
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter0 => ap_enable_reg_pp1_iter0,
      ap_enable_reg_pp1_iter1_reg => ap_enable_reg_pp1_iter1_reg_n_4,
      ap_enable_reg_pp1_iter2_reg => ap_enable_reg_pp1_iter2_reg_n_4,
      ap_reg_pp1_iter1_exitcond_reg_509 => ap_reg_pp1_iter1_exitcond_reg_509,
      \exitcond_reg_509_reg[0]\ => \exitcond_reg_509_reg_n_4_[0]\,
      \i_1_reg_356_reg[5]\(5 downto 0) => \i_1_reg_356_reg__0\(5 downto 0),
      \i_reg_345_reg[5]\(5) => tmp_2_fu_389_p3,
      \i_reg_345_reg[5]\(4) => \i_reg_345_reg_n_4_[4]\,
      \i_reg_345_reg[5]\(3) => \i_reg_345_reg_n_4_[3]\,
      \i_reg_345_reg[5]\(2) => \i_reg_345_reg_n_4_[2]\,
      \i_reg_345_reg[5]\(1) => \i_reg_345_reg_n_4_[1]\,
      \i_reg_345_reg[5]\(0) => \i_reg_345_reg_n_4_[0]\,
      \q0_reg[0]\ => inputValues_6_U_n_7,
      \q0_reg[0]_0\ => inputValues_6_U_n_8,
      \q0_reg[0]_1\ => inputValues_6_U_n_9,
      \q0_reg[0]_2\ => inputValues_6_U_n_10,
      \q0_reg[0]_3\ => inputValues_6_U_n_15,
      \q0_reg[4]\ => inputValues_6_U_n_13,
      \q0_reg[5]\ => inputValues_6_U_n_4,
      \q0_reg[5]_0\ => inputValues_6_U_n_11,
      \q0_reg[5]_1\ => inputValues_6_U_n_12,
      \q0_reg[5]_2\ => inputValues_6_U_n_14
    );
input_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input
     port map (
      D(31 downto 0) => tmp_data_V_1_fu_472_p2(31 downto 0),
      DIADI(4) => Adder2_stream_strea_U_n_4,
      DIADI(3) => Adder2_stream_strea_U_n_5,
      DIADI(2) => Adder2_stream_strea_U_n_6,
      DIADI(1) => Adder2_stream_strea_U_n_7,
      DIADI(0) => Adder2_stream_strea_U_n_8,
      E(0) => inputValues_6_U_n_5,
      I1(31 downto 0) => input_q0(31 downto 0),
      \INPUT_STREAM_V_data_V_0_payload_A_reg[31]\(26 downto 0) => INPUT_STREAM_V_data_V_0_payload_A(31 downto 5),
      \INPUT_STREAM_V_data_V_0_payload_B_reg[31]\(26 downto 0) => INPUT_STREAM_V_data_V_0_payload_B(31 downto 5),
      INPUT_STREAM_V_data_V_0_sel => INPUT_STREAM_V_data_V_0_sel,
      \INPUT_STREAM_V_data_V_0_state_reg[0]\ => \INPUT_STREAM_V_data_V_0_state_reg_n_4_[0]\,
      Q(3) => ap_CS_fsm_state9,
      Q(2) => ap_CS_fsm_pp1_stage0,
      Q(1) => ap_CS_fsm_state4,
      Q(0) => ap_CS_fsm_state3,
      ap_clk => ap_clk,
      ap_enable_reg_pp1_iter0 => ap_enable_reg_pp1_iter0,
      \i_1_reg_356_reg[5]\(5 downto 0) => \i_1_reg_356_reg__0\(5 downto 0),
      \i_reg_345_reg[5]\(5) => tmp_2_fu_389_p3,
      \i_reg_345_reg[5]\(4) => \i_reg_345_reg_n_4_[4]\,
      \i_reg_345_reg[5]\(3) => \i_reg_345_reg_n_4_[3]\,
      \i_reg_345_reg[5]\(2) => \i_reg_345_reg_n_4_[2]\,
      \i_reg_345_reg[5]\(1) => \i_reg_345_reg_n_4_[1]\,
      \i_reg_345_reg[5]\(0) => \i_reg_345_reg_n_4_[0]\,
      \tmp_reg_484_reg[6]\(6 downto 0) => tmp_reg_484(6 downto 0)
    );
\invdar_reg_334[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => \tmp_1_reg_494_reg_n_4_[0]\,
      O => invdar_reg_3340
    );
\invdar_reg_334_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(0),
      Q => \invdar_reg_334_reg_n_4_[0]\,
      R => invdar_reg_334
    );
\invdar_reg_334_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(1),
      Q => \invdar_reg_334_reg_n_4_[1]\,
      R => invdar_reg_334
    );
\invdar_reg_334_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(2),
      Q => \invdar_reg_334_reg_n_4_[2]\,
      R => invdar_reg_334
    );
\invdar_reg_334_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(3),
      Q => \invdar_reg_334_reg_n_4_[3]\,
      R => invdar_reg_334
    );
\invdar_reg_334_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(4),
      Q => \invdar_reg_334_reg_n_4_[4]\,
      R => invdar_reg_334
    );
\invdar_reg_334_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(5),
      Q => \invdar_reg_334_reg_n_4_[5]\,
      R => invdar_reg_334
    );
\invdar_reg_334_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => invdar_reg_3340,
      D => indvarinc_reg_479(6),
      Q => \invdar_reg_334_reg_n_4_[6]\,
      R => invdar_reg_334
    );
\tmp_1_reg_494[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000300AAAAAAAA"
    )
        port map (
      I0 => \tmp_1_reg_494_reg_n_4_[0]\,
      I1 => \invdar_reg_334_reg_n_4_[5]\,
      I2 => \invdar_reg_334_reg_n_4_[1]\,
      I3 => \invdar_reg_334_reg_n_4_[0]\,
      I4 => \tmp_1_reg_494[0]_i_2_n_4\,
      I5 => ce0,
      O => \tmp_1_reg_494[0]_i_1_n_4\
    );
\tmp_1_reg_494[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \invdar_reg_334_reg_n_4_[3]\,
      I1 => \invdar_reg_334_reg_n_4_[4]\,
      I2 => \invdar_reg_334_reg_n_4_[2]\,
      I3 => \invdar_reg_334_reg_n_4_[6]\,
      O => \tmp_1_reg_494[0]_i_2_n_4\
    );
\tmp_1_reg_494_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_1_reg_494[0]_i_1_n_4\,
      Q => \tmp_1_reg_494_reg_n_4_[0]\,
      R => '0'
    );
\tmp_reg_484_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[0]\,
      Q => tmp_reg_484(0),
      R => '0'
    );
\tmp_reg_484_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[1]\,
      Q => tmp_reg_484(1),
      R => '0'
    );
\tmp_reg_484_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[2]\,
      Q => tmp_reg_484(2),
      R => '0'
    );
\tmp_reg_484_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[3]\,
      Q => tmp_reg_484(3),
      R => '0'
    );
\tmp_reg_484_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[4]\,
      Q => tmp_reg_484(4),
      R => '0'
    );
\tmp_reg_484_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[5]\,
      Q => tmp_reg_484(5),
      R => '0'
    );
\tmp_reg_484_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \invdar_reg_334_reg_n_4_[6]\,
      Q => tmp_reg_484(6),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s_axi_CONTROL_BUS_AWADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_AWVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_AWREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CONTROL_BUS_WVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_WREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CONTROL_BUS_BVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_BREADY : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARADDR : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s_axi_CONTROL_BUS_ARVALID : in STD_LOGIC;
    s_axi_CONTROL_BUS_ARREADY : out STD_LOGIC;
    s_axi_CONTROL_BUS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CONTROL_BUS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CONTROL_BUS_RVALID : out STD_LOGIC;
    s_axi_CONTROL_BUS_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    interrupt : out STD_LOGIC;
    INPUT_STREAM_TVALID : in STD_LOGIC;
    INPUT_STREAM_TREADY : out STD_LOGIC;
    INPUT_STREAM_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    INPUT_STREAM_TDEST : in STD_LOGIC_VECTOR ( 5 downto 0 );
    INPUT_STREAM_TKEEP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    INPUT_STREAM_TUSER : in STD_LOGIC_VECTOR ( 1 downto 0 );
    INPUT_STREAM_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 );
    INPUT_STREAM_TID : in STD_LOGIC_VECTOR ( 4 downto 0 );
    OUTPUT_STREAM_TVALID : out STD_LOGIC;
    OUTPUT_STREAM_TREADY : in STD_LOGIC;
    OUTPUT_STREAM_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    OUTPUT_STREAM_TDEST : out STD_LOGIC_VECTOR ( 5 downto 0 );
    OUTPUT_STREAM_TKEEP : out STD_LOGIC_VECTOR ( 3 downto 0 );
    OUTPUT_STREAM_TSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    OUTPUT_STREAM_TUSER : out STD_LOGIC_VECTOR ( 1 downto 0 );
    OUTPUT_STREAM_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    OUTPUT_STREAM_TID : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "system_Adder2_0_0,Adder2,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Adder2,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_ADDR_WIDTH of U0 : label is 5;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH : integer;
  attribute C_S_AXI_CONTROL_BUS_DATA_WIDTH of U0 : label is 32;
  attribute x_interface_info : string;
  attribute x_interface_info of INPUT_STREAM_TREADY : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY";
  attribute x_interface_info of INPUT_STREAM_TVALID : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of INPUT_STREAM_TVALID : signal is "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of OUTPUT_STREAM_TREADY : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TREADY";
  attribute x_interface_info of OUTPUT_STREAM_TVALID : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TVALID";
  attribute x_interface_parameter of OUTPUT_STREAM_TVALID : signal is "XIL_INTERFACENAME OUTPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TDATA_WIDTH 32 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 2} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} TUSER_WIDTH 2}, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM:OUTPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute x_interface_parameter of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of interrupt : signal is "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT";
  attribute x_interface_parameter of interrupt : signal is "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1";
  attribute x_interface_info of s_axi_CONTROL_BUS_ARREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_ARVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_AWREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_AWVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_BREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_BVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_RREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_RVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID";
  attribute x_interface_info of s_axi_CONTROL_BUS_WREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY";
  attribute x_interface_info of s_axi_CONTROL_BUS_WVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID";
  attribute x_interface_info of INPUT_STREAM_TDATA : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA";
  attribute x_interface_info of INPUT_STREAM_TDEST : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST";
  attribute x_interface_info of INPUT_STREAM_TID : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TID";
  attribute x_interface_info of INPUT_STREAM_TKEEP : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP";
  attribute x_interface_info of INPUT_STREAM_TLAST : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST";
  attribute x_interface_info of INPUT_STREAM_TSTRB : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB";
  attribute x_interface_info of INPUT_STREAM_TUSER : signal is "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER";
  attribute x_interface_info of OUTPUT_STREAM_TDATA : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TDATA";
  attribute x_interface_info of OUTPUT_STREAM_TDEST : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TDEST";
  attribute x_interface_info of OUTPUT_STREAM_TID : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TID";
  attribute x_interface_info of OUTPUT_STREAM_TKEEP : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TKEEP";
  attribute x_interface_info of OUTPUT_STREAM_TLAST : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TLAST";
  attribute x_interface_info of OUTPUT_STREAM_TSTRB : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TSTRB";
  attribute x_interface_info of OUTPUT_STREAM_TUSER : signal is "xilinx.com:interface:axis:1.0 OUTPUT_STREAM TUSER";
  attribute x_interface_info of s_axi_CONTROL_BUS_ARADDR : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR";
  attribute x_interface_info of s_axi_CONTROL_BUS_AWADDR : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR";
  attribute x_interface_parameter of s_axi_CONTROL_BUS_AWADDR : signal is "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s_axi_CONTROL_BUS_BRESP : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP";
  attribute x_interface_info of s_axi_CONTROL_BUS_RDATA : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA";
  attribute x_interface_info of s_axi_CONTROL_BUS_RRESP : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP";
  attribute x_interface_info of s_axi_CONTROL_BUS_WDATA : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA";
  attribute x_interface_info of s_axi_CONTROL_BUS_WSTRB : signal is "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
     port map (
      INPUT_STREAM_TDATA(31 downto 0) => INPUT_STREAM_TDATA(31 downto 0),
      INPUT_STREAM_TDEST(5 downto 0) => INPUT_STREAM_TDEST(5 downto 0),
      INPUT_STREAM_TID(4 downto 0) => INPUT_STREAM_TID(4 downto 0),
      INPUT_STREAM_TKEEP(3 downto 0) => INPUT_STREAM_TKEEP(3 downto 0),
      INPUT_STREAM_TLAST(0) => INPUT_STREAM_TLAST(0),
      INPUT_STREAM_TREADY => INPUT_STREAM_TREADY,
      INPUT_STREAM_TSTRB(3 downto 0) => INPUT_STREAM_TSTRB(3 downto 0),
      INPUT_STREAM_TUSER(1 downto 0) => INPUT_STREAM_TUSER(1 downto 0),
      INPUT_STREAM_TVALID => INPUT_STREAM_TVALID,
      OUTPUT_STREAM_TDATA(31 downto 0) => OUTPUT_STREAM_TDATA(31 downto 0),
      OUTPUT_STREAM_TDEST(5 downto 0) => OUTPUT_STREAM_TDEST(5 downto 0),
      OUTPUT_STREAM_TID(4 downto 0) => OUTPUT_STREAM_TID(4 downto 0),
      OUTPUT_STREAM_TKEEP(3 downto 0) => OUTPUT_STREAM_TKEEP(3 downto 0),
      OUTPUT_STREAM_TLAST(0) => OUTPUT_STREAM_TLAST(0),
      OUTPUT_STREAM_TREADY => OUTPUT_STREAM_TREADY,
      OUTPUT_STREAM_TSTRB(3 downto 0) => OUTPUT_STREAM_TSTRB(3 downto 0),
      OUTPUT_STREAM_TUSER(1 downto 0) => OUTPUT_STREAM_TUSER(1 downto 0),
      OUTPUT_STREAM_TVALID => OUTPUT_STREAM_TVALID,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      interrupt => interrupt,
      s_axi_CONTROL_BUS_ARADDR(4 downto 0) => s_axi_CONTROL_BUS_ARADDR(4 downto 0),
      s_axi_CONTROL_BUS_ARREADY => s_axi_CONTROL_BUS_ARREADY,
      s_axi_CONTROL_BUS_ARVALID => s_axi_CONTROL_BUS_ARVALID,
      s_axi_CONTROL_BUS_AWADDR(4 downto 0) => s_axi_CONTROL_BUS_AWADDR(4 downto 0),
      s_axi_CONTROL_BUS_AWREADY => s_axi_CONTROL_BUS_AWREADY,
      s_axi_CONTROL_BUS_AWVALID => s_axi_CONTROL_BUS_AWVALID,
      s_axi_CONTROL_BUS_BREADY => s_axi_CONTROL_BUS_BREADY,
      s_axi_CONTROL_BUS_BRESP(1 downto 0) => s_axi_CONTROL_BUS_BRESP(1 downto 0),
      s_axi_CONTROL_BUS_BVALID => s_axi_CONTROL_BUS_BVALID,
      s_axi_CONTROL_BUS_RDATA(31 downto 0) => s_axi_CONTROL_BUS_RDATA(31 downto 0),
      s_axi_CONTROL_BUS_RREADY => s_axi_CONTROL_BUS_RREADY,
      s_axi_CONTROL_BUS_RRESP(1 downto 0) => s_axi_CONTROL_BUS_RRESP(1 downto 0),
      s_axi_CONTROL_BUS_RVALID => s_axi_CONTROL_BUS_RVALID,
      s_axi_CONTROL_BUS_WDATA(31 downto 0) => s_axi_CONTROL_BUS_WDATA(31 downto 0),
      s_axi_CONTROL_BUS_WREADY => s_axi_CONTROL_BUS_WREADY,
      s_axi_CONTROL_BUS_WSTRB(3 downto 0) => s_axi_CONTROL_BUS_WSTRB(3 downto 0),
      s_axi_CONTROL_BUS_WVALID => s_axi_CONTROL_BUS_WVALID
    );
end STRUCTURE;
