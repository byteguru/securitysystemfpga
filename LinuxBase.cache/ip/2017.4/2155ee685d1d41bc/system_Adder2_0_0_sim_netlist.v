// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Sat Feb 24 13:32:27 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    LAST_STREAM_TDATA,
    LAST_STREAM_TVALID,
    LAST_STREAM_TREADY,
    LAST_STREAM_TKEEP,
    LAST_STREAM_TSTRB,
    LAST_STREAM_TUSER,
    LAST_STREAM_TLAST,
    LAST_STREAM_TID,
    LAST_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input [31:0]LAST_STREAM_TDATA;
  input LAST_STREAM_TVALID;
  output LAST_STREAM_TREADY;
  input [3:0]LAST_STREAM_TKEEP;
  input [3:0]LAST_STREAM_TSTRB;
  input [1:0]LAST_STREAM_TUSER;
  input [0:0]LAST_STREAM_TLAST;
  input [4:0]LAST_STREAM_TID;
  input [5:0]LAST_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire ARESET;
  wire Adder2_CONTROL_BUS_s_axi_U_n_45;
  wire [31:0]INPUT_STREAM_TDATA;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire INPUT_STREAM_V_data_V_0_ack_in;
  wire INPUT_STREAM_V_data_V_0_load_A;
  wire INPUT_STREAM_V_data_V_0_load_B;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_A;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_B;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_6;
  wire INPUT_STREAM_V_data_V_0_sel_wr;
  wire INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_6;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_6 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_6 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_6_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_6 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_6 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_6;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_6;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_6 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ;
  wire [31:0]LAST_STREAM_TDATA;
  wire [0:0]LAST_STREAM_TLAST;
  wire LAST_STREAM_TREADY;
  wire LAST_STREAM_TVALID;
  wire LAST_STREAM_V_data_V_0_ack_in;
  wire LAST_STREAM_V_data_V_0_load_A;
  wire LAST_STREAM_V_data_V_0_load_B;
  wire [31:0]LAST_STREAM_V_data_V_0_payload_A;
  wire [31:0]LAST_STREAM_V_data_V_0_payload_B;
  wire LAST_STREAM_V_data_V_0_sel;
  wire LAST_STREAM_V_data_V_0_sel_rd_i_1_n_6;
  wire LAST_STREAM_V_data_V_0_sel_wr;
  wire LAST_STREAM_V_data_V_0_sel_wr_i_1_n_6;
  wire [1:1]LAST_STREAM_V_data_V_0_state;
  wire \LAST_STREAM_V_data_V_0_state[0]_i_1_n_6 ;
  wire \LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ;
  wire [1:1]LAST_STREAM_V_dest_V_0_state;
  wire \LAST_STREAM_V_dest_V_0_state[0]_i_1_n_6 ;
  wire \LAST_STREAM_V_dest_V_0_state_reg_n_6_[0] ;
  wire LAST_STREAM_V_last_V_0_ack_in;
  wire LAST_STREAM_V_last_V_0_payload_A;
  wire \LAST_STREAM_V_last_V_0_payload_A[0]_i_1_n_6 ;
  wire LAST_STREAM_V_last_V_0_payload_B;
  wire \LAST_STREAM_V_last_V_0_payload_B[0]_i_1_n_6 ;
  wire LAST_STREAM_V_last_V_0_sel;
  wire LAST_STREAM_V_last_V_0_sel_rd_i_1_n_6;
  wire LAST_STREAM_V_last_V_0_sel_wr;
  wire LAST_STREAM_V_last_V_0_sel_wr_i_1_n_6;
  wire [1:1]LAST_STREAM_V_last_V_0_state;
  wire \LAST_STREAM_V_last_V_0_state[0]_i_1_n_6 ;
  wire \LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ;
  wire agg_result_a_ap_vld;
  wire \ap_CS_fsm[1]_i_2_n_6 ;
  wire \ap_CS_fsm[3]_i_2_n_6 ;
  wire \ap_CS_fsm[6]_i_2_n_6 ;
  wire \ap_CS_fsm[6]_i_3_n_6 ;
  wire ap_CS_fsm_pp2_stage0;
  wire \ap_CS_fsm_reg_n_6_[0] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state5;
  wire [6:0]ap_NS_fsm;
  wire ap_NS_fsm12_out;
  wire ap_NS_fsm18_out;
  wire ap_clk;
  wire ap_enable_reg_pp2_iter0;
  wire ap_enable_reg_pp2_iter0_i_1_n_6;
  wire ap_enable_reg_pp2_iter0_i_2_n_6;
  wire ap_enable_reg_pp2_iter1;
  wire ap_enable_reg_pp2_iter1_i_1_n_6;
  wire ap_phi_mux_i_2_phi_fu_230_p41;
  wire ap_rst_n;
  wire exitcond_reg_360;
  wire \exitcond_reg_360[0]_i_1_n_6 ;
  wire i_1_reg_215;
  wire \i_1_reg_215[5]_i_4_n_6 ;
  wire \i_1_reg_215_reg_n_6_[0] ;
  wire \i_1_reg_215_reg_n_6_[1] ;
  wire \i_1_reg_215_reg_n_6_[2] ;
  wire \i_1_reg_215_reg_n_6_[3] ;
  wire \i_1_reg_215_reg_n_6_[4] ;
  wire i_2_reg_226;
  wire \i_2_reg_226_reg_n_6_[0] ;
  wire \i_2_reg_226_reg_n_6_[1] ;
  wire \i_2_reg_226_reg_n_6_[2] ;
  wire \i_2_reg_226_reg_n_6_[3] ;
  wire \i_2_reg_226_reg_n_6_[4] ;
  wire \i_2_reg_226_reg_n_6_[5] ;
  wire [5:0]i_3_fu_246_p2;
  wire [5:0]i_4_fu_274_p2;
  wire [5:0]i_5_fu_300_p2;
  wire i_5_reg_3640;
  wire \i_5_reg_364[2]_i_2_n_6 ;
  wire \i_5_reg_364[3]_i_2_n_6 ;
  wire \i_5_reg_364[3]_i_3_n_6 ;
  wire \i_5_reg_364[4]_i_2_n_6 ;
  wire \i_5_reg_364[5]_i_3_n_6 ;
  wire \i_5_reg_364[5]_i_4_n_6 ;
  wire \i_5_reg_364[5]_i_5_n_6 ;
  wire [5:0]i_5_reg_364_reg__0;
  wire i_reg_204;
  wire \i_reg_204[5]_i_4_n_6 ;
  wire \i_reg_204_reg_n_6_[0] ;
  wire \i_reg_204_reg_n_6_[1] ;
  wire \i_reg_204_reg_n_6_[2] ;
  wire \i_reg_204_reg_n_6_[3] ;
  wire \i_reg_204_reg_n_6_[4] ;
  wire inputValues_0_U_n_36;
  wire [29:0]inputValues_0_q0;
  wire interrupt;
  wire [15:0]last;
  wire last1;
  wire lastValues_0_U_n_9;
  wire [31:30]lastValues_0_q0;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]searched;
  wire [31:0]searched_read_reg_333;
  wire tmp_4_fu_266_p3;
  wire tmp_9_fu_316_p2;
  wire tmp_fu_238_p3;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.ARESET(ARESET),
        .D(ap_NS_fsm[1:0]),
        .E(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .INPUT_STREAM_V_last_V_0_payload_A(INPUT_STREAM_V_last_V_0_payload_A),
        .INPUT_STREAM_V_last_V_0_payload_B(INPUT_STREAM_V_last_V_0_payload_B),
        .INPUT_STREAM_V_last_V_0_sel(INPUT_STREAM_V_last_V_0_sel),
        .Q({agg_result_a_ap_vld,\ap_CS_fsm_reg_n_6_[0] }),
        .SR(i_reg_204),
        .\ap_CS_fsm_reg[1] (\i_reg_204[5]_i_4_n_6 ),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .\i_reg_204_reg[5] (\ap_CS_fsm[1]_i_2_n_6 ),
        .interrupt(interrupt),
        .\last_reg[15] ({last[15],last[5:0]}),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .\searched_read_reg_333_reg[31] (searched),
        .\searched_read_reg_333_reg[31]_0 (searched_read_reg_333));
  GND GND
       (.G(\<const0> ));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_A));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_B));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hDF20)) 
    INPUT_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I1(tmp_fu_238_p3),
        .I2(ap_CS_fsm_state2),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_6),
        .Q(INPUT_STREAM_V_data_V_0_sel),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_V_data_V_0_ack_in),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_6),
        .Q(INPUT_STREAM_V_data_V_0_sel_wr),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFDFFF00000000000)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_238_p3),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_V_data_V_0_ack_in),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I5(ap_rst_n),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h4FFF4F4F)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(tmp_fu_238_p3),
        .I1(ap_CS_fsm_state2),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(INPUT_STREAM_TVALID),
        .I4(INPUT_STREAM_V_data_V_0_ack_in),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_6 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(INPUT_STREAM_V_data_V_0_ack_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hF0C080C0)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(\i_reg_204[5]_i_4_n_6 ),
        .I1(\INPUT_STREAM_V_dest_V_0_state_reg_n_6_[0] ),
        .I2(ap_rst_n),
        .I3(INPUT_STREAM_TREADY),
        .I4(INPUT_STREAM_TVALID),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_6 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_238_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_6_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_TREADY),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_6 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_6_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(INPUT_STREAM_V_last_V_0_sel_wr),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_6 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_6 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(INPUT_STREAM_V_last_V_0_sel_wr),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_6 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_6 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_238_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_6),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_V_last_V_0_ack_in),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_6),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFB008800)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(\i_reg_204[5]_i_4_n_6 ),
        .I3(ap_rst_n),
        .I4(\INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_6 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_238_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_last_V_0_ack_in),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_6 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h0D)) 
    \LAST_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I1(LAST_STREAM_V_data_V_0_ack_in),
        .I2(LAST_STREAM_V_data_V_0_sel_wr),
        .O(LAST_STREAM_V_data_V_0_load_A));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[0]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[10]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[11]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[12]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[13]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[14]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[15]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[16]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[17]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[18]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[19]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[1]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[20]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[21]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[22]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[23]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[24]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[25]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[26]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[27]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[28]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[29]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[2]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[30]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[31]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[3]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[4]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[5]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[6]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[7]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[8]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[9]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \LAST_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I1(LAST_STREAM_V_data_V_0_ack_in),
        .I2(LAST_STREAM_V_data_V_0_sel_wr),
        .O(LAST_STREAM_V_data_V_0_load_B));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[0]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[10]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[11]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[12]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[13]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[14]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[15]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[16]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[17]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[18]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[19]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[1]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[20]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[21]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[22]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[23]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[24]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[25]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[26]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[27]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[28]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[29]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[2]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[30]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[31]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[3]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[4]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[5]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[6]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[7]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[8]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[9]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hDF20)) 
    LAST_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I1(tmp_4_fu_266_p3),
        .I2(ap_CS_fsm_state4),
        .I3(LAST_STREAM_V_data_V_0_sel),
        .O(LAST_STREAM_V_data_V_0_sel_rd_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    LAST_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_sel_rd_i_1_n_6),
        .Q(LAST_STREAM_V_data_V_0_sel),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h78)) 
    LAST_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(LAST_STREAM_V_data_V_0_ack_in),
        .I1(LAST_STREAM_TVALID),
        .I2(LAST_STREAM_V_data_V_0_sel_wr),
        .O(LAST_STREAM_V_data_V_0_sel_wr_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    LAST_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_sel_wr_i_1_n_6),
        .Q(LAST_STREAM_V_data_V_0_sel_wr),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFDFFF00000000000)) 
    \LAST_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_4_fu_266_p3),
        .I2(LAST_STREAM_TVALID),
        .I3(LAST_STREAM_V_data_V_0_ack_in),
        .I4(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I5(ap_rst_n),
        .O(\LAST_STREAM_V_data_V_0_state[0]_i_1_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h4FFF4F4F)) 
    \LAST_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(tmp_4_fu_266_p3),
        .I1(ap_CS_fsm_state4),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(LAST_STREAM_TVALID),
        .I4(LAST_STREAM_V_data_V_0_ack_in),
        .O(LAST_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_data_V_0_state[0]_i_1_n_6 ),
        .Q(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_state),
        .Q(LAST_STREAM_V_data_V_0_ack_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hF0C080C0)) 
    \LAST_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(\i_1_reg_215[5]_i_4_n_6 ),
        .I1(\LAST_STREAM_V_dest_V_0_state_reg_n_6_[0] ),
        .I2(ap_rst_n),
        .I3(LAST_STREAM_TREADY),
        .I4(LAST_STREAM_TVALID),
        .O(\LAST_STREAM_V_dest_V_0_state[0]_i_1_n_6 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \LAST_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_4_fu_266_p3),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(\LAST_STREAM_V_dest_V_0_state_reg_n_6_[0] ),
        .I4(LAST_STREAM_TVALID),
        .I5(LAST_STREAM_TREADY),
        .O(LAST_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_dest_V_0_state[0]_i_1_n_6 ),
        .Q(\LAST_STREAM_V_dest_V_0_state_reg_n_6_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_dest_V_0_state),
        .Q(LAST_STREAM_TREADY),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \LAST_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(LAST_STREAM_TLAST),
        .I1(\LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I2(LAST_STREAM_V_last_V_0_ack_in),
        .I3(LAST_STREAM_V_last_V_0_sel_wr),
        .I4(LAST_STREAM_V_last_V_0_payload_A),
        .O(\LAST_STREAM_V_last_V_0_payload_A[0]_i_1_n_6 ));
  FDRE \LAST_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_last_V_0_payload_A[0]_i_1_n_6 ),
        .Q(LAST_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \LAST_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(LAST_STREAM_TLAST),
        .I1(\LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I2(LAST_STREAM_V_last_V_0_ack_in),
        .I3(LAST_STREAM_V_last_V_0_sel_wr),
        .I4(LAST_STREAM_V_last_V_0_payload_B),
        .O(\LAST_STREAM_V_last_V_0_payload_B[0]_i_1_n_6 ));
  FDRE \LAST_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_last_V_0_payload_B[0]_i_1_n_6 ),
        .Q(LAST_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    LAST_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_4_fu_266_p3),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(\LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I4(LAST_STREAM_V_last_V_0_sel),
        .O(LAST_STREAM_V_last_V_0_sel_rd_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    LAST_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_last_V_0_sel_rd_i_1_n_6),
        .Q(LAST_STREAM_V_last_V_0_sel),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h78)) 
    LAST_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(LAST_STREAM_TVALID),
        .I1(LAST_STREAM_V_last_V_0_ack_in),
        .I2(LAST_STREAM_V_last_V_0_sel_wr),
        .O(LAST_STREAM_V_last_V_0_sel_wr_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    LAST_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_last_V_0_sel_wr_i_1_n_6),
        .Q(LAST_STREAM_V_last_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFB008800)) 
    \LAST_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(LAST_STREAM_TVALID),
        .I1(LAST_STREAM_V_last_V_0_ack_in),
        .I2(\i_1_reg_215[5]_i_4_n_6 ),
        .I3(ap_rst_n),
        .I4(\LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .O(\LAST_STREAM_V_last_V_0_state[0]_i_1_n_6 ));
  LUT6 #(
    .INIT(64'h20FFFFFF20FF20FF)) 
    \LAST_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_4_fu_266_p3),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I3(\LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .I4(LAST_STREAM_TVALID),
        .I5(LAST_STREAM_V_last_V_0_ack_in),
        .O(LAST_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_last_V_0_state[0]_i_1_n_6 ),
        .Q(\LAST_STREAM_V_last_V_0_state_reg_n_6_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_last_V_0_state),
        .Q(LAST_STREAM_V_last_V_0_ack_in),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(tmp_fu_238_p3),
        .I1(ap_CS_fsm_state2),
        .O(\ap_CS_fsm[1]_i_2_n_6 ));
  LUT6 #(
    .INIT(64'hFEAEAAAA00000000)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(tmp_fu_238_p3),
        .I1(INPUT_STREAM_V_last_V_0_payload_A),
        .I2(INPUT_STREAM_V_last_V_0_sel),
        .I3(INPUT_STREAM_V_last_V_0_payload_B),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I5(ap_CS_fsm_state2),
        .O(ap_NS_fsm[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFF01515555)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(\ap_CS_fsm[3]_i_2_n_6 ),
        .I1(LAST_STREAM_V_last_V_0_payload_A),
        .I2(LAST_STREAM_V_last_V_0_sel),
        .I3(LAST_STREAM_V_last_V_0_payload_B),
        .I4(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I5(ap_CS_fsm_state3),
        .O(ap_NS_fsm[3]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \ap_CS_fsm[3]_i_2 
       (.I0(tmp_4_fu_266_p3),
        .I1(ap_CS_fsm_state4),
        .O(\ap_CS_fsm[3]_i_2_n_6 ));
  LUT6 #(
    .INIT(64'hFFFF0000E2000000)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(LAST_STREAM_V_last_V_0_payload_A),
        .I1(LAST_STREAM_V_last_V_0_sel),
        .I2(LAST_STREAM_V_last_V_0_payload_B),
        .I3(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .I4(ap_CS_fsm_state4),
        .I5(tmp_4_fu_266_p3),
        .O(ap_NS_fsm[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAA22A2A2)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(ap_enable_reg_pp2_iter0),
        .I2(\ap_CS_fsm[6]_i_2_n_6 ),
        .I3(\ap_CS_fsm[6]_i_3_n_6 ),
        .I4(lastValues_0_U_n_9),
        .I5(ap_CS_fsm_state5),
        .O(ap_NS_fsm[5]));
  LUT6 #(
    .INIT(64'h3353000000000000)) 
    \ap_CS_fsm[6]_i_1 
       (.I0(\ap_CS_fsm[6]_i_2_n_6 ),
        .I1(\ap_CS_fsm[6]_i_3_n_6 ),
        .I2(ap_enable_reg_pp2_iter1),
        .I3(exitcond_reg_360),
        .I4(ap_CS_fsm_pp2_stage0),
        .I5(ap_enable_reg_pp2_iter0),
        .O(ap_NS_fsm[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \ap_CS_fsm[6]_i_2 
       (.I0(i_5_reg_364_reg__0[4]),
        .I1(i_5_reg_364_reg__0[5]),
        .I2(i_5_reg_364_reg__0[2]),
        .I3(i_5_reg_364_reg__0[3]),
        .I4(i_5_reg_364_reg__0[1]),
        .I5(i_5_reg_364_reg__0[0]),
        .O(\ap_CS_fsm[6]_i_2_n_6 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \ap_CS_fsm[6]_i_3 
       (.I0(\i_2_reg_226_reg_n_6_[4] ),
        .I1(\i_2_reg_226_reg_n_6_[5] ),
        .I2(\i_2_reg_226_reg_n_6_[2] ),
        .I3(\i_2_reg_226_reg_n_6_[3] ),
        .I4(\i_2_reg_226_reg_n_6_[1] ),
        .I5(\i_2_reg_226_reg_n_6_[0] ),
        .O(\ap_CS_fsm[6]_i_3_n_6 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_6_[0] ),
        .S(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_pp2_stage0),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[6]),
        .Q(agg_result_a_ap_vld),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hA2AAA2A2A222A2A2)) 
    ap_enable_reg_pp2_iter0_i_1
       (.I0(ap_enable_reg_pp2_iter0_i_2_n_6),
        .I1(ap_CS_fsm_pp2_stage0),
        .I2(\ap_CS_fsm[6]_i_3_n_6 ),
        .I3(exitcond_reg_360),
        .I4(ap_enable_reg_pp2_iter1),
        .I5(\ap_CS_fsm[6]_i_2_n_6 ),
        .O(ap_enable_reg_pp2_iter0_i_1_n_6));
  LUT3 #(
    .INIT(8'hE0)) 
    ap_enable_reg_pp2_iter0_i_2
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(ap_CS_fsm_state5),
        .I2(ap_rst_n),
        .O(ap_enable_reg_pp2_iter0_i_2_n_6));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp2_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp2_iter0_i_1_n_6),
        .Q(ap_enable_reg_pp2_iter0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hC000A000)) 
    ap_enable_reg_pp2_iter1_i_1
       (.I0(\ap_CS_fsm[6]_i_2_n_6 ),
        .I1(\ap_CS_fsm[6]_i_3_n_6 ),
        .I2(ap_enable_reg_pp2_iter0),
        .I3(ap_rst_n),
        .I4(\i_5_reg_364[5]_i_5_n_6 ),
        .O(ap_enable_reg_pp2_iter1_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp2_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp2_iter1_i_1_n_6),
        .Q(ap_enable_reg_pp2_iter1),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h55FF1D00)) 
    \exitcond_reg_360[0]_i_1 
       (.I0(\ap_CS_fsm[6]_i_3_n_6 ),
        .I1(ap_enable_reg_pp2_iter1),
        .I2(\ap_CS_fsm[6]_i_2_n_6 ),
        .I3(ap_CS_fsm_pp2_stage0),
        .I4(exitcond_reg_360),
        .O(\exitcond_reg_360[0]_i_1_n_6 ));
  FDRE \exitcond_reg_360_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_360[0]_i_1_n_6 ),
        .Q(exitcond_reg_360),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_1_reg_215[0]_i_1 
       (.I0(\i_1_reg_215_reg_n_6_[0] ),
        .O(i_4_fu_274_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_1_reg_215[1]_i_1 
       (.I0(\i_1_reg_215_reg_n_6_[0] ),
        .I1(\i_1_reg_215_reg_n_6_[1] ),
        .O(i_4_fu_274_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_1_reg_215[2]_i_1 
       (.I0(\i_1_reg_215_reg_n_6_[1] ),
        .I1(\i_1_reg_215_reg_n_6_[0] ),
        .I2(\i_1_reg_215_reg_n_6_[2] ),
        .O(i_4_fu_274_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_1_reg_215[3]_i_1 
       (.I0(\i_1_reg_215_reg_n_6_[2] ),
        .I1(\i_1_reg_215_reg_n_6_[0] ),
        .I2(\i_1_reg_215_reg_n_6_[1] ),
        .I3(\i_1_reg_215_reg_n_6_[3] ),
        .O(i_4_fu_274_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_1_reg_215[4]_i_1 
       (.I0(\i_1_reg_215_reg_n_6_[0] ),
        .I1(\i_1_reg_215_reg_n_6_[1] ),
        .I2(\i_1_reg_215_reg_n_6_[2] ),
        .I3(\i_1_reg_215_reg_n_6_[3] ),
        .I4(\i_1_reg_215_reg_n_6_[4] ),
        .O(i_4_fu_274_p2[4]));
  LUT5 #(
    .INIT(32'hFFB80000)) 
    \i_1_reg_215[5]_i_1 
       (.I0(LAST_STREAM_V_last_V_0_payload_B),
        .I1(LAST_STREAM_V_last_V_0_sel),
        .I2(LAST_STREAM_V_last_V_0_payload_A),
        .I3(\i_1_reg_215[5]_i_4_n_6 ),
        .I4(ap_CS_fsm_state3),
        .O(i_1_reg_215));
  LUT6 #(
    .INIT(64'h0000470000000000)) 
    \i_1_reg_215[5]_i_2 
       (.I0(LAST_STREAM_V_last_V_0_payload_B),
        .I1(LAST_STREAM_V_last_V_0_sel),
        .I2(LAST_STREAM_V_last_V_0_payload_A),
        .I3(ap_CS_fsm_state4),
        .I4(tmp_4_fu_266_p3),
        .I5(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .O(ap_NS_fsm12_out));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_1_reg_215[5]_i_3 
       (.I0(\i_1_reg_215_reg_n_6_[0] ),
        .I1(\i_1_reg_215_reg_n_6_[1] ),
        .I2(\i_1_reg_215_reg_n_6_[4] ),
        .I3(\i_1_reg_215_reg_n_6_[3] ),
        .I4(\i_1_reg_215_reg_n_6_[2] ),
        .O(i_4_fu_274_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \i_1_reg_215[5]_i_4 
       (.I0(ap_CS_fsm_state4),
        .I1(tmp_4_fu_266_p3),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .O(\i_1_reg_215[5]_i_4_n_6 ));
  FDRE \i_1_reg_215_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm12_out),
        .D(i_4_fu_274_p2[0]),
        .Q(\i_1_reg_215_reg_n_6_[0] ),
        .R(i_1_reg_215));
  FDRE \i_1_reg_215_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm12_out),
        .D(i_4_fu_274_p2[1]),
        .Q(\i_1_reg_215_reg_n_6_[1] ),
        .R(i_1_reg_215));
  FDRE \i_1_reg_215_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm12_out),
        .D(i_4_fu_274_p2[2]),
        .Q(\i_1_reg_215_reg_n_6_[2] ),
        .R(i_1_reg_215));
  FDRE \i_1_reg_215_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm12_out),
        .D(i_4_fu_274_p2[3]),
        .Q(\i_1_reg_215_reg_n_6_[3] ),
        .R(i_1_reg_215));
  FDRE \i_1_reg_215_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm12_out),
        .D(i_4_fu_274_p2[4]),
        .Q(\i_1_reg_215_reg_n_6_[4] ),
        .R(i_1_reg_215));
  FDRE \i_1_reg_215_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm12_out),
        .D(i_4_fu_274_p2[5]),
        .Q(tmp_4_fu_266_p3),
        .R(i_1_reg_215));
  LUT4 #(
    .INIT(16'hDF00)) 
    \i_2_reg_226[5]_i_1 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(exitcond_reg_360),
        .I2(ap_enable_reg_pp2_iter1),
        .I3(ap_CS_fsm_state5),
        .O(i_2_reg_226));
  LUT3 #(
    .INIT(8'h20)) 
    \i_2_reg_226[5]_i_2 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(exitcond_reg_360),
        .I2(ap_enable_reg_pp2_iter1),
        .O(ap_phi_mux_i_2_phi_fu_230_p41));
  FDRE \i_2_reg_226_reg[0] 
       (.C(ap_clk),
        .CE(ap_phi_mux_i_2_phi_fu_230_p41),
        .D(i_5_reg_364_reg__0[0]),
        .Q(\i_2_reg_226_reg_n_6_[0] ),
        .R(i_2_reg_226));
  FDRE \i_2_reg_226_reg[1] 
       (.C(ap_clk),
        .CE(ap_phi_mux_i_2_phi_fu_230_p41),
        .D(i_5_reg_364_reg__0[1]),
        .Q(\i_2_reg_226_reg_n_6_[1] ),
        .R(i_2_reg_226));
  FDRE \i_2_reg_226_reg[2] 
       (.C(ap_clk),
        .CE(ap_phi_mux_i_2_phi_fu_230_p41),
        .D(i_5_reg_364_reg__0[2]),
        .Q(\i_2_reg_226_reg_n_6_[2] ),
        .R(i_2_reg_226));
  FDRE \i_2_reg_226_reg[3] 
       (.C(ap_clk),
        .CE(ap_phi_mux_i_2_phi_fu_230_p41),
        .D(i_5_reg_364_reg__0[3]),
        .Q(\i_2_reg_226_reg_n_6_[3] ),
        .R(i_2_reg_226));
  FDRE \i_2_reg_226_reg[4] 
       (.C(ap_clk),
        .CE(ap_phi_mux_i_2_phi_fu_230_p41),
        .D(i_5_reg_364_reg__0[4]),
        .Q(\i_2_reg_226_reg_n_6_[4] ),
        .R(i_2_reg_226));
  FDRE \i_2_reg_226_reg[5] 
       (.C(ap_clk),
        .CE(ap_phi_mux_i_2_phi_fu_230_p41),
        .D(i_5_reg_364_reg__0[5]),
        .Q(\i_2_reg_226_reg_n_6_[5] ),
        .R(i_2_reg_226));
  LUT4 #(
    .INIT(16'h515D)) 
    \i_5_reg_364[0]_i_1 
       (.I0(\i_2_reg_226_reg_n_6_[0] ),
        .I1(ap_enable_reg_pp2_iter1),
        .I2(exitcond_reg_360),
        .I3(i_5_reg_364_reg__0[0]),
        .O(i_5_fu_300_p2[0]));
  LUT5 #(
    .INIT(32'h553CAA3C)) 
    \i_5_reg_364[1]_i_1 
       (.I0(\i_2_reg_226_reg_n_6_[0] ),
        .I1(i_5_reg_364_reg__0[0]),
        .I2(i_5_reg_364_reg__0[1]),
        .I3(\i_5_reg_364[5]_i_5_n_6 ),
        .I4(\i_2_reg_226_reg_n_6_[1] ),
        .O(i_5_fu_300_p2[1]));
  LUT6 #(
    .INIT(64'h555515EAAAAA15EA)) 
    \i_5_reg_364[2]_i_1 
       (.I0(\i_5_reg_364[2]_i_2_n_6 ),
        .I1(i_5_reg_364_reg__0[0]),
        .I2(i_5_reg_364_reg__0[1]),
        .I3(i_5_reg_364_reg__0[2]),
        .I4(\i_5_reg_364[5]_i_5_n_6 ),
        .I5(\i_2_reg_226_reg_n_6_[2] ),
        .O(i_5_fu_300_p2[2]));
  LUT5 #(
    .INIT(32'hA2AA0000)) 
    \i_5_reg_364[2]_i_2 
       (.I0(\i_2_reg_226_reg_n_6_[0] ),
        .I1(ap_enable_reg_pp2_iter1),
        .I2(exitcond_reg_360),
        .I3(ap_CS_fsm_pp2_stage0),
        .I4(\i_2_reg_226_reg_n_6_[1] ),
        .O(\i_5_reg_364[2]_i_2_n_6 ));
  LUT6 #(
    .INIT(64'h151515EAEAEA15EA)) 
    \i_5_reg_364[3]_i_1 
       (.I0(\i_5_reg_364[3]_i_2_n_6 ),
        .I1(\i_5_reg_364[3]_i_3_n_6 ),
        .I2(i_5_reg_364_reg__0[2]),
        .I3(i_5_reg_364_reg__0[3]),
        .I4(\i_5_reg_364[5]_i_5_n_6 ),
        .I5(\i_2_reg_226_reg_n_6_[3] ),
        .O(i_5_fu_300_p2[3]));
  LUT6 #(
    .INIT(64'hA2AA000000000000)) 
    \i_5_reg_364[3]_i_2 
       (.I0(\i_2_reg_226_reg_n_6_[1] ),
        .I1(ap_CS_fsm_pp2_stage0),
        .I2(exitcond_reg_360),
        .I3(ap_enable_reg_pp2_iter1),
        .I4(\i_2_reg_226_reg_n_6_[0] ),
        .I5(\i_2_reg_226_reg_n_6_[2] ),
        .O(\i_5_reg_364[3]_i_2_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h20000000)) 
    \i_5_reg_364[3]_i_3 
       (.I0(ap_enable_reg_pp2_iter1),
        .I1(exitcond_reg_360),
        .I2(ap_CS_fsm_pp2_stage0),
        .I3(i_5_reg_364_reg__0[0]),
        .I4(i_5_reg_364_reg__0[1]),
        .O(\i_5_reg_364[3]_i_3_n_6 ));
  LUT5 #(
    .INIT(32'h5655A6AA)) 
    \i_5_reg_364[4]_i_1 
       (.I0(\i_5_reg_364[4]_i_2_n_6 ),
        .I1(i_5_reg_364_reg__0[4]),
        .I2(exitcond_reg_360),
        .I3(ap_enable_reg_pp2_iter1),
        .I4(\i_2_reg_226_reg_n_6_[4] ),
        .O(i_5_fu_300_p2[4]));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \i_5_reg_364[4]_i_2 
       (.I0(\i_5_reg_364[3]_i_3_n_6 ),
        .I1(i_5_reg_364_reg__0[2]),
        .I2(i_5_reg_364_reg__0[3]),
        .I3(\i_5_reg_364[2]_i_2_n_6 ),
        .I4(\i_2_reg_226_reg_n_6_[2] ),
        .I5(\i_2_reg_226_reg_n_6_[3] ),
        .O(\i_5_reg_364[4]_i_2_n_6 ));
  LUT2 #(
    .INIT(4'h8)) 
    \i_5_reg_364[5]_i_1 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(ap_enable_reg_pp2_iter0),
        .O(i_5_reg_3640));
  LUT5 #(
    .INIT(32'h111EEE1E)) 
    \i_5_reg_364[5]_i_2 
       (.I0(\i_5_reg_364[5]_i_3_n_6 ),
        .I1(\i_5_reg_364[5]_i_4_n_6 ),
        .I2(i_5_reg_364_reg__0[5]),
        .I3(\i_5_reg_364[5]_i_5_n_6 ),
        .I4(\i_2_reg_226_reg_n_6_[5] ),
        .O(i_5_fu_300_p2[5]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \i_5_reg_364[5]_i_3 
       (.I0(\i_2_reg_226_reg_n_6_[2] ),
        .I1(\i_2_reg_226_reg_n_6_[3] ),
        .I2(\i_2_reg_226_reg_n_6_[4] ),
        .I3(\i_2_reg_226_reg_n_6_[1] ),
        .I4(\i_5_reg_364[5]_i_5_n_6 ),
        .I5(\i_2_reg_226_reg_n_6_[0] ),
        .O(\i_5_reg_364[5]_i_3_n_6 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \i_5_reg_364[5]_i_4 
       (.I0(i_5_reg_364_reg__0[2]),
        .I1(i_5_reg_364_reg__0[3]),
        .I2(i_5_reg_364_reg__0[4]),
        .I3(i_5_reg_364_reg__0[1]),
        .I4(i_5_reg_364_reg__0[0]),
        .I5(\i_5_reg_364[5]_i_5_n_6 ),
        .O(\i_5_reg_364[5]_i_4_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \i_5_reg_364[5]_i_5 
       (.I0(ap_enable_reg_pp2_iter1),
        .I1(exitcond_reg_360),
        .I2(ap_CS_fsm_pp2_stage0),
        .O(\i_5_reg_364[5]_i_5_n_6 ));
  FDRE \i_5_reg_364_reg[0] 
       (.C(ap_clk),
        .CE(i_5_reg_3640),
        .D(i_5_fu_300_p2[0]),
        .Q(i_5_reg_364_reg__0[0]),
        .R(1'b0));
  FDRE \i_5_reg_364_reg[1] 
       (.C(ap_clk),
        .CE(i_5_reg_3640),
        .D(i_5_fu_300_p2[1]),
        .Q(i_5_reg_364_reg__0[1]),
        .R(1'b0));
  FDRE \i_5_reg_364_reg[2] 
       (.C(ap_clk),
        .CE(i_5_reg_3640),
        .D(i_5_fu_300_p2[2]),
        .Q(i_5_reg_364_reg__0[2]),
        .R(1'b0));
  FDRE \i_5_reg_364_reg[3] 
       (.C(ap_clk),
        .CE(i_5_reg_3640),
        .D(i_5_fu_300_p2[3]),
        .Q(i_5_reg_364_reg__0[3]),
        .R(1'b0));
  FDRE \i_5_reg_364_reg[4] 
       (.C(ap_clk),
        .CE(i_5_reg_3640),
        .D(i_5_fu_300_p2[4]),
        .Q(i_5_reg_364_reg__0[4]),
        .R(1'b0));
  FDRE \i_5_reg_364_reg[5] 
       (.C(ap_clk),
        .CE(i_5_reg_3640),
        .D(i_5_fu_300_p2[5]),
        .Q(i_5_reg_364_reg__0[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_204[0]_i_1 
       (.I0(\i_reg_204_reg_n_6_[0] ),
        .O(i_3_fu_246_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_reg_204[1]_i_1 
       (.I0(\i_reg_204_reg_n_6_[0] ),
        .I1(\i_reg_204_reg_n_6_[1] ),
        .O(i_3_fu_246_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_reg_204[2]_i_1 
       (.I0(\i_reg_204_reg_n_6_[1] ),
        .I1(\i_reg_204_reg_n_6_[0] ),
        .I2(\i_reg_204_reg_n_6_[2] ),
        .O(i_3_fu_246_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_reg_204[3]_i_1 
       (.I0(\i_reg_204_reg_n_6_[2] ),
        .I1(\i_reg_204_reg_n_6_[0] ),
        .I2(\i_reg_204_reg_n_6_[1] ),
        .I3(\i_reg_204_reg_n_6_[3] ),
        .O(i_3_fu_246_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_reg_204[4]_i_1 
       (.I0(\i_reg_204_reg_n_6_[0] ),
        .I1(\i_reg_204_reg_n_6_[1] ),
        .I2(\i_reg_204_reg_n_6_[2] ),
        .I3(\i_reg_204_reg_n_6_[3] ),
        .I4(\i_reg_204_reg_n_6_[4] ),
        .O(i_3_fu_246_p2[4]));
  LUT6 #(
    .INIT(64'h0000470000000000)) 
    \i_reg_204[5]_i_2 
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(ap_CS_fsm_state2),
        .I4(tmp_fu_238_p3),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .O(ap_NS_fsm18_out));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_reg_204[5]_i_3 
       (.I0(\i_reg_204_reg_n_6_[0] ),
        .I1(\i_reg_204_reg_n_6_[1] ),
        .I2(\i_reg_204_reg_n_6_[4] ),
        .I3(\i_reg_204_reg_n_6_[3] ),
        .I4(\i_reg_204_reg_n_6_[2] ),
        .O(i_3_fu_246_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \i_reg_204[5]_i_4 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_238_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .O(\i_reg_204[5]_i_4_n_6 ));
  FDRE \i_reg_204_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(i_3_fu_246_p2[0]),
        .Q(\i_reg_204_reg_n_6_[0] ),
        .R(i_reg_204));
  FDRE \i_reg_204_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(i_3_fu_246_p2[1]),
        .Q(\i_reg_204_reg_n_6_[1] ),
        .R(i_reg_204));
  FDRE \i_reg_204_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(i_3_fu_246_p2[2]),
        .Q(\i_reg_204_reg_n_6_[2] ),
        .R(i_reg_204));
  FDRE \i_reg_204_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(i_3_fu_246_p2[3]),
        .Q(\i_reg_204_reg_n_6_[3] ),
        .R(i_reg_204));
  FDRE \i_reg_204_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(i_3_fu_246_p2[4]),
        .Q(\i_reg_204_reg_n_6_[4] ),
        .R(i_reg_204));
  FDRE \i_reg_204_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(i_3_fu_246_p2[5]),
        .Q(tmp_fu_238_p3),
        .R(i_reg_204));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb inputValues_0_U
       (.DOBDO(lastValues_0_q0),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (INPUT_STREAM_V_data_V_0_payload_A),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (INPUT_STREAM_V_data_V_0_payload_B),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .Q({tmp_fu_238_p3,\i_reg_204_reg_n_6_[4] ,\i_reg_204_reg_n_6_[3] ,\i_reg_204_reg_n_6_[2] ,\i_reg_204_reg_n_6_[1] ,\i_reg_204_reg_n_6_[0] }),
        .S(inputValues_0_U_n_36),
        .\ap_CS_fsm_reg[5] ({ap_CS_fsm_pp2_stage0,ap_CS_fsm_state2}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp2_iter0(ap_enable_reg_pp2_iter0),
        .\exitcond_reg_360_reg[0] (lastValues_0_U_n_9),
        .\i_2_reg_226_reg[5] ({\i_2_reg_226_reg_n_6_[5] ,\i_2_reg_226_reg_n_6_[4] ,\i_2_reg_226_reg_n_6_[3] ,\i_2_reg_226_reg_n_6_[2] ,\i_2_reg_226_reg_n_6_[1] ,\i_2_reg_226_reg_n_6_[0] }),
        .\i_5_reg_364_reg[5] (i_5_reg_364_reg__0),
        .\last_reg[0] (inputValues_0_q0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_0 lastValues_0_U
       (.CO(tmp_9_fu_316_p2),
        .DOBDO(lastValues_0_q0),
        .\LAST_STREAM_V_data_V_0_payload_A_reg[31] (LAST_STREAM_V_data_V_0_payload_A),
        .\LAST_STREAM_V_data_V_0_payload_B_reg[31] (LAST_STREAM_V_data_V_0_payload_B),
        .LAST_STREAM_V_data_V_0_sel(LAST_STREAM_V_data_V_0_sel),
        .\LAST_STREAM_V_data_V_0_state_reg[0] (\LAST_STREAM_V_data_V_0_state_reg_n_6_[0] ),
        .Q({tmp_4_fu_266_p3,\i_1_reg_215_reg_n_6_[4] ,\i_1_reg_215_reg_n_6_[3] ,\i_1_reg_215_reg_n_6_[2] ,\i_1_reg_215_reg_n_6_[1] ,\i_1_reg_215_reg_n_6_[0] }),
        .S(inputValues_0_U_n_36),
        .\ap_CS_fsm_reg[5] ({ap_CS_fsm_pp2_stage0,ap_CS_fsm_state4}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp2_iter0(ap_enable_reg_pp2_iter0),
        .ap_enable_reg_pp2_iter1(ap_enable_reg_pp2_iter1),
        .exitcond_reg_360(exitcond_reg_360),
        .\i_2_reg_226_reg[5] ({\i_2_reg_226_reg_n_6_[5] ,\i_2_reg_226_reg_n_6_[4] ,\i_2_reg_226_reg_n_6_[3] ,\i_2_reg_226_reg_n_6_[2] ,\i_2_reg_226_reg_n_6_[1] ,\i_2_reg_226_reg_n_6_[0] }),
        .\i_5_reg_364_reg[5] (i_5_reg_364_reg__0),
        .ram_reg(lastValues_0_U_n_9),
        .ram_reg_0(inputValues_0_q0));
  LUT3 #(
    .INIT(8'h08)) 
    \last[15]_i_1 
       (.I0(ap_CS_fsm_pp2_stage0),
        .I1(ap_enable_reg_pp2_iter1),
        .I2(tmp_9_fu_316_p2),
        .O(last1));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[0] 
       (.C(ap_clk),
        .CE(last1),
        .D(\i_2_reg_226_reg_n_6_[0] ),
        .Q(last[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[15] 
       (.C(ap_clk),
        .CE(last1),
        .D(1'b0),
        .Q(last[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[1] 
       (.C(ap_clk),
        .CE(last1),
        .D(\i_2_reg_226_reg_n_6_[1] ),
        .Q(last[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[2] 
       (.C(ap_clk),
        .CE(last1),
        .D(\i_2_reg_226_reg_n_6_[2] ),
        .Q(last[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[3] 
       (.C(ap_clk),
        .CE(last1),
        .D(\i_2_reg_226_reg_n_6_[3] ),
        .Q(last[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[4] 
       (.C(ap_clk),
        .CE(last1),
        .D(\i_2_reg_226_reg_n_6_[4] ),
        .Q(last[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[5] 
       (.C(ap_clk),
        .CE(last1),
        .D(\i_2_reg_226_reg_n_6_[5] ),
        .Q(last[5]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[0] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[0]),
        .Q(searched_read_reg_333[0]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[10] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[10]),
        .Q(searched_read_reg_333[10]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[11] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[11]),
        .Q(searched_read_reg_333[11]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[12] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[12]),
        .Q(searched_read_reg_333[12]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[13] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[13]),
        .Q(searched_read_reg_333[13]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[14] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[14]),
        .Q(searched_read_reg_333[14]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[15] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[15]),
        .Q(searched_read_reg_333[15]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[16] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[16]),
        .Q(searched_read_reg_333[16]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[17] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[17]),
        .Q(searched_read_reg_333[17]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[18] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[18]),
        .Q(searched_read_reg_333[18]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[19] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[19]),
        .Q(searched_read_reg_333[19]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[1] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[1]),
        .Q(searched_read_reg_333[1]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[20] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[20]),
        .Q(searched_read_reg_333[20]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[21] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[21]),
        .Q(searched_read_reg_333[21]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[22] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[22]),
        .Q(searched_read_reg_333[22]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[23] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[23]),
        .Q(searched_read_reg_333[23]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[24] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[24]),
        .Q(searched_read_reg_333[24]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[25] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[25]),
        .Q(searched_read_reg_333[25]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[26] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[26]),
        .Q(searched_read_reg_333[26]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[27] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[27]),
        .Q(searched_read_reg_333[27]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[28] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[28]),
        .Q(searched_read_reg_333[28]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[29] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[29]),
        .Q(searched_read_reg_333[29]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[2] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[2]),
        .Q(searched_read_reg_333[2]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[30] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[30]),
        .Q(searched_read_reg_333[30]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[31] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[31]),
        .Q(searched_read_reg_333[31]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[3] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[3]),
        .Q(searched_read_reg_333[3]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[4] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[4]),
        .Q(searched_read_reg_333[4]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[5] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[5]),
        .Q(searched_read_reg_333[5]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[6] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[6]),
        .Q(searched_read_reg_333[6]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[7] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[7]),
        .Q(searched_read_reg_333[7]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[8] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[8]),
        .Q(searched_read_reg_333[8]),
        .R(1'b0));
  FDRE \searched_read_reg_333_reg[9] 
       (.C(ap_clk),
        .CE(Adder2_CONTROL_BUS_s_axi_U_n_45),
        .D(searched[9]),
        .Q(searched_read_reg_333[9]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (ARESET,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    \searched_read_reg_333_reg[31] ,
    D,
    E,
    SR,
    interrupt,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_ARADDR,
    ap_rst_n,
    \i_reg_204_reg[5] ,
    INPUT_STREAM_V_last_V_0_payload_A,
    INPUT_STREAM_V_last_V_0_sel,
    INPUT_STREAM_V_last_V_0_payload_B,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \ap_CS_fsm_reg[1] ,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR,
    \last_reg[15] ,
    \searched_read_reg_333_reg[31]_0 );
  output ARESET;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [31:0]\searched_read_reg_333_reg[31] ;
  output [1:0]D;
  output [0:0]E;
  output [0:0]SR;
  output interrupt;
  output s_axi_CONTROL_BUS_RVALID;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [1:0]Q;
  input s_axi_CONTROL_BUS_RREADY;
  input s_axi_CONTROL_BUS_ARVALID;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  input ap_rst_n;
  input \i_reg_204_reg[5] ;
  input INPUT_STREAM_V_last_V_0_payload_A;
  input INPUT_STREAM_V_last_V_0_sel;
  input INPUT_STREAM_V_last_V_0_payload_B;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input \ap_CS_fsm_reg[1] ;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input [6:0]\last_reg[15] ;
  input [31:0]\searched_read_reg_333_reg[31]_0 ;

  wire \/FSM_onehot_wstate[1]_i_1_n_6 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_6 ;
  wire ARESET;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_wstate[3]_i_1_n_6 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_6_[0] ;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire [1:0]Q;
  wire [0:0]SR;
  wire \ap_CS_fsm_reg[1] ;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire \i_reg_204_reg[5] ;
  wire [15:0]int_agg_result_a;
  wire int_agg_result_a_ap_vld;
  wire int_agg_result_a_ap_vld_i_1_n_6;
  wire int_agg_result_a_ap_vld_i_2_n_6;
  wire [31:0]int_agg_result_b;
  wire int_agg_result_b_ap_vld;
  wire int_agg_result_b_ap_vld_i_1_n_6;
  wire int_ap_done;
  wire int_ap_done_i_1_n_6;
  wire int_ap_idle;
  wire int_ap_ready;
  wire int_ap_start_i_1_n_6;
  wire int_ap_start_i_2_n_6;
  wire int_ap_start_i_3_n_6;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_6;
  wire int_gie_i_1_n_6;
  wire int_gie_reg_n_6;
  wire \int_ier[0]_i_1_n_6 ;
  wire \int_ier[1]_i_1_n_6 ;
  wire \int_ier_reg_n_6_[0] ;
  wire int_isr;
  wire int_isr7_out;
  wire \int_isr[0]_i_1_n_6 ;
  wire \int_isr[1]_i_1_n_6 ;
  wire \int_isr_reg_n_6_[0] ;
  wire \int_searched[31]_i_3_n_6 ;
  wire interrupt;
  wire [6:0]\last_reg[15] ;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in13_out;
  wire p_1_in;
  wire [31:0]rdata_data;
  wire \rdata_data[0]_i_2_n_6 ;
  wire \rdata_data[0]_i_3_n_6 ;
  wire \rdata_data[0]_i_4_n_6 ;
  wire \rdata_data[0]_i_5_n_6 ;
  wire \rdata_data[0]_i_6_n_6 ;
  wire \rdata_data[15]_i_2_n_6 ;
  wire \rdata_data[1]_i_2_n_6 ;
  wire \rdata_data[1]_i_3_n_6 ;
  wire \rdata_data[1]_i_4_n_6 ;
  wire \rdata_data[2]_i_2_n_6 ;
  wire \rdata_data[31]_i_3_n_6 ;
  wire \rdata_data[31]_i_4_n_6 ;
  wire \rdata_data[3]_i_2_n_6 ;
  wire \rdata_data[3]_i_3_n_6 ;
  wire \rdata_data[7]_i_2_n_6 ;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_6 ;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]\searched_read_reg_333_reg[31] ;
  wire [31:0]\searched_read_reg_333_reg[31]_0 ;
  wire waddr;
  wire \waddr_reg_n_6_[0] ;
  wire \waddr_reg_n_6_[1] ;
  wire \waddr_reg_n_6_[2] ;
  wire \waddr_reg_n_6_[3] ;
  wire \waddr_reg_n_6_[4] ;
  wire \waddr_reg_n_6_[5] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_6 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_6 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_6 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_6_[0] ),
        .S(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_6 ),
        .Q(out[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_6 ),
        .Q(out[1]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_6 ),
        .Q(out[2]),
        .R(ARESET));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hAAABBBABBBBBBBBB)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(E),
        .I1(\i_reg_204_reg[5] ),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(INPUT_STREAM_V_last_V_0_sel),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h8888888880888000)) 
    \i_reg_204[5]_i_1 
       (.I0(Q[0]),
        .I1(ap_start),
        .I2(INPUT_STREAM_V_last_V_0_payload_B),
        .I3(INPUT_STREAM_V_last_V_0_sel),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .I5(\ap_CS_fsm_reg[1] ),
        .O(SR));
  LUT5 #(
    .INIT(32'hFFF7FF00)) 
    int_agg_result_a_ap_vld_i_1
       (.I0(int_agg_result_a_ap_vld_i_2_n_6),
        .I1(ar_hs),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(Q[1]),
        .I4(int_agg_result_a_ap_vld),
        .O(int_agg_result_a_ap_vld_i_1_n_6));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00100000)) 
    int_agg_result_a_ap_vld_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(int_agg_result_a_ap_vld_i_2_n_6));
  FDRE int_agg_result_a_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_a_ap_vld_i_1_n_6),
        .Q(int_agg_result_a_ap_vld),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[0] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [0]),
        .Q(int_agg_result_a[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[15] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [6]),
        .Q(int_agg_result_a[15]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[1] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [1]),
        .Q(int_agg_result_a[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[2] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [2]),
        .Q(int_agg_result_a[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[3] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [3]),
        .Q(int_agg_result_a[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[4] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [4]),
        .Q(int_agg_result_a[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[5] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\last_reg[15] [5]),
        .Q(int_agg_result_a[5]),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF7FFF00)) 
    int_agg_result_b_ap_vld_i_1
       (.I0(int_agg_result_a_ap_vld_i_2_n_6),
        .I1(ar_hs),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(Q[1]),
        .I4(int_agg_result_b_ap_vld),
        .O(int_agg_result_b_ap_vld_i_1_n_6));
  FDRE int_agg_result_b_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_b_ap_vld_i_1_n_6),
        .Q(int_agg_result_b_ap_vld),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[0] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [0]),
        .Q(int_agg_result_b[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[10] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [10]),
        .Q(int_agg_result_b[10]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[11] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [11]),
        .Q(int_agg_result_b[11]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[12] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [12]),
        .Q(int_agg_result_b[12]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[13] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [13]),
        .Q(int_agg_result_b[13]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[14] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [14]),
        .Q(int_agg_result_b[14]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[15] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [15]),
        .Q(int_agg_result_b[15]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[16] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [16]),
        .Q(int_agg_result_b[16]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[17] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [17]),
        .Q(int_agg_result_b[17]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[18] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [18]),
        .Q(int_agg_result_b[18]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[19] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [19]),
        .Q(int_agg_result_b[19]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[1] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [1]),
        .Q(int_agg_result_b[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[20] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [20]),
        .Q(int_agg_result_b[20]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[21] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [21]),
        .Q(int_agg_result_b[21]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[22] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [22]),
        .Q(int_agg_result_b[22]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[23] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [23]),
        .Q(int_agg_result_b[23]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[24] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [24]),
        .Q(int_agg_result_b[24]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[25] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [25]),
        .Q(int_agg_result_b[25]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[26] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [26]),
        .Q(int_agg_result_b[26]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[27] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [27]),
        .Q(int_agg_result_b[27]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[28] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [28]),
        .Q(int_agg_result_b[28]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[29] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [29]),
        .Q(int_agg_result_b[29]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[2] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [2]),
        .Q(int_agg_result_b[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[30] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [30]),
        .Q(int_agg_result_b[30]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[31] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [31]),
        .Q(int_agg_result_b[31]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[3] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [3]),
        .Q(int_agg_result_b[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[4] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [4]),
        .Q(int_agg_result_b[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[5] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [5]),
        .Q(int_agg_result_b[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[6] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [6]),
        .Q(int_agg_result_b[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[7] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [7]),
        .Q(int_agg_result_b[7]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[8] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [8]),
        .Q(int_agg_result_b[8]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[9] 
       (.C(ap_clk),
        .CE(Q[1]),
        .D(\searched_read_reg_333_reg[31]_0 [9]),
        .Q(int_agg_result_b[9]),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFFFDFFFFFF0000)) 
    int_ap_done_i_1
       (.I0(\rdata_data[0]_i_5_n_6 ),
        .I1(rstate[1]),
        .I2(rstate[0]),
        .I3(s_axi_CONTROL_BUS_ARVALID),
        .I4(Q[1]),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_6),
        .Q(int_ap_done),
        .R(ARESET));
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(ARESET));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[1]),
        .Q(int_ap_ready),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[1]),
        .I2(int_ap_start_i_2_n_6),
        .I3(int_ap_start_i_3_n_6),
        .I4(ap_start),
        .O(int_ap_start_i_1_n_6));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h40)) 
    int_ap_start_i_2
       (.I0(\waddr_reg_n_6_[5] ),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched[31]_i_3_n_6 ),
        .O(int_ap_start_i_2_n_6));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h04)) 
    int_ap_start_i_3
       (.I0(\waddr_reg_n_6_[3] ),
        .I1(s_axi_CONTROL_BUS_WDATA[0]),
        .I2(\waddr_reg_n_6_[2] ),
        .O(int_ap_start_i_3_n_6));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_6),
        .Q(ap_start),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFEFF0200)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(\waddr_reg_n_6_[3] ),
        .I2(\waddr_reg_n_6_[2] ),
        .I3(int_ap_start_i_2_n_6),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_6),
        .Q(int_auto_restart),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_6_[3] ),
        .I2(\waddr_reg_n_6_[2] ),
        .I3(int_ap_start_i_2_n_6),
        .I4(int_gie_reg_n_6),
        .O(int_gie_i_1_n_6));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_6),
        .Q(int_gie_reg_n_6),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_6_[2] ),
        .I2(\waddr_reg_n_6_[3] ),
        .I3(int_ap_start_i_2_n_6),
        .I4(\int_ier_reg_n_6_[0] ),
        .O(\int_ier[0]_i_1_n_6 ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_6_[2] ),
        .I2(\waddr_reg_n_6_[3] ),
        .I3(int_ap_start_i_2_n_6),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_6 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_6 ),
        .Q(\int_ier_reg_n_6_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_6 ),
        .Q(p_0_in),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFF7FFFFFFF8000)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_ap_start_i_2_n_6),
        .I2(\waddr_reg_n_6_[2] ),
        .I3(\waddr_reg_n_6_[3] ),
        .I4(int_isr7_out),
        .I5(\int_isr_reg_n_6_[0] ),
        .O(\int_isr[0]_i_1_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_isr[0]_i_2 
       (.I0(\int_ier_reg_n_6_[0] ),
        .I1(Q[1]),
        .O(int_isr7_out));
  LUT6 #(
    .INIT(64'hFFFF7FFFFFFF8000)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_ap_start_i_2_n_6),
        .I2(\waddr_reg_n_6_[2] ),
        .I3(\waddr_reg_n_6_[3] ),
        .I4(int_isr),
        .I5(p_1_in),
        .O(\int_isr[1]_i_1_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_isr[1]_i_2 
       (.I0(p_0_in),
        .I1(Q[1]),
        .O(int_isr));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_6 ),
        .Q(\int_isr_reg_n_6_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_6 ),
        .Q(p_1_in),
        .R(ARESET));
  LUT3 #(
    .INIT(8'hAC)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\searched_read_reg_333_reg[31] [0]),
        .I2(s_axi_CONTROL_BUS_WSTRB[0]),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [10]),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [11]),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [12]),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [13]),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [14]),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [15]),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [16]),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [17]),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [18]),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [19]),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [1]),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [20]),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [21]),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [22]),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_333_reg[31] [23]),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [24]),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [25]),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [26]),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [27]),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [28]),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [29]),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [2]),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [30]),
        .O(\or [30]));
  LUT4 #(
    .INIT(16'h0400)) 
    \int_searched[31]_i_1 
       (.I0(\waddr_reg_n_6_[2] ),
        .I1(\waddr_reg_n_6_[5] ),
        .I2(\waddr_reg_n_6_[3] ),
        .I3(\int_searched[31]_i_3_n_6 ),
        .O(p_0_in13_out));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_333_reg[31] [31]),
        .O(\or [31]));
  LUT5 #(
    .INIT(32'h00100000)) 
    \int_searched[31]_i_3 
       (.I0(\waddr_reg_n_6_[0] ),
        .I1(\waddr_reg_n_6_[1] ),
        .I2(s_axi_CONTROL_BUS_WVALID),
        .I3(\waddr_reg_n_6_[4] ),
        .I4(out[1]),
        .O(\int_searched[31]_i_3_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [3]),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [4]),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [5]),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [6]),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_333_reg[31] [7]),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [8]),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_333_reg[31] [9]),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [0]),
        .Q(\searched_read_reg_333_reg[31] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [10]),
        .Q(\searched_read_reg_333_reg[31] [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [11]),
        .Q(\searched_read_reg_333_reg[31] [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [12]),
        .Q(\searched_read_reg_333_reg[31] [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [13]),
        .Q(\searched_read_reg_333_reg[31] [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [14]),
        .Q(\searched_read_reg_333_reg[31] [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [15]),
        .Q(\searched_read_reg_333_reg[31] [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [16]),
        .Q(\searched_read_reg_333_reg[31] [16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [17]),
        .Q(\searched_read_reg_333_reg[31] [17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [18]),
        .Q(\searched_read_reg_333_reg[31] [18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [19]),
        .Q(\searched_read_reg_333_reg[31] [19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [1]),
        .Q(\searched_read_reg_333_reg[31] [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [20]),
        .Q(\searched_read_reg_333_reg[31] [20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [21]),
        .Q(\searched_read_reg_333_reg[31] [21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [22]),
        .Q(\searched_read_reg_333_reg[31] [22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [23]),
        .Q(\searched_read_reg_333_reg[31] [23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [24]),
        .Q(\searched_read_reg_333_reg[31] [24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [25]),
        .Q(\searched_read_reg_333_reg[31] [25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [26]),
        .Q(\searched_read_reg_333_reg[31] [26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [27]),
        .Q(\searched_read_reg_333_reg[31] [27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [28]),
        .Q(\searched_read_reg_333_reg[31] [28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [29]),
        .Q(\searched_read_reg_333_reg[31] [29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [2]),
        .Q(\searched_read_reg_333_reg[31] [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [30]),
        .Q(\searched_read_reg_333_reg[31] [30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [31]),
        .Q(\searched_read_reg_333_reg[31] [31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [3]),
        .Q(\searched_read_reg_333_reg[31] [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [4]),
        .Q(\searched_read_reg_333_reg[31] [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [5]),
        .Q(\searched_read_reg_333_reg[31] [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [6]),
        .Q(\searched_read_reg_333_reg[31] [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [7]),
        .Q(\searched_read_reg_333_reg[31] [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [8]),
        .Q(\searched_read_reg_333_reg[31] [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [9]),
        .Q(\searched_read_reg_333_reg[31] [9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hE0)) 
    interrupt_INST_0
       (.I0(\int_isr_reg_n_6_[0] ),
        .I1(p_1_in),
        .I2(int_gie_reg_n_6),
        .O(interrupt));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEFE)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[0]_i_2_n_6 ),
        .I1(\rdata_data[0]_i_3_n_6 ),
        .I2(\rdata_data[0]_i_4_n_6 ),
        .I3(ap_start),
        .I4(\rdata_data[0]_i_5_n_6 ),
        .I5(\rdata_data[0]_i_6_n_6 ),
        .O(rdata_data[0]));
  LUT5 #(
    .INIT(32'h0C800080)) 
    \rdata_data[0]_i_2 
       (.I0(int_agg_result_a_ap_vld),
        .I1(\rdata_data[1]_i_4_n_6 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(\int_isr_reg_n_6_[0] ),
        .O(\rdata_data[0]_i_2_n_6 ));
  LUT6 #(
    .INIT(64'h00F0008800000088)) 
    \rdata_data[0]_i_3 
       (.I0(\rdata_data[1]_i_4_n_6 ),
        .I1(int_gie_reg_n_6),
        .I2(\rdata_data[3]_i_3_n_6 ),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_ier_reg_n_6_[0] ),
        .O(\rdata_data[0]_i_3_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \rdata_data[0]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(int_agg_result_b_ap_vld),
        .I2(int_agg_result_a_ap_vld_i_2_n_6),
        .O(\rdata_data[0]_i_4_n_6 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \rdata_data[0]_i_5 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[0]_i_5_n_6 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[0]_i_6 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[0]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[0]),
        .I4(\searched_read_reg_333_reg[31] [0]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(\rdata_data[0]_i_6_n_6 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[10]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [10]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[10]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[11]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[11]),
        .I4(\searched_read_reg_333_reg[31] [11]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[11]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[12]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [12]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[12]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[12]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[13]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[13]),
        .I4(\searched_read_reg_333_reg[31] [13]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[13]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[14]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [14]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[14]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[14]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[15]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[15]),
        .I4(\searched_read_reg_333_reg[31] [15]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[15]));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \rdata_data[15]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[15]_i_2_n_6 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[16]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [16]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[16]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[16]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[17]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [17]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[17]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[17]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[18]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [18]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[18]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[18]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[19]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [19]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[19]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[19]));
  LUT4 #(
    .INIT(16'hFFEA)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[1]_i_2_n_6 ),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(\searched_read_reg_333_reg[31] [1]),
        .I3(\rdata_data[1]_i_3_n_6 ),
        .O(rdata_data[1]));
  LUT6 #(
    .INIT(64'h00F8000000880000)) 
    \rdata_data[1]_i_2 
       (.I0(\rdata_data[1]_i_4_n_6 ),
        .I1(p_1_in),
        .I2(\rdata_data[3]_i_3_n_6 ),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(p_0_in),
        .O(\rdata_data[1]_i_2_n_6 ));
  LUT6 #(
    .INIT(64'hF0000000CCAA0000)) 
    \rdata_data[1]_i_3 
       (.I0(int_ap_done),
        .I1(int_agg_result_a[1]),
        .I2(int_agg_result_b[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(\rdata_data[3]_i_3_n_6 ),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[1]_i_3_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \rdata_data[1]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[5]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[1]_i_4_n_6 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[20]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [20]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[20]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[20]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[21]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [21]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[21]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[21]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[22]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [22]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[22]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[22]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[23]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [23]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[23]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[23]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[24]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [24]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[24]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[24]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[25]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [25]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[25]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[25]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[26]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [26]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[26]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[26]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[27]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [27]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[27]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[27]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[28]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [28]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[28]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[28]));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[29]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [29]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[29]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[29]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \rdata_data[2]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [2]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(\rdata_data[2]_i_2_n_6 ),
        .O(rdata_data[2]));
  LUT6 #(
    .INIT(64'hF0000000CCAA0000)) 
    \rdata_data[2]_i_2 
       (.I0(int_ap_idle),
        .I1(int_agg_result_a[2]),
        .I2(int_agg_result_b[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(\rdata_data[3]_i_3_n_6 ),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[2]_i_2_n_6 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[30]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [30]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[30]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[30]));
  LUT3 #(
    .INIT(8'h10)) 
    \rdata_data[31]_i_1 
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .O(ar_hs));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[31]_i_2 
       (.I0(\searched_read_reg_333_reg[31] [31]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[31]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[31]));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \rdata_data[31]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[5]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[31]_i_3_n_6 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \rdata_data[31]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[31]_i_4_n_6 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \rdata_data[3]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [3]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(\rdata_data[3]_i_2_n_6 ),
        .O(rdata_data[3]));
  LUT6 #(
    .INIT(64'hF0000000CCAA0000)) 
    \rdata_data[3]_i_2 
       (.I0(int_ap_ready),
        .I1(int_agg_result_a[3]),
        .I2(int_agg_result_b[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(\rdata_data[3]_i_3_n_6 ),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[3]_i_2_n_6 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \rdata_data[3]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[3]_i_3_n_6 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[4]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[4]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[4]),
        .I4(\searched_read_reg_333_reg[31] [4]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[4]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[5]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[5]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[5]),
        .I4(\searched_read_reg_333_reg[31] [5]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[5]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[6]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[6]),
        .I4(\searched_read_reg_333_reg[31] [6]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[6]));
  LUT3 #(
    .INIT(8'hEA)) 
    \rdata_data[7]_i_1 
       (.I0(\rdata_data[7]_i_2_n_6 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[15]_i_2_n_6 ),
        .O(rdata_data[7]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[7]_i_2 
       (.I0(\rdata_data[0]_i_5_n_6 ),
        .I1(int_auto_restart),
        .I2(\rdata_data[31]_i_3_n_6 ),
        .I3(\searched_read_reg_333_reg[31] [7]),
        .I4(int_agg_result_b[7]),
        .I5(\rdata_data[31]_i_4_n_6 ),
        .O(\rdata_data[7]_i_2_n_6 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \rdata_data[8]_i_1 
       (.I0(\searched_read_reg_333_reg[31] [8]),
        .I1(\rdata_data[31]_i_3_n_6 ),
        .I2(int_agg_result_b[8]),
        .I3(\rdata_data[31]_i_4_n_6 ),
        .O(rdata_data[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[9]_i_1 
       (.I0(\rdata_data[15]_i_2_n_6 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[31]_i_4_n_6 ),
        .I3(int_agg_result_b[9]),
        .I4(\searched_read_reg_333_reg[31] [9]),
        .I5(\rdata_data[31]_i_3_n_6 ),
        .O(rdata_data[9]));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[10]),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .R(1'b0));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[11]),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .R(1'b0));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[12]),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(1'b0));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[13]),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(1'b0));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[14]),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(1'b0));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[15]),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(1'b0));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[16]),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(1'b0));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[17]),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(1'b0));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[18]),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(1'b0));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[19]),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(1'b0));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[20]),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(1'b0));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[21]),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(1'b0));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[22]),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(1'b0));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[23]),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(1'b0));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[24]),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(1'b0));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[25]),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(1'b0));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[26]),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(1'b0));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[27]),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(1'b0));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[28]),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(1'b0));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[29]),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(1'b0));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[30]),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(1'b0));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[31]),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(1'b0));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(1'b0));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[4]),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(1'b0));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[5]),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(1'b0));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[6]),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .R(1'b0));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(1'b0));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[8]),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .R(1'b0));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[9]),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h005C)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_RREADY),
        .I1(s_axi_CONTROL_BUS_ARVALID),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rstate[0]_i_1_n_6 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_6 ),
        .Q(rstate[0]),
        .R(ARESET));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \searched_read_reg_333[31]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .O(E));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[5]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_6_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_6_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_6_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_6_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_6_[4] ),
        .R(1'b0));
  FDRE \waddr_reg[5] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[5]),
        .Q(\waddr_reg_n_6_[5] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb
   (\last_reg[0] ,
    S,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    \ap_CS_fsm_reg[5] ,
    \exitcond_reg_360_reg[0] ,
    ap_enable_reg_pp2_iter0,
    \i_5_reg_364_reg[5] ,
    \i_2_reg_226_reg[5] ,
    DOBDO,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [29:0]\last_reg[0] ;
  output [0:0]S;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]Q;
  input [1:0]\ap_CS_fsm_reg[5] ;
  input \exitcond_reg_360_reg[0] ;
  input ap_enable_reg_pp2_iter0;
  input [5:0]\i_5_reg_364_reg[5] ;
  input [5:0]\i_2_reg_226_reg[5] ;
  input [1:0]DOBDO;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [1:0]DOBDO;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [5:0]Q;
  wire [0:0]S;
  wire [1:0]\ap_CS_fsm_reg[5] ;
  wire ap_clk;
  wire ap_enable_reg_pp2_iter0;
  wire \exitcond_reg_360_reg[0] ;
  wire [5:0]\i_2_reg_226_reg[5] ;
  wire [5:0]\i_5_reg_364_reg[5] ;
  wire [29:0]\last_reg[0] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram_1 Adder2_inputValuebkb_ram_U
       (.DOBDO(DOBDO),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .S(S),
        .\ap_CS_fsm_reg[5] (\ap_CS_fsm_reg[5] ),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp2_iter0(ap_enable_reg_pp2_iter0),
        .\exitcond_reg_360_reg[0] (\exitcond_reg_360_reg[0] ),
        .\i_2_reg_226_reg[5] (\i_2_reg_226_reg[5] ),
        .\i_5_reg_364_reg[5] (\i_5_reg_364_reg[5] ),
        .\last_reg[0] (\last_reg[0] ));
endmodule

(* ORIG_REF_NAME = "Adder2_inputValuebkb" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_0
   (DOBDO,
    CO,
    ram_reg,
    ap_clk,
    S,
    \LAST_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    \ap_CS_fsm_reg[5] ,
    ap_enable_reg_pp2_iter0,
    \i_5_reg_364_reg[5] ,
    \i_2_reg_226_reg[5] ,
    exitcond_reg_360,
    ap_enable_reg_pp2_iter1,
    ram_reg_0,
    \LAST_STREAM_V_data_V_0_payload_B_reg[31] ,
    \LAST_STREAM_V_data_V_0_payload_A_reg[31] ,
    LAST_STREAM_V_data_V_0_sel);
  output [1:0]DOBDO;
  output [0:0]CO;
  output ram_reg;
  input ap_clk;
  input [0:0]S;
  input \LAST_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]Q;
  input [1:0]\ap_CS_fsm_reg[5] ;
  input ap_enable_reg_pp2_iter0;
  input [5:0]\i_5_reg_364_reg[5] ;
  input [5:0]\i_2_reg_226_reg[5] ;
  input exitcond_reg_360;
  input ap_enable_reg_pp2_iter1;
  input [29:0]ram_reg_0;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  input LAST_STREAM_V_data_V_0_sel;

  wire [0:0]CO;
  wire [1:0]DOBDO;
  wire [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire LAST_STREAM_V_data_V_0_sel;
  wire \LAST_STREAM_V_data_V_0_state_reg[0] ;
  wire [5:0]Q;
  wire [0:0]S;
  wire [1:0]\ap_CS_fsm_reg[5] ;
  wire ap_clk;
  wire ap_enable_reg_pp2_iter0;
  wire ap_enable_reg_pp2_iter1;
  wire exitcond_reg_360;
  wire [5:0]\i_2_reg_226_reg[5] ;
  wire [5:0]\i_5_reg_364_reg[5] ;
  wire ram_reg;
  wire [29:0]ram_reg_0;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram Adder2_inputValuebkb_ram_U
       (.CO(CO),
        .DOBDO(DOBDO),
        .\LAST_STREAM_V_data_V_0_payload_A_reg[31] (\LAST_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\LAST_STREAM_V_data_V_0_payload_B_reg[31] (\LAST_STREAM_V_data_V_0_payload_B_reg[31] ),
        .LAST_STREAM_V_data_V_0_sel(LAST_STREAM_V_data_V_0_sel),
        .\LAST_STREAM_V_data_V_0_state_reg[0] (\LAST_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .S(S),
        .\ap_CS_fsm_reg[5] (\ap_CS_fsm_reg[5] ),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp2_iter0(ap_enable_reg_pp2_iter0),
        .ap_enable_reg_pp2_iter1(ap_enable_reg_pp2_iter1),
        .exitcond_reg_360(exitcond_reg_360),
        .\i_2_reg_226_reg[5] (\i_2_reg_226_reg[5] ),
        .\i_5_reg_364_reg[5] (\i_5_reg_364_reg[5] ),
        .ram_reg_0(ram_reg),
        .ram_reg_1(ram_reg_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram
   (DOBDO,
    CO,
    ram_reg_0,
    ap_clk,
    S,
    \LAST_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    \ap_CS_fsm_reg[5] ,
    ap_enable_reg_pp2_iter0,
    \i_5_reg_364_reg[5] ,
    \i_2_reg_226_reg[5] ,
    exitcond_reg_360,
    ap_enable_reg_pp2_iter1,
    ram_reg_1,
    \LAST_STREAM_V_data_V_0_payload_B_reg[31] ,
    \LAST_STREAM_V_data_V_0_payload_A_reg[31] ,
    LAST_STREAM_V_data_V_0_sel);
  output [1:0]DOBDO;
  output [0:0]CO;
  output ram_reg_0;
  input ap_clk;
  input [0:0]S;
  input \LAST_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]Q;
  input [1:0]\ap_CS_fsm_reg[5] ;
  input ap_enable_reg_pp2_iter0;
  input [5:0]\i_5_reg_364_reg[5] ;
  input [5:0]\i_2_reg_226_reg[5] ;
  input exitcond_reg_360;
  input ap_enable_reg_pp2_iter1;
  input [29:0]ram_reg_1;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  input LAST_STREAM_V_data_V_0_sel;

  wire [0:0]CO;
  wire [1:0]DOBDO;
  wire [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire LAST_STREAM_V_data_V_0_sel;
  wire \LAST_STREAM_V_data_V_0_state_reg[0] ;
  wire [5:0]Q;
  wire [0:0]S;
  wire [1:0]\ap_CS_fsm_reg[5] ;
  wire ap_clk;
  wire ap_enable_reg_pp2_iter0;
  wire ap_enable_reg_pp2_iter1;
  wire ce0;
  wire exitcond_reg_360;
  wire [5:0]\i_2_reg_226_reg[5] ;
  wire [5:0]\i_5_reg_364_reg[5] ;
  wire [29:0]lastValues_0_q0;
  wire \last[15]_i_10_n_6 ;
  wire \last[15]_i_11_n_6 ;
  wire \last[15]_i_12_n_6 ;
  wire \last[15]_i_13_n_6 ;
  wire \last[15]_i_14_n_6 ;
  wire \last[15]_i_15_n_6 ;
  wire \last[15]_i_5_n_6 ;
  wire \last[15]_i_6_n_6 ;
  wire \last[15]_i_8_n_6 ;
  wire \last[15]_i_9_n_6 ;
  wire \last_reg[15]_i_2_n_8 ;
  wire \last_reg[15]_i_2_n_9 ;
  wire \last_reg[15]_i_3_n_6 ;
  wire \last_reg[15]_i_3_n_7 ;
  wire \last_reg[15]_i_3_n_8 ;
  wire \last_reg[15]_i_3_n_9 ;
  wire \last_reg[15]_i_7_n_6 ;
  wire \last_reg[15]_i_7_n_7 ;
  wire \last_reg[15]_i_7_n_8 ;
  wire \last_reg[15]_i_7_n_9 ;
  wire p_65_in;
  wire ram_reg_0;
  wire [29:0]ram_reg_1;
  wire ram_reg_i_10_n_6;
  wire ram_reg_i_11_n_6;
  wire ram_reg_i_12_n_6;
  wire ram_reg_i_13_n_6;
  wire ram_reg_i_14_n_6;
  wire ram_reg_i_15_n_6;
  wire ram_reg_i_16_n_6;
  wire ram_reg_i_17_n_6;
  wire ram_reg_i_18_n_6;
  wire ram_reg_i_19_n_6;
  wire ram_reg_i_20_n_6;
  wire ram_reg_i_21_n_6;
  wire ram_reg_i_22_n_6;
  wire ram_reg_i_23_n_6;
  wire ram_reg_i_24_n_6;
  wire ram_reg_i_25_n_6;
  wire ram_reg_i_26_n_6;
  wire ram_reg_i_27_n_6;
  wire ram_reg_i_28_n_6;
  wire ram_reg_i_29_n_6;
  wire ram_reg_i_2__0_n_6;
  wire ram_reg_i_30_n_6;
  wire ram_reg_i_31_n_6;
  wire ram_reg_i_32_n_6;
  wire ram_reg_i_33_n_6;
  wire ram_reg_i_34_n_6;
  wire ram_reg_i_35_n_6;
  wire ram_reg_i_36_n_6;
  wire ram_reg_i_37_n_6;
  wire ram_reg_i_38_n_6;
  wire ram_reg_i_39_n_6;
  wire ram_reg_i_3__0_n_6;
  wire ram_reg_i_4__0_n_6;
  wire ram_reg_i_5__0_n_6;
  wire ram_reg_i_6__0_n_6;
  wire ram_reg_i_7__0_n_6;
  wire ram_reg_i_8_n_6;
  wire ram_reg_i_9_n_6;
  wire [3:3]\NLW_last_reg[15]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_last_reg[15]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_last_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_last_reg[15]_i_7_O_UNCONNECTED ;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_10 
       (.I0(lastValues_0_q0[17]),
        .I1(ram_reg_1[17]),
        .I2(lastValues_0_q0[16]),
        .I3(ram_reg_1[16]),
        .I4(ram_reg_1[15]),
        .I5(lastValues_0_q0[15]),
        .O(\last[15]_i_10_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_11 
       (.I0(lastValues_0_q0[14]),
        .I1(ram_reg_1[14]),
        .I2(lastValues_0_q0[13]),
        .I3(ram_reg_1[13]),
        .I4(ram_reg_1[12]),
        .I5(lastValues_0_q0[12]),
        .O(\last[15]_i_11_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_12 
       (.I0(lastValues_0_q0[11]),
        .I1(ram_reg_1[11]),
        .I2(lastValues_0_q0[10]),
        .I3(ram_reg_1[10]),
        .I4(ram_reg_1[9]),
        .I5(lastValues_0_q0[9]),
        .O(\last[15]_i_12_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_13 
       (.I0(lastValues_0_q0[8]),
        .I1(ram_reg_1[8]),
        .I2(lastValues_0_q0[7]),
        .I3(ram_reg_1[7]),
        .I4(ram_reg_1[6]),
        .I5(lastValues_0_q0[6]),
        .O(\last[15]_i_13_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_14 
       (.I0(lastValues_0_q0[5]),
        .I1(ram_reg_1[5]),
        .I2(lastValues_0_q0[4]),
        .I3(ram_reg_1[4]),
        .I4(ram_reg_1[3]),
        .I5(lastValues_0_q0[3]),
        .O(\last[15]_i_14_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_15 
       (.I0(lastValues_0_q0[2]),
        .I1(ram_reg_1[2]),
        .I2(lastValues_0_q0[1]),
        .I3(ram_reg_1[1]),
        .I4(ram_reg_1[0]),
        .I5(lastValues_0_q0[0]),
        .O(\last[15]_i_15_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_5 
       (.I0(lastValues_0_q0[29]),
        .I1(ram_reg_1[29]),
        .I2(lastValues_0_q0[28]),
        .I3(ram_reg_1[28]),
        .I4(ram_reg_1[27]),
        .I5(lastValues_0_q0[27]),
        .O(\last[15]_i_5_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_6 
       (.I0(lastValues_0_q0[26]),
        .I1(ram_reg_1[26]),
        .I2(lastValues_0_q0[25]),
        .I3(ram_reg_1[25]),
        .I4(ram_reg_1[24]),
        .I5(lastValues_0_q0[24]),
        .O(\last[15]_i_6_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_8 
       (.I0(lastValues_0_q0[23]),
        .I1(ram_reg_1[23]),
        .I2(lastValues_0_q0[22]),
        .I3(ram_reg_1[22]),
        .I4(ram_reg_1[21]),
        .I5(lastValues_0_q0[21]),
        .O(\last[15]_i_8_n_6 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \last[15]_i_9 
       (.I0(lastValues_0_q0[20]),
        .I1(ram_reg_1[20]),
        .I2(lastValues_0_q0[19]),
        .I3(ram_reg_1[19]),
        .I4(ram_reg_1[18]),
        .I5(lastValues_0_q0[18]),
        .O(\last[15]_i_9_n_6 ));
  CARRY4 \last_reg[15]_i_2 
       (.CI(\last_reg[15]_i_3_n_6 ),
        .CO({\NLW_last_reg[15]_i_2_CO_UNCONNECTED [3],CO,\last_reg[15]_i_2_n_8 ,\last_reg[15]_i_2_n_9 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_last_reg[15]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,S,\last[15]_i_5_n_6 ,\last[15]_i_6_n_6 }));
  CARRY4 \last_reg[15]_i_3 
       (.CI(\last_reg[15]_i_7_n_6 ),
        .CO({\last_reg[15]_i_3_n_6 ,\last_reg[15]_i_3_n_7 ,\last_reg[15]_i_3_n_8 ,\last_reg[15]_i_3_n_9 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_last_reg[15]_i_3_O_UNCONNECTED [3:0]),
        .S({\last[15]_i_8_n_6 ,\last[15]_i_9_n_6 ,\last[15]_i_10_n_6 ,\last[15]_i_11_n_6 }));
  CARRY4 \last_reg[15]_i_7 
       (.CI(1'b0),
        .CO({\last_reg[15]_i_7_n_6 ,\last_reg[15]_i_7_n_7 ,\last_reg[15]_i_7_n_8 ,\last_reg[15]_i_7_n_9 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_last_reg[15]_i_7_O_UNCONNECTED [3:0]),
        .S({\last[15]_i_12_n_6 ,\last[15]_i_13_n_6 ,\last[15]_i_14_n_6 ,\last[15]_i_15_n_6 }));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1600" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "49" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,1'b1,ram_reg_i_2__0_n_6,ram_reg_i_3__0_n_6,ram_reg_i_4__0_n_6,ram_reg_i_5__0_n_6,ram_reg_i_6__0_n_6,ram_reg_i_7__0_n_6,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,ram_reg_i_2__0_n_6,ram_reg_i_3__0_n_6,ram_reg_i_4__0_n_6,ram_reg_i_5__0_n_6,ram_reg_i_6__0_n_6,ram_reg_i_7__0_n_6,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({ram_reg_i_8_n_6,ram_reg_i_9_n_6,ram_reg_i_10_n_6,ram_reg_i_11_n_6,ram_reg_i_12_n_6,ram_reg_i_13_n_6,ram_reg_i_14_n_6,ram_reg_i_15_n_6,ram_reg_i_16_n_6,ram_reg_i_17_n_6,ram_reg_i_18_n_6,ram_reg_i_19_n_6,ram_reg_i_20_n_6,ram_reg_i_21_n_6,ram_reg_i_22_n_6,ram_reg_i_23_n_6}),
        .DIBDI({1'b1,1'b1,ram_reg_i_24_n_6,ram_reg_i_25_n_6,ram_reg_i_26_n_6,ram_reg_i_27_n_6,ram_reg_i_28_n_6,ram_reg_i_29_n_6,ram_reg_i_30_n_6,ram_reg_i_31_n_6,ram_reg_i_32_n_6,ram_reg_i_33_n_6,ram_reg_i_34_n_6,ram_reg_i_35_n_6,ram_reg_i_36_n_6,ram_reg_i_37_n_6}),
        .DIPADIP({ram_reg_i_38_n_6,ram_reg_i_39_n_6}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(lastValues_0_q0[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],DOBDO,lastValues_0_q0[29:18]}),
        .DOPADOP(lastValues_0_q0[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ce0),
        .ENBWREN(ce0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({p_65_in,p_65_in}),
        .WEBWE({1'b0,1'b0,p_65_in,p_65_in}));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_10
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_10_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_11
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_11_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_12
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_12_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_13
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_13_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_14
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_14_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_15
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_15_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_16
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_16_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_17
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_17_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_18
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_18_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_19
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_19_n_6));
  LUT5 #(
    .INIT(32'hFF88F888)) 
    ram_reg_i_1__0
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(\ap_CS_fsm_reg[5] [1]),
        .I2(Q[5]),
        .I3(\ap_CS_fsm_reg[5] [0]),
        .I4(\LAST_STREAM_V_data_V_0_state_reg[0] ),
        .O(ce0));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_20
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_20_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_21
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_21_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_22
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_22_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_23
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_23_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_24
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [31]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [31]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_24_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_25
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [30]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [30]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_25_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_26
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [29]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [29]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_26_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_27
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [28]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [28]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_27_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_28
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [27]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [27]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_28_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_29
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_29_n_6));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_2__0
       (.I0(Q[5]),
        .I1(ram_reg_0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [5]),
        .I5(\i_2_reg_226_reg[5] [5]),
        .O(ram_reg_i_2__0_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_30
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_30_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_31
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_31_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_32
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_32_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_33
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_33_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_34
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_34_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_35
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_35_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_36
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_36_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_37
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_37_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_38
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_38_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_39
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_39_n_6));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_3__0
       (.I0(Q[4]),
        .I1(ram_reg_0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [4]),
        .I5(\i_2_reg_226_reg[5] [4]),
        .O(ram_reg_i_3__0_n_6));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_i_40__0
       (.I0(\LAST_STREAM_V_data_V_0_state_reg[0] ),
        .I1(Q[5]),
        .I2(\ap_CS_fsm_reg[5] [0]),
        .O(p_65_in));
  LUT2 #(
    .INIT(4'hB)) 
    ram_reg_i_41
       (.I0(exitcond_reg_360),
        .I1(ap_enable_reg_pp2_iter1),
        .O(ram_reg_0));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_4__0
       (.I0(Q[3]),
        .I1(ram_reg_0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [3]),
        .I5(\i_2_reg_226_reg[5] [3]),
        .O(ram_reg_i_4__0_n_6));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_5__0
       (.I0(Q[2]),
        .I1(ram_reg_0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [2]),
        .I5(\i_2_reg_226_reg[5] [2]),
        .O(ram_reg_i_5__0_n_6));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_6__0
       (.I0(Q[1]),
        .I1(ram_reg_0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [1]),
        .I5(\i_2_reg_226_reg[5] [1]),
        .O(ram_reg_i_6__0_n_6));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_7__0
       (.I0(Q[0]),
        .I1(ram_reg_0),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [0]),
        .I5(\i_2_reg_226_reg[5] [0]),
        .O(ram_reg_i_7__0_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_8
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_8_n_6));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_9
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_9_n_6));
endmodule

(* ORIG_REF_NAME = "Adder2_inputValuebkb_ram" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram_1
   (\last_reg[0] ,
    S,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    \ap_CS_fsm_reg[5] ,
    \exitcond_reg_360_reg[0] ,
    ap_enable_reg_pp2_iter0,
    \i_5_reg_364_reg[5] ,
    \i_2_reg_226_reg[5] ,
    DOBDO,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [29:0]\last_reg[0] ;
  output [0:0]S;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [5:0]Q;
  input [1:0]\ap_CS_fsm_reg[5] ;
  input \exitcond_reg_360_reg[0] ;
  input ap_enable_reg_pp2_iter0;
  input [5:0]\i_5_reg_364_reg[5] ;
  input [5:0]\i_2_reg_226_reg[5] ;
  input [1:0]DOBDO;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [1:0]DOBDO;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [5:0]Q;
  wire [0:0]S;
  wire [5:0]address0;
  wire [1:0]\ap_CS_fsm_reg[5] ;
  wire ap_clk;
  wire ap_enable_reg_pp2_iter0;
  wire ce01_out;
  wire [31:0]d0;
  wire \exitcond_reg_360_reg[0] ;
  wire [5:0]\i_2_reg_226_reg[5] ;
  wire [5:0]\i_5_reg_364_reg[5] ;
  wire [31:30]inputValues_0_q0;
  wire [29:0]\last_reg[0] ;
  wire p_87_in;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT4 #(
    .INIT(16'h9009)) 
    \last[15]_i_4 
       (.I0(inputValues_0_q0[31]),
        .I1(DOBDO[1]),
        .I2(inputValues_0_q0[30]),
        .I3(DOBDO[0]),
        .O(S));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1600" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "49" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI(d0[15:0]),
        .DIBDI({1'b1,1'b1,d0[31:18]}),
        .DIPADIP(d0[17:16]),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(\last_reg[0] [15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],inputValues_0_q0,\last_reg[0] [29:18]}),
        .DOPADOP(\last_reg[0] [17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ce01_out),
        .ENBWREN(ce01_out),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({p_87_in,p_87_in}),
        .WEBWE({1'b0,1'b0,p_87_in,p_87_in}));
  LUT5 #(
    .INIT(32'hFF88F888)) 
    ram_reg_i_1
       (.I0(ap_enable_reg_pp2_iter0),
        .I1(\ap_CS_fsm_reg[5] [1]),
        .I2(Q[5]),
        .I3(\ap_CS_fsm_reg[5] [0]),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .O(ce01_out));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_10__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[13]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_11__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[12]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_12__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[11]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_13__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[10]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_14__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[9]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_15__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[8]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_16__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[7]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_17__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[6]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_18__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[5]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_19__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[4]));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_2
       (.I0(Q[5]),
        .I1(\exitcond_reg_360_reg[0] ),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [5]),
        .I5(\i_2_reg_226_reg[5] [5]),
        .O(address0[5]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_20__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[3]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_21__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[2]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_22__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[1]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_23__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_24__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [31]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [31]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[31]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_25__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [30]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [30]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[30]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_26__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [29]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [29]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[29]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_27__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [28]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [28]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[28]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_28__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [27]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [27]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[27]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_29__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[26]));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_3
       (.I0(Q[4]),
        .I1(\exitcond_reg_360_reg[0] ),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [4]),
        .I5(\i_2_reg_226_reg[5] [4]),
        .O(address0[4]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_30__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[25]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_31__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[24]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_32__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[23]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_33__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[22]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_34__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[21]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_35__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[20]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_36__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[19]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_37__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[18]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_38__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[17]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_39__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[16]));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_4
       (.I0(Q[3]),
        .I1(\exitcond_reg_360_reg[0] ),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [3]),
        .I5(\i_2_reg_226_reg[5] [3]),
        .O(address0[3]));
  LUT3 #(
    .INIT(8'h20)) 
    ram_reg_i_40
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I1(Q[5]),
        .I2(\ap_CS_fsm_reg[5] [0]),
        .O(p_87_in));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_5
       (.I0(Q[2]),
        .I1(\exitcond_reg_360_reg[0] ),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [2]),
        .I5(\i_2_reg_226_reg[5] [2]),
        .O(address0[2]));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_6
       (.I0(Q[1]),
        .I1(\exitcond_reg_360_reg[0] ),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [1]),
        .I5(\i_2_reg_226_reg[5] [1]),
        .O(address0[1]));
  LUT6 #(
    .INIT(64'hFAAACAAA3AAA0AAA)) 
    ram_reg_i_7
       (.I0(Q[0]),
        .I1(\exitcond_reg_360_reg[0] ),
        .I2(\ap_CS_fsm_reg[5] [1]),
        .I3(ap_enable_reg_pp2_iter0),
        .I4(\i_5_reg_364_reg[5] [0]),
        .I5(\i_2_reg_226_reg[5] [0]),
        .O(address0[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_8__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[15]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_9__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(d0[14]));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    LAST_STREAM_TVALID,
    LAST_STREAM_TREADY,
    LAST_STREAM_TDATA,
    LAST_STREAM_TDEST,
    LAST_STREAM_TKEEP,
    LAST_STREAM_TSTRB,
    LAST_STREAM_TUSER,
    LAST_STREAM_TLAST,
    LAST_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 6, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [5:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM:LAST_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME LAST_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input LAST_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TREADY" *) output LAST_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TDATA" *) input [31:0]LAST_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TDEST" *) input [5:0]LAST_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TKEEP" *) input [3:0]LAST_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TSTRB" *) input [3:0]LAST_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TUSER" *) input [1:0]LAST_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TLAST" *) input [0:0]LAST_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TID" *) input [4:0]LAST_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire [31:0]LAST_STREAM_TDATA;
  wire [5:0]LAST_STREAM_TDEST;
  wire [4:0]LAST_STREAM_TID;
  wire [3:0]LAST_STREAM_TKEEP;
  wire [0:0]LAST_STREAM_TLAST;
  wire LAST_STREAM_TREADY;
  wire [3:0]LAST_STREAM_TSTRB;
  wire [1:0]LAST_STREAM_TUSER;
  wire LAST_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .LAST_STREAM_TDATA(LAST_STREAM_TDATA),
        .LAST_STREAM_TDEST(LAST_STREAM_TDEST),
        .LAST_STREAM_TID(LAST_STREAM_TID),
        .LAST_STREAM_TKEEP(LAST_STREAM_TKEEP),
        .LAST_STREAM_TLAST(LAST_STREAM_TLAST),
        .LAST_STREAM_TREADY(LAST_STREAM_TREADY),
        .LAST_STREAM_TSTRB(LAST_STREAM_TSTRB),
        .LAST_STREAM_TUSER(LAST_STREAM_TUSER),
        .LAST_STREAM_TVALID(LAST_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
