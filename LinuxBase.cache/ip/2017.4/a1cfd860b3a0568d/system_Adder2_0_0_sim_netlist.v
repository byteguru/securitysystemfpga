// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Thu Feb 22 21:11:16 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire ARESET;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2 ;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ;
  wire \ap_CS_fsm[1]_i_2_n_2 ;
  wire \ap_CS_fsm_reg_n_2_[0] ;
  wire ap_CS_fsm_state2;
  wire [2:0]ap_NS_fsm;
  wire ap_NS_fsm10_out;
  wire ap_clk;
  wire ap_done;
  wire ap_rst_n;
  wire [5:0]i_1_fu_109_p2;
  wire i_reg_90;
  wire \i_reg_90_reg_n_2_[0] ;
  wire \i_reg_90_reg_n_2_[1] ;
  wire \i_reg_90_reg_n_2_[2] ;
  wire \i_reg_90_reg_n_2_[3] ;
  wire \i_reg_90_reg_n_2_[4] ;
  wire interrupt;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire tmp_fu_101_p3;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.ARESET(ARESET),
        .D(ap_NS_fsm[1:0]),
        .E(ap_NS_fsm10_out),
        .Q({ap_done,ap_CS_fsm_state2,\ap_CS_fsm_reg_n_2_[0] }),
        .SR(i_reg_90),
        .\ap_CS_fsm_reg[1] (\ap_CS_fsm[1]_i_2_n_2 ),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
  GND GND
       (.G(\<const0> ));
  LUT6 #(
    .INIT(64'hFB88FF8800000000)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ),
        .I2(tmp_fu_101_p3),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I4(ap_CS_fsm_state2),
        .I5(ap_rst_n),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h2FFF22FF)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ),
        .I1(INPUT_STREAM_TVALID),
        .I2(tmp_fu_101_p3),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I4(ap_CS_fsm_state2),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_2 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[1] ),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hBF008800)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_TREADY),
        .I2(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ),
        .I3(ap_rst_n),
        .I4(\INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h5DFF5D5D5D5D5D5D)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ),
        .I1(INPUT_STREAM_TREADY),
        .I2(INPUT_STREAM_TVALID),
        .I3(tmp_fu_101_p3),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I5(ap_CS_fsm_state2),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_2 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_2 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_2 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I1(ap_CS_fsm_state2),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I3(tmp_fu_101_p3),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_2),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_V_last_V_0_ack_in),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_2),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hDF008800)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(INPUT_STREAM_V_last_V_0_ack_in),
        .I1(INPUT_STREAM_TVALID),
        .I2(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ),
        .I3(ap_rst_n),
        .I4(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_2 
       (.I0(ap_CS_fsm_state2),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I2(tmp_fu_101_p3),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'h5DFF5D5D5D5D5D5D)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(INPUT_STREAM_TVALID),
        .I3(tmp_fu_101_p3),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I5(ap_CS_fsm_state2),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_2 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(ARESET));
  LUT6 #(
    .INIT(64'h5577557F55FF557F)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(ap_CS_fsm_state2),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(tmp_fu_101_p3),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .I5(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\ap_CS_fsm[1]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'hFBF8F0F000000000)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(tmp_fu_101_p3),
        .I3(INPUT_STREAM_V_last_V_0_payload_A),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .I5(ap_CS_fsm_state2),
        .O(ap_NS_fsm[2]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_2_[0] ),
        .S(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_done),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_90[0]_i_1 
       (.I0(\i_reg_90_reg_n_2_[0] ),
        .O(i_1_fu_109_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_reg_90[1]_i_1 
       (.I0(\i_reg_90_reg_n_2_[0] ),
        .I1(\i_reg_90_reg_n_2_[1] ),
        .O(i_1_fu_109_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \i_reg_90[2]_i_1 
       (.I0(\i_reg_90_reg_n_2_[2] ),
        .I1(\i_reg_90_reg_n_2_[1] ),
        .I2(\i_reg_90_reg_n_2_[0] ),
        .O(i_1_fu_109_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_reg_90[3]_i_1 
       (.I0(\i_reg_90_reg_n_2_[3] ),
        .I1(\i_reg_90_reg_n_2_[0] ),
        .I2(\i_reg_90_reg_n_2_[1] ),
        .I3(\i_reg_90_reg_n_2_[2] ),
        .O(i_1_fu_109_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_reg_90[4]_i_1 
       (.I0(\i_reg_90_reg_n_2_[4] ),
        .I1(\i_reg_90_reg_n_2_[2] ),
        .I2(\i_reg_90_reg_n_2_[1] ),
        .I3(\i_reg_90_reg_n_2_[0] ),
        .I4(\i_reg_90_reg_n_2_[3] ),
        .O(i_1_fu_109_p2[4]));
  LUT6 #(
    .INIT(64'h00001D0000000000)) 
    \i_reg_90[5]_i_2 
       (.I0(INPUT_STREAM_V_last_V_0_payload_A),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_B),
        .I3(ap_CS_fsm_state2),
        .I4(tmp_fu_101_p3),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg_n_2_[0] ),
        .O(ap_NS_fsm10_out));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_reg_90[5]_i_3 
       (.I0(\i_reg_90_reg_n_2_[3] ),
        .I1(\i_reg_90_reg_n_2_[0] ),
        .I2(\i_reg_90_reg_n_2_[1] ),
        .I3(\i_reg_90_reg_n_2_[2] ),
        .I4(\i_reg_90_reg_n_2_[4] ),
        .O(i_1_fu_109_p2[5]));
  FDRE \i_reg_90_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_109_p2[0]),
        .Q(\i_reg_90_reg_n_2_[0] ),
        .R(i_reg_90));
  FDRE \i_reg_90_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_109_p2[1]),
        .Q(\i_reg_90_reg_n_2_[1] ),
        .R(i_reg_90));
  FDRE \i_reg_90_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_109_p2[2]),
        .Q(\i_reg_90_reg_n_2_[2] ),
        .R(i_reg_90));
  FDRE \i_reg_90_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_109_p2[3]),
        .Q(\i_reg_90_reg_n_2_[3] ),
        .R(i_reg_90));
  FDRE \i_reg_90_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_109_p2[4]),
        .Q(\i_reg_90_reg_n_2_[4] ),
        .R(i_reg_90));
  FDRE \i_reg_90_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm10_out),
        .D(i_1_fu_109_p2[5]),
        .Q(tmp_fu_101_p3),
        .R(i_reg_90));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (ARESET,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    D,
    SR,
    interrupt,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_ARADDR,
    ap_rst_n,
    \ap_CS_fsm_reg[1] ,
    E,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR);
  output ARESET;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [1:0]D;
  output [0:0]SR;
  output interrupt;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [2:0]Q;
  input s_axi_CONTROL_BUS_RREADY;
  input s_axi_CONTROL_BUS_ARVALID;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input [4:0]s_axi_CONTROL_BUS_ARADDR;
  input ap_rst_n;
  input \ap_CS_fsm_reg[1] ;
  input [0:0]E;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [4:0]s_axi_CONTROL_BUS_AWADDR;

  wire \/FSM_onehot_wstate[1]_i_1_n_2 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_2 ;
  wire ARESET;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_wstate[3]_i_1_n_2 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_2_[0] ;
  wire [2:0]Q;
  wire [0:0]SR;
  wire \ap_CS_fsm_reg[1] ;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire int_ap_done;
  wire int_ap_done_i_1_n_2;
  wire int_ap_idle;
  wire int_ap_ready;
  wire [15:15]int_ap_return;
  wire \int_ap_return[15]_i_1_n_2 ;
  wire int_ap_start_i_1_n_2;
  wire int_ap_start_i_2_n_2;
  wire int_ap_start_i_3_n_2;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_2;
  wire int_gie_i_1_n_2;
  wire int_gie_i_2_n_2;
  wire int_gie_reg_n_2;
  wire \int_ier[0]_i_1_n_2 ;
  wire \int_ier[1]_i_1_n_2 ;
  wire \int_ier[1]_i_2_n_2 ;
  wire \int_ier_reg_n_2_[0] ;
  wire \int_isr[0]_i_1_n_2 ;
  wire \int_isr[0]_i_2_n_2 ;
  wire \int_isr[1]_i_1_n_2 ;
  wire \int_isr_reg_n_2_[0] ;
  wire \int_searched_reg_n_2_[0] ;
  wire \int_searched_reg_n_2_[10] ;
  wire \int_searched_reg_n_2_[11] ;
  wire \int_searched_reg_n_2_[12] ;
  wire \int_searched_reg_n_2_[13] ;
  wire \int_searched_reg_n_2_[14] ;
  wire \int_searched_reg_n_2_[15] ;
  wire \int_searched_reg_n_2_[16] ;
  wire \int_searched_reg_n_2_[17] ;
  wire \int_searched_reg_n_2_[18] ;
  wire \int_searched_reg_n_2_[19] ;
  wire \int_searched_reg_n_2_[1] ;
  wire \int_searched_reg_n_2_[20] ;
  wire \int_searched_reg_n_2_[21] ;
  wire \int_searched_reg_n_2_[22] ;
  wire \int_searched_reg_n_2_[23] ;
  wire \int_searched_reg_n_2_[24] ;
  wire \int_searched_reg_n_2_[25] ;
  wire \int_searched_reg_n_2_[26] ;
  wire \int_searched_reg_n_2_[27] ;
  wire \int_searched_reg_n_2_[28] ;
  wire \int_searched_reg_n_2_[29] ;
  wire \int_searched_reg_n_2_[2] ;
  wire \int_searched_reg_n_2_[30] ;
  wire \int_searched_reg_n_2_[31] ;
  wire \int_searched_reg_n_2_[3] ;
  wire \int_searched_reg_n_2_[4] ;
  wire \int_searched_reg_n_2_[5] ;
  wire \int_searched_reg_n_2_[6] ;
  wire \int_searched_reg_n_2_[7] ;
  wire \int_searched_reg_n_2_[8] ;
  wire \int_searched_reg_n_2_[9] ;
  wire interrupt;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in11_out;
  wire p_1_in;
  wire [7:0]rdata_data;
  wire \rdata_data[0]_i_2_n_2 ;
  wire \rdata_data[10]_i_1_n_2 ;
  wire \rdata_data[11]_i_1_n_2 ;
  wire \rdata_data[12]_i_1_n_2 ;
  wire \rdata_data[13]_i_1_n_2 ;
  wire \rdata_data[14]_i_1_n_2 ;
  wire \rdata_data[15]_i_1_n_2 ;
  wire \rdata_data[15]_i_2_n_2 ;
  wire \rdata_data[15]_i_3_n_2 ;
  wire \rdata_data[1]_i_2_n_2 ;
  wire \rdata_data[1]_i_3_n_2 ;
  wire \rdata_data[31]_i_1_n_2 ;
  wire \rdata_data[4]_i_1_n_2 ;
  wire \rdata_data[5]_i_1_n_2 ;
  wire \rdata_data[6]_i_1_n_2 ;
  wire \rdata_data[7]_i_2_n_2 ;
  wire \rdata_data[7]_i_3_n_2 ;
  wire \rdata_data[8]_i_1_n_2 ;
  wire \rdata_data[9]_i_1_n_2 ;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_2 ;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire waddr;
  wire \waddr_reg_n_2_[0] ;
  wire \waddr_reg_n_2_[1] ;
  wire \waddr_reg_n_2_[2] ;
  wire \waddr_reg_n_2_[3] ;
  wire \waddr_reg_n_2_[4] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_2 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_2 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_2_[0] ),
        .S(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_2 ),
        .Q(out[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_2 ),
        .Q(out[1]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_2 ),
        .Q(out[2]),
        .R(ARESET));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h2333)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(ap_start),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hEAE0)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(Q[1]),
        .I1(ap_start),
        .I2(Q[0]),
        .I3(\ap_CS_fsm_reg[1] ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \i_reg_90[5]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(E),
        .O(SR));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFF0000)) 
    int_ap_done_i_1
       (.I0(\rdata_data[7]_i_2_n_2 ),
        .I1(rstate[1]),
        .I2(rstate[0]),
        .I3(s_axi_CONTROL_BUS_ARVALID),
        .I4(Q[2]),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_2),
        .Q(int_ap_done),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(ARESET));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(int_ap_ready),
        .R(ARESET));
  LUT2 #(
    .INIT(4'hE)) 
    \int_ap_return[15]_i_1 
       (.I0(Q[2]),
        .I1(int_ap_return),
        .O(\int_ap_return[15]_i_1_n_2 ));
  FDRE \int_ap_return_reg[15] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ap_return[15]_i_1_n_2 ),
        .Q(int_ap_return),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hBBBBFBBB8888F888)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[2]),
        .I2(int_ap_start_i_2_n_2),
        .I3(s_axi_CONTROL_BUS_WDATA[0]),
        .I4(int_ap_start_i_3_n_2),
        .I5(ap_start),
        .O(int_ap_start_i_1_n_2));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    int_ap_start_i_2
       (.I0(\waddr_reg_n_2_[2] ),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\waddr_reg_n_2_[1] ),
        .I3(\waddr_reg_n_2_[0] ),
        .O(int_ap_start_i_2_n_2));
  LUT4 #(
    .INIT(16'hEFFF)) 
    int_ap_start_i_3
       (.I0(\waddr_reg_n_2_[3] ),
        .I1(\waddr_reg_n_2_[4] ),
        .I2(s_axi_CONTROL_BUS_WVALID),
        .I3(out[1]),
        .O(int_ap_start_i_3_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_2),
        .Q(ap_start),
        .R(ARESET));
  LUT4 #(
    .INIT(16'hFB08)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(int_ap_start_i_2_n_2),
        .I2(int_ap_start_i_3_n_2),
        .I3(int_auto_restart),
        .O(int_auto_restart_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_2),
        .Q(int_auto_restart),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_ap_start_i_3_n_2),
        .I2(\waddr_reg_n_2_[2] ),
        .I3(s_axi_CONTROL_BUS_WSTRB[0]),
        .I4(int_gie_i_2_n_2),
        .I5(int_gie_reg_n_2),
        .O(int_gie_i_1_n_2));
  LUT2 #(
    .INIT(4'hE)) 
    int_gie_i_2
       (.I0(\waddr_reg_n_2_[0] ),
        .I1(\waddr_reg_n_2_[1] ),
        .O(int_gie_i_2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_2),
        .Q(int_gie_reg_n_2),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_2_[4] ),
        .I2(int_ap_start_i_2_n_2),
        .I3(\int_ier[1]_i_2_n_2 ),
        .I4(\int_ier_reg_n_2_[0] ),
        .O(\int_ier[0]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_2_[4] ),
        .I2(int_ap_start_i_2_n_2),
        .I3(\int_ier[1]_i_2_n_2 ),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_2 ));
  LUT3 #(
    .INIT(8'h80)) 
    \int_ier[1]_i_2 
       (.I0(\waddr_reg_n_2_[3] ),
        .I1(s_axi_CONTROL_BUS_WVALID),
        .I2(out[1]),
        .O(\int_ier[1]_i_2_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_2 ),
        .Q(\int_ier_reg_n_2_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_2 ),
        .Q(p_0_in),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFF7F7F7FFF808080)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\int_ier[1]_i_2_n_2 ),
        .I2(\int_isr[0]_i_2_n_2 ),
        .I3(\int_ier_reg_n_2_[0] ),
        .I4(Q[2]),
        .I5(\int_isr_reg_n_2_[0] ),
        .O(\int_isr[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000040)) 
    \int_isr[0]_i_2 
       (.I0(\waddr_reg_n_2_[4] ),
        .I1(\waddr_reg_n_2_[2] ),
        .I2(s_axi_CONTROL_BUS_WSTRB[0]),
        .I3(\waddr_reg_n_2_[0] ),
        .I4(\waddr_reg_n_2_[1] ),
        .O(\int_isr[0]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'hFF7F7F7FFF808080)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\int_ier[1]_i_2_n_2 ),
        .I2(\int_isr[0]_i_2_n_2 ),
        .I3(p_0_in),
        .I4(Q[2]),
        .I5(p_1_in),
        .O(\int_isr[1]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_2 ),
        .Q(\int_isr_reg_n_2_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_2 ),
        .Q(p_1_in),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[0] ),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[10] ),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[11] ),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[12] ),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[13] ),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[14] ),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[15] ),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[16] ),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[17] ),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[18] ),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[19] ),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[1] ),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[20] ),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[21] ),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[22] ),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_2_[23] ),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[24] ),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[25] ),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[26] ),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[27] ),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[28] ),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[29] ),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[2] ),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[30] ),
        .O(\or [30]));
  LUT5 #(
    .INIT(32'h00100000)) 
    \int_searched[31]_i_1 
       (.I0(\waddr_reg_n_2_[0] ),
        .I1(\waddr_reg_n_2_[1] ),
        .I2(\waddr_reg_n_2_[4] ),
        .I3(\waddr_reg_n_2_[2] ),
        .I4(\int_ier[1]_i_2_n_2 ),
        .O(p_0_in11_out));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_2_[31] ),
        .O(\or [31]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[3] ),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[4] ),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[5] ),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[6] ),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_2_[7] ),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[8] ),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_2_[9] ),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [0]),
        .Q(\int_searched_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [10]),
        .Q(\int_searched_reg_n_2_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [11]),
        .Q(\int_searched_reg_n_2_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [12]),
        .Q(\int_searched_reg_n_2_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [13]),
        .Q(\int_searched_reg_n_2_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [14]),
        .Q(\int_searched_reg_n_2_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [15]),
        .Q(\int_searched_reg_n_2_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [16]),
        .Q(\int_searched_reg_n_2_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [17]),
        .Q(\int_searched_reg_n_2_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [18]),
        .Q(\int_searched_reg_n_2_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [19]),
        .Q(\int_searched_reg_n_2_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [1]),
        .Q(\int_searched_reg_n_2_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [20]),
        .Q(\int_searched_reg_n_2_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [21]),
        .Q(\int_searched_reg_n_2_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [22]),
        .Q(\int_searched_reg_n_2_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [23]),
        .Q(\int_searched_reg_n_2_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [24]),
        .Q(\int_searched_reg_n_2_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [25]),
        .Q(\int_searched_reg_n_2_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [26]),
        .Q(\int_searched_reg_n_2_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [27]),
        .Q(\int_searched_reg_n_2_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [28]),
        .Q(\int_searched_reg_n_2_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [29]),
        .Q(\int_searched_reg_n_2_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [2]),
        .Q(\int_searched_reg_n_2_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [30]),
        .Q(\int_searched_reg_n_2_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [31]),
        .Q(\int_searched_reg_n_2_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [3]),
        .Q(\int_searched_reg_n_2_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [4]),
        .Q(\int_searched_reg_n_2_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [5]),
        .Q(\int_searched_reg_n_2_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [6]),
        .Q(\int_searched_reg_n_2_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [7]),
        .Q(\int_searched_reg_n_2_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [8]),
        .Q(\int_searched_reg_n_2_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in11_out),
        .D(\or [9]),
        .Q(\int_searched_reg_n_2_[9] ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hE0)) 
    interrupt_INST_0
       (.I0(\int_isr_reg_n_2_[0] ),
        .I1(p_1_in),
        .I2(int_gie_reg_n_2),
        .O(interrupt));
  LUT5 #(
    .INIT(32'hEAFFEAEA)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[15]_i_3_n_2 ),
        .I1(\rdata_data[7]_i_3_n_2 ),
        .I2(\int_searched_reg_n_2_[0] ),
        .I3(\rdata_data[1]_i_2_n_2 ),
        .I4(\rdata_data[0]_i_2_n_2 ),
        .O(rdata_data[0]));
  LUT6 #(
    .INIT(64'hCCFFAAF0CC00AAF0)) 
    \rdata_data[0]_i_2 
       (.I0(\int_ier_reg_n_2_[0] ),
        .I1(\int_isr_reg_n_2_[0] ),
        .I2(ap_start),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[2]),
        .I5(int_gie_reg_n_2),
        .O(\rdata_data[0]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[10] ),
        .O(\rdata_data[10]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[11] ),
        .O(\rdata_data[11]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[12] ),
        .O(\rdata_data[12]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[13] ),
        .O(\rdata_data[13]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[14] ),
        .O(\rdata_data[14]_i_1_n_2 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \rdata_data[15]_i_1 
       (.I0(\rdata_data[15]_i_3_n_2 ),
        .I1(s_axi_CONTROL_BUS_ARVALID),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rdata_data[15]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[15]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[15] ),
        .O(\rdata_data[15]_i_2_n_2 ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \rdata_data[15]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(int_ap_return),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[15]_i_3_n_2 ));
  LUT5 #(
    .INIT(32'hEAFFEAEA)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[15]_i_3_n_2 ),
        .I1(\rdata_data[7]_i_3_n_2 ),
        .I2(\int_searched_reg_n_2_[1] ),
        .I3(\rdata_data[1]_i_2_n_2 ),
        .I4(\rdata_data[1]_i_3_n_2 ),
        .O(rdata_data[1]));
  LUT3 #(
    .INIT(8'hFE)) 
    \rdata_data[1]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[1]_i_2_n_2 ));
  LUT5 #(
    .INIT(32'hFC0A0C0A)) 
    \rdata_data[1]_i_3 
       (.I0(int_ap_done),
        .I1(p_0_in),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(p_1_in),
        .O(\rdata_data[1]_i_3_n_2 ));
  LUT5 #(
    .INIT(32'hFFAEAEAE)) 
    \rdata_data[2]_i_1 
       (.I0(\rdata_data[15]_i_3_n_2 ),
        .I1(int_ap_idle),
        .I2(\rdata_data[7]_i_2_n_2 ),
        .I3(\int_searched_reg_n_2_[2] ),
        .I4(\rdata_data[7]_i_3_n_2 ),
        .O(rdata_data[2]));
  LUT6 #(
    .INIT(64'hAAA8AAAAAAAAAAAA)) 
    \rdata_data[31]_i_1 
       (.I0(ar_hs),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[31]_i_1_n_2 ));
  LUT3 #(
    .INIT(8'h10)) 
    \rdata_data[31]_i_2 
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .O(ar_hs));
  LUT5 #(
    .INIT(32'hFFAEAEAE)) 
    \rdata_data[3]_i_1 
       (.I0(\rdata_data[15]_i_3_n_2 ),
        .I1(int_ap_ready),
        .I2(\rdata_data[7]_i_2_n_2 ),
        .I3(\int_searched_reg_n_2_[3] ),
        .I4(\rdata_data[7]_i_3_n_2 ),
        .O(rdata_data[3]));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[4] ),
        .O(\rdata_data[4]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[5] ),
        .O(\rdata_data[5]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[6] ),
        .O(\rdata_data[6]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hFFAEAEAE)) 
    \rdata_data[7]_i_1 
       (.I0(\rdata_data[15]_i_3_n_2 ),
        .I1(int_auto_restart),
        .I2(\rdata_data[7]_i_2_n_2 ),
        .I3(\int_searched_reg_n_2_[7] ),
        .I4(\rdata_data[7]_i_3_n_2 ),
        .O(rdata_data[7]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \rdata_data[7]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[7]_i_2_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \rdata_data[7]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[7]_i_3_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[8] ),
        .O(\rdata_data[8]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \rdata_data[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_2_[9] ),
        .O(\rdata_data[9]_i_1_n_2 ));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDSE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[10]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[11]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[12]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[13]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[14]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[15]_i_2_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[16] ),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[17] ),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[18] ),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[19] ),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[20] ),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[21] ),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[22] ),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[23] ),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[24] ),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[25] ),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[26] ),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[27] ),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[28] ),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[29] ),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[30] ),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\int_searched_reg_n_2_[31] ),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(\rdata_data[31]_i_1_n_2 ));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(1'b0));
  FDSE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[4]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[5]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[6]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(1'b0));
  FDSE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[8]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .S(\rdata_data[15]_i_1_n_2 ));
  FDSE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[9]_i_1_n_2 ),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .S(\rdata_data[15]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h005C)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_RREADY),
        .I1(s_axi_CONTROL_BUS_ARVALID),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rstate[0]_i_1_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_2 ),
        .Q(rstate[0]),
        .R(ARESET));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(ARESET));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[4]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_2_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_2_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_2_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_2_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_2_[4] ),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 5, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [4:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [4:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [4:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [4:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "5" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
