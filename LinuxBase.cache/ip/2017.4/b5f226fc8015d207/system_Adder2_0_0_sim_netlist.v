// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Fri Mar  9 12:58:25 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    LAST_STREAM_TDATA,
    LAST_STREAM_TVALID,
    LAST_STREAM_TREADY,
    LAST_STREAM_TKEEP,
    LAST_STREAM_TSTRB,
    LAST_STREAM_TUSER,
    LAST_STREAM_TLAST,
    LAST_STREAM_TID,
    LAST_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input [31:0]LAST_STREAM_TDATA;
  input LAST_STREAM_TVALID;
  output LAST_STREAM_TREADY;
  input [3:0]LAST_STREAM_TKEEP;
  input [3:0]LAST_STREAM_TSTRB;
  input [1:0]LAST_STREAM_TUSER;
  input [0:0]LAST_STREAM_TLAST;
  input [4:0]LAST_STREAM_TID;
  input [5:0]LAST_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire ARESET;
  wire Adder2_CONTROL_BUS_s_axi_U_n_12;
  wire Adder2_CONTROL_BUS_s_axi_U_n_6;
  wire [31:0]INPUT_STREAM_TDATA;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire INPUT_STREAM_V_data_V_0_ack_in;
  wire INPUT_STREAM_V_data_V_0_load_A;
  wire INPUT_STREAM_V_data_V_0_load_B;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_A;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_B;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_5;
  wire INPUT_STREAM_V_data_V_0_sel_wr;
  wire INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_5;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_5 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_5 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_5_[0] ;
  wire [31:0]LAST_STREAM_TDATA;
  wire LAST_STREAM_TREADY;
  wire LAST_STREAM_TVALID;
  wire LAST_STREAM_V_data_V_0_ack_in;
  wire LAST_STREAM_V_data_V_0_load_A;
  wire LAST_STREAM_V_data_V_0_load_B;
  wire [31:0]LAST_STREAM_V_data_V_0_payload_A;
  wire [31:0]LAST_STREAM_V_data_V_0_payload_B;
  wire LAST_STREAM_V_data_V_0_sel;
  wire LAST_STREAM_V_data_V_0_sel_rd_i_1_n_5;
  wire LAST_STREAM_V_data_V_0_sel_wr;
  wire LAST_STREAM_V_data_V_0_sel_wr_i_1_n_5;
  wire [1:1]LAST_STREAM_V_data_V_0_state;
  wire \LAST_STREAM_V_data_V_0_state[0]_i_1_n_5 ;
  wire \LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ;
  wire [1:1]LAST_STREAM_V_dest_V_0_state;
  wire \LAST_STREAM_V_dest_V_0_state[0]_i_1_n_5 ;
  wire \LAST_STREAM_V_dest_V_0_state_reg_n_5_[0] ;
  wire [31:0]a_a_1_reg_380;
  wire a_a_1_reg_3800;
  wire \a_b_2_reg_234_reg_n_5_[0] ;
  wire \a_b_2_reg_234_reg_n_5_[10] ;
  wire \a_b_2_reg_234_reg_n_5_[1] ;
  wire \a_b_2_reg_234_reg_n_5_[2] ;
  wire \a_b_2_reg_234_reg_n_5_[3] ;
  wire \a_b_2_reg_234_reg_n_5_[4] ;
  wire \a_b_2_reg_234_reg_n_5_[5] ;
  wire \a_b_2_reg_234_reg_n_5_[6] ;
  wire \a_b_2_reg_234_reg_n_5_[7] ;
  wire \a_b_2_reg_234_reg_n_5_[8] ;
  wire \a_b_2_reg_234_reg_n_5_[9] ;
  wire agg_result_a_ap_vld;
  wire \ap_CS_fsm[1]_i_2_n_5 ;
  wire \ap_CS_fsm[1]_i_3_n_5 ;
  wire \ap_CS_fsm[1]_i_5_n_5 ;
  wire \ap_CS_fsm[2]_i_2_n_5 ;
  wire \ap_CS_fsm[2]_i_3_n_5 ;
  wire \ap_CS_fsm[2]_i_4_n_5 ;
  wire \ap_CS_fsm[2]_i_6_n_5 ;
  wire \ap_CS_fsm[2]_i_7_n_5 ;
  wire \ap_CS_fsm[3]_i_2_n_5 ;
  wire \ap_CS_fsm[3]_i_4_n_5 ;
  wire \ap_CS_fsm[4]_i_2_n_5 ;
  wire \ap_CS_fsm[4]_i_5_n_5 ;
  wire \ap_CS_fsm[4]_i_6_n_5 ;
  wire \ap_CS_fsm[4]_i_7_n_5 ;
  wire ap_CS_fsm_pp0_stage0;
  wire ap_CS_fsm_pp1_stage0;
  wire \ap_CS_fsm_reg_n_5_[0] ;
  wire ap_CS_fsm_state11;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state7;
  wire ap_CS_fsm_state8;
  wire ap_CS_fsm_state9;
  wire [8:0]ap_NS_fsm;
  wire ap_NS_fsm16_out;
  wire ap_block_pp0_stage0_subdone;
  wire ap_block_pp1_stage0_11001;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter1_i_3_n_5;
  wire ap_enable_reg_pp0_iter1_reg_n_5;
  wire ap_enable_reg_pp1_iter0;
  wire ap_enable_reg_pp1_iter0_i_1_n_5;
  wire ap_enable_reg_pp1_iter1_i_1_n_5;
  wire ap_enable_reg_pp1_iter1_i_2_n_5;
  wire ap_enable_reg_pp1_iter1_reg_n_5;
  wire [7:1]ap_phi_mux_i_1_phi_fu_226_p4;
  wire [8:2]ap_phi_mux_i_1_phi_fu_226_p4__0;
  wire [7:1]ap_phi_mux_i_phi_fu_214_p4;
  wire [8:2]ap_phi_mux_i_phi_fu_214_p4__0;
  wire [31:0]ap_phi_mux_storemerge1_phi_fu_249_p4;
  wire [31:0]ap_phi_mux_storemerge_phi_fu_260_p4;
  wire ap_rst_n;
  wire exitcond1_reg_3430;
  wire \exitcond1_reg_343[0]_i_1_n_5 ;
  wire \exitcond1_reg_343_reg_n_5_[0] ;
  wire \exitcond_reg_352[0]_i_1_n_5 ;
  wire \exitcond_reg_352_reg_n_5_[0] ;
  wire \i_1_reg_222_reg_n_5_[0] ;
  wire \i_1_reg_222_reg_n_5_[10] ;
  wire \i_1_reg_222_reg_n_5_[1] ;
  wire \i_1_reg_222_reg_n_5_[2] ;
  wire \i_1_reg_222_reg_n_5_[3] ;
  wire \i_1_reg_222_reg_n_5_[4] ;
  wire \i_1_reg_222_reg_n_5_[5] ;
  wire \i_1_reg_222_reg_n_5_[6] ;
  wire \i_1_reg_222_reg_n_5_[7] ;
  wire \i_1_reg_222_reg_n_5_[8] ;
  wire \i_1_reg_222_reg_n_5_[9] ;
  wire [10:0]i_2_fu_274_p2;
  wire i_2_reg_3470;
  wire \i_2_reg_347[10]_i_3_n_5 ;
  wire \i_2_reg_347[10]_i_4_n_5 ;
  wire \i_2_reg_347[10]_i_5_n_5 ;
  wire \i_2_reg_347[10]_i_8_n_5 ;
  wire \i_2_reg_347[2]_i_2_n_5 ;
  wire \i_2_reg_347[3]_i_2_n_5 ;
  wire \i_2_reg_347[4]_i_2_n_5 ;
  wire \i_2_reg_347[5]_i_2_n_5 ;
  wire \i_2_reg_347[6]_i_2_n_5 ;
  wire \i_2_reg_347[8]_i_2_n_5 ;
  wire [10:0]i_2_reg_347_reg__0;
  wire [10:0]i_3_fu_296_p2;
  wire i_3_reg_3560;
  wire \i_3_reg_356[10]_i_3_n_5 ;
  wire \i_3_reg_356[10]_i_4_n_5 ;
  wire \i_3_reg_356[10]_i_5_n_5 ;
  wire \i_3_reg_356[10]_i_8_n_5 ;
  wire \i_3_reg_356[2]_i_2_n_5 ;
  wire \i_3_reg_356[3]_i_2_n_5 ;
  wire \i_3_reg_356[4]_i_2_n_5 ;
  wire \i_3_reg_356[5]_i_2_n_5 ;
  wire \i_3_reg_356[6]_i_2_n_5 ;
  wire \i_3_reg_356[8]_i_2_n_5 ;
  wire [10:0]i_3_reg_356_reg__0;
  wire [10:0]i_4_fu_320_p2;
  wire [10:0]i_4_reg_365;
  wire \i_4_reg_365[10]_i_2_n_5 ;
  wire i_reg_210;
  wire \i_reg_210_reg_n_5_[0] ;
  wire \i_reg_210_reg_n_5_[10] ;
  wire \i_reg_210_reg_n_5_[1] ;
  wire \i_reg_210_reg_n_5_[2] ;
  wire \i_reg_210_reg_n_5_[3] ;
  wire \i_reg_210_reg_n_5_[4] ;
  wire \i_reg_210_reg_n_5_[5] ;
  wire \i_reg_210_reg_n_5_[6] ;
  wire \i_reg_210_reg_n_5_[7] ;
  wire \i_reg_210_reg_n_5_[8] ;
  wire \i_reg_210_reg_n_5_[9] ;
  wire [31:0]inputValues_0_q0;
  wire interrupt;
  wire [31:0]lastValues_0_q0;
  wire p_52_in;
  wire p_67_in;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire tmp_5_reg_361;
  wire tmp_8_fu_332_p2;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.ARESET(ARESET),
        .D(ap_NS_fsm[1:0]),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .Q({agg_result_a_ap_vld,ap_CS_fsm_pp0_stage0,\ap_CS_fsm_reg_n_5_[0] }),
        .SR(i_reg_210),
        .\ap_CS_fsm_reg[8] (\ap_CS_fsm[1]_i_3_n_5 ),
        .ap_block_pp0_stage0_subdone(ap_block_pp0_stage0_subdone),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter0(ap_enable_reg_pp0_iter0),
        .ap_enable_reg_pp0_iter0_reg(Adder2_CONTROL_BUS_s_axi_U_n_12),
        .ap_enable_reg_pp0_iter1_reg(Adder2_CONTROL_BUS_s_axi_U_n_6),
        .ap_enable_reg_pp0_iter1_reg_0(ap_enable_reg_pp0_iter1_reg_n_5),
        .ap_phi_mux_i_phi_fu_214_p4__0({ap_phi_mux_i_phi_fu_214_p4__0[4],ap_phi_mux_i_phi_fu_214_p4__0[2]}),
        .ap_rst_n(ap_rst_n),
        .\exitcond1_reg_343_reg[0] (\exitcond1_reg_343_reg_n_5_[0] ),
        .\i_2_reg_347_reg[10] (\ap_CS_fsm[2]_i_3_n_5 ),
        .\i_reg_210_reg[0] (\ap_CS_fsm[1]_i_2_n_5 ),
        .\i_reg_210_reg[2] (\ap_CS_fsm[2]_i_4_n_5 ),
        .\i_reg_210_reg[3] (ap_enable_reg_pp0_iter1_i_3_n_5),
        .\i_reg_210_reg[8] (\ap_CS_fsm[2]_i_2_n_5 ),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .ram_reg(ap_phi_mux_storemerge1_phi_fu_249_p4),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .\tmp_5_reg_361_reg[0] (ap_phi_mux_storemerge_phi_fu_260_p4));
  GND GND
       (.G(\<const0> ));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_A));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \INPUT_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_B));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    INPUT_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_enable_reg_pp0_iter1_reg_n_5),
        .I3(\exitcond1_reg_343_reg_n_5_[0] ),
        .I4(INPUT_STREAM_V_data_V_0_sel),
        .O(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_5),
        .Q(INPUT_STREAM_V_data_V_0_sel),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_5),
        .Q(INPUT_STREAM_V_data_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hA280AA80)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_TVALID),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I4(p_67_in),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_5 ));
  LUT6 #(
    .INIT(64'h40FF40FFFFFF40FF)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(\exitcond1_reg_343_reg_n_5_[0] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I4(INPUT_STREAM_V_data_V_0_ack_in),
        .I5(INPUT_STREAM_TVALID),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_5 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(INPUT_STREAM_V_data_V_0_ack_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hA280AA80)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TREADY),
        .I2(INPUT_STREAM_TVALID),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_5_[0] ),
        .I4(p_67_in),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_5 ));
  LUT4 #(
    .INIT(16'hFF75)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_5_[0] ),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_TREADY),
        .I3(p_67_in),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_5 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_5_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h0D)) 
    \LAST_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I1(LAST_STREAM_V_data_V_0_ack_in),
        .I2(LAST_STREAM_V_data_V_0_sel_wr),
        .O(LAST_STREAM_V_data_V_0_load_A));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[0]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[10]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[11]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[12]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[13]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[14]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[15]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[16]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[17]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[18]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[19]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[1]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[20]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[21]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[22]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[23]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[24]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[25]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[26]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[27]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[28]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[29]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[2]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[30]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[31]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[3]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[4]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[5]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[6]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[7]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[8]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_A),
        .D(LAST_STREAM_TDATA[9]),
        .Q(LAST_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \LAST_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I1(LAST_STREAM_V_data_V_0_ack_in),
        .I2(LAST_STREAM_V_data_V_0_sel_wr),
        .O(LAST_STREAM_V_data_V_0_load_B));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[0]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[10]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[11]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[12]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[13]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[14]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[15]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[16]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[17]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[18]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[19]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[1]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[20]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[21]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[22]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[23]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[24]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[25]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[26]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[27]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[28]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[29]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[2]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[30]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[31]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[3]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[4]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[5]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[6]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[7]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[8]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \LAST_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(LAST_STREAM_V_data_V_0_load_B),
        .D(LAST_STREAM_TDATA[9]),
        .Q(LAST_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    LAST_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(ap_enable_reg_pp1_iter1_reg_n_5),
        .I1(\exitcond_reg_352_reg_n_5_[0] ),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(LAST_STREAM_V_data_V_0_sel),
        .O(LAST_STREAM_V_data_V_0_sel_rd_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    LAST_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_sel_rd_i_1_n_5),
        .Q(LAST_STREAM_V_data_V_0_sel),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h78)) 
    LAST_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(LAST_STREAM_TVALID),
        .I1(LAST_STREAM_V_data_V_0_ack_in),
        .I2(LAST_STREAM_V_data_V_0_sel_wr),
        .O(LAST_STREAM_V_data_V_0_sel_wr_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    LAST_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_sel_wr_i_1_n_5),
        .Q(LAST_STREAM_V_data_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hA280AA80)) 
    \LAST_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(LAST_STREAM_V_data_V_0_ack_in),
        .I2(LAST_STREAM_TVALID),
        .I3(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I4(p_52_in),
        .O(\LAST_STREAM_V_data_V_0_state[0]_i_1_n_5 ));
  LUT6 #(
    .INIT(64'h20FF20FFFFFF20FF)) 
    \LAST_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\exitcond_reg_352_reg_n_5_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_5),
        .I3(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I4(LAST_STREAM_V_data_V_0_ack_in),
        .I5(LAST_STREAM_TVALID),
        .O(LAST_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_data_V_0_state[0]_i_1_n_5 ),
        .Q(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_state),
        .Q(LAST_STREAM_V_data_V_0_ack_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hA280AA80)) 
    \LAST_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(LAST_STREAM_TREADY),
        .I2(LAST_STREAM_TVALID),
        .I3(\LAST_STREAM_V_dest_V_0_state_reg_n_5_[0] ),
        .I4(p_52_in),
        .O(\LAST_STREAM_V_dest_V_0_state[0]_i_1_n_5 ));
  LUT4 #(
    .INIT(16'hFF75)) 
    \LAST_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(\LAST_STREAM_V_dest_V_0_state_reg_n_5_[0] ),
        .I1(LAST_STREAM_TVALID),
        .I2(LAST_STREAM_TREADY),
        .I3(p_52_in),
        .O(LAST_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_dest_V_0_state[0]_i_1_n_5 ),
        .Q(\LAST_STREAM_V_dest_V_0_state_reg_n_5_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_dest_V_0_state),
        .Q(LAST_STREAM_TREADY),
        .R(ARESET));
  LUT2 #(
    .INIT(4'h2)) 
    \a_a_1_reg_380[31]_i_1 
       (.I0(ap_CS_fsm_state9),
        .I1(tmp_5_reg_361),
        .O(a_a_1_reg_3800));
  FDRE \a_a_1_reg_380_reg[0] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[0]),
        .Q(a_a_1_reg_380[0]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[10] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[10]),
        .Q(a_a_1_reg_380[10]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[11] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[11]),
        .Q(a_a_1_reg_380[11]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[12] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[12]),
        .Q(a_a_1_reg_380[12]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[13] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[13]),
        .Q(a_a_1_reg_380[13]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[14] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[14]),
        .Q(a_a_1_reg_380[14]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[15] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[15]),
        .Q(a_a_1_reg_380[15]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[16] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[16]),
        .Q(a_a_1_reg_380[16]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[17] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[17]),
        .Q(a_a_1_reg_380[17]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[18] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[18]),
        .Q(a_a_1_reg_380[18]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[19] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[19]),
        .Q(a_a_1_reg_380[19]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[1] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[1]),
        .Q(a_a_1_reg_380[1]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[20] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[20]),
        .Q(a_a_1_reg_380[20]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[21] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[21]),
        .Q(a_a_1_reg_380[21]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[22] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[22]),
        .Q(a_a_1_reg_380[22]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[23] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[23]),
        .Q(a_a_1_reg_380[23]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[24] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[24]),
        .Q(a_a_1_reg_380[24]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[25] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[25]),
        .Q(a_a_1_reg_380[25]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[26] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[26]),
        .Q(a_a_1_reg_380[26]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[27] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[27]),
        .Q(a_a_1_reg_380[27]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[28] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[28]),
        .Q(a_a_1_reg_380[28]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[29] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[29]),
        .Q(a_a_1_reg_380[29]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[2] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[2]),
        .Q(a_a_1_reg_380[2]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[30] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[30]),
        .Q(a_a_1_reg_380[30]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[31] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[31]),
        .Q(a_a_1_reg_380[31]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[3] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[3]),
        .Q(a_a_1_reg_380[3]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[4] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[4]),
        .Q(a_a_1_reg_380[4]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[5] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[5]),
        .Q(a_a_1_reg_380[5]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[6] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[6]),
        .Q(a_a_1_reg_380[6]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[7] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[7]),
        .Q(a_a_1_reg_380[7]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[8] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[8]),
        .Q(a_a_1_reg_380[8]),
        .R(1'b0));
  FDRE \a_a_1_reg_380_reg[9] 
       (.C(ap_clk),
        .CE(a_a_1_reg_3800),
        .D(inputValues_0_q0[9]),
        .Q(a_a_1_reg_380[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h40)) 
    \a_b_2_reg_234[10]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(ap_CS_fsm_state9),
        .I2(tmp_8_fu_332_p2),
        .O(ap_NS_fsm16_out));
  FDRE \a_b_2_reg_234_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[0]),
        .Q(\a_b_2_reg_234_reg_n_5_[0] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[10]),
        .Q(\a_b_2_reg_234_reg_n_5_[10] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[1]),
        .Q(\a_b_2_reg_234_reg_n_5_[1] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[2]),
        .Q(\a_b_2_reg_234_reg_n_5_[2] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[3]),
        .Q(\a_b_2_reg_234_reg_n_5_[3] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[4]),
        .Q(\a_b_2_reg_234_reg_n_5_[4] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[5]),
        .Q(\a_b_2_reg_234_reg_n_5_[5] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[6]),
        .Q(\a_b_2_reg_234_reg_n_5_[6] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[7]),
        .Q(\a_b_2_reg_234_reg_n_5_[7] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[8]),
        .Q(\a_b_2_reg_234_reg_n_5_[8] ),
        .R(ap_CS_fsm_state7));
  FDRE \a_b_2_reg_234_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm16_out),
        .D(i_4_reg_365[9]),
        .Q(\a_b_2_reg_234_reg_n_5_[9] ),
        .R(ap_CS_fsm_state7));
  LUT5 #(
    .INIT(32'h00010000)) 
    \ap_CS_fsm[1]_i_2 
       (.I0(\i_2_reg_347[2]_i_2_n_5 ),
        .I1(\i_2_reg_347[10]_i_4_n_5 ),
        .I2(ap_phi_mux_i_phi_fu_214_p4__0[8]),
        .I3(\i_2_reg_347[10]_i_3_n_5 ),
        .I4(\i_2_reg_347[10]_i_8_n_5 ),
        .O(\ap_CS_fsm[1]_i_2_n_5 ));
  LUT5 #(
    .INIT(32'h00010000)) 
    \ap_CS_fsm[1]_i_3 
       (.I0(ap_CS_fsm_state11),
        .I1(ap_CS_fsm_state8),
        .I2(ap_CS_fsm_state9),
        .I3(agg_result_a_ap_vld),
        .I4(\ap_CS_fsm[1]_i_5_n_5 ),
        .O(\ap_CS_fsm[1]_i_3_n_5 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \ap_CS_fsm[1]_i_5 
       (.I0(ap_CS_fsm_state4),
        .I1(\ap_CS_fsm_reg_n_5_[0] ),
        .I2(ap_CS_fsm_state7),
        .I3(ap_CS_fsm_pp1_stage0),
        .O(\ap_CS_fsm[1]_i_5_n_5 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(\ap_CS_fsm[2]_i_2_n_5 ),
        .I3(\ap_CS_fsm[2]_i_3_n_5 ),
        .I4(\ap_CS_fsm[2]_i_4_n_5 ),
        .I5(ap_block_pp0_stage0_subdone),
        .O(ap_NS_fsm[2]));
  LUT6 #(
    .INIT(64'h0000000000440347)) 
    \ap_CS_fsm[2]_i_2 
       (.I0(\i_reg_210_reg_n_5_[8] ),
        .I1(\i_2_reg_347[8]_i_2_n_5 ),
        .I2(i_2_reg_347_reg__0[8]),
        .I3(\i_reg_210_reg_n_5_[6] ),
        .I4(i_2_reg_347_reg__0[6]),
        .I5(\i_2_reg_347[2]_i_2_n_5 ),
        .O(\ap_CS_fsm[2]_i_2_n_5 ));
  LUT5 #(
    .INIT(32'h000ACC0A)) 
    \ap_CS_fsm[2]_i_3 
       (.I0(i_2_reg_347_reg__0[10]),
        .I1(\i_reg_210_reg_n_5_[10] ),
        .I2(i_2_reg_347_reg__0[9]),
        .I3(\i_2_reg_347[8]_i_2_n_5 ),
        .I4(\i_reg_210_reg_n_5_[9] ),
        .O(\ap_CS_fsm[2]_i_3_n_5 ));
  LUT6 #(
    .INIT(64'h1111050000000500)) 
    \ap_CS_fsm[2]_i_4 
       (.I0(ap_phi_mux_i_phi_fu_214_p4__0[4]),
        .I1(\i_reg_210_reg_n_5_[2] ),
        .I2(i_2_reg_347_reg__0[2]),
        .I3(\ap_CS_fsm[2]_i_6_n_5 ),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\ap_CS_fsm[2]_i_7_n_5 ),
        .O(\ap_CS_fsm[2]_i_4_n_5 ));
  LUT3 #(
    .INIT(8'h02)) 
    \ap_CS_fsm[2]_i_5 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_5),
        .I1(\exitcond1_reg_343_reg_n_5_[0] ),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .O(ap_block_pp0_stage0_subdone));
  LUT4 #(
    .INIT(16'h0001)) 
    \ap_CS_fsm[2]_i_6 
       (.I0(i_2_reg_347_reg__0[3]),
        .I1(i_2_reg_347_reg__0[1]),
        .I2(i_2_reg_347_reg__0[7]),
        .I3(i_2_reg_347_reg__0[5]),
        .O(\ap_CS_fsm[2]_i_6_n_5 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \ap_CS_fsm[2]_i_7 
       (.I0(\i_reg_210_reg_n_5_[3] ),
        .I1(\i_reg_210_reg_n_5_[1] ),
        .I2(\i_reg_210_reg_n_5_[7] ),
        .I3(\i_reg_210_reg_n_5_[5] ),
        .O(\ap_CS_fsm[2]_i_7_n_5 ));
  LUT6 #(
    .INIT(64'hBAFAFAFAFAFAFAFA)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(ap_CS_fsm_pp1_stage0),
        .I3(\ap_CS_fsm[4]_i_5_n_5 ),
        .I4(\ap_CS_fsm[3]_i_2_n_5 ),
        .I5(ap_block_pp1_stage0_11001),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'h1111050000000500)) 
    \ap_CS_fsm[3]_i_2 
       (.I0(ap_phi_mux_i_1_phi_fu_226_p4__0[4]),
        .I1(\i_1_reg_222_reg_n_5_[2] ),
        .I2(i_3_reg_356_reg__0[2]),
        .I3(\ap_CS_fsm[4]_i_7_n_5 ),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\ap_CS_fsm[3]_i_4_n_5 ),
        .O(\ap_CS_fsm[3]_i_2_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \ap_CS_fsm[3]_i_3 
       (.I0(ap_enable_reg_pp1_iter1_reg_n_5),
        .I1(\exitcond_reg_352_reg_n_5_[0] ),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .O(ap_block_pp1_stage0_11001));
  LUT4 #(
    .INIT(16'h0001)) 
    \ap_CS_fsm[3]_i_4 
       (.I0(\i_1_reg_222_reg_n_5_[3] ),
        .I1(\i_1_reg_222_reg_n_5_[1] ),
        .I2(\i_1_reg_222_reg_n_5_[7] ),
        .I3(\i_1_reg_222_reg_n_5_[5] ),
        .O(\ap_CS_fsm[3]_i_4_n_5 ));
  LUT6 #(
    .INIT(64'h0000000000080000)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(\ap_CS_fsm[4]_i_2_n_5 ),
        .I2(ap_phi_mux_i_1_phi_fu_226_p4__0[2]),
        .I3(ap_phi_mux_i_1_phi_fu_226_p4__0[4]),
        .I4(\ap_CS_fsm[4]_i_5_n_5 ),
        .I5(\ap_CS_fsm[4]_i_6_n_5 ),
        .O(ap_NS_fsm[4]));
  LUT6 #(
    .INIT(64'h0001FFFF00010000)) 
    \ap_CS_fsm[4]_i_2 
       (.I0(\i_1_reg_222_reg_n_5_[3] ),
        .I1(\i_1_reg_222_reg_n_5_[1] ),
        .I2(\i_1_reg_222_reg_n_5_[7] ),
        .I3(\i_1_reg_222_reg_n_5_[5] ),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\ap_CS_fsm[4]_i_7_n_5 ),
        .O(\ap_CS_fsm[4]_i_2_n_5 ));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \ap_CS_fsm[4]_i_3 
       (.I0(\i_1_reg_222_reg_n_5_[2] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[2]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4__0[2]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \ap_CS_fsm[4]_i_4 
       (.I0(\i_1_reg_222_reg_n_5_[4] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[4]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4__0[4]));
  LUT5 #(
    .INIT(32'h00010000)) 
    \ap_CS_fsm[4]_i_5 
       (.I0(\i_3_reg_356[2]_i_2_n_5 ),
        .I1(\i_3_reg_356[10]_i_4_n_5 ),
        .I2(ap_phi_mux_i_1_phi_fu_226_p4__0[8]),
        .I3(\i_3_reg_356[10]_i_3_n_5 ),
        .I4(\i_3_reg_356[10]_i_8_n_5 ),
        .O(\ap_CS_fsm[4]_i_5_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h5755)) 
    \ap_CS_fsm[4]_i_6 
       (.I0(ap_CS_fsm_pp1_stage0),
        .I1(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_enable_reg_pp1_iter1_reg_n_5),
        .O(\ap_CS_fsm[4]_i_6_n_5 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \ap_CS_fsm[4]_i_7 
       (.I0(i_3_reg_356_reg__0[3]),
        .I1(i_3_reg_356_reg__0[1]),
        .I2(i_3_reg_356_reg__0[7]),
        .I3(i_3_reg_356_reg__0[5]),
        .O(\ap_CS_fsm[4]_i_7_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(tmp_8_fu_332_p2),
        .I1(ap_CS_fsm_state9),
        .I2(tmp_5_reg_361),
        .I3(ap_CS_fsm_state7),
        .O(ap_NS_fsm[5]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'hFF04)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(tmp_8_fu_332_p2),
        .I1(ap_CS_fsm_state9),
        .I2(tmp_5_reg_361),
        .I3(ap_CS_fsm_state11),
        .O(ap_NS_fsm[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[8]_i_1 
       (.I0(ap_CS_fsm_state9),
        .I1(tmp_5_reg_361),
        .O(ap_NS_fsm[8]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_5_[0] ),
        .S(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state4),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_pp1_stage0),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state7),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state8),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_CS_fsm_state8),
        .Q(ap_CS_fsm_state9),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(agg_result_a_ap_vld),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[8] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[8]),
        .Q(ap_CS_fsm_state11),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Adder2_CONTROL_BUS_s_axi_U_n_12),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0001FFFF00010000)) 
    ap_enable_reg_pp0_iter1_i_3
       (.I0(\i_reg_210_reg_n_5_[3] ),
        .I1(\i_reg_210_reg_n_5_[1] ),
        .I2(\i_reg_210_reg_n_5_[7] ),
        .I3(\i_reg_210_reg_n_5_[5] ),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\ap_CS_fsm[2]_i_6_n_5 ),
        .O(ap_enable_reg_pp0_iter1_i_3_n_5));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    ap_enable_reg_pp0_iter1_i_4
       (.I0(\i_reg_210_reg_n_5_[2] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[2]),
        .O(ap_phi_mux_i_phi_fu_214_p4__0[2]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    ap_enable_reg_pp0_iter1_i_5
       (.I0(\i_reg_210_reg_n_5_[4] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[4]),
        .O(ap_phi_mux_i_phi_fu_214_p4__0[4]));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Adder2_CONTROL_BUS_s_axi_U_n_6),
        .Q(ap_enable_reg_pp0_iter1_reg_n_5),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hA8A8A8A800A8A8A8)) 
    ap_enable_reg_pp1_iter0_i_1
       (.I0(ap_rst_n),
        .I1(ap_CS_fsm_state4),
        .I2(ap_enable_reg_pp1_iter0),
        .I3(\ap_CS_fsm[3]_i_2_n_5 ),
        .I4(\ap_CS_fsm[4]_i_5_n_5 ),
        .I5(\ap_CS_fsm[4]_i_6_n_5 ),
        .O(ap_enable_reg_pp1_iter0_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter0_i_1_n_5),
        .Q(ap_enable_reg_pp1_iter0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h000000000CCCAAAA)) 
    ap_enable_reg_pp1_iter1_i_1
       (.I0(ap_enable_reg_pp1_iter1_reg_n_5),
        .I1(ap_enable_reg_pp1_iter0),
        .I2(\ap_CS_fsm[4]_i_5_n_5 ),
        .I3(\ap_CS_fsm[3]_i_2_n_5 ),
        .I4(ap_block_pp1_stage0_11001),
        .I5(ap_enable_reg_pp1_iter1_i_2_n_5),
        .O(ap_enable_reg_pp1_iter1_i_1_n_5));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'h0040FFFF)) 
    ap_enable_reg_pp1_iter1_i_2
       (.I0(\exitcond_reg_352_reg_n_5_[0] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(ap_CS_fsm_state4),
        .I3(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I4(ap_rst_n),
        .O(ap_enable_reg_pp1_iter1_i_2_n_5));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp1_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp1_iter1_i_1_n_5),
        .Q(ap_enable_reg_pp1_iter1_reg_n_5),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0200FFFF02000000)) 
    \exitcond1_reg_343[0]_i_1 
       (.I0(ap_enable_reg_pp0_iter1_i_3_n_5),
        .I1(ap_phi_mux_i_phi_fu_214_p4__0[2]),
        .I2(ap_phi_mux_i_phi_fu_214_p4__0[4]),
        .I3(\ap_CS_fsm[1]_i_2_n_5 ),
        .I4(exitcond1_reg_3430),
        .I5(\exitcond1_reg_343_reg_n_5_[0] ),
        .O(\exitcond1_reg_343[0]_i_1_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'hA8AA)) 
    \exitcond1_reg_343[0]_i_2 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_enable_reg_pp0_iter1_reg_n_5),
        .O(exitcond1_reg_3430));
  FDRE \exitcond1_reg_343_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond1_reg_343[0]_i_1_n_5 ),
        .Q(\exitcond1_reg_343_reg_n_5_[0] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h888888B888888888)) 
    \exitcond_reg_352[0]_i_1 
       (.I0(\exitcond_reg_352_reg_n_5_[0] ),
        .I1(\ap_CS_fsm[4]_i_6_n_5 ),
        .I2(\ap_CS_fsm[4]_i_2_n_5 ),
        .I3(ap_phi_mux_i_1_phi_fu_226_p4__0[2]),
        .I4(ap_phi_mux_i_1_phi_fu_226_p4__0[4]),
        .I5(\ap_CS_fsm[4]_i_5_n_5 ),
        .O(\exitcond_reg_352[0]_i_1_n_5 ));
  FDRE \exitcond_reg_352_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\exitcond_reg_352[0]_i_1_n_5 ),
        .Q(\exitcond_reg_352_reg_n_5_[0] ),
        .R(1'b0));
  FDRE \i_1_reg_222_reg[0] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[0]),
        .Q(\i_1_reg_222_reg_n_5_[0] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[10] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[10]),
        .Q(\i_1_reg_222_reg_n_5_[10] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[1] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[1]),
        .Q(\i_1_reg_222_reg_n_5_[1] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[2] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[2]),
        .Q(\i_1_reg_222_reg_n_5_[2] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[3] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[3]),
        .Q(\i_1_reg_222_reg_n_5_[3] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[4] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[4]),
        .Q(\i_1_reg_222_reg_n_5_[4] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[5] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[5]),
        .Q(\i_1_reg_222_reg_n_5_[5] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[6] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[6]),
        .Q(\i_1_reg_222_reg_n_5_[6] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[7] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[7]),
        .Q(\i_1_reg_222_reg_n_5_[7] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[8] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[8]),
        .Q(\i_1_reg_222_reg_n_5_[8] ),
        .R(ap_CS_fsm_state4));
  FDRE \i_1_reg_222_reg[9] 
       (.C(ap_clk),
        .CE(p_52_in),
        .D(i_3_reg_356_reg__0[9]),
        .Q(\i_1_reg_222_reg_n_5_[9] ),
        .R(ap_CS_fsm_state4));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h10DF)) 
    \i_2_reg_347[0]_i_1 
       (.I0(i_2_reg_347_reg__0[0]),
        .I1(\exitcond1_reg_343_reg_n_5_[0] ),
        .I2(ap_enable_reg_pp0_iter1_reg_n_5),
        .I3(\i_reg_210_reg_n_5_[0] ),
        .O(i_2_fu_274_p2[0]));
  LUT5 #(
    .INIT(32'hFD000000)) 
    \i_2_reg_347[10]_i_1 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_5),
        .I1(\exitcond1_reg_343_reg_n_5_[0] ),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(ap_enable_reg_pp0_iter0),
        .O(i_2_reg_3470));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_10 
       (.I0(\i_reg_210_reg_n_5_[1] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[1]),
        .O(ap_phi_mux_i_phi_fu_214_p4[1]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_11 
       (.I0(\i_reg_210_reg_n_5_[3] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[3]),
        .O(ap_phi_mux_i_phi_fu_214_p4[3]));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \i_2_reg_347[10]_i_2 
       (.I0(\i_2_reg_347[10]_i_3_n_5 ),
        .I1(\i_2_reg_347[10]_i_4_n_5 ),
        .I2(\i_2_reg_347[10]_i_5_n_5 ),
        .I3(ap_phi_mux_i_phi_fu_214_p4[7]),
        .I4(ap_phi_mux_i_phi_fu_214_p4__0[8]),
        .I5(\i_2_reg_347[10]_i_8_n_5 ),
        .O(i_2_fu_274_p2[10]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_3 
       (.I0(\i_reg_210_reg_n_5_[9] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[9]),
        .O(\i_2_reg_347[10]_i_3_n_5 ));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_4 
       (.I0(\i_reg_210_reg_n_5_[6] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[6]),
        .O(\i_2_reg_347[10]_i_4_n_5 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \i_2_reg_347[10]_i_5 
       (.I0(ap_phi_mux_i_phi_fu_214_p4[5]),
        .I1(ap_phi_mux_i_phi_fu_214_p4__0[2]),
        .I2(ap_phi_mux_i_phi_fu_214_p4[1]),
        .I3(\i_2_reg_347[2]_i_2_n_5 ),
        .I4(ap_phi_mux_i_phi_fu_214_p4[3]),
        .I5(ap_phi_mux_i_phi_fu_214_p4__0[4]),
        .O(\i_2_reg_347[10]_i_5_n_5 ));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_6 
       (.I0(\i_reg_210_reg_n_5_[7] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[7]),
        .O(ap_phi_mux_i_phi_fu_214_p4[7]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_7 
       (.I0(\i_reg_210_reg_n_5_[8] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[8]),
        .O(ap_phi_mux_i_phi_fu_214_p4__0[8]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_8 
       (.I0(\i_reg_210_reg_n_5_[10] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[10]),
        .O(\i_2_reg_347[10]_i_8_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[10]_i_9 
       (.I0(\i_reg_210_reg_n_5_[5] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[5]),
        .O(ap_phi_mux_i_phi_fu_214_p4[5]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h335ACC5A)) 
    \i_2_reg_347[1]_i_1 
       (.I0(i_2_reg_347_reg__0[1]),
        .I1(\i_reg_210_reg_n_5_[1] ),
        .I2(i_2_reg_347_reg__0[0]),
        .I3(\i_2_reg_347[8]_i_2_n_5 ),
        .I4(\i_reg_210_reg_n_5_[0] ),
        .O(i_2_fu_274_p2[1]));
  LUT6 #(
    .INIT(64'h77775FA088885FA0)) 
    \i_2_reg_347[2]_i_1 
       (.I0(\i_2_reg_347[2]_i_2_n_5 ),
        .I1(\i_reg_210_reg_n_5_[1] ),
        .I2(i_2_reg_347_reg__0[1]),
        .I3(i_2_reg_347_reg__0[2]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[2] ),
        .O(i_2_fu_274_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_2_reg_347[2]_i_2 
       (.I0(\i_reg_210_reg_n_5_[0] ),
        .I1(ap_enable_reg_pp0_iter1_reg_n_5),
        .I2(\exitcond1_reg_343_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(i_2_reg_347_reg__0[0]),
        .O(\i_2_reg_347[2]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hF5F5F30C0A0AF30C)) 
    \i_2_reg_347[3]_i_1 
       (.I0(\i_reg_210_reg_n_5_[2] ),
        .I1(i_2_reg_347_reg__0[2]),
        .I2(\i_2_reg_347[3]_i_2_n_5 ),
        .I3(i_2_reg_347_reg__0[3]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[3] ),
        .O(i_2_fu_274_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h335FFF5F)) 
    \i_2_reg_347[3]_i_2 
       (.I0(i_2_reg_347_reg__0[1]),
        .I1(\i_reg_210_reg_n_5_[1] ),
        .I2(i_2_reg_347_reg__0[0]),
        .I3(\i_2_reg_347[8]_i_2_n_5 ),
        .I4(\i_reg_210_reg_n_5_[0] ),
        .O(\i_2_reg_347[3]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hF5F5F30C0A0AF30C)) 
    \i_2_reg_347[4]_i_1 
       (.I0(\i_reg_210_reg_n_5_[3] ),
        .I1(i_2_reg_347_reg__0[3]),
        .I2(\i_2_reg_347[4]_i_2_n_5 ),
        .I3(i_2_reg_347_reg__0[4]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[4] ),
        .O(i_2_fu_274_p2[4]));
  LUT6 #(
    .INIT(64'h77775FFFFFFF5FFF)) 
    \i_2_reg_347[4]_i_2 
       (.I0(\i_2_reg_347[2]_i_2_n_5 ),
        .I1(\i_reg_210_reg_n_5_[1] ),
        .I2(i_2_reg_347_reg__0[1]),
        .I3(i_2_reg_347_reg__0[2]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[2] ),
        .O(\i_2_reg_347[4]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hBBBBAF504444AF50)) 
    \i_2_reg_347[5]_i_1 
       (.I0(\i_2_reg_347[5]_i_2_n_5 ),
        .I1(\i_reg_210_reg_n_5_[4] ),
        .I2(i_2_reg_347_reg__0[4]),
        .I3(i_2_reg_347_reg__0[5]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[5] ),
        .O(i_2_fu_274_p2[5]));
  LUT6 #(
    .INIT(64'h57F7FFFFFFFFFFFF)) 
    \i_2_reg_347[5]_i_2 
       (.I0(ap_phi_mux_i_phi_fu_214_p4__0[2]),
        .I1(i_2_reg_347_reg__0[1]),
        .I2(\i_2_reg_347[8]_i_2_n_5 ),
        .I3(\i_reg_210_reg_n_5_[1] ),
        .I4(\i_2_reg_347[2]_i_2_n_5 ),
        .I5(ap_phi_mux_i_phi_fu_214_p4[3]),
        .O(\i_2_reg_347[5]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'h77775FA088885FA0)) 
    \i_2_reg_347[6]_i_1 
       (.I0(\i_2_reg_347[6]_i_2_n_5 ),
        .I1(\i_reg_210_reg_n_5_[5] ),
        .I2(i_2_reg_347_reg__0[5]),
        .I3(i_2_reg_347_reg__0[6]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[6] ),
        .O(i_2_fu_274_p2[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_2_reg_347[6]_i_2 
       (.I0(ap_phi_mux_i_phi_fu_214_p4__0[4]),
        .I1(ap_phi_mux_i_phi_fu_214_p4[3]),
        .I2(\i_2_reg_347[2]_i_2_n_5 ),
        .I3(ap_phi_mux_i_phi_fu_214_p4[1]),
        .I4(ap_phi_mux_i_phi_fu_214_p4__0[2]),
        .O(\i_2_reg_347[6]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hBBBBAF504444AF50)) 
    \i_2_reg_347[7]_i_1 
       (.I0(\i_2_reg_347[10]_i_5_n_5 ),
        .I1(\i_reg_210_reg_n_5_[6] ),
        .I2(i_2_reg_347_reg__0[6]),
        .I3(i_2_reg_347_reg__0[7]),
        .I4(\i_2_reg_347[8]_i_2_n_5 ),
        .I5(\i_reg_210_reg_n_5_[7] ),
        .O(i_2_fu_274_p2[7]));
  LUT6 #(
    .INIT(64'hDFDDDFFF20222000)) 
    \i_2_reg_347[8]_i_1 
       (.I0(\i_2_reg_347[10]_i_4_n_5 ),
        .I1(\i_2_reg_347[10]_i_5_n_5 ),
        .I2(\i_reg_210_reg_n_5_[7] ),
        .I3(\i_2_reg_347[8]_i_2_n_5 ),
        .I4(i_2_reg_347_reg__0[7]),
        .I5(ap_phi_mux_i_phi_fu_214_p4__0[8]),
        .O(i_2_fu_274_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \i_2_reg_347[8]_i_2 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_5),
        .I1(\exitcond1_reg_343_reg_n_5_[0] ),
        .I2(ap_CS_fsm_pp0_stage0),
        .O(\i_2_reg_347[8]_i_2_n_5 ));
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \i_2_reg_347[9]_i_1 
       (.I0(ap_phi_mux_i_phi_fu_214_p4__0[8]),
        .I1(ap_phi_mux_i_phi_fu_214_p4[7]),
        .I2(\i_2_reg_347[10]_i_5_n_5 ),
        .I3(\i_2_reg_347[10]_i_4_n_5 ),
        .I4(\i_2_reg_347[10]_i_3_n_5 ),
        .O(i_2_fu_274_p2[9]));
  FDRE \i_2_reg_347_reg[0] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[0]),
        .Q(i_2_reg_347_reg__0[0]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[10] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[10]),
        .Q(i_2_reg_347_reg__0[10]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[1] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[1]),
        .Q(i_2_reg_347_reg__0[1]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[2] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[2]),
        .Q(i_2_reg_347_reg__0[2]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[3] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[3]),
        .Q(i_2_reg_347_reg__0[3]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[4] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[4]),
        .Q(i_2_reg_347_reg__0[4]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[5] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[5]),
        .Q(i_2_reg_347_reg__0[5]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[6] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[6]),
        .Q(i_2_reg_347_reg__0[6]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[7] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[7]),
        .Q(i_2_reg_347_reg__0[7]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[8] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[8]),
        .Q(i_2_reg_347_reg__0[8]),
        .R(1'b0));
  FDRE \i_2_reg_347_reg[9] 
       (.C(ap_clk),
        .CE(i_2_reg_3470),
        .D(i_2_fu_274_p2[9]),
        .Q(i_2_reg_347_reg__0[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'h10DF)) 
    \i_3_reg_356[0]_i_1 
       (.I0(i_3_reg_356_reg__0[0]),
        .I1(\exitcond_reg_352_reg_n_5_[0] ),
        .I2(ap_enable_reg_pp1_iter1_reg_n_5),
        .I3(\i_1_reg_222_reg_n_5_[0] ),
        .O(i_3_fu_296_p2[0]));
  LUT5 #(
    .INIT(32'hAAA20000)) 
    \i_3_reg_356[10]_i_1 
       (.I0(ap_enable_reg_pp1_iter0),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .I4(ap_CS_fsm_pp1_stage0),
        .O(i_3_reg_3560));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_10 
       (.I0(\i_1_reg_222_reg_n_5_[1] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[1]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4[1]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_11 
       (.I0(\i_1_reg_222_reg_n_5_[3] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[3]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4[3]));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \i_3_reg_356[10]_i_2 
       (.I0(\i_3_reg_356[10]_i_3_n_5 ),
        .I1(\i_3_reg_356[10]_i_4_n_5 ),
        .I2(\i_3_reg_356[10]_i_5_n_5 ),
        .I3(ap_phi_mux_i_1_phi_fu_226_p4[7]),
        .I4(ap_phi_mux_i_1_phi_fu_226_p4__0[8]),
        .I5(\i_3_reg_356[10]_i_8_n_5 ),
        .O(i_3_fu_296_p2[10]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_3 
       (.I0(\i_1_reg_222_reg_n_5_[9] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[9]),
        .O(\i_3_reg_356[10]_i_3_n_5 ));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_4 
       (.I0(\i_1_reg_222_reg_n_5_[6] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[6]),
        .O(\i_3_reg_356[10]_i_4_n_5 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \i_3_reg_356[10]_i_5 
       (.I0(ap_phi_mux_i_1_phi_fu_226_p4[5]),
        .I1(ap_phi_mux_i_1_phi_fu_226_p4__0[2]),
        .I2(ap_phi_mux_i_1_phi_fu_226_p4[1]),
        .I3(\i_3_reg_356[2]_i_2_n_5 ),
        .I4(ap_phi_mux_i_1_phi_fu_226_p4[3]),
        .I5(ap_phi_mux_i_1_phi_fu_226_p4__0[4]),
        .O(\i_3_reg_356[10]_i_5_n_5 ));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_6 
       (.I0(\i_1_reg_222_reg_n_5_[7] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[7]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4[7]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_7 
       (.I0(\i_1_reg_222_reg_n_5_[8] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[8]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4__0[8]));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_8 
       (.I0(\i_1_reg_222_reg_n_5_[10] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[10]),
        .O(\i_3_reg_356[10]_i_8_n_5 ));
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[10]_i_9 
       (.I0(\i_1_reg_222_reg_n_5_[5] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[5]),
        .O(ap_phi_mux_i_1_phi_fu_226_p4[5]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'h335ACC5A)) 
    \i_3_reg_356[1]_i_1 
       (.I0(i_3_reg_356_reg__0[1]),
        .I1(\i_1_reg_222_reg_n_5_[1] ),
        .I2(i_3_reg_356_reg__0[0]),
        .I3(\i_3_reg_356[8]_i_2_n_5 ),
        .I4(\i_1_reg_222_reg_n_5_[0] ),
        .O(i_3_fu_296_p2[1]));
  LUT6 #(
    .INIT(64'h77775FA088885FA0)) 
    \i_3_reg_356[2]_i_1 
       (.I0(\i_3_reg_356[2]_i_2_n_5 ),
        .I1(\i_1_reg_222_reg_n_5_[1] ),
        .I2(i_3_reg_356_reg__0[1]),
        .I3(i_3_reg_356_reg__0[2]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[2] ),
        .O(i_3_fu_296_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hAEAAA2AA)) 
    \i_3_reg_356[2]_i_2 
       (.I0(\i_1_reg_222_reg_n_5_[0] ),
        .I1(ap_enable_reg_pp1_iter1_reg_n_5),
        .I2(\exitcond_reg_352_reg_n_5_[0] ),
        .I3(ap_CS_fsm_pp1_stage0),
        .I4(i_3_reg_356_reg__0[0]),
        .O(\i_3_reg_356[2]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hF5F5F30C0A0AF30C)) 
    \i_3_reg_356[3]_i_1 
       (.I0(\i_1_reg_222_reg_n_5_[2] ),
        .I1(i_3_reg_356_reg__0[2]),
        .I2(\i_3_reg_356[3]_i_2_n_5 ),
        .I3(i_3_reg_356_reg__0[3]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[3] ),
        .O(i_3_fu_296_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'h335FFF5F)) 
    \i_3_reg_356[3]_i_2 
       (.I0(i_3_reg_356_reg__0[1]),
        .I1(\i_1_reg_222_reg_n_5_[1] ),
        .I2(i_3_reg_356_reg__0[0]),
        .I3(\i_3_reg_356[8]_i_2_n_5 ),
        .I4(\i_1_reg_222_reg_n_5_[0] ),
        .O(\i_3_reg_356[3]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hF5F5F30C0A0AF30C)) 
    \i_3_reg_356[4]_i_1 
       (.I0(\i_1_reg_222_reg_n_5_[3] ),
        .I1(i_3_reg_356_reg__0[3]),
        .I2(\i_3_reg_356[4]_i_2_n_5 ),
        .I3(i_3_reg_356_reg__0[4]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[4] ),
        .O(i_3_fu_296_p2[4]));
  LUT6 #(
    .INIT(64'h77775FFFFFFF5FFF)) 
    \i_3_reg_356[4]_i_2 
       (.I0(\i_3_reg_356[2]_i_2_n_5 ),
        .I1(\i_1_reg_222_reg_n_5_[1] ),
        .I2(i_3_reg_356_reg__0[1]),
        .I3(i_3_reg_356_reg__0[2]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[2] ),
        .O(\i_3_reg_356[4]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hBBBBAF504444AF50)) 
    \i_3_reg_356[5]_i_1 
       (.I0(\i_3_reg_356[5]_i_2_n_5 ),
        .I1(\i_1_reg_222_reg_n_5_[4] ),
        .I2(i_3_reg_356_reg__0[4]),
        .I3(i_3_reg_356_reg__0[5]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[5] ),
        .O(i_3_fu_296_p2[5]));
  LUT6 #(
    .INIT(64'h57F7FFFFFFFFFFFF)) 
    \i_3_reg_356[5]_i_2 
       (.I0(ap_phi_mux_i_1_phi_fu_226_p4__0[2]),
        .I1(i_3_reg_356_reg__0[1]),
        .I2(\i_3_reg_356[8]_i_2_n_5 ),
        .I3(\i_1_reg_222_reg_n_5_[1] ),
        .I4(\i_3_reg_356[2]_i_2_n_5 ),
        .I5(ap_phi_mux_i_1_phi_fu_226_p4[3]),
        .O(\i_3_reg_356[5]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'h77775FA088885FA0)) 
    \i_3_reg_356[6]_i_1 
       (.I0(\i_3_reg_356[6]_i_2_n_5 ),
        .I1(\i_1_reg_222_reg_n_5_[5] ),
        .I2(i_3_reg_356_reg__0[5]),
        .I3(i_3_reg_356_reg__0[6]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[6] ),
        .O(i_3_fu_296_p2[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_3_reg_356[6]_i_2 
       (.I0(ap_phi_mux_i_1_phi_fu_226_p4__0[4]),
        .I1(ap_phi_mux_i_1_phi_fu_226_p4[3]),
        .I2(\i_3_reg_356[2]_i_2_n_5 ),
        .I3(ap_phi_mux_i_1_phi_fu_226_p4[1]),
        .I4(ap_phi_mux_i_1_phi_fu_226_p4__0[2]),
        .O(\i_3_reg_356[6]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hBBBBAF504444AF50)) 
    \i_3_reg_356[7]_i_1 
       (.I0(\i_3_reg_356[10]_i_5_n_5 ),
        .I1(\i_1_reg_222_reg_n_5_[6] ),
        .I2(i_3_reg_356_reg__0[6]),
        .I3(i_3_reg_356_reg__0[7]),
        .I4(\i_3_reg_356[8]_i_2_n_5 ),
        .I5(\i_1_reg_222_reg_n_5_[7] ),
        .O(i_3_fu_296_p2[7]));
  LUT6 #(
    .INIT(64'hDFDDDFFF20222000)) 
    \i_3_reg_356[8]_i_1 
       (.I0(\i_3_reg_356[10]_i_4_n_5 ),
        .I1(\i_3_reg_356[10]_i_5_n_5 ),
        .I2(\i_1_reg_222_reg_n_5_[7] ),
        .I3(\i_3_reg_356[8]_i_2_n_5 ),
        .I4(i_3_reg_356_reg__0[7]),
        .I5(ap_phi_mux_i_1_phi_fu_226_p4__0[8]),
        .O(i_3_fu_296_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \i_3_reg_356[8]_i_2 
       (.I0(ap_enable_reg_pp1_iter1_reg_n_5),
        .I1(\exitcond_reg_352_reg_n_5_[0] ),
        .I2(ap_CS_fsm_pp1_stage0),
        .O(\i_3_reg_356[8]_i_2_n_5 ));
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \i_3_reg_356[9]_i_1 
       (.I0(ap_phi_mux_i_1_phi_fu_226_p4__0[8]),
        .I1(ap_phi_mux_i_1_phi_fu_226_p4[7]),
        .I2(\i_3_reg_356[10]_i_5_n_5 ),
        .I3(\i_3_reg_356[10]_i_4_n_5 ),
        .I4(\i_3_reg_356[10]_i_3_n_5 ),
        .O(i_3_fu_296_p2[9]));
  FDRE \i_3_reg_356_reg[0] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[0]),
        .Q(i_3_reg_356_reg__0[0]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[10] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[10]),
        .Q(i_3_reg_356_reg__0[10]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[1] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[1]),
        .Q(i_3_reg_356_reg__0[1]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[2] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[2]),
        .Q(i_3_reg_356_reg__0[2]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[3] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[3]),
        .Q(i_3_reg_356_reg__0[3]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[4] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[4]),
        .Q(i_3_reg_356_reg__0[4]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[5] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[5]),
        .Q(i_3_reg_356_reg__0[5]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[6] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[6]),
        .Q(i_3_reg_356_reg__0[6]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[7] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[7]),
        .Q(i_3_reg_356_reg__0[7]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[8] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[8]),
        .Q(i_3_reg_356_reg__0[8]),
        .R(1'b0));
  FDRE \i_3_reg_356_reg[9] 
       (.C(ap_clk),
        .CE(i_3_reg_3560),
        .D(i_3_fu_296_p2[9]),
        .Q(i_3_reg_356_reg__0[9]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \i_4_reg_365[0]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[0] ),
        .O(i_4_fu_320_p2[0]));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \i_4_reg_365[10]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[9] ),
        .I1(\a_b_2_reg_234_reg_n_5_[6] ),
        .I2(\i_4_reg_365[10]_i_2_n_5 ),
        .I3(\a_b_2_reg_234_reg_n_5_[7] ),
        .I4(\a_b_2_reg_234_reg_n_5_[8] ),
        .I5(\a_b_2_reg_234_reg_n_5_[10] ),
        .O(i_4_fu_320_p2[10]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \i_4_reg_365[10]_i_2 
       (.I0(\a_b_2_reg_234_reg_n_5_[5] ),
        .I1(\a_b_2_reg_234_reg_n_5_[2] ),
        .I2(\a_b_2_reg_234_reg_n_5_[1] ),
        .I3(\a_b_2_reg_234_reg_n_5_[0] ),
        .I4(\a_b_2_reg_234_reg_n_5_[3] ),
        .I5(\a_b_2_reg_234_reg_n_5_[4] ),
        .O(\i_4_reg_365[10]_i_2_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_4_reg_365[1]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[0] ),
        .I1(\a_b_2_reg_234_reg_n_5_[1] ),
        .O(i_4_fu_320_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_4_reg_365[2]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[0] ),
        .I1(\a_b_2_reg_234_reg_n_5_[1] ),
        .I2(\a_b_2_reg_234_reg_n_5_[2] ),
        .O(i_4_fu_320_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_4_reg_365[3]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[2] ),
        .I1(\a_b_2_reg_234_reg_n_5_[1] ),
        .I2(\a_b_2_reg_234_reg_n_5_[0] ),
        .I3(\a_b_2_reg_234_reg_n_5_[3] ),
        .O(i_4_fu_320_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_4_reg_365[4]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[3] ),
        .I1(\a_b_2_reg_234_reg_n_5_[0] ),
        .I2(\a_b_2_reg_234_reg_n_5_[1] ),
        .I3(\a_b_2_reg_234_reg_n_5_[2] ),
        .I4(\a_b_2_reg_234_reg_n_5_[4] ),
        .O(i_4_fu_320_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_4_reg_365[5]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[2] ),
        .I1(\a_b_2_reg_234_reg_n_5_[1] ),
        .I2(\a_b_2_reg_234_reg_n_5_[0] ),
        .I3(\a_b_2_reg_234_reg_n_5_[3] ),
        .I4(\a_b_2_reg_234_reg_n_5_[4] ),
        .I5(\a_b_2_reg_234_reg_n_5_[5] ),
        .O(i_4_fu_320_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \i_4_reg_365[6]_i_1 
       (.I0(\i_4_reg_365[10]_i_2_n_5 ),
        .I1(\a_b_2_reg_234_reg_n_5_[6] ),
        .O(i_4_fu_320_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \i_4_reg_365[7]_i_1 
       (.I0(\i_4_reg_365[10]_i_2_n_5 ),
        .I1(\a_b_2_reg_234_reg_n_5_[6] ),
        .I2(\a_b_2_reg_234_reg_n_5_[7] ),
        .O(i_4_fu_320_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \i_4_reg_365[8]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[6] ),
        .I1(\i_4_reg_365[10]_i_2_n_5 ),
        .I2(\a_b_2_reg_234_reg_n_5_[7] ),
        .I3(\a_b_2_reg_234_reg_n_5_[8] ),
        .O(i_4_fu_320_p2[8]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \i_4_reg_365[9]_i_1 
       (.I0(\a_b_2_reg_234_reg_n_5_[8] ),
        .I1(\a_b_2_reg_234_reg_n_5_[7] ),
        .I2(\i_4_reg_365[10]_i_2_n_5 ),
        .I3(\a_b_2_reg_234_reg_n_5_[6] ),
        .I4(\a_b_2_reg_234_reg_n_5_[9] ),
        .O(i_4_fu_320_p2[9]));
  FDRE \i_4_reg_365_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[0]),
        .Q(i_4_reg_365[0]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[10]),
        .Q(i_4_reg_365[10]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[1]),
        .Q(i_4_reg_365[1]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[2]),
        .Q(i_4_reg_365[2]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[3]),
        .Q(i_4_reg_365[3]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[4]),
        .Q(i_4_reg_365[4]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[5]),
        .Q(i_4_reg_365[5]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[6]),
        .Q(i_4_reg_365[6]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[7]),
        .Q(i_4_reg_365[7]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[8]),
        .Q(i_4_reg_365[8]),
        .R(1'b0));
  FDRE \i_4_reg_365_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(i_4_fu_320_p2[9]),
        .Q(i_4_reg_365[9]),
        .R(1'b0));
  FDRE \i_reg_210_reg[0] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[0]),
        .Q(\i_reg_210_reg_n_5_[0] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[10] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[10]),
        .Q(\i_reg_210_reg_n_5_[10] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[1] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[1]),
        .Q(\i_reg_210_reg_n_5_[1] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[2] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[2]),
        .Q(\i_reg_210_reg_n_5_[2] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[3] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[3]),
        .Q(\i_reg_210_reg_n_5_[3] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[4] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[4]),
        .Q(\i_reg_210_reg_n_5_[4] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[5] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[5]),
        .Q(\i_reg_210_reg_n_5_[5] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[6] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[6]),
        .Q(\i_reg_210_reg_n_5_[6] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[7] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[7]),
        .Q(\i_reg_210_reg_n_5_[7] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[8] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[8]),
        .Q(\i_reg_210_reg_n_5_[8] ),
        .R(i_reg_210));
  FDRE \i_reg_210_reg[9] 
       (.C(ap_clk),
        .CE(p_67_in),
        .D(i_2_reg_347_reg__0[9]),
        .Q(\i_reg_210_reg_n_5_[9] ),
        .R(i_reg_210));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb inputValues_0_U
       (.CO(tmp_8_fu_332_p2),
        .D(inputValues_0_q0),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (INPUT_STREAM_V_data_V_0_payload_A),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (INPUT_STREAM_V_data_V_0_payload_B),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .Q({ap_CS_fsm_state11,ap_CS_fsm_state8,ap_CS_fsm_pp0_stage0}),
        .\a_b_2_reg_234_reg[10] ({\a_b_2_reg_234_reg_n_5_[10] ,\a_b_2_reg_234_reg_n_5_[9] ,\a_b_2_reg_234_reg_n_5_[8] ,\a_b_2_reg_234_reg_n_5_[7] ,\a_b_2_reg_234_reg_n_5_[6] ,\a_b_2_reg_234_reg_n_5_[5] ,\a_b_2_reg_234_reg_n_5_[4] ,\a_b_2_reg_234_reg_n_5_[3] ,\a_b_2_reg_234_reg_n_5_[2] ,\a_b_2_reg_234_reg_n_5_[1] ,\a_b_2_reg_234_reg_n_5_[0] }),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter1_reg(ap_enable_reg_pp0_iter1_reg_n_5),
        .\exitcond1_reg_343_reg[0] (\exitcond1_reg_343_reg_n_5_[0] ),
        .\i_reg_210_reg[5] ({\i_reg_210_reg_n_5_[5] ,\i_reg_210_reg_n_5_[4] ,\i_reg_210_reg_n_5_[3] ,\i_reg_210_reg_n_5_[2] ,\i_reg_210_reg_n_5_[1] ,\i_reg_210_reg_n_5_[0] }),
        .\int_agg_result_b_reg[31] (ap_phi_mux_storemerge_phi_fu_260_p4),
        .lastValues_0_q0(lastValues_0_q0),
        .p_67_in(p_67_in),
        .tmp_5_reg_361(tmp_5_reg_361));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_0 lastValues_0_U
       (.\LAST_STREAM_V_data_V_0_payload_A_reg[31] (LAST_STREAM_V_data_V_0_payload_A),
        .\LAST_STREAM_V_data_V_0_payload_B_reg[31] (LAST_STREAM_V_data_V_0_payload_B),
        .LAST_STREAM_V_data_V_0_sel(LAST_STREAM_V_data_V_0_sel),
        .\LAST_STREAM_V_data_V_0_state_reg[0] (\LAST_STREAM_V_data_V_0_state_reg_n_5_[0] ),
        .Q({ap_CS_fsm_state11,ap_CS_fsm_state8,ap_CS_fsm_pp1_stage0}),
        .\a_a_1_reg_380_reg[31] (a_a_1_reg_380),
        .\a_b_2_reg_234_reg[5] ({\a_b_2_reg_234_reg_n_5_[5] ,\a_b_2_reg_234_reg_n_5_[4] ,\a_b_2_reg_234_reg_n_5_[3] ,\a_b_2_reg_234_reg_n_5_[2] ,\a_b_2_reg_234_reg_n_5_[1] ,\a_b_2_reg_234_reg_n_5_[0] }),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter1_reg(ap_enable_reg_pp1_iter1_reg_n_5),
        .\exitcond_reg_352_reg[0] (\exitcond_reg_352_reg_n_5_[0] ),
        .\i_1_reg_222_reg[5] ({\i_1_reg_222_reg_n_5_[5] ,\i_1_reg_222_reg_n_5_[4] ,\i_1_reg_222_reg_n_5_[3] ,\i_1_reg_222_reg_n_5_[2] ,\i_1_reg_222_reg_n_5_[1] ,\i_1_reg_222_reg_n_5_[0] }),
        .\int_agg_result_a_reg[31] (ap_phi_mux_storemerge1_phi_fu_249_p4),
        .lastValues_0_q0(lastValues_0_q0),
        .p_52_in(p_52_in),
        .tmp_5_reg_361(tmp_5_reg_361));
  FDRE \tmp_5_reg_361_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state8),
        .D(\a_b_2_reg_234_reg_n_5_[10] ),
        .Q(tmp_5_reg_361),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (ARESET,
    ap_enable_reg_pp0_iter1_reg,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    ap_enable_reg_pp0_iter0_reg,
    D,
    SR,
    interrupt,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    ap_block_pp0_stage0_subdone,
    \i_reg_210_reg[3] ,
    ap_phi_mux_i_phi_fu_214_p4__0,
    \i_reg_210_reg[0] ,
    ap_rst_n,
    ap_enable_reg_pp0_iter1_reg_0,
    ap_enable_reg_pp0_iter0,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_ARADDR,
    \i_reg_210_reg[8] ,
    \i_2_reg_347_reg[10] ,
    \i_reg_210_reg[2] ,
    \ap_CS_fsm_reg[8] ,
    \exitcond1_reg_343_reg[0] ,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR,
    ram_reg,
    \tmp_5_reg_361_reg[0] );
  output ARESET;
  output ap_enable_reg_pp0_iter1_reg;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output ap_enable_reg_pp0_iter0_reg;
  output [1:0]D;
  output [0:0]SR;
  output interrupt;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [2:0]Q;
  input ap_block_pp0_stage0_subdone;
  input \i_reg_210_reg[3] ;
  input [1:0]ap_phi_mux_i_phi_fu_214_p4__0;
  input \i_reg_210_reg[0] ;
  input ap_rst_n;
  input ap_enable_reg_pp0_iter1_reg_0;
  input ap_enable_reg_pp0_iter0;
  input s_axi_CONTROL_BUS_RREADY;
  input s_axi_CONTROL_BUS_ARVALID;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  input \i_reg_210_reg[8] ;
  input \i_2_reg_347_reg[10] ;
  input \i_reg_210_reg[2] ;
  input \ap_CS_fsm_reg[8] ;
  input \exitcond1_reg_343_reg[0] ;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input [31:0]ram_reg;
  input [31:0]\tmp_5_reg_361_reg[0] ;

  wire \/FSM_onehot_wstate[1]_i_1_n_5 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_5 ;
  wire ARESET;
  wire [1:0]D;
  wire \FSM_onehot_wstate[3]_i_1_n_5 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_5_[0] ;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [0:0]SR;
  wire \ap_CS_fsm[1]_i_4_n_5 ;
  wire \ap_CS_fsm_reg[8] ;
  wire ap_block_pp0_stage0_subdone;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_i_2_n_5;
  wire ap_enable_reg_pp0_iter0_reg;
  wire ap_enable_reg_pp0_iter1_i_2_n_5;
  wire ap_enable_reg_pp0_iter1_reg;
  wire ap_enable_reg_pp0_iter1_reg_0;
  wire ap_idle;
  wire [1:0]ap_phi_mux_i_phi_fu_214_p4__0;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire \exitcond1_reg_343_reg[0] ;
  wire \i_2_reg_347_reg[10] ;
  wire \i_reg_210_reg[0] ;
  wire \i_reg_210_reg[2] ;
  wire \i_reg_210_reg[3] ;
  wire \i_reg_210_reg[8] ;
  wire [31:0]int_agg_result_a;
  wire int_agg_result_a_ap_vld;
  wire int_agg_result_a_ap_vld_i_1_n_5;
  wire int_agg_result_a_ap_vld_i_2_n_5;
  wire [31:0]int_agg_result_b;
  wire int_agg_result_b_ap_vld;
  wire int_agg_result_b_ap_vld_i_1_n_5;
  wire int_ap_done;
  wire int_ap_done_i_1_n_5;
  wire int_ap_done_i_2_n_5;
  wire int_ap_idle;
  wire int_ap_ready;
  wire int_ap_start_i_1_n_5;
  wire int_ap_start_i_2_n_5;
  wire int_ap_start_i_3_n_5;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_5;
  wire int_gie_i_1_n_5;
  wire int_gie_reg_n_5;
  wire \int_ier[0]_i_1_n_5 ;
  wire \int_ier[1]_i_1_n_5 ;
  wire \int_ier_reg_n_5_[0] ;
  wire int_isr;
  wire int_isr7_out;
  wire \int_isr[0]_i_1_n_5 ;
  wire \int_isr[1]_i_1_n_5 ;
  wire \int_isr_reg_n_5_[0] ;
  wire \int_searched[31]_i_3_n_5 ;
  wire \int_searched_reg_n_5_[0] ;
  wire \int_searched_reg_n_5_[10] ;
  wire \int_searched_reg_n_5_[11] ;
  wire \int_searched_reg_n_5_[12] ;
  wire \int_searched_reg_n_5_[13] ;
  wire \int_searched_reg_n_5_[14] ;
  wire \int_searched_reg_n_5_[15] ;
  wire \int_searched_reg_n_5_[16] ;
  wire \int_searched_reg_n_5_[17] ;
  wire \int_searched_reg_n_5_[18] ;
  wire \int_searched_reg_n_5_[19] ;
  wire \int_searched_reg_n_5_[1] ;
  wire \int_searched_reg_n_5_[20] ;
  wire \int_searched_reg_n_5_[21] ;
  wire \int_searched_reg_n_5_[22] ;
  wire \int_searched_reg_n_5_[23] ;
  wire \int_searched_reg_n_5_[24] ;
  wire \int_searched_reg_n_5_[25] ;
  wire \int_searched_reg_n_5_[26] ;
  wire \int_searched_reg_n_5_[27] ;
  wire \int_searched_reg_n_5_[28] ;
  wire \int_searched_reg_n_5_[29] ;
  wire \int_searched_reg_n_5_[2] ;
  wire \int_searched_reg_n_5_[30] ;
  wire \int_searched_reg_n_5_[31] ;
  wire \int_searched_reg_n_5_[3] ;
  wire \int_searched_reg_n_5_[4] ;
  wire \int_searched_reg_n_5_[5] ;
  wire \int_searched_reg_n_5_[6] ;
  wire \int_searched_reg_n_5_[7] ;
  wire \int_searched_reg_n_5_[8] ;
  wire \int_searched_reg_n_5_[9] ;
  wire interrupt;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in13_out;
  wire p_1_in;
  wire [31:0]ram_reg;
  wire [31:0]rdata_data;
  wire \rdata_data[0]_i_2_n_5 ;
  wire \rdata_data[0]_i_3_n_5 ;
  wire \rdata_data[0]_i_4_n_5 ;
  wire \rdata_data[0]_i_5_n_5 ;
  wire \rdata_data[0]_i_6_n_5 ;
  wire \rdata_data[0]_i_7_n_5 ;
  wire \rdata_data[0]_i_8_n_5 ;
  wire \rdata_data[0]_i_9_n_5 ;
  wire \rdata_data[1]_i_2_n_5 ;
  wire \rdata_data[1]_i_3_n_5 ;
  wire \rdata_data[1]_i_4_n_5 ;
  wire \rdata_data[1]_i_5_n_5 ;
  wire \rdata_data[1]_i_6_n_5 ;
  wire \rdata_data[2]_i_2_n_5 ;
  wire \rdata_data[31]_i_3_n_5 ;
  wire \rdata_data[31]_i_4_n_5 ;
  wire \rdata_data[31]_i_5_n_5 ;
  wire \rdata_data[3]_i_2_n_5 ;
  wire \rdata_data[7]_i_2_n_5 ;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_5 ;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]\tmp_5_reg_361_reg[0] ;
  wire waddr;
  wire \waddr_reg_n_5_[0] ;
  wire \waddr_reg_n_5_[1] ;
  wire \waddr_reg_n_5_[2] ;
  wire \waddr_reg_n_5_[3] ;
  wire \waddr_reg_n_5_[4] ;
  wire \waddr_reg_n_5_[5] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_5 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_5 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_5 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_5_[0] ),
        .S(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_5 ),
        .Q(out[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_5 ),
        .Q(out[1]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_5 ),
        .Q(out[2]),
        .R(ARESET));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hBFFF0000FFFFFFFF)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_block_pp0_stage0_subdone),
        .I1(\i_reg_210_reg[2] ),
        .I2(\i_reg_210_reg[0] ),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(\ap_CS_fsm_reg[8] ),
        .I5(\ap_CS_fsm[1]_i_4_n_5 ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \ap_CS_fsm[1]_i_4 
       (.I0(ap_start),
        .I1(Q[0]),
        .O(\ap_CS_fsm[1]_i_4_n_5 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA2AAAAAAA)) 
    ap_enable_reg_pp0_iter0_i_1
       (.I0(ap_enable_reg_pp0_iter0_i_2_n_5),
        .I1(Q[1]),
        .I2(\i_reg_210_reg[8] ),
        .I3(\i_2_reg_347_reg[10] ),
        .I4(\i_reg_210_reg[2] ),
        .I5(ap_block_pp0_stage0_subdone),
        .O(ap_enable_reg_pp0_iter0_reg));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hF800)) 
    ap_enable_reg_pp0_iter0_i_2
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(ap_rst_n),
        .O(ap_enable_reg_pp0_iter0_i_2_n_5));
  LUT6 #(
    .INIT(64'hAAAAAA8AAAAAAAAA)) 
    ap_enable_reg_pp0_iter1_i_1
       (.I0(ap_enable_reg_pp0_iter1_i_2_n_5),
        .I1(ap_block_pp0_stage0_subdone),
        .I2(\i_reg_210_reg[3] ),
        .I3(ap_phi_mux_i_phi_fu_214_p4__0[0]),
        .I4(ap_phi_mux_i_phi_fu_214_p4__0[1]),
        .I5(\i_reg_210_reg[0] ),
        .O(ap_enable_reg_pp0_iter1_reg));
  LUT6 #(
    .INIT(64'h00A088A088A088A0)) 
    ap_enable_reg_pp0_iter1_i_2
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp0_iter1_reg_0),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(ap_block_pp0_stage0_subdone),
        .I4(ap_start),
        .I5(Q[0]),
        .O(ap_enable_reg_pp0_iter1_i_2_n_5));
  LUT6 #(
    .INIT(64'h8088888888888888)) 
    \i_reg_210[10]_i_1 
       (.I0(Q[0]),
        .I1(ap_start),
        .I2(\exitcond1_reg_343_reg[0] ),
        .I3(ap_enable_reg_pp0_iter1_reg_0),
        .I4(Q[1]),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .O(SR));
  LUT6 #(
    .INIT(64'hFFFFDFFFFFFF0000)) 
    int_agg_result_a_ap_vld_i_1
       (.I0(\rdata_data[0]_i_5_n_5 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(int_agg_result_a_ap_vld_i_2_n_5),
        .I3(ar_hs),
        .I4(Q[2]),
        .I5(int_agg_result_a_ap_vld),
        .O(int_agg_result_a_ap_vld_i_1_n_5));
  LUT2 #(
    .INIT(4'h2)) 
    int_agg_result_a_ap_vld_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(int_agg_result_a_ap_vld_i_2_n_5));
  FDRE int_agg_result_a_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_a_ap_vld_i_1_n_5),
        .Q(int_agg_result_a_ap_vld),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[0]),
        .Q(int_agg_result_a[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[10] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[10]),
        .Q(int_agg_result_a[10]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[11]),
        .Q(int_agg_result_a[11]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[12] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[12]),
        .Q(int_agg_result_a[12]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[13] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[13]),
        .Q(int_agg_result_a[13]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[14] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[14]),
        .Q(int_agg_result_a[14]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[15] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[15]),
        .Q(int_agg_result_a[15]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[16] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[16]),
        .Q(int_agg_result_a[16]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[17] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[17]),
        .Q(int_agg_result_a[17]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[18] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[18]),
        .Q(int_agg_result_a[18]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[19] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[19]),
        .Q(int_agg_result_a[19]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[1]),
        .Q(int_agg_result_a[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[20] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[20]),
        .Q(int_agg_result_a[20]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[21] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[21]),
        .Q(int_agg_result_a[21]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[22] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[22]),
        .Q(int_agg_result_a[22]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[23] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[23]),
        .Q(int_agg_result_a[23]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[24] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[24]),
        .Q(int_agg_result_a[24]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[25] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[25]),
        .Q(int_agg_result_a[25]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[26] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[26]),
        .Q(int_agg_result_a[26]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[27] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[27]),
        .Q(int_agg_result_a[27]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[28] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[28]),
        .Q(int_agg_result_a[28]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[29] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[29]),
        .Q(int_agg_result_a[29]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[2]),
        .Q(int_agg_result_a[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[30] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[30]),
        .Q(int_agg_result_a[30]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[31] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[31]),
        .Q(int_agg_result_a[31]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[3]),
        .Q(int_agg_result_a[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[4]),
        .Q(int_agg_result_a[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[5]),
        .Q(int_agg_result_a[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[6] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[6]),
        .Q(int_agg_result_a[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[7] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[7]),
        .Q(int_agg_result_a[7]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[8] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[8]),
        .Q(int_agg_result_a[8]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[9] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(ram_reg[9]),
        .Q(int_agg_result_a[9]),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFF7FFF00)) 
    int_agg_result_b_ap_vld_i_1
       (.I0(\rdata_data[1]_i_4_n_5 ),
        .I1(\rdata_data[0]_i_5_n_5 ),
        .I2(ar_hs),
        .I3(Q[2]),
        .I4(int_agg_result_b_ap_vld),
        .O(int_agg_result_b_ap_vld_i_1_n_5));
  FDRE int_agg_result_b_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_b_ap_vld_i_1_n_5),
        .Q(int_agg_result_b_ap_vld),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [0]),
        .Q(int_agg_result_b[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[10] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [10]),
        .Q(int_agg_result_b[10]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [11]),
        .Q(int_agg_result_b[11]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[12] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [12]),
        .Q(int_agg_result_b[12]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[13] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [13]),
        .Q(int_agg_result_b[13]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[14] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [14]),
        .Q(int_agg_result_b[14]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[15] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [15]),
        .Q(int_agg_result_b[15]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[16] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [16]),
        .Q(int_agg_result_b[16]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[17] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [17]),
        .Q(int_agg_result_b[17]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[18] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [18]),
        .Q(int_agg_result_b[18]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[19] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [19]),
        .Q(int_agg_result_b[19]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [1]),
        .Q(int_agg_result_b[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[20] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [20]),
        .Q(int_agg_result_b[20]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[21] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [21]),
        .Q(int_agg_result_b[21]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[22] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [22]),
        .Q(int_agg_result_b[22]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[23] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [23]),
        .Q(int_agg_result_b[23]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[24] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [24]),
        .Q(int_agg_result_b[24]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[25] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [25]),
        .Q(int_agg_result_b[25]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[26] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [26]),
        .Q(int_agg_result_b[26]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[27] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [27]),
        .Q(int_agg_result_b[27]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[28] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [28]),
        .Q(int_agg_result_b[28]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[29] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [29]),
        .Q(int_agg_result_b[29]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [2]),
        .Q(int_agg_result_b[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[30] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [30]),
        .Q(int_agg_result_b[30]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[31] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [31]),
        .Q(int_agg_result_b[31]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [3]),
        .Q(int_agg_result_b[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [4]),
        .Q(int_agg_result_b[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [5]),
        .Q(int_agg_result_b[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[6] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [6]),
        .Q(int_agg_result_b[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[7] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [7]),
        .Q(int_agg_result_b[7]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[8] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [8]),
        .Q(int_agg_result_b[8]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[9] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\tmp_5_reg_361_reg[0] [9]),
        .Q(int_agg_result_b[9]),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFFFFF7FFFF0000)) 
    int_ap_done_i_1
       (.I0(int_ap_done_i_2_n_5),
        .I1(ar_hs),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(Q[2]),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_5));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    int_ap_done_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(int_ap_done_i_2_n_5));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_5),
        .Q(int_ap_done),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(ARESET));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(int_ap_ready),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFBBBBBBBF8888888)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[2]),
        .I2(int_ap_start_i_2_n_5),
        .I3(int_ap_start_i_3_n_5),
        .I4(s_axi_CONTROL_BUS_WDATA[0]),
        .I5(ap_start),
        .O(int_ap_start_i_1_n_5));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h40)) 
    int_ap_start_i_2
       (.I0(\waddr_reg_n_5_[5] ),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched[31]_i_3_n_5 ),
        .O(int_ap_start_i_2_n_5));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h1)) 
    int_ap_start_i_3
       (.I0(\waddr_reg_n_5_[2] ),
        .I1(\waddr_reg_n_5_[3] ),
        .O(int_ap_start_i_3_n_5));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_5),
        .Q(ap_start),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFEFF0200)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(\waddr_reg_n_5_[3] ),
        .I2(\waddr_reg_n_5_[2] ),
        .I3(int_ap_start_i_2_n_5),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_5),
        .Q(int_auto_restart),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_5_[3] ),
        .I2(\waddr_reg_n_5_[2] ),
        .I3(int_ap_start_i_2_n_5),
        .I4(int_gie_reg_n_5),
        .O(int_gie_i_1_n_5));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_5),
        .Q(int_gie_reg_n_5),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_5_[2] ),
        .I2(\waddr_reg_n_5_[3] ),
        .I3(int_ap_start_i_2_n_5),
        .I4(\int_ier_reg_n_5_[0] ),
        .O(\int_ier[0]_i_1_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_5_[2] ),
        .I2(\waddr_reg_n_5_[3] ),
        .I3(int_ap_start_i_2_n_5),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_5 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_5 ),
        .Q(\int_ier_reg_n_5_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_5 ),
        .Q(p_0_in),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFF7FFFFFFF8000)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_ap_start_i_2_n_5),
        .I2(\waddr_reg_n_5_[2] ),
        .I3(\waddr_reg_n_5_[3] ),
        .I4(int_isr7_out),
        .I5(\int_isr_reg_n_5_[0] ),
        .O(\int_isr[0]_i_1_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_isr[0]_i_2 
       (.I0(Q[2]),
        .I1(\int_ier_reg_n_5_[0] ),
        .O(int_isr7_out));
  LUT6 #(
    .INIT(64'hFFFF7FFFFFFF8000)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_ap_start_i_2_n_5),
        .I2(\waddr_reg_n_5_[2] ),
        .I3(\waddr_reg_n_5_[3] ),
        .I4(int_isr),
        .I5(p_1_in),
        .O(\int_isr[1]_i_1_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_isr[1]_i_2 
       (.I0(Q[2]),
        .I1(p_0_in),
        .O(int_isr));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_5 ),
        .Q(\int_isr_reg_n_5_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_5 ),
        .Q(p_1_in),
        .R(ARESET));
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[0] ),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[10] ),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[11] ),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[12] ),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[13] ),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[14] ),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[15] ),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[16] ),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[17] ),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[18] ),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[19] ),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[1] ),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[20] ),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[21] ),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[22] ),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_5_[23] ),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[24] ),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[25] ),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[26] ),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[27] ),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[28] ),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[29] ),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[2] ),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[30] ),
        .O(\or [30]));
  LUT4 #(
    .INIT(16'h0400)) 
    \int_searched[31]_i_1 
       (.I0(\waddr_reg_n_5_[2] ),
        .I1(\waddr_reg_n_5_[5] ),
        .I2(\waddr_reg_n_5_[3] ),
        .I3(\int_searched[31]_i_3_n_5 ),
        .O(p_0_in13_out));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_5_[31] ),
        .O(\or [31]));
  LUT5 #(
    .INIT(32'h00000008)) 
    \int_searched[31]_i_3 
       (.I0(out[1]),
        .I1(s_axi_CONTROL_BUS_WVALID),
        .I2(\waddr_reg_n_5_[0] ),
        .I3(\waddr_reg_n_5_[4] ),
        .I4(\waddr_reg_n_5_[1] ),
        .O(\int_searched[31]_i_3_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[3] ),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[4] ),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[5] ),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[6] ),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_5_[7] ),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[8] ),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_5_[9] ),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [0]),
        .Q(\int_searched_reg_n_5_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [10]),
        .Q(\int_searched_reg_n_5_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [11]),
        .Q(\int_searched_reg_n_5_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [12]),
        .Q(\int_searched_reg_n_5_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [13]),
        .Q(\int_searched_reg_n_5_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [14]),
        .Q(\int_searched_reg_n_5_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [15]),
        .Q(\int_searched_reg_n_5_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [16]),
        .Q(\int_searched_reg_n_5_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [17]),
        .Q(\int_searched_reg_n_5_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [18]),
        .Q(\int_searched_reg_n_5_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [19]),
        .Q(\int_searched_reg_n_5_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [1]),
        .Q(\int_searched_reg_n_5_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [20]),
        .Q(\int_searched_reg_n_5_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [21]),
        .Q(\int_searched_reg_n_5_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [22]),
        .Q(\int_searched_reg_n_5_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [23]),
        .Q(\int_searched_reg_n_5_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [24]),
        .Q(\int_searched_reg_n_5_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [25]),
        .Q(\int_searched_reg_n_5_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [26]),
        .Q(\int_searched_reg_n_5_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [27]),
        .Q(\int_searched_reg_n_5_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [28]),
        .Q(\int_searched_reg_n_5_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [29]),
        .Q(\int_searched_reg_n_5_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [2]),
        .Q(\int_searched_reg_n_5_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [30]),
        .Q(\int_searched_reg_n_5_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [31]),
        .Q(\int_searched_reg_n_5_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [3]),
        .Q(\int_searched_reg_n_5_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [4]),
        .Q(\int_searched_reg_n_5_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [5]),
        .Q(\int_searched_reg_n_5_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [6]),
        .Q(\int_searched_reg_n_5_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [7]),
        .Q(\int_searched_reg_n_5_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [8]),
        .Q(\int_searched_reg_n_5_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [9]),
        .Q(\int_searched_reg_n_5_[9] ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hE0)) 
    interrupt_INST_0
       (.I0(p_1_in),
        .I1(\int_isr_reg_n_5_[0] ),
        .I2(int_gie_reg_n_5),
        .O(interrupt));
  LUT6 #(
    .INIT(64'hFFFEFEFEFEFEFEFE)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[0]_i_2_n_5 ),
        .I1(\rdata_data[0]_i_3_n_5 ),
        .I2(\rdata_data[0]_i_4_n_5 ),
        .I3(\int_isr_reg_n_5_[0] ),
        .I4(\rdata_data[0]_i_5_n_5 ),
        .I5(\rdata_data[0]_i_6_n_5 ),
        .O(rdata_data[0]));
  LUT6 #(
    .INIT(64'h8800FF008800C000)) 
    \rdata_data[0]_i_2 
       (.I0(\int_ier_reg_n_5_[0] ),
        .I1(\rdata_data[0]_i_7_n_5 ),
        .I2(ap_start),
        .I3(\rdata_data[1]_i_5_n_5 ),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\rdata_data[0]_i_8_n_5 ),
        .O(\rdata_data[0]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'h00F8008800880088)) 
    \rdata_data[0]_i_3 
       (.I0(\rdata_data[0]_i_9_n_5 ),
        .I1(\rdata_data[0]_i_5_n_5 ),
        .I2(\rdata_data[1]_i_5_n_5 ),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(int_agg_result_a_ap_vld_i_2_n_5),
        .I5(int_agg_result_a[0]),
        .O(\rdata_data[0]_i_3_n_5 ));
  LUT6 #(
    .INIT(64'h00000C0800000008)) 
    \rdata_data[0]_i_4 
       (.I0(int_agg_result_b[0]),
        .I1(\rdata_data[1]_i_4_n_5 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .I5(int_agg_result_b_ap_vld),
        .O(\rdata_data[0]_i_4_n_5 ));
  LUT3 #(
    .INIT(8'h04)) 
    \rdata_data[0]_i_5 
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[0]_i_5_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h10)) 
    \rdata_data[0]_i_6 
       (.I0(s_axi_CONTROL_BUS_ARADDR[5]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[0]_i_6_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \rdata_data[0]_i_7 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[0]_i_7_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \rdata_data[0]_i_8 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .I2(\int_searched_reg_n_5_[0] ),
        .O(\rdata_data[0]_i_8_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0B08)) 
    \rdata_data[0]_i_9 
       (.I0(int_agg_result_a_ap_vld),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(int_gie_reg_n_5),
        .O(\rdata_data[0]_i_9_n_5 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[10]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[10]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[10] ),
        .I4(int_agg_result_b[10]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[11]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[11]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[11] ),
        .I4(int_agg_result_b[11]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[11]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[12]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[12]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[12] ),
        .I4(int_agg_result_b[12]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[12]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[13]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[13]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[13] ),
        .I4(int_agg_result_b[13]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[13]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[14]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[14]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[14] ),
        .I4(int_agg_result_b[14]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[14]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[15]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[15]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[15] ),
        .I4(int_agg_result_b[15]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[15]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[16]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[16]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[16] ),
        .I4(int_agg_result_b[16]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[16]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[17]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[17]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[17] ),
        .I4(int_agg_result_b[17]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[17]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[18]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[18]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[18] ),
        .I4(int_agg_result_b[18]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[18]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[19]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[19]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[19] ),
        .I4(int_agg_result_b[19]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[19]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEEEEEEE)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[1]_i_2_n_5 ),
        .I1(\rdata_data[1]_i_3_n_5 ),
        .I2(int_agg_result_b[1]),
        .I3(\rdata_data[1]_i_4_n_5 ),
        .I4(\rdata_data[1]_i_5_n_5 ),
        .I5(\rdata_data[1]_i_6_n_5 ),
        .O(rdata_data[1]));
  LUT6 #(
    .INIT(64'h0320000000200000)) 
    \rdata_data[1]_i_2 
       (.I0(int_agg_result_a[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(\rdata_data[1]_i_5_n_5 ),
        .I5(p_0_in),
        .O(\rdata_data[1]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'h0000080C00000800)) 
    \rdata_data[1]_i_3 
       (.I0(\int_searched_reg_n_5_[1] ),
        .I1(\rdata_data[1]_i_5_n_5 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(int_ap_done),
        .O(\rdata_data[1]_i_3_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \rdata_data[1]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[5]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[1]_i_4_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \rdata_data[1]_i_5 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(\rdata_data[1]_i_5_n_5 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000080)) 
    \rdata_data[1]_i_6 
       (.I0(p_1_in),
        .I1(\rdata_data[0]_i_5_n_5 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[1]_i_6_n_5 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[20]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[20]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[20] ),
        .I4(int_agg_result_b[20]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[20]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[21]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[21]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[21] ),
        .I4(int_agg_result_b[21]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[21]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[22]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[22]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[22] ),
        .I4(int_agg_result_b[22]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[22]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[23]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[23]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[23] ),
        .I4(int_agg_result_b[23]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[23]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[24]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[24]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[24] ),
        .I4(int_agg_result_b[24]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[24]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[25]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[25]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[25] ),
        .I4(int_agg_result_b[25]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[25]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[26]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[26]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[26] ),
        .I4(int_agg_result_b[26]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[26]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[27]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[27]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[27] ),
        .I4(int_agg_result_b[27]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[27]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[28]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[28]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[28] ),
        .I4(int_agg_result_b[28]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[28]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[29]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[29]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[29] ),
        .I4(int_agg_result_b[29]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[29]));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \rdata_data[2]_i_1 
       (.I0(\rdata_data[2]_i_2_n_5 ),
        .I1(\rdata_data[31]_i_5_n_5 ),
        .I2(int_agg_result_b[2]),
        .I3(\rdata_data[31]_i_4_n_5 ),
        .I4(\int_searched_reg_n_5_[2] ),
        .O(rdata_data[2]));
  LUT6 #(
    .INIT(64'h00000C0A00000000)) 
    \rdata_data[2]_i_2 
       (.I0(int_ap_idle),
        .I1(int_agg_result_a[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(\rdata_data[1]_i_5_n_5 ),
        .O(\rdata_data[2]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[30]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[30]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[30] ),
        .I4(int_agg_result_b[30]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[30]));
  LUT3 #(
    .INIT(8'h10)) 
    \rdata_data[31]_i_1 
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .O(ar_hs));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[31]_i_2 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[31]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[31] ),
        .I4(int_agg_result_b[31]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[31]));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \rdata_data[31]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[31]_i_3_n_5 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \rdata_data[31]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[31]_i_4_n_5 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \rdata_data[31]_i_5 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[31]_i_5_n_5 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \rdata_data[3]_i_1 
       (.I0(\rdata_data[3]_i_2_n_5 ),
        .I1(\rdata_data[31]_i_5_n_5 ),
        .I2(int_agg_result_b[3]),
        .I3(\rdata_data[31]_i_4_n_5 ),
        .I4(\int_searched_reg_n_5_[3] ),
        .O(rdata_data[3]));
  LUT6 #(
    .INIT(64'h00000C0A00000000)) 
    \rdata_data[3]_i_2 
       (.I0(int_ap_ready),
        .I1(int_agg_result_a[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(\rdata_data[1]_i_5_n_5 ),
        .O(\rdata_data[3]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[4]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[4]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[4] ),
        .I4(int_agg_result_b[4]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[4]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[5]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[5]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[5] ),
        .I4(int_agg_result_b[5]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[5]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[6]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[6]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[6] ),
        .I4(int_agg_result_b[6]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[6]));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \rdata_data[7]_i_1 
       (.I0(\rdata_data[7]_i_2_n_5 ),
        .I1(\rdata_data[31]_i_5_n_5 ),
        .I2(int_agg_result_b[7]),
        .I3(\rdata_data[31]_i_4_n_5 ),
        .I4(\int_searched_reg_n_5_[7] ),
        .O(rdata_data[7]));
  LUT6 #(
    .INIT(64'h00000C0A00000000)) 
    \rdata_data[7]_i_2 
       (.I0(int_auto_restart),
        .I1(int_agg_result_a[7]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(\rdata_data[1]_i_5_n_5 ),
        .O(\rdata_data[7]_i_2_n_5 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[8]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[8]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[8] ),
        .I4(int_agg_result_b[8]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rdata_data[9]_i_1 
       (.I0(\rdata_data[31]_i_3_n_5 ),
        .I1(int_agg_result_a[9]),
        .I2(\rdata_data[31]_i_4_n_5 ),
        .I3(\int_searched_reg_n_5_[9] ),
        .I4(int_agg_result_b[9]),
        .I5(\rdata_data[31]_i_5_n_5 ),
        .O(rdata_data[9]));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[10]),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .R(1'b0));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[11]),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .R(1'b0));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[12]),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(1'b0));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[13]),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(1'b0));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[14]),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(1'b0));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[15]),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(1'b0));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[16]),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(1'b0));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[17]),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(1'b0));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[18]),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(1'b0));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[19]),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(1'b0));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[20]),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(1'b0));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[21]),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(1'b0));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[22]),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(1'b0));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[23]),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(1'b0));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[24]),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(1'b0));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[25]),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(1'b0));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[26]),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(1'b0));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[27]),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(1'b0));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[28]),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(1'b0));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[29]),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(1'b0));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[30]),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(1'b0));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[31]),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(1'b0));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(1'b0));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[4]),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(1'b0));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[5]),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(1'b0));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[6]),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .R(1'b0));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(1'b0));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[8]),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .R(1'b0));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[9]),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h005C)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_RREADY),
        .I1(s_axi_CONTROL_BUS_ARVALID),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rstate[0]_i_1_n_5 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_5 ),
        .Q(rstate[0]),
        .R(ARESET));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(ARESET));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[5]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_5_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_5_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_5_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_5_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_5_[4] ),
        .R(1'b0));
  FDRE \waddr_reg[5] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[5]),
        .Q(\waddr_reg_n_5_[5] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb
   (D,
    p_67_in,
    CO,
    \int_agg_result_b_reg[31] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    ap_enable_reg_pp0_iter1_reg,
    \exitcond1_reg_343_reg[0] ,
    lastValues_0_q0,
    \a_b_2_reg_234_reg[10] ,
    \i_reg_210_reg[5] ,
    tmp_5_reg_361,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [31:0]D;
  output p_67_in;
  output [0:0]CO;
  output [31:0]\int_agg_result_b_reg[31] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [2:0]Q;
  input ap_enable_reg_pp0_iter1_reg;
  input \exitcond1_reg_343_reg[0] ;
  input [31:0]lastValues_0_q0;
  input [10:0]\a_b_2_reg_234_reg[10] ;
  input [5:0]\i_reg_210_reg[5] ;
  input tmp_5_reg_361;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [0:0]CO;
  wire [31:0]D;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [10:0]\a_b_2_reg_234_reg[10] ;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter1_reg;
  wire \exitcond1_reg_343_reg[0] ;
  wire [5:0]\i_reg_210_reg[5] ;
  wire [31:0]\int_agg_result_b_reg[31] ;
  wire [31:0]lastValues_0_q0;
  wire p_67_in;
  wire tmp_5_reg_361;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram_1 Adder2_inputValuebkb_ram_U
       (.CO(CO),
        .D(D),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .WEBWE(p_67_in),
        .\a_b_2_reg_234_reg[10] (\a_b_2_reg_234_reg[10] ),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter1_reg(ap_enable_reg_pp0_iter1_reg),
        .\exitcond1_reg_343_reg[0] (\exitcond1_reg_343_reg[0] ),
        .\i_reg_210_reg[5] (\i_reg_210_reg[5] ),
        .\int_agg_result_b_reg[31] (\int_agg_result_b_reg[31] ),
        .lastValues_0_q0(lastValues_0_q0),
        .tmp_5_reg_361(tmp_5_reg_361));
endmodule

(* ORIG_REF_NAME = "Adder2_inputValuebkb" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_0
   (lastValues_0_q0,
    p_52_in,
    \int_agg_result_a_reg[31] ,
    ap_clk,
    ap_enable_reg_pp1_iter1_reg,
    \exitcond_reg_352_reg[0] ,
    \LAST_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    \i_1_reg_222_reg[5] ,
    \a_b_2_reg_234_reg[5] ,
    tmp_5_reg_361,
    \a_a_1_reg_380_reg[31] ,
    \LAST_STREAM_V_data_V_0_payload_B_reg[31] ,
    \LAST_STREAM_V_data_V_0_payload_A_reg[31] ,
    LAST_STREAM_V_data_V_0_sel);
  output [31:0]lastValues_0_q0;
  output p_52_in;
  output [31:0]\int_agg_result_a_reg[31] ;
  input ap_clk;
  input ap_enable_reg_pp1_iter1_reg;
  input \exitcond_reg_352_reg[0] ;
  input \LAST_STREAM_V_data_V_0_state_reg[0] ;
  input [2:0]Q;
  input [5:0]\i_1_reg_222_reg[5] ;
  input [5:0]\a_b_2_reg_234_reg[5] ;
  input tmp_5_reg_361;
  input [31:0]\a_a_1_reg_380_reg[31] ;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  input LAST_STREAM_V_data_V_0_sel;

  wire [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire LAST_STREAM_V_data_V_0_sel;
  wire \LAST_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [31:0]\a_a_1_reg_380_reg[31] ;
  wire [5:0]\a_b_2_reg_234_reg[5] ;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter1_reg;
  wire \exitcond_reg_352_reg[0] ;
  wire [5:0]\i_1_reg_222_reg[5] ;
  wire [31:0]\int_agg_result_a_reg[31] ;
  wire [31:0]lastValues_0_q0;
  wire p_52_in;
  wire tmp_5_reg_361;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram Adder2_inputValuebkb_ram_U
       (.\LAST_STREAM_V_data_V_0_payload_A_reg[31] (\LAST_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\LAST_STREAM_V_data_V_0_payload_B_reg[31] (\LAST_STREAM_V_data_V_0_payload_B_reg[31] ),
        .LAST_STREAM_V_data_V_0_sel(LAST_STREAM_V_data_V_0_sel),
        .\LAST_STREAM_V_data_V_0_state_reg[0] (\LAST_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .WEBWE(p_52_in),
        .\a_a_1_reg_380_reg[31] (\a_a_1_reg_380_reg[31] ),
        .\a_b_2_reg_234_reg[5] (\a_b_2_reg_234_reg[5] ),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp1_iter1_reg(ap_enable_reg_pp1_iter1_reg),
        .\exitcond_reg_352_reg[0] (\exitcond_reg_352_reg[0] ),
        .\i_1_reg_222_reg[5] (\i_1_reg_222_reg[5] ),
        .\int_agg_result_a_reg[31] (\int_agg_result_a_reg[31] ),
        .lastValues_0_q0(lastValues_0_q0),
        .tmp_5_reg_361(tmp_5_reg_361));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram
   (lastValues_0_q0,
    WEBWE,
    \int_agg_result_a_reg[31] ,
    ap_clk,
    ap_enable_reg_pp1_iter1_reg,
    \exitcond_reg_352_reg[0] ,
    \LAST_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    \i_1_reg_222_reg[5] ,
    \a_b_2_reg_234_reg[5] ,
    tmp_5_reg_361,
    \a_a_1_reg_380_reg[31] ,
    \LAST_STREAM_V_data_V_0_payload_B_reg[31] ,
    \LAST_STREAM_V_data_V_0_payload_A_reg[31] ,
    LAST_STREAM_V_data_V_0_sel);
  output [31:0]lastValues_0_q0;
  output [0:0]WEBWE;
  output [31:0]\int_agg_result_a_reg[31] ;
  input ap_clk;
  input ap_enable_reg_pp1_iter1_reg;
  input \exitcond_reg_352_reg[0] ;
  input \LAST_STREAM_V_data_V_0_state_reg[0] ;
  input [2:0]Q;
  input [5:0]\i_1_reg_222_reg[5] ;
  input [5:0]\a_b_2_reg_234_reg[5] ;
  input tmp_5_reg_361;
  input [31:0]\a_a_1_reg_380_reg[31] ;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  input LAST_STREAM_V_data_V_0_sel;

  wire [31:0]\LAST_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\LAST_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire LAST_STREAM_V_data_V_0_sel;
  wire \LAST_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [0:0]WEBWE;
  wire [31:0]\a_a_1_reg_380_reg[31] ;
  wire [5:0]\a_b_2_reg_234_reg[5] ;
  wire [5:0]address0;
  wire ap_clk;
  wire ap_enable_reg_pp1_iter1_reg;
  wire ce08_out;
  wire [31:0]d0;
  wire \exitcond_reg_352_reg[0] ;
  wire [5:0]\i_1_reg_222_reg[5] ;
  wire [31:0]\int_agg_result_a_reg[31] ;
  wire [31:0]lastValues_0_q0;
  wire tmp_5_reg_361;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[0]_i_1 
       (.I0(lastValues_0_q0[0]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [0]),
        .O(\int_agg_result_a_reg[31] [0]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[10]_i_1 
       (.I0(lastValues_0_q0[10]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [10]),
        .O(\int_agg_result_a_reg[31] [10]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[11]_i_1 
       (.I0(lastValues_0_q0[11]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [11]),
        .O(\int_agg_result_a_reg[31] [11]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[12]_i_1 
       (.I0(lastValues_0_q0[12]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [12]),
        .O(\int_agg_result_a_reg[31] [12]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[13]_i_1 
       (.I0(lastValues_0_q0[13]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [13]),
        .O(\int_agg_result_a_reg[31] [13]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[14]_i_1 
       (.I0(lastValues_0_q0[14]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [14]),
        .O(\int_agg_result_a_reg[31] [14]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[15]_i_1 
       (.I0(lastValues_0_q0[15]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [15]),
        .O(\int_agg_result_a_reg[31] [15]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[16]_i_1 
       (.I0(lastValues_0_q0[16]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [16]),
        .O(\int_agg_result_a_reg[31] [16]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[17]_i_1 
       (.I0(lastValues_0_q0[17]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [17]),
        .O(\int_agg_result_a_reg[31] [17]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[18]_i_1 
       (.I0(lastValues_0_q0[18]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [18]),
        .O(\int_agg_result_a_reg[31] [18]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[19]_i_1 
       (.I0(lastValues_0_q0[19]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [19]),
        .O(\int_agg_result_a_reg[31] [19]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[1]_i_1 
       (.I0(lastValues_0_q0[1]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [1]),
        .O(\int_agg_result_a_reg[31] [1]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[20]_i_1 
       (.I0(lastValues_0_q0[20]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [20]),
        .O(\int_agg_result_a_reg[31] [20]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[21]_i_1 
       (.I0(lastValues_0_q0[21]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [21]),
        .O(\int_agg_result_a_reg[31] [21]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[22]_i_1 
       (.I0(lastValues_0_q0[22]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [22]),
        .O(\int_agg_result_a_reg[31] [22]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[23]_i_1 
       (.I0(lastValues_0_q0[23]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [23]),
        .O(\int_agg_result_a_reg[31] [23]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[24]_i_1 
       (.I0(lastValues_0_q0[24]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [24]),
        .O(\int_agg_result_a_reg[31] [24]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[25]_i_1 
       (.I0(lastValues_0_q0[25]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [25]),
        .O(\int_agg_result_a_reg[31] [25]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[26]_i_1 
       (.I0(lastValues_0_q0[26]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [26]),
        .O(\int_agg_result_a_reg[31] [26]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[27]_i_1 
       (.I0(lastValues_0_q0[27]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [27]),
        .O(\int_agg_result_a_reg[31] [27]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[28]_i_1 
       (.I0(lastValues_0_q0[28]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [28]),
        .O(\int_agg_result_a_reg[31] [28]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[29]_i_1 
       (.I0(lastValues_0_q0[29]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [29]),
        .O(\int_agg_result_a_reg[31] [29]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[2]_i_1 
       (.I0(lastValues_0_q0[2]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [2]),
        .O(\int_agg_result_a_reg[31] [2]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[30]_i_1 
       (.I0(lastValues_0_q0[30]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [30]),
        .O(\int_agg_result_a_reg[31] [30]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[31]_i_1 
       (.I0(lastValues_0_q0[31]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [31]),
        .O(\int_agg_result_a_reg[31] [31]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[3]_i_1 
       (.I0(lastValues_0_q0[3]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [3]),
        .O(\int_agg_result_a_reg[31] [3]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[4]_i_1 
       (.I0(lastValues_0_q0[4]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [4]),
        .O(\int_agg_result_a_reg[31] [4]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[5]_i_1 
       (.I0(lastValues_0_q0[5]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [5]),
        .O(\int_agg_result_a_reg[31] [5]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[6]_i_1 
       (.I0(lastValues_0_q0[6]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [6]),
        .O(\int_agg_result_a_reg[31] [6]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[7]_i_1 
       (.I0(lastValues_0_q0[7]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [7]),
        .O(\int_agg_result_a_reg[31] [7]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[8]_i_1 
       (.I0(lastValues_0_q0[8]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [8]),
        .O(\int_agg_result_a_reg[31] [8]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_a[9]_i_1 
       (.I0(lastValues_0_q0[9]),
        .I1(tmp_5_reg_361),
        .I2(\a_a_1_reg_380_reg[31] [9]),
        .O(\int_agg_result_a_reg[31] [9]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1600" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "49" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI(d0[15:0]),
        .DIBDI({1'b1,1'b1,d0[31:18]}),
        .DIPADIP(d0[17:16]),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(lastValues_0_q0[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],lastValues_0_q0[31:18]}),
        .DOPADOP(lastValues_0_q0[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ce08_out),
        .ENBWREN(ce08_out),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEBWE,WEBWE}),
        .WEBWE({1'b0,1'b0,WEBWE,WEBWE}));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFA800)) 
    ram_reg_i_1
       (.I0(Q[0]),
        .I1(\LAST_STREAM_V_data_V_0_state_reg[0] ),
        .I2(\exitcond_reg_352_reg[0] ),
        .I3(ap_enable_reg_pp1_iter1_reg),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(ce08_out));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_10
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[13]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_11
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[12]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_12
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[11]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_13
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[10]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_14
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[9]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_15
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[8]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_16
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[7]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_17
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[6]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_18
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[5]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_19
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[4]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_20
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[3]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_21
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[2]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_22
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[1]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_23
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_24
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [31]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [31]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[31]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_25
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [30]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [30]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[30]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_26
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [29]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [29]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[29]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_27
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [28]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [28]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[28]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_28
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [27]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [27]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[27]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_29
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[26]));
  LUT4 #(
    .INIT(16'h00CA)) 
    ram_reg_i_2__0
       (.I0(\i_1_reg_222_reg[5] [5]),
        .I1(\a_b_2_reg_234_reg[5] [5]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(address0[5]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_30
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[25]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_31
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[24]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_32
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[23]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_33
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[22]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_34
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[21]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_35
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[20]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_36
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[19]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_37
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[18]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_38
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[17]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_39
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[16]));
  LUT4 #(
    .INIT(16'h00CA)) 
    ram_reg_i_3__0
       (.I0(\i_1_reg_222_reg[5] [4]),
        .I1(\a_b_2_reg_234_reg[5] [4]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(address0[4]));
  LUT4 #(
    .INIT(16'h2000)) 
    ram_reg_i_40
       (.I0(ap_enable_reg_pp1_iter1_reg),
        .I1(\exitcond_reg_352_reg[0] ),
        .I2(\LAST_STREAM_V_data_V_0_state_reg[0] ),
        .I3(Q[0]),
        .O(WEBWE));
  LUT4 #(
    .INIT(16'h00CA)) 
    ram_reg_i_4__0
       (.I0(\i_1_reg_222_reg[5] [3]),
        .I1(\a_b_2_reg_234_reg[5] [3]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(address0[3]));
  LUT4 #(
    .INIT(16'h00CA)) 
    ram_reg_i_5__0
       (.I0(\i_1_reg_222_reg[5] [2]),
        .I1(\a_b_2_reg_234_reg[5] [2]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(address0[2]));
  LUT4 #(
    .INIT(16'h00CA)) 
    ram_reg_i_6__0
       (.I0(\i_1_reg_222_reg[5] [1]),
        .I1(\a_b_2_reg_234_reg[5] [1]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(address0[1]));
  LUT4 #(
    .INIT(16'h00CA)) 
    ram_reg_i_7__0
       (.I0(\i_1_reg_222_reg[5] [0]),
        .I1(\a_b_2_reg_234_reg[5] [0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .O(address0[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_8
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[15]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_9
       (.I0(\LAST_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .I1(\LAST_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I2(LAST_STREAM_V_data_V_0_sel),
        .O(d0[14]));
endmodule

(* ORIG_REF_NAME = "Adder2_inputValuebkb_ram" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_inputValuebkb_ram_1
   (D,
    WEBWE,
    CO,
    \int_agg_result_b_reg[31] ,
    ap_clk,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    Q,
    ap_enable_reg_pp0_iter1_reg,
    \exitcond1_reg_343_reg[0] ,
    lastValues_0_q0,
    \a_b_2_reg_234_reg[10] ,
    \i_reg_210_reg[5] ,
    tmp_5_reg_361,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel);
  output [31:0]D;
  output [0:0]WEBWE;
  output [0:0]CO;
  output [31:0]\int_agg_result_b_reg[31] ;
  input ap_clk;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [2:0]Q;
  input ap_enable_reg_pp0_iter1_reg;
  input \exitcond1_reg_343_reg[0] ;
  input [31:0]lastValues_0_q0;
  input [10:0]\a_b_2_reg_234_reg[10] ;
  input [5:0]\i_reg_210_reg[5] ;
  input tmp_5_reg_361;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  input [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;

  wire [0:0]CO;
  wire [31:0]D;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [31:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [0:0]WEBWE;
  wire [10:0]\a_b_2_reg_234_reg[10] ;
  wire \ap_CS_fsm[7]_i_10_n_5 ;
  wire \ap_CS_fsm[7]_i_11_n_5 ;
  wire \ap_CS_fsm[7]_i_12_n_5 ;
  wire \ap_CS_fsm[7]_i_13_n_5 ;
  wire \ap_CS_fsm[7]_i_14_n_5 ;
  wire \ap_CS_fsm[7]_i_15_n_5 ;
  wire \ap_CS_fsm[7]_i_4_n_5 ;
  wire \ap_CS_fsm[7]_i_5_n_5 ;
  wire \ap_CS_fsm[7]_i_6_n_5 ;
  wire \ap_CS_fsm[7]_i_8_n_5 ;
  wire \ap_CS_fsm[7]_i_9_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_2_n_7 ;
  wire \ap_CS_fsm_reg[7]_i_2_n_8 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_7 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_8 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_7 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_8 ;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter1_reg;
  wire ce0;
  wire \exitcond1_reg_343_reg[0] ;
  wire [5:0]\i_reg_210_reg[5] ;
  wire [31:0]\int_agg_result_b_reg[31] ;
  wire [31:0]lastValues_0_q0;
  wire ram_reg_i_10__0_n_5;
  wire ram_reg_i_11__0_n_5;
  wire ram_reg_i_12__0_n_5;
  wire ram_reg_i_13__0_n_5;
  wire ram_reg_i_14__0_n_5;
  wire ram_reg_i_15__0_n_5;
  wire ram_reg_i_16__0_n_5;
  wire ram_reg_i_17__0_n_5;
  wire ram_reg_i_18__0_n_5;
  wire ram_reg_i_19__0_n_5;
  wire ram_reg_i_20__0_n_5;
  wire ram_reg_i_21__0_n_5;
  wire ram_reg_i_22__0_n_5;
  wire ram_reg_i_23__0_n_5;
  wire ram_reg_i_24__0_n_5;
  wire ram_reg_i_25__0_n_5;
  wire ram_reg_i_26__0_n_5;
  wire ram_reg_i_27__0_n_5;
  wire ram_reg_i_28__0_n_5;
  wire ram_reg_i_29__0_n_5;
  wire ram_reg_i_2_n_5;
  wire ram_reg_i_30__0_n_5;
  wire ram_reg_i_31__0_n_5;
  wire ram_reg_i_32__0_n_5;
  wire ram_reg_i_33__0_n_5;
  wire ram_reg_i_34__0_n_5;
  wire ram_reg_i_35__0_n_5;
  wire ram_reg_i_36__0_n_5;
  wire ram_reg_i_37__0_n_5;
  wire ram_reg_i_38__0_n_5;
  wire ram_reg_i_39__0_n_5;
  wire ram_reg_i_3_n_5;
  wire ram_reg_i_4_n_5;
  wire ram_reg_i_5_n_5;
  wire ram_reg_i_6_n_5;
  wire ram_reg_i_7_n_5;
  wire ram_reg_i_8__0_n_5;
  wire ram_reg_i_9__0_n_5;
  wire tmp_5_reg_361;
  wire [3:3]\NLW_ap_CS_fsm_reg[7]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_7_O_UNCONNECTED ;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_10 
       (.I0(D[16]),
        .I1(lastValues_0_q0[16]),
        .I2(D[15]),
        .I3(lastValues_0_q0[15]),
        .I4(D[17]),
        .I5(lastValues_0_q0[17]),
        .O(\ap_CS_fsm[7]_i_10_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_11 
       (.I0(D[13]),
        .I1(lastValues_0_q0[13]),
        .I2(D[12]),
        .I3(lastValues_0_q0[12]),
        .I4(D[14]),
        .I5(lastValues_0_q0[14]),
        .O(\ap_CS_fsm[7]_i_11_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_12 
       (.I0(D[10]),
        .I1(lastValues_0_q0[10]),
        .I2(D[9]),
        .I3(lastValues_0_q0[9]),
        .I4(D[11]),
        .I5(lastValues_0_q0[11]),
        .O(\ap_CS_fsm[7]_i_12_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_13 
       (.I0(D[7]),
        .I1(lastValues_0_q0[7]),
        .I2(D[6]),
        .I3(lastValues_0_q0[6]),
        .I4(D[8]),
        .I5(lastValues_0_q0[8]),
        .O(\ap_CS_fsm[7]_i_13_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_14 
       (.I0(D[4]),
        .I1(lastValues_0_q0[4]),
        .I2(D[3]),
        .I3(lastValues_0_q0[3]),
        .I4(D[5]),
        .I5(lastValues_0_q0[5]),
        .O(\ap_CS_fsm[7]_i_14_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_15 
       (.I0(D[1]),
        .I1(lastValues_0_q0[1]),
        .I2(D[0]),
        .I3(lastValues_0_q0[0]),
        .I4(D[2]),
        .I5(lastValues_0_q0[2]),
        .O(\ap_CS_fsm[7]_i_15_n_5 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ap_CS_fsm[7]_i_4 
       (.I0(D[30]),
        .I1(lastValues_0_q0[30]),
        .I2(D[31]),
        .I3(lastValues_0_q0[31]),
        .O(\ap_CS_fsm[7]_i_4_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_5 
       (.I0(D[28]),
        .I1(lastValues_0_q0[28]),
        .I2(D[27]),
        .I3(lastValues_0_q0[27]),
        .I4(D[29]),
        .I5(lastValues_0_q0[29]),
        .O(\ap_CS_fsm[7]_i_5_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_6 
       (.I0(D[25]),
        .I1(lastValues_0_q0[25]),
        .I2(D[24]),
        .I3(lastValues_0_q0[24]),
        .I4(D[26]),
        .I5(lastValues_0_q0[26]),
        .O(\ap_CS_fsm[7]_i_6_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_8 
       (.I0(D[22]),
        .I1(lastValues_0_q0[22]),
        .I2(D[21]),
        .I3(lastValues_0_q0[21]),
        .I4(D[23]),
        .I5(lastValues_0_q0[23]),
        .O(\ap_CS_fsm[7]_i_8_n_5 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_9 
       (.I0(D[19]),
        .I1(lastValues_0_q0[19]),
        .I2(D[18]),
        .I3(lastValues_0_q0[18]),
        .I4(D[20]),
        .I5(lastValues_0_q0[20]),
        .O(\ap_CS_fsm[7]_i_9_n_5 ));
  CARRY4 \ap_CS_fsm_reg[7]_i_2 
       (.CI(\ap_CS_fsm_reg[7]_i_3_n_5 ),
        .CO({\NLW_ap_CS_fsm_reg[7]_i_2_CO_UNCONNECTED [3],CO,\ap_CS_fsm_reg[7]_i_2_n_7 ,\ap_CS_fsm_reg[7]_i_2_n_8 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,\ap_CS_fsm[7]_i_4_n_5 ,\ap_CS_fsm[7]_i_5_n_5 ,\ap_CS_fsm[7]_i_6_n_5 }));
  CARRY4 \ap_CS_fsm_reg[7]_i_3 
       (.CI(\ap_CS_fsm_reg[7]_i_7_n_5 ),
        .CO({\ap_CS_fsm_reg[7]_i_3_n_5 ,\ap_CS_fsm_reg[7]_i_3_n_6 ,\ap_CS_fsm_reg[7]_i_3_n_7 ,\ap_CS_fsm_reg[7]_i_3_n_8 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_3_O_UNCONNECTED [3:0]),
        .S({\ap_CS_fsm[7]_i_8_n_5 ,\ap_CS_fsm[7]_i_9_n_5 ,\ap_CS_fsm[7]_i_10_n_5 ,\ap_CS_fsm[7]_i_11_n_5 }));
  CARRY4 \ap_CS_fsm_reg[7]_i_7 
       (.CI(1'b0),
        .CO({\ap_CS_fsm_reg[7]_i_7_n_5 ,\ap_CS_fsm_reg[7]_i_7_n_6 ,\ap_CS_fsm_reg[7]_i_7_n_7 ,\ap_CS_fsm_reg[7]_i_7_n_8 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_7_O_UNCONNECTED [3:0]),
        .S({\ap_CS_fsm[7]_i_12_n_5 ,\ap_CS_fsm[7]_i_13_n_5 ,\ap_CS_fsm[7]_i_14_n_5 ,\ap_CS_fsm[7]_i_15_n_5 }));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[0]_i_1 
       (.I0(D[0]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [0]),
        .O(\int_agg_result_b_reg[31] [0]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[10]_i_1 
       (.I0(D[10]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [10]),
        .O(\int_agg_result_b_reg[31] [10]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[11]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[11]),
        .O(\int_agg_result_b_reg[31] [11]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[12]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[12]),
        .O(\int_agg_result_b_reg[31] [12]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[13]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[13]),
        .O(\int_agg_result_b_reg[31] [13]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[14]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[14]),
        .O(\int_agg_result_b_reg[31] [14]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[15]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[15]),
        .O(\int_agg_result_b_reg[31] [15]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[16]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[16]),
        .O(\int_agg_result_b_reg[31] [16]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[17]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[17]),
        .O(\int_agg_result_b_reg[31] [17]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[18]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[18]),
        .O(\int_agg_result_b_reg[31] [18]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[19]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[19]),
        .O(\int_agg_result_b_reg[31] [19]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[1]_i_1 
       (.I0(D[1]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [1]),
        .O(\int_agg_result_b_reg[31] [1]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[20]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[20]),
        .O(\int_agg_result_b_reg[31] [20]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[21]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[21]),
        .O(\int_agg_result_b_reg[31] [21]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[22]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[22]),
        .O(\int_agg_result_b_reg[31] [22]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[23]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[23]),
        .O(\int_agg_result_b_reg[31] [23]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[24]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[24]),
        .O(\int_agg_result_b_reg[31] [24]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[25]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[25]),
        .O(\int_agg_result_b_reg[31] [25]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[26]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[26]),
        .O(\int_agg_result_b_reg[31] [26]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[27]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[27]),
        .O(\int_agg_result_b_reg[31] [27]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[28]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[28]),
        .O(\int_agg_result_b_reg[31] [28]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[29]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[29]),
        .O(\int_agg_result_b_reg[31] [29]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[2]_i_1 
       (.I0(D[2]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [2]),
        .O(\int_agg_result_b_reg[31] [2]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[30]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[30]),
        .O(\int_agg_result_b_reg[31] [30]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_b[31]_i_1 
       (.I0(tmp_5_reg_361),
        .I1(D[31]),
        .O(\int_agg_result_b_reg[31] [31]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[3]_i_1 
       (.I0(D[3]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [3]),
        .O(\int_agg_result_b_reg[31] [3]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[4]_i_1 
       (.I0(D[4]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [4]),
        .O(\int_agg_result_b_reg[31] [4]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[5]_i_1 
       (.I0(D[5]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [5]),
        .O(\int_agg_result_b_reg[31] [5]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[6]_i_1 
       (.I0(D[6]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [6]),
        .O(\int_agg_result_b_reg[31] [6]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[7]_i_1 
       (.I0(D[7]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [7]),
        .O(\int_agg_result_b_reg[31] [7]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[8]_i_1 
       (.I0(D[8]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [8]),
        .O(\int_agg_result_b_reg[31] [8]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_agg_result_b[9]_i_1 
       (.I0(D[9]),
        .I1(tmp_5_reg_361),
        .I2(\a_b_2_reg_234_reg[10] [9]),
        .O(\int_agg_result_b_reg[31] [9]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1600" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "49" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,1'b1,ram_reg_i_2_n_5,ram_reg_i_3_n_5,ram_reg_i_4_n_5,ram_reg_i_5_n_5,ram_reg_i_6_n_5,ram_reg_i_7_n_5,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,ram_reg_i_2_n_5,ram_reg_i_3_n_5,ram_reg_i_4_n_5,ram_reg_i_5_n_5,ram_reg_i_6_n_5,ram_reg_i_7_n_5,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({ram_reg_i_8__0_n_5,ram_reg_i_9__0_n_5,ram_reg_i_10__0_n_5,ram_reg_i_11__0_n_5,ram_reg_i_12__0_n_5,ram_reg_i_13__0_n_5,ram_reg_i_14__0_n_5,ram_reg_i_15__0_n_5,ram_reg_i_16__0_n_5,ram_reg_i_17__0_n_5,ram_reg_i_18__0_n_5,ram_reg_i_19__0_n_5,ram_reg_i_20__0_n_5,ram_reg_i_21__0_n_5,ram_reg_i_22__0_n_5,ram_reg_i_23__0_n_5}),
        .DIBDI({1'b1,1'b1,ram_reg_i_24__0_n_5,ram_reg_i_25__0_n_5,ram_reg_i_26__0_n_5,ram_reg_i_27__0_n_5,ram_reg_i_28__0_n_5,ram_reg_i_29__0_n_5,ram_reg_i_30__0_n_5,ram_reg_i_31__0_n_5,ram_reg_i_32__0_n_5,ram_reg_i_33__0_n_5,ram_reg_i_34__0_n_5,ram_reg_i_35__0_n_5,ram_reg_i_36__0_n_5,ram_reg_i_37__0_n_5}),
        .DIPADIP({ram_reg_i_38__0_n_5,ram_reg_i_39__0_n_5}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(D[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],D[31:18]}),
        .DOPADOP(D[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ce0),
        .ENBWREN(ce0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEBWE,WEBWE}),
        .WEBWE({1'b0,1'b0,WEBWE,WEBWE}));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_10__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_10__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_11__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_11__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_12__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_12__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_13__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_13__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_14__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_14__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_15__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_15__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_16__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_16__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_17__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_17__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_18__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_18__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_19__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_19__0_n_5));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF8880)) 
    ram_reg_i_1__0
       (.I0(ap_enable_reg_pp0_iter1_reg),
        .I1(Q[0]),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I3(\exitcond1_reg_343_reg[0] ),
        .I4(Q[1]),
        .I5(Q[2]),
        .O(ce0));
  LUT4 #(
    .INIT(16'h2320)) 
    ram_reg_i_2
       (.I0(\a_b_2_reg_234_reg[10] [5]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\i_reg_210_reg[5] [5]),
        .O(ram_reg_i_2_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_20__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_20__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_21__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_21__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_22__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_22__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_23__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_23__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_24__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [31]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [31]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_24__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_25__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [30]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [30]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_25__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_26__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [29]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [29]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_26__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_27__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [28]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [28]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_27__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_28__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [27]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [27]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_28__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_29__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_29__0_n_5));
  LUT4 #(
    .INIT(16'h2320)) 
    ram_reg_i_3
       (.I0(\a_b_2_reg_234_reg[10] [4]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\i_reg_210_reg[5] [4]),
        .O(ram_reg_i_3_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_30__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_30__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_31__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_31__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_32__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_32__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_33__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_33__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_34__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_34__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_35__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_35__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_36__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_36__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_37__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_37__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_38__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_38__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_39__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_39__0_n_5));
  LUT4 #(
    .INIT(16'h2320)) 
    ram_reg_i_4
       (.I0(\a_b_2_reg_234_reg[10] [3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\i_reg_210_reg[5] [3]),
        .O(ram_reg_i_4_n_5));
  LUT4 #(
    .INIT(16'h0080)) 
    ram_reg_i_40__0
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I1(Q[0]),
        .I2(ap_enable_reg_pp0_iter1_reg),
        .I3(\exitcond1_reg_343_reg[0] ),
        .O(WEBWE));
  LUT4 #(
    .INIT(16'h2320)) 
    ram_reg_i_5
       (.I0(\a_b_2_reg_234_reg[10] [2]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\i_reg_210_reg[5] [2]),
        .O(ram_reg_i_5_n_5));
  LUT4 #(
    .INIT(16'h2320)) 
    ram_reg_i_6
       (.I0(\a_b_2_reg_234_reg[10] [1]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\i_reg_210_reg[5] [1]),
        .O(ram_reg_i_6_n_5));
  LUT4 #(
    .INIT(16'h2320)) 
    ram_reg_i_7
       (.I0(\a_b_2_reg_234_reg[10] [0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(\i_reg_210_reg[5] [0]),
        .O(ram_reg_i_7_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_8__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_8__0_n_5));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_9__0
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .O(ram_reg_i_9__0_n_5));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    LAST_STREAM_TVALID,
    LAST_STREAM_TREADY,
    LAST_STREAM_TDATA,
    LAST_STREAM_TDEST,
    LAST_STREAM_TKEEP,
    LAST_STREAM_TSTRB,
    LAST_STREAM_TUSER,
    LAST_STREAM_TLAST,
    LAST_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 6, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 1e+08, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [5:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM:LAST_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME LAST_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input LAST_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TREADY" *) output LAST_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TDATA" *) input [31:0]LAST_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TDEST" *) input [5:0]LAST_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TKEEP" *) input [3:0]LAST_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TSTRB" *) input [3:0]LAST_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TUSER" *) input [1:0]LAST_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TLAST" *) input [0:0]LAST_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TID" *) input [4:0]LAST_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire [31:0]LAST_STREAM_TDATA;
  wire [5:0]LAST_STREAM_TDEST;
  wire [4:0]LAST_STREAM_TID;
  wire [3:0]LAST_STREAM_TKEEP;
  wire [0:0]LAST_STREAM_TLAST;
  wire LAST_STREAM_TREADY;
  wire [3:0]LAST_STREAM_TSTRB;
  wire [1:0]LAST_STREAM_TUSER;
  wire LAST_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .LAST_STREAM_TDATA(LAST_STREAM_TDATA),
        .LAST_STREAM_TDEST(LAST_STREAM_TDEST),
        .LAST_STREAM_TID(LAST_STREAM_TID),
        .LAST_STREAM_TKEEP(LAST_STREAM_TKEEP),
        .LAST_STREAM_TLAST(LAST_STREAM_TLAST),
        .LAST_STREAM_TREADY(LAST_STREAM_TREADY),
        .LAST_STREAM_TSTRB(LAST_STREAM_TSTRB),
        .LAST_STREAM_TUSER(LAST_STREAM_TUSER),
        .LAST_STREAM_TVALID(LAST_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
