-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Sat Feb 10 10:08:36 2018
-- Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_sum_0_0_sim_netlist.vhdl
-- Design      : system_sum_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum_CRTLS_s_axi is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_CRTLS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CRTLS_RVALID : out STD_LOGIC;
    s_axi_CRTLS_ARREADY : out STD_LOGIC;
    interrupt : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    s_axi_CRTLS_BREADY : in STD_LOGIC;
    s_axi_CRTLS_WVALID : in STD_LOGIC;
    s_axi_CRTLS_AWVALID : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    s_axi_CRTLS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CRTLS_ARVALID : in STD_LOGIC;
    s_axi_CRTLS_ARADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CRTLS_AWADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CRTLS_RREADY : in STD_LOGIC;
    s_axi_CRTLS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum_CRTLS_s_axi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum_CRTLS_s_axi is
  signal \/FSM_onehot_wstate[1]_i_1_n_0\ : STD_LOGIC;
  signal \/FSM_onehot_wstate[2]_i_1_n_0\ : STD_LOGIC;
  signal ARESET : STD_LOGIC;
  signal \FSM_onehot_wstate[3]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_wstate_reg_n_0_[0]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_wstate_reg_n_0_[0]\ : signal is "yes";
  signal a : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ap_done : STD_LOGIC;
  signal ap_return : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ar_hs : STD_LOGIC;
  signal b : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \int_a[31]_i_3_n_0\ : STD_LOGIC;
  signal int_ap_done : STD_LOGIC;
  signal int_ap_done_i_1_n_0 : STD_LOGIC;
  signal int_ap_done_i_2_n_0 : STD_LOGIC;
  signal int_ap_idle : STD_LOGIC;
  signal int_ap_ready : STD_LOGIC;
  signal int_ap_return : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \int_ap_return[11]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[11]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[11]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[11]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[15]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[15]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[15]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[15]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[19]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[19]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[19]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[19]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[23]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[23]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[23]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[23]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[27]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[27]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[27]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[27]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[31]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[31]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[31]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[31]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[3]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[3]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[3]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[3]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return[7]_i_2_n_0\ : STD_LOGIC;
  signal \int_ap_return[7]_i_3_n_0\ : STD_LOGIC;
  signal \int_ap_return[7]_i_4_n_0\ : STD_LOGIC;
  signal \int_ap_return[7]_i_5_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \int_ap_return_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \int_ap_return_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \int_ap_return_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \int_ap_return_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal int_ap_start1 : STD_LOGIC;
  signal int_ap_start_i_1_n_0 : STD_LOGIC;
  signal int_auto_restart_i_1_n_0 : STD_LOGIC;
  signal int_auto_restart_i_2_n_0 : STD_LOGIC;
  signal int_gie_i_1_n_0 : STD_LOGIC;
  signal int_gie_reg_n_0 : STD_LOGIC;
  signal int_ier : STD_LOGIC;
  signal int_ier_i_1_n_0 : STD_LOGIC;
  signal int_isr_i_1_n_0 : STD_LOGIC;
  signal int_isr_i_2_n_0 : STD_LOGIC;
  signal int_isr_i_3_n_0 : STD_LOGIC;
  signal int_isr_reg_n_0 : STD_LOGIC;
  signal \or\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal or0_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^out\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute RTL_KEEP of \^out\ : signal is "yes";
  signal p_0_in : STD_LOGIC;
  signal p_0_in10_out : STD_LOGIC;
  signal p_5_in : STD_LOGIC_VECTOR ( 7 to 7 );
  signal rdata_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \rdata_data[0]_i_2_n_0\ : STD_LOGIC;
  signal \rdata_data[0]_i_3_n_0\ : STD_LOGIC;
  signal \rdata_data[0]_i_4_n_0\ : STD_LOGIC;
  signal \rdata_data[0]_i_5_n_0\ : STD_LOGIC;
  signal \rdata_data[10]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[11]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[12]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[13]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[14]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[15]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[16]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[17]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[18]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[19]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[1]_i_2_n_0\ : STD_LOGIC;
  signal \rdata_data[20]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[21]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[22]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[23]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[24]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[25]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[26]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[27]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[28]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[29]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[2]_i_2_n_0\ : STD_LOGIC;
  signal \rdata_data[30]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[31]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[31]_i_3_n_0\ : STD_LOGIC;
  signal \rdata_data[31]_i_4_n_0\ : STD_LOGIC;
  signal \rdata_data[31]_i_5_n_0\ : STD_LOGIC;
  signal \rdata_data[31]_i_6_n_0\ : STD_LOGIC;
  signal \rdata_data[3]_i_2_n_0\ : STD_LOGIC;
  signal \rdata_data[4]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[5]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[6]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[7]_i_2_n_0\ : STD_LOGIC;
  signal \rdata_data[7]_i_3_n_0\ : STD_LOGIC;
  signal \rdata_data[8]_i_1_n_0\ : STD_LOGIC;
  signal \rdata_data[9]_i_1_n_0\ : STD_LOGIC;
  signal rstate : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rstate[0]_i_1_n_0\ : STD_LOGIC;
  signal waddr : STD_LOGIC;
  signal \waddr_reg_n_0_[0]\ : STD_LOGIC;
  signal \waddr_reg_n_0_[1]\ : STD_LOGIC;
  signal \waddr_reg_n_0_[2]\ : STD_LOGIC;
  signal \waddr_reg_n_0_[3]\ : STD_LOGIC;
  signal \waddr_reg_n_0_[4]\ : STD_LOGIC;
  signal \waddr_reg_n_0_[5]\ : STD_LOGIC;
  signal \NLW_int_ap_return_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[0]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_wstate_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[1]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[2]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_wstate_reg[3]\ : label is "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001";
  attribute KEEP of \FSM_onehot_wstate_reg[3]\ : label is "yes";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \int_a[0]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \int_a[10]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \int_a[11]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \int_a[12]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \int_a[13]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \int_a[14]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \int_a[15]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \int_a[16]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \int_a[17]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \int_a[18]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \int_a[19]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \int_a[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \int_a[20]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \int_a[21]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \int_a[22]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \int_a[23]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \int_a[24]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \int_a[25]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \int_a[26]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \int_a[27]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \int_a[28]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \int_a[29]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \int_a[2]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \int_a[30]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \int_a[31]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \int_a[3]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \int_a[4]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \int_a[5]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \int_a[6]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \int_a[7]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \int_a[8]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \int_a[9]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of int_ap_start_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \int_b[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \int_b[10]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \int_b[11]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \int_b[12]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \int_b[13]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \int_b[14]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \int_b[15]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \int_b[16]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \int_b[17]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \int_b[18]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \int_b[19]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \int_b[1]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \int_b[20]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \int_b[21]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \int_b[22]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \int_b[23]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \int_b[24]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \int_b[25]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \int_b[26]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \int_b[27]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \int_b[28]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \int_b[29]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \int_b[2]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \int_b[30]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \int_b[31]_i_2\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \int_b[3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \int_b[4]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \int_b[5]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \int_b[6]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \int_b[7]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \int_b[8]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \int_b[9]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of int_isr_i_3 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \rstate[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of s_axi_CRTLS_RVALID_INST_0 : label is "soft_lutpair1";
begin
  \out\(2 downto 0) <= \^out\(2 downto 0);
\/FSM_onehot_wstate[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000BFF0B"
    )
        port map (
      I0 => s_axi_CRTLS_BREADY,
      I1 => \^out\(2),
      I2 => \^out\(1),
      I3 => \^out\(0),
      I4 => s_axi_CRTLS_AWVALID,
      O => \/FSM_onehot_wstate[1]_i_1_n_0\
    );
\/FSM_onehot_wstate[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => s_axi_CRTLS_AWVALID,
      I1 => \^out\(0),
      I2 => \^out\(1),
      I3 => s_axi_CRTLS_WVALID,
      O => \/FSM_onehot_wstate[2]_i_1_n_0\
    );
\FSM_onehot_wstate[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => ARESET
    );
\FSM_onehot_wstate[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000F404"
    )
        port map (
      I0 => s_axi_CRTLS_BREADY,
      I1 => \^out\(2),
      I2 => \^out\(1),
      I3 => s_axi_CRTLS_WVALID,
      I4 => \^out\(0),
      O => \FSM_onehot_wstate[3]_i_2_n_0\
    );
\FSM_onehot_wstate_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => '0',
      Q => \FSM_onehot_wstate_reg_n_0_[0]\,
      S => ARESET
    );
\FSM_onehot_wstate_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \/FSM_onehot_wstate[1]_i_1_n_0\,
      Q => \^out\(0),
      R => ARESET
    );
\FSM_onehot_wstate_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \/FSM_onehot_wstate[2]_i_1_n_0\,
      Q => \^out\(1),
      R => ARESET
    );
\FSM_onehot_wstate_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \FSM_onehot_wstate[3]_i_2_n_0\,
      Q => \^out\(2),
      R => ARESET
    );
\int_a[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(0),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(0),
      O => or0_out(0)
    );
\int_a[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(10),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(10),
      O => or0_out(10)
    );
\int_a[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(11),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(11),
      O => or0_out(11)
    );
\int_a[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(12),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(12),
      O => or0_out(12)
    );
\int_a[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(13),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(13),
      O => or0_out(13)
    );
\int_a[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(14),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(14),
      O => or0_out(14)
    );
\int_a[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(15),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(15),
      O => or0_out(15)
    );
\int_a[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(16),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(16),
      O => or0_out(16)
    );
\int_a[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(17),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(17),
      O => or0_out(17)
    );
\int_a[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(18),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(18),
      O => or0_out(18)
    );
\int_a[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(19),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(19),
      O => or0_out(19)
    );
\int_a[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(1),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(1),
      O => or0_out(1)
    );
\int_a[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(20),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(20),
      O => or0_out(20)
    );
\int_a[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(21),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(21),
      O => or0_out(21)
    );
\int_a[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(22),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(22),
      O => or0_out(22)
    );
\int_a[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(23),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => a(23),
      O => or0_out(23)
    );
\int_a[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(24),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(24),
      O => or0_out(24)
    );
\int_a[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(25),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(25),
      O => or0_out(25)
    );
\int_a[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(26),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(26),
      O => or0_out(26)
    );
\int_a[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(27),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(27),
      O => or0_out(27)
    );
\int_a[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(28),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(28),
      O => or0_out(28)
    );
\int_a[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(29),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(29),
      O => or0_out(29)
    );
\int_a[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(2),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(2),
      O => or0_out(2)
    );
\int_a[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(30),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(30),
      O => or0_out(30)
    );
\int_a[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => \waddr_reg_n_0_[4]\,
      I1 => \waddr_reg_n_0_[5]\,
      I2 => \waddr_reg_n_0_[3]\,
      I3 => \waddr_reg_n_0_[2]\,
      I4 => \int_a[31]_i_3_n_0\,
      O => p_0_in10_out
    );
\int_a[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(31),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => a(31),
      O => or0_out(31)
    );
\int_a[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \waddr_reg_n_0_[0]\,
      I1 => \^out\(1),
      I2 => s_axi_CRTLS_WVALID,
      I3 => \waddr_reg_n_0_[1]\,
      O => \int_a[31]_i_3_n_0\
    );
\int_a[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(3),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(3),
      O => or0_out(3)
    );
\int_a[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(4),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(4),
      O => or0_out(4)
    );
\int_a[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(5),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(5),
      O => or0_out(5)
    );
\int_a[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(6),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(6),
      O => or0_out(6)
    );
\int_a[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(7),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => a(7),
      O => or0_out(7)
    );
\int_a[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(8),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(8),
      O => or0_out(8)
    );
\int_a[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(9),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => a(9),
      O => or0_out(9)
    );
\int_a_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(0),
      Q => a(0),
      R => '0'
    );
\int_a_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(10),
      Q => a(10),
      R => '0'
    );
\int_a_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(11),
      Q => a(11),
      R => '0'
    );
\int_a_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(12),
      Q => a(12),
      R => '0'
    );
\int_a_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(13),
      Q => a(13),
      R => '0'
    );
\int_a_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(14),
      Q => a(14),
      R => '0'
    );
\int_a_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(15),
      Q => a(15),
      R => '0'
    );
\int_a_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(16),
      Q => a(16),
      R => '0'
    );
\int_a_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(17),
      Q => a(17),
      R => '0'
    );
\int_a_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(18),
      Q => a(18),
      R => '0'
    );
\int_a_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(19),
      Q => a(19),
      R => '0'
    );
\int_a_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(1),
      Q => a(1),
      R => '0'
    );
\int_a_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(20),
      Q => a(20),
      R => '0'
    );
\int_a_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(21),
      Q => a(21),
      R => '0'
    );
\int_a_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(22),
      Q => a(22),
      R => '0'
    );
\int_a_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(23),
      Q => a(23),
      R => '0'
    );
\int_a_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(24),
      Q => a(24),
      R => '0'
    );
\int_a_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(25),
      Q => a(25),
      R => '0'
    );
\int_a_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(26),
      Q => a(26),
      R => '0'
    );
\int_a_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(27),
      Q => a(27),
      R => '0'
    );
\int_a_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(28),
      Q => a(28),
      R => '0'
    );
\int_a_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(29),
      Q => a(29),
      R => '0'
    );
\int_a_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(2),
      Q => a(2),
      R => '0'
    );
\int_a_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(30),
      Q => a(30),
      R => '0'
    );
\int_a_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(31),
      Q => a(31),
      R => '0'
    );
\int_a_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(3),
      Q => a(3),
      R => '0'
    );
\int_a_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(4),
      Q => a(4),
      R => '0'
    );
\int_a_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(5),
      Q => a(5),
      R => '0'
    );
\int_a_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(6),
      Q => a(6),
      R => '0'
    );
\int_a_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(7),
      Q => a(7),
      R => '0'
    );
\int_a_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(8),
      Q => a(8),
      R => '0'
    );
\int_a_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in10_out,
      D => or0_out(9),
      Q => a(9),
      R => '0'
    );
int_ap_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFFFFFF0000"
    )
        port map (
      I0 => rstate(1),
      I1 => rstate(0),
      I2 => s_axi_CRTLS_ARVALID,
      I3 => int_ap_done_i_2_n_0,
      I4 => ap_done,
      I5 => int_ap_done,
      O => int_ap_done_i_1_n_0
    );
int_ap_done_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(4),
      I1 => s_axi_CRTLS_ARADDR(5),
      I2 => s_axi_CRTLS_ARADDR(2),
      I3 => s_axi_CRTLS_ARADDR(3),
      I4 => s_axi_CRTLS_ARADDR(0),
      I5 => s_axi_CRTLS_ARADDR(1),
      O => int_ap_done_i_2_n_0
    );
int_ap_done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_done_i_1_n_0,
      Q => int_ap_done,
      R => ARESET
    );
int_ap_idle_reg: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => '1',
      Q => int_ap_idle,
      R => ARESET
    );
int_ap_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => ap_done,
      Q => int_ap_ready,
      R => ARESET
    );
\int_ap_return[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(11),
      I1 => a(11),
      O => \int_ap_return[11]_i_2_n_0\
    );
\int_ap_return[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(10),
      I1 => a(10),
      O => \int_ap_return[11]_i_3_n_0\
    );
\int_ap_return[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(9),
      I1 => a(9),
      O => \int_ap_return[11]_i_4_n_0\
    );
\int_ap_return[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(8),
      I1 => a(8),
      O => \int_ap_return[11]_i_5_n_0\
    );
\int_ap_return[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(15),
      I1 => a(15),
      O => \int_ap_return[15]_i_2_n_0\
    );
\int_ap_return[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(14),
      I1 => a(14),
      O => \int_ap_return[15]_i_3_n_0\
    );
\int_ap_return[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(13),
      I1 => a(13),
      O => \int_ap_return[15]_i_4_n_0\
    );
\int_ap_return[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(12),
      I1 => a(12),
      O => \int_ap_return[15]_i_5_n_0\
    );
\int_ap_return[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(19),
      I1 => a(19),
      O => \int_ap_return[19]_i_2_n_0\
    );
\int_ap_return[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(18),
      I1 => a(18),
      O => \int_ap_return[19]_i_3_n_0\
    );
\int_ap_return[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(17),
      I1 => a(17),
      O => \int_ap_return[19]_i_4_n_0\
    );
\int_ap_return[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(16),
      I1 => a(16),
      O => \int_ap_return[19]_i_5_n_0\
    );
\int_ap_return[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(23),
      I1 => a(23),
      O => \int_ap_return[23]_i_2_n_0\
    );
\int_ap_return[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(22),
      I1 => a(22),
      O => \int_ap_return[23]_i_3_n_0\
    );
\int_ap_return[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(21),
      I1 => a(21),
      O => \int_ap_return[23]_i_4_n_0\
    );
\int_ap_return[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(20),
      I1 => a(20),
      O => \int_ap_return[23]_i_5_n_0\
    );
\int_ap_return[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(27),
      I1 => a(27),
      O => \int_ap_return[27]_i_2_n_0\
    );
\int_ap_return[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(26),
      I1 => a(26),
      O => \int_ap_return[27]_i_3_n_0\
    );
\int_ap_return[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(25),
      I1 => a(25),
      O => \int_ap_return[27]_i_4_n_0\
    );
\int_ap_return[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(24),
      I1 => a(24),
      O => \int_ap_return[27]_i_5_n_0\
    );
\int_ap_return[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(31),
      I1 => a(31),
      O => \int_ap_return[31]_i_2_n_0\
    );
\int_ap_return[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(30),
      I1 => a(30),
      O => \int_ap_return[31]_i_3_n_0\
    );
\int_ap_return[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(29),
      I1 => a(29),
      O => \int_ap_return[31]_i_4_n_0\
    );
\int_ap_return[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(28),
      I1 => a(28),
      O => \int_ap_return[31]_i_5_n_0\
    );
\int_ap_return[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(3),
      I1 => a(3),
      O => \int_ap_return[3]_i_2_n_0\
    );
\int_ap_return[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(2),
      I1 => a(2),
      O => \int_ap_return[3]_i_3_n_0\
    );
\int_ap_return[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(1),
      I1 => a(1),
      O => \int_ap_return[3]_i_4_n_0\
    );
\int_ap_return[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(0),
      I1 => a(0),
      O => \int_ap_return[3]_i_5_n_0\
    );
\int_ap_return[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(7),
      I1 => a(7),
      O => \int_ap_return[7]_i_2_n_0\
    );
\int_ap_return[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(6),
      I1 => a(6),
      O => \int_ap_return[7]_i_3_n_0\
    );
\int_ap_return[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(5),
      I1 => a(5),
      O => \int_ap_return[7]_i_4_n_0\
    );
\int_ap_return[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => b(4),
      I1 => a(4),
      O => \int_ap_return[7]_i_5_n_0\
    );
\int_ap_return_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(0),
      Q => int_ap_return(0),
      R => ARESET
    );
\int_ap_return_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(10),
      Q => int_ap_return(10),
      R => ARESET
    );
\int_ap_return_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(11),
      Q => int_ap_return(11),
      R => ARESET
    );
\int_ap_return_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[7]_i_1_n_0\,
      CO(3) => \int_ap_return_reg[11]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[11]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[11]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(11 downto 8),
      O(3 downto 0) => ap_return(11 downto 8),
      S(3) => \int_ap_return[11]_i_2_n_0\,
      S(2) => \int_ap_return[11]_i_3_n_0\,
      S(1) => \int_ap_return[11]_i_4_n_0\,
      S(0) => \int_ap_return[11]_i_5_n_0\
    );
\int_ap_return_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(12),
      Q => int_ap_return(12),
      R => ARESET
    );
\int_ap_return_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(13),
      Q => int_ap_return(13),
      R => ARESET
    );
\int_ap_return_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(14),
      Q => int_ap_return(14),
      R => ARESET
    );
\int_ap_return_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(15),
      Q => int_ap_return(15),
      R => ARESET
    );
\int_ap_return_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[11]_i_1_n_0\,
      CO(3) => \int_ap_return_reg[15]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[15]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[15]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(15 downto 12),
      O(3 downto 0) => ap_return(15 downto 12),
      S(3) => \int_ap_return[15]_i_2_n_0\,
      S(2) => \int_ap_return[15]_i_3_n_0\,
      S(1) => \int_ap_return[15]_i_4_n_0\,
      S(0) => \int_ap_return[15]_i_5_n_0\
    );
\int_ap_return_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(16),
      Q => int_ap_return(16),
      R => ARESET
    );
\int_ap_return_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(17),
      Q => int_ap_return(17),
      R => ARESET
    );
\int_ap_return_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(18),
      Q => int_ap_return(18),
      R => ARESET
    );
\int_ap_return_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(19),
      Q => int_ap_return(19),
      R => ARESET
    );
\int_ap_return_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[15]_i_1_n_0\,
      CO(3) => \int_ap_return_reg[19]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[19]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[19]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(19 downto 16),
      O(3 downto 0) => ap_return(19 downto 16),
      S(3) => \int_ap_return[19]_i_2_n_0\,
      S(2) => \int_ap_return[19]_i_3_n_0\,
      S(1) => \int_ap_return[19]_i_4_n_0\,
      S(0) => \int_ap_return[19]_i_5_n_0\
    );
\int_ap_return_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(1),
      Q => int_ap_return(1),
      R => ARESET
    );
\int_ap_return_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(20),
      Q => int_ap_return(20),
      R => ARESET
    );
\int_ap_return_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(21),
      Q => int_ap_return(21),
      R => ARESET
    );
\int_ap_return_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(22),
      Q => int_ap_return(22),
      R => ARESET
    );
\int_ap_return_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(23),
      Q => int_ap_return(23),
      R => ARESET
    );
\int_ap_return_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[19]_i_1_n_0\,
      CO(3) => \int_ap_return_reg[23]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[23]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[23]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(23 downto 20),
      O(3 downto 0) => ap_return(23 downto 20),
      S(3) => \int_ap_return[23]_i_2_n_0\,
      S(2) => \int_ap_return[23]_i_3_n_0\,
      S(1) => \int_ap_return[23]_i_4_n_0\,
      S(0) => \int_ap_return[23]_i_5_n_0\
    );
\int_ap_return_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(24),
      Q => int_ap_return(24),
      R => ARESET
    );
\int_ap_return_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(25),
      Q => int_ap_return(25),
      R => ARESET
    );
\int_ap_return_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(26),
      Q => int_ap_return(26),
      R => ARESET
    );
\int_ap_return_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(27),
      Q => int_ap_return(27),
      R => ARESET
    );
\int_ap_return_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[23]_i_1_n_0\,
      CO(3) => \int_ap_return_reg[27]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[27]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[27]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(27 downto 24),
      O(3 downto 0) => ap_return(27 downto 24),
      S(3) => \int_ap_return[27]_i_2_n_0\,
      S(2) => \int_ap_return[27]_i_3_n_0\,
      S(1) => \int_ap_return[27]_i_4_n_0\,
      S(0) => \int_ap_return[27]_i_5_n_0\
    );
\int_ap_return_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(28),
      Q => int_ap_return(28),
      R => ARESET
    );
\int_ap_return_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(29),
      Q => int_ap_return(29),
      R => ARESET
    );
\int_ap_return_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(2),
      Q => int_ap_return(2),
      R => ARESET
    );
\int_ap_return_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(30),
      Q => int_ap_return(30),
      R => ARESET
    );
\int_ap_return_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(31),
      Q => int_ap_return(31),
      R => ARESET
    );
\int_ap_return_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[27]_i_1_n_0\,
      CO(3) => \NLW_int_ap_return_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \int_ap_return_reg[31]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[31]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => b(30 downto 28),
      O(3 downto 0) => ap_return(31 downto 28),
      S(3) => \int_ap_return[31]_i_2_n_0\,
      S(2) => \int_ap_return[31]_i_3_n_0\,
      S(1) => \int_ap_return[31]_i_4_n_0\,
      S(0) => \int_ap_return[31]_i_5_n_0\
    );
\int_ap_return_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(3),
      Q => int_ap_return(3),
      R => ARESET
    );
\int_ap_return_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \int_ap_return_reg[3]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[3]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[3]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(3 downto 0),
      O(3 downto 0) => ap_return(3 downto 0),
      S(3) => \int_ap_return[3]_i_2_n_0\,
      S(2) => \int_ap_return[3]_i_3_n_0\,
      S(1) => \int_ap_return[3]_i_4_n_0\,
      S(0) => \int_ap_return[3]_i_5_n_0\
    );
\int_ap_return_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(4),
      Q => int_ap_return(4),
      R => ARESET
    );
\int_ap_return_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(5),
      Q => int_ap_return(5),
      R => ARESET
    );
\int_ap_return_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(6),
      Q => int_ap_return(6),
      R => ARESET
    );
\int_ap_return_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(7),
      Q => int_ap_return(7),
      R => ARESET
    );
\int_ap_return_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \int_ap_return_reg[3]_i_1_n_0\,
      CO(3) => \int_ap_return_reg[7]_i_1_n_0\,
      CO(2) => \int_ap_return_reg[7]_i_1_n_1\,
      CO(1) => \int_ap_return_reg[7]_i_1_n_2\,
      CO(0) => \int_ap_return_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(7 downto 4),
      O(3 downto 0) => ap_return(7 downto 4),
      S(3) => \int_ap_return[7]_i_2_n_0\,
      S(2) => \int_ap_return[7]_i_3_n_0\,
      S(1) => \int_ap_return[7]_i_4_n_0\,
      S(0) => \int_ap_return[7]_i_5_n_0\
    );
\int_ap_return_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(8),
      Q => int_ap_return(8),
      R => ARESET
    );
\int_ap_return_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_done,
      D => ap_return(9),
      Q => int_ap_return(9),
      R => ARESET
    );
int_ap_start_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => ap_done,
      I1 => p_5_in(7),
      I2 => s_axi_CRTLS_WDATA(0),
      I3 => int_ap_start1,
      O => int_ap_start_i_1_n_0
    );
int_ap_start_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => \waddr_reg_n_0_[4]\,
      I1 => \waddr_reg_n_0_[5]\,
      I2 => \waddr_reg_n_0_[2]\,
      I3 => \waddr_reg_n_0_[3]\,
      I4 => int_isr_i_2_n_0,
      O => int_ap_start1
    );
int_ap_start_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ap_start_i_1_n_0,
      Q => ap_done,
      R => ARESET
    );
int_auto_restart_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(7),
      I1 => int_auto_restart_i_2_n_0,
      I2 => \waddr_reg_n_0_[2]\,
      I3 => \waddr_reg_n_0_[3]\,
      I4 => int_isr_i_2_n_0,
      I5 => p_5_in(7),
      O => int_auto_restart_i_1_n_0
    );
int_auto_restart_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \waddr_reg_n_0_[4]\,
      I1 => \waddr_reg_n_0_[5]\,
      O => int_auto_restart_i_2_n_0
    );
int_auto_restart_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_auto_restart_i_1_n_0,
      Q => p_5_in(7),
      R => ARESET
    );
\int_b[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(0),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(0),
      O => \or\(0)
    );
\int_b[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(10),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(10),
      O => \or\(10)
    );
\int_b[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(11),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(11),
      O => \or\(11)
    );
\int_b[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(12),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(12),
      O => \or\(12)
    );
\int_b[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(13),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(13),
      O => \or\(13)
    );
\int_b[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(14),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(14),
      O => \or\(14)
    );
\int_b[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(15),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(15),
      O => \or\(15)
    );
\int_b[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(16),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(16),
      O => \or\(16)
    );
\int_b[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(17),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(17),
      O => \or\(17)
    );
\int_b[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(18),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(18),
      O => \or\(18)
    );
\int_b[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(19),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(19),
      O => \or\(19)
    );
\int_b[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(1),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(1),
      O => \or\(1)
    );
\int_b[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(20),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(20),
      O => \or\(20)
    );
\int_b[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(21),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(21),
      O => \or\(21)
    );
\int_b[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(22),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(22),
      O => \or\(22)
    );
\int_b[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(23),
      I1 => s_axi_CRTLS_WSTRB(2),
      I2 => b(23),
      O => \or\(23)
    );
\int_b[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(24),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(24),
      O => \or\(24)
    );
\int_b[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(25),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(25),
      O => \or\(25)
    );
\int_b[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(26),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(26),
      O => \or\(26)
    );
\int_b[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(27),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(27),
      O => \or\(27)
    );
\int_b[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(28),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(28),
      O => \or\(28)
    );
\int_b[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(29),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(29),
      O => \or\(29)
    );
\int_b[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(2),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(2),
      O => \or\(2)
    );
\int_b[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(30),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(30),
      O => \or\(30)
    );
\int_b[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \waddr_reg_n_0_[5]\,
      I1 => \waddr_reg_n_0_[4]\,
      I2 => \waddr_reg_n_0_[2]\,
      I3 => \waddr_reg_n_0_[3]\,
      I4 => \int_a[31]_i_3_n_0\,
      O => p_0_in
    );
\int_b[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(31),
      I1 => s_axi_CRTLS_WSTRB(3),
      I2 => b(31),
      O => \or\(31)
    );
\int_b[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(3),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(3),
      O => \or\(3)
    );
\int_b[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(4),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(4),
      O => \or\(4)
    );
\int_b[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(5),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(5),
      O => \or\(5)
    );
\int_b[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(6),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(6),
      O => \or\(6)
    );
\int_b[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(7),
      I1 => s_axi_CRTLS_WSTRB(0),
      I2 => b(7),
      O => \or\(7)
    );
\int_b[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(8),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(8),
      O => \or\(8)
    );
\int_b[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(9),
      I1 => s_axi_CRTLS_WSTRB(1),
      I2 => b(9),
      O => \or\(9)
    );
\int_b_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(0),
      Q => b(0),
      R => '0'
    );
\int_b_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(10),
      Q => b(10),
      R => '0'
    );
\int_b_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(11),
      Q => b(11),
      R => '0'
    );
\int_b_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(12),
      Q => b(12),
      R => '0'
    );
\int_b_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(13),
      Q => b(13),
      R => '0'
    );
\int_b_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(14),
      Q => b(14),
      R => '0'
    );
\int_b_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(15),
      Q => b(15),
      R => '0'
    );
\int_b_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(16),
      Q => b(16),
      R => '0'
    );
\int_b_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(17),
      Q => b(17),
      R => '0'
    );
\int_b_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(18),
      Q => b(18),
      R => '0'
    );
\int_b_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(19),
      Q => b(19),
      R => '0'
    );
\int_b_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(1),
      Q => b(1),
      R => '0'
    );
\int_b_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(20),
      Q => b(20),
      R => '0'
    );
\int_b_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(21),
      Q => b(21),
      R => '0'
    );
\int_b_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(22),
      Q => b(22),
      R => '0'
    );
\int_b_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(23),
      Q => b(23),
      R => '0'
    );
\int_b_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(24),
      Q => b(24),
      R => '0'
    );
\int_b_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(25),
      Q => b(25),
      R => '0'
    );
\int_b_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(26),
      Q => b(26),
      R => '0'
    );
\int_b_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(27),
      Q => b(27),
      R => '0'
    );
\int_b_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(28),
      Q => b(28),
      R => '0'
    );
\int_b_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(29),
      Q => b(29),
      R => '0'
    );
\int_b_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(2),
      Q => b(2),
      R => '0'
    );
\int_b_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(30),
      Q => b(30),
      R => '0'
    );
\int_b_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(31),
      Q => b(31),
      R => '0'
    );
\int_b_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(3),
      Q => b(3),
      R => '0'
    );
\int_b_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(4),
      Q => b(4),
      R => '0'
    );
\int_b_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(5),
      Q => b(5),
      R => '0'
    );
\int_b_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(6),
      Q => b(6),
      R => '0'
    );
\int_b_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(7),
      Q => b(7),
      R => '0'
    );
\int_b_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(8),
      Q => b(8),
      R => '0'
    );
\int_b_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => p_0_in,
      D => \or\(9),
      Q => b(9),
      R => '0'
    );
int_gie_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(0),
      I1 => int_auto_restart_i_2_n_0,
      I2 => \waddr_reg_n_0_[2]\,
      I3 => \waddr_reg_n_0_[3]\,
      I4 => int_isr_i_2_n_0,
      I5 => int_gie_reg_n_0,
      O => int_gie_i_1_n_0
    );
int_gie_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_gie_i_1_n_0,
      Q => int_gie_reg_n_0,
      R => ARESET
    );
int_ier_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(0),
      I1 => int_auto_restart_i_2_n_0,
      I2 => \waddr_reg_n_0_[3]\,
      I3 => \waddr_reg_n_0_[2]\,
      I4 => int_isr_i_2_n_0,
      I5 => int_ier,
      O => int_ier_i_1_n_0
    );
int_ier_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_ier_i_1_n_0,
      Q => int_ier,
      R => ARESET
    );
int_isr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7F7F7FFF808080"
    )
        port map (
      I0 => s_axi_CRTLS_WDATA(0),
      I1 => int_isr_i_2_n_0,
      I2 => int_isr_i_3_n_0,
      I3 => ap_done,
      I4 => int_ier,
      I5 => int_isr_reg_n_0,
      O => int_isr_i_1_n_0
    );
int_isr_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00400000"
    )
        port map (
      I0 => \waddr_reg_n_0_[1]\,
      I1 => s_axi_CRTLS_WVALID,
      I2 => \^out\(1),
      I3 => \waddr_reg_n_0_[0]\,
      I4 => s_axi_CRTLS_WSTRB(0),
      O => int_isr_i_2_n_0
    );
int_isr_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => \waddr_reg_n_0_[3]\,
      I1 => \waddr_reg_n_0_[2]\,
      I2 => \waddr_reg_n_0_[5]\,
      I3 => \waddr_reg_n_0_[4]\,
      O => int_isr_i_3_n_0
    );
int_isr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => int_isr_i_1_n_0,
      Q => int_isr_reg_n_0,
      R => ARESET
    );
interrupt_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => int_isr_reg_n_0,
      I1 => int_gie_reg_n_0,
      O => interrupt
    );
\rdata_data[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAFCFA0C0AFC0A0"
    )
        port map (
      I0 => int_gie_reg_n_0,
      I1 => int_isr_reg_n_0,
      I2 => \rdata_data[0]_i_4_n_0\,
      I3 => \rdata_data[0]_i_5_n_0\,
      I4 => ap_done,
      I5 => int_ier,
      O => \rdata_data[0]_i_2_n_0\
    );
\rdata_data[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(0),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(0),
      I4 => a(0),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[0]_i_3_n_0\
    );
\rdata_data[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFE8"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(4),
      I1 => s_axi_CRTLS_ARADDR(5),
      I2 => s_axi_CRTLS_ARADDR(3),
      I3 => s_axi_CRTLS_ARADDR(2),
      I4 => s_axi_CRTLS_ARADDR(1),
      I5 => s_axi_CRTLS_ARADDR(0),
      O => \rdata_data[0]_i_4_n_0\
    );
\rdata_data[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFE2"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(3),
      I1 => s_axi_CRTLS_ARADDR(4),
      I2 => s_axi_CRTLS_ARADDR(2),
      I3 => s_axi_CRTLS_ARADDR(1),
      I4 => s_axi_CRTLS_ARADDR(0),
      I5 => s_axi_CRTLS_ARADDR(5),
      O => \rdata_data[0]_i_5_n_0\
    );
\rdata_data[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(10),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(10),
      I4 => a(10),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[10]_i_1_n_0\
    );
\rdata_data[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(11),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(11),
      I4 => a(11),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[11]_i_1_n_0\
    );
\rdata_data[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(12),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(12),
      I4 => a(12),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[12]_i_1_n_0\
    );
\rdata_data[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(13),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(13),
      I4 => a(13),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[13]_i_1_n_0\
    );
\rdata_data[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(14),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(14),
      I4 => a(14),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[14]_i_1_n_0\
    );
\rdata_data[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(15),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(15),
      I4 => a(15),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[15]_i_1_n_0\
    );
\rdata_data[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(16),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(16),
      I4 => a(16),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[16]_i_1_n_0\
    );
\rdata_data[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(17),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(17),
      I4 => a(17),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[17]_i_1_n_0\
    );
\rdata_data[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(18),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(18),
      I4 => a(18),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[18]_i_1_n_0\
    );
\rdata_data[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(19),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(19),
      I4 => a(19),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[19]_i_1_n_0\
    );
\rdata_data[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAC0"
    )
        port map (
      I0 => \rdata_data[1]_i_2_n_0\,
      I1 => int_ap_done,
      I2 => \rdata_data[31]_i_4_n_0\,
      I3 => \rdata_data[7]_i_3_n_0\,
      O => rdata_data(1)
    );
\rdata_data[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(1),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(1),
      I4 => a(1),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[1]_i_2_n_0\
    );
\rdata_data[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(20),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(20),
      I4 => a(20),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[20]_i_1_n_0\
    );
\rdata_data[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(21),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(21),
      I4 => a(21),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[21]_i_1_n_0\
    );
\rdata_data[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(22),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(22),
      I4 => a(22),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[22]_i_1_n_0\
    );
\rdata_data[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(23),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(23),
      I4 => a(23),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[23]_i_1_n_0\
    );
\rdata_data[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(24),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(24),
      I4 => a(24),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[24]_i_1_n_0\
    );
\rdata_data[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(25),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(25),
      I4 => a(25),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[25]_i_1_n_0\
    );
\rdata_data[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(26),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(26),
      I4 => a(26),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[26]_i_1_n_0\
    );
\rdata_data[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(27),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(27),
      I4 => a(27),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[27]_i_1_n_0\
    );
\rdata_data[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(28),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(28),
      I4 => a(28),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[28]_i_1_n_0\
    );
\rdata_data[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(29),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(29),
      I4 => a(29),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[29]_i_1_n_0\
    );
\rdata_data[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAC0"
    )
        port map (
      I0 => \rdata_data[2]_i_2_n_0\,
      I1 => int_ap_idle,
      I2 => \rdata_data[31]_i_4_n_0\,
      I3 => \rdata_data[7]_i_3_n_0\,
      O => rdata_data(2)
    );
\rdata_data[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(2),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(2),
      I4 => a(2),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[2]_i_2_n_0\
    );
\rdata_data[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(30),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(30),
      I4 => a(30),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[30]_i_1_n_0\
    );
\rdata_data[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => ar_hs,
      I1 => s_axi_CRTLS_ARADDR(4),
      I2 => s_axi_CRTLS_ARADDR(5),
      I3 => s_axi_CRTLS_ARADDR(1),
      I4 => s_axi_CRTLS_ARADDR(0),
      O => \rdata_data[31]_i_1_n_0\
    );
\rdata_data[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => rstate(1),
      I1 => rstate(0),
      I2 => s_axi_CRTLS_ARVALID,
      O => ar_hs
    );
\rdata_data[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(31),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(31),
      I4 => a(31),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[31]_i_3_n_0\
    );
\rdata_data[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(0),
      I1 => s_axi_CRTLS_ARADDR(1),
      I2 => s_axi_CRTLS_ARADDR(2),
      I3 => s_axi_CRTLS_ARADDR(3),
      I4 => s_axi_CRTLS_ARADDR(5),
      O => \rdata_data[31]_i_4_n_0\
    );
\rdata_data[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010100"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(0),
      I1 => s_axi_CRTLS_ARADDR(1),
      I2 => s_axi_CRTLS_ARADDR(2),
      I3 => s_axi_CRTLS_ARADDR(3),
      I4 => s_axi_CRTLS_ARADDR(5),
      I5 => s_axi_CRTLS_ARADDR(4),
      O => \rdata_data[31]_i_5_n_0\
    );
\rdata_data[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000100"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(5),
      I1 => s_axi_CRTLS_ARADDR(0),
      I2 => s_axi_CRTLS_ARADDR(1),
      I3 => s_axi_CRTLS_ARADDR(2),
      I4 => s_axi_CRTLS_ARADDR(4),
      I5 => s_axi_CRTLS_ARADDR(3),
      O => \rdata_data[31]_i_6_n_0\
    );
\rdata_data[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAC0"
    )
        port map (
      I0 => \rdata_data[3]_i_2_n_0\,
      I1 => int_ap_ready,
      I2 => \rdata_data[31]_i_4_n_0\,
      I3 => \rdata_data[7]_i_3_n_0\,
      O => rdata_data(3)
    );
\rdata_data[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(3),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(3),
      I4 => a(3),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[3]_i_2_n_0\
    );
\rdata_data[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(4),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(4),
      I4 => a(4),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[4]_i_1_n_0\
    );
\rdata_data[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(5),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(5),
      I4 => a(5),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[5]_i_1_n_0\
    );
\rdata_data[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(6),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(6),
      I4 => a(6),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[6]_i_1_n_0\
    );
\rdata_data[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAC0"
    )
        port map (
      I0 => \rdata_data[7]_i_2_n_0\,
      I1 => p_5_in(7),
      I2 => \rdata_data[31]_i_4_n_0\,
      I3 => \rdata_data[7]_i_3_n_0\,
      O => rdata_data(7)
    );
\rdata_data[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(7),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(7),
      I4 => a(7),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[7]_i_2_n_0\
    );
\rdata_data[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => s_axi_CRTLS_ARADDR(0),
      I1 => s_axi_CRTLS_ARADDR(1),
      I2 => s_axi_CRTLS_ARADDR(5),
      I3 => s_axi_CRTLS_ARADDR(4),
      O => \rdata_data[7]_i_3_n_0\
    );
\rdata_data[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(8),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(8),
      I4 => a(8),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[8]_i_1_n_0\
    );
\rdata_data[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rdata_data[31]_i_4_n_0\,
      I1 => int_ap_return(9),
      I2 => \rdata_data[31]_i_5_n_0\,
      I3 => b(9),
      I4 => a(9),
      I5 => \rdata_data[31]_i_6_n_0\,
      O => \rdata_data[9]_i_1_n_0\
    );
\rdata_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(0),
      Q => s_axi_CRTLS_RDATA(0),
      R => '0'
    );
\rdata_data_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \rdata_data[0]_i_2_n_0\,
      I1 => \rdata_data[0]_i_3_n_0\,
      O => rdata_data(0),
      S => \rdata_data[7]_i_3_n_0\
    );
\rdata_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[10]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(10),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[11]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(11),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[12]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(12),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[13]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(13),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[14]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(14),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[15]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(15),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[16]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(16),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[17]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(17),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[18]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(18),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[19]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(19),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(1),
      Q => s_axi_CRTLS_RDATA(1),
      R => '0'
    );
\rdata_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[20]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(20),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[21]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(21),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[22]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(22),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[23]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(23),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[24]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(24),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[25]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(25),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[26]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(26),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[27]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(27),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[28]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(28),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[29]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(29),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(2),
      Q => s_axi_CRTLS_RDATA(2),
      R => '0'
    );
\rdata_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[30]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(30),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[31]_i_3_n_0\,
      Q => s_axi_CRTLS_RDATA(31),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(3),
      Q => s_axi_CRTLS_RDATA(3),
      R => '0'
    );
\rdata_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[4]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(4),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[5]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(5),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[6]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(6),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => rdata_data(7),
      Q => s_axi_CRTLS_RDATA(7),
      R => '0'
    );
\rdata_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[8]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(8),
      R => \rdata_data[31]_i_1_n_0\
    );
\rdata_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ar_hs,
      D => \rdata_data[9]_i_1_n_0\,
      Q => s_axi_CRTLS_RDATA(9),
      R => \rdata_data[31]_i_1_n_0\
    );
\rstate[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"005C"
    )
        port map (
      I0 => s_axi_CRTLS_RREADY,
      I1 => s_axi_CRTLS_ARVALID,
      I2 => rstate(0),
      I3 => rstate(1),
      O => \rstate[0]_i_1_n_0\
    );
\rstate_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \rstate[0]_i_1_n_0\,
      Q => rstate(0),
      R => ARESET
    );
\rstate_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => '0',
      Q => rstate(1),
      S => ARESET
    );
s_axi_CRTLS_ARREADY_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rstate(1),
      I1 => rstate(0),
      O => s_axi_CRTLS_ARREADY
    );
s_axi_CRTLS_RVALID_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rstate(0),
      I1 => rstate(1),
      O => s_axi_CRTLS_RVALID
    );
\waddr[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_CRTLS_AWVALID,
      I1 => \^out\(0),
      O => waddr
    );
\waddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CRTLS_AWADDR(0),
      Q => \waddr_reg_n_0_[0]\,
      R => '0'
    );
\waddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CRTLS_AWADDR(1),
      Q => \waddr_reg_n_0_[1]\,
      R => '0'
    );
\waddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CRTLS_AWADDR(2),
      Q => \waddr_reg_n_0_[2]\,
      R => '0'
    );
\waddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CRTLS_AWADDR(3),
      Q => \waddr_reg_n_0_[3]\,
      R => '0'
    );
\waddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CRTLS_AWADDR(4),
      Q => \waddr_reg_n_0_[4]\,
      R => '0'
    );
\waddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => waddr,
      D => s_axi_CRTLS_AWADDR(5),
      Q => \waddr_reg_n_0_[5]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum is
  port (
    s_axi_CRTLS_AWVALID : in STD_LOGIC;
    s_axi_CRTLS_AWREADY : out STD_LOGIC;
    s_axi_CRTLS_AWADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CRTLS_WVALID : in STD_LOGIC;
    s_axi_CRTLS_WREADY : out STD_LOGIC;
    s_axi_CRTLS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CRTLS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CRTLS_ARVALID : in STD_LOGIC;
    s_axi_CRTLS_ARREADY : out STD_LOGIC;
    s_axi_CRTLS_ARADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CRTLS_RVALID : out STD_LOGIC;
    s_axi_CRTLS_RREADY : in STD_LOGIC;
    s_axi_CRTLS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CRTLS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CRTLS_BVALID : out STD_LOGIC;
    s_axi_CRTLS_BREADY : in STD_LOGIC;
    s_axi_CRTLS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    interrupt : out STD_LOGIC
  );
  attribute C_S_AXI_CRTLS_ADDR_WIDTH : integer;
  attribute C_S_AXI_CRTLS_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum : entity is 6;
  attribute C_S_AXI_CRTLS_DATA_WIDTH : integer;
  attribute C_S_AXI_CRTLS_DATA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum : entity is 32;
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum is
  signal \<const0>\ : STD_LOGIC;
begin
  s_axi_CRTLS_BRESP(1) <= \<const0>\;
  s_axi_CRTLS_BRESP(0) <= \<const0>\;
  s_axi_CRTLS_RRESP(1) <= \<const0>\;
  s_axi_CRTLS_RRESP(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
sum_CRTLS_s_axi_U: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum_CRTLS_s_axi
     port map (
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      interrupt => interrupt,
      \out\(2) => s_axi_CRTLS_BVALID,
      \out\(1) => s_axi_CRTLS_WREADY,
      \out\(0) => s_axi_CRTLS_AWREADY,
      s_axi_CRTLS_ARADDR(5 downto 0) => s_axi_CRTLS_ARADDR(5 downto 0),
      s_axi_CRTLS_ARREADY => s_axi_CRTLS_ARREADY,
      s_axi_CRTLS_ARVALID => s_axi_CRTLS_ARVALID,
      s_axi_CRTLS_AWADDR(5 downto 0) => s_axi_CRTLS_AWADDR(5 downto 0),
      s_axi_CRTLS_AWVALID => s_axi_CRTLS_AWVALID,
      s_axi_CRTLS_BREADY => s_axi_CRTLS_BREADY,
      s_axi_CRTLS_RDATA(31 downto 0) => s_axi_CRTLS_RDATA(31 downto 0),
      s_axi_CRTLS_RREADY => s_axi_CRTLS_RREADY,
      s_axi_CRTLS_RVALID => s_axi_CRTLS_RVALID,
      s_axi_CRTLS_WDATA(31 downto 0) => s_axi_CRTLS_WDATA(31 downto 0),
      s_axi_CRTLS_WSTRB(3 downto 0) => s_axi_CRTLS_WSTRB(3 downto 0),
      s_axi_CRTLS_WVALID => s_axi_CRTLS_WVALID
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s_axi_CRTLS_AWADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CRTLS_AWVALID : in STD_LOGIC;
    s_axi_CRTLS_AWREADY : out STD_LOGIC;
    s_axi_CRTLS_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CRTLS_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_CRTLS_WVALID : in STD_LOGIC;
    s_axi_CRTLS_WREADY : out STD_LOGIC;
    s_axi_CRTLS_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CRTLS_BVALID : out STD_LOGIC;
    s_axi_CRTLS_BREADY : in STD_LOGIC;
    s_axi_CRTLS_ARADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_CRTLS_ARVALID : in STD_LOGIC;
    s_axi_CRTLS_ARREADY : out STD_LOGIC;
    s_axi_CRTLS_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_CRTLS_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_CRTLS_RVALID : out STD_LOGIC;
    s_axi_CRTLS_RREADY : in STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    interrupt : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "system_sum_0_0,sum,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sum,Vivado 2017.4";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_S_AXI_CRTLS_ADDR_WIDTH : integer;
  attribute C_S_AXI_CRTLS_ADDR_WIDTH of U0 : label is 6;
  attribute C_S_AXI_CRTLS_DATA_WIDTH : integer;
  attribute C_S_AXI_CRTLS_DATA_WIDTH of U0 : label is 32;
  attribute x_interface_info : string;
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CRTLS, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0";
  attribute x_interface_info of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute x_interface_parameter of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}";
  attribute x_interface_info of interrupt : signal is "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT";
  attribute x_interface_parameter of interrupt : signal is "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1";
  attribute x_interface_info of s_axi_CRTLS_ARREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS ARREADY";
  attribute x_interface_info of s_axi_CRTLS_ARVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS ARVALID";
  attribute x_interface_info of s_axi_CRTLS_AWREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS AWREADY";
  attribute x_interface_info of s_axi_CRTLS_AWVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS AWVALID";
  attribute x_interface_info of s_axi_CRTLS_BREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS BREADY";
  attribute x_interface_info of s_axi_CRTLS_BVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS BVALID";
  attribute x_interface_info of s_axi_CRTLS_RREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS RREADY";
  attribute x_interface_info of s_axi_CRTLS_RVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS RVALID";
  attribute x_interface_info of s_axi_CRTLS_WREADY : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS WREADY";
  attribute x_interface_info of s_axi_CRTLS_WVALID : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS WVALID";
  attribute x_interface_info of s_axi_CRTLS_ARADDR : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS ARADDR";
  attribute x_interface_info of s_axi_CRTLS_AWADDR : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS AWADDR";
  attribute x_interface_parameter of s_axi_CRTLS_AWADDR : signal is "XIL_INTERFACENAME s_axi_CRTLS, ADDR_WIDTH 6, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  attribute x_interface_info of s_axi_CRTLS_BRESP : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS BRESP";
  attribute x_interface_info of s_axi_CRTLS_RDATA : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS RDATA";
  attribute x_interface_info of s_axi_CRTLS_RRESP : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS RRESP";
  attribute x_interface_info of s_axi_CRTLS_WDATA : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS WDATA";
  attribute x_interface_info of s_axi_CRTLS_WSTRB : signal is "xilinx.com:interface:aximm:1.0 s_axi_CRTLS WSTRB";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sum
     port map (
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      interrupt => interrupt,
      s_axi_CRTLS_ARADDR(5 downto 0) => s_axi_CRTLS_ARADDR(5 downto 0),
      s_axi_CRTLS_ARREADY => s_axi_CRTLS_ARREADY,
      s_axi_CRTLS_ARVALID => s_axi_CRTLS_ARVALID,
      s_axi_CRTLS_AWADDR(5 downto 0) => s_axi_CRTLS_AWADDR(5 downto 0),
      s_axi_CRTLS_AWREADY => s_axi_CRTLS_AWREADY,
      s_axi_CRTLS_AWVALID => s_axi_CRTLS_AWVALID,
      s_axi_CRTLS_BREADY => s_axi_CRTLS_BREADY,
      s_axi_CRTLS_BRESP(1 downto 0) => s_axi_CRTLS_BRESP(1 downto 0),
      s_axi_CRTLS_BVALID => s_axi_CRTLS_BVALID,
      s_axi_CRTLS_RDATA(31 downto 0) => s_axi_CRTLS_RDATA(31 downto 0),
      s_axi_CRTLS_RREADY => s_axi_CRTLS_RREADY,
      s_axi_CRTLS_RRESP(1 downto 0) => s_axi_CRTLS_RRESP(1 downto 0),
      s_axi_CRTLS_RVALID => s_axi_CRTLS_RVALID,
      s_axi_CRTLS_WDATA(31 downto 0) => s_axi_CRTLS_WDATA(31 downto 0),
      s_axi_CRTLS_WREADY => s_axi_CRTLS_WREADY,
      s_axi_CRTLS_WSTRB(3 downto 0) => s_axi_CRTLS_WSTRB(3 downto 0),
      s_axi_CRTLS_WVALID => s_axi_CRTLS_WVALID
    );
end STRUCTURE;
