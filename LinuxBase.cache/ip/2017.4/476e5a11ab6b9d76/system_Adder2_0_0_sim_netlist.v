// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Fri Apr  6 15:16:00 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    LAST_STREAM_TDATA,
    LAST_STREAM_TVALID,
    LAST_STREAM_TREADY,
    LAST_STREAM_TKEEP,
    LAST_STREAM_TSTRB,
    LAST_STREAM_TUSER,
    LAST_STREAM_TLAST,
    LAST_STREAM_TID,
    LAST_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input [31:0]LAST_STREAM_TDATA;
  input LAST_STREAM_TVALID;
  output LAST_STREAM_TREADY;
  input [3:0]LAST_STREAM_TKEEP;
  input [3:0]LAST_STREAM_TSTRB;
  input [1:0]LAST_STREAM_TUSER;
  input [0:0]LAST_STREAM_TLAST;
  input [4:0]LAST_STREAM_TID;
  input [5:0]LAST_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire ARESET;
  wire Adder2_CONTROL_BUS_s_axi_U_n_1;
  wire Adder2_CONTROL_BUS_s_axi_U_n_10;
  wire Adder2_CONTROL_BUS_s_axi_U_n_8;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_0 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_0_[1] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_0 ;
  wire \INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ;
  wire \INPUT_STREAM_V_dest_V_0_state[1]_i_4_n_0 ;
  wire \INPUT_STREAM_V_dest_V_0_state[1]_i_5_n_0 ;
  wire \INPUT_STREAM_V_dest_V_0_state[1]_i_6_n_0 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_0_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_data_out;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_0 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_0 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_0;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_0;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_0 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ;
  wire LAST_STREAM_TREADY;
  wire LAST_STREAM_TVALID;
  wire [1:1]LAST_STREAM_V_data_V_0_state;
  wire \LAST_STREAM_V_data_V_0_state[0]_i_1_n_0 ;
  wire \LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ;
  wire \LAST_STREAM_V_data_V_0_state_reg_n_0_[1] ;
  wire [1:1]LAST_STREAM_V_dest_V_0_state;
  wire \LAST_STREAM_V_dest_V_0_state[0]_i_1_n_0 ;
  wire \LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ;
  wire \LAST_STREAM_V_dest_V_0_state_reg_n_0_[0] ;
  wire agg_result_a_ap_vld;
  wire \ap_CS_fsm[2]_i_2_n_0 ;
  wire ap_CS_fsm_pp0_stage0;
  wire \ap_CS_fsm_reg_n_0_[0] ;
  wire [2:1]ap_NS_fsm;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter1_reg_n_0;
  wire ap_rst_n;
  wire in1Count_1_reg_152;
  wire \in1Count_1_reg_152[0]_i_1_n_0 ;
  wire \in1Count_1_reg_152[10]_i_1_n_0 ;
  wire \in1Count_1_reg_152[11]_i_1_n_0 ;
  wire \in1Count_1_reg_152[12]_i_1_n_0 ;
  wire \in1Count_1_reg_152[13]_i_1_n_0 ;
  wire \in1Count_1_reg_152[14]_i_1_n_0 ;
  wire \in1Count_1_reg_152[15]_i_1_n_0 ;
  wire \in1Count_1_reg_152[16]_i_1_n_0 ;
  wire \in1Count_1_reg_152[17]_i_1_n_0 ;
  wire \in1Count_1_reg_152[18]_i_1_n_0 ;
  wire \in1Count_1_reg_152[19]_i_2_n_0 ;
  wire \in1Count_1_reg_152[19]_i_3_n_0 ;
  wire \in1Count_1_reg_152[1]_i_1_n_0 ;
  wire \in1Count_1_reg_152[2]_i_1_n_0 ;
  wire \in1Count_1_reg_152[3]_i_1_n_0 ;
  wire \in1Count_1_reg_152[4]_i_1_n_0 ;
  wire \in1Count_1_reg_152[5]_i_1_n_0 ;
  wire \in1Count_1_reg_152[6]_i_1_n_0 ;
  wire \in1Count_1_reg_152[7]_i_1_n_0 ;
  wire \in1Count_1_reg_152[8]_i_1_n_0 ;
  wire \in1Count_1_reg_152[9]_i_1_n_0 ;
  wire \in1Count_1_reg_152_reg_n_0_[0] ;
  wire \in1Count_1_reg_152_reg_n_0_[10] ;
  wire \in1Count_1_reg_152_reg_n_0_[11] ;
  wire \in1Count_1_reg_152_reg_n_0_[12] ;
  wire \in1Count_1_reg_152_reg_n_0_[13] ;
  wire \in1Count_1_reg_152_reg_n_0_[14] ;
  wire \in1Count_1_reg_152_reg_n_0_[15] ;
  wire \in1Count_1_reg_152_reg_n_0_[16] ;
  wire \in1Count_1_reg_152_reg_n_0_[17] ;
  wire \in1Count_1_reg_152_reg_n_0_[18] ;
  wire \in1Count_1_reg_152_reg_n_0_[19] ;
  wire \in1Count_1_reg_152_reg_n_0_[1] ;
  wire \in1Count_1_reg_152_reg_n_0_[2] ;
  wire \in1Count_1_reg_152_reg_n_0_[3] ;
  wire \in1Count_1_reg_152_reg_n_0_[4] ;
  wire \in1Count_1_reg_152_reg_n_0_[5] ;
  wire \in1Count_1_reg_152_reg_n_0_[6] ;
  wire \in1Count_1_reg_152_reg_n_0_[7] ;
  wire \in1Count_1_reg_152_reg_n_0_[8] ;
  wire \in1Count_1_reg_152_reg_n_0_[9] ;
  wire in1Count_3_reg_1880;
  wire \in1Count_3_reg_188[0]_i_3_n_0 ;
  wire \in1Count_3_reg_188[0]_i_4_n_0 ;
  wire \in1Count_3_reg_188[0]_i_5_n_0 ;
  wire \in1Count_3_reg_188[0]_i_6_n_0 ;
  wire \in1Count_3_reg_188[12]_i_2_n_0 ;
  wire \in1Count_3_reg_188[12]_i_3_n_0 ;
  wire \in1Count_3_reg_188[12]_i_4_n_0 ;
  wire \in1Count_3_reg_188[12]_i_5_n_0 ;
  wire \in1Count_3_reg_188[16]_i_2_n_0 ;
  wire \in1Count_3_reg_188[16]_i_3_n_0 ;
  wire \in1Count_3_reg_188[16]_i_4_n_0 ;
  wire \in1Count_3_reg_188[16]_i_5_n_0 ;
  wire \in1Count_3_reg_188[4]_i_2_n_0 ;
  wire \in1Count_3_reg_188[4]_i_3_n_0 ;
  wire \in1Count_3_reg_188[4]_i_4_n_0 ;
  wire \in1Count_3_reg_188[4]_i_5_n_0 ;
  wire \in1Count_3_reg_188[8]_i_2_n_0 ;
  wire \in1Count_3_reg_188[8]_i_3_n_0 ;
  wire \in1Count_3_reg_188[8]_i_4_n_0 ;
  wire \in1Count_3_reg_188[8]_i_5_n_0 ;
  wire [19:0]in1Count_3_reg_188_reg;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_0 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_1 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_2 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_3 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_4 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_5 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_6 ;
  wire \in1Count_3_reg_188_reg[0]_i_2_n_7 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_0 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_1 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_2 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_3 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_4 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_5 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_6 ;
  wire \in1Count_3_reg_188_reg[12]_i_1_n_7 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_1 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_2 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_3 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_4 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_5 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_6 ;
  wire \in1Count_3_reg_188_reg[16]_i_1_n_7 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_0 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_1 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_2 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_3 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_4 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_5 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_6 ;
  wire \in1Count_3_reg_188_reg[4]_i_1_n_7 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_0 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_1 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_2 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_3 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_4 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_5 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_6 ;
  wire \in1Count_3_reg_188_reg[8]_i_1_n_7 ;
  wire in1Count_reg_140;
  wire in1Count_reg_1400;
  wire \in1Count_reg_140[19]_i_3_n_0 ;
  wire \in1Count_reg_140[19]_i_4_n_0 ;
  wire \in1Count_reg_140_reg_n_0_[0] ;
  wire \in1Count_reg_140_reg_n_0_[10] ;
  wire \in1Count_reg_140_reg_n_0_[11] ;
  wire \in1Count_reg_140_reg_n_0_[12] ;
  wire \in1Count_reg_140_reg_n_0_[13] ;
  wire \in1Count_reg_140_reg_n_0_[14] ;
  wire \in1Count_reg_140_reg_n_0_[15] ;
  wire \in1Count_reg_140_reg_n_0_[16] ;
  wire \in1Count_reg_140_reg_n_0_[17] ;
  wire \in1Count_reg_140_reg_n_0_[18] ;
  wire \in1Count_reg_140_reg_n_0_[19] ;
  wire \in1Count_reg_140_reg_n_0_[1] ;
  wire \in1Count_reg_140_reg_n_0_[2] ;
  wire \in1Count_reg_140_reg_n_0_[3] ;
  wire \in1Count_reg_140_reg_n_0_[4] ;
  wire \in1Count_reg_140_reg_n_0_[5] ;
  wire \in1Count_reg_140_reg_n_0_[6] ;
  wire \in1Count_reg_140_reg_n_0_[7] ;
  wire \in1Count_reg_140_reg_n_0_[8] ;
  wire \in1Count_reg_140_reg_n_0_[9] ;
  wire interrupt;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire tmp_fu_162_p2;
  wire tmp_last_V_reg_194;
  wire \tmp_last_V_reg_194[0]_i_1_n_0 ;
  wire tmp_reg_184;
  wire \tmp_reg_184[0]_i_1_n_0 ;
  wire [3:3]\NLW_in1Count_3_reg_188_reg[16]_i_1_CO_UNCONNECTED ;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.ARESET(ARESET),
        .D({ap_NS_fsm[1],Adder2_CONTROL_BUS_s_axi_U_n_8}),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\ap_CS_fsm[2]_i_2_n_0 ),
        .INPUT_STREAM_V_last_V_0_data_out(INPUT_STREAM_V_last_V_0_data_out),
        .Q({agg_result_a_ap_vld,ap_CS_fsm_pp0_stage0,\ap_CS_fsm_reg_n_0_[0] }),
        .SR(in1Count_reg_140),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter0(ap_enable_reg_pp0_iter0),
        .ap_enable_reg_pp0_iter0_reg(Adder2_CONTROL_BUS_s_axi_U_n_10),
        .ap_enable_reg_pp0_iter0_reg_0(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ),
        .ap_enable_reg_pp0_iter1_reg(Adder2_CONTROL_BUS_s_axi_U_n_1),
        .ap_enable_reg_pp0_iter1_reg_0(ap_enable_reg_pp0_iter1_reg_n_0),
        .ap_rst_n(ap_rst_n),
        .\in1Count_1_reg_152_reg[19] ({\in1Count_1_reg_152_reg_n_0_[19] ,\in1Count_1_reg_152_reg_n_0_[18] ,\in1Count_1_reg_152_reg_n_0_[17] ,\in1Count_1_reg_152_reg_n_0_[16] ,\in1Count_1_reg_152_reg_n_0_[15] ,\in1Count_1_reg_152_reg_n_0_[14] ,\in1Count_1_reg_152_reg_n_0_[13] ,\in1Count_1_reg_152_reg_n_0_[12] ,\in1Count_1_reg_152_reg_n_0_[11] ,\in1Count_1_reg_152_reg_n_0_[10] ,\in1Count_1_reg_152_reg_n_0_[9] ,\in1Count_1_reg_152_reg_n_0_[8] ,\in1Count_1_reg_152_reg_n_0_[7] ,\in1Count_1_reg_152_reg_n_0_[6] ,\in1Count_1_reg_152_reg_n_0_[5] ,\in1Count_1_reg_152_reg_n_0_[4] ,\in1Count_1_reg_152_reg_n_0_[3] ,\in1Count_1_reg_152_reg_n_0_[2] ,\in1Count_1_reg_152_reg_n_0_[1] ,\in1Count_1_reg_152_reg_n_0_[0] }),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .tmp_fu_162_p2(tmp_fu_162_p2),
        .\tmp_reg_184_reg[0] (\in1Count_reg_140[19]_i_3_n_0 ));
  GND GND
       (.G(\<const0> ));
  LUT5 #(
    .INIT(32'h8A80AA80)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TVALID),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[1] ),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I4(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hDFDD)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I1(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .I2(INPUT_STREAM_TVALID),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[1] ),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_0 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[1] ),
        .R(ARESET));
  LUT5 #(
    .INIT(32'h8A80AA80)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_TREADY),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_0_[0] ),
        .I4(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hDFDD)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_0_[0] ),
        .I1(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_TREADY),
        .O(INPUT_STREAM_V_dest_V_0_state));
  LUT6 #(
    .INIT(64'h5400000000000000)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_3 
       (.I0(\INPUT_STREAM_V_dest_V_0_state[1]_i_4_n_0 ),
        .I1(\INPUT_STREAM_V_dest_V_0_state[1]_i_5_n_0 ),
        .I2(\INPUT_STREAM_V_dest_V_0_state[1]_i_6_n_0 ),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I4(ap_CS_fsm_pp0_stage0),
        .I5(ap_enable_reg_pp0_iter0),
        .O(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_4 
       (.I0(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(\INPUT_STREAM_V_dest_V_0_state[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h555515555555D555)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_5 
       (.I0(\in1Count_reg_140_reg_n_0_[18] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[18]),
        .O(\INPUT_STREAM_V_dest_V_0_state[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h555515555555D555)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_6 
       (.I0(\in1Count_reg_140_reg_n_0_[19] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[19]),
        .O(\INPUT_STREAM_V_dest_V_0_state[1]_i_6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_0 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFEE2022)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_0 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_0 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBFBB8088)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_0 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_0 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(\INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ),
        .I1(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .I2(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_0),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(ARESET));
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_0),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(ARESET));
  LUT5 #(
    .INIT(32'h8A80AA80)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ),
        .I4(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'hDFDD)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(\INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ),
        .I1(\INPUT_STREAM_V_dest_V_0_state[1]_i_3_n_0 ),
        .I2(INPUT_STREAM_TVALID),
        .I3(INPUT_STREAM_V_last_V_0_ack_in),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_0 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFE00F000FF000000)) 
    \LAST_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(\in1Count_reg_140[19]_i_4_n_0 ),
        .I1(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ),
        .I2(LAST_STREAM_TVALID),
        .I3(ap_rst_n),
        .I4(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I5(\LAST_STREAM_V_data_V_0_state_reg_n_0_[1] ),
        .O(\LAST_STREAM_V_data_V_0_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4FFF4F4F4F4F4F4F)) 
    \LAST_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(LAST_STREAM_TVALID),
        .I1(\LAST_STREAM_V_data_V_0_state_reg_n_0_[1] ),
        .I2(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I3(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ),
        .I4(tmp_reg_184),
        .I5(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(LAST_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_data_V_0_state[0]_i_1_n_0 ),
        .Q(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_data_V_0_state),
        .Q(\LAST_STREAM_V_data_V_0_state_reg_n_0_[1] ),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFE00F000FF000000)) 
    \LAST_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(\in1Count_reg_140[19]_i_4_n_0 ),
        .I1(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ),
        .I2(LAST_STREAM_TVALID),
        .I3(ap_rst_n),
        .I4(\LAST_STREAM_V_dest_V_0_state_reg_n_0_[0] ),
        .I5(LAST_STREAM_TREADY),
        .O(\LAST_STREAM_V_dest_V_0_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4FFF4F4F4F4F4F4F)) 
    \LAST_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(LAST_STREAM_TVALID),
        .I1(LAST_STREAM_TREADY),
        .I2(\LAST_STREAM_V_dest_V_0_state_reg_n_0_[0] ),
        .I3(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ),
        .I4(tmp_reg_184),
        .I5(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(LAST_STREAM_V_dest_V_0_state));
  LUT6 #(
    .INIT(64'hAAAAFEAAFFFFFFFF)) 
    \LAST_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(\INPUT_STREAM_V_dest_V_0_state[1]_i_4_n_0 ),
        .I1(\INPUT_STREAM_V_dest_V_0_state[1]_i_5_n_0 ),
        .I2(\INPUT_STREAM_V_dest_V_0_state[1]_i_6_n_0 ),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I5(ap_CS_fsm_pp0_stage0),
        .O(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\LAST_STREAM_V_dest_V_0_state[0]_i_1_n_0 ),
        .Q(\LAST_STREAM_V_dest_V_0_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \LAST_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(LAST_STREAM_V_dest_V_0_state),
        .Q(LAST_STREAM_TREADY),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(\ap_CS_fsm[2]_i_2_n_0 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_enable_reg_pp0_iter0),
        .O(ap_NS_fsm[2]));
  LUT6 #(
    .INIT(64'h444044404440FFFF)) 
    \ap_CS_fsm[2]_i_2 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(\INPUT_STREAM_V_dest_V_0_state[1]_i_6_n_0 ),
        .I3(\INPUT_STREAM_V_dest_V_0_state[1]_i_5_n_0 ),
        .I4(\in1Count_reg_140[19]_i_4_n_0 ),
        .I5(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .O(\ap_CS_fsm[2]_i_2_n_0 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(Adder2_CONTROL_BUS_s_axi_U_n_8),
        .Q(\ap_CS_fsm_reg_n_0_[0] ),
        .S(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(ARESET));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(agg_result_a_ap_vld),
        .R(ARESET));
  LUT3 #(
    .INIT(8'hB8)) 
    ap_enable_reg_pp0_iter0_i_2
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .O(INPUT_STREAM_V_last_V_0_data_out));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Adder2_CONTROL_BUS_s_axi_U_n_10),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Adder2_CONTROL_BUS_s_axi_U_n_1),
        .Q(ap_enable_reg_pp0_iter1_reg_n_0),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[0]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[0] ),
        .I3(in1Count_3_reg_188_reg[0]),
        .O(\in1Count_1_reg_152[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[10]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[10] ),
        .I3(in1Count_3_reg_188_reg[10]),
        .O(\in1Count_1_reg_152[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[11]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[11] ),
        .I3(in1Count_3_reg_188_reg[11]),
        .O(\in1Count_1_reg_152[11]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[12]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[12] ),
        .I3(in1Count_3_reg_188_reg[12]),
        .O(\in1Count_1_reg_152[12]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[13]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[13] ),
        .I3(in1Count_3_reg_188_reg[13]),
        .O(\in1Count_1_reg_152[13]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[14]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[14] ),
        .I3(in1Count_3_reg_188_reg[14]),
        .O(\in1Count_1_reg_152[14]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[15]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[15] ),
        .I3(in1Count_3_reg_188_reg[15]),
        .O(\in1Count_1_reg_152[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[16]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[16] ),
        .I3(in1Count_3_reg_188_reg[16]),
        .O(\in1Count_1_reg_152[16]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[17]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[17] ),
        .I3(in1Count_3_reg_188_reg[17]),
        .O(\in1Count_1_reg_152[17]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF780)) 
    \in1Count_1_reg_152[18]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(in1Count_3_reg_188_reg[18]),
        .I3(\in1Count_reg_140_reg_n_0_[18] ),
        .O(\in1Count_1_reg_152[18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h40444040)) 
    \in1Count_1_reg_152[19]_i_1 
       (.I0(\ap_CS_fsm[2]_i_2_n_0 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\in1Count_1_reg_152[19]_i_3_n_0 ),
        .I3(tmp_fu_162_p2),
        .I4(ap_enable_reg_pp0_iter0),
        .O(in1Count_1_reg_152));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'hF780)) 
    \in1Count_1_reg_152[19]_i_2 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(in1Count_3_reg_188_reg[19]),
        .I3(\in1Count_reg_140_reg_n_0_[19] ),
        .O(\in1Count_1_reg_152[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \in1Count_1_reg_152[19]_i_3 
       (.I0(tmp_last_V_reg_194),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .O(\in1Count_1_reg_152[19]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[1]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[1] ),
        .I3(in1Count_3_reg_188_reg[1]),
        .O(\in1Count_1_reg_152[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[2]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[2] ),
        .I3(in1Count_3_reg_188_reg[2]),
        .O(\in1Count_1_reg_152[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[3]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[3] ),
        .I3(in1Count_3_reg_188_reg[3]),
        .O(\in1Count_1_reg_152[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[4]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[4] ),
        .I3(in1Count_3_reg_188_reg[4]),
        .O(\in1Count_1_reg_152[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[5]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[5] ),
        .I3(in1Count_3_reg_188_reg[5]),
        .O(\in1Count_1_reg_152[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[6]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[6] ),
        .I3(in1Count_3_reg_188_reg[6]),
        .O(\in1Count_1_reg_152[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[7]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[7] ),
        .I3(in1Count_3_reg_188_reg[7]),
        .O(\in1Count_1_reg_152[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[8]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[8] ),
        .I3(in1Count_3_reg_188_reg[8]),
        .O(\in1Count_1_reg_152[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF870)) 
    \in1Count_1_reg_152[9]_i_1 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(\in1Count_reg_140_reg_n_0_[9] ),
        .I3(in1Count_3_reg_188_reg[9]),
        .O(\in1Count_1_reg_152[9]_i_1_n_0 ));
  FDRE \in1Count_1_reg_152_reg[0] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[0]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[10] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[10]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[11] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[11]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[11] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[12] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[12]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[12] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[13] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[13]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[13] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[14] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[14]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[14] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[15] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[15]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[15] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[16] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[16]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[16] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[17] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[17]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[17] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[18] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[18]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[18] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[19] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[19]_i_2_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[19] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[1] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[1]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[2] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[2]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[3] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[3]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[4] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[4]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[5] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[5]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[6] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[6]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[7] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[7]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[8] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[8]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \in1Count_1_reg_152_reg[9] 
       (.C(ap_clk),
        .CE(in1Count_1_reg_152),
        .D(\in1Count_1_reg_152[9]_i_1_n_0 ),
        .Q(\in1Count_1_reg_152_reg_n_0_[9] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h80A080A080A00000)) 
    \in1Count_3_reg_188[0]_i_1 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(tmp_fu_162_p2),
        .I4(\in1Count_reg_140[19]_i_4_n_0 ),
        .I5(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .O(in1Count_3_reg_1880));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[0]_i_3 
       (.I0(\in1Count_reg_140_reg_n_0_[3] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[3]),
        .O(\in1Count_3_reg_188[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[0]_i_4 
       (.I0(\in1Count_reg_140_reg_n_0_[2] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[2]),
        .O(\in1Count_3_reg_188[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[0]_i_5 
       (.I0(\in1Count_reg_140_reg_n_0_[1] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[1]),
        .O(\in1Count_3_reg_188[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h10000000DFFFFFFF)) 
    \in1Count_3_reg_188[0]_i_6 
       (.I0(in1Count_3_reg_188_reg[0]),
        .I1(tmp_last_V_reg_194),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_enable_reg_pp0_iter1_reg_n_0),
        .I4(tmp_reg_184),
        .I5(\in1Count_reg_140_reg_n_0_[0] ),
        .O(\in1Count_3_reg_188[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[12]_i_2 
       (.I0(\in1Count_reg_140_reg_n_0_[15] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[15]),
        .O(\in1Count_3_reg_188[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[12]_i_3 
       (.I0(\in1Count_reg_140_reg_n_0_[14] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[14]),
        .O(\in1Count_3_reg_188[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[12]_i_4 
       (.I0(\in1Count_reg_140_reg_n_0_[13] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[13]),
        .O(\in1Count_3_reg_188[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[12]_i_5 
       (.I0(\in1Count_reg_140_reg_n_0_[12] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[12]),
        .O(\in1Count_3_reg_188[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \in1Count_3_reg_188[16]_i_2 
       (.I0(in1Count_3_reg_188_reg[19]),
        .I1(tmp_last_V_reg_194),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_enable_reg_pp0_iter1_reg_n_0),
        .I4(tmp_reg_184),
        .I5(\in1Count_reg_140_reg_n_0_[19] ),
        .O(\in1Count_3_reg_188[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \in1Count_3_reg_188[16]_i_3 
       (.I0(in1Count_3_reg_188_reg[18]),
        .I1(tmp_last_V_reg_194),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_enable_reg_pp0_iter1_reg_n_0),
        .I4(tmp_reg_184),
        .I5(\in1Count_reg_140_reg_n_0_[18] ),
        .O(\in1Count_3_reg_188[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[16]_i_4 
       (.I0(\in1Count_reg_140_reg_n_0_[17] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[17]),
        .O(\in1Count_3_reg_188[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[16]_i_5 
       (.I0(\in1Count_reg_140_reg_n_0_[16] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[16]),
        .O(\in1Count_3_reg_188[16]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[4]_i_2 
       (.I0(\in1Count_reg_140_reg_n_0_[7] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[7]),
        .O(\in1Count_3_reg_188[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[4]_i_3 
       (.I0(\in1Count_reg_140_reg_n_0_[6] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[6]),
        .O(\in1Count_3_reg_188[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[4]_i_4 
       (.I0(\in1Count_reg_140_reg_n_0_[5] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[5]),
        .O(\in1Count_3_reg_188[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[4]_i_5 
       (.I0(\in1Count_reg_140_reg_n_0_[4] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[4]),
        .O(\in1Count_3_reg_188[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[8]_i_2 
       (.I0(\in1Count_reg_140_reg_n_0_[11] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[11]),
        .O(\in1Count_3_reg_188[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[8]_i_3 
       (.I0(\in1Count_reg_140_reg_n_0_[10] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[10]),
        .O(\in1Count_3_reg_188[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[8]_i_4 
       (.I0(\in1Count_reg_140_reg_n_0_[9] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[9]),
        .O(\in1Count_3_reg_188[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \in1Count_3_reg_188[8]_i_5 
       (.I0(\in1Count_reg_140_reg_n_0_[8] ),
        .I1(tmp_reg_184),
        .I2(ap_enable_reg_pp0_iter1_reg_n_0),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(tmp_last_V_reg_194),
        .I5(in1Count_3_reg_188_reg[8]),
        .O(\in1Count_3_reg_188[8]_i_5_n_0 ));
  FDRE \in1Count_3_reg_188_reg[0] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[0]_i_2_n_7 ),
        .Q(in1Count_3_reg_188_reg[0]),
        .R(1'b0));
  CARRY4 \in1Count_3_reg_188_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\in1Count_3_reg_188_reg[0]_i_2_n_0 ,\in1Count_3_reg_188_reg[0]_i_2_n_1 ,\in1Count_3_reg_188_reg[0]_i_2_n_2 ,\in1Count_3_reg_188_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\in1Count_3_reg_188_reg[0]_i_2_n_4 ,\in1Count_3_reg_188_reg[0]_i_2_n_5 ,\in1Count_3_reg_188_reg[0]_i_2_n_6 ,\in1Count_3_reg_188_reg[0]_i_2_n_7 }),
        .S({\in1Count_3_reg_188[0]_i_3_n_0 ,\in1Count_3_reg_188[0]_i_4_n_0 ,\in1Count_3_reg_188[0]_i_5_n_0 ,\in1Count_3_reg_188[0]_i_6_n_0 }));
  FDRE \in1Count_3_reg_188_reg[10] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[8]_i_1_n_5 ),
        .Q(in1Count_3_reg_188_reg[10]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[11] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[8]_i_1_n_4 ),
        .Q(in1Count_3_reg_188_reg[11]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[12] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[12]_i_1_n_7 ),
        .Q(in1Count_3_reg_188_reg[12]),
        .R(1'b0));
  CARRY4 \in1Count_3_reg_188_reg[12]_i_1 
       (.CI(\in1Count_3_reg_188_reg[8]_i_1_n_0 ),
        .CO({\in1Count_3_reg_188_reg[12]_i_1_n_0 ,\in1Count_3_reg_188_reg[12]_i_1_n_1 ,\in1Count_3_reg_188_reg[12]_i_1_n_2 ,\in1Count_3_reg_188_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\in1Count_3_reg_188_reg[12]_i_1_n_4 ,\in1Count_3_reg_188_reg[12]_i_1_n_5 ,\in1Count_3_reg_188_reg[12]_i_1_n_6 ,\in1Count_3_reg_188_reg[12]_i_1_n_7 }),
        .S({\in1Count_3_reg_188[12]_i_2_n_0 ,\in1Count_3_reg_188[12]_i_3_n_0 ,\in1Count_3_reg_188[12]_i_4_n_0 ,\in1Count_3_reg_188[12]_i_5_n_0 }));
  FDRE \in1Count_3_reg_188_reg[13] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[12]_i_1_n_6 ),
        .Q(in1Count_3_reg_188_reg[13]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[14] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[12]_i_1_n_5 ),
        .Q(in1Count_3_reg_188_reg[14]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[15] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[12]_i_1_n_4 ),
        .Q(in1Count_3_reg_188_reg[15]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[16] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[16]_i_1_n_7 ),
        .Q(in1Count_3_reg_188_reg[16]),
        .R(1'b0));
  CARRY4 \in1Count_3_reg_188_reg[16]_i_1 
       (.CI(\in1Count_3_reg_188_reg[12]_i_1_n_0 ),
        .CO({\NLW_in1Count_3_reg_188_reg[16]_i_1_CO_UNCONNECTED [3],\in1Count_3_reg_188_reg[16]_i_1_n_1 ,\in1Count_3_reg_188_reg[16]_i_1_n_2 ,\in1Count_3_reg_188_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\in1Count_3_reg_188_reg[16]_i_1_n_4 ,\in1Count_3_reg_188_reg[16]_i_1_n_5 ,\in1Count_3_reg_188_reg[16]_i_1_n_6 ,\in1Count_3_reg_188_reg[16]_i_1_n_7 }),
        .S({\in1Count_3_reg_188[16]_i_2_n_0 ,\in1Count_3_reg_188[16]_i_3_n_0 ,\in1Count_3_reg_188[16]_i_4_n_0 ,\in1Count_3_reg_188[16]_i_5_n_0 }));
  FDRE \in1Count_3_reg_188_reg[17] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[16]_i_1_n_6 ),
        .Q(in1Count_3_reg_188_reg[17]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[18] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[16]_i_1_n_5 ),
        .Q(in1Count_3_reg_188_reg[18]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[19] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[16]_i_1_n_4 ),
        .Q(in1Count_3_reg_188_reg[19]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[1] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[0]_i_2_n_6 ),
        .Q(in1Count_3_reg_188_reg[1]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[2] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[0]_i_2_n_5 ),
        .Q(in1Count_3_reg_188_reg[2]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[3] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[0]_i_2_n_4 ),
        .Q(in1Count_3_reg_188_reg[3]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[4] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[4]_i_1_n_7 ),
        .Q(in1Count_3_reg_188_reg[4]),
        .R(1'b0));
  CARRY4 \in1Count_3_reg_188_reg[4]_i_1 
       (.CI(\in1Count_3_reg_188_reg[0]_i_2_n_0 ),
        .CO({\in1Count_3_reg_188_reg[4]_i_1_n_0 ,\in1Count_3_reg_188_reg[4]_i_1_n_1 ,\in1Count_3_reg_188_reg[4]_i_1_n_2 ,\in1Count_3_reg_188_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\in1Count_3_reg_188_reg[4]_i_1_n_4 ,\in1Count_3_reg_188_reg[4]_i_1_n_5 ,\in1Count_3_reg_188_reg[4]_i_1_n_6 ,\in1Count_3_reg_188_reg[4]_i_1_n_7 }),
        .S({\in1Count_3_reg_188[4]_i_2_n_0 ,\in1Count_3_reg_188[4]_i_3_n_0 ,\in1Count_3_reg_188[4]_i_4_n_0 ,\in1Count_3_reg_188[4]_i_5_n_0 }));
  FDRE \in1Count_3_reg_188_reg[5] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[4]_i_1_n_6 ),
        .Q(in1Count_3_reg_188_reg[5]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[6] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[4]_i_1_n_5 ),
        .Q(in1Count_3_reg_188_reg[6]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[7] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[4]_i_1_n_4 ),
        .Q(in1Count_3_reg_188_reg[7]),
        .R(1'b0));
  FDRE \in1Count_3_reg_188_reg[8] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[8]_i_1_n_7 ),
        .Q(in1Count_3_reg_188_reg[8]),
        .R(1'b0));
  CARRY4 \in1Count_3_reg_188_reg[8]_i_1 
       (.CI(\in1Count_3_reg_188_reg[4]_i_1_n_0 ),
        .CO({\in1Count_3_reg_188_reg[8]_i_1_n_0 ,\in1Count_3_reg_188_reg[8]_i_1_n_1 ,\in1Count_3_reg_188_reg[8]_i_1_n_2 ,\in1Count_3_reg_188_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\in1Count_3_reg_188_reg[8]_i_1_n_4 ,\in1Count_3_reg_188_reg[8]_i_1_n_5 ,\in1Count_3_reg_188_reg[8]_i_1_n_6 ,\in1Count_3_reg_188_reg[8]_i_1_n_7 }),
        .S({\in1Count_3_reg_188[8]_i_2_n_0 ,\in1Count_3_reg_188[8]_i_3_n_0 ,\in1Count_3_reg_188[8]_i_4_n_0 ,\in1Count_3_reg_188[8]_i_5_n_0 }));
  FDRE \in1Count_3_reg_188_reg[9] 
       (.C(ap_clk),
        .CE(in1Count_3_reg_1880),
        .D(\in1Count_3_reg_188_reg[8]_i_1_n_6 ),
        .Q(in1Count_3_reg_188_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h5454545400545454)) 
    \in1Count_reg_140[19]_i_2 
       (.I0(\in1Count_reg_140[19]_i_3_n_0 ),
        .I1(\LAST_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .I2(\in1Count_reg_140[19]_i_4_n_0 ),
        .I3(tmp_fu_162_p2),
        .I4(ap_enable_reg_pp0_iter0),
        .I5(\INPUT_STREAM_V_data_V_0_state_reg_n_0_[0] ),
        .O(in1Count_reg_1400));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \in1Count_reg_140[19]_i_3 
       (.I0(tmp_reg_184),
        .I1(ap_enable_reg_pp0_iter1_reg_n_0),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(tmp_last_V_reg_194),
        .O(\in1Count_reg_140[19]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \in1Count_reg_140[19]_i_4 
       (.I0(ap_enable_reg_pp0_iter1_reg_n_0),
        .I1(tmp_reg_184),
        .O(\in1Count_reg_140[19]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h335FFF5F)) 
    \in1Count_reg_140[19]_i_5 
       (.I0(in1Count_3_reg_188_reg[19]),
        .I1(\in1Count_reg_140_reg_n_0_[19] ),
        .I2(in1Count_3_reg_188_reg[18]),
        .I3(\in1Count_reg_140[19]_i_3_n_0 ),
        .I4(\in1Count_reg_140_reg_n_0_[18] ),
        .O(tmp_fu_162_p2));
  FDRE \in1Count_reg_140_reg[0] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[0]),
        .Q(\in1Count_reg_140_reg_n_0_[0] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[10] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[10]),
        .Q(\in1Count_reg_140_reg_n_0_[10] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[11] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[11]),
        .Q(\in1Count_reg_140_reg_n_0_[11] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[12] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[12]),
        .Q(\in1Count_reg_140_reg_n_0_[12] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[13] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[13]),
        .Q(\in1Count_reg_140_reg_n_0_[13] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[14] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[14]),
        .Q(\in1Count_reg_140_reg_n_0_[14] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[15] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[15]),
        .Q(\in1Count_reg_140_reg_n_0_[15] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[16] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[16]),
        .Q(\in1Count_reg_140_reg_n_0_[16] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[17] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[17]),
        .Q(\in1Count_reg_140_reg_n_0_[17] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[18] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[18]),
        .Q(\in1Count_reg_140_reg_n_0_[18] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[19] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[19]),
        .Q(\in1Count_reg_140_reg_n_0_[19] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[1] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[1]),
        .Q(\in1Count_reg_140_reg_n_0_[1] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[2] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[2]),
        .Q(\in1Count_reg_140_reg_n_0_[2] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[3] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[3]),
        .Q(\in1Count_reg_140_reg_n_0_[3] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[4] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[4]),
        .Q(\in1Count_reg_140_reg_n_0_[4] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[5] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[5]),
        .Q(\in1Count_reg_140_reg_n_0_[5] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[6] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[6]),
        .Q(\in1Count_reg_140_reg_n_0_[6] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[7] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[7]),
        .Q(\in1Count_reg_140_reg_n_0_[7] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[8] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[8]),
        .Q(\in1Count_reg_140_reg_n_0_[8] ),
        .R(in1Count_reg_140));
  FDRE \in1Count_reg_140_reg[9] 
       (.C(ap_clk),
        .CE(in1Count_reg_1400),
        .D(in1Count_3_reg_188_reg[9]),
        .Q(\in1Count_reg_140_reg_n_0_[9] ),
        .R(in1Count_reg_140));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    \tmp_last_V_reg_194[0]_i_1 
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(tmp_fu_162_p2),
        .I4(\LAST_STREAM_V_dest_V_0_state[1]_i_2_n_0 ),
        .I5(tmp_last_V_reg_194),
        .O(\tmp_last_V_reg_194[0]_i_1_n_0 ));
  FDRE \tmp_last_V_reg_194_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_last_V_reg_194[0]_i_1_n_0 ),
        .Q(tmp_last_V_reg_194),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \tmp_reg_184[0]_i_1 
       (.I0(tmp_fu_162_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\ap_CS_fsm[2]_i_2_n_0 ),
        .I3(tmp_reg_184),
        .O(\tmp_reg_184[0]_i_1_n_0 ));
  FDRE \tmp_reg_184_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_reg_184[0]_i_1_n_0 ),
        .Q(tmp_reg_184),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (ARESET,
    ap_enable_reg_pp0_iter1_reg,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    D,
    SR,
    ap_enable_reg_pp0_iter0_reg,
    interrupt,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    \in1Count_1_reg_152_reg[19] ,
    ap_enable_reg_pp0_iter1_reg_0,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    ap_enable_reg_pp0_iter0,
    ap_rst_n,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    \tmp_reg_184_reg[0] ,
    tmp_fu_162_p2,
    INPUT_STREAM_V_last_V_0_data_out,
    ap_enable_reg_pp0_iter0_reg_0,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR);
  output ARESET;
  output ap_enable_reg_pp0_iter1_reg;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [1:0]D;
  output [0:0]SR;
  output ap_enable_reg_pp0_iter0_reg;
  output interrupt;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [2:0]Q;
  input [19:0]\in1Count_1_reg_152_reg[19] ;
  input ap_enable_reg_pp0_iter1_reg_0;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input ap_enable_reg_pp0_iter0;
  input ap_rst_n;
  input s_axi_CONTROL_BUS_ARVALID;
  input s_axi_CONTROL_BUS_RREADY;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input \tmp_reg_184_reg[0] ;
  input tmp_fu_162_p2;
  input INPUT_STREAM_V_last_V_0_data_out;
  input ap_enable_reg_pp0_iter0_reg_0;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;

  wire \/FSM_onehot_wstate[1]_i_1_n_0 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_0 ;
  wire ARESET;
  wire [1:0]D;
  wire \FSM_onehot_wstate[3]_i_1_n_0 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_0_[0] ;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire INPUT_STREAM_V_last_V_0_data_out;
  wire [2:0]Q;
  wire [0:0]SR;
  wire ap_NS_fsm16_out;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_reg;
  wire ap_enable_reg_pp0_iter0_reg_0;
  wire ap_enable_reg_pp0_iter1_reg;
  wire ap_enable_reg_pp0_iter1_reg_0;
  wire ap_idle;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire [19:0]\in1Count_1_reg_152_reg[19] ;
  wire [19:0]int_agg_result_a;
  wire int_agg_result_a_ap_vld;
  wire int_agg_result_a_ap_vld_i_1_n_0;
  wire int_agg_result_a_ap_vld_i_2_n_0;
  wire int_agg_result_b_ap_vld;
  wire int_agg_result_b_ap_vld_i_1_n_0;
  wire int_ap_done;
  wire int_ap_done_i_1_n_0;
  wire int_ap_done_i_2_n_0;
  wire int_ap_done_i_3_n_0;
  wire int_ap_idle;
  wire int_ap_ready;
  wire int_ap_start3_out;
  wire int_ap_start_i_1_n_0;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_0;
  wire int_gie_i_1_n_0;
  wire int_gie_reg_n_0;
  wire \int_ier[0]_i_1_n_0 ;
  wire \int_ier[1]_i_1_n_0 ;
  wire \int_ier[1]_i_2_n_0 ;
  wire \int_ier_reg_n_0_[0] ;
  wire int_isr6_out;
  wire \int_isr[0]_i_1_n_0 ;
  wire \int_isr[1]_i_1_n_0 ;
  wire \int_isr_reg_n_0_[0] ;
  wire \int_searched[31]_i_3_n_0 ;
  wire \int_searched_reg_n_0_[0] ;
  wire \int_searched_reg_n_0_[10] ;
  wire \int_searched_reg_n_0_[11] ;
  wire \int_searched_reg_n_0_[12] ;
  wire \int_searched_reg_n_0_[13] ;
  wire \int_searched_reg_n_0_[14] ;
  wire \int_searched_reg_n_0_[15] ;
  wire \int_searched_reg_n_0_[16] ;
  wire \int_searched_reg_n_0_[17] ;
  wire \int_searched_reg_n_0_[18] ;
  wire \int_searched_reg_n_0_[19] ;
  wire \int_searched_reg_n_0_[1] ;
  wire \int_searched_reg_n_0_[20] ;
  wire \int_searched_reg_n_0_[21] ;
  wire \int_searched_reg_n_0_[22] ;
  wire \int_searched_reg_n_0_[23] ;
  wire \int_searched_reg_n_0_[24] ;
  wire \int_searched_reg_n_0_[25] ;
  wire \int_searched_reg_n_0_[26] ;
  wire \int_searched_reg_n_0_[27] ;
  wire \int_searched_reg_n_0_[28] ;
  wire \int_searched_reg_n_0_[29] ;
  wire \int_searched_reg_n_0_[2] ;
  wire \int_searched_reg_n_0_[30] ;
  wire \int_searched_reg_n_0_[31] ;
  wire \int_searched_reg_n_0_[3] ;
  wire \int_searched_reg_n_0_[4] ;
  wire \int_searched_reg_n_0_[5] ;
  wire \int_searched_reg_n_0_[6] ;
  wire \int_searched_reg_n_0_[7] ;
  wire \int_searched_reg_n_0_[8] ;
  wire \int_searched_reg_n_0_[9] ;
  wire interrupt;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in13_out;
  wire p_1_in;
  wire \rdata_data[0]_i_1_n_0 ;
  wire \rdata_data[0]_i_2_n_0 ;
  wire \rdata_data[0]_i_3_n_0 ;
  wire \rdata_data[0]_i_4_n_0 ;
  wire \rdata_data[0]_i_5_n_0 ;
  wire \rdata_data[10]_i_1_n_0 ;
  wire \rdata_data[11]_i_1_n_0 ;
  wire \rdata_data[12]_i_1_n_0 ;
  wire \rdata_data[13]_i_1_n_0 ;
  wire \rdata_data[14]_i_1_n_0 ;
  wire \rdata_data[15]_i_1_n_0 ;
  wire \rdata_data[16]_i_1_n_0 ;
  wire \rdata_data[17]_i_1_n_0 ;
  wire \rdata_data[18]_i_1_n_0 ;
  wire \rdata_data[19]_i_1_n_0 ;
  wire \rdata_data[19]_i_2_n_0 ;
  wire \rdata_data[1]_i_1_n_0 ;
  wire \rdata_data[1]_i_2_n_0 ;
  wire \rdata_data[1]_i_3_n_0 ;
  wire \rdata_data[1]_i_4_n_0 ;
  wire \rdata_data[1]_i_5_n_0 ;
  wire \rdata_data[20]_i_1_n_0 ;
  wire \rdata_data[21]_i_1_n_0 ;
  wire \rdata_data[22]_i_1_n_0 ;
  wire \rdata_data[23]_i_1_n_0 ;
  wire \rdata_data[24]_i_1_n_0 ;
  wire \rdata_data[25]_i_1_n_0 ;
  wire \rdata_data[26]_i_1_n_0 ;
  wire \rdata_data[27]_i_1_n_0 ;
  wire \rdata_data[28]_i_1_n_0 ;
  wire \rdata_data[29]_i_1_n_0 ;
  wire \rdata_data[2]_i_1_n_0 ;
  wire \rdata_data[30]_i_1_n_0 ;
  wire \rdata_data[31]_i_1_n_0 ;
  wire \rdata_data[31]_i_3_n_0 ;
  wire \rdata_data[3]_i_1_n_0 ;
  wire \rdata_data[4]_i_1_n_0 ;
  wire \rdata_data[5]_i_1_n_0 ;
  wire \rdata_data[6]_i_1_n_0 ;
  wire \rdata_data[7]_i_1_n_0 ;
  wire \rdata_data[8]_i_1_n_0 ;
  wire \rdata_data[9]_i_1_n_0 ;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_0 ;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire tmp_fu_162_p2;
  wire \tmp_reg_184_reg[0] ;
  wire waddr;
  wire \waddr_reg_n_0_[0] ;
  wire \waddr_reg_n_0_[1] ;
  wire \waddr_reg_n_0_[2] ;
  wire \waddr_reg_n_0_[3] ;
  wire \waddr_reg_n_0_[4] ;
  wire \waddr_reg_n_0_[5] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_0_[0] ),
        .S(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_0 ),
        .Q(out[0]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_0 ),
        .Q(out[1]),
        .R(ARESET));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_0 ),
        .Q(out[2]),
        .R(ARESET));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(ARESET));
  LUT4 #(
    .INIT(16'h4555)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(ap_start),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hFFFBFFFBFF000000)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I1(ap_enable_reg_pp0_iter1_reg_0),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(Q[0]),
        .I4(ap_start),
        .I5(Q[1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hF2F2F20000000000)) 
    ap_enable_reg_pp0_iter0_i_1
       (.I0(tmp_fu_162_p2),
        .I1(INPUT_STREAM_V_last_V_0_data_out),
        .I2(ap_enable_reg_pp0_iter0_reg_0),
        .I3(ap_NS_fsm16_out),
        .I4(ap_enable_reg_pp0_iter0),
        .I5(ap_rst_n),
        .O(ap_enable_reg_pp0_iter0_reg));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ap_enable_reg_pp0_iter0_i_3
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_NS_fsm16_out));
  LUT6 #(
    .INIT(64'h70FF700000000000)) 
    ap_enable_reg_pp0_iter1_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .I2(ap_enable_reg_pp0_iter1_reg_0),
        .I3(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I4(ap_enable_reg_pp0_iter0),
        .I5(ap_rst_n),
        .O(ap_enable_reg_pp0_iter1_reg));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \in1Count_reg_140[19]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I3(\tmp_reg_184_reg[0] ),
        .O(SR));
  LUT5 #(
    .INIT(32'hFFFBAAAA)) 
    int_agg_result_a_ap_vld_i_1
       (.I0(Q[2]),
        .I1(int_agg_result_a_ap_vld_i_2_n_0),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(int_agg_result_a_ap_vld),
        .O(int_agg_result_a_ap_vld_i_1_n_0));
  LUT5 #(
    .INIT(32'h00000080)) 
    int_agg_result_a_ap_vld_i_2
       (.I0(ar_hs),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(int_agg_result_a_ap_vld_i_2_n_0));
  FDRE int_agg_result_a_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_a_ap_vld_i_1_n_0),
        .Q(int_agg_result_a_ap_vld),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [0]),
        .Q(int_agg_result_a[0]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[10] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [10]),
        .Q(int_agg_result_a[10]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [11]),
        .Q(int_agg_result_a[11]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[12] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [12]),
        .Q(int_agg_result_a[12]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[13] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [13]),
        .Q(int_agg_result_a[13]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[14] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [14]),
        .Q(int_agg_result_a[14]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[15] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [15]),
        .Q(int_agg_result_a[15]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[16] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [16]),
        .Q(int_agg_result_a[16]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[17] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [17]),
        .Q(int_agg_result_a[17]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[18] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [18]),
        .Q(int_agg_result_a[18]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[19] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [19]),
        .Q(int_agg_result_a[19]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [1]),
        .Q(int_agg_result_a[1]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [2]),
        .Q(int_agg_result_a[2]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [3]),
        .Q(int_agg_result_a[3]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [4]),
        .Q(int_agg_result_a[4]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [5]),
        .Q(int_agg_result_a[5]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[6] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [6]),
        .Q(int_agg_result_a[6]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[7] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [7]),
        .Q(int_agg_result_a[7]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[8] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [8]),
        .Q(int_agg_result_a[8]),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[9] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\in1Count_1_reg_152_reg[19] [9]),
        .Q(int_agg_result_a[9]),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFFFAAAA)) 
    int_agg_result_b_ap_vld_i_1
       (.I0(Q[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(int_agg_result_a_ap_vld_i_2_n_0),
        .I4(int_agg_result_b_ap_vld),
        .O(int_agg_result_b_ap_vld_i_1_n_0));
  FDRE int_agg_result_b_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_b_ap_vld_i_1_n_0),
        .Q(int_agg_result_b_ap_vld),
        .R(ARESET));
  LUT6 #(
    .INIT(64'hFFFEFFFFAAAAAAAA)) 
    int_ap_done_i_1
       (.I0(Q[2]),
        .I1(int_ap_done_i_2_n_0),
        .I2(int_ap_done_i_3_n_0),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(ar_hs),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    int_ap_done_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(int_ap_done_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'hE)) 
    int_ap_done_i_3
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(int_ap_done_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_0),
        .Q(int_ap_done),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(ARESET));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(int_ap_ready),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFBF8)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[2]),
        .I2(int_ap_start3_out),
        .I3(ap_start),
        .O(int_ap_start_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000002000000)) 
    int_ap_start_i_2
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_0_[3] ),
        .I2(\waddr_reg_n_0_[2] ),
        .I3(\int_searched[31]_i_3_n_0 ),
        .I4(s_axi_CONTROL_BUS_WSTRB[0]),
        .I5(\waddr_reg_n_0_[5] ),
        .O(int_ap_start3_out));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_0),
        .Q(ap_start),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(\int_ier[1]_i_2_n_0 ),
        .I2(\waddr_reg_n_0_[2] ),
        .I3(\waddr_reg_n_0_[3] ),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_0),
        .Q(int_auto_restart),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_0_[3] ),
        .I2(\int_ier[1]_i_2_n_0 ),
        .I3(\waddr_reg_n_0_[2] ),
        .I4(int_gie_reg_n_0),
        .O(int_gie_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_0),
        .Q(int_gie_reg_n_0),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\int_ier[1]_i_2_n_0 ),
        .I2(\waddr_reg_n_0_[3] ),
        .I3(\waddr_reg_n_0_[2] ),
        .I4(\int_ier_reg_n_0_[0] ),
        .O(\int_ier[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\int_ier[1]_i_2_n_0 ),
        .I2(\waddr_reg_n_0_[3] ),
        .I3(\waddr_reg_n_0_[2] ),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \int_ier[1]_i_2 
       (.I0(\int_searched[31]_i_3_n_0 ),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\waddr_reg_n_0_[5] ),
        .O(\int_ier[1]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_0 ),
        .Q(\int_ier_reg_n_0_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_0 ),
        .Q(p_0_in),
        .R(ARESET));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_isr6_out),
        .I2(\int_ier_reg_n_0_[0] ),
        .I3(Q[2]),
        .I4(\int_isr_reg_n_0_[0] ),
        .O(\int_isr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00800000)) 
    \int_isr[0]_i_2 
       (.I0(\waddr_reg_n_0_[3] ),
        .I1(\int_searched[31]_i_3_n_0 ),
        .I2(s_axi_CONTROL_BUS_WSTRB[0]),
        .I3(\waddr_reg_n_0_[5] ),
        .I4(\waddr_reg_n_0_[2] ),
        .O(int_isr6_out));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_isr6_out),
        .I2(p_0_in),
        .I3(Q[2]),
        .I4(p_1_in),
        .O(\int_isr[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_0 ),
        .Q(\int_isr_reg_n_0_[0] ),
        .R(ARESET));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_0 ),
        .Q(p_1_in),
        .R(ARESET));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[0] ),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[10] ),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[11] ),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[12] ),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[13] ),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[14] ),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[15] ),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[16] ),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[17] ),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[18] ),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[19] ),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[1] ),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[20] ),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[21] ),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[22] ),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\int_searched_reg_n_0_[23] ),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[24] ),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[25] ),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[26] ),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[27] ),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[28] ),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[29] ),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[2] ),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[30] ),
        .O(\or [30]));
  LUT4 #(
    .INIT(16'h0008)) 
    \int_searched[31]_i_1 
       (.I0(\int_searched[31]_i_3_n_0 ),
        .I1(\waddr_reg_n_0_[5] ),
        .I2(\waddr_reg_n_0_[3] ),
        .I3(\waddr_reg_n_0_[2] ),
        .O(p_0_in13_out));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\int_searched_reg_n_0_[31] ),
        .O(\or [31]));
  LUT5 #(
    .INIT(32'h00040000)) 
    \int_searched[31]_i_3 
       (.I0(\waddr_reg_n_0_[4] ),
        .I1(s_axi_CONTROL_BUS_WVALID),
        .I2(\waddr_reg_n_0_[1] ),
        .I3(\waddr_reg_n_0_[0] ),
        .I4(out[1]),
        .O(\int_searched[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[3] ),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[4] ),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[5] ),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[6] ),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\int_searched_reg_n_0_[7] ),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[8] ),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\int_searched_reg_n_0_[9] ),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [0]),
        .Q(\int_searched_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [10]),
        .Q(\int_searched_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [11]),
        .Q(\int_searched_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [12]),
        .Q(\int_searched_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [13]),
        .Q(\int_searched_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [14]),
        .Q(\int_searched_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [15]),
        .Q(\int_searched_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [16]),
        .Q(\int_searched_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [17]),
        .Q(\int_searched_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [18]),
        .Q(\int_searched_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [19]),
        .Q(\int_searched_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [1]),
        .Q(\int_searched_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [20]),
        .Q(\int_searched_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [21]),
        .Q(\int_searched_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [22]),
        .Q(\int_searched_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [23]),
        .Q(\int_searched_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [24]),
        .Q(\int_searched_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [25]),
        .Q(\int_searched_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [26]),
        .Q(\int_searched_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [27]),
        .Q(\int_searched_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [28]),
        .Q(\int_searched_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [29]),
        .Q(\int_searched_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [2]),
        .Q(\int_searched_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [30]),
        .Q(\int_searched_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [31]),
        .Q(\int_searched_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [3]),
        .Q(\int_searched_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [4]),
        .Q(\int_searched_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [5]),
        .Q(\int_searched_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [6]),
        .Q(\int_searched_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [7]),
        .Q(\int_searched_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [8]),
        .Q(\int_searched_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [9]),
        .Q(\int_searched_reg_n_0_[9] ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA8)) 
    interrupt_INST_0
       (.I0(int_gie_reg_n_0),
        .I1(p_1_in),
        .I2(\int_isr_reg_n_0_[0] ),
        .O(interrupt));
  LUT6 #(
    .INIT(64'h005DFFFF005D0000)) 
    \rdata_data[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(\rdata_data[0]_i_2_n_0 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(\rdata_data[0]_i_3_n_0 ),
        .I4(ar_hs),
        .I5(s_axi_CONTROL_BUS_RDATA[0]),
        .O(\rdata_data[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \rdata_data[0]_i_2 
       (.I0(int_agg_result_b_ap_vld),
        .I1(\int_isr_reg_n_0_[0] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(int_agg_result_a_ap_vld),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(int_gie_reg_n_0),
        .O(\rdata_data[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEFAAEFEFAAAAAAAA)) 
    \rdata_data[0]_i_3 
       (.I0(int_ap_done_i_3_n_0),
        .I1(\rdata_data[0]_i_4_n_0 ),
        .I2(\int_searched_reg_n_0_[0] ),
        .I3(int_ap_done_i_2_n_0),
        .I4(ap_start),
        .I5(\rdata_data[0]_i_5_n_0 ),
        .O(\rdata_data[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \rdata_data[0]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFF0F77)) 
    \rdata_data[0]_i_5 
       (.I0(\int_ier_reg_n_0_[0] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(int_agg_result_a[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[0]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[10]_i_1 
       (.I0(int_agg_result_a[10]),
        .I1(\int_searched_reg_n_0_[10] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[11]_i_1 
       (.I0(int_agg_result_a[11]),
        .I1(\int_searched_reg_n_0_[11] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[12]_i_1 
       (.I0(int_agg_result_a[12]),
        .I1(\int_searched_reg_n_0_[12] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[13]_i_1 
       (.I0(int_agg_result_a[13]),
        .I1(\int_searched_reg_n_0_[13] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[14]_i_1 
       (.I0(int_agg_result_a[14]),
        .I1(\int_searched_reg_n_0_[14] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[15]_i_1 
       (.I0(int_agg_result_a[15]),
        .I1(\int_searched_reg_n_0_[15] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[16]_i_1 
       (.I0(int_agg_result_a[16]),
        .I1(\int_searched_reg_n_0_[16] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[17]_i_1 
       (.I0(int_agg_result_a[17]),
        .I1(\int_searched_reg_n_0_[17] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[18]_i_1 
       (.I0(int_agg_result_a[18]),
        .I1(\int_searched_reg_n_0_[18] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1010101010101000)) 
    \rdata_data[19]_i_1 
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .I3(s_axi_CONTROL_BUS_ARADDR[1]),
        .I4(s_axi_CONTROL_BUS_ARADDR[0]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[19]_i_2 
       (.I0(int_agg_result_a[19]),
        .I1(\int_searched_reg_n_0_[19] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h02FF0200)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[1]_i_2_n_0 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(ar_hs),
        .I4(s_axi_CONTROL_BUS_RDATA[1]),
        .O(\rdata_data[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2F2F2F2F202F2020)) 
    \rdata_data[1]_i_2 
       (.I0(p_1_in),
        .I1(\rdata_data[1]_i_3_n_0 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .I3(\rdata_data[1]_i_4_n_0 ),
        .I4(int_agg_result_a[1]),
        .I5(\rdata_data[1]_i_5_n_0 ),
        .O(\rdata_data[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \rdata_data[1]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \rdata_data[1]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[5]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0300232303002020)) 
    \rdata_data[1]_i_5 
       (.I0(p_0_in),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(\int_searched_reg_n_0_[1] ),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(int_ap_done),
        .O(\rdata_data[1]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[20]_i_1 
       (.I0(\int_searched_reg_n_0_[20] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[21]_i_1 
       (.I0(\int_searched_reg_n_0_[21] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[22]_i_1 
       (.I0(\int_searched_reg_n_0_[22] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[23]_i_1 
       (.I0(\int_searched_reg_n_0_[23] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[24]_i_1 
       (.I0(\int_searched_reg_n_0_[24] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[25]_i_1 
       (.I0(\int_searched_reg_n_0_[25] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[26]_i_1 
       (.I0(\int_searched_reg_n_0_[26] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[27]_i_1 
       (.I0(\int_searched_reg_n_0_[27] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[28]_i_1 
       (.I0(\int_searched_reg_n_0_[28] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[29]_i_1 
       (.I0(\int_searched_reg_n_0_[29] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000F0CCCC00AA)) 
    \rdata_data[2]_i_1 
       (.I0(int_ap_idle),
        .I1(int_agg_result_a[2]),
        .I2(\int_searched_reg_n_0_[2] ),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[30]_i_1 
       (.I0(\int_searched_reg_n_0_[30] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAAA8A)) 
    \rdata_data[31]_i_1 
       (.I0(ar_hs),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(rstate[0]),
        .I2(rstate[1]),
        .O(ar_hs));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[31]_i_3 
       (.I0(\int_searched_reg_n_0_[31] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00C00FCA00C000CA)) 
    \rdata_data[3]_i_1 
       (.I0(int_ap_ready),
        .I1(int_agg_result_a[3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\int_searched_reg_n_0_[3] ),
        .O(\rdata_data[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[4]_i_1 
       (.I0(int_agg_result_a[4]),
        .I1(\int_searched_reg_n_0_[4] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[5]_i_1 
       (.I0(int_agg_result_a[5]),
        .I1(\int_searched_reg_n_0_[5] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[6]_i_1 
       (.I0(int_agg_result_a[6]),
        .I1(\int_searched_reg_n_0_[6] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000F0AAAA00CC)) 
    \rdata_data[7]_i_1 
       (.I0(int_agg_result_a[7]),
        .I1(int_auto_restart),
        .I2(\int_searched_reg_n_0_[7] ),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[8]_i_1 
       (.I0(int_agg_result_a[8]),
        .I1(\int_searched_reg_n_0_[8] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000CAA00)) 
    \rdata_data[9]_i_1 
       (.I0(int_agg_result_a[9]),
        .I1(\int_searched_reg_n_0_[9] ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[9]_i_1_n_0 ));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rdata_data[0]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[10]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[11]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[12]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[13]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[14]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[15]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[16]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[17]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[18]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[19]_i_2_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rdata_data[1]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[20]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[21]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[22]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[23]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[24]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[25]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[26]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[27]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[28]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[29]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[2]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[30]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[31]_i_3_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(\rdata_data[31]_i_1_n_0 ));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[3]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[4]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[5]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[6]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[7]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[8]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .R(\rdata_data[19]_i_1_n_0 ));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[9]_i_1_n_0 ),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .R(\rdata_data[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h003A)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(s_axi_CONTROL_BUS_RREADY),
        .I2(rstate[0]),
        .I3(rstate[1]),
        .O(\rstate[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_0 ),
        .Q(rstate[0]),
        .R(ARESET));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(ARESET));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[5]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \waddr_reg[5] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[5]),
        .Q(\waddr_reg_n_0_[5] ),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    LAST_STREAM_TVALID,
    LAST_STREAM_TREADY,
    LAST_STREAM_TDATA,
    LAST_STREAM_TDEST,
    LAST_STREAM_TKEEP,
    LAST_STREAM_TSTRB,
    LAST_STREAM_TUSER,
    LAST_STREAM_TLAST,
    LAST_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 6, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 1e+08, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [5:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM:LAST_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME LAST_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input LAST_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TREADY" *) output LAST_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TDATA" *) input [31:0]LAST_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TDEST" *) input [5:0]LAST_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TKEEP" *) input [3:0]LAST_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TSTRB" *) input [3:0]LAST_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TUSER" *) input [1:0]LAST_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TLAST" *) input [0:0]LAST_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 LAST_STREAM TID" *) input [4:0]LAST_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire [31:0]LAST_STREAM_TDATA;
  wire [5:0]LAST_STREAM_TDEST;
  wire [4:0]LAST_STREAM_TID;
  wire [3:0]LAST_STREAM_TKEEP;
  wire [0:0]LAST_STREAM_TLAST;
  wire LAST_STREAM_TREADY;
  wire [3:0]LAST_STREAM_TSTRB;
  wire [1:0]LAST_STREAM_TUSER;
  wire LAST_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .LAST_STREAM_TDATA(LAST_STREAM_TDATA),
        .LAST_STREAM_TDEST(LAST_STREAM_TDEST),
        .LAST_STREAM_TID(LAST_STREAM_TID),
        .LAST_STREAM_TKEEP(LAST_STREAM_TKEEP),
        .LAST_STREAM_TLAST(LAST_STREAM_TLAST),
        .LAST_STREAM_TREADY(LAST_STREAM_TREADY),
        .LAST_STREAM_TSTRB(LAST_STREAM_TSTRB),
        .LAST_STREAM_TUSER(LAST_STREAM_TUSER),
        .LAST_STREAM_TVALID(LAST_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
