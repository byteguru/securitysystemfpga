// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Fri Feb 23 21:12:55 2018
// Host        : DESKTOP-871TSOM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ system_Adder2_0_0_sim_netlist.v
// Design      : system_Adder2_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2
   (ap_clk,
    ap_rst_n,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID,
    INPUT_STREAM_TDEST,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_BRESP,
    interrupt);
  input ap_clk;
  input ap_rst_n;
  input [31:0]INPUT_STREAM_TDATA;
  input INPUT_STREAM_TVALID;
  output INPUT_STREAM_TREADY;
  input [3:0]INPUT_STREAM_TKEEP;
  input [3:0]INPUT_STREAM_TSTRB;
  input [1:0]INPUT_STREAM_TUSER;
  input [0:0]INPUT_STREAM_TLAST;
  input [4:0]INPUT_STREAM_TID;
  input [5:0]INPUT_STREAM_TDEST;
  input s_axi_CONTROL_BUS_AWVALID;
  output s_axi_CONTROL_BUS_AWREADY;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input s_axi_CONTROL_BUS_WVALID;
  output s_axi_CONTROL_BUS_WREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_ARVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  output s_axi_CONTROL_BUS_RVALID;
  input s_axi_CONTROL_BUS_RREADY;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  output [1:0]s_axi_CONTROL_BUS_RRESP;
  output s_axi_CONTROL_BUS_BVALID;
  input s_axi_CONTROL_BUS_BREADY;
  output [1:0]s_axi_CONTROL_BUS_BRESP;
  output interrupt;

  wire \<const0> ;
  wire [31:0]INPUT_STREAM_TDATA;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire INPUT_STREAM_TVALID;
  wire INPUT_STREAM_V_data_V_0_ack_in;
  wire INPUT_STREAM_V_data_V_0_load_A;
  wire INPUT_STREAM_V_data_V_0_load_B;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_A;
  wire [31:0]INPUT_STREAM_V_data_V_0_payload_B;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_3;
  wire INPUT_STREAM_V_data_V_0_sel_wr;
  wire INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_3;
  wire [1:1]INPUT_STREAM_V_data_V_0_state;
  wire \INPUT_STREAM_V_data_V_0_state[0]_i_1_n_3 ;
  wire \INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ;
  wire [1:1]INPUT_STREAM_V_dest_V_0_state;
  wire \INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_3 ;
  wire \INPUT_STREAM_V_dest_V_0_state_reg_n_3_[0] ;
  wire INPUT_STREAM_V_last_V_0_ack_in;
  wire INPUT_STREAM_V_last_V_0_payload_A;
  wire \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_3 ;
  wire INPUT_STREAM_V_last_V_0_payload_B;
  wire \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_3 ;
  wire INPUT_STREAM_V_last_V_0_sel;
  wire INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_3;
  wire INPUT_STREAM_V_last_V_0_sel_wr;
  wire INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_3;
  wire [1:1]INPUT_STREAM_V_last_V_0_state;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_1_n_3 ;
  wire \INPUT_STREAM_V_last_V_0_state[0]_i_2_n_3 ;
  wire \INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ;
  wire a_a_1_reg_197;
  wire \a_a_1_reg_197_reg_n_3_[0] ;
  wire \a_a_1_reg_197_reg_n_3_[1] ;
  wire \a_a_1_reg_197_reg_n_3_[2] ;
  wire \a_a_1_reg_197_reg_n_3_[3] ;
  wire \a_a_1_reg_197_reg_n_3_[4] ;
  wire \a_a_1_reg_197_reg_n_3_[5] ;
  wire [31:1]a_a_fu_302_p2;
  wire agg_result_a_ap_vld;
  wire \ap_CS_fsm_reg_n_3_[0] ;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state5;
  wire ap_CS_fsm_state6;
  wire ap_CS_fsm_state7;
  wire ap_CS_fsm_state9;
  wire [8:0]ap_NS_fsm;
  wire ap_NS_fsm11_out;
  wire ap_NS_fsm15_out;
  wire ap_NS_fsm17_out;
  wire ap_NS_fsm18_out;
  wire ap_clk;
  wire ap_phi_mux_storemerge_phi_fu_212_p41;
  wire ap_rst_n;
  wire ce0;
  wire [4:0]d0;
  wire [5:0]i_1_fu_249_p2;
  wire [5:0]i_2_fu_277_p2;
  wire [5:0]i_2_reg_354;
  wire \i_reg_186_reg_n_3_[0] ;
  wire \i_reg_186_reg_n_3_[1] ;
  wire \i_reg_186_reg_n_3_[2] ;
  wire \i_reg_186_reg_n_3_[3] ;
  wire \i_reg_186_reg_n_3_[4] ;
  wire [6:0]indvarinc_fu_219_p2;
  wire [6:0]indvarinc_reg_320;
  wire \indvarinc_reg_320[6]_i_2_n_3 ;
  wire interrupt;
  wire invdar_reg_175;
  wire invdar_reg_1750;
  wire \invdar_reg_175_reg_n_3_[0] ;
  wire \invdar_reg_175_reg_n_3_[1] ;
  wire \invdar_reg_175_reg_n_3_[2] ;
  wire \invdar_reg_175_reg_n_3_[3] ;
  wire \invdar_reg_175_reg_n_3_[4] ;
  wire \invdar_reg_175_reg_n_3_[5] ;
  wire \invdar_reg_175_reg_n_3_[6] ;
  wire \last[0]_i_2_n_3 ;
  wire [31:0]last_reg;
  wire \last_reg[0]_i_1_n_10 ;
  wire \last_reg[0]_i_1_n_3 ;
  wire \last_reg[0]_i_1_n_4 ;
  wire \last_reg[0]_i_1_n_5 ;
  wire \last_reg[0]_i_1_n_6 ;
  wire \last_reg[0]_i_1_n_7 ;
  wire \last_reg[0]_i_1_n_8 ;
  wire \last_reg[0]_i_1_n_9 ;
  wire \last_reg[12]_i_1_n_10 ;
  wire \last_reg[12]_i_1_n_3 ;
  wire \last_reg[12]_i_1_n_4 ;
  wire \last_reg[12]_i_1_n_5 ;
  wire \last_reg[12]_i_1_n_6 ;
  wire \last_reg[12]_i_1_n_7 ;
  wire \last_reg[12]_i_1_n_8 ;
  wire \last_reg[12]_i_1_n_9 ;
  wire \last_reg[16]_i_1_n_10 ;
  wire \last_reg[16]_i_1_n_3 ;
  wire \last_reg[16]_i_1_n_4 ;
  wire \last_reg[16]_i_1_n_5 ;
  wire \last_reg[16]_i_1_n_6 ;
  wire \last_reg[16]_i_1_n_7 ;
  wire \last_reg[16]_i_1_n_8 ;
  wire \last_reg[16]_i_1_n_9 ;
  wire \last_reg[20]_i_1_n_10 ;
  wire \last_reg[20]_i_1_n_3 ;
  wire \last_reg[20]_i_1_n_4 ;
  wire \last_reg[20]_i_1_n_5 ;
  wire \last_reg[20]_i_1_n_6 ;
  wire \last_reg[20]_i_1_n_7 ;
  wire \last_reg[20]_i_1_n_8 ;
  wire \last_reg[20]_i_1_n_9 ;
  wire \last_reg[24]_i_1_n_10 ;
  wire \last_reg[24]_i_1_n_3 ;
  wire \last_reg[24]_i_1_n_4 ;
  wire \last_reg[24]_i_1_n_5 ;
  wire \last_reg[24]_i_1_n_6 ;
  wire \last_reg[24]_i_1_n_7 ;
  wire \last_reg[24]_i_1_n_8 ;
  wire \last_reg[24]_i_1_n_9 ;
  wire \last_reg[28]_i_1_n_10 ;
  wire \last_reg[28]_i_1_n_4 ;
  wire \last_reg[28]_i_1_n_5 ;
  wire \last_reg[28]_i_1_n_6 ;
  wire \last_reg[28]_i_1_n_7 ;
  wire \last_reg[28]_i_1_n_8 ;
  wire \last_reg[28]_i_1_n_9 ;
  wire \last_reg[4]_i_1_n_10 ;
  wire \last_reg[4]_i_1_n_3 ;
  wire \last_reg[4]_i_1_n_4 ;
  wire \last_reg[4]_i_1_n_5 ;
  wire \last_reg[4]_i_1_n_6 ;
  wire \last_reg[4]_i_1_n_7 ;
  wire \last_reg[4]_i_1_n_8 ;
  wire \last_reg[4]_i_1_n_9 ;
  wire \last_reg[8]_i_1_n_10 ;
  wire \last_reg[8]_i_1_n_3 ;
  wire \last_reg[8]_i_1_n_4 ;
  wire \last_reg[8]_i_1_n_5 ;
  wire \last_reg[8]_i_1_n_6 ;
  wire \last_reg[8]_i_1_n_7 ;
  wire \last_reg[8]_i_1_n_8 ;
  wire \last_reg[8]_i_1_n_9 ;
  wire [31:0]p_2_in;
  wire reset;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]searched;
  wire [31:0]searched_read_reg_314;
  wire [31:0]storemerge_reg_209;
  wire \storemerge_reg_209[31]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[12]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[12]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[12]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[12]_i_2_n_6 ;
  wire \storemerge_reg_209_reg[16]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[16]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[16]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[16]_i_2_n_6 ;
  wire \storemerge_reg_209_reg[20]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[20]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[20]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[20]_i_2_n_6 ;
  wire \storemerge_reg_209_reg[24]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[24]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[24]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[24]_i_2_n_6 ;
  wire \storemerge_reg_209_reg[28]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[28]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[28]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[28]_i_2_n_6 ;
  wire \storemerge_reg_209_reg[31]_i_4_n_5 ;
  wire \storemerge_reg_209_reg[31]_i_4_n_6 ;
  wire \storemerge_reg_209_reg[4]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[4]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[4]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[4]_i_2_n_6 ;
  wire \storemerge_reg_209_reg[8]_i_2_n_3 ;
  wire \storemerge_reg_209_reg[8]_i_2_n_4 ;
  wire \storemerge_reg_209_reg[8]_i_2_n_5 ;
  wire \storemerge_reg_209_reg[8]_i_2_n_6 ;
  wire \tmp_1_reg_335[0]_i_1_n_3 ;
  wire \tmp_1_reg_335[0]_i_2_n_3 ;
  wire \tmp_1_reg_335_reg_n_3_[0] ;
  wire tmp_2_fu_241_p3;
  wire tmp_6_reg_350;
  wire tmp_8_fu_288_p2;
  wire [6:0]tmp_reg_325;
  wire [3:3]\NLW_last_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_storemerge_reg_209_reg[31]_i_4_CO_UNCONNECTED ;
  wire [3:3]\NLW_storemerge_reg_209_reg[31]_i_4_O_UNCONNECTED ;

  assign s_axi_CONTROL_BUS_BRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_BRESP[0] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[1] = \<const0> ;
  assign s_axi_CONTROL_BUS_RRESP[0] = \<const0> ;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi Adder2_CONTROL_BUS_s_axi_U
       (.D(ap_NS_fsm[1:0]),
        .E(ap_NS_fsm18_out),
        .Q({agg_result_a_ap_vld,ap_CS_fsm_state3,\ap_CS_fsm_reg_n_3_[0] }),
        .SR(invdar_reg_175),
        .\a_a_1_reg_197_reg[5] ({\a_a_1_reg_197_reg_n_3_[5] ,\a_a_1_reg_197_reg_n_3_[4] ,\a_a_1_reg_197_reg_n_3_[3] ,\a_a_1_reg_197_reg_n_3_[2] ,\a_a_1_reg_197_reg_n_3_[1] ,\a_a_1_reg_197_reg_n_3_[0] }),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .out({s_axi_CONTROL_BUS_BVALID,s_axi_CONTROL_BUS_WREADY,s_axi_CONTROL_BUS_AWREADY}),
        .reset(reset),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID),
        .\searched_read_reg_314_reg[31] (searched),
        .\searched_read_reg_314_reg[31]_0 (searched_read_reg_314),
        .\storemerge_reg_209_reg[31] (storemerge_reg_209),
        .\tmp_1_reg_335_reg[0] (\tmp_1_reg_335_reg_n_3_[0] ),
        .tmp_6_reg_350(tmp_6_reg_350));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb Adder2_stream_int_i_U
       (.DIADI(d0),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[4] (INPUT_STREAM_V_data_V_0_payload_B[4:0]),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4:0]),
        .\ap_CS_fsm_reg[3] ({ap_CS_fsm_state4,ce0}),
        .ap_clk(ap_clk),
        .\invdar_reg_175_reg[6] ({\invdar_reg_175_reg_n_3_[6] ,\invdar_reg_175_reg_n_3_[5] ,\invdar_reg_175_reg_n_3_[4] ,\invdar_reg_175_reg_n_3_[3] ,\invdar_reg_175_reg_n_3_[2] ,\invdar_reg_175_reg_n_3_[1] ,\invdar_reg_175_reg_n_3_[0] }));
  GND GND
       (.G(\<const0> ));
  LUT3 #(
    .INIT(8'h0D)) 
    \INPUT_STREAM_V_data_V_0_payload_A[31]_i_1 
       (.I0(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_load_A));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_A),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA2)) 
    \INPUT_STREAM_V_data_V_0_payload_B[31]_i_1 
       (.I0(INPUT_STREAM_V_data_V_0_sel_wr),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(INPUT_STREAM_V_data_V_0_ack_in),
        .O(INPUT_STREAM_V_data_V_0_load_B));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[0]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[0]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[10]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[10]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[11]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[11]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[12]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[12]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[13]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[13]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[14]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[14]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[15]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[15]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[16]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[16]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[17]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[17]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[18]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[18]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[19]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[19]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[1]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[1]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[20]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[20]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[21]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[21]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[22]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[22]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[23]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[23]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[24]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[24]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[25]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[25]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[26]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[26]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[27]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[27]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[28]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[28]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[29]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[29]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[2]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[2]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[30]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[30]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[31]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[31]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[3]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[3]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[4]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[4]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[5]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[5]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[6]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[6]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[7]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[7]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[8]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[8]),
        .R(1'b0));
  FDRE \INPUT_STREAM_V_data_V_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(INPUT_STREAM_V_data_V_0_load_B),
        .D(INPUT_STREAM_TDATA[9]),
        .Q(INPUT_STREAM_V_data_V_0_payload_B[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBF40)) 
    INPUT_STREAM_V_data_V_0_sel_rd_i_1
       (.I0(tmp_2_fu_241_p3),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(ap_CS_fsm_state4),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .O(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_3));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_rd_i_1_n_3),
        .Q(INPUT_STREAM_V_data_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_data_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_data_V_0_ack_in),
        .I2(INPUT_STREAM_V_data_V_0_sel_wr),
        .O(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_3));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_data_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_sel_wr_i_1_n_3),
        .Q(INPUT_STREAM_V_data_V_0_sel_wr),
        .R(reset));
  LUT6 #(
    .INIT(64'hAAAA80A0A0A0A0A0)) 
    \INPUT_STREAM_V_data_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(tmp_2_fu_241_p3),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I3(ap_CS_fsm_state4),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_V_data_V_0_ack_in),
        .O(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_3 ));
  LUT5 #(
    .INIT(32'h3B3BFF3B)) 
    \INPUT_STREAM_V_data_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(tmp_2_fu_241_p3),
        .I3(INPUT_STREAM_V_data_V_0_ack_in),
        .I4(INPUT_STREAM_TVALID),
        .O(INPUT_STREAM_V_data_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_data_V_0_state[0]_i_1_n_3 ),
        .Q(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_data_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_data_V_0_state),
        .Q(INPUT_STREAM_V_data_V_0_ack_in),
        .R(reset));
  LUT5 #(
    .INIT(32'h8A80AA80)) 
    \INPUT_STREAM_V_dest_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(INPUT_STREAM_TVALID),
        .I2(INPUT_STREAM_TREADY),
        .I3(\INPUT_STREAM_V_dest_V_0_state_reg_n_3_[0] ),
        .I4(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_3 ),
        .O(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h55D5FFFF55D555D5)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_2 
       (.I0(\INPUT_STREAM_V_dest_V_0_state_reg_n_3_[0] ),
        .I1(ap_CS_fsm_state4),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I3(tmp_2_fu_241_p3),
        .I4(INPUT_STREAM_TVALID),
        .I5(INPUT_STREAM_TREADY),
        .O(INPUT_STREAM_V_dest_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_dest_V_0_state[0]_i_1_n_3 ),
        .Q(\INPUT_STREAM_V_dest_V_0_state_reg_n_3_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_dest_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_dest_V_0_state),
        .Q(INPUT_STREAM_TREADY),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \INPUT_STREAM_V_last_V_0_payload_A[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_ack_in),
        .I3(INPUT_STREAM_V_last_V_0_sel_wr),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .O(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_3 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_A[0]_i_1_n_3 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hBBFB8808)) 
    \INPUT_STREAM_V_last_V_0_payload_B[0]_i_1 
       (.I0(INPUT_STREAM_TLAST),
        .I1(INPUT_STREAM_V_last_V_0_sel_wr),
        .I2(\INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ),
        .I3(INPUT_STREAM_V_last_V_0_ack_in),
        .I4(INPUT_STREAM_V_last_V_0_payload_B),
        .O(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_3 ));
  FDRE \INPUT_STREAM_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_payload_B[0]_i_1_n_3 ),
        .Q(INPUT_STREAM_V_last_V_0_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    INPUT_STREAM_V_last_V_0_sel_rd_i_1
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(tmp_2_fu_241_p3),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_sel),
        .O(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_3));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_rd_i_1_n_3),
        .Q(INPUT_STREAM_V_last_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h78)) 
    INPUT_STREAM_V_last_V_0_sel_wr_i_1
       (.I0(INPUT_STREAM_TVALID),
        .I1(INPUT_STREAM_V_last_V_0_ack_in),
        .I2(INPUT_STREAM_V_last_V_0_sel_wr),
        .O(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_3));
  FDRE #(
    .INIT(1'b0)) 
    INPUT_STREAM_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_sel_wr_i_1_n_3),
        .Q(INPUT_STREAM_V_last_V_0_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hAA088888)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(\INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ),
        .I2(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_3 ),
        .I3(INPUT_STREAM_TVALID),
        .I4(INPUT_STREAM_V_last_V_0_ack_in),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \INPUT_STREAM_V_last_V_0_state[0]_i_2 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(tmp_2_fu_241_p3),
        .O(\INPUT_STREAM_V_last_V_0_state[0]_i_2_n_3 ));
  LUT6 #(
    .INIT(64'h08FF08FFFFFF08FF)) 
    \INPUT_STREAM_V_last_V_0_state[1]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(tmp_2_fu_241_p3),
        .I3(\INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ),
        .I4(INPUT_STREAM_V_last_V_0_ack_in),
        .I5(INPUT_STREAM_TVALID),
        .O(INPUT_STREAM_V_last_V_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\INPUT_STREAM_V_last_V_0_state[0]_i_1_n_3 ),
        .Q(\INPUT_STREAM_V_last_V_0_state_reg_n_3_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \INPUT_STREAM_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(INPUT_STREAM_V_last_V_0_state),
        .Q(INPUT_STREAM_V_last_V_0_ack_in),
        .R(reset));
  LUT4 #(
    .INIT(16'hAA8A)) 
    \a_a_1_reg_197[5]_i_1 
       (.I0(ap_CS_fsm_state5),
        .I1(tmp_6_reg_350),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_8_fu_288_p2),
        .O(a_a_1_reg_197));
  LUT3 #(
    .INIT(8'h04)) 
    \a_a_1_reg_197[5]_i_2 
       (.I0(tmp_8_fu_288_p2),
        .I1(ap_CS_fsm_state7),
        .I2(tmp_6_reg_350),
        .O(ap_NS_fsm11_out));
  FDRE \a_a_1_reg_197_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(i_2_reg_354[0]),
        .Q(\a_a_1_reg_197_reg_n_3_[0] ),
        .R(a_a_1_reg_197));
  FDRE \a_a_1_reg_197_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(i_2_reg_354[1]),
        .Q(\a_a_1_reg_197_reg_n_3_[1] ),
        .R(a_a_1_reg_197));
  FDRE \a_a_1_reg_197_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(i_2_reg_354[2]),
        .Q(\a_a_1_reg_197_reg_n_3_[2] ),
        .R(a_a_1_reg_197));
  FDRE \a_a_1_reg_197_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(i_2_reg_354[3]),
        .Q(\a_a_1_reg_197_reg_n_3_[3] ),
        .R(a_a_1_reg_197));
  FDRE \a_a_1_reg_197_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(i_2_reg_354[4]),
        .Q(\a_a_1_reg_197_reg_n_3_[4] ),
        .R(a_a_1_reg_197));
  FDRE \a_a_1_reg_197_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm11_out),
        .D(i_2_reg_354[5]),
        .Q(\a_a_1_reg_197_reg_n_3_[5] ),
        .R(a_a_1_reg_197));
  LUT4 #(
    .INIT(16'hAA30)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(\tmp_1_reg_335_reg_n_3_[0] ),
        .I1(ap_NS_fsm[4]),
        .I2(ap_CS_fsm_state4),
        .I3(ap_CS_fsm_state3),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'hAAAAAAAA80888000)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(ap_CS_fsm_state4),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I2(INPUT_STREAM_V_last_V_0_payload_B),
        .I3(INPUT_STREAM_V_last_V_0_sel),
        .I4(INPUT_STREAM_V_last_V_0_payload_A),
        .I5(tmp_2_fu_241_p3),
        .O(ap_NS_fsm[4]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'hAABA)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(ap_CS_fsm_state5),
        .I1(tmp_6_reg_350),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_8_fu_288_p2),
        .O(ap_NS_fsm[5]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'hBAAA)) 
    \ap_CS_fsm[7]_i_1 
       (.I0(ap_CS_fsm_state9),
        .I1(tmp_6_reg_350),
        .I2(ap_CS_fsm_state7),
        .I3(tmp_8_fu_288_p2),
        .O(ap_NS_fsm[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \ap_CS_fsm[8]_i_1 
       (.I0(tmp_6_reg_350),
        .I1(ap_CS_fsm_state7),
        .O(ap_NS_fsm[8]));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_3_[0] ),
        .S(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ce0),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ce0),
        .Q(ap_CS_fsm_state3),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_state4),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state5),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state6),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_CS_fsm_state6),
        .Q(ap_CS_fsm_state7),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[7] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[7]),
        .Q(agg_result_a_ap_vld),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[8] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[8]),
        .Q(ap_CS_fsm_state9),
        .R(reset));
  LUT1 #(
    .INIT(2'h1)) 
    \i_2_reg_354[0]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[0] ),
        .O(i_2_fu_277_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_2_reg_354[1]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[0] ),
        .I1(\a_a_1_reg_197_reg_n_3_[1] ),
        .O(i_2_fu_277_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_2_reg_354[2]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[0] ),
        .I1(\a_a_1_reg_197_reg_n_3_[1] ),
        .I2(\a_a_1_reg_197_reg_n_3_[2] ),
        .O(i_2_fu_277_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_2_reg_354[3]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[3] ),
        .I1(\a_a_1_reg_197_reg_n_3_[0] ),
        .I2(\a_a_1_reg_197_reg_n_3_[1] ),
        .I3(\a_a_1_reg_197_reg_n_3_[2] ),
        .O(i_2_fu_277_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_2_reg_354[4]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[4] ),
        .I1(\a_a_1_reg_197_reg_n_3_[2] ),
        .I2(\a_a_1_reg_197_reg_n_3_[1] ),
        .I3(\a_a_1_reg_197_reg_n_3_[0] ),
        .I4(\a_a_1_reg_197_reg_n_3_[3] ),
        .O(i_2_fu_277_p2[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \i_2_reg_354[5]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[5] ),
        .I1(\a_a_1_reg_197_reg_n_3_[3] ),
        .I2(\a_a_1_reg_197_reg_n_3_[0] ),
        .I3(\a_a_1_reg_197_reg_n_3_[1] ),
        .I4(\a_a_1_reg_197_reg_n_3_[2] ),
        .I5(\a_a_1_reg_197_reg_n_3_[4] ),
        .O(i_2_fu_277_p2[5]));
  FDRE \i_2_reg_354_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_2_fu_277_p2[0]),
        .Q(i_2_reg_354[0]),
        .R(1'b0));
  FDRE \i_2_reg_354_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_2_fu_277_p2[1]),
        .Q(i_2_reg_354[1]),
        .R(1'b0));
  FDRE \i_2_reg_354_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_2_fu_277_p2[2]),
        .Q(i_2_reg_354[2]),
        .R(1'b0));
  FDRE \i_2_reg_354_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_2_fu_277_p2[3]),
        .Q(i_2_reg_354[3]),
        .R(1'b0));
  FDRE \i_2_reg_354_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_2_fu_277_p2[4]),
        .Q(i_2_reg_354[4]),
        .R(1'b0));
  FDRE \i_2_reg_354_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(i_2_fu_277_p2[5]),
        .Q(i_2_reg_354[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_186[0]_i_1 
       (.I0(\i_reg_186_reg_n_3_[0] ),
        .O(i_1_fu_249_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_reg_186[1]_i_1 
       (.I0(\i_reg_186_reg_n_3_[0] ),
        .I1(\i_reg_186_reg_n_3_[1] ),
        .O(i_1_fu_249_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_reg_186[2]_i_1 
       (.I0(\i_reg_186_reg_n_3_[0] ),
        .I1(\i_reg_186_reg_n_3_[1] ),
        .I2(\i_reg_186_reg_n_3_[2] ),
        .O(i_1_fu_249_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \i_reg_186[3]_i_1 
       (.I0(\i_reg_186_reg_n_3_[3] ),
        .I1(\i_reg_186_reg_n_3_[0] ),
        .I2(\i_reg_186_reg_n_3_[1] ),
        .I3(\i_reg_186_reg_n_3_[2] ),
        .O(i_1_fu_249_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \i_reg_186[4]_i_1 
       (.I0(\i_reg_186_reg_n_3_[4] ),
        .I1(\i_reg_186_reg_n_3_[2] ),
        .I2(\i_reg_186_reg_n_3_[1] ),
        .I3(\i_reg_186_reg_n_3_[0] ),
        .I4(\i_reg_186_reg_n_3_[3] ),
        .O(i_1_fu_249_p2[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \i_reg_186[5]_i_1 
       (.I0(ap_CS_fsm_state3),
        .I1(\tmp_1_reg_335_reg_n_3_[0] ),
        .O(ap_NS_fsm17_out));
  LUT6 #(
    .INIT(64'h0000000047000000)) 
    \i_reg_186[5]_i_2 
       (.I0(INPUT_STREAM_V_last_V_0_payload_B),
        .I1(INPUT_STREAM_V_last_V_0_sel),
        .I2(INPUT_STREAM_V_last_V_0_payload_A),
        .I3(ap_CS_fsm_state4),
        .I4(\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .I5(tmp_2_fu_241_p3),
        .O(ap_NS_fsm15_out));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \i_reg_186[5]_i_3 
       (.I0(\i_reg_186_reg_n_3_[3] ),
        .I1(\i_reg_186_reg_n_3_[0] ),
        .I2(\i_reg_186_reg_n_3_[1] ),
        .I3(\i_reg_186_reg_n_3_[2] ),
        .I4(\i_reg_186_reg_n_3_[4] ),
        .O(i_1_fu_249_p2[5]));
  FDRE \i_reg_186_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm15_out),
        .D(i_1_fu_249_p2[0]),
        .Q(\i_reg_186_reg_n_3_[0] ),
        .R(ap_NS_fsm17_out));
  FDRE \i_reg_186_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm15_out),
        .D(i_1_fu_249_p2[1]),
        .Q(\i_reg_186_reg_n_3_[1] ),
        .R(ap_NS_fsm17_out));
  FDRE \i_reg_186_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm15_out),
        .D(i_1_fu_249_p2[2]),
        .Q(\i_reg_186_reg_n_3_[2] ),
        .R(ap_NS_fsm17_out));
  FDRE \i_reg_186_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm15_out),
        .D(i_1_fu_249_p2[3]),
        .Q(\i_reg_186_reg_n_3_[3] ),
        .R(ap_NS_fsm17_out));
  FDRE \i_reg_186_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm15_out),
        .D(i_1_fu_249_p2[4]),
        .Q(\i_reg_186_reg_n_3_[4] ),
        .R(ap_NS_fsm17_out));
  FDRE \i_reg_186_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm15_out),
        .D(i_1_fu_249_p2[5]),
        .Q(tmp_2_fu_241_p3),
        .R(ap_NS_fsm17_out));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \indvarinc_reg_320[0]_i_1 
       (.I0(\invdar_reg_175_reg_n_3_[0] ),
        .O(indvarinc_fu_219_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \indvarinc_reg_320[1]_i_1 
       (.I0(\invdar_reg_175_reg_n_3_[0] ),
        .I1(\invdar_reg_175_reg_n_3_[1] ),
        .O(indvarinc_fu_219_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \indvarinc_reg_320[2]_i_1 
       (.I0(\invdar_reg_175_reg_n_3_[0] ),
        .I1(\invdar_reg_175_reg_n_3_[1] ),
        .I2(\invdar_reg_175_reg_n_3_[2] ),
        .O(indvarinc_fu_219_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \indvarinc_reg_320[3]_i_1 
       (.I0(\invdar_reg_175_reg_n_3_[1] ),
        .I1(\invdar_reg_175_reg_n_3_[0] ),
        .I2(\invdar_reg_175_reg_n_3_[2] ),
        .I3(\invdar_reg_175_reg_n_3_[3] ),
        .O(indvarinc_fu_219_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \indvarinc_reg_320[4]_i_1 
       (.I0(\invdar_reg_175_reg_n_3_[2] ),
        .I1(\invdar_reg_175_reg_n_3_[0] ),
        .I2(\invdar_reg_175_reg_n_3_[1] ),
        .I3(\invdar_reg_175_reg_n_3_[3] ),
        .I4(\invdar_reg_175_reg_n_3_[4] ),
        .O(indvarinc_fu_219_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \indvarinc_reg_320[5]_i_1 
       (.I0(\invdar_reg_175_reg_n_3_[3] ),
        .I1(\invdar_reg_175_reg_n_3_[1] ),
        .I2(\invdar_reg_175_reg_n_3_[0] ),
        .I3(\invdar_reg_175_reg_n_3_[2] ),
        .I4(\invdar_reg_175_reg_n_3_[4] ),
        .I5(\invdar_reg_175_reg_n_3_[5] ),
        .O(indvarinc_fu_219_p2[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \indvarinc_reg_320[6]_i_1 
       (.I0(\indvarinc_reg_320[6]_i_2_n_3 ),
        .I1(\invdar_reg_175_reg_n_3_[5] ),
        .I2(\invdar_reg_175_reg_n_3_[6] ),
        .O(indvarinc_fu_219_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \indvarinc_reg_320[6]_i_2 
       (.I0(\invdar_reg_175_reg_n_3_[4] ),
        .I1(\invdar_reg_175_reg_n_3_[2] ),
        .I2(\invdar_reg_175_reg_n_3_[0] ),
        .I3(\invdar_reg_175_reg_n_3_[1] ),
        .I4(\invdar_reg_175_reg_n_3_[3] ),
        .O(\indvarinc_reg_320[6]_i_2_n_3 ));
  FDRE \indvarinc_reg_320_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[0]),
        .Q(indvarinc_reg_320[0]),
        .R(1'b0));
  FDRE \indvarinc_reg_320_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[1]),
        .Q(indvarinc_reg_320[1]),
        .R(1'b0));
  FDRE \indvarinc_reg_320_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[2]),
        .Q(indvarinc_reg_320[2]),
        .R(1'b0));
  FDRE \indvarinc_reg_320_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[3]),
        .Q(indvarinc_reg_320[3]),
        .R(1'b0));
  FDRE \indvarinc_reg_320_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[4]),
        .Q(indvarinc_reg_320[4]),
        .R(1'b0));
  FDRE \indvarinc_reg_320_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[5]),
        .Q(indvarinc_reg_320[5]),
        .R(1'b0));
  FDRE \indvarinc_reg_320_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(indvarinc_fu_219_p2[6]),
        .Q(indvarinc_reg_320[6]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input input_U
       (.CO(tmp_8_fu_288_p2),
        .DIADI(d0),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (INPUT_STREAM_V_data_V_0_payload_A[31:5]),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (INPUT_STREAM_V_data_V_0_payload_B[31:5]),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg_n_3_[0] ),
        .Q({ap_CS_fsm_state6,ap_CS_fsm_state4,ap_CS_fsm_state3}),
        .\a_a_1_reg_197_reg[5] ({\a_a_1_reg_197_reg_n_3_[5] ,\a_a_1_reg_197_reg_n_3_[4] ,\a_a_1_reg_197_reg_n_3_[3] ,\a_a_1_reg_197_reg_n_3_[2] ,\a_a_1_reg_197_reg_n_3_[1] ,\a_a_1_reg_197_reg_n_3_[0] }),
        .ap_clk(ap_clk),
        .\i_reg_186_reg[5] ({tmp_2_fu_241_p3,\i_reg_186_reg_n_3_[4] ,\i_reg_186_reg_n_3_[3] ,\i_reg_186_reg_n_3_[2] ,\i_reg_186_reg_n_3_[1] ,\i_reg_186_reg_n_3_[0] }),
        .\searched_read_reg_314_reg[31] (searched_read_reg_314),
        .\tmp_reg_325_reg[6] (tmp_reg_325));
  LUT2 #(
    .INIT(4'h2)) 
    \invdar_reg_175[6]_i_2 
       (.I0(ap_CS_fsm_state3),
        .I1(\tmp_1_reg_335_reg_n_3_[0] ),
        .O(invdar_reg_1750));
  FDRE \invdar_reg_175_reg[0] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[0]),
        .Q(\invdar_reg_175_reg_n_3_[0] ),
        .R(invdar_reg_175));
  FDRE \invdar_reg_175_reg[1] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[1]),
        .Q(\invdar_reg_175_reg_n_3_[1] ),
        .R(invdar_reg_175));
  FDRE \invdar_reg_175_reg[2] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[2]),
        .Q(\invdar_reg_175_reg_n_3_[2] ),
        .R(invdar_reg_175));
  FDRE \invdar_reg_175_reg[3] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[3]),
        .Q(\invdar_reg_175_reg_n_3_[3] ),
        .R(invdar_reg_175));
  FDRE \invdar_reg_175_reg[4] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[4]),
        .Q(\invdar_reg_175_reg_n_3_[4] ),
        .R(invdar_reg_175));
  FDRE \invdar_reg_175_reg[5] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[5]),
        .Q(\invdar_reg_175_reg_n_3_[5] ),
        .R(invdar_reg_175));
  FDRE \invdar_reg_175_reg[6] 
       (.C(ap_clk),
        .CE(invdar_reg_1750),
        .D(indvarinc_reg_320[6]),
        .Q(\invdar_reg_175_reg_n_3_[6] ),
        .R(invdar_reg_175));
  LUT1 #(
    .INIT(2'h1)) 
    \last[0]_i_2 
       (.I0(last_reg[0]),
        .O(\last[0]_i_2_n_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[0]_i_1_n_10 ),
        .Q(last_reg[0]),
        .R(1'b0));
  CARRY4 \last_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\last_reg[0]_i_1_n_3 ,\last_reg[0]_i_1_n_4 ,\last_reg[0]_i_1_n_5 ,\last_reg[0]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\last_reg[0]_i_1_n_7 ,\last_reg[0]_i_1_n_8 ,\last_reg[0]_i_1_n_9 ,\last_reg[0]_i_1_n_10 }),
        .S({last_reg[3:1],\last[0]_i_2_n_3 }));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[8]_i_1_n_8 ),
        .Q(last_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[8]_i_1_n_7 ),
        .Q(last_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[12]_i_1_n_10 ),
        .Q(last_reg[12]),
        .R(1'b0));
  CARRY4 \last_reg[12]_i_1 
       (.CI(\last_reg[8]_i_1_n_3 ),
        .CO({\last_reg[12]_i_1_n_3 ,\last_reg[12]_i_1_n_4 ,\last_reg[12]_i_1_n_5 ,\last_reg[12]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[12]_i_1_n_7 ,\last_reg[12]_i_1_n_8 ,\last_reg[12]_i_1_n_9 ,\last_reg[12]_i_1_n_10 }),
        .S(last_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[12]_i_1_n_9 ),
        .Q(last_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[12]_i_1_n_8 ),
        .Q(last_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[12]_i_1_n_7 ),
        .Q(last_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[16]_i_1_n_10 ),
        .Q(last_reg[16]),
        .R(1'b0));
  CARRY4 \last_reg[16]_i_1 
       (.CI(\last_reg[12]_i_1_n_3 ),
        .CO({\last_reg[16]_i_1_n_3 ,\last_reg[16]_i_1_n_4 ,\last_reg[16]_i_1_n_5 ,\last_reg[16]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[16]_i_1_n_7 ,\last_reg[16]_i_1_n_8 ,\last_reg[16]_i_1_n_9 ,\last_reg[16]_i_1_n_10 }),
        .S(last_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[16]_i_1_n_9 ),
        .Q(last_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[16]_i_1_n_8 ),
        .Q(last_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[16]_i_1_n_7 ),
        .Q(last_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[0]_i_1_n_9 ),
        .Q(last_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[20]_i_1_n_10 ),
        .Q(last_reg[20]),
        .R(1'b0));
  CARRY4 \last_reg[20]_i_1 
       (.CI(\last_reg[16]_i_1_n_3 ),
        .CO({\last_reg[20]_i_1_n_3 ,\last_reg[20]_i_1_n_4 ,\last_reg[20]_i_1_n_5 ,\last_reg[20]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[20]_i_1_n_7 ,\last_reg[20]_i_1_n_8 ,\last_reg[20]_i_1_n_9 ,\last_reg[20]_i_1_n_10 }),
        .S(last_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[20]_i_1_n_9 ),
        .Q(last_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[20]_i_1_n_8 ),
        .Q(last_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[20]_i_1_n_7 ),
        .Q(last_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[24]_i_1_n_10 ),
        .Q(last_reg[24]),
        .R(1'b0));
  CARRY4 \last_reg[24]_i_1 
       (.CI(\last_reg[20]_i_1_n_3 ),
        .CO({\last_reg[24]_i_1_n_3 ,\last_reg[24]_i_1_n_4 ,\last_reg[24]_i_1_n_5 ,\last_reg[24]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[24]_i_1_n_7 ,\last_reg[24]_i_1_n_8 ,\last_reg[24]_i_1_n_9 ,\last_reg[24]_i_1_n_10 }),
        .S(last_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[24]_i_1_n_9 ),
        .Q(last_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[24]_i_1_n_8 ),
        .Q(last_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[24]_i_1_n_7 ),
        .Q(last_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[28]_i_1_n_10 ),
        .Q(last_reg[28]),
        .R(1'b0));
  CARRY4 \last_reg[28]_i_1 
       (.CI(\last_reg[24]_i_1_n_3 ),
        .CO({\NLW_last_reg[28]_i_1_CO_UNCONNECTED [3],\last_reg[28]_i_1_n_4 ,\last_reg[28]_i_1_n_5 ,\last_reg[28]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[28]_i_1_n_7 ,\last_reg[28]_i_1_n_8 ,\last_reg[28]_i_1_n_9 ,\last_reg[28]_i_1_n_10 }),
        .S(last_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[28]_i_1_n_9 ),
        .Q(last_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[0]_i_1_n_8 ),
        .Q(last_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[30] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[28]_i_1_n_8 ),
        .Q(last_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[31] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[28]_i_1_n_7 ),
        .Q(last_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[0]_i_1_n_7 ),
        .Q(last_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[4]_i_1_n_10 ),
        .Q(last_reg[4]),
        .R(1'b0));
  CARRY4 \last_reg[4]_i_1 
       (.CI(\last_reg[0]_i_1_n_3 ),
        .CO({\last_reg[4]_i_1_n_3 ,\last_reg[4]_i_1_n_4 ,\last_reg[4]_i_1_n_5 ,\last_reg[4]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[4]_i_1_n_7 ,\last_reg[4]_i_1_n_8 ,\last_reg[4]_i_1_n_9 ,\last_reg[4]_i_1_n_10 }),
        .S(last_reg[7:4]));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[4]_i_1_n_9 ),
        .Q(last_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[4]_i_1_n_8 ),
        .Q(last_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \last_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[4]_i_1_n_7 ),
        .Q(last_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[8]_i_1_n_10 ),
        .Q(last_reg[8]),
        .R(1'b0));
  CARRY4 \last_reg[8]_i_1 
       (.CI(\last_reg[4]_i_1_n_3 ),
        .CO({\last_reg[8]_i_1_n_3 ,\last_reg[8]_i_1_n_4 ,\last_reg[8]_i_1_n_5 ,\last_reg[8]_i_1_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\last_reg[8]_i_1_n_7 ,\last_reg[8]_i_1_n_8 ,\last_reg[8]_i_1_n_9 ,\last_reg[8]_i_1_n_10 }),
        .S(last_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \last_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state9),
        .D(\last_reg[8]_i_1_n_9 ),
        .Q(last_reg[9]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[0]),
        .Q(searched_read_reg_314[0]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[10] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[10]),
        .Q(searched_read_reg_314[10]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[11] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[11]),
        .Q(searched_read_reg_314[11]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[12] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[12]),
        .Q(searched_read_reg_314[12]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[13] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[13]),
        .Q(searched_read_reg_314[13]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[14] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[14]),
        .Q(searched_read_reg_314[14]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[15] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[15]),
        .Q(searched_read_reg_314[15]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[16] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[16]),
        .Q(searched_read_reg_314[16]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[17] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[17]),
        .Q(searched_read_reg_314[17]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[18] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[18]),
        .Q(searched_read_reg_314[18]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[19] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[19]),
        .Q(searched_read_reg_314[19]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[1]),
        .Q(searched_read_reg_314[1]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[20] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[20]),
        .Q(searched_read_reg_314[20]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[21] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[21]),
        .Q(searched_read_reg_314[21]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[22] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[22]),
        .Q(searched_read_reg_314[22]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[23] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[23]),
        .Q(searched_read_reg_314[23]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[24] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[24]),
        .Q(searched_read_reg_314[24]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[25] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[25]),
        .Q(searched_read_reg_314[25]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[26] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[26]),
        .Q(searched_read_reg_314[26]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[27] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[27]),
        .Q(searched_read_reg_314[27]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[28] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[28]),
        .Q(searched_read_reg_314[28]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[29] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[29]),
        .Q(searched_read_reg_314[29]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[2]),
        .Q(searched_read_reg_314[2]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[30] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[30]),
        .Q(searched_read_reg_314[30]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[31] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[31]),
        .Q(searched_read_reg_314[31]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[3]),
        .Q(searched_read_reg_314[3]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[4]),
        .Q(searched_read_reg_314[4]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[5] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[5]),
        .Q(searched_read_reg_314[5]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[6] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[6]),
        .Q(searched_read_reg_314[6]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[7] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[7]),
        .Q(searched_read_reg_314[7]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[8] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[8]),
        .Q(searched_read_reg_314[8]),
        .R(1'b0));
  FDRE \searched_read_reg_314_reg[9] 
       (.C(ap_clk),
        .CE(ap_NS_fsm18_out),
        .D(searched[9]),
        .Q(searched_read_reg_314[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h08FB)) 
    \storemerge_reg_209[0]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[0] ),
        .I1(agg_result_a_ap_vld),
        .I2(tmp_6_reg_350),
        .I3(last_reg[0]),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[10]_i_1 
       (.I0(a_a_fu_302_p2[10]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[11]_i_1 
       (.I0(a_a_fu_302_p2[11]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[12]_i_1 
       (.I0(a_a_fu_302_p2[12]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[13]_i_1 
       (.I0(a_a_fu_302_p2[13]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[14]_i_1 
       (.I0(a_a_fu_302_p2[14]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[15]_i_1 
       (.I0(a_a_fu_302_p2[15]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[16]_i_1 
       (.I0(a_a_fu_302_p2[16]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[17]_i_1 
       (.I0(a_a_fu_302_p2[17]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[18]_i_1 
       (.I0(a_a_fu_302_p2[18]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[19]_i_1 
       (.I0(a_a_fu_302_p2[19]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[19]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \storemerge_reg_209[1]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[1] ),
        .I1(agg_result_a_ap_vld),
        .I2(tmp_6_reg_350),
        .I3(a_a_fu_302_p2[1]),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[20]_i_1 
       (.I0(a_a_fu_302_p2[20]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[21]_i_1 
       (.I0(a_a_fu_302_p2[21]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[22]_i_1 
       (.I0(a_a_fu_302_p2[22]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[23]_i_1 
       (.I0(a_a_fu_302_p2[23]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[24]_i_1 
       (.I0(a_a_fu_302_p2[24]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[25]_i_1 
       (.I0(a_a_fu_302_p2[25]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[26]_i_1 
       (.I0(a_a_fu_302_p2[26]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[27]_i_1 
       (.I0(a_a_fu_302_p2[27]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[28]_i_1 
       (.I0(a_a_fu_302_p2[28]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[29]_i_1 
       (.I0(a_a_fu_302_p2[29]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[29]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \storemerge_reg_209[2]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[2] ),
        .I1(agg_result_a_ap_vld),
        .I2(tmp_6_reg_350),
        .I3(a_a_fu_302_p2[2]),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[30]_i_1 
       (.I0(a_a_fu_302_p2[30]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[30]));
  LUT2 #(
    .INIT(4'h2)) 
    \storemerge_reg_209[31]_i_1 
       (.I0(agg_result_a_ap_vld),
        .I1(tmp_6_reg_350),
        .O(ap_phi_mux_storemerge_phi_fu_212_p41));
  LUT3 #(
    .INIT(8'hBA)) 
    \storemerge_reg_209[31]_i_2 
       (.I0(ap_CS_fsm_state9),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(\storemerge_reg_209[31]_i_2_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[31]_i_3 
       (.I0(a_a_fu_302_p2[31]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[31]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \storemerge_reg_209[3]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[3] ),
        .I1(agg_result_a_ap_vld),
        .I2(tmp_6_reg_350),
        .I3(a_a_fu_302_p2[3]),
        .O(p_2_in[3]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \storemerge_reg_209[4]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[4] ),
        .I1(agg_result_a_ap_vld),
        .I2(tmp_6_reg_350),
        .I3(a_a_fu_302_p2[4]),
        .O(p_2_in[4]));
  LUT4 #(
    .INIT(16'hFB08)) 
    \storemerge_reg_209[5]_i_1 
       (.I0(\a_a_1_reg_197_reg_n_3_[5] ),
        .I1(agg_result_a_ap_vld),
        .I2(tmp_6_reg_350),
        .I3(a_a_fu_302_p2[5]),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[6]_i_1 
       (.I0(a_a_fu_302_p2[6]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[7]_i_1 
       (.I0(a_a_fu_302_p2[7]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[8]_i_1 
       (.I0(a_a_fu_302_p2[8]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    \storemerge_reg_209[9]_i_1 
       (.I0(a_a_fu_302_p2[9]),
        .I1(tmp_6_reg_350),
        .I2(agg_result_a_ap_vld),
        .O(p_2_in[9]));
  FDRE \storemerge_reg_209_reg[0] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[0]),
        .Q(storemerge_reg_209[0]),
        .R(1'b0));
  FDRE \storemerge_reg_209_reg[10] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[10]),
        .Q(storemerge_reg_209[10]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[11] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[11]),
        .Q(storemerge_reg_209[11]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[12] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[12]),
        .Q(storemerge_reg_209[12]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[12]_i_2 
       (.CI(\storemerge_reg_209_reg[8]_i_2_n_3 ),
        .CO({\storemerge_reg_209_reg[12]_i_2_n_3 ,\storemerge_reg_209_reg[12]_i_2_n_4 ,\storemerge_reg_209_reg[12]_i_2_n_5 ,\storemerge_reg_209_reg[12]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[12:9]),
        .S(last_reg[12:9]));
  FDRE \storemerge_reg_209_reg[13] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[13]),
        .Q(storemerge_reg_209[13]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[14] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[14]),
        .Q(storemerge_reg_209[14]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[15] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[15]),
        .Q(storemerge_reg_209[15]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[16] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[16]),
        .Q(storemerge_reg_209[16]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[16]_i_2 
       (.CI(\storemerge_reg_209_reg[12]_i_2_n_3 ),
        .CO({\storemerge_reg_209_reg[16]_i_2_n_3 ,\storemerge_reg_209_reg[16]_i_2_n_4 ,\storemerge_reg_209_reg[16]_i_2_n_5 ,\storemerge_reg_209_reg[16]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[16:13]),
        .S(last_reg[16:13]));
  FDRE \storemerge_reg_209_reg[17] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[17]),
        .Q(storemerge_reg_209[17]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[18] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[18]),
        .Q(storemerge_reg_209[18]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[19] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[19]),
        .Q(storemerge_reg_209[19]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[1] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[1]),
        .Q(storemerge_reg_209[1]),
        .R(1'b0));
  FDRE \storemerge_reg_209_reg[20] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[20]),
        .Q(storemerge_reg_209[20]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[20]_i_2 
       (.CI(\storemerge_reg_209_reg[16]_i_2_n_3 ),
        .CO({\storemerge_reg_209_reg[20]_i_2_n_3 ,\storemerge_reg_209_reg[20]_i_2_n_4 ,\storemerge_reg_209_reg[20]_i_2_n_5 ,\storemerge_reg_209_reg[20]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[20:17]),
        .S(last_reg[20:17]));
  FDRE \storemerge_reg_209_reg[21] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[21]),
        .Q(storemerge_reg_209[21]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[22] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[22]),
        .Q(storemerge_reg_209[22]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[23] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[23]),
        .Q(storemerge_reg_209[23]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[24] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[24]),
        .Q(storemerge_reg_209[24]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[24]_i_2 
       (.CI(\storemerge_reg_209_reg[20]_i_2_n_3 ),
        .CO({\storemerge_reg_209_reg[24]_i_2_n_3 ,\storemerge_reg_209_reg[24]_i_2_n_4 ,\storemerge_reg_209_reg[24]_i_2_n_5 ,\storemerge_reg_209_reg[24]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[24:21]),
        .S(last_reg[24:21]));
  FDRE \storemerge_reg_209_reg[25] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[25]),
        .Q(storemerge_reg_209[25]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[26] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[26]),
        .Q(storemerge_reg_209[26]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[27] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[27]),
        .Q(storemerge_reg_209[27]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[28] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[28]),
        .Q(storemerge_reg_209[28]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[28]_i_2 
       (.CI(\storemerge_reg_209_reg[24]_i_2_n_3 ),
        .CO({\storemerge_reg_209_reg[28]_i_2_n_3 ,\storemerge_reg_209_reg[28]_i_2_n_4 ,\storemerge_reg_209_reg[28]_i_2_n_5 ,\storemerge_reg_209_reg[28]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[28:25]),
        .S(last_reg[28:25]));
  FDRE \storemerge_reg_209_reg[29] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[29]),
        .Q(storemerge_reg_209[29]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[2] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[2]),
        .Q(storemerge_reg_209[2]),
        .R(1'b0));
  FDRE \storemerge_reg_209_reg[30] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[30]),
        .Q(storemerge_reg_209[30]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[31] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[31]),
        .Q(storemerge_reg_209[31]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[31]_i_4 
       (.CI(\storemerge_reg_209_reg[28]_i_2_n_3 ),
        .CO({\NLW_storemerge_reg_209_reg[31]_i_4_CO_UNCONNECTED [3:2],\storemerge_reg_209_reg[31]_i_4_n_5 ,\storemerge_reg_209_reg[31]_i_4_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_storemerge_reg_209_reg[31]_i_4_O_UNCONNECTED [3],a_a_fu_302_p2[31:29]}),
        .S({1'b0,last_reg[31:29]}));
  FDRE \storemerge_reg_209_reg[3] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[3]),
        .Q(storemerge_reg_209[3]),
        .R(1'b0));
  FDRE \storemerge_reg_209_reg[4] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[4]),
        .Q(storemerge_reg_209[4]),
        .R(1'b0));
  CARRY4 \storemerge_reg_209_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\storemerge_reg_209_reg[4]_i_2_n_3 ,\storemerge_reg_209_reg[4]_i_2_n_4 ,\storemerge_reg_209_reg[4]_i_2_n_5 ,\storemerge_reg_209_reg[4]_i_2_n_6 }),
        .CYINIT(last_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[4:1]),
        .S(last_reg[4:1]));
  FDRE \storemerge_reg_209_reg[5] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[5]),
        .Q(storemerge_reg_209[5]),
        .R(1'b0));
  FDRE \storemerge_reg_209_reg[6] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[6]),
        .Q(storemerge_reg_209[6]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[7] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[7]),
        .Q(storemerge_reg_209[7]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  FDRE \storemerge_reg_209_reg[8] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[8]),
        .Q(storemerge_reg_209[8]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  CARRY4 \storemerge_reg_209_reg[8]_i_2 
       (.CI(\storemerge_reg_209_reg[4]_i_2_n_3 ),
        .CO({\storemerge_reg_209_reg[8]_i_2_n_3 ,\storemerge_reg_209_reg[8]_i_2_n_4 ,\storemerge_reg_209_reg[8]_i_2_n_5 ,\storemerge_reg_209_reg[8]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(a_a_fu_302_p2[8:5]),
        .S(last_reg[8:5]));
  FDRE \storemerge_reg_209_reg[9] 
       (.C(ap_clk),
        .CE(\storemerge_reg_209[31]_i_2_n_3 ),
        .D(p_2_in[9]),
        .Q(storemerge_reg_209[9]),
        .R(ap_phi_mux_storemerge_phi_fu_212_p41));
  LUT6 #(
    .INIT(64'h00000300AAAAAAAA)) 
    \tmp_1_reg_335[0]_i_1 
       (.I0(\tmp_1_reg_335_reg_n_3_[0] ),
        .I1(\invdar_reg_175_reg_n_3_[5] ),
        .I2(\invdar_reg_175_reg_n_3_[1] ),
        .I3(\invdar_reg_175_reg_n_3_[0] ),
        .I4(\tmp_1_reg_335[0]_i_2_n_3 ),
        .I5(ce0),
        .O(\tmp_1_reg_335[0]_i_1_n_3 ));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \tmp_1_reg_335[0]_i_2 
       (.I0(\invdar_reg_175_reg_n_3_[3] ),
        .I1(\invdar_reg_175_reg_n_3_[4] ),
        .I2(\invdar_reg_175_reg_n_3_[2] ),
        .I3(\invdar_reg_175_reg_n_3_[6] ),
        .O(\tmp_1_reg_335[0]_i_2_n_3 ));
  FDRE \tmp_1_reg_335_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_1_reg_335[0]_i_1_n_3 ),
        .Q(\tmp_1_reg_335_reg_n_3_[0] ),
        .R(1'b0));
  FDRE \tmp_6_reg_350_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\a_a_1_reg_197_reg_n_3_[5] ),
        .Q(tmp_6_reg_350),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[0] ),
        .Q(tmp_reg_325[0]),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[1] ),
        .Q(tmp_reg_325[1]),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[2] ),
        .Q(tmp_reg_325[2]),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[3] ),
        .Q(tmp_reg_325[3]),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[4] ),
        .Q(tmp_reg_325[4]),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[5] ),
        .Q(tmp_reg_325[5]),
        .R(1'b0));
  FDRE \tmp_reg_325_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\invdar_reg_175_reg_n_3_[6] ),
        .Q(tmp_reg_325[6]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb
   (DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4] ,
    INPUT_STREAM_V_data_V_0_sel,
    \ap_CS_fsm_reg[3] ,
    \invdar_reg_175_reg[6] ,
    ap_clk);
  output [4:0]DIADI;
  input [4:0]Q;
  input [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [1:0]\ap_CS_fsm_reg[3] ;
  input [6:0]\invdar_reg_175_reg[6] ;
  input ap_clk;

  wire [4:0]DIADI;
  wire [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire [4:0]Q;
  wire [1:0]\ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire [6:0]\invdar_reg_175_reg[6] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom Adder2_Adder2_strbkb_rom_U
       (.DIADI(DIADI),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[4] (\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .Q(Q),
        .\ap_CS_fsm_reg[3] (\ap_CS_fsm_reg[3] ),
        .ap_clk(ap_clk),
        .\invdar_reg_175_reg[6] (\invdar_reg_175_reg[6] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_Adder2_strbkb_rom
   (DIADI,
    Q,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[4] ,
    INPUT_STREAM_V_data_V_0_sel,
    \ap_CS_fsm_reg[3] ,
    \invdar_reg_175_reg[6] ,
    ap_clk);
  output [4:0]DIADI;
  input [4:0]Q;
  input [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [1:0]\ap_CS_fsm_reg[3] ;
  input [6:0]\invdar_reg_175_reg[6] ;
  input ap_clk;

  wire [4:0]DIADI;
  wire [4:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[4] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire [4:0]Q;
  wire [1:0]\ap_CS_fsm_reg[3] ;
  wire ap_clk;
  wire g0_b0_n_3;
  wire g0_b1_n_3;
  wire g0_b2_n_3;
  wire g0_b3_n_3;
  wire g0_b4_n_3;
  wire g1_b1_n_3;
  wire g1_b2_n_3;
  wire g1_b4_n_3;
  wire [6:0]\invdar_reg_175_reg[6] ;
  wire [4:0]q0;
  wire \q0[0]_i_1_n_3 ;
  wire \q0[1]_i_1_n_3 ;
  wire \q0[3]_i_1_n_3 ;
  wire \q0_reg[2]_i_1_n_3 ;
  wire \q0_reg[4]_i_1_n_3 ;

  LUT6 #(
    .INIT(64'h552AA554AA9552AA)) 
    g0_b0
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g0_b0_n_3));
  LUT6 #(
    .INIT(64'h99B33666CCD99B33)) 
    g0_b1
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g0_b1_n_3));
  LUT6 #(
    .INIT(64'h1E43C8790F21E43C)) 
    g0_b2
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g0_b2_n_3));
  LUT6 #(
    .INIT(64'h1F83F07E0FC1F83F)) 
    g0_b3
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g0_b3_n_3));
  LUT6 #(
    .INIT(64'hE07C0F81F03E07C0)) 
    g0_b4
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g0_b4_n_3));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    g1_b1
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g1_b1_n_3));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    g1_b2
       (.I0(\invdar_reg_175_reg[6] [0]),
        .I1(\invdar_reg_175_reg[6] [1]),
        .I2(\invdar_reg_175_reg[6] [2]),
        .I3(\invdar_reg_175_reg[6] [3]),
        .I4(\invdar_reg_175_reg[6] [4]),
        .I5(\invdar_reg_175_reg[6] [5]),
        .O(g1_b2_n_3));
  LUT5 #(
    .INIT(32'h00000001)) 
    g1_b4
       (.I0(\invdar_reg_175_reg[6] [1]),
        .I1(\invdar_reg_175_reg[6] [2]),
        .I2(\invdar_reg_175_reg[6] [3]),
        .I3(\invdar_reg_175_reg[6] [4]),
        .I4(\invdar_reg_175_reg[6] [5]),
        .O(g1_b4_n_3));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[0]_i_1 
       (.I0(g1_b1_n_3),
        .I1(\invdar_reg_175_reg[6] [6]),
        .I2(g0_b0_n_3),
        .O(\q0[0]_i_1_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \q0[1]_i_1 
       (.I0(g1_b1_n_3),
        .I1(\invdar_reg_175_reg[6] [6]),
        .I2(g0_b1_n_3),
        .O(\q0[1]_i_1_n_3 ));
  LUT2 #(
    .INIT(4'h2)) 
    \q0[3]_i_1 
       (.I0(g0_b3_n_3),
        .I1(\invdar_reg_175_reg[6] [6]),
        .O(\q0[3]_i_1_n_3 ));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[0]_i_1_n_3 ),
        .Q(q0[0]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[1]_i_1_n_3 ),
        .Q(q0[1]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0_reg[2]_i_1_n_3 ),
        .Q(q0[2]),
        .R(1'b0));
  MUXF7 \q0_reg[2]_i_1 
       (.I0(g0_b2_n_3),
        .I1(g1_b2_n_3),
        .O(\q0_reg[2]_i_1_n_3 ),
        .S(\invdar_reg_175_reg[6] [6]));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0[3]_i_1_n_3 ),
        .Q(q0[3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(\ap_CS_fsm_reg[3] [0]),
        .D(\q0_reg[4]_i_1_n_3 ),
        .Q(q0[4]),
        .R(1'b0));
  MUXF7 \q0_reg[4]_i_1 
       (.I0(g0_b4_n_3),
        .I1(g1_b4_n_3),
        .O(\q0_reg[4]_i_1_n_3 ),
        .S(\invdar_reg_175_reg[6] [6]));
  LUT5 #(
    .INIT(32'hCAFFCA00)) 
    ram_reg_i_20
       (.I0(Q[4]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [4]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\ap_CS_fsm_reg[3] [1]),
        .I4(q0[4]),
        .O(DIADI[4]));
  LUT5 #(
    .INIT(32'hCAFFCA00)) 
    ram_reg_i_21
       (.I0(Q[3]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [3]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\ap_CS_fsm_reg[3] [1]),
        .I4(q0[3]),
        .O(DIADI[3]));
  LUT5 #(
    .INIT(32'hAFCFA0C0)) 
    ram_reg_i_22
       (.I0(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [2]),
        .I1(Q[2]),
        .I2(\ap_CS_fsm_reg[3] [1]),
        .I3(INPUT_STREAM_V_data_V_0_sel),
        .I4(q0[2]),
        .O(DIADI[2]));
  LUT5 #(
    .INIT(32'hFC0CACAC)) 
    ram_reg_i_23
       (.I0(Q[1]),
        .I1(q0[1]),
        .I2(\ap_CS_fsm_reg[3] [1]),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [1]),
        .I4(INPUT_STREAM_V_data_V_0_sel),
        .O(DIADI[1]));
  LUT5 #(
    .INIT(32'hCAFFCA00)) 
    ram_reg_i_24
       (.I0(Q[0]),
        .I1(\INPUT_STREAM_V_data_V_0_payload_B_reg[4] [0]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\ap_CS_fsm_reg[3] [1]),
        .I4(q0[0]),
        .O(DIADI[0]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_CONTROL_BUS_s_axi
   (reset,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_ARREADY,
    out,
    \searched_read_reg_314_reg[31] ,
    SR,
    E,
    interrupt,
    D,
    s_axi_CONTROL_BUS_RDATA,
    ap_clk,
    Q,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_RREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_ARADDR,
    ap_rst_n,
    \tmp_1_reg_335_reg[0] ,
    \storemerge_reg_209_reg[31] ,
    tmp_6_reg_350,
    \a_a_1_reg_197_reg[5] ,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWADDR,
    \searched_read_reg_314_reg[31]_0 );
  output reset;
  output s_axi_CONTROL_BUS_RVALID;
  output s_axi_CONTROL_BUS_ARREADY;
  output [2:0]out;
  output [31:0]\searched_read_reg_314_reg[31] ;
  output [0:0]SR;
  output [0:0]E;
  output interrupt;
  output [1:0]D;
  output [31:0]s_axi_CONTROL_BUS_RDATA;
  input ap_clk;
  input [2:0]Q;
  input s_axi_CONTROL_BUS_ARVALID;
  input s_axi_CONTROL_BUS_RREADY;
  input [31:0]s_axi_CONTROL_BUS_WDATA;
  input [3:0]s_axi_CONTROL_BUS_WSTRB;
  input s_axi_CONTROL_BUS_WVALID;
  input [5:0]s_axi_CONTROL_BUS_ARADDR;
  input ap_rst_n;
  input \tmp_1_reg_335_reg[0] ;
  input [31:0]\storemerge_reg_209_reg[31] ;
  input tmp_6_reg_350;
  input [5:0]\a_a_1_reg_197_reg[5] ;
  input s_axi_CONTROL_BUS_BREADY;
  input s_axi_CONTROL_BUS_AWVALID;
  input [5:0]s_axi_CONTROL_BUS_AWADDR;
  input [31:0]\searched_read_reg_314_reg[31]_0 ;

  wire \/FSM_onehot_wstate[1]_i_1_n_3 ;
  wire \/FSM_onehot_wstate[2]_i_1_n_3 ;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_wstate[3]_i_1_n_3 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_wstate_reg_n_3_[0] ;
  wire [2:0]Q;
  wire [0:0]SR;
  wire [5:0]\a_a_1_reg_197_reg[5] ;
  wire [31:0]agg_result_a;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst_n;
  wire ap_start;
  wire ar_hs;
  wire [31:0]int_agg_result_a;
  wire int_agg_result_a_ap_vld;
  wire int_agg_result_a_ap_vld_i_1_n_3;
  wire int_agg_result_a_ap_vld_i_2_n_3;
  wire int_agg_result_a_ap_vld_i_3_n_3;
  wire [31:0]int_agg_result_b;
  wire int_agg_result_b_ap_vld;
  wire int_agg_result_b_ap_vld_i_1_n_3;
  wire int_ap_done;
  wire int_ap_done_i_1_n_3;
  wire int_ap_done_i_2_n_3;
  wire int_ap_idle;
  wire int_ap_ready;
  wire int_ap_start3_out;
  wire int_ap_start_i_1_n_3;
  wire int_auto_restart;
  wire int_auto_restart_i_1_n_3;
  wire int_gie_i_1_n_3;
  wire int_gie_reg_n_3;
  wire \int_ier[0]_i_1_n_3 ;
  wire \int_ier[1]_i_1_n_3 ;
  wire \int_ier[1]_i_2_n_3 ;
  wire \int_ier_reg_n_3_[0] ;
  wire int_isr6_out;
  wire \int_isr[0]_i_1_n_3 ;
  wire \int_isr[1]_i_1_n_3 ;
  wire \int_isr_reg_n_3_[0] ;
  wire \int_searched[31]_i_3_n_3 ;
  wire interrupt;
  wire [31:0]\or ;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire p_0_in;
  wire p_0_in13_out;
  wire p_1_in;
  wire [7:0]rdata_data;
  wire \rdata_data[0]_i_2_n_3 ;
  wire \rdata_data[0]_i_3_n_3 ;
  wire \rdata_data[0]_i_4_n_3 ;
  wire \rdata_data[0]_i_5_n_3 ;
  wire \rdata_data[10]_i_1_n_3 ;
  wire \rdata_data[11]_i_1_n_3 ;
  wire \rdata_data[12]_i_1_n_3 ;
  wire \rdata_data[13]_i_1_n_3 ;
  wire \rdata_data[14]_i_1_n_3 ;
  wire \rdata_data[15]_i_1_n_3 ;
  wire \rdata_data[16]_i_1_n_3 ;
  wire \rdata_data[17]_i_1_n_3 ;
  wire \rdata_data[18]_i_1_n_3 ;
  wire \rdata_data[19]_i_1_n_3 ;
  wire \rdata_data[1]_i_2_n_3 ;
  wire \rdata_data[1]_i_3_n_3 ;
  wire \rdata_data[1]_i_4_n_3 ;
  wire \rdata_data[1]_i_5_n_3 ;
  wire \rdata_data[20]_i_1_n_3 ;
  wire \rdata_data[21]_i_1_n_3 ;
  wire \rdata_data[22]_i_1_n_3 ;
  wire \rdata_data[23]_i_1_n_3 ;
  wire \rdata_data[24]_i_1_n_3 ;
  wire \rdata_data[25]_i_1_n_3 ;
  wire \rdata_data[26]_i_1_n_3 ;
  wire \rdata_data[27]_i_1_n_3 ;
  wire \rdata_data[28]_i_1_n_3 ;
  wire \rdata_data[29]_i_1_n_3 ;
  wire \rdata_data[2]_i_2_n_3 ;
  wire \rdata_data[30]_i_1_n_3 ;
  wire \rdata_data[31]_i_1_n_3 ;
  wire \rdata_data[31]_i_3_n_3 ;
  wire \rdata_data[3]_i_2_n_3 ;
  wire \rdata_data[4]_i_1_n_3 ;
  wire \rdata_data[5]_i_1_n_3 ;
  wire \rdata_data[6]_i_1_n_3 ;
  wire \rdata_data[7]_i_2_n_3 ;
  wire \rdata_data[7]_i_3_n_3 ;
  wire \rdata_data[8]_i_1_n_3 ;
  wire \rdata_data[9]_i_1_n_3 ;
  wire reset;
  wire [1:0]rstate;
  wire \rstate[0]_i_1_n_3 ;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;
  wire [31:0]\searched_read_reg_314_reg[31] ;
  wire [31:0]\searched_read_reg_314_reg[31]_0 ;
  wire [31:0]\storemerge_reg_209_reg[31] ;
  wire \tmp_1_reg_335_reg[0] ;
  wire tmp_6_reg_350;
  wire waddr;
  wire \waddr_reg_n_3_[0] ;
  wire \waddr_reg_n_3_[1] ;
  wire \waddr_reg_n_3_[2] ;
  wire \waddr_reg_n_3_[3] ;
  wire \waddr_reg_n_3_[4] ;
  wire \waddr_reg_n_3_[5] ;

  LUT5 #(
    .INIT(32'h000BFF0B)) 
    \/FSM_onehot_wstate[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(s_axi_CONTROL_BUS_AWVALID),
        .O(\/FSM_onehot_wstate[1]_i_1_n_3 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \/FSM_onehot_wstate[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_AWVALID),
        .I1(out[0]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .O(\/FSM_onehot_wstate[2]_i_1_n_3 ));
  LUT5 #(
    .INIT(32'h0000F404)) 
    \FSM_onehot_wstate[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_BREADY),
        .I1(out[2]),
        .I2(out[1]),
        .I3(s_axi_CONTROL_BUS_WVALID),
        .I4(out[0]),
        .O(\FSM_onehot_wstate[3]_i_1_n_3 ));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_wstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(\FSM_onehot_wstate_reg_n_3_[0] ),
        .S(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[1]_i_1_n_3 ),
        .Q(out[0]),
        .R(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\/FSM_onehot_wstate[2]_i_1_n_3 ),
        .Q(out[1]),
        .R(reset));
  (* FSM_ENCODED_STATES = "wridle:0010,wrdata:0100,wrresp:1000,iSTATE:0001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_wstate_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\FSM_onehot_wstate[3]_i_1_n_3 ),
        .Q(out[2]),
        .R(reset));
  LUT1 #(
    .INIT(2'h1)) 
    \INPUT_STREAM_V_dest_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(reset));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(Q[2]),
        .I1(ap_start),
        .I2(Q[0]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\tmp_1_reg_335_reg[0] ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \int_agg_result_a[0]_i_1 
       (.I0(\a_a_1_reg_197_reg[5] [0]),
        .I1(tmp_6_reg_350),
        .I2(\storemerge_reg_209_reg[31] [0]),
        .O(agg_result_a[0]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[10]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [10]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[10]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[11]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [11]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[11]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[12]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [12]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[12]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[13]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [13]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[13]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[14]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [14]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[14]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[15]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [15]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[15]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[16]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [16]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[16]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[17]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [17]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[17]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[18]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [18]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[18]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[19]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [19]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[19]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \int_agg_result_a[1]_i_1 
       (.I0(\a_a_1_reg_197_reg[5] [1]),
        .I1(tmp_6_reg_350),
        .I2(\storemerge_reg_209_reg[31] [1]),
        .O(agg_result_a[1]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[20]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [20]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[20]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[21]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [21]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[21]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[22]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [22]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[22]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[23]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [23]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[23]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[24]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [24]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[24]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[25]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [25]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[25]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[26]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [26]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[26]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[27]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [27]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[27]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[28]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [28]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[28]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[29]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [29]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[29]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \int_agg_result_a[2]_i_1 
       (.I0(\a_a_1_reg_197_reg[5] [2]),
        .I1(tmp_6_reg_350),
        .I2(\storemerge_reg_209_reg[31] [2]),
        .O(agg_result_a[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[30]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [30]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[30]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[31]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [31]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[31]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \int_agg_result_a[3]_i_1 
       (.I0(\a_a_1_reg_197_reg[5] [3]),
        .I1(tmp_6_reg_350),
        .I2(\storemerge_reg_209_reg[31] [3]),
        .O(agg_result_a[3]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \int_agg_result_a[4]_i_1 
       (.I0(\a_a_1_reg_197_reg[5] [4]),
        .I1(tmp_6_reg_350),
        .I2(\storemerge_reg_209_reg[31] [4]),
        .O(agg_result_a[4]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    \int_agg_result_a[5]_i_1 
       (.I0(\a_a_1_reg_197_reg[5] [5]),
        .I1(tmp_6_reg_350),
        .I2(\storemerge_reg_209_reg[31] [5]),
        .O(agg_result_a[5]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[6]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [6]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[6]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[7]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [7]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[7]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[8]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [8]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[8]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \int_agg_result_a[9]_i_1 
       (.I0(\storemerge_reg_209_reg[31] [9]),
        .I1(tmp_6_reg_350),
        .O(agg_result_a[9]));
  LUT5 #(
    .INIT(32'hFFEFAAAA)) 
    int_agg_result_a_ap_vld_i_1
       (.I0(Q[2]),
        .I1(int_agg_result_a_ap_vld_i_2_n_3),
        .I2(ar_hs),
        .I3(int_agg_result_a_ap_vld_i_3_n_3),
        .I4(int_agg_result_a_ap_vld),
        .O(int_agg_result_a_ap_vld_i_1_n_3));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    int_agg_result_a_ap_vld_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(int_agg_result_a_ap_vld_i_2_n_3));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    int_agg_result_a_ap_vld_i_3
       (.I0(s_axi_CONTROL_BUS_ARADDR[1]),
        .I1(s_axi_CONTROL_BUS_ARADDR[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(int_agg_result_a_ap_vld_i_3_n_3));
  FDRE int_agg_result_a_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_a_ap_vld_i_1_n_3),
        .Q(int_agg_result_a_ap_vld),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[0]),
        .Q(int_agg_result_a[0]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[10] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[10]),
        .Q(int_agg_result_a[10]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[11]),
        .Q(int_agg_result_a[11]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[12] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[12]),
        .Q(int_agg_result_a[12]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[13] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[13]),
        .Q(int_agg_result_a[13]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[14] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[14]),
        .Q(int_agg_result_a[14]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[15] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[15]),
        .Q(int_agg_result_a[15]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[16] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[16]),
        .Q(int_agg_result_a[16]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[17] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[17]),
        .Q(int_agg_result_a[17]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[18] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[18]),
        .Q(int_agg_result_a[18]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[19] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[19]),
        .Q(int_agg_result_a[19]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[1]),
        .Q(int_agg_result_a[1]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[20] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[20]),
        .Q(int_agg_result_a[20]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[21] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[21]),
        .Q(int_agg_result_a[21]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[22] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[22]),
        .Q(int_agg_result_a[22]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[23] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[23]),
        .Q(int_agg_result_a[23]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[24] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[24]),
        .Q(int_agg_result_a[24]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[25] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[25]),
        .Q(int_agg_result_a[25]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[26] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[26]),
        .Q(int_agg_result_a[26]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[27] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[27]),
        .Q(int_agg_result_a[27]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[28] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[28]),
        .Q(int_agg_result_a[28]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[29] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[29]),
        .Q(int_agg_result_a[29]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[2]),
        .Q(int_agg_result_a[2]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[30] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[30]),
        .Q(int_agg_result_a[30]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[31] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[31]),
        .Q(int_agg_result_a[31]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[3]),
        .Q(int_agg_result_a[3]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[4]),
        .Q(int_agg_result_a[4]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[5]),
        .Q(int_agg_result_a[5]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[6] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[6]),
        .Q(int_agg_result_a[6]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[7] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[7]),
        .Q(int_agg_result_a[7]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[8] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[8]),
        .Q(int_agg_result_a[8]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_a_reg[9] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(agg_result_a[9]),
        .Q(int_agg_result_a[9]),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFEFAAAA)) 
    int_agg_result_b_ap_vld_i_1
       (.I0(Q[2]),
        .I1(\rdata_data[7]_i_3_n_3 ),
        .I2(ar_hs),
        .I3(int_agg_result_a_ap_vld_i_3_n_3),
        .I4(int_agg_result_b_ap_vld),
        .O(int_agg_result_b_ap_vld_i_1_n_3));
  FDRE int_agg_result_b_ap_vld_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_agg_result_b_ap_vld_i_1_n_3),
        .Q(int_agg_result_b_ap_vld),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[0] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [0]),
        .Q(int_agg_result_b[0]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[10] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [10]),
        .Q(int_agg_result_b[10]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[11] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [11]),
        .Q(int_agg_result_b[11]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[12] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [12]),
        .Q(int_agg_result_b[12]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[13] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [13]),
        .Q(int_agg_result_b[13]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[14] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [14]),
        .Q(int_agg_result_b[14]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[15] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [15]),
        .Q(int_agg_result_b[15]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[16] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [16]),
        .Q(int_agg_result_b[16]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[17] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [17]),
        .Q(int_agg_result_b[17]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[18] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [18]),
        .Q(int_agg_result_b[18]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[19] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [19]),
        .Q(int_agg_result_b[19]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[1] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [1]),
        .Q(int_agg_result_b[1]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[20] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [20]),
        .Q(int_agg_result_b[20]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[21] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [21]),
        .Q(int_agg_result_b[21]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[22] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [22]),
        .Q(int_agg_result_b[22]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[23] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [23]),
        .Q(int_agg_result_b[23]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[24] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [24]),
        .Q(int_agg_result_b[24]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[25] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [25]),
        .Q(int_agg_result_b[25]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[26] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [26]),
        .Q(int_agg_result_b[26]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[27] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [27]),
        .Q(int_agg_result_b[27]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[28] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [28]),
        .Q(int_agg_result_b[28]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[29] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [29]),
        .Q(int_agg_result_b[29]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[2] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [2]),
        .Q(int_agg_result_b[2]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[30] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [30]),
        .Q(int_agg_result_b[30]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[31] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [31]),
        .Q(int_agg_result_b[31]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[3] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [3]),
        .Q(int_agg_result_b[3]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[4] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [4]),
        .Q(int_agg_result_b[4]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[5] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [5]),
        .Q(int_agg_result_b[5]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[6] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [6]),
        .Q(int_agg_result_b[6]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[7] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [7]),
        .Q(int_agg_result_b[7]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[8] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [8]),
        .Q(int_agg_result_b[8]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_agg_result_b_reg[9] 
       (.C(ap_clk),
        .CE(Q[2]),
        .D(\searched_read_reg_314_reg[31]_0 [9]),
        .Q(int_agg_result_b[9]),
        .R(reset));
  LUT6 #(
    .INIT(64'hFEFFFFFFAAAAAAAA)) 
    int_ap_done_i_1
       (.I0(Q[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(ar_hs),
        .I4(int_ap_done_i_2_n_3),
        .I5(int_ap_done),
        .O(int_ap_done_i_1_n_3));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    int_ap_done_i_2
       (.I0(s_axi_CONTROL_BUS_ARADDR[3]),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .O(int_ap_done_i_2_n_3));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_done_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_done_i_1_n_3),
        .Q(int_ap_done),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h2)) 
    int_ap_idle_i_1
       (.I0(Q[0]),
        .I1(ap_start),
        .O(ap_idle));
  FDRE int_ap_idle_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_idle),
        .Q(int_ap_idle),
        .R(reset));
  FDRE int_ap_ready_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(int_ap_ready),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFBF8)) 
    int_ap_start_i_1
       (.I0(int_auto_restart),
        .I1(Q[2]),
        .I2(int_ap_start3_out),
        .I3(ap_start),
        .O(int_ap_start_i_1_n_3));
  LUT6 #(
    .INIT(64'h0000000002000000)) 
    int_ap_start_i_2
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_3_[3] ),
        .I2(\waddr_reg_n_3_[2] ),
        .I3(\int_searched[31]_i_3_n_3 ),
        .I4(s_axi_CONTROL_BUS_WSTRB[0]),
        .I5(\waddr_reg_n_3_[5] ),
        .O(int_ap_start3_out));
  FDRE #(
    .INIT(1'b0)) 
    int_ap_start_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_ap_start_i_1_n_3),
        .Q(ap_start),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    int_auto_restart_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(\int_ier[1]_i_2_n_3 ),
        .I2(\waddr_reg_n_3_[2] ),
        .I3(\waddr_reg_n_3_[3] ),
        .I4(int_auto_restart),
        .O(int_auto_restart_i_1_n_3));
  FDRE #(
    .INIT(1'b0)) 
    int_auto_restart_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_auto_restart_i_1_n_3),
        .Q(int_auto_restart),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    int_gie_i_1
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\int_ier[1]_i_2_n_3 ),
        .I2(\waddr_reg_n_3_[2] ),
        .I3(\waddr_reg_n_3_[3] ),
        .I4(int_gie_reg_n_3),
        .O(int_gie_i_1_n_3));
  FDRE #(
    .INIT(1'b0)) 
    int_gie_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(int_gie_i_1_n_3),
        .Q(int_gie_reg_n_3),
        .R(reset));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(\waddr_reg_n_3_[2] ),
        .I2(\int_ier[1]_i_2_n_3 ),
        .I3(\waddr_reg_n_3_[3] ),
        .I4(\int_ier_reg_n_3_[0] ),
        .O(\int_ier[0]_i_1_n_3 ));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \int_ier[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(\waddr_reg_n_3_[2] ),
        .I2(\int_ier[1]_i_2_n_3 ),
        .I3(\waddr_reg_n_3_[3] ),
        .I4(p_0_in),
        .O(\int_ier[1]_i_1_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \int_ier[1]_i_2 
       (.I0(\int_searched[31]_i_3_n_3 ),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\waddr_reg_n_3_[5] ),
        .O(\int_ier[1]_i_2_n_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[0]_i_1_n_3 ),
        .Q(\int_ier_reg_n_3_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_ier_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_ier[1]_i_1_n_3 ),
        .Q(p_0_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(int_isr6_out),
        .I2(\int_ier_reg_n_3_[0] ),
        .I3(Q[2]),
        .I4(\int_isr_reg_n_3_[0] ),
        .O(\int_isr[0]_i_1_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00800000)) 
    \int_isr[0]_i_2 
       (.I0(\waddr_reg_n_3_[2] ),
        .I1(\int_searched[31]_i_3_n_3 ),
        .I2(s_axi_CONTROL_BUS_WSTRB[0]),
        .I3(\waddr_reg_n_3_[5] ),
        .I4(\waddr_reg_n_3_[3] ),
        .O(int_isr6_out));
  LUT5 #(
    .INIT(32'hF777F888)) 
    \int_isr[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(int_isr6_out),
        .I2(p_0_in),
        .I3(Q[2]),
        .I4(p_1_in),
        .O(\int_isr[1]_i_1_n_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[0]_i_1_n_3 ),
        .Q(\int_isr_reg_n_3_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \int_isr_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\int_isr[1]_i_1_n_3 ),
        .Q(p_1_in),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[0]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [0]),
        .O(\or [0]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[10]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[10]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [10]),
        .O(\or [10]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[11]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[11]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [11]),
        .O(\or [11]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[12]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[12]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [12]),
        .O(\or [12]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[13]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[13]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [13]),
        .O(\or [13]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[14]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[14]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [14]),
        .O(\or [14]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[15]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[15]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [15]),
        .O(\or [15]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[16]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[16]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [16]),
        .O(\or [16]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[17]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[17]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [17]),
        .O(\or [17]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[18]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[18]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [18]),
        .O(\or [18]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[19]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[19]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [19]),
        .O(\or [19]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[1]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[1]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [1]),
        .O(\or [1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[20]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[20]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [20]),
        .O(\or [20]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[21]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[21]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [21]),
        .O(\or [21]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[22]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[22]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [22]),
        .O(\or [22]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[23]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[23]),
        .I1(s_axi_CONTROL_BUS_WSTRB[2]),
        .I2(\searched_read_reg_314_reg[31] [23]),
        .O(\or [23]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[24]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[24]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [24]),
        .O(\or [24]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[25]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[25]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [25]),
        .O(\or [25]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[26]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[26]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [26]),
        .O(\or [26]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[27]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[27]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [27]),
        .O(\or [27]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[28]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[28]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [28]),
        .O(\or [28]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[29]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[29]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [29]),
        .O(\or [29]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[2]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[2]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [2]),
        .O(\or [2]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[30]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[30]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [30]),
        .O(\or [30]));
  LUT4 #(
    .INIT(16'h0008)) 
    \int_searched[31]_i_1 
       (.I0(\int_searched[31]_i_3_n_3 ),
        .I1(\waddr_reg_n_3_[5] ),
        .I2(\waddr_reg_n_3_[3] ),
        .I3(\waddr_reg_n_3_[2] ),
        .O(p_0_in13_out));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_WDATA[31]),
        .I1(s_axi_CONTROL_BUS_WSTRB[3]),
        .I2(\searched_read_reg_314_reg[31] [31]),
        .O(\or [31]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \int_searched[31]_i_3 
       (.I0(s_axi_CONTROL_BUS_WVALID),
        .I1(\waddr_reg_n_3_[1] ),
        .I2(\waddr_reg_n_3_[0] ),
        .I3(\waddr_reg_n_3_[4] ),
        .I4(out[1]),
        .O(\int_searched[31]_i_3_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[3]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[3]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [3]),
        .O(\or [3]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[4]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[4]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [4]),
        .O(\or [4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[5]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[5]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [5]),
        .O(\or [5]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[6]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[6]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [6]),
        .O(\or [6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[7]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[7]),
        .I1(s_axi_CONTROL_BUS_WSTRB[0]),
        .I2(\searched_read_reg_314_reg[31] [7]),
        .O(\or [7]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[8]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[8]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [8]),
        .O(\or [8]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \int_searched[9]_i_1 
       (.I0(s_axi_CONTROL_BUS_WDATA[9]),
        .I1(s_axi_CONTROL_BUS_WSTRB[1]),
        .I2(\searched_read_reg_314_reg[31] [9]),
        .O(\or [9]));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[0] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [0]),
        .Q(\searched_read_reg_314_reg[31] [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[10] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [10]),
        .Q(\searched_read_reg_314_reg[31] [10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[11] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [11]),
        .Q(\searched_read_reg_314_reg[31] [11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[12] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [12]),
        .Q(\searched_read_reg_314_reg[31] [12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[13] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [13]),
        .Q(\searched_read_reg_314_reg[31] [13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[14] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [14]),
        .Q(\searched_read_reg_314_reg[31] [14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[15] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [15]),
        .Q(\searched_read_reg_314_reg[31] [15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[16] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [16]),
        .Q(\searched_read_reg_314_reg[31] [16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[17] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [17]),
        .Q(\searched_read_reg_314_reg[31] [17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[18] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [18]),
        .Q(\searched_read_reg_314_reg[31] [18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[19] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [19]),
        .Q(\searched_read_reg_314_reg[31] [19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[1] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [1]),
        .Q(\searched_read_reg_314_reg[31] [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[20] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [20]),
        .Q(\searched_read_reg_314_reg[31] [20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[21] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [21]),
        .Q(\searched_read_reg_314_reg[31] [21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[22] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [22]),
        .Q(\searched_read_reg_314_reg[31] [22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[23] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [23]),
        .Q(\searched_read_reg_314_reg[31] [23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[24] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [24]),
        .Q(\searched_read_reg_314_reg[31] [24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[25] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [25]),
        .Q(\searched_read_reg_314_reg[31] [25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[26] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [26]),
        .Q(\searched_read_reg_314_reg[31] [26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[27] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [27]),
        .Q(\searched_read_reg_314_reg[31] [27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[28] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [28]),
        .Q(\searched_read_reg_314_reg[31] [28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[29] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [29]),
        .Q(\searched_read_reg_314_reg[31] [29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[2] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [2]),
        .Q(\searched_read_reg_314_reg[31] [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[30] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [30]),
        .Q(\searched_read_reg_314_reg[31] [30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[31] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [31]),
        .Q(\searched_read_reg_314_reg[31] [31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[3] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [3]),
        .Q(\searched_read_reg_314_reg[31] [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[4] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [4]),
        .Q(\searched_read_reg_314_reg[31] [4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[5] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [5]),
        .Q(\searched_read_reg_314_reg[31] [5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[6] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [6]),
        .Q(\searched_read_reg_314_reg[31] [6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[7] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [7]),
        .Q(\searched_read_reg_314_reg[31] [7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[8] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [8]),
        .Q(\searched_read_reg_314_reg[31] [8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \int_searched_reg[9] 
       (.C(ap_clk),
        .CE(p_0_in13_out),
        .D(\or [9]),
        .Q(\searched_read_reg_314_reg[31] [9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hA8)) 
    interrupt_INST_0
       (.I0(int_gie_reg_n_3),
        .I1(p_1_in),
        .I2(\int_isr_reg_n_3_[0] ),
        .O(interrupt));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h8088)) 
    \invdar_reg_175[6]_i_1 
       (.I0(ap_start),
        .I1(Q[0]),
        .I2(\tmp_1_reg_335_reg[0] ),
        .I3(Q[1]),
        .O(SR));
  LUT6 #(
    .INIT(64'hFFFFFFFF0100010F)) 
    \rdata_data[0]_i_1 
       (.I0(\rdata_data[0]_i_2_n_3 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .I2(\rdata_data[0]_i_3_n_3 ),
        .I3(s_axi_CONTROL_BUS_ARADDR[2]),
        .I4(\rdata_data[0]_i_4_n_3 ),
        .I5(\rdata_data[0]_i_5_n_3 ),
        .O(rdata_data[0]));
  LUT6 #(
    .INIT(64'h00330F55FF330F55)) 
    \rdata_data[0]_i_2 
       (.I0(int_gie_reg_n_3),
        .I1(\int_isr_reg_n_3_[0] ),
        .I2(int_agg_result_a_ap_vld),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(int_agg_result_b_ap_vld),
        .O(\rdata_data[0]_i_2_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \rdata_data[0]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[0]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .O(\rdata_data[0]_i_3_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hF1FFFDFF)) 
    \rdata_data[0]_i_4 
       (.I0(\int_ier_reg_n_3_[0] ),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(int_agg_result_b[0]),
        .O(\rdata_data[0]_i_4_n_3 ));
  LUT6 #(
    .INIT(64'h08AA08A0080A0800)) 
    \rdata_data[0]_i_5 
       (.I0(int_ap_done_i_2_n_3),
        .I1(int_agg_result_a[0]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(ap_start),
        .I5(\searched_read_reg_314_reg[31] [0]),
        .O(\rdata_data[0]_i_5_n_3 ));
  LUT6 #(
    .INIT(64'h00A00CF000A00C00)) 
    \rdata_data[10]_i_1 
       (.I0(int_agg_result_b[10]),
        .I1(\searched_read_reg_314_reg[31] [10]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(int_agg_result_a[10]),
        .O(\rdata_data[10]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[11]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [11]),
        .I1(int_agg_result_a[11]),
        .I2(int_agg_result_b[11]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[11]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[12]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [12]),
        .I1(int_agg_result_a[12]),
        .I2(int_agg_result_b[12]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[12]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0A000CF00A000C00)) 
    \rdata_data[13]_i_1 
       (.I0(int_agg_result_b[13]),
        .I1(int_agg_result_a[13]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(\searched_read_reg_314_reg[31] [13]),
        .O(\rdata_data[13]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000CCAA00F00000)) 
    \rdata_data[14]_i_1 
       (.I0(int_agg_result_a[14]),
        .I1(int_agg_result_b[14]),
        .I2(\searched_read_reg_314_reg[31] [14]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[14]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[15]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [15]),
        .I1(int_agg_result_a[15]),
        .I2(int_agg_result_b[15]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[15]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h000000AACCF00000)) 
    \rdata_data[16]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [16]),
        .I1(int_agg_result_b[16]),
        .I2(int_agg_result_a[16]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[16]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[17]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [17]),
        .I1(int_agg_result_a[17]),
        .I2(int_agg_result_b[17]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[17]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[18]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [18]),
        .I1(int_agg_result_a[18]),
        .I2(int_agg_result_b[18]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[18]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h00A00CF000A00C00)) 
    \rdata_data[19]_i_1 
       (.I0(int_agg_result_b[19]),
        .I1(\searched_read_reg_314_reg[31] [19]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(int_agg_result_a[19]),
        .O(\rdata_data[19]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'hAAEAAAEAFFFFAAEA)) 
    \rdata_data[1]_i_1 
       (.I0(\rdata_data[1]_i_2_n_3 ),
        .I1(\rdata_data[1]_i_3_n_3 ),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .I3(\rdata_data[1]_i_4_n_3 ),
        .I4(int_agg_result_b[1]),
        .I5(\rdata_data[1]_i_5_n_3 ),
        .O(rdata_data[1]));
  LUT6 #(
    .INIT(64'h08AA08A0080A0800)) 
    \rdata_data[1]_i_2 
       (.I0(int_ap_done_i_2_n_3),
        .I1(int_agg_result_a[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(int_ap_done),
        .I5(\searched_read_reg_314_reg[31] [1]),
        .O(\rdata_data[1]_i_2_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h000B0008)) 
    \rdata_data[1]_i_3 
       (.I0(p_1_in),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(p_0_in),
        .O(\rdata_data[1]_i_3_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \rdata_data[1]_i_4 
       (.I0(s_axi_CONTROL_BUS_ARADDR[5]),
        .I1(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[1]_i_4_n_3 ));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    \rdata_data[1]_i_5 
       (.I0(s_axi_CONTROL_BUS_ARADDR[2]),
        .I1(s_axi_CONTROL_BUS_ARADDR[1]),
        .I2(s_axi_CONTROL_BUS_ARADDR[0]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[1]_i_5_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000CCAA00)) 
    \rdata_data[20]_i_1 
       (.I0(int_agg_result_a[20]),
        .I1(\searched_read_reg_314_reg[31] [20]),
        .I2(int_agg_result_b[20]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[20]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000CCAA00F00000)) 
    \rdata_data[21]_i_1 
       (.I0(int_agg_result_a[21]),
        .I1(int_agg_result_b[21]),
        .I2(\searched_read_reg_314_reg[31] [21]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[21]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000AACC00F00000)) 
    \rdata_data[22]_i_1 
       (.I0(int_agg_result_b[22]),
        .I1(int_agg_result_a[22]),
        .I2(\searched_read_reg_314_reg[31] [22]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[22]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h000000AACCF00000)) 
    \rdata_data[23]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [23]),
        .I1(int_agg_result_b[23]),
        .I2(int_agg_result_a[23]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[23]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h000000CCAAF00000)) 
    \rdata_data[24]_i_1 
       (.I0(int_agg_result_b[24]),
        .I1(\searched_read_reg_314_reg[31] [24]),
        .I2(int_agg_result_a[24]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[24]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000AACC00F00000)) 
    \rdata_data[25]_i_1 
       (.I0(int_agg_result_b[25]),
        .I1(int_agg_result_a[25]),
        .I2(\searched_read_reg_314_reg[31] [25]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[25]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h00A00CF000A00C00)) 
    \rdata_data[26]_i_1 
       (.I0(int_agg_result_b[26]),
        .I1(\searched_read_reg_314_reg[31] [26]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(int_agg_result_a[26]),
        .O(\rdata_data[26]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000CCAA00)) 
    \rdata_data[27]_i_1 
       (.I0(int_agg_result_a[27]),
        .I1(\searched_read_reg_314_reg[31] [27]),
        .I2(int_agg_result_b[27]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[27]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000CCAA00F00000)) 
    \rdata_data[28]_i_1 
       (.I0(int_agg_result_a[28]),
        .I1(int_agg_result_b[28]),
        .I2(\searched_read_reg_314_reg[31] [28]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[28]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000CCAA00)) 
    \rdata_data[29]_i_1 
       (.I0(int_agg_result_a[29]),
        .I1(\searched_read_reg_314_reg[31] [29]),
        .I2(int_agg_result_b[29]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[29]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'hAAAAAAABAAAAAAAA)) 
    \rdata_data[2]_i_1 
       (.I0(\rdata_data[2]_i_2_n_3 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(\rdata_data[7]_i_3_n_3 ),
        .I5(int_agg_result_b[2]),
        .O(rdata_data[2]));
  LUT6 #(
    .INIT(64'h08AA08A0080A0800)) 
    \rdata_data[2]_i_2 
       (.I0(int_ap_done_i_2_n_3),
        .I1(int_agg_result_a[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(int_ap_idle),
        .I5(\searched_read_reg_314_reg[31] [2]),
        .O(\rdata_data[2]_i_2_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000CCAA00)) 
    \rdata_data[30]_i_1 
       (.I0(int_agg_result_a[30]),
        .I1(\searched_read_reg_314_reg[31] [30]),
        .I2(int_agg_result_b[30]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[30]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h1010101010101000)) 
    \rdata_data[31]_i_1 
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .I2(s_axi_CONTROL_BUS_ARVALID),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(s_axi_CONTROL_BUS_ARADDR[1]),
        .I5(s_axi_CONTROL_BUS_ARADDR[2]),
        .O(\rdata_data[31]_i_1_n_3 ));
  LUT3 #(
    .INIT(8'h02)) 
    \rdata_data[31]_i_2 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(rstate[1]),
        .I2(rstate[0]),
        .O(ar_hs));
  LUT6 #(
    .INIT(64'h000000AACCF00000)) 
    \rdata_data[31]_i_3 
       (.I0(\searched_read_reg_314_reg[31] [31]),
        .I1(int_agg_result_b[31]),
        .I2(int_agg_result_a[31]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[4]),
        .I5(s_axi_CONTROL_BUS_ARADDR[5]),
        .O(\rdata_data[31]_i_3_n_3 ));
  LUT6 #(
    .INIT(64'hAAAAAAABAAAAAAAA)) 
    \rdata_data[3]_i_1 
       (.I0(\rdata_data[3]_i_2_n_3 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(\rdata_data[7]_i_3_n_3 ),
        .I5(int_agg_result_b[3]),
        .O(rdata_data[3]));
  LUT6 #(
    .INIT(64'h0A8A0A80008A0080)) 
    \rdata_data[3]_i_2 
       (.I0(int_ap_done_i_2_n_3),
        .I1(\searched_read_reg_314_reg[31] [3]),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(int_ap_ready),
        .I5(int_agg_result_a[3]),
        .O(\rdata_data[3]_i_2_n_3 ));
  LUT6 #(
    .INIT(64'h00A00CF000A00C00)) 
    \rdata_data[4]_i_1 
       (.I0(int_agg_result_b[4]),
        .I1(\searched_read_reg_314_reg[31] [4]),
        .I2(s_axi_CONTROL_BUS_ARADDR[4]),
        .I3(s_axi_CONTROL_BUS_ARADDR[5]),
        .I4(s_axi_CONTROL_BUS_ARADDR[3]),
        .I5(int_agg_result_a[4]),
        .O(\rdata_data[4]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000AACC00F00000)) 
    \rdata_data[5]_i_1 
       (.I0(int_agg_result_b[5]),
        .I1(int_agg_result_a[5]),
        .I2(\searched_read_reg_314_reg[31] [5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[3]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[4]),
        .O(\rdata_data[5]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[6]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [6]),
        .I1(int_agg_result_a[6]),
        .I2(int_agg_result_b[6]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[6]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'hAAAAAAABAAAAAAAA)) 
    \rdata_data[7]_i_1 
       (.I0(\rdata_data[7]_i_2_n_3 ),
        .I1(s_axi_CONTROL_BUS_ARADDR[2]),
        .I2(s_axi_CONTROL_BUS_ARADDR[1]),
        .I3(s_axi_CONTROL_BUS_ARADDR[0]),
        .I4(\rdata_data[7]_i_3_n_3 ),
        .I5(int_agg_result_b[7]),
        .O(rdata_data[7]));
  LUT6 #(
    .INIT(64'h0AA800A80A080008)) 
    \rdata_data[7]_i_2 
       (.I0(int_ap_done_i_2_n_3),
        .I1(int_auto_restart),
        .I2(s_axi_CONTROL_BUS_ARADDR[5]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(int_agg_result_a[7]),
        .I5(\searched_read_reg_314_reg[31] [7]),
        .O(\rdata_data[7]_i_2_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \rdata_data[7]_i_3 
       (.I0(s_axi_CONTROL_BUS_ARADDR[4]),
        .I1(s_axi_CONTROL_BUS_ARADDR[5]),
        .I2(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[7]_i_3_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000CCAA00)) 
    \rdata_data[8]_i_1 
       (.I0(int_agg_result_a[8]),
        .I1(\searched_read_reg_314_reg[31] [8]),
        .I2(int_agg_result_b[8]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[8]_i_1_n_3 ));
  LUT6 #(
    .INIT(64'h0000F00000AACC00)) 
    \rdata_data[9]_i_1 
       (.I0(\searched_read_reg_314_reg[31] [9]),
        .I1(int_agg_result_a[9]),
        .I2(int_agg_result_b[9]),
        .I3(s_axi_CONTROL_BUS_ARADDR[4]),
        .I4(s_axi_CONTROL_BUS_ARADDR[5]),
        .I5(s_axi_CONTROL_BUS_ARADDR[3]),
        .O(\rdata_data[9]_i_1_n_3 ));
  FDRE \rdata_data_reg[0] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[0]),
        .Q(s_axi_CONTROL_BUS_RDATA[0]),
        .R(1'b0));
  FDRE \rdata_data_reg[10] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[10]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[10]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[11] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[11]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[11]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[12] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[12]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[12]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[13] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[13]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[13]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[14] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[14]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[14]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[15] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[15]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[15]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[16] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[16]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[16]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[17] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[17]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[17]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[18] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[18]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[18]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[19] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[19]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[19]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[1] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[1]),
        .Q(s_axi_CONTROL_BUS_RDATA[1]),
        .R(1'b0));
  FDRE \rdata_data_reg[20] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[20]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[20]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[21] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[21]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[21]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[22] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[22]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[22]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[23] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[23]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[23]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[24] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[24]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[24]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[25] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[25]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[25]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[26] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[26]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[26]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[27] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[27]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[27]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[28] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[28]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[28]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[29] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[29]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[29]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[2] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[2]),
        .Q(s_axi_CONTROL_BUS_RDATA[2]),
        .R(1'b0));
  FDRE \rdata_data_reg[30] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[30]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[30]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[31] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[31]_i_3_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[31]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[3] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[3]),
        .Q(s_axi_CONTROL_BUS_RDATA[3]),
        .R(1'b0));
  FDRE \rdata_data_reg[4] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[4]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[4]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[5] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[5]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[5]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[6] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[6]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[6]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[7] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(rdata_data[7]),
        .Q(s_axi_CONTROL_BUS_RDATA[7]),
        .R(1'b0));
  FDRE \rdata_data_reg[8] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[8]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[8]),
        .R(\rdata_data[31]_i_1_n_3 ));
  FDRE \rdata_data_reg[9] 
       (.C(ap_clk),
        .CE(ar_hs),
        .D(\rdata_data[9]_i_1_n_3 ),
        .Q(s_axi_CONTROL_BUS_RDATA[9]),
        .R(\rdata_data[31]_i_1_n_3 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0232)) 
    \rstate[0]_i_1 
       (.I0(s_axi_CONTROL_BUS_ARVALID),
        .I1(rstate[1]),
        .I2(rstate[0]),
        .I3(s_axi_CONTROL_BUS_RREADY),
        .O(\rstate[0]_i_1_n_3 ));
  FDRE #(
    .INIT(1'b0)) 
    \rstate_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\rstate[0]_i_1_n_3 ),
        .Q(rstate[0]),
        .R(reset));
  FDSE #(
    .INIT(1'b1)) 
    \rstate_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(1'b0),
        .Q(rstate[1]),
        .S(reset));
  LUT2 #(
    .INIT(4'h1)) 
    s_axi_CONTROL_BUS_ARREADY_INST_0
       (.I0(rstate[1]),
        .I1(rstate[0]),
        .O(s_axi_CONTROL_BUS_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_CONTROL_BUS_RVALID_INST_0
       (.I0(rstate[0]),
        .I1(rstate[1]),
        .O(s_axi_CONTROL_BUS_RVALID));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \searched_read_reg_314[31]_i_1 
       (.I0(Q[0]),
        .I1(ap_start),
        .O(E));
  LUT2 #(
    .INIT(4'h8)) 
    \waddr[5]_i_1 
       (.I0(out[0]),
        .I1(s_axi_CONTROL_BUS_AWVALID),
        .O(waddr));
  FDRE \waddr_reg[0] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[0]),
        .Q(\waddr_reg_n_3_[0] ),
        .R(1'b0));
  FDRE \waddr_reg[1] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[1]),
        .Q(\waddr_reg_n_3_[1] ),
        .R(1'b0));
  FDRE \waddr_reg[2] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[2]),
        .Q(\waddr_reg_n_3_[2] ),
        .R(1'b0));
  FDRE \waddr_reg[3] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[3]),
        .Q(\waddr_reg_n_3_[3] ),
        .R(1'b0));
  FDRE \waddr_reg[4] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[4]),
        .Q(\waddr_reg_n_3_[4] ),
        .R(1'b0));
  FDRE \waddr_reg[5] 
       (.C(ap_clk),
        .CE(waddr),
        .D(s_axi_CONTROL_BUS_AWADDR[5]),
        .Q(\waddr_reg_n_3_[5] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input
   (CO,
    ap_clk,
    DIADI,
    Q,
    \i_reg_186_reg[5] ,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \searched_read_reg_314_reg[31] ,
    \tmp_reg_325_reg[6] ,
    \a_a_1_reg_197_reg[5] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] );
  output [0:0]CO;
  input ap_clk;
  input [4:0]DIADI;
  input [2:0]Q;
  input [5:0]\i_reg_186_reg[5] ;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [31:0]\searched_read_reg_314_reg[31] ;
  input [6:0]\tmp_reg_325_reg[6] ;
  input [5:0]\a_a_1_reg_197_reg[5] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;

  wire [0:0]CO;
  wire [4:0]DIADI;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [5:0]\a_a_1_reg_197_reg[5] ;
  wire ap_clk;
  wire [5:0]\i_reg_186_reg[5] ;
  wire [31:0]\searched_read_reg_314_reg[31] ;
  wire [6:0]\tmp_reg_325_reg[6] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram Adder2_input_ram_U
       (.CO(CO),
        .DIADI(DIADI),
        .\INPUT_STREAM_V_data_V_0_payload_A_reg[31] (\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ),
        .\INPUT_STREAM_V_data_V_0_payload_B_reg[31] (\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ),
        .INPUT_STREAM_V_data_V_0_sel(INPUT_STREAM_V_data_V_0_sel),
        .\INPUT_STREAM_V_data_V_0_state_reg[0] (\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .Q(Q),
        .\a_a_1_reg_197_reg[5] (\a_a_1_reg_197_reg[5] ),
        .ap_clk(ap_clk),
        .\i_reg_186_reg[5] (\i_reg_186_reg[5] ),
        .\searched_read_reg_314_reg[31] (\searched_read_reg_314_reg[31] ),
        .\tmp_reg_325_reg[6] (\tmp_reg_325_reg[6] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2_input_ram
   (CO,
    ap_clk,
    DIADI,
    Q,
    \i_reg_186_reg[5] ,
    \INPUT_STREAM_V_data_V_0_state_reg[0] ,
    \searched_read_reg_314_reg[31] ,
    \tmp_reg_325_reg[6] ,
    \a_a_1_reg_197_reg[5] ,
    \INPUT_STREAM_V_data_V_0_payload_A_reg[31] ,
    INPUT_STREAM_V_data_V_0_sel,
    \INPUT_STREAM_V_data_V_0_payload_B_reg[31] );
  output [0:0]CO;
  input ap_clk;
  input [4:0]DIADI;
  input [2:0]Q;
  input [5:0]\i_reg_186_reg[5] ;
  input \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  input [31:0]\searched_read_reg_314_reg[31] ;
  input [6:0]\tmp_reg_325_reg[6] ;
  input [5:0]\a_a_1_reg_197_reg[5] ;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  input INPUT_STREAM_V_data_V_0_sel;
  input [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;

  wire [0:0]CO;
  wire [4:0]DIADI;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_A_reg[31] ;
  wire [26:0]\INPUT_STREAM_V_data_V_0_payload_B_reg[31] ;
  wire INPUT_STREAM_V_data_V_0_sel;
  wire \INPUT_STREAM_V_data_V_0_state_reg[0] ;
  wire [2:0]Q;
  wire [5:0]\a_a_1_reg_197_reg[5] ;
  wire [6:0]address0;
  wire \ap_CS_fsm[7]_i_10_n_3 ;
  wire \ap_CS_fsm[7]_i_11_n_3 ;
  wire \ap_CS_fsm[7]_i_12_n_3 ;
  wire \ap_CS_fsm[7]_i_13_n_3 ;
  wire \ap_CS_fsm[7]_i_14_n_3 ;
  wire \ap_CS_fsm[7]_i_15_n_3 ;
  wire \ap_CS_fsm[7]_i_4_n_3 ;
  wire \ap_CS_fsm[7]_i_5_n_3 ;
  wire \ap_CS_fsm[7]_i_6_n_3 ;
  wire \ap_CS_fsm[7]_i_8_n_3 ;
  wire \ap_CS_fsm[7]_i_9_n_3 ;
  wire \ap_CS_fsm_reg[7]_i_2_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_2_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_3 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_4 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_3_n_6 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_3 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_4 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_5 ;
  wire \ap_CS_fsm_reg[7]_i_7_n_6 ;
  wire ap_clk;
  wire [31:5]d0;
  wire [5:0]\i_reg_186_reg[5] ;
  wire [31:0]input_q0;
  wire ram_reg_i_1_n_3;
  wire [31:0]\searched_read_reg_314_reg[31] ;
  wire [6:0]\tmp_reg_325_reg[6] ;
  wire we0;
  wire [3:3]\NLW_ap_CS_fsm_reg[7]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_ap_CS_fsm_reg[7]_i_7_O_UNCONNECTED ;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_10 
       (.I0(\searched_read_reg_314_reg[31] [16]),
        .I1(input_q0[16]),
        .I2(\searched_read_reg_314_reg[31] [17]),
        .I3(input_q0[17]),
        .I4(input_q0[15]),
        .I5(\searched_read_reg_314_reg[31] [15]),
        .O(\ap_CS_fsm[7]_i_10_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_11 
       (.I0(\searched_read_reg_314_reg[31] [14]),
        .I1(input_q0[14]),
        .I2(\searched_read_reg_314_reg[31] [12]),
        .I3(input_q0[12]),
        .I4(input_q0[13]),
        .I5(\searched_read_reg_314_reg[31] [13]),
        .O(\ap_CS_fsm[7]_i_11_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_12 
       (.I0(\searched_read_reg_314_reg[31] [9]),
        .I1(input_q0[9]),
        .I2(\searched_read_reg_314_reg[31] [11]),
        .I3(input_q0[11]),
        .I4(input_q0[10]),
        .I5(\searched_read_reg_314_reg[31] [10]),
        .O(\ap_CS_fsm[7]_i_12_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_13 
       (.I0(\searched_read_reg_314_reg[31] [7]),
        .I1(input_q0[7]),
        .I2(\searched_read_reg_314_reg[31] [8]),
        .I3(input_q0[8]),
        .I4(input_q0[6]),
        .I5(\searched_read_reg_314_reg[31] [6]),
        .O(\ap_CS_fsm[7]_i_13_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_14 
       (.I0(\searched_read_reg_314_reg[31] [5]),
        .I1(input_q0[5]),
        .I2(\searched_read_reg_314_reg[31] [3]),
        .I3(input_q0[3]),
        .I4(input_q0[4]),
        .I5(\searched_read_reg_314_reg[31] [4]),
        .O(\ap_CS_fsm[7]_i_14_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_15 
       (.I0(\searched_read_reg_314_reg[31] [0]),
        .I1(input_q0[0]),
        .I2(\searched_read_reg_314_reg[31] [2]),
        .I3(input_q0[2]),
        .I4(input_q0[1]),
        .I5(\searched_read_reg_314_reg[31] [1]),
        .O(\ap_CS_fsm[7]_i_15_n_3 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ap_CS_fsm[7]_i_4 
       (.I0(input_q0[30]),
        .I1(\searched_read_reg_314_reg[31] [30]),
        .I2(\searched_read_reg_314_reg[31] [31]),
        .I3(input_q0[31]),
        .O(\ap_CS_fsm[7]_i_4_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_5 
       (.I0(\searched_read_reg_314_reg[31] [28]),
        .I1(input_q0[28]),
        .I2(\searched_read_reg_314_reg[31] [29]),
        .I3(input_q0[29]),
        .I4(input_q0[27]),
        .I5(\searched_read_reg_314_reg[31] [27]),
        .O(\ap_CS_fsm[7]_i_5_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_6 
       (.I0(\searched_read_reg_314_reg[31] [26]),
        .I1(input_q0[26]),
        .I2(\searched_read_reg_314_reg[31] [24]),
        .I3(input_q0[24]),
        .I4(input_q0[25]),
        .I5(\searched_read_reg_314_reg[31] [25]),
        .O(\ap_CS_fsm[7]_i_6_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_8 
       (.I0(\searched_read_reg_314_reg[31] [21]),
        .I1(input_q0[21]),
        .I2(\searched_read_reg_314_reg[31] [23]),
        .I3(input_q0[23]),
        .I4(input_q0[22]),
        .I5(\searched_read_reg_314_reg[31] [22]),
        .O(\ap_CS_fsm[7]_i_8_n_3 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ap_CS_fsm[7]_i_9 
       (.I0(\searched_read_reg_314_reg[31] [20]),
        .I1(input_q0[20]),
        .I2(\searched_read_reg_314_reg[31] [18]),
        .I3(input_q0[18]),
        .I4(input_q0[19]),
        .I5(\searched_read_reg_314_reg[31] [19]),
        .O(\ap_CS_fsm[7]_i_9_n_3 ));
  CARRY4 \ap_CS_fsm_reg[7]_i_2 
       (.CI(\ap_CS_fsm_reg[7]_i_3_n_3 ),
        .CO({\NLW_ap_CS_fsm_reg[7]_i_2_CO_UNCONNECTED [3],CO,\ap_CS_fsm_reg[7]_i_2_n_5 ,\ap_CS_fsm_reg[7]_i_2_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,\ap_CS_fsm[7]_i_4_n_3 ,\ap_CS_fsm[7]_i_5_n_3 ,\ap_CS_fsm[7]_i_6_n_3 }));
  CARRY4 \ap_CS_fsm_reg[7]_i_3 
       (.CI(\ap_CS_fsm_reg[7]_i_7_n_3 ),
        .CO({\ap_CS_fsm_reg[7]_i_3_n_3 ,\ap_CS_fsm_reg[7]_i_3_n_4 ,\ap_CS_fsm_reg[7]_i_3_n_5 ,\ap_CS_fsm_reg[7]_i_3_n_6 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_3_O_UNCONNECTED [3:0]),
        .S({\ap_CS_fsm[7]_i_8_n_3 ,\ap_CS_fsm[7]_i_9_n_3 ,\ap_CS_fsm[7]_i_10_n_3 ,\ap_CS_fsm[7]_i_11_n_3 }));
  CARRY4 \ap_CS_fsm_reg[7]_i_7 
       (.CI(1'b0),
        .CO({\ap_CS_fsm_reg[7]_i_7_n_3 ,\ap_CS_fsm_reg[7]_i_7_n_4 ,\ap_CS_fsm_reg[7]_i_7_n_5 ,\ap_CS_fsm_reg[7]_i_7_n_6 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_ap_CS_fsm_reg[7]_i_7_O_UNCONNECTED [3:0]),
        .S({\ap_CS_fsm[7]_i_12_n_3 ,\ap_CS_fsm[7]_i_13_n_3 ,\ap_CS_fsm[7]_i_14_n_3 ,\ap_CS_fsm[7]_i_15_n_3 }));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "2112" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "65" *) 
  (* bram_ext_slice_begin = "18" *) 
  (* bram_ext_slice_end = "31" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,address0,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI({d0[15:5],DIADI}),
        .DIBDI({1'b1,1'b1,d0[31:18]}),
        .DIPADIP(d0[17:16]),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(input_q0[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],input_q0[31:18]}),
        .DOPADOP(input_q0[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ram_reg_i_1_n_3),
        .ENBWREN(ram_reg_i_1_n_3),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({we0,we0}),
        .WEBWE({1'b0,1'b0,we0,we0}));
  LUT5 #(
    .INIT(32'hFFFFFFA8)) 
    ram_reg_i_1
       (.I0(Q[1]),
        .I1(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I2(\i_reg_186_reg[5] [5]),
        .I3(Q[2]),
        .I4(Q[0]),
        .O(ram_reg_i_1_n_3));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_10
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [9]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [9]),
        .O(d0[14]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_11
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [8]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [8]),
        .O(d0[13]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_12
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [7]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [7]),
        .O(d0[12]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_13
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [6]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [6]),
        .O(d0[11]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_14
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [5]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [5]),
        .O(d0[10]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_15
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [4]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [4]),
        .O(d0[9]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_16
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [3]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [3]),
        .O(d0[8]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_17
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [2]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [2]),
        .O(d0[7]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_18
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [1]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [1]),
        .O(d0[6]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_19
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [0]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [0]),
        .O(d0[5]));
  LUT3 #(
    .INIT(8'h02)) 
    ram_reg_i_2
       (.I0(\tmp_reg_325_reg[6] [6]),
        .I1(Q[2]),
        .I2(Q[1]),
        .O(address0[6]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_25
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [26]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [26]),
        .O(d0[31]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_26
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [25]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [25]),
        .O(d0[30]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_27
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [24]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [24]),
        .O(d0[29]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_28
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [23]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [23]),
        .O(d0[28]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_29
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [22]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [22]),
        .O(d0[27]));
  LUT5 #(
    .INIT(32'hFAFC0A0C)) 
    ram_reg_i_3
       (.I0(\i_reg_186_reg[5] [5]),
        .I1(\tmp_reg_325_reg[6] [5]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(\a_a_1_reg_197_reg[5] [5]),
        .O(address0[5]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_30
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [21]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [21]),
        .O(d0[26]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_31
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [20]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [20]),
        .O(d0[25]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_32
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [19]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [19]),
        .O(d0[24]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_33
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [18]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [18]),
        .O(d0[23]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_34
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [17]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [17]),
        .O(d0[22]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_35
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [16]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [16]),
        .O(d0[21]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_36
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [15]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [15]),
        .O(d0[20]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_37
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [14]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [14]),
        .O(d0[19]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_38
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [13]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [13]),
        .O(d0[18]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_39
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [12]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [12]),
        .O(d0[17]));
  LUT5 #(
    .INIT(32'hF3C0E2E2)) 
    ram_reg_i_4
       (.I0(\tmp_reg_325_reg[6] [4]),
        .I1(Q[2]),
        .I2(\a_a_1_reg_197_reg[5] [4]),
        .I3(\i_reg_186_reg[5] [4]),
        .I4(Q[1]),
        .O(address0[4]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_40
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [11]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [11]),
        .O(d0[16]));
  LUT4 #(
    .INIT(16'hBAAA)) 
    ram_reg_i_41
       (.I0(Q[0]),
        .I1(\i_reg_186_reg[5] [5]),
        .I2(\INPUT_STREAM_V_data_V_0_state_reg[0] ),
        .I3(Q[1]),
        .O(we0));
  LUT5 #(
    .INIT(32'hF3C0E2E2)) 
    ram_reg_i_5
       (.I0(\tmp_reg_325_reg[6] [3]),
        .I1(Q[2]),
        .I2(\a_a_1_reg_197_reg[5] [3]),
        .I3(\i_reg_186_reg[5] [3]),
        .I4(Q[1]),
        .O(address0[3]));
  LUT5 #(
    .INIT(32'hFAFC0A0C)) 
    ram_reg_i_6
       (.I0(\i_reg_186_reg[5] [2]),
        .I1(\tmp_reg_325_reg[6] [2]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(\a_a_1_reg_197_reg[5] [2]),
        .O(address0[2]));
  LUT5 #(
    .INIT(32'hF3C0E2E2)) 
    ram_reg_i_7
       (.I0(\tmp_reg_325_reg[6] [1]),
        .I1(Q[2]),
        .I2(\a_a_1_reg_197_reg[5] [1]),
        .I3(\i_reg_186_reg[5] [1]),
        .I4(Q[1]),
        .O(address0[1]));
  LUT5 #(
    .INIT(32'hFAFC0A0C)) 
    ram_reg_i_8
       (.I0(\i_reg_186_reg[5] [0]),
        .I1(\tmp_reg_325_reg[6] [0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(\a_a_1_reg_197_reg[5] [0]),
        .O(address0[0]));
  LUT4 #(
    .INIT(16'hC808)) 
    ram_reg_i_9
       (.I0(\INPUT_STREAM_V_data_V_0_payload_A_reg[31] [10]),
        .I1(Q[1]),
        .I2(INPUT_STREAM_V_data_V_0_sel),
        .I3(\INPUT_STREAM_V_data_V_0_payload_B_reg[31] [10]),
        .O(d0[15]));
endmodule

(* CHECK_LICENSE_TYPE = "system_Adder2_0_0,Adder2,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Adder2,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s_axi_CONTROL_BUS_AWADDR,
    s_axi_CONTROL_BUS_AWVALID,
    s_axi_CONTROL_BUS_AWREADY,
    s_axi_CONTROL_BUS_WDATA,
    s_axi_CONTROL_BUS_WSTRB,
    s_axi_CONTROL_BUS_WVALID,
    s_axi_CONTROL_BUS_WREADY,
    s_axi_CONTROL_BUS_BRESP,
    s_axi_CONTROL_BUS_BVALID,
    s_axi_CONTROL_BUS_BREADY,
    s_axi_CONTROL_BUS_ARADDR,
    s_axi_CONTROL_BUS_ARVALID,
    s_axi_CONTROL_BUS_ARREADY,
    s_axi_CONTROL_BUS_RDATA,
    s_axi_CONTROL_BUS_RRESP,
    s_axi_CONTROL_BUS_RVALID,
    s_axi_CONTROL_BUS_RREADY,
    ap_clk,
    ap_rst_n,
    interrupt,
    INPUT_STREAM_TVALID,
    INPUT_STREAM_TREADY,
    INPUT_STREAM_TDATA,
    INPUT_STREAM_TDEST,
    INPUT_STREAM_TKEEP,
    INPUT_STREAM_TSTRB,
    INPUT_STREAM_TUSER,
    INPUT_STREAM_TLAST,
    INPUT_STREAM_TID);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_CONTROL_BUS, ADDR_WIDTH 6, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [5:0]s_axi_CONTROL_BUS_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWVALID" *) input s_axi_CONTROL_BUS_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS AWREADY" *) output s_axi_CONTROL_BUS_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WDATA" *) input [31:0]s_axi_CONTROL_BUS_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WSTRB" *) input [3:0]s_axi_CONTROL_BUS_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WVALID" *) input s_axi_CONTROL_BUS_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS WREADY" *) output s_axi_CONTROL_BUS_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BRESP" *) output [1:0]s_axi_CONTROL_BUS_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BVALID" *) output s_axi_CONTROL_BUS_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS BREADY" *) input s_axi_CONTROL_BUS_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARADDR" *) input [5:0]s_axi_CONTROL_BUS_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARVALID" *) input s_axi_CONTROL_BUS_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS ARREADY" *) output s_axi_CONTROL_BUS_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RDATA" *) output [31:0]s_axi_CONTROL_BUS_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RRESP" *) output [1:0]s_axi_CONTROL_BUS_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RVALID" *) output s_axi_CONTROL_BUS_RVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_CONTROL_BUS RREADY" *) input s_axi_CONTROL_BUS_RREADY;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axi_CONTROL_BUS:INPUT_STREAM, ASSOCIATED_RESET ap_rst_n, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {CLK {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {RST {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME interrupt, SENSITIVITY LEVEL_HIGH, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {INTERRUPT {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}}}, PortWidth 1" *) output interrupt;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME INPUT_STREAM, TDATA_NUM_BYTES 4, TDEST_WIDTH 6, TID_WIDTH 5, TUSER_WIDTH 2, LAYERED_METADATA undef, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0" *) input INPUT_STREAM_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TREADY" *) output INPUT_STREAM_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDATA" *) input [31:0]INPUT_STREAM_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TDEST" *) input [5:0]INPUT_STREAM_TDEST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TKEEP" *) input [3:0]INPUT_STREAM_TKEEP;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TSTRB" *) input [3:0]INPUT_STREAM_TSTRB;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TUSER" *) input [1:0]INPUT_STREAM_TUSER;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TLAST" *) input [0:0]INPUT_STREAM_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 INPUT_STREAM TID" *) input [4:0]INPUT_STREAM_TID;

  wire [31:0]INPUT_STREAM_TDATA;
  wire [5:0]INPUT_STREAM_TDEST;
  wire [4:0]INPUT_STREAM_TID;
  wire [3:0]INPUT_STREAM_TKEEP;
  wire [0:0]INPUT_STREAM_TLAST;
  wire INPUT_STREAM_TREADY;
  wire [3:0]INPUT_STREAM_TSTRB;
  wire [1:0]INPUT_STREAM_TUSER;
  wire INPUT_STREAM_TVALID;
  wire ap_clk;
  wire ap_rst_n;
  wire interrupt;
  wire [5:0]s_axi_CONTROL_BUS_ARADDR;
  wire s_axi_CONTROL_BUS_ARREADY;
  wire s_axi_CONTROL_BUS_ARVALID;
  wire [5:0]s_axi_CONTROL_BUS_AWADDR;
  wire s_axi_CONTROL_BUS_AWREADY;
  wire s_axi_CONTROL_BUS_AWVALID;
  wire s_axi_CONTROL_BUS_BREADY;
  wire [1:0]s_axi_CONTROL_BUS_BRESP;
  wire s_axi_CONTROL_BUS_BVALID;
  wire [31:0]s_axi_CONTROL_BUS_RDATA;
  wire s_axi_CONTROL_BUS_RREADY;
  wire [1:0]s_axi_CONTROL_BUS_RRESP;
  wire s_axi_CONTROL_BUS_RVALID;
  wire [31:0]s_axi_CONTROL_BUS_WDATA;
  wire s_axi_CONTROL_BUS_WREADY;
  wire [3:0]s_axi_CONTROL_BUS_WSTRB;
  wire s_axi_CONTROL_BUS_WVALID;

  (* C_S_AXI_CONTROL_BUS_ADDR_WIDTH = "6" *) 
  (* C_S_AXI_CONTROL_BUS_DATA_WIDTH = "32" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Adder2 U0
       (.INPUT_STREAM_TDATA(INPUT_STREAM_TDATA),
        .INPUT_STREAM_TDEST(INPUT_STREAM_TDEST),
        .INPUT_STREAM_TID(INPUT_STREAM_TID),
        .INPUT_STREAM_TKEEP(INPUT_STREAM_TKEEP),
        .INPUT_STREAM_TLAST(INPUT_STREAM_TLAST),
        .INPUT_STREAM_TREADY(INPUT_STREAM_TREADY),
        .INPUT_STREAM_TSTRB(INPUT_STREAM_TSTRB),
        .INPUT_STREAM_TUSER(INPUT_STREAM_TUSER),
        .INPUT_STREAM_TVALID(INPUT_STREAM_TVALID),
        .ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .interrupt(interrupt),
        .s_axi_CONTROL_BUS_ARADDR(s_axi_CONTROL_BUS_ARADDR),
        .s_axi_CONTROL_BUS_ARREADY(s_axi_CONTROL_BUS_ARREADY),
        .s_axi_CONTROL_BUS_ARVALID(s_axi_CONTROL_BUS_ARVALID),
        .s_axi_CONTROL_BUS_AWADDR(s_axi_CONTROL_BUS_AWADDR),
        .s_axi_CONTROL_BUS_AWREADY(s_axi_CONTROL_BUS_AWREADY),
        .s_axi_CONTROL_BUS_AWVALID(s_axi_CONTROL_BUS_AWVALID),
        .s_axi_CONTROL_BUS_BREADY(s_axi_CONTROL_BUS_BREADY),
        .s_axi_CONTROL_BUS_BRESP(s_axi_CONTROL_BUS_BRESP),
        .s_axi_CONTROL_BUS_BVALID(s_axi_CONTROL_BUS_BVALID),
        .s_axi_CONTROL_BUS_RDATA(s_axi_CONTROL_BUS_RDATA),
        .s_axi_CONTROL_BUS_RREADY(s_axi_CONTROL_BUS_RREADY),
        .s_axi_CONTROL_BUS_RRESP(s_axi_CONTROL_BUS_RRESP),
        .s_axi_CONTROL_BUS_RVALID(s_axi_CONTROL_BUS_RVALID),
        .s_axi_CONTROL_BUS_WDATA(s_axi_CONTROL_BUS_WDATA),
        .s_axi_CONTROL_BUS_WREADY(s_axi_CONTROL_BUS_WREADY),
        .s_axi_CONTROL_BUS_WSTRB(s_axi_CONTROL_BUS_WSTRB),
        .s_axi_CONTROL_BUS_WVALID(s_axi_CONTROL_BUS_WVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
