#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"


void startAdder(int adderAddress);

u32 isAdderDone(int adderAddress);

u32 adderGetReturn(int adderAddress);
