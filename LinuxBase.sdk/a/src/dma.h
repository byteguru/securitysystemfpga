#include "adder.h"
#include "memory.h"
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

void Add(int *in,
		int *out,
		int length);
