library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity clockDevider is
    Port ( sys_clk : in STD_LOGIC;
           clk : out STD_LOGIC_VECTOR(32 downto 0));
end clockDevider;

architecture Behavioral of clockDevider is

signal div : STD_LOGIC_VECTOR (32 downto 0 );

begin

    process(sys_clk)
    begin
        if(sys_clk'event and sys_clk = '1')
        then
            div <= std_logic_vector(unsigned(div) + 1);
        end if;
    end process;
    
    clk <= div;


end Behavioral;